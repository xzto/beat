#include "beat_include.hh"

#if 0
static inline NoteTime
os_time_to_song_ts(GameState *gs, f64 os_time)
{
    ASSERT(!"NOT IMPLEMENTED");
    ASSERT(gs->playing.other.state >= PlayingState_Started);
    NoteTime song_ts = NOTE_TIMESTAMP_FROM_SECONDS(os_time - gs->playing.other.os_time_offset);
    return song_ts;
}
#endif

static inline f64
get_judge_latest_seconds()
{
    return JUDGE_BAD_MAX;
}
static inline f64
get_judge_earliest_seconds()
{
    // @HardcodedJudge
    return -JUDGE_BAD_MAX;
}

static inline bool
input_is_too_late_for_roll_middle(f64 seconds)
{
    return seconds >= get_judge_latest_seconds();
}
static inline bool
input_is_too_early_for_roll_middle(f64 seconds)
{
    return seconds <= get_judge_earliest_seconds();
}

static inline bool
input_is_too_late_for_hold_finish(f64 seconds)
{
    return seconds >= get_judge_latest_seconds();
}
static inline bool
input_is_too_early_for_hold_finish(f64 seconds)
{
    return seconds <= get_judge_earliest_seconds();
}

static inline bool
input_is_valid_for_hold_finish(f64 seconds)
{
    if (seconds < 0) {
        return !input_is_too_early_for_hold_finish(seconds);
    } else {
        return !input_is_too_late_for_hold_finish(seconds);
    }
}

static inline bool
input_is_too_early(f64 seconds) {
    return seconds <= get_judge_earliest_seconds();
}

static inline bool
input_is_too_late(f64 seconds) {
    return seconds >= get_judge_latest_seconds();
}

static inline bool
input_is_valid(f64 seconds) {
    // real_ts_diff was made with input_time - note_time;
    if (seconds < 0) {
        return !input_is_too_early(seconds);
    } else {
        return !input_is_too_late(seconds);
    }
}

// Returns -1 if too early, +1 if too late, 0 if ok
static inline s32
input_early_or_late(f64 seconds)
{
    if (seconds < 0) {
        return input_is_too_early(seconds) ? -1 : 0;
    } else {
        return input_is_too_late(seconds)  ? 1 : 0;
    }
}

static InputActionStuff
get_input_action_tap(f64 rate_float, NoteTimestamp playing_ts, TimedInput *ti1, TimedInput *ti2, VisibleNote *vn,
                     s32 idx_column)
{
    ASSERT(!(vn->flags & VN_TAP_IS_DEAD));
    ASSERT(!(vn->flags & VN_TAP_HIT));
    InputActionStuff result = {};
    // To make the tap, this note must be the first to use this input.
    // If not for this, the same input could be used for two taps.

    // To make it explicit.
    bool we_want_the_default_case = false;
    // TODO: What if we press the key much earlier with (t1->times_used == 0) and never release it?
    // Currently, we only give a TAP_DIE when we release the key.
    if (ti1->pressed) {
        if (ti1->times_used == 0) {
            ASSERT(vn->inputs_used == 0);
            f64 offset_seconds = note_timestamp_diff_to_seconds(ti1->song_ts - vn->song_ts, rate_float);
            s32 early_or_late = input_early_or_late(offset_seconds);
            if (early_or_late < 0) {
                // Do the default case down there
                we_want_the_default_case = true;
            } else if (early_or_late > 0) {
                // Input is too late and for some reason we didn't make it die
                // already (it's unlikely the scenario is valid, implementing just
                // to be sure)
                result.type = INPUT_ACTION_TAP_DIE;
                result.tap.offset_seconds = {JUDGE_MISS_MIN}; // @HardcodedJudge
                // result.input_song_ts = vn->song_ts + get_judge_latest_ts();
                return result;
            } else {
                result.type = INPUT_ACTION_TAP_TAPS;
                result.tap.offset_seconds = offset_seconds;
                // result.input_song_ts = ti1->song_ts;
                return result;
            }
        } else {
            // Do the default case down there
            we_want_the_default_case = true;
        }
    } else {
        // Do the default case down there
        we_want_the_default_case = true;
    }
    // The default case. If there is more input it does nothing. If there isn't
    // more input it tests if we're too late and dies if necessary.
    ASSERT(we_want_the_default_case);
    if (ti2) {
        result.type = INPUT_ACTION_NOTHING;
        return result;
    } else {
        // @BugFramerateDependantInput
        f64 offset_seconds_playing = note_timestamp_diff_to_seconds(playing_ts - vn->song_ts, rate_float);
        if (input_is_too_late(offset_seconds_playing)) {
            // There isn't more input and the key is released and it is too
            // late to have more inputs. Die!
            result.type = INPUT_ACTION_TAP_DIE;
            result.tap.offset_seconds = {JUDGE_MISS_MIN}; // @HardcodedJudge
            // result.input_song_ts = vn->song_ts + get_judge_latest_ts();
            return result;
        } else {
            // We can still have more valid input for this key
            result.type = INPUT_ACTION_NOTHING;
            return result;
        }
    }
    INVALID_CODE_PATH;
    result.type = INPUT_ACTION_NOTHING;
    return result;
}

static InputActionStuff
get_input_action_mine(f64 rate_float, NoteTimestamp playing_ts, TimedInput *ti1, TimedInput *ti2,
                      VisibleNote *vn, s32 idx_column)
{
    ASSERT(!(vn->flags & VN_MINE_IS_DEAD));
    ASSERT(!(vn->flags & VN_MINE_EXPLODED));
    InputActionStuff result = {};
#if 0
    // Let's only consider hitting the mine if we are past it.
    // @BugFramerateDependantInput
    if (playing_ts < vn->song_ts) {
        // TODO: Properly test the input even here. That's to make sure replays work as expected.
        // Technically, a mine could DIE in the gameplay but EXPLODE if we replay the inputs.
        // Can't explode the mine because we're too far away from it.
        result.type = INPUT_ACTION_NOTHING;
        return result;
    }
#endif

    if (ti1->pressed) {
        if (ti1->song_ts > vn->song_ts) {
            // we press it after the mine, die
            result.type = INPUT_ACTION_MINE_DIE;
            // result.input_song_ts = vn->song_ts;
            return result;
        } else {
            // we press before the mine
            if (ti2) {
                if (ti2->song_ts > vn->song_ts) {
                    // we press before and release after the mine
                    result.type = INPUT_ACTION_MINE_EXPLODE;
                    // result.input_song_ts = vn->song_ts;
                    return result;
                } else {
                    // we release before the mine.
                    result.type = INPUT_ACTION_NOTHING;
                    return result;
                }
            } else {
                // we don't have any more input right now. test playing_time
                // will assume we don't release after vn->song_ts + X.
                // TODO: Verify that inputs are always < playing_ts. then I won't need "ts_to_skip_to_assume_no_release"
                // If there was more input after the mine, we will DIE or EXPLODE immediately when we get the input.
                // If there wasn't more input after the mine, we will only DIE or EXPLODE after 0.01s have passed from the mine.
                // TODO: If I can confirm later that inputs are always < playing_ts (or is it > ??idk), I won't need it.
                // ts_to_skip_to_assume_no_release is probably useless if the frametime is less than it.
                NoteTimestampDiff ts_to_skip_to_assume_no_release = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(0.01)};
                if (playing_ts > vn->song_ts + ts_to_skip_to_assume_no_release) {
                    // we're still pressed and we don't release
                    result.type = INPUT_ACTION_MINE_EXPLODE;
                    // result.input_song_ts = vn->song_ts;
                    return result;
                } else {
                    result.type = INPUT_ACTION_NOTHING;
                    return result;
                }
            }
        }
    } else {
        // TODO: Verify that inputs are always < playing_ts. then I won't need "ts_to_skip_to_assume_no_release"
        // ts_to_skip_to_assume_no_release is probably useless if the frametime is less than it.
        NoteTimestampDiff ts_to_skip_to_assume_no_release = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(0.01)};
        if (playing_ts > vn->song_ts + ts_to_skip_to_assume_no_release) {
            // we're still released and we didn't press
            result.type = INPUT_ACTION_MINE_DIE;
            // result.input_song_ts = vn->song_ts;
            return result;
        } else {
            result.type = INPUT_ACTION_NOTHING;
            return result;
        }
    }
    INVALID_CODE_PATH;
    result.type = INPUT_ACTION_NOTHING;
    return result;
}

static InputActionStuff
get_input_action_hold(f64 rate_float, NoteTimestamp playing_ts, TimedInput *ti1, TimedInput *ti2,
                      VisibleNote *vn, s32 idx_column)
{
    // I don't support things like
    // start hold // you tap
    // start hold // you quickly release and tap
    // end hold   // you release
    // All the starts have to have matching end before a new start begins
    // Maybe that's a problem and if i find that it is, i'll implement that feature
    ASSERT(!(vn->flags & VN_HOLD_IS_DEAD));
    InputActionStuff result = {};
    // @BugFramerateDependantInput
    // TODO: Later Verify that result.input_song_ts is right for every case.
    f64 offset_seconds_start_playing = note_timestamp_diff_to_seconds(playing_ts - vn->song_ts, rate_float);
    f64 offset_seconds_end_playing   = note_timestamp_diff_to_seconds(playing_ts - vn->hold.song_ts_end, rate_float);
    f64 offset_seconds_start         = note_timestamp_diff_to_seconds(ti1->song_ts - vn->song_ts, rate_float);
    f64 offset_seconds_end           = note_timestamp_diff_to_seconds(ti1->song_ts - vn->hold.song_ts_end, rate_float);
    // I have feeling that something is wrong here. Something to do with the release key.
    if (vn->flags & VN_HOLD_IS_RELEASED) { // Hold, is released
        if (vn->hold.times_released > 0) { // did release in the middle
            if (ti1->pressed) {
                ASSERT(ti1->times_used == 0);
                // ti1->times_used can't be != 0 if ti1->pressed.
                if (input_is_valid(offset_seconds_end) || offset_seconds_end < 0) {
                    // grab it
                    result.type = INPUT_ACTION_HOLD_GRAB_OTHER;
                    result.hold.other_grab.offset_seconds_from_start = offset_seconds_start;
                    result.hold.other_grab.offset_seconds_from_release =
                        note_timestamp_diff_to_seconds(ti1->song_ts - vn->latest_input_song_ts, rate_float);
                    // result.input_song_ts = ti1->song_ts;
                    return result;
                } else {
                    result.type = INPUT_ACTION_NOTHING;
                    return result;
                }
            } else {
                // @HardcodedJudge
                if (input_is_too_late_for_hold_finish(offset_seconds_end_playing)) {
                    // released it way too early before and didn't regrab
                    result.type = INPUT_ACTION_HOLD_BREAK_END_WASREL;
                    // result.input_song_ts = vn->hold.song_ts_end + get_judge_latest_ts();
                    return result;
                } else {
                    result.type = INPUT_ACTION_HOLD_BREAK_CONTINUE;
                    return result;
                }
            }
        } else { // didn't release in the middle, so we didnt hit it yet
            // TODO: If HOLD_DIE does not set VN_FINISHED_INPUT, I need to make some changes here. Currently, it would say first grab when you actually broke it once. Maybe I should create a ias.hold.first_grab_after_die.
            bool we_want_the_default_case = false;
            if (ti1->pressed) {
                if (ti1->times_used == 0) {
                    // grab it
                    ASSERT(vn->inputs_used == 0);
                    s32 early_or_late = input_early_or_late(offset_seconds_start);
                    if (early_or_late < 0) {
                        // Do the default case down there
                        we_want_the_default_case = true;
                    } else if (early_or_late > 0) {
                        // Input is too late and for some reason we didn't make it die
                        // already (it's unlikely the scenario is valid, implementing just
                        // to be sure)
                        result.type = INPUT_ACTION_HOLD_DIE;
                        result.hold.first_grab.offset_seconds = {JUDGE_MISS_MIN};
                        // result.input_song_ts = vn->song_ts + get_judge_latest_ts();
                        return result;
                    } else {
                        result.type = INPUT_ACTION_HOLD_GRAB;
                        result.hold.first_grab.offset_seconds = offset_seconds_start;
                        // result.input_song_ts = ti1->song_ts;
                        return result;
                    }
                } else {
                    we_want_the_default_case = true;
                }
            } else {
                we_want_the_default_case = true;
            }
            ASSERT(we_want_the_default_case);
            if (ti2) {
                result.type = INPUT_ACTION_NOTHING;
                return result;
            } else {
                if (input_is_too_late(offset_seconds_start_playing)) {
                    result.type = INPUT_ACTION_HOLD_DIE;
                    result.hold.first_grab.offset_seconds = {JUDGE_MISS_MIN}; // @HardcodedJudge
                    // result.input_song_ts = vn->song_ts + get_judge_latest_ts();
                    return result;
                } else {
                    // We can still have more valid input for this key
                    result.type = INPUT_ACTION_NOTHING;
                    return result;
                }
            }
        }
    } else if (vn->flags & VN_HOLD_IS_PRESSED) { // Hold is pressed
        // Also have to check for the finish thing
        if (ti1->pressed) { // key is pressed
            // @HardcodedJudge
            if (!ti2 && input_is_too_late(offset_seconds_end_playing)) {
                // it's too late and we didnt release
                result.type = INPUT_ACTION_HOLD_BREAK_END_WASPRESS;
                // result.hold.break_end_was_pressed.real_ts_diff = NOTE_TIMESTAMP_FROM_SECONDS(JUDGE_MISS_MIN);
                // result.input_song_ts = vn->hold.song_ts_end + get_judge_latest_ts();
                return result;
            } else {
                // keep it pressed
                result.type = INPUT_ACTION_HOLD_GRAB_CONTINUE;
                return result;
            }
        } else { // key is released
            // either break it or finish it
            // @HardcodedJudge
            if (input_is_valid_for_hold_finish(offset_seconds_end)) {
                // finish it
                result.type = INPUT_ACTION_HOLD_FINISH;
                result.hold.finish.offset_seconds = offset_seconds_end;
                // result.input_song_ts = ti1->song_ts;
                return result;
            } else {
                // break it
                result.type = INPUT_ACTION_HOLD_BREAK_CAN_REGRAB;
                // result.input_song_ts = ti1->song_ts;
                return result;
            }
        }
    } else {
        // Hold is neither released nor pressed (bug)
        INVALID_CODE_PATH;
    }
    // Should have returned before this line
    INVALID_CODE_PATH;
    result.type = INPUT_ACTION_NOTHING;
    return result;
}

static InputActionStuff
get_input_action_roll(f64 rate_float, NoteTimestamp playing_ts, TimedInput *ti1, TimedInput *ti2,
                      VisibleNote *vn, s32 idx_column)
{
    ASSERT(!(vn->flags & VN_SMROLL_IS_DEAD));
    // @BugFramerateDependantInput
    // TODO: Verify how exactly a "roll" should behave, particularly when close
    // to the end: If there is a roll and right after the roll there's a tap, I
    // think the tap should register input only after playing_ts > roll_end_ts.
    InputActionStuff result = {};
    f64 offset_seconds_start_playing = note_timestamp_diff_to_seconds(playing_ts - vn->song_ts, rate_float);
    f64 offset_seconds_end_playing   = note_timestamp_diff_to_seconds(playing_ts - vn->smroll.song_ts_end, rate_float);
    f64 offset_seconds_start         = note_timestamp_diff_to_seconds(ti1->song_ts - vn->song_ts, rate_float);
    f64 offset_seconds_end           = note_timestamp_diff_to_seconds(ti1->song_ts - vn->smroll.song_ts_end, rate_float);
    f64 offset_seconds_last;
    f64 offset_seconds_last_playing;
    if (vn->smroll.times_tapped > 0) {
        ASSERT(vn->inputs_used > 0);
        offset_seconds_last = note_timestamp_diff_to_seconds(ti1->song_ts - vn->latest_input_song_ts, rate_float);
        offset_seconds_last_playing = note_timestamp_diff_to_seconds(playing_ts - vn->latest_input_song_ts, rate_float);
    } else {
        ASSERT(vn->inputs_used == 0);
        offset_seconds_last         = note_timestamp_diff_to_seconds(ti1->song_ts - vn->song_ts, rate_float);
        offset_seconds_last_playing = note_timestamp_diff_to_seconds(playing_ts - vn->song_ts, rate_float);
    }

    // TODO: Modify this like i did get_input_action_tap
    // TODO: Don't use real_diff_xxx_playing so much

    if (input_is_too_early(offset_seconds_start)) {
        result.type = INPUT_ACTION_NOTHING;
        return result;
    }

    if (ti1->pressed && ti1->times_used == 0) {
        // This is a new tap.
        if (vn->smroll.times_tapped == 0) {
            // It's the first tap
            result.type = INPUT_ACTION_SMROLL_TAP_FIRST;
            result.smroll.offset_seconds = offset_seconds_last;
            // result.input_song_ts = ti1->song_ts;
            return result;
        } else {
            // Already tapped before
            // hit it
            result.type = INPUT_ACTION_SMROLL_TAP_CONTINUE;
            result.smroll.offset_seconds = offset_seconds_last;
            // result.input_song_ts = ti1->song_ts;
            return result;
        }
    } else {
        // Not a new tap.
        if (offset_seconds_end_playing >= 0 || offset_seconds_end >= 0) {
            // Already past the end of the roll.
            if (vn->smroll.times_tapped == 0) {
                // @HardcodedJudge
                if (!ti2 && input_is_too_late(offset_seconds_start_playing)) {
                    // @BugFramerateDependantInput
                    // This should be DIE_FINISH instead.
                    result.type = INPUT_ACTION_SMROLL_DIE;
                    result.smroll.offset_seconds = {JUDGE_MISS_MIN};
                    // result.input_song_ts = vn->song_ts + get_judge_latest_ts();
                    return result;
                } else {
                    result.type = INPUT_ACTION_NOTHING;
                    return result;
                }
            } else {
                result.type = INPUT_ACTION_SMROLL_FINISH;
                return result;
            }
        } else {
            // Still inside the roll
            if (vn->smroll.times_tapped == 0) {
                // No taps yet
                // @HardcodedJudge
                if (ti2 == NULL && input_is_too_late(offset_seconds_start_playing)) {
                    // This is wrong! I should make INPUT_ACTION_SMROLL_DIE_CAN_CONTINUE (here) and DIE_FINISH
                    result.type = INPUT_ACTION_SMROLL_DIE;
                    result.smroll.offset_seconds = {JUDGE_MISS_MIN};
                    // result.input_song_ts = vn->song_ts + get_judge_latest_ts();
                    return result;
                }
            } else {
                // Did tap at least once
                if (ti2 == NULL && input_is_too_late_for_roll_middle(offset_seconds_last_playing)) {
                    // It's too late
                    result.type = INPUT_ACTION_SMROLL_BREAK;
                    result.smroll.offset_seconds = {JUDGE_MISS_MIN};
                    // TODO: verify this input_song_ts
                    // result.input_song_ts = vn->latest_input_song_ts + get_judge_latest_ts();
                    return result;
                }
            }
            result.type = INPUT_ACTION_NOTHING;
            return result;
        }
    }
    INVALID_CODE_PATH;
    result.type = INPUT_ACTION_NOTHING;
    return result;
}

// Returns whether the input does something to the note.
// Returns an enum of the action the input will do.
// Does not do the action, the caller should do that.
//
// Maybe return a struct containing information that this function computed, like time_diff for taps
// Maybe also pass an argument for gamemode information like the timing windows,
// if you can regrab a hold, and stuff.
static inline InputActionStuff
get_input_action(f64 rate_float, NoteTimestamp playing_ts, TimedInput *ti1, TimedInput *ti2,
                 VisibleNote *vn, s32 idx_column)
{
    ASSERT(ti1 != NULL);

    if (vn->flags & VN_FINISHED_INPUT)
        return { .type=INPUT_ACTION_NOTHING };

    // @NoteType
    switch (vn->type) {
    case NOTE_BLANK:    return { .type=INPUT_ACTION_NOTHING };
    case NOTE_TAP:      return get_input_action_tap(rate_float,  playing_ts, ti1, ti2, vn, idx_column);
    case NOTE_MINE:     return get_input_action_mine(rate_float, playing_ts, ti1, ti2, vn, idx_column);
    case NOTE_HOLD:     return get_input_action_hold(rate_float, playing_ts, ti1, ti2, vn, idx_column);
    case NOTE_SMROLL:   return get_input_action_roll(rate_float, playing_ts, ti1, ti2, vn, idx_column);
    default: INVALID_CODE_PATH; break;
    }

    INVALID_CODE_PATH;
    return { .type=INPUT_ACTION_NOTHING };
}

static int
compare_visible_notes(const void *a, const void *b)
{
    VisibleNote *vn_a = (VisibleNote *)a;
    VisibleNote *vn_b = (VisibleNote *)b;
    NoteTimestampDiff diff = vn_a->song_ts - vn_b->song_ts;
    // int result = (int)diff;
    int result = diff.value == 0 ? 0 : (diff.value < 0 ? -1 : 1);
    return result;
}

static void
sort_visible_notes(GameState *gs)
{
    s32 columns = gs->playing.other.columns;
    ASSERT(columns > 0);

    for (s32 idx_col = 0; idx_col < columns; idx_col++) {
        NewArrayDynamic<VisibleNote> *vns = &gs->playing.other.per_column[idx_col].vns;
        ASSERT(vns != NULL);
        if (vns->len > 0) {
            qsort(vns->data, (size_t)vns->len, (size_t)SSIZEOF_VAL(vns->data),
                  compare_visible_notes);
        }
    }
}

static void
make_sure_visible_notes_are_sorted(GameState *gs)
{
    s32 columns = gs->playing.other.columns;
    ASSERT(columns > 0);

    for (s32 idx_col = 0; idx_col < columns; idx_col++) {
        NewArrayDynamic<VisibleNote> *vns = &gs->playing.other.per_column[idx_col].vns;
        for (s32 idx_vn = 0; idx_vn < vns->len - 1; idx_vn++) {
            VisibleNote *vn1 = &vns->data[idx_vn];
            VisibleNote *vn2 = &vns->data[idx_vn+1];
            if (compare_visible_notes(vn1, vn2) > 0) {
                INVALID_CODE_PATH;
                return;
            }
        }
    }
}


// Not wife
static f64
wife(f64 seconds)
{
    f64 result = 0.0;
    f64 abs_seconds = seconds < 0 ? -seconds : seconds;
    // TODO: Figure out why the assertion failed and possibly remove this clamp.
    abs_seconds = clamp(abs_seconds, (f64)0, (f64)JUDGE_MISS_MIN);
    ASSERT(abs_seconds <= JUDGE_MISS_MIN);


    // TODO: Proper wife.
    struct {
        f64 seconds_start, seconds_end;
        f64 score_start, score_end;
    } curves[] = {
#if 0
        {0,             JUDGE_MARV,     SCORE_FOR_MARV,     SCORE_FOR_PERF},
        {JUDGE_MARV,    JUDGE_PERF,     SCORE_FOR_PERF,     SCORE_FOR_GREAT},
        {JUDGE_PERF,    JUDGE_GREAT,    SCORE_FOR_GREAT,    SCORE_FOR_GOOD},
        {JUDGE_GREAT,   JUDGE_GOOD,     SCORE_FOR_GOOD,     SCORE_FOR_BAD},
        {JUDGE_GOOD,    JUDGE_BAD,      SCORE_FOR_BAD,      SCORE_FOR_MISS},
        // {JUDGE_BAD, F64_MAX, SCORE_FOR_MISS, SCORE_FOR_MISS},
#else
        // https://cdn.discordapp.com/attachments/688449872612950151/696112821494087741/unknown.png
        // wife3 2020-04-08
        {0,             0.0215,         2.0,                2.0},
        {0.0215,        0.0300,         2.0,                1.875},
        {0.0300,        0.0400,         1.875,              1.75},
        {0.0400,        0.0450,         1.75,               1.5},
        {0.0450,        0.0850,         1.5,               -1.75},
        {0.0850,        JUDGE_MISS_MIN, -1.75,             -5.5},
#endif
    };

    if (abs_seconds >= curves[ARRAY_COUNT(curves)-1].seconds_end) {
        result = SCORE_FOR_MISS;
    } else {
        b32 got_it = false;
        for (s32 i = 0; i < ARRAY_COUNT(curves); i++) {
            if (abs_seconds < curves[i].seconds_end) {
                result = lerp(curves[i].score_start, curves[i].score_end,
                              (abs_seconds - curves[i].seconds_start) /
                              (curves[i].seconds_end - curves[i].seconds_start));
                got_it = true;
                break;
            }
        }
        ASSERT(got_it);
    }

    return result;
}

static inline s32
find_vn(NewArrayDynamic<VisibleNote> *vns, NoteType to_find_type, NoteTimestamp to_find_song_ts)
{
    for (s32 idx_vn = 0; idx_vn < vns->len; idx_vn++) {
        VisibleNote *vn = &vns->data[idx_vn];
        if (vn->song_ts == to_find_song_ts && vn->type == to_find_type)
            return idx_vn;
    }
    return -1;
}

static inline s32
add_vn(NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_add)
{
    vns->push(*to_add);
    return (s32)(vns->len - 1);
}

static inline s32
add_tap(NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_add)
{
    return add_vn(vns, to_add);
}

static inline s32
add_mine(NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_add)
{
    return add_vn(vns, to_add);
}

static s32
add_hold_start(Diff *diff, s32 column, NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_add)
{
    s32 end = find_hold_end_idx_with_start_idx(&diff->notes, column, to_add->idx_row, NOTE_HOLD_END);
    if (end >= 0) {
        ASSERT(end < diff->notes.rows_count);
        to_add->hold.idx_row_end = end;
        to_add->hold.song_ts_end = diff->notes.timestamps[end];
    } else {
        to_add->hold.idx_row_end = -1;
        to_add->hold.song_ts_end = NOTE_MAX_TIMESTAMP;
    }
    return add_vn(vns, to_add);
}

static s32
add_roll_start(Diff *diff, s32 column, NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_add)
{
    s32 end = find_hold_end_idx_with_start_idx(&diff->notes, column, to_add->idx_row, NOTE_SMROLL_END);
    if (end >= 0) {
        ASSERT(end < diff->notes.rows_count);
        to_add->smroll.idx_row_end = end;
        to_add->smroll.song_ts_end = diff->notes.timestamps[end];
    } else {
        to_add->smroll.idx_row_end = -1;
        to_add->smroll.song_ts_end = NOTE_MAX_TIMESTAMP;
    }
    return add_vn(vns, to_add);
}

static s32
find_hold_with_end(NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_find)
{
    for (s32 idx_vn = 0; idx_vn < vns->len; idx_vn++) {
        VisibleNote *vn = &vns->data[idx_vn];
        if (vn->hold.song_ts_end == to_find->hold.song_ts_end) {
            ASSERT(vn->type == NOTE_HOLD);
            return idx_vn;
        }
    }
    return -1;
}

static s32
find_roll_with_end(NewArrayDynamic<VisibleNote> *vns, VisibleNote *to_find)
{
    for (s32 idx_vn = 0; idx_vn < vns->len; idx_vn++) {
        VisibleNote *vn = &vns->data[idx_vn];
        if (vn->smroll.song_ts_end == to_find->smroll.song_ts_end) {
            ASSERT(vn->type == NOTE_SMROLL);
            return idx_vn;
        }
    }
    return -1;
}

struct AccBuilderAddThingArgs {
    f64 offset_seconds;
    bool miss;
    f64 delta_max_score_right_now;
    bool delta_score_is_wife_of_offset_seconds;
    f64 delta_score;
};

static void
acc_builder_add_thing(GameState *gs, s32 idx_column, AccBuilderAddThingArgs args)
{
    s32 idx = gs->playing.other.acc_index;
    TestAcc *acc = &gs->playing.other.acc[idx];
    zero_struct(acc);
    acc->offset_seconds = args.offset_seconds;
    acc->playing_ts = gs->playing.other.newthings.song_ts;
    acc->miss = args.miss;
    gs->playing.other.acc_index = (idx + 1) % ARRAY_COUNT(gs->playing.other.acc);
    gs->playing.other.acc_total++;

    gs->playing.other.per_column[idx_column].debug_test_accs.push(*acc);

    if (args.delta_max_score_right_now != 0) {
        gs->playing.other.max_score_right_now += args.delta_max_score_right_now;
    }
    if (args.delta_score_is_wife_of_offset_seconds) {
        ASSERT(args.delta_score == 0);
        gs->playing.other.score += wife(acc->offset_seconds);
    } else if (args.delta_score != 0) {
        gs->playing.other.score += args.delta_score;
    }
}

static void
do_acc_system(GameState *gs, VisibleNote *vn, s32 idx_column, InputActionStuff *action)
{
    s32 idx = gs->playing.other.acc_index;
    switch (action->type) {
    case INPUT_ACTION_NOTHING: {
        // Do nothing
    } break;

    case INPUT_ACTION_TAP_DIE: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = JUDGE_MISS_MIN,
                              .miss = true,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score = SCORE_FOR_MISS
                              });
    } break;
    case INPUT_ACTION_TAP_TAPS: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = action->tap.offset_seconds,
                              .miss = false,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score_is_wife_of_offset_seconds = true
                              });
        // @HardcodedJudge, TODO: Define properly if I get a great if offset <
        // JUDGE_GREAT or if offset <= JUDGE_GREAT
        if (fabs(action->tap.offset_seconds) < JUDGE_GREAT_MAX) {
            vn->flags |= VN_TAP_DO_NOT_DRAW;
        }
    } break;

    case INPUT_ACTION_MINE_DIE: {
        // Do nothing
    } break;
    case INPUT_ACTION_MINE_EXPLODE: {
        gs->playing.other.score += SCORE_FOR_MINE_EXPLODE;
    } break;

    case INPUT_ACTION_HOLD_DIE: {
        ASSERT(action->tap.offset_seconds >= JUDGE_MISS_MIN);
        // NOTE: Increase max score twice (head and tail of the hold)
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = JUDGE_MISS_MIN,
                              .miss = true,
                              .delta_max_score_right_now = 2*SCORE_FOR_MARV,
                              .delta_score = 2*SCORE_FOR_MISS
                              });
    } break;
    case INPUT_ACTION_HOLD_GRAB: {
        ASSERT(vn->hold.times_released == 0);
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = action->hold.first_grab.offset_seconds,
                              .miss = false,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score_is_wife_of_offset_seconds = true
                              });
    } break;
    case INPUT_ACTION_HOLD_GRAB_OTHER: {
        ASSERT(vn->hold.times_released > 0);
        // When you break the hold you get -5.5 score. Let's remove some of that penalty if you regrab it.
        gs->playing.other.score += -0.8*SCORE_FOR_MISS;
    } break;
    case INPUT_ACTION_HOLD_GRAB_CONTINUE: {
        // Do nothing
    } break;
    case INPUT_ACTION_HOLD_BREAK_CAN_REGRAB: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = JUDGE_MISS_MIN,
                              .miss = true,
                              .delta_max_score_right_now = 0,
                              .delta_score = SCORE_FOR_MISS
                              });
    } break;
    case INPUT_ACTION_HOLD_BREAK_END_WASREL: {
        // Do nothing
        gs->playing.other.max_score_right_now += SCORE_FOR_MARV;
    } break;
    case INPUT_ACTION_HOLD_BREAK_END_WASPRESS: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = JUDGE_MISS_MIN,
                              .miss = true,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score = SCORE_FOR_MISS
                              });
    } break;
    case INPUT_ACTION_HOLD_BREAK_CONTINUE: {
        // Do nothing
    } break;
    case INPUT_ACTION_HOLD_FINISH: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = action->hold.finish.offset_seconds,
                              .miss = false,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score_is_wife_of_offset_seconds = true
                              });
    } break;

    case INPUT_ACTION_SMROLL_DIE: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = JUDGE_MISS_MIN,
                              .miss = true,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score = SCORE_FOR_MISS
                              });
    } break;
    case INPUT_ACTION_SMROLL_TAP_FIRST: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = action->smroll.offset_seconds,
                              .miss = false,
                              .delta_max_score_right_now = SCORE_FOR_MARV,
                              .delta_score_is_wife_of_offset_seconds = true
                              });
    } break;
    case INPUT_ACTION_SMROLL_TAP_CONTINUE: {
        // Do nothing
    } break;
    case INPUT_ACTION_SMROLL_BREAK: {
        acc_builder_add_thing(gs, idx_column, {
                              .offset_seconds = JUDGE_MISS_MIN,
                              .miss = true,
                              .delta_max_score_right_now = 0,
                              .delta_score = SCORE_FOR_MISS
                              });
    } break;
    case INPUT_ACTION_SMROLL_FINISH: {
        // Do nothing
    } break;
    }
}

static void
do_life_system(GameState *gs, VisibleNote *vn, s32 idx_column, InputActionStuff *action)
{
}


static void
do_post_process_note_for_acc_system_mytestwife3(GameState *gs, VisibleNote *vn, s32 idx_col)
{
}

static void
do_post_process_note_for_life_system_testdonothing(GameState *gs, VisibleNote *vn, s32 idx_col)
{
}

// This function gets called every frame for every note just after input was processed for it.
static void
do_post_process_note(GameState *gs, VisibleNote *vn, s32 idx_col)
{
    // Nothing for now.
    // This might be dependant on the life system and the acc system.
    // I could split this function in do_post_process_note_for_acc_system() and for_life_system
    // When I add more acc systems I might want to create a function pointer for it.
    switch (gs->acc_system.type) {
    case AccSystemType_Mytestwife3: do_post_process_note_for_acc_system_mytestwife3(gs, vn, idx_col); break;
    default: INVALID_CODE_PATH; break;
    }

    switch (gs->life_system.type) {
    case LifeSystemType_Testdonothing: do_post_process_note_for_life_system_testdonothing(gs, vn, idx_col); break;
    default: INVALID_CODE_PATH; break;
    }
}

// maybe i could return if the note got any input or not. probably not useful
static void
maybe_handle_input_for_note(GameState *gs, VisibleNote *vn, s32 idx_column, f64 rate_float)
{
    // For all the notes, see if there's an input that matches this note.
    NewArrayDynamic<TimedInput> *tis = &gs->playing.other.per_column[idx_column].tis;
    for (s32 idx_input = 0;
         idx_input < tis->len;
         idx_input++) {
        TimedInput *ti1 = &tis->data[idx_input];
        // Get the second input if there's any.
        TimedInput *ti2 = NULL;
        if (idx_input+1 < tis->len) {
            ti2 = &tis->data[idx_input+1];
            ASSERT(ti1->pressed != ti2->pressed || ti1->song_ts.value == -NOTE_MAX_TIMESTAMP_F64);
            // ASSERT(ti1->song_ts < ti2->song_ts); // audio code triggered this TODO fix this
            ASSERT(ti1->song_ts <= ti2->song_ts); // audio code didn't trigger this
        }
        InputActionStuff ias = get_input_action(rate_float, gs->playing.other.newthings.song_ts, ti1, ti2, vn, idx_column);
        // @NoteType @HardcodedGamemode
        switch (ias.type) {
        case INPUT_ACTION_NOTHING: {
            // Do nothing
        } break;
        case INPUT_ACTION_MINE_DIE: {
            // it's possible that ti1->pressed if ti1->song_ts > vn->song_ts
            // it's possible that !ti1->pressed
            ASSERT((vn->flags & ~VN_GENERIC_MASK) == 0);
            ASSERT(vn->inputs_used == 0);
            vn->flags |= VN_FINISHED_INPUT;
            vn->flags |= VN_MINE_IS_DEAD;
            // beat_printf("mine die\n");
        } break;
        case INPUT_ACTION_MINE_EXPLODE: {
            ASSERT(ti1->pressed);
            ASSERT((vn->flags & ~VN_GENERIC_MASK) == 0);
            ASSERT(vn->inputs_used == 0);
            // ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_FINISHED_INPUT;
            vn->flags |= VN_MINE_EXPLODED;
            // vn->texture = ...
            // vn->animation = ...
            // beat_printf("mine explode\n");
        } break;
        case INPUT_ACTION_TAP_DIE: {
            // ti1->pressed: possible if ti1->times_used > 0
            // technically possible if ti1->song_ts is much bigger than
            // vn->song_ts (shouldn't happen with decent framerate)
            // not ti1->pressed: possible
            ASSERT((vn->flags & ~VN_GENERIC_MASK) == 0);
            ASSERT(vn->inputs_used == 0);
            // ASSERT(ti1->times_used == 0);
            vn->flags |= VN_FINISHED_INPUT;
            vn->flags |= VN_TAP_IS_DEAD;
            // beat_printf("tap die\n");
        } break;
        case INPUT_ACTION_TAP_TAPS: {
            ASSERT(ti1->pressed);
            ASSERT((vn->flags & ~VN_GENERIC_MASK) == 0);
            ASSERT(vn->inputs_used == 0);
            ASSERT(ti1->times_used == 0);
            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_FINISHED_INPUT;
            vn->flags |= VN_TAP_HIT;
            // texture and animation?
            // beat_printf("tap taps\n");
        } break;
        case INPUT_ACTION_HOLD_DIE: {
            ASSERT(vn->flags & VN_HOLD_IS_RELEASED);
            ASSERT(!(vn->flags & VN_HOLD_IS_PRESSED));
            ASSERT(vn->inputs_used == 0);
            // ASSERT(ti1->times_used == 0);
            vn->flags |= VN_FINISHED_INPUT;
            vn->flags |= VN_HOLD_IS_DEAD;
            // beat_printf("hold die\n");
        } break;
        case INPUT_ACTION_HOLD_GRAB: {
            ASSERT(ti1->pressed);
            ASSERT(vn->flags & VN_HOLD_IS_RELEASED);
            ASSERT(!(vn->flags & VN_HOLD_IS_PRESSED));
            // ASSERT(vn->inputs_used == 0);
            ASSERT(ti1->times_used == 0);
            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_HOLD_IS_PRESSED;
            vn->flags &= ~VN_HOLD_IS_RELEASED;
            // beat_printf("hold grab\n");
        } break;
        case INPUT_ACTION_HOLD_GRAB_OTHER: {
            ASSERT(ti1->pressed);
            ASSERT(vn->flags & VN_HOLD_IS_RELEASED);
            ASSERT(!(vn->flags & VN_HOLD_IS_PRESSED));
            // ASSERT(vn->inputs_used == 0);
            // ASSERT(ti1->times_used == 0);
            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_HOLD_IS_PRESSED;
            vn->flags &= ~VN_HOLD_IS_RELEASED;
            // beat_printf("hold grab\n");
        } break;
        case INPUT_ACTION_HOLD_GRAB_CONTINUE: {
            ASSERT(ti1->pressed);
            ASSERT(vn->flags & VN_HOLD_IS_PRESSED);
            ASSERT(!(vn->flags & VN_HOLD_IS_RELEASED));
            ASSERT(ti1->times_used == 1);
            // if (FRAME_COUNT_ONCE_IN_A_WHILE)
            //     beat_printf("hold grab continue\n");
        } break;
        case INPUT_ACTION_HOLD_BREAK_CAN_REGRAB: {
            ASSERT(!ti1->pressed);
            ASSERT(vn->flags & VN_HOLD_IS_PRESSED);
            ASSERT(ti1->times_used == 0);
            ASSERT(vn->inputs_used >= 1);
            // broke in the middle
            // if (osu holds mode) add finished input; // TODO
            // vn->flags |= VN_FINISHED_INPUT; // @HardcodedGamemode


            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_HOLD_IS_RELEASED;
            vn->flags &= ~VN_HOLD_IS_PRESSED;
            vn->hold.times_released++;
            // beat_printf("hold break can regrab\n");
        } break;
        case INPUT_ACTION_HOLD_BREAK_END_WASREL: {
            ASSERT(!ti1->pressed);
            ASSERT(vn->flags & VN_HOLD_IS_RELEASED);
            ASSERT(ti1->times_used == 1);
            ASSERT(vn->inputs_used >= 1);
            ASSERT(vn->hold.times_released > 0);
            // broke at the end because you didn't regrab it.
            // ASSERT(not osu holds mode);
            vn->flags |= VN_FINISHED_INPUT;


            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_HOLD_IS_RELEASED;
            vn->flags &= ~VN_HOLD_IS_PRESSED;
            vn->hold.times_released++;
            // beat_printf("hold break too late for end was released\n");
        } break;
        case INPUT_ACTION_HOLD_BREAK_END_WASPRESS: {
            ASSERT(ti1->pressed);
            // broke because we didn't release the key in the end
            ASSERT((vn->flags & VN_HOLD_IS_PRESSED));
            ASSERT(!(vn->flags & VN_HOLD_IS_RELEASED));
            ASSERT(ti1->times_used == 1);
            ASSERT(vn->inputs_used >= 1);
            vn->flags |= VN_FINISHED_INPUT;


            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_HOLD_IS_RELEASED;
            vn->flags &= ~VN_HOLD_IS_PRESSED;
            vn->hold.times_released++;
            // beat_printf("hold break too late for end was pressed\n");
        } break;
        case INPUT_ACTION_HOLD_BREAK_CONTINUE: {
            ASSERT(!ti1->pressed);
            ASSERT(vn->flags & VN_HOLD_IS_RELEASED);
            ASSERT(!(vn->flags & VN_HOLD_IS_PRESSED));
            ASSERT(vn->hold.times_released >= 1);
            ASSERT(vn->inputs_used >= 1);
            ASSERT(!ti1->pressed);
            // if (FRAME_COUNT_ONCE_IN_A_WHILE)
            //     beat_printf("hold break continue\n");
        } break;
        case INPUT_ACTION_HOLD_FINISH: {
            // TODO: Might change the behaviour of INPUT_ACTION_HOLD_FINISH to
            // HOLD_BREAK if we don't release after the end. If I do that, uncomment the assert of VN_HOLD_IS_PRESSED
            // @HardcodedGamemode
            ASSERT(vn->flags & VN_HOLD_IS_PRESSED);
            ASSERT(~vn->flags & VN_HOLD_IS_RELEASED);
            ASSERT(ti1->times_used == 0);
            ASSERT(!ti1->pressed);
            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->flags |= VN_HOLD_IS_RELEASED;
            vn->flags &= ~VN_HOLD_IS_PRESSED;
            vn->hold.times_released++;
            vn->flags |= VN_FINISHED_INPUT;
            // beat_printf("hold finish\n");
        } break;
        case INPUT_ACTION_SMROLL_DIE: {
            // @HardcodedGamemode Have to verify exactly what happens to rolls when you don't press them.
            ASSERT(!ti1->pressed || ti1->times_used > 0);
            ASSERT(vn->inputs_used == 0);
            vn->flags |= VN_FINISHED_INPUT;
            vn->flags |= VN_SMROLL_IS_DEAD;
            // beat_printf("roll die\n");
        } break;
        case INPUT_ACTION_SMROLL_TAP_FIRST: {
            ASSERT(ti1->pressed);
            ASSERT(ti1->times_used == 0);
            ASSERT(vn->inputs_used == 0);
            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->smroll.times_tapped++;
            // beat_printf("roll tap first\n");
        } break;
        case INPUT_ACTION_SMROLL_TAP_CONTINUE: {
            ASSERT(ti1->pressed);
            ASSERT(ti1->times_used == 0);
            ti1->times_used++;
            vn->latest_input_song_ts = ti1->song_ts;
            vn->inputs_used++;
            vn->smroll.times_tapped++;
            // beat_printf("roll tap continue\n");
        } break;
        case INPUT_ACTION_SMROLL_BREAK: {
            // @HardcodedGamemode Have to verify exactly what happens to rolls when you don't press them.
            ASSERT(vn->inputs_used > 0);
            // Let's say we finish input for the hold if you break it.
            vn->flags |= VN_FINISHED_INPUT;
            // beat_printf("roll break\n");
        } break;
        case INPUT_ACTION_SMROLL_FINISH: {
            ASSERT(ti1->pressed);
            ASSERT(ti1->times_used == 0);
            vn->flags |= VN_FINISHED_INPUT;
            // beat_printf("roll finish\n");
        } break;
        }

        if (ias.type != INPUT_ACTION_NOTHING) {
            do_acc_system(gs, vn, idx_column, &ias);
            do_life_system(gs, vn, idx_column, &ias);
        }
    }
    do_post_process_note(gs, vn, idx_column);
}

static inline bool
is_note_offscreen(GameState *gs, f32 note_pos)
{
    bool result = note_pos > (f32)gs->rendering.window_height;
    return result;
}

static int
compare_timed_inputs(const void *a, const void *b)
{
    TimedInput *ti_a = (TimedInput *)a;
    TimedInput *ti_b = (TimedInput *)b;
    NoteTimestampDiff diff = ti_a->song_ts - ti_b->song_ts;
    // int result = (int)diff;
    int result = diff.value == 0 ? 0 : (diff.value < 0 ? -1 : 1);
    return result;
}

static void
make_sure_timed_inputs_are_sorted(GameState *gs)
{
    s32 columns = gs->playing.other.columns;
    ASSERT(columns > 0);

    for (s32 idx_col = 0; idx_col < columns; idx_col++) {
        NewArrayDynamic<TimedInput> *tis = &gs->playing.other.per_column[idx_col].tis;
#if 1
        for (s32 idx_ti = 0; idx_ti < tis->len-1; idx_ti++) {
            TimedInput *ti1 = &tis->data[idx_ti];
            TimedInput *ti2 = &tis->data[idx_ti+1];
            if (compare_timed_inputs(ti1, ti2) > 0) {
                INVALID_CODE_PATH;
                return;
            }
        }
#else
        qsort(tis, tis_count, SSIZEOF_VAL(tis),
              compare_timed_inputs);
#endif
    }
}

// Get the input from the platform for only the keys of interest and transform
// This functions does:
// 1. Delete all but the newest input. (maybe the two newest?)
// 2. If there are new inputs (there might be more than one new input per
// key) add them to the list.
// 3. Sort the inputs by timestamp.
// Thought: Is it possible that the platform might give two PRESS events
// without a RELEASE in the middle?
// If that is possible, I have to add a virtual release between these two.
// Well, I think it's better to do that in the platform side of the code
// and not worry about that in here.
static void
register_timed_inputs(GameState *gs, GameInput *gin)
{
    // TODO: Move this to the platform layer, put TimedInput inside GameInput, and make a TimedInput for every key, not just the keys for the columns.

    // When we pause the game and the key was pressed, if we unpause stuff will break.
    ASSERT(gs->scene == SCENE_PLAYING);
    ASSERT(gs->playing.other.state >= PlayingState_Started);
    ASSERT(gs->playing.other.columns > 0);
    f64 rate_float = (f64)gs->playing.params.rate_float;
    for (s32 idx_col = 0; idx_col < gs->playing.other.columns; idx_col++) {
        // Clear them first.
        NewArrayDynamic<TimedInput> *tis = &gs->playing.other.per_column[idx_col].tis;
        if (!gs->playing.other.paused){
            if (tis->len == 0) {
                // If no inputs, create a first input (released)
                TimedInput ti0 = {};
                ti0.pressed = false;
                // ti0.song_ts.value = -NOTE_MAX_TIMESTAMP_F64;
                ti0.song_ts = gs->playing.other.newthings.song_ts;
                tis->push(ti0);
            } else if (tis->len > 1) {
                // If there's more than one input (leftover from the previous frame) save just the last one.
                tis->data[0] = tis->data[tis->len-1];
                tis->len = 1;
            }

            // Add new input if there is any.
            // This is temporary. Ideally we'd have a separate thread getting input
            // at a fast polling rate and here we would copy the new inputs.
            ASSERT(tis->len == 1);
            ButtonInput theinput = column_key_get(gs, gin, gs->playing.other.keymode, idx_col);
            ButtonState state = theinput.state;
            bool dothething = false;
            bool pressed;
            if (state & ButtonState_PRESS) {
                if (tis->data[tis->len-1].pressed) {
                    // Do nothing. It might happen if you unpause and in the
                    // same frame press a key that was pressed before you paused.
                    // Or if you have two bindings to the same column and you
                    // release a key and press another in the same frame.
                    // Or if you release and press again VERY quickly in the same frame.
                    printf("XXX XXX Got key down (ok)\n");
                } else {
                    dothething = true;
                    pressed = true;
                }
            } else if (state & ButtonState_RELEASE) {
                if (!tis->data[tis->len-1].pressed) {
                    // Do nothing. It might happen if you unpause and in the
                    // same frame release a key that was pressed before you paused.
                    printf("XXX XXX Got key up (ok)\n");
                } else {
                    dothething = true;
                    pressed = false;
                }
            }
            if (dothething) {
                TimedInput new_ti = {};
                new_ti.pressed = pressed;
                f64 ref_os_time_seconds = gs->playing.other.reference.os_time_seconds;
                NoteTimestamp ref_ts = gs->playing.other.reference.song_ts;
                new_ti.song_ts = seconds_to_song_ts(theinput.os_time_seconds, rate_float,
                                                    ref_os_time_seconds, ref_ts);
                // ASSERT(new_ti.song_ts > tis->data[tis->len-1].song_ts); // audio code triggered this TODO fix this
                ASSERT(new_ti.song_ts >= tis->data[tis->len-1].song_ts); // TODO fix this

                ASSERT(new_ti.pressed != tis->data[tis->len-1].pressed);
                // new_ti.song_ts = gs->playing.other.newthings.song_ts;
                tis->push(new_ti);
                {
                    TimedInput *ti1 = &tis->data[tis->len-1];
                    NoteTimestamp playing_ts = gs->playing.other.newthings.song_ts;
                    f64 diff_ms = 1000*note_timestamp_diff_to_seconds(ti1->song_ts - playing_ts, rate_float);
                    if (fabs(diff_ms) > 0.001) {
                        printf("ti %s diff: %5.2fms\n", (pressed ? "down" : "up  "), diff_ms);
                        // ASSERT(false);
                    }
                }
            }
        }
    }
    make_sure_timed_inputs_are_sorted(gs);
}

// This will remove invalid entries like a tap that is now offscreen (late)
static void
cleanup_visible_notes(GameState *gs, NoteTimestamp min_song_ts)
{
    // min_song_ts to keep (remove note only if note_ts < min_song_ts)
    for (s32 idx_col = 0; idx_col < gs->playing.other.columns; idx_col++) {
        NewArrayDynamic<VisibleNote> *vns = &gs->playing.other.per_column[idx_col].vns;
        // the thing i do to remove:
        // while vn[i] is to be removed, count++;
        // if not all were removed, move from vn[count..len-1] to vn[0]
        // vns->len -= count;
        // done;
        // example: r=remove, n=no remove
        // 0 1 2 3
        // r r n n
        // ->
        // 0 1
        // n n
        s32 first_to_remove = -1;
        s32 place_to_move_to = -1;
        s32 extras_to_remove = 0;
        s32 count_removed = 0;
        // TODO: Refactor this loop. It can be much simpler since I know vns is sorted.
        for (s32 idx_vn = 0; idx_vn < vns->len; idx_vn++) {
            VisibleNote *vn = &vns->data[idx_vn];
#if 1
            // can probably do this to speed things up
            // will comment it out just to test that the code works.
            if (first_to_remove == -1 && idx_vn > 0) {
                break;
            }
#endif
            // test if the note starts before min_song_ts
            if (vn->song_ts < min_song_ts) {
                // test if it's a hold and don't delete it if the end comes after min_song_ts
                if (vn->type == NOTE_HOLD) {
                    if (~vn->flags & VN_HOLD_VISIBLE_END || vn->hold.song_ts_end >= min_song_ts) {
                        continue;
                    }
                } else if (vn->type == NOTE_SMROLL) {
                    if (~vn->flags & VN_SMROLL_VISIBLE_END || vn->smroll.song_ts_end >= min_song_ts) {
                        continue;
                    }
                }
                // remove vn
                if (first_to_remove == -1) {
                    // mark this element as the first to remove
                    ASSERT(idx_vn == 0); // make sure notes are sorted
                    first_to_remove = idx_vn;
                    extras_to_remove = 0;
                    count_removed++;
                    if (place_to_move_to == -1) {
                        place_to_move_to = idx_vn;
                    }
                } else if (idx_vn == first_to_remove + extras_to_remove + 1) {
                    // last element was marked to removed too
                    extras_to_remove++;
                    count_removed++;
                } else {
                    // this element is marked to remove but the previous(es) is not.
                    // notes aren't sorted.
                    INVALID_CODE_PATH;
                }
            }
        }
        if (first_to_remove != -1) {
            // gotta remove it. the last group
            // ASSERT(place_to_move_to == first_to_remove + extras_to_remove + 1 - (count_removed));
            ASSERT(place_to_move_to == first_to_remove);
            ASSERT(count_removed == extras_to_remove + 1);
            ASSERT(count_removed != 0);
            s32 src = first_to_remove + extras_to_remove + 1;
            if (extras_to_remove+1 < vns->len) {
                // didn't remove all of them
                // move from src..end to place_to_move_to
                xmemmove_struct_count(&vns->data[place_to_move_to],
                                      &vns->data[src],
                                      VisibleNote,
                                      (vns->len - src));
            } else {
                // removed all of them, no need to move memory
                ASSERT(first_to_remove + extras_to_remove + 1 == vns->len);
            }
            vns->len -= count_removed;
        }
    }
}

static void
newsong_game_loop(GameState *gs, GameInput *gin)
{
    ASSERT(gs->playing.other.state >= PlayingState_Started);
    ASSERT(gs->playing.other.columns > 0);
    ASSERT(gs->playing.other.columns < 1000);

    // This should get new timed inputs from the platform, only for the game keys.
    register_timed_inputs(gs, gin); // this already the time from os_time to song_ts



    Diff *diff = gs->playing.params.diff;
    ASSERT(diff->notes.columns == gs->playing.other.columns);
    // f64 to NoteTime clenaup later
    NoteTimestamp ts_to_start_at = gs->playing.other.newthings.song_ts + (NoteTimestampDiff)seconds_to_song_ts(-TIME_TO_LOOK_BEHIND, gs->playing.params.rate_float);
    s32 starting_point = find_row_idx_by_similar_timestamp(&diff->notes, ts_to_start_at, 0, false);

    // Removes invisible notes.
    // cleanup_visible_notes(gs, diff->notes.timestamps[starting_point]);
    cleanup_visible_notes(gs, diff->notes.timestamps[starting_point] + (NoteTimestampDiff)seconds_to_song_ts(-1, gs->playing.params.rate_float));
    // TODO: For now this will do, but in the future I'll want
    // cleanup_visible_notes to already leave things sorted.
#if 0
    sort_visible_notes(gs);
#else
    make_sure_visible_notes_are_sorted(gs);
#endif

    f32 notepos_to_add, notepos_to_multiply;
    get_timestamp_to_visual_distance_multipler(gs->playing.other.visual_config, (f32)gs->rendering.window_height,
                                               gs->playing.params.rate_float,
                                               &notepos_to_multiply, &notepos_to_add);

    // Loop through the notes of the file (not too early and not too late)
    s32 prev_beat = gs->playing.other.newthings.curr_bpm_idx;
    for (s32 idx_row = starting_point;
         idx_row < diff->notes.rows_count;
         idx_row++) {
        NoteTimestamp netimestamp = diff->notes.timestamps[idx_row];
        s32 idx_beat = find_beat_idx_by_timestamp(&diff->notes.timings, netimestamp, prev_beat, NULL, true);
        prev_beat = idx_beat;
        BeatInfo *bi = &diff->notes.timings.bpms[idx_beat];

        // we would be calculating note_pos twice later for rendering... maybe we should put it into VisibleNote?
        // TODO: Get of of these __INTERNAL
        NoteTimestampDiff delta_ts_from_now = netimestamp - gs->playing.other.newthings.song_ts;
        f64 delta_seconds_from_now = note_timestamp_diff_to_seconds(netimestamp - gs->playing.other.newthings.song_ts, gs->playing.params.rate_float);
        f32 note_pos = calc_pixels_from_bottom(notepos_to_add, notepos_to_multiply, delta_ts_from_now);
        bool offscreen = is_note_offscreen(gs, note_pos);
        bool too_ahead_of_time = delta_seconds_from_now > TIME_TO_LOOK_AHEAD;
        if (offscreen && too_ahead_of_time)
            break;

        NoteType *notes_row = diff->notes.raw_notes + idx_row*diff->notes.pitch;


        for (s32 idx_column = 0; idx_column < diff->notes.columns; idx_column++) {
            NewArrayDynamic<VisibleNote> *vns = &gs->playing.other.per_column[idx_column].vns;
            // @NoteType
            NoteType note = NOTES_GET_COLUMN(notes_row, idx_column);
            switch (note) {
            case NOTE_COUNT: INVALID_CODE_PATH; break;
            case NOTE_BLANK: {
                // Do nothing
            } break;
            case NOTE_TAP:  {
                VisibleNote vn = {};
                vn.type = note;
                vn.flags = 0;
                // vn.column = idx_column;
                vn.idx_row = idx_row;
                vn.song_ts = netimestamp;
                s32 idx_vn = find_vn(vns, vn.type, vn.song_ts);
                if (idx_vn < 0) {
                    if (vn.song_ts < gs->playing.other.newthings.song_ts) {
                        vn.flags |= VN_FINISHED_INPUT;
                        vn.flags |= VN_TAP_IS_DEAD;
                    }
                    idx_vn = add_tap(vns, &vn);
                }
            } break;
            case NOTE_MINE: {
                VisibleNote vn = {};
                vn.type = note;
                vn.flags = 0;
                // vn.column = idx_column;
                vn.idx_row = idx_row;
                vn.song_ts = netimestamp;
                s32 idx_vn = find_vn(vns, vn.type, vn.song_ts);
                if (idx_vn < 0) {
                    if (vn.song_ts < gs->playing.other.newthings.song_ts) {
                        vn.flags |= VN_FINISHED_INPUT;
                        vn.flags |= VN_MINE_IS_DEAD;
                    }
                    idx_vn = add_mine(vns, &vn);
                }
            } break;
            case NOTE_HOLD: {
                // TODO: Configuration for holds and lns
                VisibleNote vn = {};
                vn.type = note;
                vn.flags = 0;
                // vn.column = idx_column;
                vn.idx_row = idx_row;
                vn.song_ts = netimestamp;
                vn.flags |= VN_HOLD_IS_RELEASED;
                vn.hold.idx_row_end = -1;
                vn.hold.song_ts_end = NOTE_MAX_TIMESTAMP;

                s32 idx_vn = find_vn(vns, vn.type, vn.song_ts);
                if (idx_vn < 0) {
                    if (vn.song_ts < gs->playing.other.newthings.song_ts) {
                        vn.flags |= VN_FINISHED_INPUT;
                        vn.flags |= VN_HOLD_IS_DEAD;
                    }
                    idx_vn = add_hold_start(diff, idx_column, vns, &vn);
                }
            } break;
            case NOTE_SMROLL: {
                VisibleNote vn = {};
                vn.type = note;
                vn.flags = 0;
                // vn.column = idx_column;
                vn.idx_row = idx_row;
                vn.song_ts = netimestamp;
                vn.smroll.idx_row_end = -1;
                vn.smroll.song_ts_end = NOTE_MAX_TIMESTAMP;

                s32 idx_vn = find_vn(vns, vn.type, vn.song_ts);
                if (idx_vn < 0) {
                    if (vn.song_ts < gs->playing.other.newthings.song_ts) {
                        vn.flags |= VN_FINISHED_INPUT;
                        vn.flags |= VN_SMROLL_IS_DEAD;
                    }
                    idx_vn = add_roll_start(diff, idx_column, vns, &vn);
                }
            } break;
            case NOTE_HOLD_END: {
                // TODO: Configuration for holds and lns
                VisibleNote vn = {};
                vn.type = note;
                vn.flags = 0;
                // vn.column = idx_column;
                vn.idx_row = -1; // shouldn't be used anyway
                vn.hold.idx_row_end = idx_row;
                vn.hold.song_ts_end = netimestamp;

                s32 idx_vn_start = find_hold_with_end(vns, &vn);
                if (idx_vn_start < 0) {
                    // If just started and didn't find the start, search for it and add first.
                    s32 idx_row_start = find_hold_start_idx_with_end_idx(&diff->notes, idx_column, idx_row, NOTE_HOLD);
                    NoteTimestamp netimestamp_start = diff->notes.timestamps[idx_row_start];
                    VisibleNote vn_start_to_add = {};
                    vn_start_to_add.type = NOTE_HOLD;
                    vn_start_to_add.flags = 0;
                    vn_start_to_add.idx_row = idx_row_start;
                    vn_start_to_add.song_ts = netimestamp_start;
                    vn_start_to_add.flags |= VN_HOLD_IS_RELEASED;
                    vn_start_to_add.hold.idx_row_end = -1;
                    vn_start_to_add.hold.song_ts_end = NOTE_MAX_TIMESTAMP;

                    ASSERT(vn_start_to_add.song_ts < gs->playing.other.newthings.song_ts);
                    vn_start_to_add.flags |= VN_FINISHED_INPUT;
                    vn_start_to_add.flags |= VN_HOLD_IS_DEAD;
                    idx_vn_start = add_hold_start(diff, idx_column, vns, &vn_start_to_add);
                }

                ASSERT(idx_vn_start >= 0);
                VisibleNote *vn_start = &vns->data[idx_vn_start];
                ASSERT(vn_start->type == NOTE_HOLD);
                ASSERT(vn_start->hold.idx_row_end >= 0);
                ASSERT(vn_start->hold.idx_row_end == idx_row);
                if (!(vn_start->flags & VN_HOLD_VISIBLE_END)) {
                    vn_start->flags |= VN_HOLD_VISIBLE_END;
                }
            } break;
            case NOTE_SMROLL_END: {
                VisibleNote vn = {};
                vn.type = note;
                vn.flags = 0;
                // vn.column = idx_column;
                vn.idx_row = -1;
                vn.smroll.idx_row_end = idx_row;
                vn.smroll.song_ts_end = netimestamp;

                s32 idx_vn_start = find_roll_with_end(vns, &vn);
                if (idx_vn_start < 0) {
                    // If just started and didn't find the start, search for it and add first.
                    s32 idx_row_start = find_hold_start_idx_with_end_idx(&diff->notes, idx_column, idx_row, NOTE_SMROLL);
                    NoteTimestamp netimestamp_start = diff->notes.timestamps[idx_row_start];
                    VisibleNote vn_start_to_add = {};
                    vn_start_to_add.type = NOTE_SMROLL;
                    vn_start_to_add.flags = 0;
                    vn_start_to_add.idx_row = idx_row_start;
                    vn_start_to_add.song_ts = netimestamp_start;
                    vn_start_to_add.hold.idx_row_end = -1;
                    vn_start_to_add.hold.song_ts_end = NOTE_MAX_TIMESTAMP;

                    ASSERT(vn_start_to_add.song_ts < gs->playing.other.newthings.song_ts);
                    vn_start_to_add.flags |= VN_FINISHED_INPUT;
                    vn_start_to_add.flags |= VN_SMROLL_IS_DEAD;
                    idx_vn_start = add_roll_start(diff, idx_column, vns, &vn_start_to_add);
                }

                ASSERT(idx_vn_start >= 0);
                VisibleNote *vn_start = &vns->data[idx_vn_start];
                ASSERT(vn_start->type == NOTE_SMROLL);
                ASSERT(vn_start->smroll.idx_row_end >= 0);
                ASSERT(vn_start->smroll.idx_row_end == idx_row);
                if (!(vn_start->flags & VN_SMROLL_VISIBLE_END)) {
                    vn_start->flags |= VN_SMROLL_VISIBLE_END;
                }
            } break;
            }
        }
    }
    sort_visible_notes(gs);

    for (s32 idx_column = 0;
         idx_column < diff->notes.columns;
         idx_column++) {
        NewArrayDynamic<VisibleNote> *vns = &gs->playing.other.per_column[idx_column].vns;
        for (s32 i = 0; i < vns->len; i++) {
            VisibleNote *vn = &vns->data[i];
            // if there's input for the note, it'll handle it. if the note
            // didn't get any input it will be a miss
            maybe_handle_input_for_note(gs, vn, idx_column, gs->playing.params.rate_float);
        }
    }
}
