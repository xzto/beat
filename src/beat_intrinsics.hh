#ifndef BEAT_INTRINSICS_H
#define BEAT_INTRINSICS_H


#if BEAT_ARCH_X64
# include <immintrin.h>
# define X86_PAUSE() _mm_pause()

static inline void cpu_relax() {
    X86_PAUSE();
}

#else
# error "ARCH NOT SUPPORTED"
#endif




#if 0
// For __rdtsc
#if BEAT_COMPILER_MSVC
# include <intrin.h>
#else
# include <x86intrin.h>
#endif
#endif


#endif
