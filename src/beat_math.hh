#ifndef BEAT_MATH_H
#define BEAT_MATH_H

struct v2 {
    union {
        struct { f32 x, y; };
        f32 e[2];
    };
};

struct v3 {
    union {
        struct { f32 x, y, z; };
        struct { v2 xy; };
        f32 e[3];
    };
};

struct v4 {
    union {
        struct { f32 x, y, z, w; };
        struct { v3 xyz; };
        struct { f32 r, g, b, a; };
        struct { v3 rgb; };
        f32 e[4];
    };
};


struct v2s {
    union {
        struct { s32 x, y; };
        s32 e[2];
    };
};

struct v3s {
    union {
        struct { s32 x, y, z; };
        struct { v2s xy; };
        s32 e[3];
    };
};

struct v4s {
    union {
        struct { s32 x, y, z, w; };
        struct { v3s xyz; };
        struct { s32 r, g, b, a; };
        struct { v3s rgb; };
        s32 e[4];
    };
};

struct v2u {
    union {
        struct { u32 x, y; };
        u32 e[2];
    };
};

struct v3u {
    union {
        struct { u32 x, y, z; };
        struct { v2u xy; };
        u32 e[3];
    };
};

struct v4u {
    union {
        struct { u32 x, y, z, w; };
        struct { v3u xyz; };
        struct { u32 r, g, b, a; };
        struct { v3u rgb; };
        u32 e[4];
    };
};

#define V2(x, y)        (v2){.e={x, y}}
#define V3(x, y, z)     (v3){.e={x, y, z}}
#define V4(x, y, z, w)  (v4){.e={x, y, z, w}}

#define V2S(x, y)        (v2s){.e={x, y}}
#define V3S(x, y, z)     (v3s){.e={x, y, z}}
#define V4S(x, y, z, w)  (v4s){.e={x, y, z, w}}

#define V2U(x, y)        (v2u){.e={x, y}}
#define V3U(x, y, z)     (v3u){.e={x, y, z}}
#define V4U(x, y, z, w)  (v4u){.e={x, y, z, w}}


struct QuadPos {
    v2 pos[4];
};
struct AabbPos {
    v2 min;
    v2 max;
};

#endif
