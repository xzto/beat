#include "../beat_include.hh"

// #include "../beat_threads.cc"

#define wave_report_error(...) printf("wave error: " __VA_ARGS__)
#define wave_report_info(...)  printf("wave info:  " __VA_ARGS__)

#pragma pack(push, 1)
// All integers are little endian for "RIFF"
struct RIFF_Header {
    u32 id; // "RIFF"
    u32 remaining_riff_size; // size after this
    u32 format_id; // "WAVE"
};
struct RIFF_JunkChunkHeader {
    u32 id; // "JUNK"
    u32 remaining_chunk_size;
    // chunk_size bytes
    // one more byte if chunk_size is odd
};

// TODO: Make struct for 0x12 and 0x20
struct RIFF_FormatChunkSmaller {
    u32 id; // "fmt_"
    u32 remaining_chunk_size; // 0x10, 0x12, 0x20
    u16 audio_format; // always 0x01
    u16 num_channels; // 1=mono, 2=stereo, etc
    u32 sample_rate; // in Hz like 48000
    u32 bytes_per_second;
    u16 bytes_per_sample; // 1=8bit mono, 2=8bit stereo or 16bit mono, 4=16bit stereo
    u16 bits_per_sample;
};
struct RIFF_ListChunkHeader {
    u32 id; // "LIST"
    u32 remaining_chunk_size;
    // data
};
struct RIFF_DataChunkHeader {
    u32 id; // "data"
    u32 remaining_chunk_size;
    // data
};
struct _TestPragmaPack1 {
    u32 a;
    u16 b;
};
STATIC_ASSERT(sizeof(_TestPragmaPack1) == 6);
#pragma pack(pop)
struct _TestPragmaPack2 {
    u32 a;
    u16 b;
};
STATIC_ASSERT(sizeof(_TestPragmaPack2) == 8);

#define RIFF_ID(a, b, c, d) (((u32)(a) << 0) | ((u32)(b) << 8) | ((u32)(c) << 16) | ((u32)(d) << 24))
#define RIFF_ID_STR(s) RIFF_ID((s)[0], (s)[1], (s)[2], (s)[3])

static u32
read_riff_id(u8 *buf)
{
    return RIFF_ID(buf[0], buf[1], buf[2], buf[3]);
}



static bool
read_riff_header(Buffer *buf, RIFF_Header **result)
{
    if (buf->len < 4 || read_riff_id(buf->data) != RIFF_ID_STR("RIFF")) {
        wave_report_error("File is not RIFF\n");
        return false;
    }
    if (buf->len < ssizeof(RIFF_Header)) {
        wave_report_error("File is too small to fit RIFF_Header\n");
        return false;
    }
    RIFF_Header *riff_header = (RIFF_Header*)buf->data;
    if (buf->len-8 > riff_header->remaining_riff_size) {
        buf->len = riff_header->remaining_riff_size + 8;
    } else if (buf->len-8 < riff_header->remaining_riff_size) {
        wave_report_error("buf->len-8 < riff_header->remaining_size\n");
        return false;
    }
    if (riff_header->format_id != RIFF_ID_STR("WAVE")) {
        wave_report_error("'WAVE' id does not match\n");
        return false;
    }
    advance(buf, ssizeof(RIFF_Header));
    *result = riff_header;
    return true;
}

static s32
riff_ignore_junk__internal(Buffer *buf, int __just_for_typecheck)
{
    (void)__just_for_typecheck; // if i accidentally call __internal, the compiler will complain about a missing argument.
    if (buf->len < 4) {
        return 2;
    }
    if (read_riff_id(buf->data) == RIFF_ID_STR("JUNK")) {
        if (buf->len < ssizeof(RIFF_JunkChunkHeader)) {
            wave_report_error("File ended abruptly\n");
            return 0;
        }
        RIFF_JunkChunkHeader *junk_header = (RIFF_JunkChunkHeader*)buf->data;
        advance(buf, ssizeof(RIFF_JunkChunkHeader));
        s32 to_skip = (s32)junk_header->remaining_chunk_size;
        to_skip += (to_skip & 1);
        if (to_skip >= 0 && buf->len >= to_skip) {
            advance(buf, to_skip);
            return 1;
        } else {
            wave_report_error("junk_header->chunk_size is invalid\n");
            return 0;
        }
    } else {
        return 2;
    }
}

static bool
riff_ignore_junk(Buffer *buf)
{
    s32 ignore_junk_result;
    while((ignore_junk_result = riff_ignore_junk__internal(buf, 0)) == 1);
    if (ignore_junk_result == 0) {
        wave_report_error("riff_ignore_junk failed\n");
        return false;
    }
    return true;
}

struct MyWaveFormatResult {
    s32 num_channels;
    s32 samples_per_second;
    s32 bytes_per_second;
    s32 bytes_per_sample;
    s32 bytes_per_sample_per_channel;
};

static bool
read_riff_fmt(Buffer *buf, MyWaveFormatResult *result)
{
    *result = {};

    if (!riff_ignore_junk(buf)) {
        return false;
    }

    if (buf->len < 4) {
        wave_report_error("File ended abruptly\n");
        return false;
    }
    if (read_riff_id(buf->data) != RIFF_ID_STR("fmt ")) {
        wave_report_error("'fmt ' id does not match\n");
        return false;
    }
    if (buf->len < ssizeof(RIFF_FormatChunkSmaller)) {
        wave_report_error("File is too small to fit RIFF_FormatChunkSmaller\n");
        return false;
    }

    RIFF_FormatChunkSmaller *format_chunk = (RIFF_FormatChunkSmaller*)buf->data;
    if (buf->len < format_chunk->remaining_chunk_size + 8) {
        wave_report_error("format_chunk->remaining_chunk_size is too big for the buffer\n");
        return false;
    }
    if (!(format_chunk->remaining_chunk_size == 0x10 ||
          format_chunk->remaining_chunk_size == 0x12 ||
          format_chunk->remaining_chunk_size == 0x28)) {
        wave_report_error("format_chunk->chunk_size != 0x10\n");
        return false;
    }
    if (format_chunk->audio_format != 0x01) {
        // 0x01 is pcm
        // TODO: 0x03 is float32 or float64
        wave_report_error("format_chunk->audio_format != 0x01\n");
        return false;
    }
    if (!(format_chunk->num_channels == 1 || format_chunk->num_channels == 2)) {
        wave_report_error("format_chunk->num_channels is not 1 or 2\n");
        return false;
    }
    result->num_channels = format_chunk->num_channels;
    result->samples_per_second = (s32)format_chunk->sample_rate;
    if (result->samples_per_second <= 0) {
        wave_report_error("bad sample rate: %d\n", result->samples_per_second);
        return false;
    }
    result->bytes_per_sample = format_chunk->bytes_per_sample;
    if (result->bytes_per_sample <= 0) {
        wave_report_error("bad bytes_per_sample: %d with %d channels\n", result->bytes_per_sample, result->num_channels);
        return false;
    }
    result->bytes_per_sample_per_channel = result->bytes_per_sample / result->num_channels;
    result->bytes_per_second = result->bytes_per_sample * result->samples_per_second;
    if (result->num_channels == 1) {
        if (result->bytes_per_sample == 1) {
            // mono 8bit TODO
            wave_report_error("bad bytes_per_sample: %d with %d channels\n", result->bytes_per_sample, result->num_channels);
            return false;
        } else if (result->bytes_per_sample == 2) {
            // mono 16bit
            // ok
        } else {
            wave_report_error("bad bytes_per_sample: %d with %d channels\n", result->bytes_per_sample, result->num_channels);
            return false;
        }
    } else if (result->num_channels == 2) {
        if (result->bytes_per_sample == 2) {
            // stereo 8bit TODO
            wave_report_error("bad bytes_per_sample: %d with %d channels\n", result->bytes_per_sample, result->num_channels);
            return false;
        } else if (result->bytes_per_sample == 4) {
            // stereo 16bit
            // ok
        } else {
            wave_report_error("bad bytes_per_sample: %d with %d channels\n", result->bytes_per_sample, result->num_channels);
            return false;
        }
    } else {
        INVALID_CODE_PATH;
    }

    advance(buf, format_chunk->remaining_chunk_size + 8);
    return true;
}

static bool
read_riff_maybe_list_remove_me_later(Buffer *buf)
{
    if (!riff_ignore_junk(buf)) {
        return false;
    }

    if (buf->len < 4) {
        // wave_report_error("File ended abruptly\n");
        // return false;
        return true;
    }
    if (read_riff_id(buf->data) != RIFF_ID_STR("LIST")) {
        return true;
        // wave_report_error("'LIST' id does not match\n");
        // return false;
    }
    if (buf->len < ssizeof(RIFF_ListChunkHeader)) {
        wave_report_error("File is too small to fit RIFF_ListChunk\n");
        return false;
    }
    RIFF_ListChunkHeader *list_header = (RIFF_ListChunkHeader*)buf->data;
    advance(buf, ssizeof(RIFF_ListChunkHeader));
    if (buf->len < list_header->remaining_chunk_size) {
        wave_report_error("list_header->remaining_chunk_size is too big for the buffer\n");
        return false;
    }
    advance(buf, list_header->remaining_chunk_size);
    return true;
}

static bool
read_wave_data_header(Buffer *buf)
{
    // This will truncate buf to the data size!
    if (!riff_ignore_junk(buf)) {
        return false;
    }

    if (buf->len < 4) {
        wave_report_error("File ended abruptly\n");
        return false;
    }
    if (read_riff_id(buf->data) != RIFF_ID_STR("data")) {
        wave_report_error("'data' id does not match\n");
        DEBUG_BREAK;
        return false;
    }
    if (buf->len < ssizeof(RIFF_DataChunkHeader)) {
        wave_report_error("File is too small to fit RIFF_DataChunkHeader\n");
        return false;
    }
    RIFF_DataChunkHeader *data_header = (RIFF_DataChunkHeader*)buf->data;
    advance(buf, ssizeof(RIFF_DataChunkHeader));
    // now test for data_header->remaining_chunk_size
    if (buf->len > data_header->remaining_chunk_size) {
        // there is more data and we will ignore it.
        wave_report_info("there is more data in the file but we'll ignore it. (%ld)\n",
                         buf->len - data_header->remaining_chunk_size);
        buf->len = data_header->remaining_chunk_size;
    } else if (buf->len < data_header->remaining_chunk_size) {
        wave_report_error("file is not big enough for the data\n");
        return false;
    }

    return true;
}

struct MyWaveAudio {
    s8 num_channels;
    // s8 bytes_per_sample;
    // s8 bytes_per_sample_per_channel; // always == ssizeof(s16)
    s32 samples_per_second; // example 48000
    // s32 bytes_per_second; // always == samples_per_second * bytes_per_sample

    // s32 total_bytes_count; // always == sample_count * bytes_per_sample
    // s32 _padding;
    s32 sample_count; // s32 will fit 12 hours (8.5GiB) of sample_count for 48kHz (no need to worry about this)
    s16 *samples;
    // f64 duration_seconds; // always == (f64)sample_count / (f64)samples_per_second
};

static bool
load_entire_wav_from_filename(String filename, MyWaveAudio *ret_wave_audio)
{
    EntireFile ef;
    if (!read_entire_file(filename, &ef)) {
        return false;
    }
    defer { xfree(ef.buf.data); };
    Buffer buf = ef.buf;

    RIFF_Header *riff_header;
    if (!read_riff_header(&buf, &riff_header)) {
        wave_report_error("File is not RIFF\n");
        return false;
    }

    MyWaveFormatResult wave_format;
    if (!read_riff_fmt(&buf, &wave_format)) {
        wave_report_error("Failed to read riff fmt\n");
        return false;
    }

    if (!read_riff_maybe_list_remove_me_later(&buf)) {
        wave_report_error("Failed to read riff LIST\n");
        return false;
    }

    if (!read_wave_data_header(&buf)) {
        wave_report_error("Failed to read wave data header\n");
        return false;
    }

    // NOTE: This code reads the entire wav file into memory and uses pointers
    // to that memory. It doesn't check for proper alignment. On x64 it is
    // probably ok.

    ASSERT(wave_format.num_channels == 1 || wave_format.num_channels == 2);

    s32 bytes_to_read = ((s32)buf.len / wave_format.bytes_per_sample) * wave_format.bytes_per_sample;
    if (bytes_to_read <= 0) {
        // thing is empty TODO what do i do
        wave_report_error("thing is empty\n");
        return false;
    }
    // TODO: Fail if size is too big
    if (bytes_to_read > GIGABYTES(1)) {
        wave_report_error("thing is too big for this program\n");
        return false;
    }
    s32 total_sample_count = bytes_to_read / wave_format.bytes_per_sample;
    s32 total_bytes_per_channel = total_sample_count * wave_format.bytes_per_sample_per_channel;
    f64 duration_seconds = (f64)total_sample_count / (f64)wave_format.samples_per_second;
    smem align_to = 16;
    s16 *dest_samples = (s16*)xmalloc(bytes_to_read);

    if (wave_format.num_channels == 1) {
        // just a memcpy
        if (wave_format.bytes_per_sample_per_channel == 1) {
            NOT_IMPLEMENTED;
        } else if (wave_format.bytes_per_sample_per_channel == 2) {
            xmemcpy(dest_samples, buf.data, bytes_to_read);
        } else {
            INVALID_CODE_PATH;
        }
    } else {
        // deinterleave
        if (wave_format.bytes_per_sample_per_channel == 1) {
            NOT_IMPLEMENTED;
        } else if (wave_format.bytes_per_sample_per_channel == 2) {
            xmemcpy(dest_samples, buf.data, bytes_to_read);
        } else {
            INVALID_CODE_PATH;
        }
    }

    MyWaveAudio wave_audio = {};
    wave_audio.num_channels = (s8)wave_format.num_channels;
    wave_audio.samples_per_second = wave_format.samples_per_second;
    // wave_audio.bytes_per_second = wave_format.bytes_per_second;
    // wave_audio.bytes_per_sample = (s8)wave_format.bytes_per_sample;
    // wave_audio.bytes_per_sample_per_channel = wave_format.bytes_per_sample_per_channel; // always 2

    // wave_audio.total_bytes_count = bytes_to_read;
    wave_audio.sample_count = total_sample_count;
    // wave_audio.duration_seconds = duration_seconds;
    wave_audio.samples = dest_samples;

    *ret_wave_audio = wave_audio;

    // DEBUG_BREAK;


    return true;
}


#if 0
int
main(int argc, char **argv)
{
    // String filename = "1875.wav"_s;
    String filename = "service-bell_daniel_simion.wav"_s;
    bool success = load_entire_wav_from_filename(filename);
    if (success) {
        printf("Loaded wav file '%.*s' succesfully!\n", EXPAND_STR_FOR_PRINTF(filename));
    } else {
        printf("Failed to load wave file '%.*s'\n", EXPAND_STR_FOR_PRINTF(filename));
    }
}
#endif
