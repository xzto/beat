
static constexpr KeyBinding
make_keybinding(BeatKey key, KeyboardMods mods = KeyboardMods_None)
{
    return { .key=key, .mods=mods };
}

// this function recognizes LSHIFT and other mods as a key press. If you press
// CTRL-O this function will return BEAT_KEY_LCTRL on the frame that ctrl was
// pressed and BEAT_KEY_O on the other frame when O is pressed.
static BeatKey
check_pressed_beat_key(GameInput *gin)
{
    for (BeatKey key = 0; key < ARRAY_COUNT(gin->raw_keys); key++) {
        ButtonInput *bi = &gin->raw_keys[key];
        if (bi->state & ButtonState_PRESS) {
            return key;
        }
    }
    // TODO Check controllers too
    return BEAT_KEY_INVALID;
}

// this function does not recognize LSHIFT and other mods as a key press, so it is possible to return something like CTRL-O. If you press CTRL-O this function will return nothing on the frame ctrl was pressed and on the frame that O weas pressed it will return {key=BEAT_KEY_O, mods=KeyboardMods::Ctrl}.
static KeyBinding
check_pressed_key_binding_with_mods(GameInput *gin)
{
    // loop on all keys, if pressed return it.
    for (BeatKey key = 0; key < ARRAY_COUNT(gin->raw_keys); key++) {
        if (BETWEEN(key, BEAT_KEY__MODS_START, BEAT_KEY__MODS_END)) continue;
        ButtonInput *bi = &gin->raw_keys[key];
        if (bi->state & ButtonState_PRESS) {
            return make_keybinding(key, bi->mods_for_press_or_release);
        }
    }
    // TODO Check controllers too
    return {};
}


static String
beat_key_to_string(BeatKey key)
{
    if (ABC(key, ARRAY_COUNT(beat_key_to_string_table))) {
        return beat_key_to_string_table[key];
    }
    // Not implemented yet, but it should not allocate any memory.
    NOT_IMPLEMENTED;
    return "(not implemented)"_s;
}

static String
keyboard_mods_to_string(KeyboardMods mods)
{
    bool any = (mods & KeyboardMods_Any) != KeyboardMods_None;
    bool s = (mods & KeyboardMods_Shift) != KeyboardMods_None;
    bool c = (mods & KeyboardMods_Ctrl) != KeyboardMods_None;
    bool a = (mods & KeyboardMods_Alt) != KeyboardMods_None;
    bool w = (mods & KeyboardMods_Win) != KeyboardMods_None;
    s32 tonum = ((s32)w<<0) | ((s32)a<<1) | ((s32)c<<2)| ((s32)s<<3) | ((s32)any<<4);
    switch (tonum) {
    case 0*16 + 0*8 + 0*4 + 0*2 + 0*1: return ""_s;
    case 0*16 + 0*8 + 0*4 + 0*2 + 1*1: return "WIN-"_s;
    case 0*16 + 0*8 + 0*4 + 1*2 + 0*1: return "ALT-"_s;
    case 0*16 + 0*8 + 0*4 + 1*2 + 1*1: return "WIN-ALT-"_s;
    case 0*16 + 0*8 + 1*4 + 0*2 + 0*1: return "CTRL-"_s;
    case 0*16 + 0*8 + 1*4 + 0*2 + 1*1: return "WIN-CTRL-"_s;
    case 0*16 + 0*8 + 1*4 + 1*2 + 0*1: return "ALT-CTRL-"_s;
    case 0*16 + 0*8 + 1*4 + 1*2 + 1*1: return "WIN-ALT-CTRL-"_s;

    case 0*16 + 1*8 + 0*4 + 0*2 + 0*1: return "SHIFT-"_s;
    case 0*16 + 1*8 + 0*4 + 0*2 + 1*1: return "WIN-SHIFT-"_s;
    case 0*16 + 1*8 + 0*4 + 1*2 + 0*1: return "ALT-SHIFT-"_s;
    case 0*16 + 1*8 + 0*4 + 1*2 + 1*1: return "WIN-ALT-SHIFT-"_s;
    case 0*16 + 1*8 + 1*4 + 0*2 + 0*1: return "CTRL-SHIFT-"_s;
    case 0*16 + 1*8 + 1*4 + 0*2 + 1*1: return "WIN-CTRL-SHIFT-"_s;
    case 0*16 + 1*8 + 1*4 + 1*2 + 0*1: return "ALT-CTRL-SHIFT-"_s;
    case 0*16 + 1*8 + 1*4 + 1*2 + 1*1: return "WIN-ALT-CTRL-SHIFT-"_s;

    case 1*16 + 0*8 + 0*4 + 0*2 + 0*1: return "ANY-"_s;
    case 1*16 + 0*8 + 0*4 + 0*2 + 1*1: return "ANY-WIN-"_s;
    case 1*16 + 0*8 + 0*4 + 1*2 + 0*1: return "ANY-ALT-"_s;
    case 1*16 + 0*8 + 0*4 + 1*2 + 1*1: return "ANY-WIN-ALT-"_s;
    case 1*16 + 0*8 + 1*4 + 0*2 + 0*1: return "ANY-CTRL-"_s;
    case 1*16 + 0*8 + 1*4 + 0*2 + 1*1: return "ANY-WIN-CTRL-"_s;
    case 1*16 + 0*8 + 1*4 + 1*2 + 0*1: return "ANY-ALT-CTRL-"_s;
    case 1*16 + 0*8 + 1*4 + 1*2 + 1*1: return "ANY-WIN-ALT-CTRL-"_s;

    case 1*16 + 1*8 + 0*4 + 0*2 + 0*1: return "ANY-SHIFT-"_s;
    case 1*16 + 1*8 + 0*4 + 0*2 + 1*1: return "ANY-WIN-SHIFT-"_s;
    case 1*16 + 1*8 + 0*4 + 1*2 + 0*1: return "ANY-ALT-SHIFT-"_s;
    case 1*16 + 1*8 + 0*4 + 1*2 + 1*1: return "ANY-WIN-ALT-SHIFT-"_s;
    case 1*16 + 1*8 + 1*4 + 0*2 + 0*1: return "ANY-CTRL-SHIFT-"_s;
    case 1*16 + 1*8 + 1*4 + 0*2 + 1*1: return "ANY-WIN-CTRL-SHIFT-"_s;
    case 1*16 + 1*8 + 1*4 + 1*2 + 0*1: return "ANY-ALT-CTRL-SHIFT-"_s;
    case 1*16 + 1*8 + 1*4 + 1*2 + 1*1: return "ANY-WIN-ALT-CTRL-SHIFT-"_s;
    default: INVALID_CODE_PATH; break;
    }

    INVALID_CODE_PATH;
    return {};
}

static bool
keyboard_mods_match(KeyboardMods a, KeyboardMods b)
{
    KeyboardMods const a_required = (a & KeyboardMods_Required);
    KeyboardMods const b_required = (b & KeyboardMods_Required);
    KeyboardMods const a_virtual  = (a & KeyboardMods_Any) != KeyboardMods_None ? KeyboardMods_Required : a_required;
    KeyboardMods const b_virtual  = (b & KeyboardMods_Any) != KeyboardMods_None ? KeyboardMods_Required : b_required;
    // ANY-CTRL- == ANY-ALT-CTRL-   -> true
    // ANY-ALT-CTRL- == ANY-CTRL-   -> true
    // ALT-CTRL == CTRL             -> false
    // ALT-CTRL == ANY              -> true
    if ((a & KeyboardMods_Any) != KeyboardMods_None) {
        bool b_has_a_required = (b_virtual & a_required) == a_required;
        return b_has_a_required;
    } else if ((b & KeyboardMods_Any) != KeyboardMods_None) {
        bool a_has_b_required = (a_virtual & b_required) == b_required;
        return a_has_b_required;
    } else {
        return a_required == b_required;
    }
}



#if 0
static String
keybinding_to_string(MemoryArena *arena, KeyBinding keybinding)
{
    // This function allocates memory on the arena.
    // Maybe I shouldn't use this function when printing stuff.
    // I can instead write ("%.*s%.*s", keyboard_mods_to_string(kb.mods), beat_key_to_string(kb.key))
    // If I find some use to this function I'll uncomment it.
    if (keybinding.mods == KeyboardMods::None) {
        return arena_push_string(arena, beat_key_to_string(keybinding.key));
    }
    String str_mods = keyboard_mods_to_string(keybinding.mods);
    String str_key = beat_key_to_string(keybinding.key);
    String final_str = {};
    final_str.data = (u8 *)arena_push_size(arena, str_mods.len + str_key.len, arena_flags_nozero());
    memcpy(final_str.data + final_str.len, str_mods.data, str_mods.len);
    final_str.len += str_mods.len;
    memcpy(final_str.data + final_str.len, str_key.data, str_key.len);
    final_str.len += str_key.len;
    return final_str;
}
#endif



static BeatKey
string_to_beat_key(String str)
{
    // TODO: Might be useful to allow the string to say the number instead of
    // the string representing that number. "!65" instead of "A".
    // TODO: "!number" and "!controller_id=hex_number,button=base10_number"
    if (str.len > 0) {
        if (str.len == 1) {
            String asdf = beat_key_to_string(str.data[0]);
            if (asdf.len > 0)
                return (BeatKey)str.data[0];
        }
        for (BeatKey key = 0; key < ARRAY_COUNT(beat_key_to_string_table); key++) {
            String to_compare = beat_key_to_string_table[key];
            if (str == to_compare) {
                return key;
            }
        }
    }
    printf("Invalid string for string_to_beat_key: '%.*s'\n", EXPAND_STR_FOR_PRINTF(str));
    return 0;
}

// TODO: When I implement hot reloading of the configuration file I might want to pass the dynamic array as a pointer argument instead of creating a new one. that way I can reuse some memory.
// or not.
// i can just create an arena for the configuration stuff and when i reload the file, I clear the arena.
// ok
static void
string_to_multiple_beat_keys(NewArrayDynamic<BeatKey> *arr, String str, s32 max_bindings = -1)
{
    String remaining = str;
    while (true) {
        if (max_bindings >= 0 && arr->len >= max_bindings) break;
        String binding_str;
        split_by_char_and_trim_whitespace(remaining, ' ', &binding_str, &remaining);
        if (binding_str.len <= 0) break;
        BeatKey key = string_to_beat_key(binding_str);
        if (key)
            arr->push(key);
    }
}


static KeyboardMods
eat_keyboard_mods(String *str)
{
    KeyboardMods result = {};
    String thestr = *str;
    while (true) {
        String lhs, rhs;
        if (!split_by_char(thestr, '-', &lhs, &rhs))
            break;
        if (lhs.len <= 0)
            break;

        if (0) {}
        else if (lhs == "ANY"_s)    result |= KeyboardMods_Any;
        else if (lhs == "WIN"_s)    result |= KeyboardMods_Win;
        else if (lhs == "ALT"_s)    result |= KeyboardMods_Alt;
        else if (lhs == "CTRL"_s)   result |= KeyboardMods_Ctrl;
        else if (lhs == "SHIFT"_s)  result |= KeyboardMods_Shift;
        else break;
        thestr = rhs;
    }
    *str = thestr;
    return result;
}

static KeyBinding
string_to_key_binding(String str)
{
    KeyBinding result = {};
    result.mods = eat_keyboard_mods(&str);
    result.key = string_to_beat_key(str);
    // When I press LCTRL, the CTRL mod is always enabled.
    // If I don't do this, when I call key_binding_pressed() for
    // "LCTLR" it won't ever say it is pressed because the mods aren't right (wanted None, got Ctrl).
    // I won't have to say "CTRL-LCTRL".
    switch (result.key) {
    case BEAT_KEY_LSHIFT: case BEAT_KEY_RSHIFT: result.mods |= KeyboardMods_Shift; break;
    case BEAT_KEY_LCTRL:  case BEAT_KEY_RCTRL:  result.mods |= KeyboardMods_Ctrl; break;
    case BEAT_KEY_LALT:   case BEAT_KEY_RALT:   result.mods |= KeyboardMods_Alt; break;
    case BEAT_KEY_LWIN:   case BEAT_KEY_RWIN:   result.mods |= KeyboardMods_Win; break;
    default: break;
    }
    return result;
}

static void
string_to_multiple_key_bindings(NewArrayDynamic<KeyBinding> *arr, String str, s32 max_bindings = -1)
{
    String remaining = str;
    while (true) {
        if (max_bindings >= 0 && arr->len >= max_bindings) break;
        String binding_str;
        split_by_char_and_trim_whitespace(remaining, ' ', &binding_str, &remaining);
        if (binding_str.len <= 0) break;
        KeyBinding binding = string_to_key_binding(binding_str);
        if (binding.key)
            arr->push(binding);
    }
}




static inline ButtonInput
simple_key_get(GameInput *gin, BeatKey key)
{
    if (!key) return {};

    if (key < ARRAY_COUNT(gin->raw_keys)) {
        return gin->raw_keys[key];
    } else {
        ASSERT(!"TODO: Support controllers.");
        return {};
    }
}

static inline bool
simple_key_down(GameInput *gin, BeatKey key)
{
    return (simple_key_get(gin, key).state & ButtonState_DOWN) != 0;
}

static inline bool
simple_key_pressed(GameInput *gin, BeatKey key)
{
    return (simple_key_get(gin, key).state & ButtonState_PRESS) != 0;
}

static inline bool
simple_key_pressed_or_repeat(GameInput *gin, BeatKey key)
{
    return (simple_key_get(gin, key).state & (ButtonState_PRESS | ButtonState_REPEAT)) != 0;
}

// @KeyBindingArrayDoublePress
// TODO: This returns a PRESS if you have two bindings and release one key and press the other in the same frame.
// Example:
// PRESS    a
// UP       b
// -> PRESS
// DOWN     a
// PRESS    b
// -> DOWN
// RELEASE  a
// PRESS    b
// -> PRESS
// UP       a
// RELEASE  b
// -> RELEASE
// The correct behaviour should be
//
// PRESS    a
// UP       b
// -> PRESS
// DOWN     a
// PRESS    b
// -> DOWN
// RELEASE  a
// PRESS    b
// -> DOWN
// UP       a
// RELEASE  b
// -> RELEASE
//
// Getting PRESS, DOWN, PRESS, RELEASE is wrong!
// I should get PRESS, DOWN, DOWN, RELEASE!
// How to fix it: Compute effective_state for gin->last_raw_keys too. If this frame I got a PRESS and the last frame I had it DOWN, return a DOWN.
static inline ButtonState
simple_key_get_state(GameInput *gin, Slice<BeatKey> keys)
{
    if (keys.len <= 0) return {};
    bool pressed = false;
    bool repeat = false;
    bool release = false;

    s32 count_pressed = 0;
    s32 count_repeat = 0;
    s32 count_down = 0;
    ButtonState effective_state = {};
    for (s32 i = 0; i < keys.len; i++) {
        auto k = keys.data[i];
        auto bi = simple_key_get(gin, k);
        auto s = bi.state;
        if (s & ButtonState_DOWN)
            count_down++;
        if (s & ButtonState_PRESS) {
            count_pressed++;
            pressed = true;
        }
        if (s & ButtonState_REPEAT) {
            count_repeat++;
            repeat = true;
        }
        if (s & ButtonState_RELEASE)
            release = true;
    }

    if (count_down > 0) {
        effective_state |= ButtonState_DOWN;
        if (count_down == count_pressed && pressed) {
            effective_state |= ButtonState_PRESS;
        } else if (repeat) {
            effective_state |= ButtonState_REPEAT;
        }
    } else if (release) {
        effective_state |= ButtonState_RELEASE;
    } else {
        effective_state = {};
    }

    return effective_state;
}



// @KeyBindingArrayDoublePress
// TODO: Make tests to make sure simple_key_get(gin, keys).state == simple_key_get_state(gin, keys)
static inline ButtonInput
simple_key_get(GameInput *gin, Slice<BeatKey> keys)
{
    if (keys.len <= 0) return {};
    // TODO: Have to test this function..
    s32 count_down = 0;
    s32 count_pressed = 0;
    s32 count_repeat = 0;
    bool pressed = false;
    bool repeat = false;
    bool release = false;

    f64 oldest_down_time_seconds = F64_MAX;
    f64 oldest_pressed_time_seconds = F64_MAX;
    f64 oldest_repeat_time_seconds = F64_MAX;
    f64 oldest_release_time_seconds = F64_MAX;
    f64 newest_up_time_seconds = -F64_MAX;

    s32 theidx_pressed = -1;
    s32 theidx_release = -1;
    s32 theidx_repeat = -1;
    s32 theidx_down = -1;
    s32 theidx_up = -1;
    for (s32 i = 0; i < keys.len; i++) {
        auto bi = simple_key_get(gin, keys.data[i]);
        auto s = bi.state;
        if (s & ButtonState_DOWN) {
            count_down++;
            if (oldest_down_time_seconds > bi.os_time_seconds) {
                oldest_down_time_seconds = bi.os_time_seconds;
                theidx_down = i;
            }
            if (s & ButtonState_PRESS) {
                count_pressed++;
                pressed = true;
                if (oldest_pressed_time_seconds > bi.os_time_seconds) {
                    oldest_pressed_time_seconds = bi.os_time_seconds;
                    theidx_pressed = i;
                }
            }
            if (s & ButtonState_REPEAT) {
                count_repeat++;
                repeat = true;
                if (oldest_repeat_time_seconds > bi.os_time_seconds) {
                    oldest_repeat_time_seconds = bi.os_time_seconds;
                    theidx_repeat = i;
                }
            }
        } else {
            if (s & ButtonState_RELEASE) {
                release = true;
                if (oldest_release_time_seconds > bi.os_time_seconds) {
                    oldest_release_time_seconds = bi.os_time_seconds;
                    theidx_release = i;
                }
            }

            if (newest_up_time_seconds < bi.os_time_seconds) {
                newest_up_time_seconds = bi.os_time_seconds;
                theidx_up = i;
            }
        }
    }
    s32 theidx = -1;
    auto effective_state = ButtonState_NULL;
    if (count_down > 0) {
        effective_state |= ButtonState_DOWN;
        if (count_down == count_pressed && pressed) {
            effective_state |= ButtonState_PRESS;
            theidx = theidx_pressed;
        } else if (repeat) {
            effective_state |= ButtonState_REPEAT;
            theidx = theidx_repeat;
        } else {
            theidx = theidx_down;
        }
    } else if (release) {
        effective_state |= ButtonState_RELEASE;
        theidx = theidx_release;
    } else {
        effective_state = {};
        theidx = theidx_up;
    }

    ASSERT(theidx != -1);
    // TODO: Probably wrong with repeat.
    auto bi = simple_key_get(gin, keys.data[theidx]);
    ASSERT(bi.state == effective_state);
    ASSERT_SLOW(bi.state == simple_key_get_state(gin, keys));
    return bi;
}

static inline bool
simple_key_down(GameInput *gin, Slice<BeatKey> keys)
{
    return (simple_key_get_state(gin, keys) & ButtonState_DOWN) != 0;
}

static inline bool
simple_key_pressed(GameInput *gin, Slice<BeatKey> keys)
{
    return (simple_key_get_state(gin, keys) & ButtonState_PRESS) != 0;
}

static inline bool
simple_key_pressed_or_repeat(GameInput *gin, Slice<BeatKey> keys)
{
    return (simple_key_get_state(gin, keys) & (ButtonState_PRESS | ButtonState_REPEAT)) != 0;
}



static inline bool
key_binding_down(GameInput *gin, KeyBinding key)
{
    ButtonInput bi = simple_key_get(gin, key.key);
    if ((bi.state & ButtonState_DOWN)
        && keyboard_mods_match(bi.mods_for_press_or_release, key.mods))
        return true;
    return false;
}

static inline bool
key_binding_pressed(GameInput *gin, KeyBinding key)
{
    ButtonInput bi = simple_key_get(gin, key.key);
    if ((bi.state & ButtonState_PRESS)
        && keyboard_mods_match(bi.mods_for_press_or_release, key.mods))
        return true;
    return false;
}

static inline bool
key_binding_pressed_or_repeat(GameInput *gin, KeyBinding key)
{
    ButtonInput bi = simple_key_get(gin, key.key);
    if ((bi.state & (ButtonState_PRESS | ButtonState_REPEAT))
        && keyboard_mods_match(bi.mods_for_press_or_release, key.mods))
        return true;
    return false;
}

// @KeyBindingArrayDoublePress
static inline ButtonState
key_binding_get_state(GameInput *gin, Slice<KeyBinding> keys)
{
    if (keys.len <= 0) return {};
    s32 count_down = 0;
    s32 count_pressed = 0;
    s32 count_repeat = 0;
    bool pressed = false;
    bool repeat = false;
    bool release = false;

    ButtonState effective_state = {};
    for (s32 i = 0; i < keys.len; i++) {
        auto k = keys.data[i];
        auto bi = simple_key_get(gin, k.key);
        bool mods_match = keyboard_mods_match(bi.mods_for_press_or_release, k.mods);
        auto s = bi.state;
        if (s & ButtonState_DOWN && mods_match)
            count_down++;
        if (s & ButtonState_PRESS && mods_match) {
            count_pressed++;
            pressed = true;
        }
        if (s & ButtonState_REPEAT && mods_match) {
            count_repeat++;
            repeat = true;
        }
        if (s & ButtonState_RELEASE)
            release = true;
    }

    if (count_down > 0) {
        effective_state |= ButtonState_DOWN;
        if (count_down == count_pressed && pressed) {
            effective_state |= ButtonState_PRESS;
        } else if (repeat) {
            effective_state |= ButtonState_REPEAT;
        }
    } else if (release) {
        effective_state |= ButtonState_RELEASE;
    } else {
        effective_state = {};
    }

    return effective_state;
}

static inline bool
key_binding_down(GameInput *gin, Slice<KeyBinding> bindings)
{
    return (key_binding_get_state(gin, bindings) & ButtonState_DOWN) != 0;
}

static inline bool
key_binding_pressed(GameInput *gin, Slice<KeyBinding> bindings)
{
    return (key_binding_get_state(gin, bindings) & ButtonState_PRESS) != 0;
}

static inline bool
key_binding_pressed_or_repeat(GameInput *gin, Slice<KeyBinding> bindings)
{
    return (key_binding_get_state(gin, bindings) & (ButtonState_PRESS | ButtonState_REPEAT)) != 0;
}



static inline Slice<BeatKey>
get_column_key_bindings(GameState *gs, Keymode keymode, s32 column)
{
    ASSERT(gs->playing.other.state >= PlayingState_Ready);
    ASSERT(column >= 0);
    ASSERT(column < keymode_to_columns_table[keymode]);
    auto *bindings_for_keymodes = gs->bindings_for_keymodes[keymode];
    ASSERT(bindings_for_keymodes);
    auto result = bindings_for_keymodes[column];
    return result;
}

static inline ButtonState
column_key_get_state(GameState *gs, GameInput *gin, Keymode keymode, s32 column)
{
    auto bindings = get_column_key_bindings(gs, keymode, column);
    return simple_key_get_state(gin, bindings);
}

static inline ButtonInput
column_key_get(GameState *gs, GameInput *gin, Keymode keymode, s32 column)
{
    auto bindings = get_column_key_bindings(gs, keymode, column);
    return simple_key_get(gin, bindings);
}

static inline bool
column_key_down(GameState *gs, GameInput *gin, Keymode keymode, s32 column)
{
    auto bindings = get_column_key_bindings(gs, keymode, column);
    return simple_key_down(gin, bindings);
}

