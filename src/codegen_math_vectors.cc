#include <stdio.h>
#include <stdlib.h>

static char buf[10<<20];
static char *buf_ptr;
static int do_cpp = 1;

typedef struct {
    char const *type;
    int count;
    char const *base;
} VecType;

static void
generate_hadamard(VecType v, char const *function_name, char const *op)
{
    buf_ptr += sprintf(buf_ptr,
                       "static inline %s %s%s%s(%s a, %s b)\n"
                       "{\n"
                       "    %s result;\n",
                       // v.type, function_name, do_cpp ? "" : "_", do_cpp ? "" : v.type, v.type, v.type,
                       v.type, function_name, "_", v.type, v.type, v.type,
                       v.type);
    for (int i = 0; i < v.count; i++) {
        buf_ptr += sprintf(buf_ptr,
                           "    result.e[%d] = a.e[%d] %s b.e[%d];\n",
                           i, i, op, i);
    }
    buf_ptr += sprintf(buf_ptr,
                       "    return result;\n"
                       "}\n\n");

    if (do_cpp) {
        buf_ptr += sprintf(buf_ptr,
                           "#ifdef __cplusplus\n"
                           "static inline %s %s(%s a, %s b) {return %s_%s(a, b);}\n"
                           "#endif\n\n",
                           v.type, function_name, v.type, v.type, function_name, v.type);
    }
}

static void
generate_cpp_operator(VecType v, char const *op)
{
    buf_ptr += sprintf(buf_ptr, "#ifdef __cplusplus\n");
    buf_ptr += sprintf(buf_ptr,
                       "inline %s operator%s(%s a, %s b)\n"
                       "{\n"
                       "    %s result;\n",
                       v.type, op, v.type, v.type,
                       v.type);
    for (int i = 0; i < v.count; i++) {
        buf_ptr += sprintf(buf_ptr,
                           "    result.e[%d] = a.e[%d] %s b.e[%d];\n",
                           i, i, op, i);
    }
    buf_ptr += sprintf(buf_ptr,
                       "    return result;\n"
                       "}\n\n");
    buf_ptr += sprintf(buf_ptr,
                       "inline %s &operator%s=(%s &a, %s b)\n"
                       "{\n"
                       "    a = a %s b;\n"
                       "    return a;\n"
                       "}\n",
                       v.type, op, v.type, v.type,
                       op);
    buf_ptr += sprintf(buf_ptr, "#endif\n\n");
}

static void
generate_scalar_mult(VecType v)
{
    buf_ptr += sprintf(buf_ptr,
                       "static inline %s scalar_%s(%s a, %s b)\n"
                       "{\n"
                       "    %s result;\n",
                       v.type, v.type, v.base, v.type,
                       v.type);
    for (int i = 0; i < v.count; i++) {
        buf_ptr += sprintf(buf_ptr,
                           "    result.e[%d] = a * b.e[%d];\n",
                           i, i);
    }
    buf_ptr += sprintf(buf_ptr,
                       "    return result;\n"
                       "}\n\n");



    if (do_cpp) {
        buf_ptr += sprintf(buf_ptr, "#ifdef __cplusplus\n");
        buf_ptr += sprintf(buf_ptr,
                           "static inline %s %s(%s a, %s b) {return scalar_%s(a, b);}\n\n",
                           v.type, "scalar", v.base, v.type, v.type);

        buf_ptr += sprintf(buf_ptr,
                           "inline %s operator*(%s a, %s b)\n"
                           "{\n"
                           "    %s result;\n",
                           v.type, v.base, v.type,
                           v.type);
        for (int i = 0; i < v.count; i++) {
            buf_ptr += sprintf(buf_ptr,
                               "    result.e[%d] = a * b.e[%d];\n",
                               i, i);
        }
        buf_ptr += sprintf(buf_ptr,
                           "    return result;\n"
                           "}\n\n");

        buf_ptr += sprintf(buf_ptr,
                           "inline %s &operator*=(%s &a, %s b)\n"
                           "{\n"
                           "    a = b * a;\n"
                           "    return a;\n"
                           "}\n\n",
                           v.type, v.type, v.base);
        buf_ptr += sprintf(buf_ptr, "#endif\n\n");
    }
}

static void
generate_inner(VecType v)
{
    buf_ptr += sprintf(buf_ptr,
                       "static inline %s %s%s%s(%s a, %s b)\n"
                       "{\n"
                       "    %s result = (%s)0;\n",
                       // v.base, "inner", do_cpp ? "" : "_", do_cpp ? "" : v.type, v.type, v.type,
                       v.base, "inner", "_", v.type, v.type, v.type,
                       v.base, v.base);
    for (int i = 0; i < v.count; i++) {
        buf_ptr += sprintf(buf_ptr,
                           "    result += a.e[%d] * b.e[%d];\n",
                           i, i);
    }
    buf_ptr += sprintf(buf_ptr,
                       "    return result;\n"
                       "}\n\n");


    if (do_cpp) {
        buf_ptr += sprintf(buf_ptr,
                           "#ifdef __cplusplus\n"
                           "static inline %s %s(%s a, %s b) {return inner_%s(a, b);}\n"
                           "#endif\n\n",
                           v.base, "inner", v.type, v.type, v.type);
    }
}

static void
generate_length_sq(VecType v)
{
    buf_ptr += sprintf(buf_ptr,
                       "static inline %s %s%s%s(%s a)\n"
                       "{\n"
                       "    %s result = inner_%s(a, a);\n",
                       v.base, "length_sq", "_", v.type, v.type,
                       v.base, v.type);
    buf_ptr += sprintf(buf_ptr,
                       "    return result;\n"
                       "}\n\n");


    if (do_cpp) {
        buf_ptr += sprintf(buf_ptr,
                           "#ifdef __cplusplus\n"
                           "static inline %s %s(%s a) {return length_sq_%s(a);}\n"
                           "#endif\n\n",
                           v.base, "length_sq", v.type, v.type);
    }
}
#ifndef ssizeof
# define ssizeof(x) (ssize_t)sizeof(x)
#endif
#define ARRAY_COUNT(x) (ssizeof(x)/ssizeof((x)[0]))

int main(int argc, char **argv)
{
    if (argc < 2) {
        printf("usage: %s out_filename\n", argv[0]);
        return 1;
    }
    buf_ptr = buf;
    VecType types[] = {
        {"v2"  , 2 , "f32"} ,
        {"v3"  , 3 , "f32"} ,
        {"v4"  , 4 , "f32"} ,

#if 1
        {"v2s" , 2 , "s32"} ,
        {"v3s" , 3 , "s32"} ,
        {"v4s" , 4 , "s32"} ,

        {"v2u" , 2 , "u32"} ,
        {"v3u" , 3 , "u32"} ,
        {"v4u" , 4 , "u32"} ,
#endif
    };
    for (int i = 0; i < (int)ARRAY_COUNT(types); i++) {
        generate_hadamard(types[i], "hadamard_mult", "*");
        generate_hadamard(types[i], "hadamard_div", "/");
        generate_hadamard(types[i], "add", "+");
        generate_hadamard(types[i], "sub", "-");
        generate_inner(types[i]);
        generate_length_sq(types[i]);
        generate_scalar_mult(types[i]);
        if (do_cpp) {
            generate_cpp_operator(types[i], "+");
            generate_cpp_operator(types[i], "-");
        }
    }

    char const *filename = argv[1];
    FILE *cfile = fopen(filename, "wb");
    if (!cfile) {
        fprintf(stderr, "codegen_math_vectors: Failed to open file '%s'\n", filename);
        return 1;
    }
    fwrite(buf, (size_t)(buf_ptr-buf), 1, cfile);
    fclose(cfile);
    return 0;
}
