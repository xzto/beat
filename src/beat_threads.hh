#ifndef BEAT_THREADS_H
#define BEAT_THREADS_H


// struct Thread {
// };

// struct Semaphore {
//     void *sdl_sem;
// };

#if 0
struct Mutex {
    void *ptr;
};


static Mutex mutex_create();
static void mutex_destroy(Mutex *m);
static bool mutex_is_initialized(Mutex *m);
static void mutex_lock(Mutex *m);
static void mutex_unlock(Mutex *m);
#endif


// To initialize, set it to zero.
struct TicketMutex {
    typedef u32 Ticket;
    Atomic<Ticket> ticket;
    Atomic<Ticket> served;
};

static inline void ticket_mutex_lock(TicketMutex *m);
static inline bool ticket_mutex_try_lock(TicketMutex *m);
static inline void ticket_mutex_unlock(TicketMutex *m);

// usage: SCOPED_LOCK(&mutex) or SCOPED_LOCK(&ticket_mutex)
struct Scoped_Lock_Helper { int a; };

#if 0
struct Scoped_Lock_Mutex {
    ~Scoped_Lock_Mutex() {
        mutex_unlock(mutex);
    }
    Mutex *mutex;
};
Scoped_Lock_Mutex operator+=(const Scoped_Lock_Helper&, Mutex *mutex)
{
    mutex_lock(mutex);
    return Scoped_Lock_Mutex{ .mutex = mutex };
}
#endif

struct Scoped_Lock_TicketMutex {
    Scoped_Lock_TicketMutex(TicketMutex *m) {
        this->mutex = m;
        ticket_mutex_lock(m);
    }
    ~Scoped_Lock_TicketMutex() {
        ticket_mutex_unlock(mutex);
    }
    TicketMutex *mutex;
};

#define SCOPED_LOCK_JOIN_A(a, b) a ## b
#define SCOPED_LOCK_JOIN_B(a, b) SCOPED_LOCK_JOIN_A(a, b)
#define SCOPED_LOCK_COUNTER(mutex, counter) \
    Scoped_Lock_TicketMutex SCOPED_LOCK_JOIN_B(_scoped_lock_, counter)(mutex);
#define SCOPED_LOCK(mutex) SCOPED_LOCK_COUNTER(mutex, __COUNTER__)



#if 0
// TODO: Write tests for SharedMutex. I don't want to find out that my try_lock
// is completely wrong after a few weeks like I did with TicketMutex.
// TODO: This lock isn't fair.
// TODO: Do research to see if there is anything better than this.
// NOTE: My only use case at the moment for this SharedMutex (aka read/write lock) is that I need shared access 99.99999999% of the time and unique access 0.000000000000000001% of the time.
// TODO: I should optimize it for shared access (if possible). Currently unique access is much faster.
// TODO: Suppose A and B want shared access while C wants exclusive access.
// A calls shared_lock
// B calls shared_lock
// C calls unique_lock
// A releases
// A calls shared_lock
// B releases
// A acquires the shared lock
// C might never get the lock in this case.
struct SharedMutex {
    static constexpr u32 COMBINED_VALUE_FOR_UNIQUE_HELD = 0x10000; // The combined value when a unique lock is held.
    static constexpr u32 COMBINED_VALUE_FOR_UNIQUE_FREE = 0;       // The combined value when there is a unique lock available.
    union {
        struct {
            Atomic<u16> shared; // Number of shared locks held.
            Atomic<u16> unique; // Number of unique locks held (0 or 1).
        };
        Atomic<u32> combined;
    };
};
static inline void shared_mutex_init(SharedMutex *m);
static inline void shared_mutex_unique_lock(SharedMutex *m);
static inline void shared_mutex_unique_unlock(SharedMutex *m);
static inline void shared_mutex_shared_lock(SharedMutex *m);
static inline void shared_mutex_shared_unlock(SharedMutex *m);
#else
struct SharedTicketMutex {
    TicketMutex underlying_mutex;
    Atomic<u32> shared_count;
    static inline SharedTicketMutex init();
};
static inline void shared_ticket_mutex_unique_lock(SharedTicketMutex *m);
static inline void shared_ticket_mutex_unique_unlock(SharedTicketMutex *m);
static inline void shared_ticket_mutex_shared_lock(SharedTicketMutex *m);
static inline void shared_ticket_mutex_shared_unlock(SharedTicketMutex *m);
#endif



struct Scoped_UniqueLock_SharedTicketMutex {
    Scoped_UniqueLock_SharedTicketMutex(SharedTicketMutex *m) {
        this->mutex = m;
        shared_ticket_mutex_unique_lock(mutex);
    }
    ~Scoped_UniqueLock_SharedTicketMutex() {
        shared_ticket_mutex_unique_unlock(mutex);
    }
    SharedTicketMutex *mutex;
};

#define SCOPED_UNIQUE_LOCK_JOIN_A(a, b) a ## b
#define SCOPED_UNIQUE_LOCK_JOIN_B(a, b) SCOPED_UNIQUE_LOCK_JOIN_A(a, b)
#define SCOPED_UNIQUE_LOCK_COUNTER(mutex, counter) \
    Scoped_UniqueLock_SharedTicketMutex SCOPED_UNIQUE_LOCK_JOIN_B(_scoped_unique_lock_, counter)(mutex);
#define SCOPED_UNIQUE_LOCK(mutex) SCOPED_UNIQUE_LOCK_COUNTER(mutex, __COUNTER__)

struct Scoped_SharedLock_SharedTicketMutex {
    Scoped_SharedLock_SharedTicketMutex(SharedTicketMutex *m) {
        this->mutex = m;
        shared_ticket_mutex_shared_lock(mutex);
    }
    ~Scoped_SharedLock_SharedTicketMutex() {
        shared_ticket_mutex_shared_unlock(mutex);
    }
    SharedTicketMutex *mutex;
};

#define SCOPED_SHARED_LOCK_JOIN_A(a, b) a ## b
#define SCOPED_SHARED_LOCK_JOIN_B(a, b) SCOPED_SHARED_LOCK_JOIN_A(a, b)
#define SCOPED_SHARED_LOCK_COUNTER(mutex, counter) \
    Scoped_SharedLock_SharedTicketMutex SCOPED_SHARED_LOCK_JOIN_B(_scoped_shared_lock_, counter)(mutex);
#define SCOPED_SHARED_LOCK(mutex) SCOPED_SHARED_LOCK_COUNTER(mutex, __COUNTER__)

#endif
