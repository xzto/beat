// Returns the row idx of the note added
// @IgnoreSomeBitsOfTheMantissa
static s32
parser_utils_add_note_to_diff(Notes *notes, NewArrayDynamic<NoteTimestamp> *ts_buf, NewArrayDynamic<NoteType> *raw_notes_buf,
                              NoteType note_value, s32 column, f64 seconds_from_song_start, s32 start_idx)
{
    // Search for ts_new in diff->notes.timestamps. If found, set the column
    // there. If not found or the diff doesn't have any rows, add one more row
    // to the diff, sort it, and set the column.

    ASSERT(seconds_from_song_start >= 0);
    ASSERT(column >= 0 && column < notes->columns);

    s32 row_idx = -1;
    s32 approx_row_idx = -1;
    NoteTimestamp ts_new = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(seconds_from_song_start)};
    // NoteTimestamp ts_new = seconds_to_song_ts(seconds_from_song_start, 1, 0, 0);
    bool is_new_timestamp = false;
    bool has_to_sort_it = false;
    s32 pitch = notes->pitch;
    NoteTimestamp ts_at_approx_row = {-1};
    if (notes->rows_count == 0) {
        is_new_timestamp = true;
    } else {
        approx_row_idx = find_row_idx_by_similar_timestamp(notes, ts_new, start_idx, true);
        ts_at_approx_row = notes->timestamps[approx_row_idx];
        if (ts_at_approx_row == ts_new) {
            // Same timestamp, just set the column.
            row_idx = approx_row_idx;
            NoteType *row_ptr = notes->raw_notes + row_idx*pitch;
            ASSERT(NOTES_GET_COLUMN(row_ptr, column) == NOTE_BLANK);
            NOTES_SET_COLUMN(row_ptr, column, note_value);
        } else if (approx_row_idx == notes->rows_count-1 && ts_at_approx_row < ts_new) {
            // New timestamp.
            // The new timestamp will be at the end of the thing, so no need to sort it.
            is_new_timestamp = true;
            has_to_sort_it = false;
        } else {
            // New timestamp.
            // The new tiemstamp will not be at the end of the thing so we have
            // to move some memory and then insert it at the right place.
            is_new_timestamp = true;
            has_to_sort_it = true;
        }
    }

    if (is_new_timestamp) {
        // Do array_add(xx_buf) and set the new pointer for notes->xx
        // set row_idx (result value)
        // Zero the new row
        void *old_ts_buf_data = ts_buf->data;
        void *old_raw_notes_buf_data = raw_notes_buf->data;

        ts_buf->add_uninit(1);
        if (ts_buf->data != old_ts_buf_data)
            notes->timestamps = ts_buf->data;

        raw_notes_buf->add_uninit(pitch);
        if (raw_notes_buf->data != old_raw_notes_buf_data)
            notes->raw_notes = raw_notes_buf->data;

        notes->rows_count++;
        if (has_to_sort_it) {
            // it will insert in the middle to keep things sorted.
            if (ts_at_approx_row < ts_new) {
                // insert after
                row_idx = approx_row_idx + 1;
            } else {
                // insert before
                row_idx = approx_row_idx;
            }
            // move around memory
            s32 elems_to_move = (notes->rows_count - row_idx - 1);
            void *move_ptr;
            s32 elem_size;
            // move raw_notes
            move_ptr = notes->raw_notes + row_idx*pitch;
            elem_size = ssizeof(NoteType)*pitch;
            xmemmove((u8 *)move_ptr + elem_size, move_ptr, elems_to_move*elem_size);
            // move timestamps
            move_ptr = notes->timestamps + row_idx;
            elem_size = SSIZEOF_VAL(notes->timestamps);
            xmemmove((u8 *)move_ptr + elem_size, move_ptr, elems_to_move*elem_size);
        } else {
            row_idx = notes->rows_count - 1;
        }
        notes->timestamps[row_idx] = ts_new;

        NoteType *row_ptr = notes->raw_notes + row_idx*pitch;
        zero_struct_count(row_ptr, pitch);
        ASSERT(NOTES_GET_COLUMN(row_ptr, column) == NOTE_BLANK);
        NOTES_SET_COLUMN(row_ptr, column, note_value);
        ASSERT(row_idx >= 0);
    }

    // printf("Set row %d col %d val %d\n", row_idx, column, note_value);
    ASSERT(row_idx >= 0 && row_idx < notes->rows_count);
    return row_idx;
}
