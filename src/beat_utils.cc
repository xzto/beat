#include <stdbool.h>
#include <math.h>

static bool
is_aligned_to(u64 x, u64 alignment)
{
    // alignment might not be a power of two.
    return ((x / alignment) * alignment) == x;
}
static bool
is_aligned_to(s64 x, s64 alignment)
{
    // alignment might not be a power of two.
    return ((x / alignment) * alignment) == x;
}


static inline void * xmemset(void *ptr, u8 byte, smem n) {
    ASSERT_SLOW(n >= 0);
    return memset(ptr, byte, (size_t)n);
}

static inline s32
xmemcmp(void *a, void *b, smem n) {
    ASSERT_SLOW(n >= 0);
    return memcmp(a, b, (size_t)n);
}

//returns the number of characters written
ATTRIBUTE_FORMAT_PRINTF(3, 4)
static smem
xsnprintf(u8 *buf, smem bufsize, const char *fmt, ...)
{
    ASSERT_SLOW(bufsize >= 0);
    va_list ap;
    va_start(ap, fmt);
    smem result = (smem)vsnprintf((char*)buf, (size_t)bufsize, fmt, ap);
    va_end(ap);
    return result;
}

#if 0
{
    // usage:
    u8 buf[512];
    String bufstr = {};
    smem num_new_chars = xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "thing: %d", 1920);
    num_new_chars = xsnprintf_string(buf, ssizeof(buf), true, &bufstr, " x %d", 1080);
}
#endif
//returns the number of characters written
ATTRIBUTE_FORMAT_PRINTF(5, 6)
static void
xsnprintf_string(u8 *buf, smem bufsize, bool append, String *str, const char *fmt, ...)
{
    ASSERT_SLOW(bufsize >= 0);
    if (str->data) {
        // this will probably catch an uninitialized variable; I wonder if I can tell the compiler that `str` must be initialized before going into this function...
        ASSERT_SLOW(str->data == buf);
    }
    if (!str->data || !append) {
        str->data = buf;
        str->len = 0;
    }
    smem num_bytes_written = 0;
    va_list ap;
    va_start(ap, fmt);
    auto size_remaining = bufsize - str->len;
    if (size_remaining > 0) {
        num_bytes_written = vsnprintf((char*)(buf + str->len), (size_t)size_remaining, fmt, ap);
        // TODO: Pass an argument to crash if it doesn't fit in the buffer.
        if (num_bytes_written >= size_remaining)
            num_bytes_written = size_remaining - 1;
        str->len += num_bytes_written;
    } else {
        num_bytes_written = 0;
    }
    va_end(ap);
    ASSERT_SLOW(str->len <= bufsize);
    // return num_bytes_written;
}

struct XmallocFlags {
    bool zero = false;
    smem align = 8;
};

static inline XmallocFlags
default_xmalloc_flags() {
    XmallocFlags flags = {};
    return flags;
}

static inline XmallocFlags
xmalloc_flags_zero() {
    XmallocFlags flags = default_xmalloc_flags();
    flags.zero = true;
    return flags;
}

static inline XmallocFlags
xmalloc_flags_nozero() {
    XmallocFlags flags = default_xmalloc_flags();
    flags.zero = false;
    return flags;
}

static inline XmallocFlags
xmalloc_flags_zero_align(smem align) {
    XmallocFlags flags = default_xmalloc_flags();
    flags.zero = true;
    flags.align = align;
    return flags;
}

static inline XmallocFlags
xmalloc_flags_nozero_align(smem align) {
    XmallocFlags flags = default_xmalloc_flags();
    flags.zero = false;
    flags.align = align;
    return flags;
}

static inline void *
xmalloc(smem size, XmallocFlags flags = default_xmalloc_flags()) {
    ASSERT_SLOW(size >= 0);
    ASSERT_SLOW(flags.align > 0);
    flags.align = MAXIMUM(flags.align, ssizeof(void*));
    void *result = 0;
    result = aligned_alloc((size_t)flags.align, (size_t)size);
    ASSERT(is_aligned_to((umem)result, (umem)flags.align));
    ASSERT(result);
    if (flags.zero) {
        zero_size(result, size);
    }
    return result;
}

// I prefer using these instead of xmalloc(ssizeof(s) * count)
#define xmalloc_struct_count(s, count, ...) (s *)xmalloc(ssizeof(s)*(count), ##__VA_ARGS__)
#define xmalloc_struct(s, ...) xmalloc_struct_count(s, 1, ##__VA_ARGS__)

static inline void *
xrealloc(void *prev_ptr, smem size) {
    ASSERT_SLOW(size >= 0);
    void *result = 0;
    result = realloc(prev_ptr, (size_t)size);
    ASSERT(result);
    return result;
}

static inline void
xfree(void *ptr) {
    free(ptr);
}

static inline void *
xmemcpy(void *dest, void *src, smem n) {
    ASSERT_SLOW(n >= 0);
    return memcpy(dest, src, (size_t)n);
}

// I prefer using these instead of xmemcpy(dst, src, ssizeof(*dst)*count)
#define xmemcpy_struct_count(dst, src, type, count) do {                                \
    type *_temp_test_typecheck_dst = (dst);                                             \
    type *_temp_test_typecheck_src = (src);                                             \
    xmemcpy(_temp_test_typecheck_dst, _temp_test_typecheck_src, ssizeof(type)*(count)); \
} while (0);

#define xmemcpy_struct(dst, src, type) xmemcpy_struct_count(dst, src, type, 1)

static inline void *
xmemmove(void *dest, void *src, smem n) {
    ASSERT_SLOW(n >= 0);
    return memmove(dest, src, (size_t)n);
}

#define xmemmove_struct_count(dst, src, type, count) do {                               \
    type *_temp_test_typecheck_dst = (dst);                                             \
    type *_temp_test_typecheck_src = (src);                                             \
    xmemcpy(_temp_test_typecheck_dst, _temp_test_typecheck_src, ssizeof(type)*(count)); \
} while (0);

#define xmemmove_struct(dst, src, type) xmemmove_struct_count(dst, src, type, 1)

static inline smem
xstrlen(char *s)
{
    return (smem)strlen(s);
}

static inline smem
xstrlen(u8 *s)
{
    return (smem)strlen((char*)s);
}

// returns the len of "s" or "max" at most
static smem
cstr_len_or_max(char *s, smem max)
{
    ASSERT_SLOW(max >= 0);
    for (smem i = 0; i < max; i++) {
        if (s[i] == 0)
            return i;
    }
    return max;
}

static inline String
cstr_to_string(char *s)
{
    String result = { .len = xstrlen(s), .data = (u8*)s };
    return result;
}

static inline String
cstr_to_string_max_size(char *s, smem max_size)
{
    String result = { .len = cstr_len_or_max(s, max_size), .data = (u8*)s };
    return result;
}

// copy_string and copy_cstring: if the length is zero we return {.len=0, .data=NULL}
static String
copy_string(String a) {
    String result = {};
    if (a.len > 0) {
        ASSERT(a.data != NULL);
        result.data = (u8 *)xmalloc(a.len + 1);
        xmemcpy(result.data, a.data, a.len);
        result.len = a.len;
        result.data[result.len] = 0;
    }
    return result;
}

static String
copy_cstring(char *c_str) {
    String result = {};
    if (c_str != NULL) {
        smem c_str_len = xstrlen(c_str);

        if (c_str_len > 0) {
            result.data = (u8 *)xmalloc(c_str_len);
            xmemcpy(result.data, c_str, c_str_len);
            result.len = c_str_len;
        }
    }
    return result;
}

static inline void
free_string(String *str)
{
    ASSERT(str != NULL);
    xfree(str->data);
    zero_struct(str);
}

static inline bool
string_exists_and_is_not_empty(String s)
{
    bool result = s.data != NULL && s.len > 0;
    return result;
}

// i use it for some asserts. maybe I should create a type StringNullter instead.
static inline bool
string_is_nullter(String s)
{
    bool result = s.data[s.len] == 0;
    return result;
}

static inline bool
// string_nullter_is_actually_the_same_len(StringNullter str)
string_nullter_is_actually_the_same_len(String str)
{
    // str.assert();
    ASSERT(string_is_nullter(str));
    return str.len == xstrlen(str.data);
}

// TODO: Support utf8
static int
string_compare(String a, String b)
{
#if 0
    u8 *a_ptr = a.data;
    u8 *b_ptr = b.data;
    u8 *a_max = a.data + a.len;
    u8 *b_max = b.data + b.len;

    while (a_ptr < a_max && b_ptr < b_max) {
        if (*a_ptr != *b_ptr) {
            if (*a_ptr < *b_ptr) return -1;
            if (*a_ptr > *b_ptr) return +1;
        }
        ++a_ptr;
        ++b_ptr;
    }
    if (a.len < b.len) return -1; // '\0' - *b_ptr
    if (a.len > b.len) return +1; // *a_ptr - '\0'

    return 0;
#else
    ASSERT_SLOW(a.len >= 0 && b.len >= 0);
    int cmp = xmemcmp(a.data, b.data, MINIMUM(a.len, b.len));
    if (cmp == 0) {
        if (a.len < b.len) return -1; // '\0' - *b_ptr
        if (a.len > b.len) return +1; // *a_ptr - '\0'
        return 0;
    }
    return cmp;
#endif
}

static int
string_compare_case_insensitive(String a, String b)
{
    u8 *a_ptr = a.data;
    u8 *b_ptr = b.data;
    u8 *a_max = a.data + a.len;
    u8 *b_max = b.data + b.len;

    // TODO: 'A' should have a higher precedence than 'a'. Currently they have the same precedence.
    while (a_ptr < a_max && b_ptr < b_max) {
        u8 a_ch = *a_ptr;
        u8 b_ch = *b_ptr;
        if (BETWEEN(a_ch, 'A', 'Z')) a_ch |= 32;
        if (BETWEEN(b_ch, 'A', 'Z')) b_ch |= 32;
        if (a_ch != b_ch) {
            if (a_ch < b_ch) return -1;
            if (a_ch > b_ch) return +1;
        }
        ++a_ptr;
        ++b_ptr;
    }
    if (a.len < b.len) return -1; // '\0' - *b_ptr
    if (a.len > b.len) return +1; // *a_ptr - '\0'

    return 0;
}

static int
string_compare_max_len(String a, String b, smem max_len)
{
    String anew = a;
    String bnew = b;
    if (anew.len > max_len) anew.len = max_len;
    if (bnew.len > max_len) bnew.len = max_len;
    return string_compare(anew, bnew);
}


static inline bool
strings_are_equal(String a, String b)
{
    ASSERT_SLOW(a.len >= 0 && b.len >= 0);
    if (a.len != b.len) return false;
    return xmemcmp(a.data, b.data, a.len) == 0;
}

static inline bool
strings_are_equal_case_insensitive(String a, String b)
{
    if (a.len != b.len) return false;
    return string_compare_case_insensitive(a, b) == 0;
}

static inline bool
strings_are_equal_max_len(String a, String b, smem max_len)
{
    String anew = a;
    String bnew = b;
    if (anew.len > max_len) anew.len = max_len;
    if (bnew.len > max_len) bnew.len = max_len;
    return strings_are_equal(anew, bnew);
}

// test if `str` starts with `test_start`
static inline bool
string_starts_with(String str, String test_start)
{
    String strnew = str;
    if (strnew.len > test_start.len) strnew.len = test_start.len;
    return strings_are_equal(strnew, test_start);
}
static inline bool
string_starts_with_case_insensitive(String str, String test_start)
{
    String strnew = str;
    if (strnew.len > test_start.len) strnew.len = test_start.len;
    return strings_are_equal_case_insensitive(strnew, test_start);
}

// test if `sub` is a substring of `str`
static bool
string_has_substr(String str, String sub, smem *maybe_ret_idx=0)
{
    // TODO: faster alrorithm
    if (sub.len > str.len) {
        if (maybe_ret_idx) *maybe_ret_idx = -1;
        return false;
    }
    smem diff_size = str.len - sub.len;
    for (smem i = 0; i < diff_size+1; i++) {
        String test = {};
        test.len = sub.len;
        test.data = str.data + i;
        ASSERT_SLOW(test.data + test.len <= str.data + str.len);
        if (strings_are_equal(test, sub)) {
            if (maybe_ret_idx) *maybe_ret_idx = i;
            return true;
        }
    }
    if (maybe_ret_idx) *maybe_ret_idx = -1;
    return false;
}

// test if `sub` is a substring of `str`
static bool
string_has_substr_case_insensitive(String str, String sub, smem *maybe_ret_idx=0)
{
    // TODO: faster alrorithm
    if (sub.len > str.len) {
        if (maybe_ret_idx) *maybe_ret_idx = -1;
        return false;
    }
    smem diff_size = str.len - sub.len;
    for (smem i = 0; i < diff_size+1; i++) {
        String test = {};
        test.len = sub.len;
        test.data = str.data + i;
        ASSERT_SLOW(test.data + test.len <= str.data + str.len);
        if (strings_are_equal_case_insensitive(test, sub)) {
            if (maybe_ret_idx) *maybe_ret_idx = i;
            return true;
        }
    }
    if (maybe_ret_idx) *maybe_ret_idx = -1;
    return false;
}

// for std::numerical_limit<BoyerMooreTableType>::max();
#include <limits>
typedef s16 BoyerMooreTableType;

static bool
string_has_substring_boyer_moore_with_table(String haystack, String needle, BoyerMooreTableType table[256],
                                            smem *maybe_ret_idx=0)
{
    if (needle.len <= 0) {
        if (maybe_ret_idx) *maybe_ret_idx = 0;
        return true;
    }
    if (needle.len > haystack.len) {
        if (maybe_ret_idx) *maybe_ret_idx = -1;
        return false;
    }
    if (needle.len == haystack.len) {
        if (strings_are_equal(haystack, needle)) {
            if (maybe_ret_idx) *maybe_ret_idx = 0;
            return true;
        } else {
            if (maybe_ret_idx) *maybe_ret_idx = -1;
            return false;
        }
    }

    u8 needle_last = needle.data[needle.len-1];
    for (smem k = needle.len-1; k < haystack.len; ) {
        u8 haystack_at_k = haystack.data[k];
        if (haystack_at_k == needle_last) {
            String haystack_test = {};
            haystack_test.data = haystack.data + k - (needle.len-1);
            haystack_test.len = needle.len;
            ASSERT_SLOW(haystack_test.data >= haystack.data && haystack_test.data + haystack_test.len-1 <= haystack.data + haystack.len-1);
            if (strings_are_equal(haystack_test, needle)) {
                if (maybe_ret_idx) *maybe_ret_idx = k - (needle.len-1);
                return true;
            }
        }
        k += table[haystack_at_k];
    }
    if (maybe_ret_idx) *maybe_ret_idx = -1;
    return false;
}

static void
generate_table_for_boyer_moore_string_search(String needle, BoyerMooreTableType table[256])
{
    BoyerMooreTableType numeric_max = std::numeric_limits<BoyerMooreTableType>::max();
    BoyerMooreTableType to_fill = (BoyerMooreTableType)MINIMUM(numeric_max, MAXIMUM(1, needle.len));
    for (smem i = 0; i < 256; i++) {
        table[i] = to_fill;
    }
    for (smem i = 0; i < needle.len; i++) {
        u8 needle_at_i = (u8)needle.data[i];
        table[needle_at_i] = (BoyerMooreTableType)MINIMUM(numeric_max, MAXIMUM(1, needle.len - 1 - i));
    }
}
static bool
string_has_substring_boyer_moore(String haystack, String needle, smem *maybe_ret_idx=0)
{
    if (needle.len <= 0) {
        if (maybe_ret_idx) *maybe_ret_idx = 0;
        return true;
    }
    if (needle.len > haystack.len) {
        if (maybe_ret_idx) *maybe_ret_idx = -1;
        return false;
    }
    if (needle.len == haystack.len) {
        if (strings_are_equal(haystack, needle)) {
            if (maybe_ret_idx) *maybe_ret_idx = 0;
            return true;
        } else {
            if (maybe_ret_idx) *maybe_ret_idx = -1;
            return false;
        }
    }

    BoyerMooreTableType table[256];
    generate_table_for_boyer_moore_string_search(needle, table);
    return string_has_substring_boyer_moore_with_table(haystack, needle, table, maybe_ret_idx);
}


static bool
string_has_substring_case_insensitive_boyer_moore_with_table(String haystack, String needle, BoyerMooreTableType table[256],
                                                             smem *maybe_ret_idx=0)
{
    if (needle.len <= 0) {
        if (maybe_ret_idx) *maybe_ret_idx = 0;
        return true;
    }
    if (needle.len > haystack.len) {
        if (maybe_ret_idx) *maybe_ret_idx = -1;
        return false;
    }
    if (needle.len == haystack.len) {
        if (strings_are_equal_case_insensitive(haystack, needle)) {
            if (maybe_ret_idx) *maybe_ret_idx = 0;
            return true;
        } else {
            if (maybe_ret_idx) *maybe_ret_idx = -1;
            return false;
        }
    }

    u8 needle_last = needle.data[needle.len-1];
    if (BETWEEN(needle_last, 'a', 'z'))
        needle_last &= ~32;
    for (smem k = needle.len-1; k < haystack.len; ) {
        u8 haystack_at_k = haystack.data[k];
        if (BETWEEN(haystack_at_k, 'a', 'z'))
            haystack_at_k &= ~32;
        if (haystack_at_k == needle_last) {
            String haystack_test = {};
            haystack_test.data = haystack.data + k - (needle.len-1);
            haystack_test.len = needle.len;
            ASSERT_SLOW(haystack_test.data >= haystack.data && haystack_test.data + haystack_test.len-1 <= haystack.data + haystack.len-1);
            if (strings_are_equal_case_insensitive(haystack_test, needle)) {
                if (maybe_ret_idx) *maybe_ret_idx = k - (needle.len-1);
                return true;
            }
        }
        k += table[haystack_at_k];
    }
    if (maybe_ret_idx) *maybe_ret_idx = -1;
    return false;
}

static void
generate_table_for_case_insensitive_boyer_moore_string_search(String needle, BoyerMooreTableType table[256])
{
    BoyerMooreTableType numeric_max = std::numeric_limits<BoyerMooreTableType>::max();
    BoyerMooreTableType to_fill = (BoyerMooreTableType)MINIMUM(numeric_max, MAXIMUM(1, needle.len));
    for (smem i = 0; i < 256; i++) {
        table[i] = to_fill;
    }
    for (smem i = 0; i < needle.len; i++) {
        u8 needle_at_i = (u8)needle.data[i];
        if (BETWEEN(needle_at_i, 'a', 'z'))
            needle_at_i &= ~32;
        table[needle_at_i] = (BoyerMooreTableType)MINIMUM(numeric_max, MAXIMUM(1, needle.len - 1 - i));
    }
}

static bool
string_has_substring_case_insensitive_boyer_moore(String haystack, String needle, smem *maybe_ret_idx=0)
{
    if (needle.len <= 0) {
        if (maybe_ret_idx) *maybe_ret_idx = 0;
        return true;
    }
    if (needle.len > haystack.len) {
        if (maybe_ret_idx) *maybe_ret_idx = -1;
        return false;
    }
    if (needle.len == haystack.len) {
        if (strings_are_equal_case_insensitive(haystack, needle)) {
            if (maybe_ret_idx) *maybe_ret_idx = 0;
            return true;
        } else {
            if (maybe_ret_idx) *maybe_ret_idx = -1;
            return false;
        }
    }

    BoyerMooreTableType table[256];
    generate_table_for_case_insensitive_boyer_moore_string_search(needle, table);
    return string_has_substring_case_insensitive_boyer_moore_with_table(haystack, needle, table, maybe_ret_idx);
}

// This function says the're equal if the difference between them is less than some margin.
inline static bool
f64_equal_margin(f64 a, f64 b, f64 margin)
{
    if (a == b)
        return true;
    bool result = false;

    // I want a to be the most negative of the two
    if (a < b) {
        result = a + margin >= b;
    } else {
        result = b + margin >= a;
    }

    return result;
}

inline static bool
f32_equal_margin(f32 a, f32 b, f32 margin)
{
    if (a == b)
        return true;
    bool result = false;

    // I want a to be the most negative of the two
    if (a < b) {
        result = a + margin >= b;
    } else {
        result = b + margin >= a;
    }

    return result;
}

inline static bool f32_less_than_and_not_eq_margin(f32 a, f32 b, f32 margin) { return a + margin < b; }
inline static bool f64_less_than_and_not_eq_margin(f64 a, f64 b, f64 margin) { return a + margin < b; }
inline static bool f32_less_than_or_eq_margin(f32 a, f32 b, f32 margin) { return a - margin < b; }
inline static bool f64_less_than_or_eq_margin(f64 a, f64 b, f64 margin) { return a - margin < b; }

inline static bool f32_greater_than_and_not_eq_margin(f32 a, f32 b, f32 margin) { return a - margin > b; }
inline static bool f64_greater_than_and_not_eq_margin(f64 a, f64 b, f64 margin) { return a - margin > b; }
inline static bool f32_greater_than_or_eq_margin(f32 a, f32 b, f32 margin) { return a + margin > b; }
inline static bool f64_greater_than_or_eq_margin(f64 a, f64 b, f64 margin) { return a + margin > b; }

// I don't know what I'm doing. I should do some research but for now this will
// have to do.
// TODO: Search for fnv-1a
// TODO: Remove the code that uses this.
#if 0
static u64
make_hash_from_simple_string(String buf)
{
    if (buf.data == NULL || buf.len == 0)
        return 0;

    u64 a = 66241858123891759; // 55.87 bits
    u64 result = 0;
    u8 *end = buf.data + buf.len;
    u8 *ptr;
    u64 i;
#if 1
    for (i=0, ptr=buf.data;
         ptr < end;
         i=(i+1)&7, ptr++) {
        result += (~((u64)(*ptr) << (8*i))) ^ a;
        // result += (*ptr << (8*i)) + 0;
        // result += a;
        // result += (*ptr << (8*i)) ^ a;
    }
#endif
    // add a length component
    result ^= ((u64)buf.len << 24);
    return result;
}
#endif

// Will return a substring to the filename containing just the extension. 
// Example: "filename.png" -> "png", "rc" -> ""
static String
get_file_extension(String filename)
{
    ASSERT(string_exists_and_is_not_empty(filename));
    String result = {};
    u8 *end = filename.data + filename.len;
    u8 *search = end - 1;
    while (search > filename.data && *search != '.') {
        search--;
    }
    if (*search == '.') {
        // found the dot
        result.data = search + 1;
        result.len = (smem)(end - search) - 1;
    } else {
        // didn't find the dot: return a null string
    }
    return result;
}

template<typename T>
static inline void
align_forward(T *value, smem alignment)
{
    // alignment must be a power of two
    T to_align = (*value) & ((T)(alignment)-1);
    if (to_align) {
        *value += (T)alignment - to_align;
    }
}

static inline s32
to_next_power_of_two(s32 value)
{
    auto v = value - 1;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v += 1;
    return v;
}
static inline s64
to_next_power_of_two(s64 value)
{
    auto v = value - 1;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v |= v >> 32;
    v += 1;
    return v;
}


// Returns a bool indicating whether float_seconds was negative.
struct GetMinutesAndSecondsResult {
    bool is_negative;
    s32 minutes;
    s32 seconds;
};

static GetMinutesAndSecondsResult
get_minutes_and_seconds(f64 float_seconds)
{
    bool negative = float_seconds < 0;
    f64 abs_float_seconds = negative ? -float_seconds : float_seconds;
    s32 minutes = (s32)(abs_float_seconds / 60.0);
    GetMinutesAndSecondsResult result = {
        .is_negative = negative,
        .minutes = minutes,
        .seconds = (s32)(abs_float_seconds - (f64)minutes*60.0),
    };

    return result;
}


// TODO:
static inline f32
ignore_some_bits_of_the_mantissa_f32(f32 f, u32 bits_to_ignore)
{
    ASSERT(bits_to_ignore < 23);
    // printf("before: %x\n", *(u32*)&f);
    f32 thing = (f32)((u64)1 << (23-bits_to_ignore));
    f32 result = roundf(f * thing) / thing;
    // printf("after:  %x\n", *(u32*)&result);
    return result;
}

static inline f64
ignore_some_bits_of_the_mantissa_f64(f64 f, u32 bits_to_ignore)
{
    ASSERT(bits_to_ignore < 52);
    // printf("before: %llx\n", *(unsigned long long*)&f);
    f64 thing = (f64)((u64)1 << (52-bits_to_ignore));
    f64 result = round(f * thing) / thing;
    // printf("after:  %llx\n", *(unsigned long long*)&result);
    return result;
}

static s32
utf8_decode(String str, smem offset, u32 *out_codepoint)
{
    // if there's a valid codepoint, it returns the size in bytes.
    // if there isn't a valid codepoint - for example if the first byte was 0xf8 (4 bytes) but
    // there wasn't 4 bytes in the string, or if the string was empty - it returns zero.
    ASSERT(ABC(offset, str.len));
    u8 *s = &str.data[offset];
    smem remaining_len = str.len - offset;
    if (remaining_len < 1) return 0;

    s32 size = 0;
    if (0xf0 == (0xf8 & s[0])) {
        // 4 byte utf8 codepoint
        size = 4;
        if (remaining_len < size) return 0;
        *out_codepoint = (u32)(((0x07 & s[0]) << 18) | ((0x3f & s[1]) << 12) | ((0x3f & s[2]) << 6) | (0x3f & s[3]));
    } else if (0xe0 == (0xf0 & s[0])) {
        // 3 byte utf8 codepoint
        size = 3;
        if (remaining_len < size) return 0;
        *out_codepoint = (u32)(((0x0f & s[0]) << 12) | ((0x3f & s[1]) << 6) | (0x3f & s[2]));
    } else if (0xc0 == (0xe0 & s[0])) {
        // 2 byte utf8 codepoint
        size = 2;
        if (remaining_len < size) return 0;
        *out_codepoint = (u32)(((0x1f & s[0]) << 6) | (0x3f & s[1]));
    } else {
        // 1 byte utf8 codepoint otherwise
        size = 1;
        *out_codepoint = s[0];
    }

    return size;
}

static s32
utf8_decode_backwards(String str, smem offset_backwards, u32 *out_codepoint)
{
    // 10000000 -> 0x80
    // 11000000 -> 0xc0
    // 11100000 -> 0xe0
    // 11110000 -> 0xf0
    // 11111000 -> 0xf8
    // 11111100 -> 0xfc
    // 11111110 -> 0xfe
    // if there is a valid codepoint at the end it returns that codepoint's size in bytes.
    // if there isn't, it returns zero.
    str.len -= offset_backwards;
    ASSERT(str.len >= 0);
    s32 result = 0;
    if (str.len >= 1) {
        // TODO: I don't know if I need to check for == 0x80. I didn't on utf8_decode.
        if ((str.data[str.len-1] & 0x80) == 0) {
            result = 1;
            u8 *s = &str.data[str.len-result];
            if (out_codepoint) *out_codepoint = s[0];
        } else if (str.len >= 2
                   && ((str.data[str.len-1] & 0xc0) == 0x80)
                   && ((str.data[str.len-2] & 0xe0) == 0xc0)) {
            result = 2;
            u8 *s = &str.data[str.len-result];
            if (out_codepoint) *out_codepoint = (u32)(((0x1f & s[0]) << 6) | (0x3f & s[1]));
        } else if (str.len >= 3
                   && ((str.data[str.len-1] & 0xc0) == 0x80)
                   && ((str.data[str.len-2] & 0xc0) == 0x80)
                   && ((str.data[str.len-3] & 0xf0) == 0xe0)) {
            result = 3;
            u8 *s = &str.data[str.len-result];
            if (out_codepoint) *out_codepoint = (u32)(((0x0f & s[0]) << 12) | ((0x3f & s[1]) << 6) | (0x3f & s[2]));
        } else if (str.len >= 4
                   && ((str.data[str.len-1] & 0xc0) == 0x80)
                   && ((str.data[str.len-2] & 0xc0) == 0x80)
                   && ((str.data[str.len-3] & 0xc0) == 0x80)
                   && ((str.data[str.len-4] & 0xf8) == 0xf0)) {
            result = 4;
            u8 *s = &str.data[str.len-result];
            if (out_codepoint) *out_codepoint = (u32)(((0x07 & s[0]) << 18) | ((0x3f & s[1]) << 12) | ((0x3f & s[2]) << 6) | (0x3f & s[3]));
        } else {
            result = 0;
        }
    }
    return result;
}

static inline String
string_offset(String str, smem n)
{
    String result = str;
    if (n <= result.len) {
        result.len  -= n;
        result.data += n;
    } else {
        ASSERT(false);
        // result.data += result.len;
        // result.len = 0;
    }
    return result;
}

// advances n bytes or until the end (whichever comes first).
static inline void
advance(String *str, smem n)
{
    if (n <= str->len) {
        str->len  -= n;
        str->data += n;
    } else {
        ASSERT(false);
        // str->data += str->len;
        // str->len = 0;
    }
}

// TODO: UNC paths can't have '/'!
static inline bool
is_path_separator(u8 ch)
{
#if BEAT_OS_WIN32
    return ch == '/' || ch == '\\';
#else
    return ch == '/';
#endif
}


// TODO: UNC paths can't have '/'!
// TODO: Treat paths starting with `\\?\` differently!
static String
get_base_dir(String str)
{
    // Examples;
    // "lkjlkj" -> "", 0
    // "asdf/zxcv" -> "asdf/", 1
    // "/thedir//zxcv" -> "/thedir//", 2
    // "/file" -> "/", 1
    // see get_base_dir_tests() for more
    // TODO: UNC Paths on windows
    smem dir_idx = -1;
    if (str.len > 0) {
        bool only_got_slashes = is_path_separator(str.data[str.len-1]);
        for (smem i = str.len-1; i >= 0; i--) {
            u8 ch = str.data[i];
            ASSERT(ch);
            if (!is_path_separator(ch)) {
                only_got_slashes = false;
            } else if (!only_got_slashes) {
                dir_idx = i;
                break;
            }
        }
        if (only_got_slashes) {
            ASSERT(dir_idx == -1);
            // only slashes in the path (root)
            dir_idx = str.len-1;
            beat_log("Tried to get base_dir of '/': '%.*s'\n", EXPAND_STR_FOR_PRINTF(str));
            // ASSERT(false);
        }
#if BEAT_OS_WIN32
        if (dir_idx == -1 && str.len >= 2) {
            bool is_c_colon = str.data[1] == ':' && BETWEEN(str.data[0]|32, 'a', 'z');
            if (str.len > 2) is_c_colon = is_c_colon && is_path_separator(str.data[2]);
            if (is_c_colon) {
                dir_idx = str.len-1;
                beat_log("Tried to get base_dir of 'X:/': '%.*s'\n", EXPAND_STR_FOR_PRINTF(str));
                // ASSERT(false);
            }
        }
#endif
        ASSERT(dir_idx+1 < str.len || only_got_slashes);
    }
    String result;
    result.data = str.data;
    result.len = dir_idx + 1; // even for (-1) it will work just fine.
    return result;
}

static String
get_base_filename(String str)
{
    String dir = get_base_dir(str);
    String result = string_offset(str, dir.len);
    // if result.len == 0, we got bad input. this input might be controlled by
    // the user so maybe we don't want to crash..
    ASSERT(result.len > 0);
    return result;
}

static void
get_base_dir_and_filename(String str, String *ret_dir, String *ret_filename)
{
    String dir = get_base_dir(str);
    String filename = string_offset(str, dir.len);
    ASSERT(filename.len > 0);
    // if filename.len == 0, we got bad input
    *ret_dir = dir;
    *ret_filename = filename;
}


#include "beat_memory.hh"

// join dir with filename, separated by a '/', into the arena.
// does not add '/' if dir already has one. (if dir had multiply slashes it would still do it)
// if dir is empty we return filename copied into the arena.
// TODO: Handle windows paths
static String
join_paths_nullter(MemoryArena *arena, String dir, String filename)
{
    String result = {};
    result.data = (u8 *)arena_push_size(arena, dir.len + filename.len + 1 + 1, arena_flags_nozero());
    result.len = 0;

    if (dir.len > 0) {
        xmemcpy(result.data+result.len, dir.data, dir.len);
        result.len += dir.len;
        if (!is_path_separator(result.data[result.len-1]))
            result.data[result.len++] = '/';
    }

    xmemcpy(result.data+result.len, filename.data, filename.len);
    result.len += filename.len;

    result.data[result.len] = 0;
    ASSERT(string_nullter_is_actually_the_same_len(result));

    return result;
}



static inline bool
is_whitespace(u8 ch)
{
    return ch == ' ' || ch == '\t';
}

/// trim whitespace left and right
static void
trim_whitespace(String *str)
{
    smem i = 0;
    while (i < str->len && is_whitespace(str->data[i])) {
        i++;
    }
    if (i) {
        str->data += i;
        str->len -= i;
    }

    i = 0;
    while (i < str->len && is_whitespace(str->data[str->len-(i+1)])) {
        i++;
    }
    if (i) {
        str->len -= i;
    }
}



// returns whether it found the char
static bool
split_by_char(String str, u8 ch, String *ret_left, String *maybe_ret_right)
{
    smem i = 0;
    while (i < str.len) {
        // TODO: This works with utf8 but does it work with shiftjis?
        if (str.data[i] == ch) {
            break;
        }
        i++;
    }
    smem lhs_len = i;
    bool found_ch;
    if (i < str.len) {
        // found ch
        ASSERT_SLOW(str.data[i] == ch);
        found_ch = true;
    } else {
        found_ch = false;
    }

    String lhs = {.len = lhs_len, .data = str.data};
    String rhs;
    if (i+1 < str.len) {
        rhs.len = str.len - (i+1);
        rhs.data = str.data + (i+1);
    } else {
        rhs.len = 0;
        rhs.data = str.data + str.len;
    }
    *ret_left = lhs;
    if (maybe_ret_right) *maybe_ret_right = rhs;
    return found_ch;
}

static bool
split_by_char_and_trim_whitespace(String str, u8 ch, String *ret_left, String *maybe_ret_right)
{
    String lhs, rhs;
    bool found_ch = split_by_char(str, ch, &lhs, &rhs);
    trim_whitespace(&lhs);
    trim_whitespace(&rhs);

    *ret_left = lhs;
    if (maybe_ret_right) *maybe_ret_right = rhs;
    return found_ch;
}

static bool
split_by_char_from_right(String str, u8 ch, String *ret_left, String *maybe_ret_right)
{
    smem i = str.len-1;
    while (i >= 0) {
        // TODO: This works with utf8 but does it work with shiftjis?
        if (str.data[i] == ch) {
            break;
        }
        i--;
    }
    smem lhs_len = i;
    bool found_ch;
    if (i < str.len) {
        // found ch
        ASSERT_SLOW(str.data[i] == ch);
        found_ch = true;
    } else {
        found_ch = false;
    }

    String lhs = {.len = lhs_len, .data = str.data};
    String rhs;
    if (i+1 < str.len) {
        rhs.len = str.len - (i+1);
        rhs.data = str.data + (i+1);
    } else {
        rhs.len = 0;
        rhs.data = str.data + str.len;
    }
    *ret_left = lhs;
    if (maybe_ret_right) *maybe_ret_right = rhs;
    return found_ch;
}

static bool
split_by_char_from_right_and_trim_whitespace(String str, u8 ch, String *ret_left, String *maybe_ret_right)
{
    String lhs, rhs;
    bool found_ch = split_by_char_from_right(str, ch, &lhs, &rhs);
    trim_whitespace(&lhs);
    trim_whitespace(&rhs);

    *ret_left = lhs;
    if (maybe_ret_right) *maybe_ret_right = rhs;
    return found_ch;
}



// returns whether it hit the target or not
// modifies curr and target
static bool
do_smooth_scroll(f32 *curr, f32 *target, f32 *ret_diff, f32 base_speed, f32 dt)
{
    /*
       q = target
       amount = q - y
       dy = amount * s * dt
       dy = (q - y)*s*dt
       1/(q - y) dy = dt
       1/s * INT(1/(q-y) dy) = dt
       substitute u = (q-y)
       du = -dy

       -1/s * INT(1/(u) du) = dt
       -1/s * ln(|u|) = t + C,  C is real
       -1/s * ln(|q-y|) = t + C
       ln(q-y) = -st + C,  C is real
       q-y = exp(-st + C)
       y = q - exp(-st + C)
       we want y(t=0) = 0, so C = ln(q)
       we get { y = q*(1 - exp(-st)) }

       time for % of the target
       with r in [0, 1]
       r = y/q = 1 - exp(-st)
       r = 1 - exp(-st)
       1 - r = exp(-st)
       ln(1-r) = -st
       t = -1/s * ln(1-r)
       time for r% = w(r) = -1/s * ln(1-r)
       if you double the speed, you halve the time it takes for r%
       as target-curr gets larger, it also gets faster.

       for bc:
       define w(r, s) {
        -1/s * l(1-r)
       }

       this table shows how much time it would take to get to 99% of the target.
       | s   | w(0.99, s)     | w(1- 2^-24, s)     | w(1- 2^-53, s)     |
       |-----+----------------+--------------------+--------------------|
       | 2   | 2.30  seconds  | 8.31  seconds      | 18.36 seconds      |
       | 4   | 1.15           | 4.15               | 9.18               |
       | 6   | 0.76           | 2.77               | 6.12               |
       | 8   | 0.57           | 2.07               | 4.59               |
       | 10  | 0.46           | 1.66               | 3.67               |
       */
    // I feel like there are some unnecessary ifs here.
    f32 diff = *target-*curr;
    if (diff != 0) {
        f32 target_abs_divided = fabsf(*target)*(1.0f/(1<<18));
        // if (fabsf(diff) > 0.0001f)
        // f32 themin = MAXIMUM(0.0001f, target_abs_divided);
        f32 themin = MAXIMUM(0.0000001f, target_abs_divided);
        themin = target_abs_divided;
        if (!true && ( true || FRAME_COUNT_ONCE_IN_A_WHILE)) {
            printf("target %.6f  curr %.6f  diff %.6f  target_abs_divided %.6f\n",
                   (f64)*target, (f64)*curr, (f64)diff, (f64)target_abs_divided);
        }
        if(!true || fabsf(diff) > themin)
        { // this check may be unnecessary for f32; it won't take too long to reach the machine epsilon if base_speed is decent. didn't test it for f64, but it will probably take more time.
            f32 new_curr = *curr + diff*base_speed*dt;
            f32 new_diff = *target - new_curr;
            if (new_diff != 0) {
                if (new_diff != diff) {
                    if (!((new_diff < 0) != (diff < 0))) { // verify that current didn't surpass the target // TODO i need to verify that this is possible
                        // dont do it if it went past the target (technically possible if base_speed * dt > 1)
                        // Technically, if I don't subtract the integer before setting target_abs_divided, I could lose precision on just the first time the function is called. For example:
                        // static f32 curr = 1000000;
                        // static f32 target = curr + whatever;
                        // do_smooth_scroll(&curr, &target, NULL, base_speed, dt)
                        // It would yield slightly a different result if curr was
                        // close to zero at the start. That's not really a problem.
                        // Just don't set curr to a very high value.
                        *curr = new_curr;
                        f32 to_decrease = (f32)(s32)*curr;
                        *curr -= to_decrease;
                        *target -= to_decrease;
                        if (ret_diff) *ret_diff = new_diff;
                        return false;
                    } else {
                        // NOTE: This can happen if `base_speed*dt > 1`.
                        // printf("NOT !((new_diff < 0) != (diff < 0))\n");
                    }
                } else {
                    // NOTE: This probably won't happen because of the check `fabsf(diff) > themin`.
                    // new_diff != diff: This only matter for the first call to the function. If `*curr` is so big that `diff*base_speed*dt` is smaller than the machine epsilon for `*curr`, this could happen.
                    // It also could happen without the check fabsf(diff) > themin
                    // printf("NOT new_diff != diff\n");
                }
            } else {
                // NOTE: This probably won't happen because of the check `fabsf(diff) > themin`.
                // new_diff != 0: If it hit the thing this time, we return true and reset curr and target.
                // printf("NOT new_diff != 0\n");
            }
        } else {
            // NOTE: This will happen most of the time, unless either speed or dt is incredibly high.
            // We do this because it would take too long to get to the machine epsilon, and we don't need that much precision 99.9999% is probably close enough. If it's not good enough I can pass that 99.9999% as an argument.
            // printf("NOT fabsf(diff) > themin\n");
        }
    } else {
        // NOTE: If we already hit the target.
        // printf("NOT diff != 0\n");
    }
    // *curr = *target;
    *curr = *target = 0;
    if (ret_diff) *ret_diff = 0;
    return true;
}



static bool
parse_s64(String str, s64 *ret_s64, u8 base, s64 min = S64_MIN, s64 max = S64_MAX)
{
    ASSERT_SLOW(base > 0);
    if (str.len <= 0)
        return false;

    // @CStringFutureProblem
    const smem max_len = 70;
    u8 buf[max_len];
    smem thelen = MINIMUM(str.len, max_len-1);
    xmemcpy(buf, str.data, thelen);
    buf[thelen] = 0;

    bool valid = true;
    char *end;
    s64 i = strtol((char *)str.data, &end, base);
    valid = end != (char *)str.data;
    if (!valid) {
        return false;
    }
    valid = end <= (char *)str.data + str.len;
    if (!valid) {
        return false;
    }
    valid = i >= (s64)min && i <= (s64)max;
    if (!valid)
        return false;
    *ret_s64 = (s64)i;
    return true;
}

static bool
parse_s32(String str, s32 *ret_s32, u8 base, s32 min = S32_MIN, s32 max = S32_MAX)
{
    ASSERT_SLOW(base > 0);
    s64 val_s64;
    if (parse_s64(str, &val_s64, base, min, max)) {
        *ret_s32 = (s32)val_s64;
        return true;
    }
    return false;
}

static bool
parse_f64(String str, f64 *ret_f64, f64 min = -F64_MAX, f64 max = F64_MAX)
{
    if (str.len <= 0)
        return false;

    // @CStringFutureProblem
    const smem max_len = 150;
    u8 buf[max_len];
    smem thelen = MINIMUM(str.len, max_len-1);
    xmemcpy(buf, str.data, thelen);
    buf[thelen] = 0;

    bool valid = true;
    char *end;
    f64 val = strtod((char *)str.data, &end);
    valid = end != (char *)str.data;
    if (!valid) {
        return false;
    }
    valid = end <= (char *)str.data + str.len;
    if (!valid) {
        return false;
    }
    valid = val >= (f64)min && val <= (f64)max;
    if (!valid)
        return false;
    *ret_f64 = (f64)val;
    return true;
}

static bool
parse_f32(String str, f32 *ret_f32, f32 min = -F32_MAX, f32 max = F32_MAX)
{
    f64 val_f64;
    if (parse_f64(str, &val_f64, (f64)min, (f64)max)) {
        *ret_f32 = (f32)val_f64;
        return true;
    }
    return false;
}


static s32
ring_buffer_eq_is_full_count_available_to_write(s32 buffer_size, s32 next_to_read, s32 next_to_write)
{
    // NOTE: If read == write, return zero. (it's full)
    ASSERT(next_to_read < buffer_size);
    ASSERT(next_to_write < buffer_size);
    if (next_to_write <= next_to_read) {
        return (next_to_read-1) - (next_to_write) + 1;
    } else {
        return (buffer_size - next_to_write) + next_to_read;
    }
}

static s32
ring_buffer_eq_is_empty_count_available_to_write(s32 buffer_size, s32 next_to_read, s32 next_to_write)
{
    // NOTE: If read == write, return buffer_size. (it's empty)
    ASSERT(next_to_read < buffer_size);
    ASSERT(next_to_write < buffer_size);
    if (next_to_write < next_to_read) {
        return (next_to_read-1) - (next_to_write) + 1;
    } else {
        return (buffer_size - next_to_write) + next_to_read;
    }
}




static u32
fnv1a(u8 const *ptr, smem len) {
    u32 const seed = 0x811C9DC5;
    u32 const prime = 0x01000193;
    u32 hash = seed;
    for (smem i = 0; i < len; i++) {
        hash = (ptr[i] ^ hash) * prime;
    }
    return hash;
}
