#ifndef BEAT_LINUXSDL_BEAT_H
#define BEAT_LINUXSDL_BEAT_H

// opendir, readdir, closedir, errno
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

// #include <SDL2/SDL_atomic.h>

struct DirectoryIter_OsSpecific {
    DIR *dir;

    struct dirent *dirent;
};


#include <SDL2/SDL_mutex.h>
struct WorkQueue_OsSpecific {
    SDL_semaphore *semaphore;
};

#endif
