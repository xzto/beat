#ifndef BEAT_PARSER_BMS_H
#define BEAT_PARSER_BMS_H


#define parser_bms_report_error_v(c, format, ...) do {                  \
    if ((c)->report_errors) {                                           \
        beat_log("[parser_bms] %.*s:%d: error: " format,                \
                (int)(c)->filename.len, (c)->filename.data, (c)->line1, \
                __VA_ARGS__);                                           \
    }                                                                   \
} while (0);

#define parser_bms_report_error_s(c, format) do {                        \
    if ((c)->report_errors) {                                            \
        beat_log("[parser_bms] %.*s:%d: error: " format,                 \
                (int)(c)->filename.len, (c)->filename.data, (c)->line1); \
    }                                                                    \
} while (0);


// i'll just use ParserOjn_Bpm. it's exactly the same thing
// struct ParserBms_Bpm {
//     f32 bps;
//     s32 measure;
//     f32 measure_position; // from 0 to 1
//     f64 start_seconds;
// };

struct ParserBms_ChannelData {
    s16 measure;
    s16 channel;
    union {
        NewArrayDynamic<u16> items;
        f32 f32_val; // for chan02
    };
};

struct ParserBms_Ctx {
    bool report_errors;
    String filename;
    String buf;
    u8 *buf_ptr;
    u8 *buf_end;
    s32 line1;

    // MemoryArena *final_arena;
    MemoryArena *temp_arena;

    // if there is a utf8 order mark at the start of the file we mark it as utf8.
    // if there is not we say it's shift-jis. we'll need to convert all the non ascii strings into utf8.
    bool is_buf_utf8;

    // For these, if Maybe.ok is false it means that either the value didn't exist in the file or we failed to parse it.

    // TODO: Document these better on the possible values and defaults.
    Maybe<s32> player;
    Maybe<String> genre_utf8;
    Maybe<String> title_utf8;
    Maybe<String> artist_utf8;
    Maybe<String> comment_utf8;
    Maybe<f32> header_bpm; // TODO: BPMxx too
    // Maybe<s32> playlevel; // Not always a number.
    Maybe<String> playlevel_str_utf8;
    Maybe<s32> rank; // VERY HARD / HARD / NORMAL / EASY (judgement)
    Maybe<s32> total; // ???
    Maybe<s32> difficulty; // EASY / NORMAL / HARD / HYPER / EX / ANOTHER / etc
    Maybe<String> stage_file;

    // Maybe<String> wav_files[36*36]; // ???
    // Maybe<String> bmp_files[36*36]; // ???

    // TODO: data_fields for each player.
    // a data_field is a #xxxyy:zzzzzz
    NewArrayDynamic<ParserBms_ChannelData> channels_data;
    // TODO: u16 exbpms_data[36*36];
    // TODO: bool exbpms_ok[36*36];
    struct {
        f32 bpms[36*36];
        bool ok[36*36];
    } exbpms;
};

#endif
