#include "beat_include.hh"

static void
parser_ojn_free_file(ParserOjnFile *ojn_file)
{
    // ASSERT(!"NOT IMPLEMENTED");
    // filename, buf, header
    // We don't allocate anything on parse_file_from_buffer. Everything inside
    // ojn_file points to the entire_file thing. Since we allocate that
    // outside of parse_file_from_buffer we don't free anything here. When we
    // change it to read the entire file inside parse_file_from_buffer we'll
}


static s32
parser_ojn_find_beat_idx_by_measure_frac(Slice<ParserOjn_Bpm> bpms, f64 measure_frac, s32 beat_idx_start,
                                         ParserOjn_Bpm *maybe_ret_bpm, bool can_go_reverse)
{
    ASSERT(beat_idx_start >= 0);
    ASSERT(beat_idx_start < bpms.len);
    s32 result = 0;
    bool found = false;
    bool go_reverse = false;

    // At the end of the function, if (found) we'll write to the result
    // beat_info this beat_info here
    ParserOjn_Bpm *bi = NULL;
    s32 i = 0;
    f64 thef64margin = 0.0001; // TODO: find a good value
    if (measure_frac < 0.0) {
        found = true;
        result = 0;
        bi = &bpms.data[0];
    } else {
        for (i = beat_idx_start;
             i < bpms.len;
             i++) {
            bi = &bpms.data[i];

            f64 bpm_measure_frac = (f64)bi->measure + bi->measure_position;
            if (f64_less_than_or_eq_margin(bpm_measure_frac, measure_frac, thef64margin)) {
                if (i == bpms.len-1 || f64_greater_than_and_not_eq_margin((f64)(bi+1)->measure + (bi+1)->measure_position, measure_frac, thef64margin)) {
                    // Beat started before 'measure_frac'. it is active
                    // What we want is this one.
                    found = true;
                    result = i;
                    break;
                } else {
                    // Beat started before 'beat'. it is not active.
                    // What we want is after this one.
                    continue;
                }
            } else {
                // Beat started after 'beat', is not active.
                // What we want is before this one.

                // If this hits, there either was an error on the if() or
                // beat_idx_start is too big.
                // If beat_idx_start is too big, let's make another loop
                // decrement i until we find it.
                // If this assert fails, the comparison in the if() msut be the problem
                ASSERT(i == beat_idx_start);
                go_reverse = true;
                break;
            }
        }
        if (go_reverse) {
            for (i--; i >= 0; i--) {
                bi = &bpms.data[i];
                f64 bpm_measure_frac = (f64)bi->measure + bi->measure_position;
                if (f64_less_than_or_eq_margin(bpm_measure_frac, measure_frac, thef64margin)) {
                    if (i == bpms.len-1 || f64_greater_than_and_not_eq_margin((f64)(bi+1)->measure + (bi+1)->measure_position, measure_frac, thef64margin)) {
                        // Beat started before 'beat', is active
                        // What we want is this one.
                        found = true;
                        result = i;
                        break;
                    } else {
                        // Beat started before 'beat', is not active.
                        // What we want is after this one.
                        ASSERT(false);
                    }
                    ASSERT(found);
                } else {
                    // Beat started after 'beat', is not active.
                    // What we want is before this one.
                    continue;
                }
            }
        }
    }
    // if we ever hit this, the code that called this functions has en error
    ASSERT(can_go_reverse || !go_reverse);
    // if found == false, we hit the end, just confirming it
    ASSERT(found);
    ASSERT(bi != NULL);
    ASSERT(bi == &bpms.data[result]);

    if (maybe_ret_bpm != NULL) {
        *maybe_ret_bpm = *bi;
    }
    f64 bpm_measure_frac = (f64)bi->measure + bi->measure_position;
    ASSERT(f64_less_than_or_eq_margin(bpm_measure_frac, measure_frac, thef64margin));

    return result;
}

static s32 TEST_section_end[PARSER_OJN_NUM_DIFFS];

// This is a mess right now.
static bool
parser_ojn_parse_file_from_buffer(MemoryArena *final_arena, MemoryArena *temp_arena,
                                  String filename, String buf, bool report_errors,
                                  ParserOjnFile *ret_ojn_file)
{
    ASSERT(final_arena);
    ASSERT(temp_arena);

    ASSERT(string_exists_and_is_not_empty(filename));
    // I don't really need a ctx for ojn...
    ParserOjnCtx cntx = {};
    cntx.report_errors = report_errors;
    cntx.filename = filename;
    ASSERT(cntx.filename.data[cntx.filename.len] == 0); // TODO: Don't read out of a bad region pls
    cntx.buf = buf;
    cntx.buf_ptr = buf.data;
    cntx.buf_end = buf.data + buf.len;

    zero_struct(ret_ojn_file);

    bool valid = true;

    s32 total_size;
    bool has_at_least_one_valid_diff;


    ParserOjnFileHeader *header = NULL;
    valid = cntx.buf.len >= SSIZEOF_VAL(header);
    if (!valid) {
        parser_ojn_report_error_s(&cntx, "File too small to fit the header\n");
        goto rage_quit;
    }
    header = (ParserOjnFileHeader *)cntx.buf.data;
    valid = header->magic_ojn == PARSER_OJN_MAGIC_OJN;
    if (!valid) {
        parser_ojn_report_error_s(&cntx, "ojn magic doesn't match\n");
        goto rage_quit;
    }


    valid = header->bpm > 0.001f;
    if (!valid) {
        parser_ojn_report_error_s(&cntx, "header bmp is zero\n");
        goto rage_quit;
    }
    // Do more checks.
    ASSERT(1 || !"NOT IMPLEMENTED: Do more checks");

    // Now verify the file is big enough to fit all the blocks and everything.
    // Could do it in the middle of parsing the file too. Maybe I'll merge it later.

    zero_static_carray(TEST_section_end);
    // Make sure the file didn't get cut in the middle.
    total_size = 0;
    has_at_least_one_valid_diff = false;
    for (s32 idx_diff = 0; idx_diff < PARSER_OJN_NUM_DIFFS; idx_diff++) {
        // ret_ojn_file->bpms[idx_diff].arena = final_arena;

        // Loop through every block in every diff and get block->event_count and make sure the
        // file is big enough to fit it
        s32 block_count = header->block_count[idx_diff];
        s32 block_header_offset = header->note_section_offset[idx_diff];
        // If it's zero, we don't have that difficulty.
        if (block_header_offset == 0)
            continue;
        ParserOjnBlockHeader *block_header = NULL;
        valid = block_header_offset + SSIZEOF_VAL(block_header) <= cntx.buf.len;
        if (!valid)  {
            parser_ojn_report_error_s(&cntx, "File end abruptly firstcase\n");
            goto rage_quit;
        }
        s32 total_block_size = 0;

        // TODO: 99.99% of the files will not have this problem, and that will make the parser slower.
        // TODO: Put this check when I actually parse the file so that it is not so slow.
        block_header = (ParserOjnBlockHeader *)((u8 *)header + block_header_offset + total_block_size);
        // How much space this block used.
        // s32 num_total_blocks_count = 0;
        // s32 num_total_events = 0;
        for (s32 idx_block = 0; idx_block < block_count; idx_block++) {
            total_block_size += SSIZEOF_VAL(block_header);
            s32 num_events = block_header->event_count;
            // num_total_events += num_events;
            total_block_size += num_events * ssizeof(ParserOjnEvent);
            valid = (block_header_offset + total_block_size <= cntx.buf.len);
            if (!valid) {
                parser_ojn_report_error_s(&cntx, "File ends abruptly\n");
                goto rage_quit;
            }
            block_header = (ParserOjnBlockHeader *)((u8 *)header + block_header_offset + total_block_size);

            // num_total_blocks_count++;
        }

#if 0
        // s32 block_count = header->block_count[idx_diff];
        s32 event_count = header->event_count[idx_diff];
        s32 note_count = header->note_count[idx_diff];
        s32 measure_count = header->measure_count[idx_diff];
        s32 section_start = header->note_section_offset[idx_diff];
        s32 section_end = (section_start
                           + block_count * (s32)ssizeof(ParserOjnBlockHeader)
                           + event_count * (s32)ssizeof(ParserOjnEvent));
        // printf("end: %d\n", section_end);
        valid = section_end <= cntx.buf.len;
        if (!valid) {
            parser_ojn_report_error_s(&cntx, "File ends abruptly\n");
            goto rage_quit;
        }
        TEST_section_end[idx_diff] = section_end;
        // printf("diff %d from header: blocks %d events %d, section_end %d\n",
        //        idx_diff, block_count, note_count+event_count+measure_count, section_end-section_start);
        // printf("diff %d calculated: blocks %d events %d, section_end %d\n",
        //        idx_diff, num_total_blocks_count, num_total_events, total_block_size);
#endif
        has_at_least_one_valid_diff = true;
    }
    valid = has_at_least_one_valid_diff;
    if (!valid) {
        parser_ojn_report_error_s(&cntx, "File has empty difficulties\n");
        goto rage_quit;
    }

    ret_ojn_file->filename = cntx.filename;
    ret_ojn_file->buf = buf;
    ret_ojn_file->header = header;

    // hack
    ret_ojn_file->ctx = cntx;

    return true;
rage_quit:
    // TODO: Free everything I allocated in this function.
    return false;
}

// parser_ojn_free_file(ojn_file) at the end
static bool
parser_ojn_to_fileset(MemoryArena *final_arena, MemoryArena *temp_arena,
                      ParserOjnFile *ojn_file, Fileset *ret_fileset)
{
    // For now there are some leaks on failure.
    // If a difficulty has an error, we discard the entire fileset instead of just that difficulty. Maybe I'll change this later.

    ASSERT(final_arena);
    ASSERT(temp_arena);

    zero_struct(ret_fileset);
    bool valid = true;

    TempMemory temp_mem = begin_temp_memory(temp_arena);
    TempMemory temp_for_final_if_error = begin_temp_memory(final_arena);

    ParserOjnCtx *ctx = &ojn_file->ctx;

    // stupid c++....
    Fileset fileset = {};
    Metadata temp_metadata;
    NewArrayDynamic<NoteTimestamp> ts_buf = {};
    NewArrayDynamic<NoteType> raw_notes_buf = {};
    ParserOjnFileHeader *header;
    bool *inside_a_hold;

    const String the_table_for_diffname[3] = {
        "Easy"_s,
        "Normal"_s,
        "Hard"_s,
    };

    // compiler warning unused label - remove later
    valid = true;
    if (!valid) goto rage_quit;

    header = ojn_file->header;

    // set artist, title, genre, etc
    temp_metadata = {};
    temp_metadata.artist = cstr_to_string_max_size(header->artist, ssizeof(header->artist));
    temp_metadata.title = cstr_to_string_max_size(header->title, ssizeof(header->title));
    temp_metadata.creator = cstr_to_string_max_size(header->notecharter, ssizeof(header->notecharter));
    fileset.filepath = copy_string(ojn_file->filename); // @Malloc
    fileset.parser_type = ParserType_OJN;

    fileset.diffs = xmalloc_struct_count(Diff, PARSER_OJN_NUM_DIFFS);

    ts_buf = {};
    raw_notes_buf = {};
    ts_buf.arena = temp_arena;
    ts_buf.reserve(4096/SSIZEOF_VAL(ts_buf.data));
    raw_notes_buf.arena = temp_arena;
    raw_notes_buf.reserve(4*4096);
    inside_a_hold = arena_push_struct_count(temp_arena, bool, 7, arena_flags_zero());

    // make the diffs
    for (s32 idx_diff = 0;
         idx_diff < PARSER_OJN_NUM_DIFFS;
         // reset temp buffers at the end of the diff
         idx_diff++, ts_buf.len = 0, raw_notes_buf.len = 0) {
        zero_struct_count(inside_a_hold, 7);
        Diff diff = {};
        // diff.parent_fileset = ret_fileset;
        diff.notes.keymode = Keymode_7k;
        diff.notes.columns = 7;
        diff.notes.pitch = NOTES_GET_PITCH_SIZE(diff.notes.columns);
        diff.notes.timestamps = ts_buf.data;
        diff.notes.raw_notes = raw_notes_buf.data;
        diff.filepath = copy_string(fileset.filepath); // @Malloc
        copy_metadata_malloc(&temp_metadata, &diff.metadata); // @Malloc
        diff.metadata.diff_idx = idx_diff;
        {
            u8 buf[512];
            String tmp = {};
            tmp.data = buf;
            tmp.len = snprintf((char *)buf, sizeof(buf), "%.*s o2 %d",
                               EXPAND_STR_FOR_PRINTF(the_table_for_diffname[idx_diff]),
                               header->diff_level[idx_diff]);
            diff.metadata.diff_name = copy_string(tmp);
        }



        // Skip diff if empty
        s32 note_count = header->note_count[idx_diff];
        if (note_count == 0) // @Leak
            continue;

        NewArrayDynamic<ParserOjn_Bpm> *thebpms = &ojn_file->bpms[idx_diff];
        // This is a mess right now.
        thebpms->arena = temp_arena;

        s32 block_count = header->block_count[idx_diff];
        s32 block_header_offset = header->note_section_offset[idx_diff];
        if (block_header_offset == 0) continue; // @Leak
        s32 total_blocks_size = 0;
        ParserOjnBlockHeader *block = (ParserOjnBlockHeader *)((u8 *)header + block_header_offset);
        // TODO: Make sure blocks are sorted by measure.
        // Sort order: measure, channel (fractional_measure, bpm, notes, autoplaysamples)
        // If they're not sorted, just discard the difficulty.

        // printf("-newdiff: %d\n", idx_diff);
        // printf("header: bpm: %f, time: %f, measure: %d\n",
        //        (f64)header->bpm,
        //        (f64)header->duration_seconds[idx_diff],
        //        header->measure_count[idx_diff]);

        s32 curr_measure = 0;

        s32 last_note_idx = 0;
        // -123 so that things will work on the first loop
        // s32 last_measure = -123;
        f64 measure_start_seconds = 0.0;
        s32 last_bidx_used = 0;
        ParserOjn_Bpm *measure_start_bpm = NULL;

        if (!thebpms->len) {
            ParserOjn_Bpm thebpm = {};
            thebpm.bps = header->bpm / 60.0f;
            measure_start_bpm = thebpms->push(thebpm);
        }

        for (s32 idx_block = 0; idx_block < block_count; idx_block++) {
            // @Bug: potential buffer overflow if file is corrupted and num_events is very large. Need to check if it fits inside the buffer.
            s32 this_block_size = SSIZEOF_VAL(block);
            s32 num_events = block->event_count;
            ASSERT((u8*)block < (u8*)ojn_file->buf.data + ojn_file->buf.len);

            // printf("Measure %d delta %d   ", block->measure, block->measure-curr_measure);
            // printf("%d %d %d\n", block->measure, block->channel, block->event_count);
            if (block->measure != curr_measure) {
                valid = (block->measure > curr_measure);
                if (!valid) {
                    parser_ojn_report_error_s(ctx, "parser_ojn_to_fileset: block measures aren't sorted\n");
                    goto rage_quit;
                }
                curr_measure = block->measure;
            }

            s32 measure_start_bidx = -1;
            if (thebpms->len > 0) {
                f64 measure_frac = (f64)block->measure;
                measure_start_bidx = parser_ojn_find_beat_idx_by_measure_frac(*thebpms, measure_frac, last_bidx_used,
                                                                              NULL, true);
                last_bidx_used = measure_start_bidx;
                measure_start_bpm = &thebpms->data[measure_start_bidx];

                f64 bpm_measure_frac = (f64)measure_start_bpm->measure + measure_start_bpm->measure_position;
                f64 delta_measure_frac_from_bpm_start = measure_frac - bpm_measure_frac;
                ASSERT(measure_start_bpm->bps != 0.0f);
                f64 seconds_from_bpm_start = 4*delta_measure_frac_from_bpm_start / (f64)measure_start_bpm->bps;
                f64 seconds_from_song_start = measure_start_bpm->start_seconds + seconds_from_bpm_start;
                measure_start_seconds = seconds_from_song_start;
            }

            if (block->channel >= PARSER_OJN_CHANNEL_NOTE_1 && block->channel <= PARSER_OJN_CHANNEL_NOTE_7) {
                ASSERT(thebpms->len > 0);
                // if (!(ojn_file->bpms[idx_diff].len > 0)) continue;
                // printf("Channel NOTE_%d count %d: ", block->channel - PARSER_OJN_CHANNEL_NOTE_1 + 1, num_events);
                // printf("\n");
                s32 column = block->channel - PARSER_OJN_CHANNEL_NOTE_1;
                // printf("note: %d\n", num_events);

                // TODO: proper time signature instead of multiply by 4

                // @Bug: potential buffer overflow if file is corrupted and num_events is very large. Need to check if it fits inside the buffer.
                f64 last_seconds_from_song_start = -F64_MAX;
                s32 this_last_bidx = -123;
                f64 this_last_measure_frac = -F64_MAX;
                s32 this_last_note_idx = -123;
                for (s32 idx_event = 0; idx_event < num_events; idx_event++) {
                    // Get the note and column
                    // Make a function add_note_to_temp_diff(tempdiff, note, column, timestamp).
                    // The function will search for that timestamp. if it exists set that column to that thing.
                    // If the timestamp does not exist, add a row with that timestamp and add the note to that column.

                    ParserOjnEvent *ev = (ParserOjnEvent *)((u8 *)block + this_block_size + idx_event*SSIZEOF_VAL(ev));


                    // if (idx_event == 17) {
                    //     printf("ev 17: col %d note %d val %d\n", column, ev->note.note_type, ev->note.value);
                    // }
                    if (ev->note.value == 0) {
                        // printf("0");
                        continue;
                    }

                    f64 measure_position = (f64)idx_event / (f64)num_events;
                    f64 measure_frac = (f64)block->measure + measure_position;
                    ASSERT(measure_frac > this_last_measure_frac);
                    this_last_measure_frac = measure_frac;

                    ParserOjn_Bpm this_thebpm;
                    s32 this_bidx = parser_ojn_find_beat_idx_by_measure_frac(*thebpms, measure_frac, last_bidx_used,
                                                                             &this_thebpm, true);
                    last_bidx_used = this_bidx;
                    ASSERT(this_bidx >= this_last_bidx);
                    this_last_bidx = this_bidx;;

                    f64 bpm_measure_frac = (f64)this_thebpm.measure + (f64)this_thebpm.measure_position;
                    f64 delta_measure_frac_from_bpm_start = measure_frac - bpm_measure_frac;
                    ASSERT(this_thebpm.bps != 0.0f);
                    f64 seconds_from_bpm_start = 4*delta_measure_frac_from_bpm_start / (f64)this_thebpm.bps;
                    f64 seconds_from_song_start = this_thebpm.start_seconds + seconds_from_bpm_start;
                    ASSERT(seconds_from_song_start > last_seconds_from_song_start);
                    last_seconds_from_song_start = seconds_from_song_start;
                    // printf("|%.2f-", seconds_from_song_start);


                    u32 ojn_note_type = ev->note.note_type;
                    NoteType new_note_type;
                    if (ojn_note_type == PARSER_OJN_NOTE_TYPE_OGG_SAMPLE) {
                        // Ignore it for now.
                        // printf("Got NOTE_TYPE_OGG_SAMPLE\n");
                        continue;
                    }
                    // printf("%c", ojn_note_type + '0');
                    switch (ojn_note_type) {
                    case PARSER_OJN_NOTE_TYPE_NORMAL: {
                        new_note_type = NOTE_TAP;
                        valid = !inside_a_hold[column];
                        if (!valid) {
                            parser_ojn_report_error_s(ctx, "parser_ojn_to_fileset: got a TAP inside a LN.\n");
                            goto rage_quit;
                        }
                    } break;
                    case PARSER_OJN_NOTE_TYPE_LN_START: {
                        new_note_type = NOTE_HOLD;
                        // printf("LN Start col %d ev %d\n", column, idx_event);
                        valid = !inside_a_hold[column];
                        if (!valid) {
                            parser_ojn_report_error_s(ctx, "parser_ojn_to_fileset: got LN_START after a LN_START.\n");
                            goto rage_quit;
                        }
                        inside_a_hold[column] = true;
                    } break;
                    case PARSER_OJN_NOTE_TYPE_LN_END: {
                        new_note_type = NOTE_HOLD_END;
                        // printf("LN End col %d ev %d\n", column, idx_event);
                        valid = inside_a_hold[column];
                        if (!valid) {
                            parser_ojn_report_error_s(ctx, "parser_ojn_to_fileset: got LN_END without a LN_START.\n");
                            goto rage_quit;
                        }
                        inside_a_hold[column] = false;
                    } break;
                    default: {
                        parser_ojn_report_error_s(ctx, "Unknown note type\n");
                        goto rage_quit;
                    } break;
                    }
                    // printf("%c", new_note_type + '0');
                    // @IgnoreSomeBitsOfTheMantissa
                    s32 new_note_idx = parser_utils_add_note_to_diff(&diff.notes, &ts_buf, &raw_notes_buf,
                                                                     new_note_type, column, seconds_from_song_start, last_note_idx);
                    ASSERT(new_note_idx > this_last_note_idx);
                    last_note_idx = new_note_idx;
                    this_last_note_idx = new_note_idx;
                }
                // printf("\n");
            } else if (block->channel == PARSER_OJN_CHANNEL_TIME_SIGNATURE) {
                parser_ojn_report_error_s(ctx, "TIME_SIGNATURE is not supported yet.\n");
                goto rage_quit;
                // printf("Channel TIME_SIGNATURE count %d\n", num_events);
                // I don't know what this does.
                // printf("num bpms: %d\n", num_events);
                // @Bug: potential buffer overflow if file is corrupted and num_events is very large. Need to check if it fits inside the buffer.
                for (s32 idx_event = 0; idx_event < num_events; idx_event++) {
                    // Get the note and column
                    // Make a function add_note_to_diff(tempdiff, note, column, timestamp).
                    // The function will search for that timestamp. if it exists set that column to that thing.
                    // If the timestamp does not exist, add a row with that timestamp and add the note to that column.
                    ParserOjnEvent *ev = (ParserOjnEvent *)((u8 *)block + this_block_size + idx_event*SSIZEOF_VAL(ev));
                    printf("%f, ", (f64)ev->time_signature.value);
                    // @IgnoreSomeBitsOfTheMantissa
                }
                // printf("\n");

                ASSERT(!"NOT IMPLEMENTED");
            } else if (block->channel == PARSER_OJN_CHANNEL_BPM_CHANGE) {
                // printf("Channel BPM_CHANGE count %d\n", num_events);
                // @IgnoreSomeBitsOfTheMantissa

                f64 seconds_from_measure_start = 0.0;
                ParserOjn_Bpm *last_thebpm = NULL;
                // @Bug: potential buffer overflow if file is corrupted and num_events is very large. Need to check if it fits inside the buffer.
                ParserOjn_Bpm something_for_the_thing = {
                    .bps = thebpms->data[thebpms->len-1].bps,
                    .measure = block->measure,
                    .measure_position = 0,
                    .start_seconds = measure_start_seconds,
                };

                bool all_zeros = true;
                for (s32 idx_event = 0; idx_event < num_events; idx_event++) {
                    ParserOjnEvent *ev = (ParserOjnEvent *)((u8 *)block + this_block_size + idx_event*SSIZEOF_VAL(ev));
                    // ev->bpm_change.bpm @IgnoreSomeBitsOfTheMantissa
                    f32 bpm = ev->bpm_change.bpm;
                    if (bpm == 0.0f) {
                        if (idx_event == 0) {
                            // a hack
                            last_thebpm = &something_for_the_thing;
                        }
                        continue;
                    }
                    valid = bpm > 0.0001f;
                    if (!valid) {
                        parser_ojn_report_error_s(ctx, "parser_ojn_to_fileset: got bpm equal to zero.\n");
                        goto rage_quit;
                    }
                    all_zeros = false;

                    f32 bps = bpm / 60.0f;
                    f64 measure_position = (f64)idx_event / (f64)num_events;
                    f64 measure_frac = (f64)block->measure + measure_position;
                    if (last_thebpm) {
                        f64 last_thebpm_measure_frac = (f64)last_thebpm->measure + last_thebpm->measure_position;

                        f64 delta_measure_frac_from_bpm_start = measure_frac - last_thebpm_measure_frac;
                        ASSERT(delta_measure_frac_from_bpm_start > 0);
                        // TODO: proper time signature instead of multiply by 4
                        ASSERT(last_thebpm->bps > 0.0f);
                        f64 seconds_from_last_bpm_start = 4.0*delta_measure_frac_from_bpm_start / (f64)last_thebpm->bps;
                        f64 seconds_from_start = seconds_from_last_bpm_start + last_thebpm->start_seconds;
                        seconds_from_measure_start = seconds_from_start - measure_start_seconds;
                        // seconds_from_measure_start += 4.0*delta_measure_frac / (f64)last_thebpm->bps;
                    }

                    ParserOjn_Bpm thebpm = {};
                    thebpm.bps = bps;
                    thebpm.measure = block->measure;
                    thebpm.measure_position = measure_position;
                    thebpm.start_seconds = measure_start_seconds + seconds_from_measure_start; // @IgnoreSomeBitsOfTheMantissa
                    // If this is the first bpm at measure_frac 0.0, just overwrite the default bpm.
                    if (measure_frac == 0) {
                        ASSERT(thebpms->len == 1);
                        last_thebpm = &thebpms->data[thebpms->len-1];
                        *last_thebpm = thebpm;
                    } else {
                        last_thebpm = thebpms->push(thebpm);
                    }
                    // printf("bpm %.2f, start_seconds %.2f\n", (f64)bpm, thebpm.start_seconds);
                }
                valid = !all_zeros;
                if (!valid) {
                    parser_ojn_report_error_s(ctx, "parser_ojn_to_fileset: got bpms with all zeros\n");
                    goto rage_quit;
                }
                // printf("\n");
            } else if (block->channel >= PARSER_OJN_CHANNEL_AUTO_PLAY_SAMPLES_START) {
                // ASSERT(num_events == 1);
                ASSERT(1 || !"NOT IMPLEMENTED");
                // printf("Channel AUTO_PLAY_SAMPLES_START count %d\n", num_events);
            } else {
                // TODO: discard the difficulty, not the whole file
                // goto end_blocks;
                parser_ojn_report_error_v(ctx, "Invalid channel: %d\n", block->channel);
                goto rage_quit;
            }
            this_block_size += num_events * ssizeof(ParserOjnEvent);
            total_blocks_size += this_block_size;
            block = (ParserOjnBlockHeader *)((u8 *)block + this_block_size);
        }
        // end_blocks:
        // if (!valid) { free_things(); continue; }
        bool valid_diff = true;
        if (!valid_diff) {
            // Forget about the rows and things because
            diff.notes.rows_count = 0;
            diff.notes.raw_notes = NULL;
            diff.notes.timestamps = NULL;
            // nothing to free // free_diff(diff);
            // TODO: Free metadata
            continue;
        }

        // printf("curr:            time: %f, measure: %d\n", measure_start_seconds, curr_measure);

        // Done, now copy the notes and timestamps into the diff
        s32 rows_count = diff.notes.rows_count;
        s32 pitch = diff.notes.pitch;
        Diff *newdiff = &fileset.diffs[fileset.diffs_count++];
        *newdiff = diff;
        newdiff->notes.raw_notes = xmalloc_struct_count(NoteType, rows_count * pitch);
        xmemcpy_struct_count(newdiff->notes.raw_notes, diff.notes.raw_notes, NoteType, rows_count * pitch);
        newdiff->notes.timestamps = xmalloc_struct_count(NoteTimestamp, rows_count);
        xmemcpy_struct_count(newdiff->notes.timestamps, diff.notes.timestamps, NoteTimestamp, rows_count);


        newdiff->notes.timings.bpms_count = (s32)thebpms->len;
        newdiff->notes.timings.bpms = xmalloc_struct_count(BeatInfo, newdiff->notes.timings.bpms_count, xmalloc_flags_zero());
        for (s32 idx_bpm = 0; idx_bpm < thebpms->len; idx_bpm++) {
            ParserOjn_Bpm *thebpm = &thebpms->data[idx_bpm];
            BeatInfo *new_bi = &newdiff->notes.timings.bpms[idx_bpm];
            new_bi->bps = thebpm->bps;
            new_bi->ts_start = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(thebpm->start_seconds)};
            if (idx_bpm + 1 < thebpms->len) {
                // @IgnoreSomeBitsOfTheMantissa
                new_bi->ts_end = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL((thebpm+1)->start_seconds)};
            } else {
                new_bi->ts_end = NOTE_MAX_TIMESTAMP;
            }
            ASSERT(new_bi->ts_start.value >= 0);
            ASSERT(new_bi->ts_end > new_bi->ts_start);
        }



        // BeatInfo bi = {};
        // bi.bps = header->bpm / 60.0f;
        // bi.ts_start = 0;
        // bi.ts_end = NOTE_MAX_TIMESTAMP;
        // newdiff->notes.timings.bpms[0] = bi;

        // Provisional
        newdiff->notes.last_hold_is_released = xmalloc_struct_count(b8, newdiff->notes.columns, xmalloc_flags_zero());

#if 0
        newdiff->notes.hold_pairs_count = xmalloc(newdiff->notes.columns*SSIZEOF_VAL(newdiff->notes.hold_pairs_count));
        zero_struct_count(newdiff->notes.hold_pairs_count, newdiff->notes.columns);

        newdiff->notes.hold_idx_start = xmalloc(newdiff->notes.columns*SSIZEOF_VAL(newdiff->notes.hold_idx_start));
        zero_struct_count(newdiff->notes.hold_idx_start, newdiff->notes.columns);

        newdiff->notes.hold_idx_end = xmalloc(newdiff->notes.columns*SSIZEOF_VAL(newdiff->notes.hold_idx_end));
        zero_struct_count(newdiff->notes.hold_idx_end, newdiff->notes.columns);
#endif

        // print_diff(newdiff);

        zero_struct_count(inside_a_hold, 7);
        for (s32 idx_row = 0; idx_row < newdiff->notes.rows_count; idx_row++) {
            NoteType *row = newdiff->notes.raw_notes + idx_row*newdiff->notes.pitch;
            for (s32 idx_col = 0; idx_col < 7; idx_col++) {
                NoteType note = NOTES_GET_COLUMN(row, idx_col);
                if (note == NOTE_HOLD) {
                    ASSERT(!inside_a_hold[idx_col]);
                    inside_a_hold[idx_col] = true;
                } else if (note == NOTE_HOLD_END) {
                    ASSERT(inside_a_hold[idx_col]);
                    inside_a_hold[idx_col] = false;
                } else if (note == NOTE_TAP || note == NOTE_MINE) {
                    ASSERT(!inside_a_hold[idx_col]);
                }
            }
        }

        // TODO: Make sure everything sorted one last time.
    }

    // for all the diffs, create hold_idx_start and stuff

    ts_buf.free();
    raw_notes_buf.free();
    // parser_ojn_free_file(ojn_file);

    end_temp_memory_dont_free(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);

    *ret_fileset = fileset;
    return true;
rage_quit:
    // Free everything (not complete)
    // TODO: Free metadata for all diffs
    ts_buf.free();
    raw_notes_buf.free();
    end_temp_memory(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    free_fileset(&fileset);
    return false;
}

static bool
parser_ojn_fileset_from_buffer(MemoryArena *final_arena, MemoryArena *temp_arena,
                               String filename, String buf, bool report_errors,
                               Fileset *ret_fileset)
{
    ASSERT(temp_arena->base);
    TempMemory temp_mem = begin_temp_memory(temp_arena);

    ParserOjnFile ojn_file;
    bool valid = true;

    // TODO: Check how much memory the thing uses compared to the file size and come up with a good constant for this.
    // TODO: Don't read the whole file.
    smem estimated_max_size = MAXIMUM(buf.len, KILOBYTES(256));
    MemoryArena ojn_final_arena = sub_arena(temp_arena, estimated_max_size);
    MemoryArena *ojn_temp_arena = temp_arena;

    valid = parser_ojn_parse_file_from_buffer(&ojn_final_arena, ojn_temp_arena, filename, buf, report_errors, &ojn_file);
    if (!valid) {
        end_temp_memory(&temp_mem);
        return false;
    }

    valid = parser_ojn_to_fileset(final_arena, temp_arena, &ojn_file, ret_fileset);
    parser_ojn_free_file(&ojn_file);
    if (!valid) {
        end_temp_memory(&temp_mem);
        return false;
    }

    end_temp_memory(&temp_mem);
    return true;
}

static bool
parser_ojn_fileset_from_file(MemoryArena *final_arena, MemoryArena *temp_arena,
                             String filename, bool report_errors, Fileset *ret_fileset)
{
    ASSERT(temp_arena->base);
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    defer { end_temp_memory(&temp_mem); };
    bool valid;

    ASSERT(filename.data[filename.len] == 0);
    DebugOpenFileHandle handle = debug_open_file((char *)filename.data);
    if (handle.error) {
        return false;
    }
    defer { debug_close_file(&handle); };

    smem file_pos;
    void *ptr;
    smem bytes_read = debug_read_file_allocate(&handle, 0, ssizeof(ParserOjnFileHeader), &ptr, temp_arena);
    if (handle.error) {
        beat_log("[parser_ojn] SHIT Failed to read ojn header\n");
        return false;
    }
    smem the_size = bytes_read;
    {
        // Figure out how much to read. We check if the image section comes
        // after the difficulties. If it is we only read up to that. If it
        // isn't we read the whole file. I did this because I though the file
        // was very big because of the image. It is not. It was foolish of me
        // not to check it before writing this. For example:
        // A 1.5MB file has the notes ending at 1.2MB.
        // A 250KB file has the notes ending at 120KB.
        ParserOjnFileHeader *header = (ParserOjnFileHeader*)ptr;
        the_size = handle.total_size - bytes_read;
        s32 max_off = header->image_section_offset;
        s32 off0 = header->note_section_offset[0];
        s32 off1 = header->note_section_offset[1];
        s32 off2 = header->note_section_offset[2];
        max_off = MAXIMUM(max_off, off0);
        max_off = MAXIMUM(max_off, off1);
        max_off = MAXIMUM(max_off, off2);
        if (max_off == header->image_section_offset) {
            the_size = max_off;
        } else {
            the_size = handle.total_size;
        }
        // printf("offs: %d %d %d %d size: %d\n", header->image_section_offset, off0, off1, off2, (int)handle.total_size);
    }
    ptr = arena_realloc(temp_arena, ptr, the_size+1, 16);
    smem size_to_read = the_size-bytes_read;
    bytes_read = debug_read_file_into_buffer(&handle, handle.offset, size_to_read, (u8*)ptr+bytes_read);
    if (handle.error) {
        beat_log("[parser_ojn] SHIT Failed to read rest of ojn file\n");
        return false;
    }
    ASSERT(bytes_read == size_to_read);
    ((u8*)ptr)[the_size] = 0;
    String buf_for_ojn = {.len=the_size, .data=(u8*)ptr};

    valid = parser_ojn_fileset_from_buffer(final_arena, temp_arena, filename, buf_for_ojn, report_errors, ret_fileset);
    if (!valid) {
        return false;
    }

    return true;
}
