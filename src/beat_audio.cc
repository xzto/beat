// this sucks
//
// NOTE: Everything in this file is temporary code. I will rewrite everything from scratch.

// #include "experiments/wave_file.cc"


#include "beat_audio.hh"

namespace audio {


static AudioState global_audio_state;

inline AudioDecoderId AudioDecoderId::generate() {
    return { .gen = global_audio_state.decoders.get_next_gen_id() };
}

/// Can return nullptr.
static AudioDecoder *
get_decoder_by_id__already_have_the_lock(AudioDecoderId id)
{
    AudioDecoder *result = nullptr;
    if (id.is_valid()) {
        // SCOPED_SHARED_LOCK(&global_audio_state.decoders.shared_mutex);
        Forp (global_audio_state.decoders.active_list, _, dec) {
            if (dec->id == id) {
                result = dec;
                break;
            }
        }
    }
    return result;
}

// Returns the number of samples decoded
static s64
maybe_decode_audio(AudioDecoder *decoder)
{
    // TODO: What would happen if the decoder takes so much time that the audio output thread reads the entire buffer?
    // Right now I don't read decoder->total_read_sample_idx;
    //
    // If I wanted song preview I would probably do something a little different.
    // I start playing the sound but I only actually start playing it after the ring buffer has been filled for the first time.
    ticket_mutex_lock(&decoder->locked.mutex);
    auto at_start_locked_data = decoder->locked.data;
    ticket_mutex_unlock(&decoder->locked.mutex);
    if (at_start_locked_data.mixer.is_finished) return 0;
    auto running_total_write_sample_idx = at_start_locked_data.decoder.total_write_sample_idx;
    auto running_buffer_write_idx = at_start_locked_data.decoder.buffer_write_idx;

    auto channels = decoder->channels;
    auto total_sample_count = decoder->total_sample_count;
    auto ring_buffer_sample_count = decoder->ring_buffer_sample_count;
    auto secondary_buffer = decoder->secondary_buffer;
    auto ring_buffer_samples = decoder->ring_buffer_samples;

    ASSERT(ABC(running_buffer_write_idx, ring_buffer_sample_count));
    ASSERT(BETWEEN(running_total_write_sample_idx, 0, total_sample_count));
    auto samples_i_can_decode = ring_buffer_eq_is_full_count_available_to_write(ring_buffer_sample_count, at_start_locked_data.mixer.buffer_read_idx, running_buffer_write_idx);
    // TODO: I probably don't need `did_first_write` if I change the behaviour of `buffer_write_idx` to mean "the last idx it wrote to" instead to "the next idx it should write to"
    if (!at_start_locked_data.decoder.did_first_write)
        samples_i_can_decode = ring_buffer_sample_count;
    const s32 min_samples_to_decode = MINIMUM(8192, decoder->ring_buffer_sample_count);
    if (running_total_write_sample_idx < total_sample_count
        && (samples_i_can_decode >= min_samples_to_decode
            || running_total_write_sample_idx + samples_i_can_decode >= total_sample_count)){
        smem new_samples_to_write = 0;
        // decode more
        switch (decoder->specific.kind) {
        case AudioDecoder::Specific::Kind::mp3: {
            if (mp3dec_ex_seek(decoder->specific.mp3, (u64)(channels * running_total_write_sample_idx))) {
                // error
                NOT_IMPLEMENTED;
            }
            size_t readed = mp3dec_ex_read(decoder->specific.mp3, secondary_buffer.data, (size_t)(channels*samples_i_can_decode));
            if (readed != (size_t)(channels*samples_i_can_decode)) {
                // normal eof or error condition
                if (decoder->specific.mp3->last_error) {
                    // error
                    NOT_IMPLEMENTED;
                }
            }
            new_samples_to_write = (smem)readed / channels;
        } break;
        }

        {
            SCOPED_LOCK(&decoder->locked.mutex);
            // Bail if the values changed (i changed play position on the main thread)
            if (decoder->locked.data.decoder == at_start_locked_data.decoder) {
                decoder->locked.data.decoder.did_first_write = true;
                s64 samples_written = 0;
                // printf("new samples to write: %ld\n", new_samples_to_write);
                s16 *src = secondary_buffer.data;
                while (new_samples_to_write > 0) {
                    auto this_samples_to_write = MINIMUM(new_samples_to_write, ring_buffer_sample_count - running_buffer_write_idx);
                    s16 *dest = &ring_buffer_samples.data[running_buffer_write_idx*channels];
                    xmemcpy_struct_count(dest, src, s16, channels*this_samples_to_write);
                    running_buffer_write_idx += this_samples_to_write;
                    running_total_write_sample_idx += this_samples_to_write;
                    if (running_buffer_write_idx >= ring_buffer_sample_count) {
                        ASSERT(running_buffer_write_idx == ring_buffer_sample_count);
                        running_buffer_write_idx = 0;
                    }
                    samples_written += this_samples_to_write;
                    new_samples_to_write -= this_samples_to_write;
                    src += channels*this_samples_to_write;
                }
                decoder->locked.data.decoder.buffer_write_idx = running_buffer_write_idx;
                decoder->locked.data.decoder.total_write_sample_idx = running_total_write_sample_idx;
                ASSERT(new_samples_to_write == 0);
                return samples_written;
            }
        }
    }
    return 0;
}



static void
do_the_mixer__internal_no_lerp(f32 *dest, s16 *src, s32 count_samples_to_write,
                               s32 src_channels,
                               PannedVolume volume,
                               s32 *running_read_sample_idx,
                               s64 *running_total_sample_idx,
                               s32 *samples_written,
                               s32 *samples_to_write)
{
    ASSERT(count_samples_to_write > 0);
    if (src_channels == 1) {
        for (s32 i = 0; i < count_samples_to_write; i++) {
            *dest++ += volume.e[0] * (*src  ) / 32768.0f;
            *dest++ += volume.e[1] * (*src++) / 32768.0f;
        }
    } else {
        for (s32 i = 0; i < count_samples_to_write; i++) {
            *dest++ += volume.e[0] * (*src++) / 32768.0f;
            *dest++ += volume.e[1] * (*src++) / 32768.0f;
        }
    }
    *running_read_sample_idx += count_samples_to_write;
    *running_total_sample_idx += count_samples_to_write;
    *samples_written += count_samples_to_write;
    *samples_to_write -= count_samples_to_write;
    ASSERT(*samples_to_write >= 0);
}

static void
do_the_mixer__internal_with_lerp(f32 *dest, s16 *src, s32 count_buffer_samples_to_write,
                                 f32 float_count_buffer_samples_to_write,
                                 s32 src_channels,
                                 PannedVolume volume,
                                 f32 src_to_buffer_rate,
                                 f32 speed_to_play,
                                 s32 src_remaining_samples,
                                 s32 *running_read_sample_idx,
                                 f32 *running_read_sample_delta,
                                 s64 *running_total_sample_idx,
                                 s32 *samples_written,
                                 s32 *samples_to_write)
{
    //  This is VERY buggy!!!!!!!
    //  This is VERY buggy!!!!!!!
    //  This is VERY buggy!!!!!!!
    //  This is VERY buggy!!!!!!!
    //  This is VERY buggy!!!!!!!
    //  This is VERY buggy!!!!!!!
    if (src_channels == 1) {
        for (s32 i = 0; i < count_buffer_samples_to_write; i++) {
            f32 float_src_sample_idx = (f32)i*src_to_buffer_rate*speed_to_play;
            s32 src_sample_idx = (s32)float_src_sample_idx;
            f32 delta_src_sample_idx = float_src_sample_idx - (f32)src_sample_idx;
            if (src_sample_idx < src_remaining_samples) {
                s32 next_src_sample_idx = MINIMUM(src_sample_idx+1, src_remaining_samples-1); // TODO: This is wrong! we should wrap around the ring buffer
                f32 src0_value = src[src_sample_idx*src_channels];
                f32 next_src0_value = src[next_src_sample_idx*src_channels]; // WRong!
                src0_value = lerp(src0_value, next_src0_value, delta_src_sample_idx);

                // f32 src1_value = src[src_sample_idx*src_channels + 1];
                // f32 next_src1_value = src[next_src_sample_idx*src_channels + 1];
                // src1_value = lerp(src1_value, next_src1_value, delta_src_sample_idx);

                *dest++ += volume.e[0] * (src0_value) / 32768.0f;
                *dest++ += volume.e[1] * (src0_value) / 32768.0f;
            } else {
                // TODO
            }
        }
    } else {
        for (s32 i = 0; i < count_buffer_samples_to_write; i++) {
            f32 float_src_sample_idx = (f32)i*src_to_buffer_rate*speed_to_play;
            s32 src_sample_idx = (s32)float_src_sample_idx;
            f32 delta_src_sample_idx = float_src_sample_idx - (f32)src_sample_idx;
            if (src_sample_idx < src_remaining_samples) {
                s32 next_src_sample_idx = MINIMUM(src_sample_idx+1, src_remaining_samples-1); // TODO: This is wrong! we should wrap around the ring buffer

                f32 src0_value = src[src_sample_idx*src_channels];
                f32 next_src0_value = src[next_src_sample_idx*src_channels]; // WRong!
                src0_value = lerp(src0_value, next_src0_value, delta_src_sample_idx);

                f32 src1_value = src[src_sample_idx*src_channels + 1];
                f32 next_src1_value = src[next_src_sample_idx*src_channels + 1]; // WRong!
                src1_value = lerp(src1_value, next_src1_value, delta_src_sample_idx);

                *dest++ += volume.e[0] * (src0_value) / 32768.0f;
                *dest++ += volume.e[1] * (src1_value) / 32768.0f;
            } else {
                // TODO
            }
        }
    }
    f32 float_count_source_samples_written = float_count_buffer_samples_to_write * src_to_buffer_rate * speed_to_play;
    s32 int_count_source_samples_written = (s32)float_count_source_samples_written;
    f32 float_delta_plus = float_count_source_samples_written - (f32)int_count_source_samples_written;
    *running_read_sample_idx += int_count_source_samples_written;
    *running_total_sample_idx += int_count_source_samples_written;
    *running_read_sample_delta += float_delta_plus;
    s32 int_of_running_read_sample_delta = (s32)*running_read_sample_delta;
    if (int_of_running_read_sample_delta > 0) {
        *running_read_sample_idx += int_of_running_read_sample_delta;
        *running_total_sample_idx += int_of_running_read_sample_delta;
        *running_read_sample_delta -= (f32)int_of_running_read_sample_delta;
    }
    *samples_written += count_buffer_samples_to_write;
    *samples_to_write -= count_buffer_samples_to_write;
    ASSERT(*samples_to_write >= 0);
}

static void
get_src_samples_i_will_touch(
    s64 src_total_sample_count,
    s64 running_total_read_sample_idx,
    f32 running_read_sample_delta,
    f32 src_to_buffer_rate,
    s32 *inout_output_samples_to_write,
    f64 *ret_f64_src_samples_i_will_touch
) {
    f64 remaining_src_samples = (f64)src_total_sample_count - ((f64)running_total_read_sample_idx+(f64)running_read_sample_delta);
    f64 remaining_src_samples_in_output_units = (remaining_src_samples/(f64)src_to_buffer_rate);
    if ((f64)*inout_output_samples_to_write > remaining_src_samples_in_output_units) {
        *inout_output_samples_to_write = (s32)floor(remaining_src_samples_in_output_units);
    }
    *ret_f64_src_samples_i_will_touch = (f64)*inout_output_samples_to_write / (f64)src_to_buffer_rate;
    ASSERT((*ret_f64_src_samples_i_will_touch + (f64)running_total_read_sample_idx + (f64)running_read_sample_delta)
           <= (f64)src_total_sample_count/*  + 0.001 */);
}


static void
do_the_mixer(f32 *buffer_data, s32 buffer_sample_count, s32 buffer_samples_per_second,
             AudioDecoder *decoder,
             PannedVolume volume)
{
    ticket_mutex_lock(&decoder->locked.mutex);
    auto at_start_locked_data = decoder->locked.data;
    ticket_mutex_unlock(&decoder->locked.mutex);
    if (at_start_locked_data.mixer.is_finished || at_start_locked_data.is_paused) return;
    auto src_ring_buffer_sample_count = decoder->ring_buffer_sample_count;
    auto src_ring_buffer_samples = decoder->ring_buffer_samples;
    auto src_total_sample_count = decoder->total_sample_count;
    auto src_channels = decoder->channels;
    s32 src_samples_per_second = decoder->samples_per_second;
    ASSERT(ABC(at_start_locked_data.mixer.buffer_read_idx, src_ring_buffer_sample_count));
    ASSERT(at_start_locked_data.mixer.total_read_sample_idx <= src_total_sample_count);

    auto speed_to_play = at_start_locked_data.mixer.speed_to_play;
    // TODO: Actually use a ring buffer in the decoder.
    s32 running_read_sample_idx = at_start_locked_data.mixer.buffer_read_idx;
    f32 running_read_sample_delta = at_start_locked_data.mixer.buffer_read_delta_idx;
    s64 running_total_read_sample_idx = at_start_locked_data.mixer.total_read_sample_idx;
    s32 samples_written = 0;

    ASSERT(buffer_sample_count <= decoder->ring_buffer_sample_count);


    const bool same_speed = (buffer_samples_per_second == src_samples_per_second) && (speed_to_play == 1);
    const f32 src_to_buffer_rate = (f32)src_samples_per_second / (f32)buffer_samples_per_second;
    s32 output_samples_to_write = buffer_sample_count; // output samples
    f64 f64_src_samples_i_will_touch = -123123; // only used when !same_speed.
    ASSERT(running_total_read_sample_idx <= src_total_sample_count);
    if (same_speed) {
        if (running_total_read_sample_idx + output_samples_to_write > src_total_sample_count) {
            output_samples_to_write = (s32)(src_total_sample_count - running_total_read_sample_idx);
        }
    } else {
        get_src_samples_i_will_touch(
            src_total_sample_count,
            running_total_read_sample_idx,
            running_read_sample_delta,
            src_to_buffer_rate,
            &output_samples_to_write,
            &f64_src_samples_i_will_touch
        );

        // if ((f64_src_samples_i_will_touch + (f64)running_total_read_sample_idx + (f64)running_read_sample_delta)
        //     > (f64)src_total_sample_count) {
        //     f64_src_samples_i_will_touch = src_total_sample_count - ((f64)running_total_read_sample_idx + (f64)running_read_sample_delta);
        // }
        ASSERT((f64_src_samples_i_will_touch + (f64)running_total_read_sample_idx + (f64)running_read_sample_delta)
               <= (f64)src_total_sample_count + 0.001);
    }
    auto remaining_output_samples_to_write = output_samples_to_write;
    s32 debug__prev_output_samples_to_write = remaining_output_samples_to_write; // make sure there is no infinite loop
    s32 debug__loop_count = 0;
    // TODO: Handle `running_total_read_sample_idx < 0`.
    while (remaining_output_samples_to_write > 0) {
        if (samples_written >= buffer_sample_count)
            break;

        s32 src_ring_sample_count = src_ring_buffer_sample_count;
        s16 *src_ring_samples_data = src_ring_buffer_samples.data;

        // TODO: Assert read doesn't go over write!
        if (running_total_read_sample_idx >= 0) {
            s16 *src = src_ring_samples_data + running_read_sample_idx*src_channels;
            f32 *dest = buffer_data + 2*samples_written;
            if (same_speed) {
                s32 count_output_samples_to_write = MINIMUM(src_ring_sample_count - running_read_sample_idx, remaining_output_samples_to_write);
                if (count_output_samples_to_write > 0) {
                    do_the_mixer__internal_no_lerp(dest, src, count_output_samples_to_write, src_channels,
                                                   volume,
                                                   &running_read_sample_idx,
                                                   &running_total_read_sample_idx,
                                                   &samples_written,
                                                   &remaining_output_samples_to_write);
                } else {
                    INVALID_CODE_PATH;
                    // running_read_sample_idx = total_src_sample_count;
                    // remaining_output_samples_to_write = 0;
                }
                if (running_read_sample_idx >= src_ring_sample_count) {
                    ASSERT(running_read_sample_idx == src_ring_sample_count);
                    running_read_sample_idx = 0;
                    // ASSERT(remaining_output_samples_to_write == 0);
                }
            } else {
                //  This is VERY buggy!!!!!!!
                //  TODO: Rewrite this part of the code.
                f32 float_source_remaining_buffer_samples = (((f32)src_ring_sample_count - ((f32)running_read_sample_idx+running_read_sample_delta))/src_to_buffer_rate);
                f32 float_count_buffer_samples_to_write = MINIMUM(float_source_remaining_buffer_samples, (f32)remaining_output_samples_to_write);
                s32 count_buffer_samples_to_write = (s32)float_count_buffer_samples_to_write;
                ASSERT(samples_written + count_buffer_samples_to_write <= buffer_sample_count);

                //  This is VERY buggy!!!!!!!
                if (float_count_buffer_samples_to_write > 0) {
                    do_the_mixer__internal_with_lerp(dest, src, count_buffer_samples_to_write,
                                                     float_count_buffer_samples_to_write,
                                                     src_channels,
                                                     volume,
                                                     src_to_buffer_rate,
                                                     speed_to_play,
                                                     src_ring_sample_count - running_read_sample_idx,
                                                     &running_read_sample_idx,
                                                     &running_read_sample_delta,
                                                     &running_total_read_sample_idx,
                                                     &samples_written,
                                                     &remaining_output_samples_to_write);
                    if (count_buffer_samples_to_write == 0) {
                        // ??????
                        remaining_output_samples_to_write -= 1;
                        samples_written += 1;
                    }
                } else {
                    ASSERT(remaining_output_samples_to_write > 0);
                    // INVALID_CODE_PATH;
                    remaining_output_samples_to_write -= 1;
                    samples_written += 1;
                    ASSERT(running_read_sample_idx < src_ring_sample_count);
                    ASSERT(running_total_read_sample_idx < src_total_sample_count);
                    running_read_sample_idx += 1;
                    running_total_read_sample_idx += 1;
                }
                if (running_read_sample_idx >= src_ring_sample_count) {
                    // ASSERT(running_read_sample_idx == src_ring_sample_count);
                    ASSERT(running_read_sample_idx == src_ring_sample_count || true); // This is a temporary fix!
                    // running_read_sample_idx = 0;
                    running_read_sample_idx -= src_ring_sample_count;
                    // ASSERT(remaining_output_samples_to_write == 0);
                }
            }
        } else {
            auto idx_to_start_playing = -running_total_read_sample_idx;
            // TODO check different samples per second / speed
            if (idx_to_start_playing < buffer_sample_count) { // TODO wrong
                running_read_sample_idx = 0; // TODO wrong
                running_total_read_sample_idx = 0;
                running_read_sample_delta = 0;
                samples_written = (s32)idx_to_start_playing;
                remaining_output_samples_to_write -= (s32)idx_to_start_playing;
            } else {
                running_total_read_sample_idx += buffer_sample_count; // TODO wrong
                samples_written += buffer_sample_count;
                remaining_output_samples_to_write -= buffer_sample_count;
            }
        }
        // make sure there is no infinite loop
        ASSERT(remaining_output_samples_to_write < debug__prev_output_samples_to_write);
        debug__prev_output_samples_to_write = remaining_output_samples_to_write;
        debug__loop_count++;
    }
    ASSERT(samples_written <= buffer_sample_count);
    // printf("debug__loop_count = %d\n", debug__loop_count);

    ASSERT(running_total_read_sample_idx <= src_total_sample_count);

    {
        SCOPED_LOCK(&decoder->locked.mutex);
        // Commit changes only if I didn't change the position in another thread.
        // Maybe instead of doing this I could make a `decoder.locked.data.mixer.modification_id` and every time I make changes I increment that id. Here instead of comparing the entire `at_start_locked_data.mixer` I would simply check if that `decoder->locked.data.mixer.modification_id` was the same as `at_start_locked_data.mixer.modification_id`.
        if (decoder->locked.data.mixer == at_start_locked_data.mixer) {
            decoder->locked.data.mixer.buffer_read_idx = running_read_sample_idx;
            decoder->locked.data.mixer.buffer_read_delta_idx = running_read_sample_delta;
            decoder->locked.data.mixer.total_read_sample_idx = running_total_read_sample_idx;

            decoder->locked.data.mixer.is_finished = (running_total_read_sample_idx >= src_total_sample_count);

            decoder->locked.data.mixer.curr_sound_time = (((f64)running_total_read_sample_idx + (f64)running_read_sample_delta) / (f64)src_samples_per_second);
        }
    }
}


static int
my_pa_callback(const void *_input_buffer/*ignored*/, void *output_buffer_,
               unsigned long frames_per_buffer,
               const PaStreamCallbackTimeInfo *time_info,
               PaStreamCallbackFlags status_flags,
               void *user_data)
{
    const auto audio_state = &global_audio_state;
    const auto output_buffer = (f32*)output_buffer_;
    const auto output_buffer_samples_per_second = audio_state->output_buffer_samples_per_second;

    zero_size(output_buffer, ssizeof(f32)*frames_per_buffer*2);

#if 0
    static bool is_first_time = true;
    if (is_first_time) {
        is_first_time = false;
        return paContinue;
    }

    if (pl->curr.reference_input_time == 0) {
        pl->curr.reference_input_time = time_info->inputBufferAdcTime;
        pl->curr.reference_sample_idx = 0;
    }

    f64 seconds_into_sound = time_info->inputBufferAdcTime - pl->curr.reference_input_time;
    f64 float_samples_into_sound = (f64)(seconds_into_sound * (f64)pl->samples_per_second);
    float_samples_into_sound *= (f64)speed_to_play;
    s32 uwu_sample_idx = (s32)float_samples_into_sound;
    f32 uwu_float_sample_delta = (f32)float_samples_into_sound - (f32)uwu_sample_idx;
#endif

    const auto master_volume = audio_state->master_volume;

    {
        SCOPED_SHARED_LOCK(&audio_state->decoders.shared_mutex);

        Forp (global_audio_state.decoders.active_list, _, decoder) {
            do_the_mixer(output_buffer, (s32)frames_per_buffer, output_buffer_samples_per_second,
                         decoder,
                         // pl->speed,
                         // &uwu_sample_idx, &uwu_float_sample_delta,
                         master_volume);
        }
    }
    audio_state->test_current_time = time_info->currentTime;
    audio_state->test_output_buffer_dac_time = time_info->outputBufferDacTime;
    audio_state->test_input_buffer_adc_time = time_info->inputBufferAdcTime;


    // if (time_info->inputBufferAdcTime != 0)
    //     printf("FROM CALLBACK: inputBufferAdcTime: %f\n", time_info->inputBufferAdcTime);
    // Apparently currentTime being zero has to do with pulseaudio
    if (false && time_info->currentTime != 0)
        printf("FROM CALLBACK: currentTime: %f\n", time_info->currentTime);
    if (false && time_info->outputBufferDacTime != 0)
        printf("FROM CALLBACK: outputBufferDacTime: %f\n", time_info->outputBufferDacTime);
    if (false)
        printf("FROM CALLBACK outputBufferDacTime - currentTime: %f\n", time_info->outputBufferDacTime - time_info->currentTime);

#if 0
    if (status_flags & paOutputUnderflow)
        DEBUG_BREAK;
    if (status_flags & paOutputOverflow)
        DEBUG_BREAK;
    if (status_flags & paPrimingOutput)
        DEBUG_BREAK;
#endif

#if 0
    static s32 asdf = 0;
    asdf++;
    if ((asdf % (3*data->buffer_samples_per_second/(s32)frames_per_buffer)) == 0) {
        usleep(100*1000); // will cause "underun" / "underflow"
    }
#endif
    return paContinue;
}

static AudioDecoderId
make_audio_decoder(String sound_filename, f64 sound_offset_seconds)
{
    // TODO: ASSERT(is on main thread) // actually it might not be necessary
    //
    // TODO: Other kinds of decoders
    // TODO: Maybe call `mp3dec_ex_open` on another thread and return a dummy
    // decoder (decoder.kind = AudioDecoderKind::DummyQueued). The other thread
    // will load it and if it succeeds it will overwrite that dummy decoder
    // with the correct one.
    // If it fails to load, the other thread will set the kind to DummyLoaded.
    // (make_audio_decoder will never return invalid_id)
    // When I need the sound loaded (for playing a file, for example) I should check if the kind is `DummyQueued`. If the kind is `DummyQueued`, I know that the sound has not yet loaded.

    auto start_clock = platform.get_clock();
    bool debug__ok = false;
    AudioDecoder::Specific specific = {};
    s32 channels;
    s32 samples_per_second;
    s64 total_sample_count;
    const auto ext = get_file_extension(sound_filename);
    char filename_cstr[4096];
    if (sound_filename.len > ssizeof(filename_cstr)-1) {
        printf("[make_audio_decoder] sound filename is too big\n");
        return AudioDecoderId::INVALID();
    }
    xmemcpy(filename_cstr, sound_filename.data, sound_filename.len);
    filename_cstr[sound_filename.len] = 0;

    // TODO: If `sound_filename` is not found, try to find another file with a different extension.
    // TODO: Check file magic instead of file extension. Other games do it so I should do it too.
    if (ext == "mp3"_s) {
        specific = {
            .kind = AudioDecoder::Specific::Kind::mp3,
            .mp3 = xmalloc_struct(mp3dec_ex_t),
        };


        if (mp3dec_ex_open(specific.mp3, filename_cstr, MP3D_SEEK_TO_SAMPLE)) {
            printf("mp3dec_ex_open failed for '%s'\n", filename_cstr);
            // exit(1);
            // NOT_IMPLEMENTED;
            xfree(specific.mp3);
            return AudioDecoderId::INVALID();
        }

        printf("Samples: %lu    Hz %d   layer %d   channels %d\n",
               specific.mp3->samples, specific.mp3->info.hz, specific.mp3->info.layer, specific.mp3->info.channels);
        if (!(specific.mp3->info.channels == 1 || specific.mp3->info.channels == 2)) {
            // I won't ever support more than two channels.
            // NOT_IMPLEMENTED;
            printf("make_audio_decoder failed for filename '%s': wrong channel count: %d\n",
                   filename_cstr, specific.mp3->info.channels);
            mp3dec_ex_close(specific.mp3);
            xfree(specific.mp3);
            return AudioDecoderId::INVALID();
        }
        ASSERT(specific.mp3->samples >= 0);
        channels = specific.mp3->info.channels;
        samples_per_second = specific.mp3->info.hz;
        total_sample_count = (s64)specific.mp3->samples / (s64)specific.mp3->info.channels;

        debug__ok = true;
    } else {
        // unrecognized extension
        // NOT_IMPLEMENTED;
        return AudioDecoderId::INVALID();
    }

    ASSERT(debug__ok);

    AudioDecoder *decoder;
    smem decoders_active_list_len;
    {
        SCOPED_UNIQUE_LOCK(&global_audio_state.decoders.shared_mutex);
        decoder = global_audio_state.decoders.active_list.add_uninit(1);
        zero_struct(decoder);
        decoders_active_list_len = global_audio_state.decoders.active_list.len;
        decoder->id                 = AudioDecoderId::generate();
        decoder->specific = specific;
        decoder->channels           = channels;
        decoder->samples_per_second = samples_per_second;
        decoder->sound_offset_seconds = sound_offset_seconds;
        decoder->total_sample_count = total_sample_count;

        decoder->locked = {
            .mutex = TicketMutex{},
            .data = {
                .debug__called_start_playing_at_least_once  = false,
                .is_paused                                  = true,
                .mixer = {
                    .total_read_sample_idx  = 0,
                    .buffer_read_idx        = 0,
                    .buffer_read_delta_idx  = 0,
                    .curr_sound_time        = 0,
                    .speed_to_play          = 1,
                    .is_finished            = (0 == total_sample_count),
                },
                .decoder = {
                    .total_write_sample_idx = 0,
                    .buffer_write_idx       = 0,
                    .did_first_write        = false,
                },
            },
        };
        decoder->ring_buffer_sample_count = MAXIMUM(1, (s32)MINIMUM(
                (s64)decoder->MAXIMUM_RING_BUFFER_SAMPLE_COUNT, decoder->total_sample_count));
        {
            smem count_s16_to_alloc = decoder->ring_buffer_sample_count*decoder->channels;
            decoder->ring_buffer_samples.data = xmalloc_struct_count(
                s16, count_s16_to_alloc, xmalloc_flags_zero_align(AudioDecoder::RING_BUFFER_ALIGN));
            decoder->secondary_buffer.data = xmalloc_struct_count(
                s16, count_s16_to_alloc, xmalloc_flags_zero_align(AudioDecoder::RING_BUFFER_ALIGN));
        }
    }


    // for one random file: 60ms not on the filesystem cache. 5ms on the filesystem cache.
    printf("Added decoder id %lu: %ld active\n", decoder->id.gen, decoders_active_list_len);
    printf("make_audio_decoder for filename '%s' took %.2fms\n",
           filename_cstr, 1000*(f64)(platform.get_clock()-start_clock)*platform.get_secs_per_clock());
    return decoder->id;
}

void
AudioDecoder::deinit()
{
    // Must have unique lock to global_audio_state.decoders.shared_mutex!
    switch (specific.kind) {
    case Specific::Kind::mp3: {
        mp3dec_ex_close(specific.mp3);
        xfree(specific.mp3);
    } break;
    }
    xfree(secondary_buffer.data);
    xfree(ring_buffer_samples.data);
    INVALIDATE_STRUCT(this);
}

// Something feels weird with this global offset. It kinda works but I change from 0 to -50ms and there isn't that much difference. I didn't test it without using my eyes, though. Maybe that's it.
// If you hit too early, decrease it.
static volatile f64 debug__sound_global_offset = 0/1000.0;
static f64
debug__my_time_to_sound_time(f64 my_time, f64 mp3_offset, f64 rate)
{
    return (my_time + debug__sound_global_offset)*rate - mp3_offset;
}
static f64
debug__sound_time_to_my_time(f64 sound_time, f64 mp3_offset, f64 rate)
{
    return (sound_time + mp3_offset)/rate - debug__sound_global_offset;
}

static NoteTimestamp
sound_time_to_note_timestamp(f64 sound_time, f64 mp3_offset, f64 rate)
{
    return {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(sound_time + mp3_offset - debug__sound_global_offset*rate)};
}

static void
start_playing_sound(AudioDecoderId decoder_id, f32 speed, f64 my_real_time_seconds)
{
    // TODO: Must only be called from the main thread (ASSERT it)
    ASSERT(decoder_id.is_valid());
    ASSERT(global_audio_state.is_initialized);
    SCOPED_SHARED_LOCK(&global_audio_state.decoders.shared_mutex);
    AudioDecoder *decoder = get_decoder_by_id__already_have_the_lock(decoder_id);
    ASSERT(decoder);
    {
        SCOPED_LOCK(&decoder->locked.mutex);
        decoder->locked.data.debug__called_start_playing_at_least_once = true;
        decoder->locked.data.mixer.speed_to_play = speed;
        auto temp_curr_sound_time = debug__my_time_to_sound_time(my_real_time_seconds, decoder->sound_offset_seconds, (f64)speed);
        decoder->locked.data.mixer.buffer_read_delta_idx = 0; // Technically we might be offsync by 1/44100th of a second.
        auto total_read_sample_idx = (s64)floor(temp_curr_sound_time * (f64)decoder->samples_per_second);
        total_read_sample_idx = MINIMUM(decoder->total_sample_count, total_read_sample_idx);
        decoder->locked.data.mixer.total_read_sample_idx = total_read_sample_idx;
        ASSERT(total_read_sample_idx <= decoder->total_sample_count);
        decoder->locked.data.mixer.is_finished = (total_read_sample_idx == decoder->total_sample_count);
        decoder->locked.data.is_paused = false;
        decoder->locked.data.mixer.curr_sound_time = (((f64)decoder->locked.data.mixer.total_read_sample_idx + (f64)decoder->locked.data.mixer.buffer_read_delta_idx) / (f64)decoder->samples_per_second);

        decoder->locked.data.decoder.total_write_sample_idx = MAXIMUM(0, decoder->locked.data.mixer.total_read_sample_idx);
        decoder->locked.data.decoder.did_first_write = false;
        decoder->locked.data.mixer.buffer_read_idx = 0;
        decoder->locked.data.decoder.buffer_write_idx = 0;
    }
}

static void
sound_set_pause(AudioDecoderId decoder_id, bool paused) {
    ASSERT(decoder_id.is_valid());
    ASSERT(global_audio_state.is_initialized);
    SCOPED_SHARED_LOCK(&global_audio_state.decoders.shared_mutex);
    AudioDecoder *decoder = get_decoder_by_id__already_have_the_lock(decoder_id);
    ASSERT(decoder);
    {
        SCOPED_LOCK(&decoder->locked.mutex);
        ASSERT(decoder->locked.data.debug__called_start_playing_at_least_once);
        decoder->locked.data.is_paused = paused;
    }
}

static void
sound_toggle_pause(AudioDecoderId decoder_id) {
    ASSERT(decoder_id.is_valid());
    ASSERT(global_audio_state.is_initialized);
    SCOPED_SHARED_LOCK(&global_audio_state.decoders.shared_mutex);
    AudioDecoder *decoder = get_decoder_by_id__already_have_the_lock(decoder_id);
    ASSERT(decoder);
    {
        SCOPED_LOCK(&decoder->locked.mutex);
        ASSERT(decoder->locked.data.debug__called_start_playing_at_least_once);
        decoder->locked.data.is_paused = !decoder->locked.data.is_paused;
    }
}

static void
remove_audio_decoder(AudioDecoderId id)
{
    // TODO: Must only be called from the main thread (ASSERT it)
    ASSERT(id.is_valid());
    bool found = false;
    smem active_list_len;
    {
        SCOPED_UNIQUE_LOCK(&global_audio_state.decoders.shared_mutex);
        Fordecp (global_audio_state.decoders.active_list, it_idx, it) {
            if (it->id == id) {
                found = true;
                it->deinit();
                global_audio_state.decoders.active_list.unordered_remove(it_idx);
                active_list_len = global_audio_state.decoders.active_list.len;
                break;
            }
        }
    }
    ASSERT(found);
    printf("Removed decoder id %lu: %ld active\n", id.gen, active_list_len);
}

static void
init()
{
    ASSERT(!global_audio_state.is_initialized);
    PaError pa_err = Pa_Initialize();
    if (pa_err != paNoError) {
        printf("error Pa_Initialize: %s\n", Pa_GetErrorText(pa_err));
        NOT_IMPLEMENTED;
        exit(1);
    }


    global_audio_state = {};
    global_audio_state.is_initialized = true;
    init_arena(&global_audio_state.arena, MEGABYTES(10), NULL, true);
    global_audio_state.output_buffer_samples_per_second = 44100;
    global_audio_state.master_volume.e[0] = 0.5f;
    global_audio_state.master_volume.e[1] = global_audio_state.master_volume.e[0];
    global_audio_state.decoders.shared_mutex = SharedTicketMutex::init();
    global_audio_state.decoders.next_generational_id = 1;
    global_audio_state.decoders.active_list =
        NewArrayDynamic<AudioDecoder>::init_capacity(
            &global_audio_state.arena, 32); // more than enough (except for native bms/o2jam audio - I will probably not support that)

    PaStreamParameters outputParameters = {};

    outputParameters.device = Pa_GetDefaultOutputDevice();
    if (outputParameters.device == paNoDevice) {
        printf("Error PortAudio: No default output device.\n");
        NOT_IMPLEMENTED;
        exit(1);
    }
    outputParameters.channelCount = 2;       /* stereo output */
    outputParameters.sampleFormat = paFloat32;
    outputParameters.suggestedLatency = Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    pa_err = Pa_OpenStream(&global_audio_state.pa_stream,
                           0,
                           &outputParameters,
                           global_audio_state.output_buffer_samples_per_second,
                           // paFramesPerBufferUnspecified,
                           512,// 512,
                           paClipOff, // flags
                           my_pa_callback,
                           0);
    // PaAlsa_EnableRealtimeScheduling(pa_stream, 1); // ???
    if (pa_err != paNoError) {
        // printf("error Pa_OpenDefaultStream: %s\n", Pa_GetErrorText(pa_err));
        printf("error Pa_OpenStream: %s\n", Pa_GetErrorText(pa_err));
        NOT_IMPLEMENTED;
        exit(1);
    }

    pa_err = Pa_StartStream(global_audio_state.pa_stream);
    if (pa_err != paNoError) {
        printf("error Pa_StartStream: %s\n", Pa_GetErrorText(pa_err));
        NOT_IMPLEMENTED;
        exit(1);
    }
}

static void
deinit()
{
    ASSERT(global_audio_state.is_initialized);
    Pa_StopStream(global_audio_state.pa_stream);
    Pa_Terminate();
}

} // namespace audio
