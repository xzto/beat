#ifndef BEAT_WORK_QUEUE_H
#define BEAT_WORK_QUEUE_H

#include "beat_threads.hh"

#define WORKER_FN(name) void name(u64 _thread_stuff, void *data, TempMemory temp_mem, TempMemory temp_mem2)
typedef WORKER_FN(WorkerFn);

struct WorkQueueEntry {
    WorkerFn *fn;
    void *data;
};

struct WorkQueue_OsSpecific;
// @WorkQueueABA1
struct WorkQueue {
    WorkQueue_OsSpecific *os_specific;
    Atomic<u32> entry_to_do;
    Atomic<u32> entry_to_add;
    volatile WorkQueueEntry entries[512];
};


struct WorkMemory {
    // this is used when I want to create a task and need memory for it to start (different from workqueue's tempmemory) (for example if I need to copy a string)
    bool is_initialized;
    bool deinitialize_on_end;
    MemoryArena arena;
    bool being_used;
    TempMemory temp_for_work;
};

// enum WorkAsyncDataType : s32 {
//     WorkAsyncDataType_wheel_add_directory,
//     WorkAsyncDataType_load_font_chunk,
//     WorkAsyncDataType_COUNT,
// };

struct WorkAsyncData {
    // bool is_initialized;
    bool being_used;
    volatile bool completed;
    volatile f64 seconds_elapsed;
};


// NOTE: Work_xxx: the argument passed to the worker function
// NOTE: WorkMemory: memory to start the work
// NOTE: WorkAsyncData_xxx: data to communicate between the main thread and the worker thread.
// Maybe I can merge WorkMemory and WorkAsyncData

struct WorkAsyncData_wheel_add_directory; // on beat.h
struct Work_wheel_add_directory; // on beat.h
static WORKER_FN(worker_wheel_add_directory); // on beat.h

struct WorkAsyncData_load_font_chunk; // on beat.h
struct Work_load_font_chunk; // on beat.h
static WORKER_FN(worker_load_font_chunk); // on beat.h



#endif
