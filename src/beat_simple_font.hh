#ifndef TEST_FONT_H
#define TEST_FONT_H

// -[x] signed distance field fonts
// -[ ] colored emojis (no idea how to do it)
//  - maybe sdf for normal glyphs and normal bitmap for emojis? each glyph has a bool saying whether it is a plain bitmap or a sdf.



// Bitmaps are left->right, top->down
// Terminology:
// Codepoint: the integer representing a unicode character like 'A' (65)
// Glyph: The structure representing a character's image.
// Id: The index to the glyphs array. Zero is a reserved invalid ID.
// - Glyph *g = fontheader.glyphs[id];
// codepoint_to_glyph_idx: The table to get from codepoint to glyph_idx
// - u32 glyph_idx = chunk.codepoint_to_glyph_idx[codepoint - chunk.first_codepoint];
//   if glyph_idx is zero, the glyph is invalid
// Advance: How much to advance in pixels when drawing two characters.
// Kerning: The difference in the advance for a sequence of two characters
// - get_advance(a, b) get's the kerning if there is one. if there isn't a
//   kerning for these two characters, it just returns first_glyph->advance;

#define SIMPLE_FONT_BITMAP_ROW_ALIGNMENT 4
#define SIMPLE_FONT_BYTES_PER_PIXEL 1

STATIC_ASSERT(BEAT_IS_LITTLE_ENDIAN);


#if 0
// usage:
{
    // (dont do this, use get_advance(first_glyph, second_glyph))
    FontGlyph *first_glyph, *second_glyph;
    for (smem idx_kern = 0; idx_kern < first_glyph->kernings_count; idx_kern++) {
        FontKerning *k = first_glyph->kernings_data[idx_kern];
        if (k->other_codepoint == seconds_glyph->codepoint) {
            // use this kerning
            break;
        }
    }
}
#endif
struct FontKerning {
    u32 other_codepoint;
    f32 delta; // mult by font_size_px
};

#if 0
// Example code
{
    u32 codepoint = 32;
    FontGlyph *glyph = codepoint_to_glyph(font, codepoint);
    ASSERT(glyph->codepoint == codepoint);
    u32 tex = game_memory->debug_font0_textures[glyph->total_glyph_idx];
}
// ---
{
    Font *font;
    String str = "abc\001d"_s;
    bool draw_unknown_glyphs = true;
    FontGlyph *prev_glyph = NULL;
    s32 x = 0;
    f32 baseline_pos = 100;
    f32 y = baseline_pos;
    f32 font_height_px = 20;
    for (s32 i = 0; i < str.len;) {
        u32 codepoint = 0;
        s32 codepoint_size = utf8_decode(str, i, &codepoint);
        if (!codepoint_size) {
            // also could draw the byte as "\xFF"
            i++;
            continue;
        }
        FontChunk *chunk;
        FontGlyph *glyph = codepoint_to_glyph(font, codepoint, &chunk);
        x += font_height_px * get_advance(prev_glyph, glyph);
        if (glyph) {
            if (glyph->has_bitmap) {
                v2 size = (font_height_px / font->pixel_height_stbtt) * v2_cast_f32(glyph->atlas_element_size);
                v2 bottom_left = V2(x, y);
                bottom_left.x +=  font_height_px*glyph->offset.x;
                bottom_left.y += -font_height_px*glyph->offset.y - size.y;
                u32 atlas_tex = chunk->opengl_atlas_tex;
                // whitespace (32) wouldn't have a bitmap but would have a glyph->advance
                // draw_sdf_atlas will compute the uvs
                if (atlas_tex) draw_sdf_atlas(bottom_left, size, color, atlas_tex, chunk->atlas_bitmap_size,
                                              glyph->atlas_element_position, glyph->atlas_element_size);
            }
        } else if (draw_unknown_glyphs) {
            x = my_draw_unknown_glyph_bitmap(pos, codepoint);
        }
        prev_glyph = glyph;
    }
    // return x;
}
#endif


struct FontGlyph {
    u32 codepoint;
    // s32 chunk_idx;
    // s32 total_glyph_idx;
    v2 offset; // mult by font_size_px
    f32 advance; // mult by font_size_px
    // to get width and height for rendering do `(font_size_px / pixel_height_stbtt) * (f32)bitmap_width`;

    bool has_bitmap;
    v2s atlas_element_position; // in pixels // bottom up
    v2s atlas_element_size; // in pixels

    // TODO kernings
    // kerning is a pointer to the first kerning of this glyph
    // kerning_count is the number of kernings for this glyph
    s32 kernings_count;
    FontKerning *kernings;
};

struct FontChunk {
    u32 first_codepoint;
    u32 count_codepoint;
    s32 *codepoint_to_glyph_idx; // [count_codepoint]; // zero is invalid.

    s32 glyphs_count; // includes the invalid glyph
    // access with glyphs[glyph_idx], where glyph_idx = codepoint_to_glyph_idx[codepoint - first_codepoint] unless glyph_idx is zero.
    FontGlyph *glyphs; // [valid_glyphs_count+1];

    v2s atlas_bitmap_size; // bottom-up
    s32 atlas_bitmap_pitch;
    u8 *atlas_bitmap; // (pitch * size.y) bytes

    u32 opengl_atlas_tex;
    bool did_load_gl_crap;
};

struct FontChunkAndQueuedId {
    FontChunk chunk;
    u64 queued_id;
};

struct FontChunkQueued {
    u32 first_codepoint;
    u32 count_codepoint;
    bool job_started;
    u64 id;
};

#include "stb/stb_truetype.h"
struct FontMakeinfo {
    EntireFile ef;
    stbtt_fontinfo stbinfo;
    f32 stb_scale;
    f32 font_height_px;
    u8 sdf_pad;
    u8 sdf_onedge_value;
    f32 sdf_pixel_dist_scale;
};

struct Font {
    f32 pixel_height_stbtt;

    // ascent is the coordinate above the baseline the font extends;
    // descent is the coordinate below the baseline the font extends (it is typically negative)
    // line_gap is the spacing between one row's descent and the next row's ascent...
    // you need to multiply these three by font_height_px to get values in pixels.
    f32 line_gap;
    f32 ascent;
    f32 descent;

    u8 sdf_onedge_value;
    u8 sdf_pad;
    f32 sdf_pixel_dist_scale;

    s32 total_glyph_count; // sum of chunk.valid_glyphs_count for all chunks

    NewArrayDynamic<FontChunk> chunks; // @Malloc

    u64 next_chunk_queued_id;
    NewArrayDynamic<FontChunkQueued> chunks_queued; // @Malloc

    FontMakeinfo make_info; // TODO: array


#if 0
    // For now each chunk and glyph will have its allocation for everything. TODO: Put all chunks in one allocation for each thing.
    smem main_codepoint_to_glyph_idx_count;
    u32 *main_codepoint_to_glyph_idx_data; // [main_codepoint_to_glyph_idx_count]

    smem main_glyphs_count;
    FontGlyph *main_glyphs_data; // [main_glyphs_count]

    smem main_kernings_count;
    FontKerning *main_kernings_data; // [main_kernings_count]
#endif
};

#endif
