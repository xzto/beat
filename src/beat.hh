#ifndef BEAT_TEST_SIM_H
#define BEAT_TEST_SIM_H

#include "beat_math.hh"


#define SCROLL_CHANGE_MULTIPLIER (1 + 1.0f/64)
#define MIN_TIME_FROM_NOW_TO_IGNORE_NOTES 2.0
#define MIN_TIMESTAMP_FROM_NOW_TO_IGNORE_NOTES NOTE_TIMESTAMP_FROM_SECONDS(MIN_TIME_FROM_NOW_TO_IGNORE_NOTES)

enum ScrollType {
    SCROLL_TYPE_PIXELS_PER_SECOND,
    SCROLL_TYPE_TIME_TO_APPEAR,
};

// 10 and 100000 is not a hard limit, it's just so it doesn't get too big/small
#define SCROLL_LIMIT_MIN 64
#define SCROLL_LIMIT_MAX 1000000
#define DIAMOND_FOR_NOTES 0
#define DRAW_BAR_LN_END 0

struct ScrollConfig {
    // bool direction_up_down;
    // make an enum with options for scale with bpm, like scale with main bpm, sacle with every bpm, dont scale
    bool scale_with_bpm;
    // f64 bpm_to_normalize_to;
    ScrollType type;
    // union { // Might be a good idea not to use a union here.
    struct {
        s32 pixels_per_second;
        // maybe store time_to_appear in the scale of s^-1 so that a bigger number means faster scrolling
        s32 time_to_appear; // in ms
    };
};

// this will disappear when I load bitmaps
enum {
    // Unknown must be the first one.
    SnappingType_Unknown = 0,

    SnappingType_1_1,
    SnappingType_1_2,
    SnappingType_1_4,
    SnappingType_1_8,

    SnappingType_1_3,
    SnappingType_1_6,
    SnappingType_1_12,
    SnappingType_1_24,

    SnappingType_Count,
};
typedef u8 SnappingType;

struct PlayingVisualConfig {
    ScrollConfig scroll;
    v4 receiver_color;
    v4 receiver_pressed_color;
    v4 background_color;
    v4 playfield_color;
    bool note_coloring_by_column;
    // colors_by_xxx will disappear when I load bitmaps.
    struct {
        struct {
            s32 colors_by_column_count;
            v4 *colors_by_column; // [colors_by_column_count] i think in memory->permanent
        };
        struct {
            v4 colors_by_snapping_type[SnappingType_Count];
        };
    };

    // these are in px without scaling (you need to multiply them by window_scale_factor to draw things)
    f32 note_base_height; // in px
    f32 note_width;
    f32 note_spacing;
};


enum SceneEnum {
    SCENE_FIRST,
    SCENE_WHEEL,
    SCENE_PLAYING,
    // SCENE_OPTIONS,
};

typedef struct {
    bool is_initialized;

    s32 selected_option;
} SceneFirst;

#define RATE_ID_CONSTANT 100
#define RATE_ID_MIN (RATE_ID_CONSTANT/2)
#define RATE_ID_MAX (RATE_ID_CONSTANT*3)
#define rate_id_is_valid(rate) ((rate).id >= RATE_ID_MIN && \
                              (rate).id <= RATE_ID_MAX)
#define rate_id_to_float(rate) ((f64)(rate).id / (f64)RATE_ID_CONSTANT)
#define rate_id_from_float(f) RateID{(u16)(f * (f64)RATE_ID_CONSTANT)}
#define RATE_ID_ONE (RateID{RATE_ID_CONSTANT})
struct RateID {
    u16 id;
};

struct WheelDiffLocation {
    u64 fileset_unique_id;
    s32 diff_idx;
};
inline static bool operator == (WheelDiffLocation a, WheelDiffLocation b) {
    return a.fileset_unique_id == b.fileset_unique_id && a.diff_idx == b.diff_idx;
}
inline static bool operator != (WheelDiffLocation a, WheelDiffLocation b) {
    return !(a.fileset_unique_id == b.fileset_unique_id && a.diff_idx == b.diff_idx);
}

#if 0
// TODO: Something like this
struct WheelEntry {
    bool is_a_dir;
    union {
        struct {
            String name;
            NewArrayDynamic<WheelEntry> entries;
        } dir;
        struct {
            WheelDiffLocation loc;
        } diff;
    };
};
#endif

struct WorkAsyncData_load_font_chunk {
    // static constexpr auto TYPECHECK_WorkAsyncData = 1;
    // static constexpr auto ENUM_VALUE = WorkAsyncDataType_load_font_chunk;

    WorkAsyncData base;

    struct {
        TicketMutex mutex;
        NewArrayDynamic<FontChunkAndQueuedId> new_chunks;
    };

    // to printf stuff at the end; only modified at the main thread
    s32 count_new_chunks;
    s32 new_glyphs_count;
};
struct Work_load_font_chunk {
    WorkMemory *work_mem;
    WorkAsyncData_load_font_chunk *async_data;
    NewArrayDynamic<FontChunkQueued> queued_chunks;
    FontMakeinfo *font_make_info;
};

struct WorkAsyncData_wheel_add_directory {
    // static constexpr auto TYPECHECK_WorkAsyncData = 1;
    // static constexpr auto ENUM_VALUE = WorkAsyncDataType_wheel_add_directory;
    // static constexpr auto CAN_BEGIN_FAIL = false; // TODO
    // static constexpr auto MAXIMUM_CONCURRENT_THINGS = false; // TODO

    WorkAsyncData base;

    struct {
        TicketMutex mutex;
        NewArrayDynamic<Fileset> new_filesets;
    };
};
struct Work_wheel_add_directory {
    WorkMemory *work_mem;
    String dir; // on work_mem.arena
    s32 debug_max_to_open;
    WorkAsyncData_wheel_add_directory *async_data;
};

struct SceneWheel
{
    bool is_initialized;

    MemoryArena *arena;

    u64 next_unique_id_for_fileset;

    struct {
        // When we modify wheel_entries we have to recalculate the selected thing. When we modify sorted_diffs we modify wheel_entries too. When we modify loaded_filesets we have to recaculate everything. (Not necessarily recalculate from scratch. If we add a fileset we can just add the diffs to the end and resort and refilter. If we delete a fileset we need to delete those diffs and then resort and refilter). Modify X -> recalculate the things below them.

        // TODO: I probably will want some sort of cache for the loaded filesets so that the game doesn't take too long to start.
        // The cache would have the diff metadata and path and file modified time to check for difference.
        // Diff would have a "loaded" flag.
        // there would be a specific unique id for an unloaded fileset.

        // The filesets we have loaded. They are sorted on unique_id (makes it easier to search for a fileset).
        NewArrayDynamic<Fileset> loaded_filesets;
        NewArrayDynamic<WheelDiffLocation> sorted_diffs; // All the diffs sorted without filtering
        NewArrayDynamic<WheelDiffLocation> wheel_entries; // sorted_diffs but filtered

        // store the index to wheel_entries. When we modify wheel_entries we
        // have to recalculate this (we need to get the selected diff, then we
        // modify the entries and then we search for the entry we had).
        smem selected_wheel_entry;
    } thenew;

    // TODO: I want to load songs in more than one thread. (maybe it does the directory iterator and adds the filenames to an array, after everything is finished it splits up the array in many threads). (I already support multiple workers at the same time however each call to add_work_wheel_add_directory one fires up one thread).
    // TODO: Remove Duplicate filesets before merging.
    // TODO: Remove filesets that were removed from the filesystem.
    // It is a NewArrayDynamic but it doesn't grow indefinitely.

    // Maybe pack these in a struct so I can easily reinitialize them.
    RateID rate_id;

    bool is_search_active;

    StringNewArrayDynamic search_str; // @Leak // TODO I should probably set a limit to this thing. // on wheel->arena // maybe it would be better if this wasn't in wheel->arena. (unless it had a fixed cap)
    StringNewArrayDynamic prev_search_str; // this is the search_str from the previous frame. // on wheel->arena
};

typedef struct {
    f64 offset_seconds;
    NoteTimestamp playing_ts; // gs->playing.newthings.timestamp when it happened
    bool miss;
} TestAcc;

enum PlayingState : u32 {
    PlayingState_Waiting, // When it's waiting for something to load (mp3) (unlikely, maybe i'll delete it)
    PlayingState_Ready, // When everything is loaded but it didn't start yet
    PlayingState_Started,
    PlayingState_Ended,
};

struct ScenePlayingPerColumnThing {
    NewArrayDynamic<VisibleNote> vns;
    NewArrayDynamic<TimedInput> tis;
    NewArrayDynamic<TestAcc> debug_test_accs;
};

// struct DebugReadEntireMp3Result;
struct AudioDecoder;

struct ScenePlaying
{
    bool is_initialized;
    MemoryArena *arena;
    struct {
        Diff *diff;

        RateID rate_id;
        f64 rate_float;
    } params;
    struct {
        PlayingVisualConfig *visual_config;
        Keymode keymode;
        s32 columns;

        ScenePlayingPerColumnThing *per_column; // [columns]

        SnappingType *snappings; // [params.diff->notes.rows_count]

        struct {
            NoteTimestamp song_ts;
            s32 curr_row_idx;
            s32 curr_bpm_idx;
        } newthings;

        struct {
            // u64 os_clock;
            f64 os_time_seconds;
            NoteTimestamp song_ts;
            f64 last_reported_audio_sound_time;
            u64 last_seek_frame_count;
            // TODO: When I implement sound, put a reference of the playing sound here.
            // I'll start playing the sound at -2 seconds and set playing state to Ready. then I'll try to get a reference of the sound playing and when I successfully do that I put that reference here. I should do that as close as possible to when I get os_time_seconds.
            // at the end of the frame at game_update_and_render:
            //  if (gs->scene == SCENE_PLAYING && gs->playing.other.state == PlayingState_Ready) {
            //      pos = get_sound_playing_position();
            //      if (success) {
            //          gs->got_sound_reference_on_previous_frame = true;
            //          gs->playing.other.reference.sound = pos;
            //      }
            //  }
            //  at the start of the frame in scene_playing
            //  if (gs->playing.other.reference.got_sound_reference_on_previous_frame) {
            //      gs->playing.other.reference.got_sound_reference_on_previous_fram = false;
            //      gs->playing.other.state = PlayingState_Started;
            //      gs->playing.other.reference.os_time_seconds = gin->os_time_seconds;
            //      set_time_playing_from_sound_reference(&gs->playing, gin->os_time_seconds, gs->playing.other.reference.sound);
            //  }
        } reference;

        s32 acc_index;
        s32 acc_total;
        TestAcc acc[128];

        f64 score, max_score_right_now;

        bool paused;
        PlayingState state;

        audio::AudioDecoderId debug_the_audio_decoder_id;
    } other;
};


enum AccSystemType {
    AccSystemType_Invalid,
    AccSystemType_Mytestwife3,
    AccSystemType_COUNT,
};
enum LifeSystemType {
    LifeSystemType_Invalid,
    LifeSystemType_Testdonothing,
    LifeSystemType_COUNT,
};

enum class TestArgGameMode {
    WheelFromFile,
    WheelSingleFile,
    WheelDirectoryIter,
};

struct TestArgs {
    TestArgGameMode mode;
    String wheel_directory_iter, wheel_from_file, wheel_single_file;
};

struct FadingText {
    String text;
    f64 seconds_elapsed;
    static FadingText init(String str) {
        return {
            .text = copy_string(str),
            .seconds_elapsed = 0,
        };
    }
    void deinit() {
        ASSERT(text.data != nullptr);
        xfree(text.data);
        *this = {};
    }
};

struct GameState
{
    bool debug_overlay_active;
    s32 debug_show_keypress;
    s32 debug_show_text_rect;

    PlayingVisualConfig playing_visual_configs_per_keymode[Keymode_COUNT];
    // bindings_for_keymodes can not be a KeyBinding.
    // Suppose we bind CTRL for key0 and A for key1. If we press CTRL and then
    // A, key1 will not register. So we must use BeatKey.
    // TODO: Multiple bindings for the scratch. How to do this? Make
    // NewArrayDynamic<BeatKey> instead of just BeatKey.
    NewArrayDynamic<BeatKey> *bindings_for_keymodes[Keymode_COUNT]; // mapped_keys_for_keymodes[keymode][columns for keymode]

    struct {
        SceneEnum last_scene;
        SceneEnum scene;
        SceneFirst scene_first;
        SceneWheel wheel;
        ScenePlaying playing;
    };

    TestArgs test_args;
    SimpleIni simple_ini_config;

    union {
        struct {
            // mabe I'll make macros for these
            // If I wanted to make many bindings for each function I could make it a
            // NewArrayDynamic<KeyBinding> and in the config file I separate the bindings
            // by a space (i'd have to change eat_keyboard_mods because currently it trims
            // the spaces).
            // binding_playing_restart = ANY-GRAVE CTRL-R
            // this would make it either GRAVE or CTRL-R
            NewArrayDynamic<KeyBinding>
                playing_restart,
                scroll_faster,
                scroll_slower,
                debug_virtual_time_faster,
                debug_virtual_time_slower,
                wheel_rate_faster,
                wheel_rate_slower,
                wheel_left,
                wheel_right,
                wheel_up,
                wheel_down,
                wheel_enter,
                wheel_back,
                wheel_remove_and_reload,
                wheel_sort,
                toggle_debug_overlay,
                toggle_show_keypress,
                toggle_debug_show_text_rect,
                playing_seek_negative,
                playing_seek_positive,
                playing_pause,
                wheel_toggle_search,
                wheel_global_audio_offset_plus,
                wheel_global_audio_offset_minus,
                wheel_global_visual_offset_plus,
                wheel_global_visual_offset_minus,

                // _terminator must be the last!
                _terminator;
        };
        NewArrayDynamic<KeyBinding> as_array[26];
    } bindings;

    // Temporary stuff that I don't know where to place yet.
    struct {
        s32 placeholder;

        NewArrayDynamic<FadingText> fading_texts; // The array is on permanent, the strings are malloced
    } testing;

    // struct {
    //     Wheel wheel;
    // } test2;

    struct {
        AccSystemType type;
    } acc_system;
    struct {
        LifeSystemType type;
    } life_system;

    NewArrayDynamic<WorkAsyncData_wheel_add_directory*> work_async_data_arr_wheel_add_directory;
    NewArrayDynamic<WorkAsyncData_load_font_chunk*> work_async_data_arr_load_font_chunk;

    // TODO: Get rendering out of GameState
    Rendering rendering;
};
// verify as_array has the right number of things
STATIC_ASSERT_MSG(STRUCT_OFFSET(GameState, bindings._terminator)-STRUCT_OFFSET(GameState, bindings.as_array[0]) == sizeof(NewArrayDynamic<KeyBinding>)*ARRAY_COUNT(GameState::bindings.as_array), "GameState::bindings.as_array does not have the right number of elements");
// verify _terminator is the last thing
STATIC_ASSERT_MSG(STRUCT_OFFSET(GameState, bindings._terminator)-STRUCT_OFFSET(GameState, bindings.as_array[0])+sizeof(NewArrayDynamic<KeyBinding>) == sizeof(GameState::bindings), "GameState::bindings._terminator is not the last thing");

// static PlatformApi platform;

#endif
