#include "beat_include.hh"

#include "beat_new_song.hh"

// @Malloc
static Timings
copy_timings(Timings *timings)
{
    Timings result = {};
    result.bpms_count = timings->bpms_count;
    smem total_size = result.bpms_count * SSIZEOF_VAL(result.bpms);
    result.bpms = (BeatInfo *)xmalloc(total_size);
    xmemcpy(result.bpms, timings->bpms, total_size);
    return result;
}

static Timings
copy_timings_into_arena(MemoryArena *arena, Timings *timings)
{
    Timings result = {};
    result.bpms_count = timings->bpms_count;
    result.bpms = arena_push_struct_count(arena, BeatInfo, result.bpms_count, arena_flags_nozero());
    xmemcpy_struct_count(result.bpms, timings->bpms, BeatInfo, result.bpms_count);
    return result;
}

static void
copy_metadata_malloc(Metadata *src, Metadata *dest)
{
    if (src->title.data != NULL)            dest->title = copy_string(src->title);
    if (src->title_translit.data != NULL)   dest->title_translit = copy_string(src->title_translit);
    if (src->artist.data != NULL)           dest->artist = copy_string(src->artist);
    if (src->artist_translit.data != NULL)  dest->artist_translit = copy_string(src->artist_translit);
}

static void
copy_metadata_arena(MemoryArena *arena, Metadata *src, Metadata *dest)
{
    if (src->title.data != NULL)            dest->title = arena_push_string_nullter(arena, src->title);
    if (src->title_translit.data != NULL)   dest->title_translit = arena_push_string_nullter(arena, src->title_translit);
    if (src->artist.data != NULL)           dest->artist = arena_push_string_nullter(arena, src->artist);
    if (src->artist_translit.data != NULL)  dest->artist_translit = arena_push_string_nullter(arena, src->artist_translit);
}

static ParserType
get_parser_from_filename(String filename)
{
    String ext = get_file_extension(filename);
    ParserType parser_type = ParserType_INVALID;
    for (s32 i = 0; i < ARRAY_COUNT(parser_type_and_file_extension_list); i++) {
        ParserTypeAndFileExtensionString type_and_ext = parser_type_and_file_extension_list[i];
        if (ext == type_and_ext.ext) {
            parser_type = type_and_ext.type;
            break;
        }
    }
    ASSERT(parser_type >= 0);
    ASSERT(parser_type < ParserType_COUNT);
    return parser_type;
}


static void
free_newsong_timings(Timings *timings)
{
    ASSERT(timings != NULL);

    xfree(timings->bpms);
    zero_struct(timings);
}

static void
free_newsong_notes(Notes *notes)
{
    ASSERT(notes != NULL);

#if 0
    for (s32 idx_col = 0; idx_col < notes->columns; idx_col++) {
        xfree(notes->hold_idx_start[idx_col]);
        xfree(notes->hold_idx_end[idx_col]);
    }
    xfree(notes->hold_idx_start);
    xfree(notes->hold_idx_end);
    xfree(notes->hold_pairs_count);
#endif
    xfree(notes->last_hold_is_released);
    xfree(notes->raw_notes);
    xfree(notes->timestamps);

    free_newsong_timings(&notes->timings);

    // xfree(notes->timestamps); // will get away
    zero_struct(notes);
}

static void
free_newsong_metadata(Metadata *metadata)
{
    ASSERT(metadata != NULL);
#if 1
    free_string(&metadata->title);
    free_string(&metadata->artist);
    free_string(&metadata->title_translit);
    free_string(&metadata->artist_translit);
#else
    // don't free them
#endif
    zero_struct(metadata);
}

static void
free_newsong_diff(Diff *diff)
{
    ASSERT(diff != NULL);
    if (diff == NULL)
        return;

    free_string(&diff->filepath);

    free_newsong_metadata(&diff->metadata);

    free_newsong_notes(&diff->notes);
    // zero_struct(&diff->notes);
    // xfree(diff->notes);
    // diff->notes = NULL;

    zero_struct(diff);
}

static void
free_fileset(Fileset *fileset)
{
    ASSERT(fileset);

#if !FILESET_USE_ARENAS
    free_string(&fileset->filepath);

    for (s32 i = 0; i < fileset->diffs_count; i++) {
        free_newsong_diff(&fileset->diffs[i]);
    }
    xfree(fileset->diffs);
    fileset->diffs = NULL;
    fileset->diffs_count = 0;
#else
    ASSERT(fileset->arena.base);
    xfree(fileset->arena.base);
#endif

    zero_struct(fileset);
}

static bool
timestamp_seconds_close_enough(f64 a, f64 b)
{
    return f64_equal_margin(a, b, NOTE_PRECISION_SECONDS);
}


static s32
find_beat_idx_by_timestamp(Timings *timings, NoteTimestamp ts, s32 beat_idx_start,
                           BeatInfo *maybe_ret_beat_info,
                           bool can_go_reverse)

{
    ASSERT(timings != NULL);
    ASSERT(beat_idx_start >= 0);
    ASSERT(beat_idx_start < timings->bpms_count);
    s32 result = 0;
    bool found = false;
    bool go_reverse = false;

    // At the end of the function, if (found) we'll write to the result
    // beat_info this beat_info here
    BeatInfo *bi = NULL;
    s32 i = 0;
    if (ts.value < 0) {
        found = true;
        result = 0;
        bi = &timings->bpms[0];
    } else {
        for (i = beat_idx_start;
             i < timings->bpms_count;
             i++) {
            bi = &timings->bpms[i];
            if (bi->ts_start <= ts) {
                if (bi->ts_end > ts) {
                    // Beat started before 'time', is active
                    // What we want is this one.
                    found = true;
                    result = i;
                    break;
                } else {
                    // Beat started before 'time', is not active.
                    // What we want is after this one.
                    continue;
                }
            } else {
                // Beat started after 'time', is not active.
                // What we want is before this one.

                // If this hits, there either was an error on the if() or
                // beat_idx_start is too big.
                // If beat_idx_start is too big, let's make another loop
                // decrement i until we find it.
                // If this assert fails, the comparison in the if() msut be the problem
                ASSERT(i == beat_idx_start);
                go_reverse = true;
                break;
            }
        }
        if (go_reverse) {
            // if we ever fail this assert, the code that called this functions has en error
            ASSERT(can_go_reverse);
            for (i--; i >= 0; i--) {
                bi = &timings->bpms[i];
                if (bi->ts_start <= ts) {
                    if (bi->ts_end > ts) {
                        // Beat started before 'time', is active
                        // What we want is this one.
                        found = true;
                        result = i;
                        break;
                    } else {
                        // Beat started before 'time', is not active.
                        // What we want is after this one.
                        ASSERT(false);
                    }
                    ASSERT(found);
                } else {
                    // Beat started after 'time', is not active.
                    // What we want is before this one.
                    continue;
                }
            }
        }
    }
    // if found == false, we hit the end, just confirming it
    ASSERT(found);
    ASSERT(bi != NULL);
    ASSERT(bi == &timings->bpms[result]);

    if (maybe_ret_beat_info != NULL) {
        *maybe_ret_beat_info = *bi;
    }

    ASSERT(result >= 0);
    ASSERT(result < timings->bpms_count);
    return result;
}


// find_row_idx_by_similar_timestamp: clamps "ts" to [0, last_ts_in_the_diff]
// and returns the row idx to the closest timestamp (it floors, if you give 0.7
// and there is ts 0.1 and ts 0.8 it will give you the row idx to 0.1)
static s32
find_row_idx_by_similar_timestamp(Notes *notes, NoteTimestamp ts, s32 start_idx,
                                  bool can_go_reverse)
{
    ASSERT(start_idx >= 0);
    ASSERT(start_idx < notes->rows_count);

    s32 result = -1;
    bool found = false;
    bool go_reverse = false;

    if (ts.value < 0 || ts < notes->timestamps[0]) {
        found = true;
        result = 0;
    } else if (ts > notes->timestamps[notes->rows_count-1]) {
        found = true;
        result = notes->rows_count - 1;
    } else {
        s32 i = 0;
        for (i = start_idx;
             i < notes->rows_count;
             i++) {
            NoteTimestamp this_ts = notes->timestamps[i];
            if (this_ts == ts) {
                found = true;
                result = i;
                break;
            }
            if (this_ts < ts) {
                ASSERT(i + 1 < notes->rows_count);
                NoteTimestamp next_ts = notes->timestamps[i+1];

                if (next_ts > ts) {
                    found = true;
                    result = i;
                    break;
                } else {
                    continue;
                }
            }
            // now this_ts > ts)
            ASSERT(i == start_idx);
            go_reverse = true;
            break;
        }
        if (go_reverse) {
            // If this assert fails, the code that calls this function has an error.
            ASSERT(can_go_reverse);
            for (i--; i >= 0; i--) {
                NoteTimestamp this_ts = notes->timestamps[i];
                if (this_ts == ts) {
                    found = true;
                    result = i;
                    break;
                }
                if (this_ts < ts) {
                    ASSERT(i + 1 < notes->rows_count);
                    NoteTimestamp next_ts = notes->timestamps[i+1];

                    if (next_ts > ts) {
                        found = true;
                        result = i;
                        break;
                    } else {
                        ASSERT(false);
                    }
                }
                // now this_ts > ts
                continue;
            }
        }
    }
    ASSERT(found);

    ASSERT(result >= 0);
    ASSERT(result < notes->rows_count);

    return result;
}

static s32
find_hold_end_idx_with_start_idx(Notes *notes, s32 column, s32 hold_start_idx, NoteType end_note)
{
    ASSERT(hold_start_idx >= 0);
    ASSERT(hold_start_idx < notes->rows_count);
#if 0
    for (s32 idx_pair = 0; idx_pair < notes->hold_pairs_count[column]; idx_pair++) {
        s32 head = notes->hold_idx_start[column][idx_pair];
        if (head == hold_start_idx) {
            return notes->hold_idx_end[column][idx_pair];
        }
    }
#else
    for (s32 idx_row = hold_start_idx; idx_row < notes->rows_count; idx_row++) {
        NoteType *row = notes->raw_notes + idx_row*notes->pitch;
        NoteType note = NOTES_GET_COLUMN(row, column);
        if (note == end_note) {
            return idx_row;
        }
    }
#endif
    ASSERT(!notes->last_hold_is_released[column]);
    return -1;
}

static s32
find_hold_start_idx_with_end_idx(Notes *notes, s32 column, s32 hold_end_idx, NoteType start_note)
{
    ASSERT(hold_end_idx >= 0);
    ASSERT(hold_end_idx < notes->rows_count);
    for (s32 idx_row = hold_end_idx; idx_row >= 0; idx_row--) {
        NoteType *row = notes->raw_notes + idx_row*notes->pitch;
        NoteType note = NOTES_GET_COLUMN(row, column);
        if (note == start_note) {
            return idx_row;
        }
    }
    INVALID_CODE_PATH;
    return -1;
}

static void
print_diff(Diff *diff)
{
    s32 rows_count = diff->notes.rows_count;
    // rows_count = rows_count < 10 ? rows_count : 10;
    for (s32 idx_row = 0; idx_row < rows_count; idx_row++) {
        NoteTimestamp ts = diff->notes.timestamps[idx_row];
        NoteType *row = diff->notes.raw_notes + idx_row*diff->notes.pitch;
        printf("%.2f,", ts.value);
        for (s32 idx_col = 0; idx_col < diff->notes.columns; idx_col++) {
            fputc('0' + NOTES_GET_COLUMN(row, idx_col), stdout);
        }
        fputc('\n', stdout);
    }
}
