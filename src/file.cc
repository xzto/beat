#include "beat_memory.hh"

// TODO: Don't use fopen and fread. Use the OS specific functions. (or SDL)

static smem
get_file_size(FILE *f) {
    smem size, pos;
    pos = ftell(f);
    if (pos < 0) return -1;
    smem err = fseek(f, 0, SEEK_END);
    if (err) return -1;
    size = ftell(f);
    if (size < 0) return -1;
    err = fseek(f, pos, SEEK_SET);
    if (err) return -1;
    return size;
}

static bool
read_entire_file__internal(String filename_not_nullter, MemoryArena *arena, b32 null_terminate,
                           EntireFile *ret_ef)
{
    ASSERT(string_exists_and_is_not_empty(filename_not_nullter));
    zero_struct(ret_ef);
    // TODO: Don't do this. Give a null terminated string instead.
    u8 buf_for_filename[1024]; // TODO: thread safety. require an arena or something..
    ASSERT((smem)sizeof(buf_for_filename)-1 >= filename_not_nullter.len);
    xmemcpy(buf_for_filename, filename_not_nullter.data, filename_not_nullter.len);
    buf_for_filename[filename_not_nullter.len] = 0;
    String filename = {.len=filename_not_nullter.len, .data=buf_for_filename};
    ASSERT(string_nullter_is_actually_the_same_len(filename));

    bool got_arena = (umem)arena != (umem)1;

    FILE *f;
    f = fopen((char*)filename.data, "rb");
    if (!f) {
        fprintf(stderr, "Error opening file '%.*s'!\n", EXPAND_STR_FOR_PRINTF(filename));
        return false;
    }
    bool valid;
    u8 *file_contents = NULL;
    smem size = get_file_size(f);
    if (size < 0) {
        fprintf(stderr, "Problem getting file size '%.*s'!\n", EXPAND_STR_FOR_PRINTF(filename));
        fclose(f);
        return false;
    }
    if (size > GIGABYTES(1)) {
        fprintf(stderr, "File too big '%.*s'!\n", EXPAND_STR_FOR_PRINTF(filename));
        fclose(f);
        return false;
    }

    if (got_arena) {
        if (arena->size - arena->used < size) {
            fprintf(stderr, "File doesn't fit into the arena '%.*s'!!!\n", EXPAND_STR_FOR_PRINTF(filename));
            fclose(f);
            return false;
        }
        file_contents = (u8 *)arena_push_size(arena, null_terminate ? size+1 : size, arena_flags_nozero_align(16));
    } else {
        file_contents = (u8 *)xmalloc(null_terminate ? size+1 : size);
    }
    if (!fread(file_contents, (size_t)size, 1, f)) {
        fprintf(stderr, "Error reading file '%.*s'!\n", EXPAND_STR_FOR_PRINTF(filename));
        fclose(f);
        if (got_arena) {
            arena_free(arena, file_contents);
        } else {
            xfree(file_contents);
        }
        return false;
    }
    fclose(f);
    if (null_terminate) {
        file_contents[size] = 0;
    }

    if (got_arena) {
        ret_ef->arena = arena;
    }
    ret_ef->filename = filename;
    ret_ef->buf.data = file_contents;
    ret_ef->buf.len = size;
    return true;
}

static inline bool
read_entire_file(String filename, EntireFile *ef)
{
    return read_entire_file__internal(filename, (MemoryArena*)1, false, ef);
}

static inline bool
read_entire_file_nullter(String filename, EntireFile *ef)
{
    return read_entire_file__internal(filename, (MemoryArena*)1, true, ef);
}

static inline bool
read_entire_file_into_arena(String filename, MemoryArena *arena, EntireFile *ef)
{
    ASSERT(arena != NULL);
    return read_entire_file__internal(filename, arena, false, ef);
}

static inline bool
read_entire_file_nullter_into_arena(String filename, MemoryArena *arena, EntireFile *ef)
{
    ASSERT(arena != NULL);
    return read_entire_file__internal(filename, arena, true, ef);
}

static void
free_entire_file(EntireFile *ef)
{
    if (ef->arena) {
        // do nothing
    } else {
        xfree(ef->buf.data);
        // xfree(ef->filename.data);
    }
    zero_struct(ef);
}


struct DebugOpenFileHandle {
    // TODO: 'filename' is not allocated; it is the same pointer you passed to
    // debug_open_file. If you are on linux and move the file, 'filename' is
    // not going to change.
    String filename; // null terminated
    FILE *fp;
    smem total_size;
    smem offset;
    bool error;
};

static DebugOpenFileHandle
debug_open_file(char *filename_cstr)
{
    DebugOpenFileHandle handle = {};
    handle.filename = cstr_to_string(filename_cstr);

    smem total_size = 0;
    FILE *fp = fopen(filename_cstr, "rb");
    if (!fp) {
        fprintf(stderr, "Error opening file '%s'!\n", filename_cstr);
        goto rage_quit;
    }
    total_size = get_file_size(fp);
    if (total_size < 0) {
        fprintf(stderr, "Problem getting file size '%s'!\n", filename_cstr);
        goto rage_quit;
    }

    handle.total_size = total_size;
    handle.fp = fp;
    return handle;
rage_quit:
    if (fp) {
        fclose(fp);
        fp = 0;
    }
    handle.error = true;
    return handle;
}

static void
debug_close_file(DebugOpenFileHandle *handle)
{
    fclose(handle->fp);
    handle->fp = 0;
    zero_struct(handle);
}

static smem
debug_read_file_into_buffer(DebugOpenFileHandle *handle, smem offset, smem size_to_read, void *buf)
{
    ASSERT(handle->fp);
    ASSERT(size_to_read >= 0);
    ASSERT(offset >= 0 && offset <= handle->total_size);
    if (handle->error) {
        ASSERT(!"Tried to read a file with errors.");
        return -1;
    }

    if (offset + size_to_read > handle->total_size) {
        fprintf(stderr, "Tried to read past the end of the file.\n");
        handle->error = true;
        return -1;
    }

    if (fseek(handle->fp, offset, SEEK_SET) != 0) {
        fprintf(stderr, "Error on fseek file '%.*s'!\n", EXPAND_STR_FOR_PRINTF(handle->filename));
        handle->error = true;
        return -1;
    }
    handle->offset = offset;

    // TODO: Not fread
    smem elems_read = (smem)fread(buf, (size_t)size_to_read, 1, handle->fp);
    smem bytes_read = elems_read * size_to_read;
    if (elems_read == 0 && size_to_read != 0) {
        fprintf(stderr, "Error reading file '%.*s'!\n", EXPAND_STR_FOR_PRINTF(handle->filename));
        handle->error = true;
        return -1;
    }
    if (bytes_read != size_to_read) {
        fprintf(stderr, "File didn't read all the bytes: %ld/%ld\n", bytes_read, size_to_read);
        handle->error = true;
        return -1;
    }
    handle->offset += bytes_read;
    return bytes_read;
}

static smem
debug_read_file_into_buffer_and_nullter(DebugOpenFileHandle *handle, smem offset, smem size_to_read, void *buf)
{
    smem bytes_read = debug_read_file_into_buffer(handle, offset, size_to_read, buf);
    if (bytes_read < 0) {
        return -1;
    }
    ((u8*)buf)[bytes_read] = 0;
    return bytes_read;
}

// TODO: Create a debug_read_file_allocate__internal just like
// read_entire_file__internal and a separate
// debug_read_file_allocate_into_arena, debug_read_file_allocate_malloc,
// debug_read_file_allocate_and_nullter_into_arena, etc.
static smem
debug_read_file_allocate(DebugOpenFileHandle *handle, smem offset, smem size_to_read, void **ret_ptr,
                         MemoryArena *arena = (MemoryArena*)1)
{
    ASSERT(size_to_read >= 0);
    ASSERT(offset >= 0 && offset < handle->total_size);
    *ret_ptr = 0;
    bool got_arena = ((umem)arena != (umem)1);

    if (handle->error) {
        ASSERT(!"Tried to read a file with errors.");
        return -1;
    }
    void *buf = 0;
    if (got_arena) {
        if (arena->size - arena->used < size_to_read) {
            fprintf(stderr, "File doesn't fit into the arena '%.*s'!!!\n", EXPAND_STR_FOR_PRINTF(handle->filename));
            return -1;
        }
        buf = arena_push_size(arena, size_to_read, arena_flags_nozero_align(16));
    } else {
        buf = xmalloc(size_to_read);
    }
    smem bytes_read = debug_read_file_into_buffer(handle, offset, size_to_read, buf);
    if (bytes_read != size_to_read) {
        if (got_arena) {
            arena_free(arena, buf);
        } else {
            xfree(buf);
        }
        ASSERT(handle->error);
        return -1;
    }
    *ret_ptr = buf;
    return bytes_read;
}

static smem
debug_read_file_allocate_and_nullter(DebugOpenFileHandle *handle, smem offset, smem size_to_read, void **ret_ptr,
                                     MemoryArena *arena = (MemoryArena*)1)
{
    ASSERT(size_to_read >= 0);
    ASSERT(offset >= 0 && offset < handle->total_size);
    *ret_ptr = 0;

    bool got_arena = ((umem)arena != (umem)1);

    void *buf = 0;
    if (got_arena) {
        if (arena->size - arena->used < size_to_read+1) {
            fprintf(stderr, "File doesn't fit into the arena '%.*s'!!!\n", EXPAND_STR_FOR_PRINTF(handle->filename));
            return -1;
        }
        buf = arena_push_size(arena, size_to_read+1, arena_flags_nozero_align(16));
    } else {
        buf = xmalloc(size_to_read+1);
    }
    smem bytes_read = debug_read_file_into_buffer_and_nullter(handle, offset, size_to_read, buf);
    if (bytes_read != size_to_read) {
        if (got_arena) {
            arena_free(arena, buf);
        } else {
            xfree(buf);
        }
        ASSERT(handle->error);
        return -1;
    }
    *ret_ptr = buf;
    return bytes_read;
}

