#ifndef BEAT_AUDIO_H
#define BEAT_AUDIO_H

#include "minimp3/minimp3_my_include_ex.h"

#include <portaudio.h>
// #include <pa_linux_alsa.h>

namespace audio {

struct AudioDecoderId {
    u64 gen;
    static inline AudioDecoderId INVALID() { return {}; }
    inline bool is_valid() { return gen != AudioDecoderId::INVALID().gen; }
    inline bool operator == (AudioDecoderId const &other) { return gen == other.gen; }
    inline bool operator != (AudioDecoderId const &other) { return !(*this == other); }
    inline static AudioDecoderId generate();
};

// TODO: Ability to loop sounds.

// You must not keep pointers to an AudioDecoder without a lock to global_audio_state.decoder.shared_mutex. You can store the AudioDecoderId safely.
struct AudioDecoder {
    /// Unique id for every audio decoder that i make with debug__make_audio_decoder
    AudioDecoderId id;

    struct Specific {
        enum class Kind {
            mp3,
        };
        Kind kind;
        union {
            mp3dec_ex_t *mp3; // on the heap
        };
    };
    Specific specific;

    /// `sound_offset_seconds`, `channels`, `samples_per_second` and `total_sample_count` stay constant until the decoder is destroyed.
    f64 sound_offset_seconds; // TODO: Maybe this should not be part of the decoder.
    s32 channels, samples_per_second;
    s64 total_sample_count; // calculated before it starts decoding.
    // f64 total_seconds_length;

    struct LockedData {
        bool debug__called_start_playing_at_least_once;
        bool is_paused;
        // TODO: is_looping

        // the mixer writes these
        struct MixerData {
            s64 total_read_sample_idx;
            s32 buffer_read_idx; // The decoder reads this once before decoding.
            f32 buffer_read_delta_idx;

            f64 curr_sound_time; // does not take sound_offset_seconds into consideration
            f32 speed_to_play;
            bool is_finished; // the mixer writes to this when it finishes playing

            inline bool operator == (MixerData const &other) {
                return total_read_sample_idx == other.total_read_sample_idx
                    && buffer_read_idx       == other.buffer_read_idx
                    && buffer_read_delta_idx == other.buffer_read_delta_idx
                    && curr_sound_time       == other.curr_sound_time
                    && speed_to_play         == other.speed_to_play;
            }
            inline bool operator != (MixerData const &other) { return !(*this == other); }
        } mixer;

        // the decoder writes these
        struct DecoderData {
            s64 total_write_sample_idx;
            s32 buffer_write_idx;
            bool did_first_write;
            inline bool operator == (DecoderData const &other) {
                return total_write_sample_idx == other.total_write_sample_idx
                    && buffer_write_idx       == other.buffer_write_idx
                    && did_first_write        == other.did_first_write;
            }
            inline bool operator != (DecoderData const &other) { return !(*this == other); }
        } decoder;
    };
    // This stuff needs a lock.
    struct {
        // To change the play position I need a mutex.
        // I use this mutex if I need to read something (locked), do something (mixer / decoding) (unlocked) and then write it (locked) and only write it the previosu values didn't change. If the previous values changed it means I seeked.
        TicketMutex mutex;
        LockedData data;
    } locked;

    // The decoder buffer size should be much bigger than the mixer buffer size
    // (unless `total_sample_count` is small.).
    // TODO: There's a bug in the decoder code. If total_write_sample_idx is too far from total_read_sample_idx, it will be offsync by `ring_buffer_sample_count`.
    // static const s32 ring_buffer_sample_count = 64*512; // ~700ms for 44100Hz. // I probably don't need that much.
    static const s32 MAXIMUM_RING_BUFFER_SAMPLE_COUNT = 64*512; // ~700ms for 44100Hz. // I probably don't need that much.
    s32 ring_buffer_sample_count; // MAXIMUM(1, MINIMUM(MAXIMUM_RING_BUFFER_SAMPLE_COUNT, total_sample_count))
    static const s32 MAX_CHANNELS = 2;
    static const smem RING_BUFFER_ALIGN = 16;
    struct SampleBuffer {
        // alignas(16) s16 data[MAX_CHANNELS*ring_buffer_sample_count];
        s16 *data; // size is [ring_buffer_sample_count * num_channels] // On the heap
    };

    // No lock for accessing these buffers.
    // Both the reader and the writer access `ring_buffer_samples`
    SampleBuffer ring_buffer_samples;

    // Only the writer accesses `secondary_buffer`
    // The decoder will use this to store the samples before writing them to the ring buffer
    // TODO This is probably unnecessary.
    SampleBuffer secondary_buffer;

    void deinit();
};

// STATIC_ASSERT(sizeof(AudioDecoder) <= 128+8);

typedef v2 PannedVolume;

// A global
struct AudioState {
    bool is_initialized;
    MemoryArena arena; // This is permanent data

    struct {
        // Unique access to modify `decoder_list` and `next_generational_id`.
        // Shared access to read from the list.
        SharedTicketMutex shared_mutex;
        u64 next_generational_id;
        u64 get_next_gen_id() {
            ASSERT(next_generational_id < std::numeric_limits<decltype(next_generational_id)>::max());
            return next_generational_id++;
        }

        // TODO: FreeAndActiveSinglyLinkedList<AudioDecoder> list;
        NewArrayDynamic<AudioDecoder> active_list;
    } decoders;

    PannedVolume master_volume;
    s32 output_buffer_samples_per_second;


    double test_current_time;
    double test_output_buffer_dac_time;
    double test_input_buffer_adc_time;

    PaStream *pa_stream;
};

} // namespace audio

#endif
