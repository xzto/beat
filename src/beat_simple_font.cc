#include "beat_include.hh"

// NOTE: Can't use this for the final game because I want to support emojis.
// This is a simple test to start drawing fonts.

// TODO: Kernings

// TODO: get_font_string_length or something.
// There is two ways to do this:
// A: Call the code that draws the string twice. On the first time it doesn't actually draw anything.
// B: Draw the string first and then modify the render entries' position.
// Pros ans cons
// A:
//  - that is stupid
//  - slower
// B:
//  + Probably faster because there's no need to pull information about glyphs twice.

// NOTE:
// `FONT_CHUNK_SIZE` small => loads fast, renders slow when a string contains many chunks
// `FONT_CHUNK_SIZE` big   => loads slow, renders fast when a string contains many chunks
// Small chunk sizes makes rendering strings with many different chunks slower because each chunk would be a different OpenGL texture and each different OpenGL texture would make a different render command.
// One example: 27198 vertices, (SCENE_WHEEL_FONT_SIZE=16, search for "little riddle" and then cancel search)
//  chunk size 256 =  868 render cmds, 7.4ms
//  chunk size  64 = 1513 render cmds, 11.8ms
//  chunk size  16 = 3267 render cmds, 23.3ms

#define FONT_CHUNK_SIZE 128

static void
get_font_chunk_region_for_codepoint(u32 codepoint, u32 *ret_first_codepoint, u32 *ret_count_codepoint)
{
    *ret_first_codepoint = (codepoint / FONT_CHUNK_SIZE) * FONT_CHUNK_SIZE;
    *ret_count_codepoint = FONT_CHUNK_SIZE;
}

static void
queue_font_chunk_for_codepoint(Font *font, u32 codepoint)
{
    // can only be called from the main thread

    // TODO: if BEAT_SLOW, verify we don't have it loaded already.
    for (smem idx_queued = 0; idx_queued < font->chunks_queued.len; idx_queued++) {
        auto *queued = &font->chunks_queued.data[idx_queued];
        if (BETWEEN(codepoint, queued->first_codepoint, queued->first_codepoint + queued->count_codepoint-1)) {
            // already queued
            return;
        }
    }
    // not already queued
    FontChunkQueued new_queued = {};
    get_font_chunk_region_for_codepoint(codepoint, &new_queued.first_codepoint, &new_queued.count_codepoint);
    new_queued.id = font->next_chunk_queued_id++;
    font->chunks_queued.push(new_queued);
}

static FontGlyph *
codepoint_to_glyph(Font *font, u32 codepoint, FontChunk **maybe_ret_chunk=0)
{
    // TODO: Outer loop for all loaded fonts
    for (smem idx_chunk = 0; idx_chunk < font->chunks.len; idx_chunk++) {
        auto *chunk = &font->chunks.data[idx_chunk];
        ASSERT_SLOW(chunk->first_codepoint % FONT_CHUNK_SIZE == 0 && chunk->count_codepoint == FONT_CHUNK_SIZE);
        if (BETWEEN(codepoint, chunk->first_codepoint, chunk->first_codepoint + chunk->count_codepoint-1)) {
            // we found a chunk that has this codepoint
            auto glyph_idx = chunk->codepoint_to_glyph_idx[codepoint - chunk->first_codepoint];
            if (glyph_idx) {
                ASSERT(ABC(glyph_idx, chunk->glyphs_count));
                auto *glyph = &chunk->glyphs[glyph_idx];
                ASSERT(glyph->codepoint == codepoint);
                if (maybe_ret_chunk) *maybe_ret_chunk = chunk;
                return glyph;
            } else {
                // TODO `continue` inner loop; the outer loop will loop on all fonts;
                if (maybe_ret_chunk) *maybe_ret_chunk = chunk;
                return 0;
            }
        }
    }
    // we didn't find a chunk that has this codepoint
    queue_font_chunk_for_codepoint(font, codepoint);
    if (maybe_ret_chunk) *maybe_ret_chunk = 0;
    return 0;
}

// Gets the advance. Multiply this result by your font height.
static f32
get_advance(FontGlyph *first, FontGlyph *second)
{
    // If there's no first character there is no advance.
    if (!first)
        return 0;
    // If there's no second character, advance by the first's advance.
    if (!second)
        return first->advance;

    // TODO: kernings
    f32 delta = 0;
    bool found_ker = false;
    if (first->kernings_count > 0) {
        FontKerning *kernings = first->kernings;
        ASSERT(kernings != NULL);
        u32 second_codepoint = second->codepoint;
        FontKerning *ker = 0;
        do_binary_search(kernings, first->kernings_count, NULL, &ker, other_codepoint, second_codepoint);
        if (ker) {
            delta = ker->delta;
        }
    }
    return first->advance + delta;
}

static void
blit_to_atlas_bitmap_invert_y(u8 *atlas_bitmap, s32 atlas_bitmap_size_x, s32 atlas_bitmap_size_y, s32 atlas_bitmap_pitch,
                              u8 *src_bitmap_topdown, s32 src_width, s32 src_height, s32 src_pitch,
                              s32 atlas_element_position_x, s32 atlas_element_position_y)
{
    // atlas_bitmap is bottom-up. src_bitmap_topdown is topdown.
    s32 dest_x_min = atlas_element_position_x;
    s32 dest_x_max = dest_x_min + src_width;
    if (dest_x_min < 0 || dest_x_min >= atlas_bitmap_size_x)
        INVALID_CODE_PATH;
    if (dest_x_max <= dest_x_min || dest_x_max > atlas_bitmap_size_x)
        INVALID_CODE_PATH;

    s32 dest_y_min = atlas_element_position_y;
    s32 dest_y_max = dest_y_min + src_height;
    if (dest_y_min < 0 || dest_y_min >= atlas_bitmap_size_y)
        INVALID_CODE_PATH;
    if (dest_y_max <= dest_y_min || dest_y_max > atlas_bitmap_size_y)
        INVALID_CODE_PATH;

    for (s32 dest_y = dest_y_min, src_y = 0; dest_y < dest_y_max; dest_y++, src_y++) {
        u8 *dest_row = atlas_bitmap + dest_y*atlas_bitmap_pitch + dest_x_min;
        u8 *src_row  = src_bitmap_topdown + ((src_height-1-src_y)*src_pitch) + 0;
        xmemcpy(dest_row, src_row, src_width);
    }
}


#include "stb/stb_truetype.h"

static void
make_font_chunk(FontChunk *ret_chunk,
                FontMakeinfo *make_info,
                u32 first_codepoint, u32 count_codepoint,
                MemoryArena *temp_arena)
{
    ASSERT(count_codepoint == FONT_CHUNK_SIZE);
    ASSERT(first_codepoint % FONT_CHUNK_SIZE == 0);
    ASSERT(temp_arena);
    SCOPED_TEMP_ARENA(temp_arena);

    *ret_chunk = {};
    FontChunk this_chunk = {};
    this_chunk.first_codepoint = first_codepoint;
    this_chunk.count_codepoint = count_codepoint;

    ASSERT(count_codepoint > 0);

    NewArrayDynamic<FontGlyph> chunk_glyphs = {}; // @FontMalloc
    chunk_glyphs.reserve(this_chunk.count_codepoint+1); // might not use everything
    chunk_glyphs.push({}); // the invalid glyph of index 0

    struct FontGlyphBitmapTemp {
        s32 width, height;
        u8 *bitmap_topdown;
    };

    NewArrayDynamic<FontGlyphBitmapTemp> chunk_glyphs_bitmap_temp = {};
    chunk_glyphs_bitmap_temp.arena = temp_arena;
    chunk_glyphs_bitmap_temp.reserve(chunk_glyphs.cap);
    chunk_glyphs_bitmap_temp.push({});

    NewArrayDynamic<s32> chunk_codepoint_to_glyph_idx = {};
    chunk_codepoint_to_glyph_idx.reserve(this_chunk.count_codepoint);

    for (u32 codepoint = this_chunk.first_codepoint;
         codepoint < this_chunk.first_codepoint + this_chunk.count_codepoint;
         codepoint++) {
        s32 stbindex = stbtt_FindGlyphIndex(&make_info->stbinfo, (int)codepoint);
        if (!stbindex) {
            chunk_codepoint_to_glyph_idx.push(0);
            continue;
        }

        s32 stb_advance=0, stb_left_side_bearing=0;
        stbtt_GetGlyphHMetrics(&make_info->stbinfo, stbindex, &stb_advance, &stb_left_side_bearing);
        f32 my_advance = make_info->stb_scale * (f32)stb_advance / make_info->font_height_px;

        u8 *stb_bitmap = 0;
        s32 stb_bitmap_width = 0;
        s32 stb_bitmap_height = 0;
        v2 my_offset;
        {
            s32 w=0, h=0, ix0=0, iy0=0;
            // TODO: Don't let stbtt allocate! Give it a buffer.
            stb_bitmap = stbtt_GetGlyphSDF(&make_info->stbinfo, make_info->stb_scale, stbindex,
                                           make_info->sdf_pad, make_info->sdf_onedge_value, make_info->sdf_pixel_dist_scale,
                                           &w, &h, &ix0, &iy0);
            stb_bitmap_width = w;
            stb_bitmap_height = h;
            my_offset = V2((f32)ix0 / make_info->font_height_px, (f32)iy0 / make_info->font_height_px);
        }
        ASSERT(stb_bitmap_width >= 0);
        ASSERT(stb_bitmap_height >= 0);

        FontGlyph this_glyph = {};
        this_glyph.codepoint = codepoint;
        // this_glyph.chunk_idx = (s32)chunks->len;
        // this_glyph.total_glyph_idx = *total_glyph_count;
        // *total_glyph_count += 1;
        this_glyph.offset = my_offset;
        this_glyph.advance = my_advance;

        this_glyph.kernings_count =0; // TODO kernings
        this_glyph.kernings =0; // TODO kernings

#if 0
        this_glyph.bitmap_width = (s16)stb_bitmap_width;
        this_glyph.bitmap_height = (s16)stb_bitmap_height;
        s32 my_bitmap_pitch = stb_bitmap_width;
        align_forward(&my_bitmap_pitch, SIMPLE_FONT_BITMAP_ROW_ALIGNMENT);
        this_glyph.bitmap_pitch = (s16)my_bitmap_pitch;
        // printf("%c %d %d\n", this_glyph.codepoint, this_glyph.bitmap_width, this_glyph.bitmap_pitch);
        this_glyph.bitmap = 0; // i'll copy it later if it exists

        if (stb_bitmap) {
            STATIC_ASSERT(SIMPLE_FONT_BYTES_PER_PIXEL == 1);
            u8 *my_bitmap = (u8*)xmalloc(my_bitmap_pitch * stb_bitmap_height);
            ASSERT(my_bitmap);
            if (stb_bitmap_width == my_bitmap_pitch) {
                xmemcpy(my_bitmap, stb_bitmap, stb_bitmap_width * stb_bitmap_height);
            } else {
                for (s32 y = 0; y < stb_bitmap_height; y++) {
                    u8 *my_row = my_bitmap + y * my_bitmap_pitch;
                    u8 *stb_row = stb_bitmap + y * stb_bitmap_width;
                    xmemcpy(my_row, stb_row, stb_bitmap_width);
                }
            }
            this_glyph.bitmap = my_bitmap;
        }
        stbtt_FreeSDF(stb_bitmap, 0);
        stb_bitmap = 0;
#else
        FontGlyphBitmapTemp glyph_bitmap_temp = {};
        glyph_bitmap_temp.width = stb_bitmap_width;
        glyph_bitmap_temp.height = stb_bitmap_height;
        glyph_bitmap_temp.bitmap_topdown = stb_bitmap;
#endif
        smem this_glyph_idx = chunk_glyphs.len;
        ASSERT(this_glyph_idx > 0);
        chunk_glyphs.push(this_glyph);
        chunk_glyphs_bitmap_temp.push(glyph_bitmap_temp);
        chunk_codepoint_to_glyph_idx.push((s32)this_glyph_idx);
    }
    ASSERT(chunk_codepoint_to_glyph_idx.len == this_chunk.count_codepoint);
    ASSERT(chunk_glyphs.len == chunk_glyphs_bitmap_temp.len);

    // Make atlas. TODO try to be smart about packing it. Currently it is very dumb and wastes a bit of memory.
    {
        v2s atlas_bitmap_size = {};
        smem count_valid_bitmaps = 0;
        s32 atlas_element_pad_x = 2;
        for (smem idx_glyph = 1; idx_glyph < chunk_glyphs.len; idx_glyph++) {
            auto *g = &chunk_glyphs.data[idx_glyph];
            auto *gt = &chunk_glyphs_bitmap_temp.data[idx_glyph];
            if (gt->bitmap_topdown) {
                count_valid_bitmaps++;
                g->has_bitmap = true;
                g->atlas_element_position.x = atlas_bitmap_size.x;
                g->atlas_element_position.y = 0;
                g->atlas_element_size.x = gt->width;
                g->atlas_element_size.y = gt->height;
                atlas_bitmap_size.x += gt->width + atlas_element_pad_x;
                atlas_bitmap_size.y = MAXIMUM(atlas_bitmap_size.y, gt->height);
            }
        }
        if (count_valid_bitmaps > 0) {
            ASSERT(atlas_bitmap_size.x > 0 && atlas_bitmap_size.y > 0);
            s32 atlas_bitmap_pitch = atlas_bitmap_size.x;
            align_forward(&atlas_bitmap_pitch, SIMPLE_FONT_BITMAP_ROW_ALIGNMENT);
            smem atlas_memory_size = atlas_bitmap_pitch * atlas_bitmap_size.y;
            u8 *atlas_memory = (u8*)xmalloc(atlas_memory_size, xmalloc_flags_zero()); // @Malloc
            for (smem idx_glyph = 1; idx_glyph < chunk_glyphs.len; idx_glyph++) {
                auto *g = &chunk_glyphs.data[idx_glyph];
                auto *gt = &chunk_glyphs_bitmap_temp.data[idx_glyph];
                if (gt->bitmap_topdown) {
                    blit_to_atlas_bitmap_invert_y(atlas_memory, atlas_bitmap_size.x, atlas_bitmap_size.y, atlas_bitmap_pitch,
                                                  gt->bitmap_topdown, gt->width, gt->height, gt->width,
                                                  g->atlas_element_position.x, g->atlas_element_position.y);
                    stbtt_FreeSDF(gt->bitmap_topdown, 0);
                    gt->bitmap_topdown = 0;
                }
            }
            this_chunk.atlas_bitmap = atlas_memory;
            this_chunk.atlas_bitmap_pitch = atlas_bitmap_pitch;
            this_chunk.atlas_bitmap_size = atlas_bitmap_size;
        }
    }

    this_chunk.codepoint_to_glyph_idx = chunk_codepoint_to_glyph_idx.data;
    this_chunk.glyphs_count = (s32)chunk_glyphs.len;
    this_chunk.glyphs = chunk_glyphs.data;
    ASSERT(chunk_codepoint_to_glyph_idx.len == this_chunk.count_codepoint);
    // chunks->push(this_chunk);
    *ret_chunk = this_chunk;
}

static bool
new_make_simple_font_from_ttf_buffer(EntireFile ef, f32 font_height_px, Font *ret_font, MemoryArena *temp_arena)
{
    ASSERT(temp_arena);
    SCOPED_TEMP_ARENA(temp_arena);

    ASSERT(font_height_px > 0);
    // TODO: Put everything in one array for every type of thing.

    stbtt_fontinfo stbfont;
    int stbvalid = stbtt_InitFont(&stbfont, ef.buf.data, stbtt_GetFontOffsetForIndex(ef.buf.data, 0));
    if (!stbvalid) {
        ASSERT(!"failed stbtt_InitFont");
        return false;
    }

    s32 stb_ascent, stb_descent, stb_line_gap;
    stbtt_GetFontVMetrics(&stbfont, &stb_ascent, &stb_descent, &stb_line_gap);
    float stb_scale = stbtt_ScaleForPixelHeight(&stbfont, font_height_px);
    f32 my_ascent   = ((f32)stb_ascent  *stb_scale/font_height_px);
    f32 my_descent  = ((f32)stb_descent *stb_scale/font_height_px);
    f32 my_line_gap = ((f32)stb_line_gap*stb_scale/font_height_px);

    u8 sdf_onedge_value = 180;
    u8 sdf_pad = 5;
    f32 sdf_pixel_dist_scale = (f32)sdf_onedge_value / (f32)sdf_pad;


    Font font = {};
    font.pixel_height_stbtt = font_height_px;
    font.line_gap = my_line_gap;
    font.ascent = my_ascent;
    font.descent = my_descent;
    font.sdf_onedge_value = sdf_onedge_value;
    font.sdf_pad = sdf_pad;
    font.sdf_pixel_dist_scale = sdf_pixel_dist_scale;

    font.chunks = {}; // @Malloc

    font.make_info.ef = ef;
    font.make_info.stbinfo = stbfont;
    font.make_info.stb_scale = stb_scale;
    font.make_info.font_height_px = font.pixel_height_stbtt;
    font.make_info.sdf_pad = font.sdf_pad;
    font.make_info.sdf_onedge_value = font.sdf_onedge_value;
    font.make_info.sdf_pixel_dist_scale = font.sdf_pixel_dist_scale;

    font.chunks_queued = {}; // @Malloc
    font.next_chunk_queued_id = 2;

    // Load the first chunks (ascii).
    u32 const to_load_max_codepoint = 0x100;
    for (u32 first_codepoint = 0; first_codepoint < to_load_max_codepoint; first_codepoint += FONT_CHUNK_SIZE) {
        FontChunk this_chunk;
        make_font_chunk(&this_chunk,
                        &font.make_info,
                        first_codepoint, FONT_CHUNK_SIZE,
                        temp_arena);
        font.chunks.push(this_chunk);
        font.total_glyph_count += this_chunk.glyphs_count - 1;
    }

    *ret_font = font;

    return true;
}
