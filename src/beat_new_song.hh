#ifndef BEAT_NEW_SONG_H
#define BEAT_NEW_SONG_H

// TODO: Also make another argument for the scratch in 7kp1
#define DO_ALL_KEYMODE_THINGS()                          \
    DO_KEYMODE_THING(4k   , 4 , "4K")   /*Keymode_4k*/   \
    DO_KEYMODE_THING(5k   , 5 , "5K")   /*Keymode_5k*/   \
    DO_KEYMODE_THING(5kp1 , 6 , "5K+1") /*Keymode_5kp1*/ \
    DO_KEYMODE_THING(6k   , 6 , "6K")   /*Keymode_6k*/   \
    DO_KEYMODE_THING(7k   , 7 , "7K")   /*Keymode_7k*/   \
    DO_KEYMODE_THING(7kp1 , 8 , "7K+1") /*Keymode_7kp1*/ \
    DO_KEYMODE_THING(8k   , 8 , "8K")   /*Keymode_8k*/   \
    DO_KEYMODE_THING(9k   , 9 , "9K")   /*Keymode_9k*/

enum Keymode {
    Keymode_Invalid = 0,
#define DO_KEYMODE_THING(keymode, columns, fancy_str) Keymode_##keymode,
    DO_ALL_KEYMODE_THINGS()
#undef DO_KEYMODE_THING
    Keymode_COUNT,
};

static constexpr s32 keymode_to_columns_table[Keymode_COUNT] = {
#define DO_KEYMODE_THING(keymode, columns, fancy_str) [Keymode_##keymode] = columns,
    DO_ALL_KEYMODE_THINGS()
#undef DO_KEYMODE_THING
};

// "7K+1"
static const String keymode_to_fancy_string_table[Keymode_COUNT] = {
#define DO_KEYMODE_THING(keymode, columns, fancy_str) [Keymode_##keymode] = string_literal(fancy_str),
    DO_ALL_KEYMODE_THINGS()
#undef DO_KEYMODE_THING
};

// "7kp1"
static const String keymode_to_simple_string_table[Keymode_COUNT] = {
#define DO_KEYMODE_THING(keymode, columns, fancy_str) [Keymode_##keymode] = string_literal(#keymode),
    DO_ALL_KEYMODE_THINGS()
#undef DO_KEYMODE_THING
};

// "keymode_7kp1"
static const String keymode_to_ini_string_table[Keymode_COUNT] = {
#define DO_KEYMODE_THING(keymode, columns, fancy_str) [Keymode_##keymode] = string_literal(STRINGIFY(keymode_##keymode)),
    DO_ALL_KEYMODE_THINGS()
#undef DO_KEYMODE_THING
};

static inline bool keymode_is_valid(Keymode k)
{
    return k > Keymode_Invalid && k < Keymode_COUNT;
}



// @HardcodedJudge
// This is the limit for the specified judge. if offset < JUDGE_MARV, it's a marv.
#define JUDGE_MARV_MAX 0.0225
#define JUDGE_PERF_MAX 0.045
#define JUDGE_GREAT_MAX 0.090
#define JUDGE_GOOD_MAX 0.135
#define JUDGE_BAD_MAX 0.180
#define JUDGE_MISS_MIN JUDGE_BAD_MAX

#define SCORE_FOR_MARV  (2.0)
#define SCORE_FOR_PERF  (2.0)
#define SCORE_FOR_GREAT (1.0)
#define SCORE_FOR_GOOD  (0.0)
#define SCORE_FOR_BAD   (-3.5)
#define SCORE_FOR_MISS  (-5.5)
#define SCORE_FOR_MINE_EXPLODE (-5.5)

// @ParserSmMaybeFutureProblem1 If I change this, I have to make sure
// PARSER_SM_NOTE_TO_NEW_NOTE doesn't break 
// @NoteType
enum NoteType : u8 {
    NOTE_BLANK = 0,

    NOTE_TAP,
    NOTE_MINE,
    NOTE_HOLD,
    NOTE_HOLD_END, // not used at vn
    NOTE_SMROLL,
    NOTE_SMROLL_END, // not used at vn

    // Thoughts:
    // I can think of more than types of holds. In beatoraja you can select between LN, CN and HCN.
    // LN: it's just like in stepmania. you can release in the middle  and regrab and the lift is not timed.
    // CN: it's just like LN except the lift is timed. If you break in the middle you lose a fixed amount of life.
    // HCN: If you break in the middle you keep losing life until you regrab it. (i think osu also does this)
    // do_acc_system() {
    // 
    // }
#if 0
    // I think this is better than having two separate types of holds. We have store only one type of hold. We store the default type for the file (files from .osu and .ojn will default to have osu lifts and files from .sm will default to not having it). The user can configure that in the wheel.
    static void do_acc_system(InputActionStuff ias) {
        if (acc_do_osu_lifts) {
            switch (ias.type) {
            case INPUT_ACTION_HOLD_BREAK:  total_acc += acc_for_osu_break; break;
            case INPUT_ACTION_HOLD_FINISH: total_acc += acc_for_ias(); break;
            }
        } else {
            switch (ias.type) {
            case INPUT_ACTION_HOLD_BREAK:  total_acc -= minus_acc_for_osu_break; break;
            case INPUT_ACTION_HOLD_FINISH: /* do nothing */ break;
            }
        }
    }
    static void do_life_system(InputActionStuff ias) {
        bool life_do_hcns = acc_do_osu_lifts;
        bool life_do_cns = !life_do_hcns && true;
        if (life_do_hcns) {
            switch ias.type {
            case INPUT_ACTION_HOLD_BREAK:          do_hcn_break(); break;
            case INPUT_ACTION_HOLD_BREAK_CONTINUE: do_hcn_break_continue; break;
            }
        } else if (life_do_cns) {
            switch ias.type {
            case INPUT_ACTION_HOLD_BREAK:          do_cn_break(); break;
            case INPUT_ACTION_HOLD_BREAK_CONTINUE: /*do nothing*/ break;
            }
        }
    }
    ////
    InputActionStuff action = get_input_action();
    // Might need to redefine HOLD_BREAK to HOLD_BREAK_MIDDLE and HOLD_BREAK_END
    switch (action.type) {
    case INPUT_ACTION_HOLD_BREAK:
    }
    do_post_process(vn, &action);
    ////
    static void do_post_process(vn, action) {
        if (osu_lifts_mode) {
            // this thing here I could make either in post_process or on get_input_action_xx
            if ((vn->type == NOTE_HOLD) &&
                (vn->flags & RELEASED) &&
                (to_real_ts(curr_ts - vn->last_input_ts, rate) < max_time_to_regrab)) {
                vn->flags |= VN_FINISHED_INPUT;
                do_acc_for_osu_failed_to_regrab_in_time_before_end();
            }
        }
    }
#endif
    // Do I really need one type for timed lifts and another type for untimed lifts? Yes because in
    // stepmania you can have a file with lifts and holds at the same time. Well, we could just
    // ignore that and only have one type of hold. The chart would have information about what hold
    // type it had.

    // TODO: TIMED_LN
    // Maybe I'll delete this.
    // NOTE_TIMED_LN,
    // NOTE_TIMED_LN_END,

    // NOTE_FAKE,

#if 0
    // TODO: Do this and remove the other one, make a table for parser_sm to translate between PARSER_SM_NOTE_XXX and NOTE_XXX.
    // The old thing above only supports one type of hold that is configurable globally or per file.
    // The new thing will support both types in one file (if the file supports it) by default, or forced untimed/timed.
#if 0
    switch (note) {
    case NOTE_UNTIMED_HOLD_HEAD: {
        if (mode == HOLD_MODE_DEFAULT || HOLD_MODE_UNTIMED) {
            do_untimed_head();
        } else if (mode == HOLD_MODE_TIMED) {
            do_timed_head();
        } else ASSERT(false);
    } break;
    case NOTE_TIMED_HOLD_HEAD: {
        if (mode == HOLD_MODE_DEFAULT || HOLD_MODE_TIMED) {
            do_timed_head();
        } else if (mode == HOLD_MODE_UNTIMED) {
            do_untimed_head();
        } else ASSERT(false);
    } break;
    case ...:
    }
#endif
    // This will support .sm files with both untimed and timed holds (lifts).
    NOTE_UNTIMED_HOLD_HEAD,
    NOTE_UNTIMED_HOLD_TAIL,

    NOTE_SM_ROLL_HEAD,
    NOTE_SM_ROLL_TAIL,

    NOTE_TIMED_LN_HEAD,
    NOTE_TIMED_LN_TAIL,
#endif

    NOTE_COUNT,
};

struct NoteTimestampDiff {
    f64 value;
};
static inline NoteTimestampDiff operator - (NoteTimestampDiff a) {
    return { -a.value };
}

struct NoteTimestamp {
    f64 value;
    explicit operator NoteTimestampDiff() { return { value }; }
};
static inline NoteTimestampDiff operator - (NoteTimestamp a, NoteTimestamp b) {
    return { a.value - b.value };
}
static inline NoteTimestamp operator + (NoteTimestamp a, NoteTimestampDiff b) {
    return { a.value + b.value };
}
// static inline NoteTimestamp &operator += (NoteTimestamp &a, NoteTimestampDiff b) {
//     a = { a.value + b.value };
//     return a;
// }


#define DO_THE_THING_BOOL_OP(Type, val, op)                      \
    static BEAT_FORCE_INLINE bool operator op (Type a, Type b) { \
        return a.val op b.val;                                   \
    }
#define DO_THE_THING_ALL_BOOL_OP(Type, val) \
    DO_THE_THING_BOOL_OP(Type, val, <);     \
    DO_THE_THING_BOOL_OP(Type, val, <=);    \
    DO_THE_THING_BOOL_OP(Type, val, >);     \
    DO_THE_THING_BOOL_OP(Type, val, >=);    \
    DO_THE_THING_BOOL_OP(Type, val, ==);    \
    DO_THE_THING_BOOL_OP(Type, val, !=);
DO_THE_THING_ALL_BOOL_OP(NoteTimestamp, value);
DO_THE_THING_ALL_BOOL_OP(NoteTimestampDiff, value);
#undef DO_THE_THING_OP


// TODO: Make timestamp be 1.0 second instead of 0.0001 seconds. (That was useful when I was using s32 for NoteTime but now that I decided to use f64 it's fine not to multiply it. It will be faster too. I'll still define NOTE_PRECISION_SECONDS but that will mean something else.)

// Maybe I want something to account for rounding errors..
// If the resulting thing was 1.999999 it would cast to 1...
// see @IgnoreSomeBitsOfTheMantissa
// ((NoteTime)(0.001 + (sec / NOTE_PRECISION_SECONDS)))
// From seconds to timestamp -> multiply by rate
// From timestamp to seconds -> divide by rate
// NOTE: With rate:
//   NOTE_TIMESTAMP_FROM_SECONDS(rate_float * seconds);
//   NOTE_SECONDS_FROM_TIMESTAMP(ts)/rate_float;
#define NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(sec) \
    ((f64)(((sec) / NOTE_PRECISION_SECONDS)))
#define NOTE_SECONDS_FROM_TIMESTAMP__INTERNAL(ts) \
    ((f64)(ts) * NOTE_PRECISION_SECONDS)

#define NOTE_MAX_TIMESTAMP_F64 (2000000000.0)
#define NOTE_MAX_TIMESTAMP ((NoteTimestamp){NOTE_MAX_TIMESTAMP_F64})
#define NOTE_PRECISION_SECONDS 0.0001

static inline f64
note_timestamp_diff_to_seconds(NoteTimestampDiff ts_diff, f64 rate_float)
{
    return (1/rate_float)*NOTE_SECONDS_FROM_TIMESTAMP__INTERNAL(ts_diff.value);
}

static inline NoteTimestamp
seconds_to_song_ts(f64 seconds, f64 rate_float,
                   f64 ref_seconds, NoteTimestamp ref_note_ts)
{
    return {
        NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(rate_float*(seconds - ref_seconds))
            + ref_note_ts.value
    };
}

static inline NoteTimestamp
seconds_to_song_ts(f64 seconds, f64 rate_float)
{
    return {
        NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(rate_float*(seconds))
    };
}


// Maybe ts_end is not necessary. I'll keep it for now until I learn more about bpm changes and osu SVs.
struct BeatInfo {
    f32 bps; // @IgnoreSomeBitsOfTheMantissa
    // timestamp start and end
    // @IgnoreSomeBitsOfTheMantissa
    NoteTimestamp ts_start, ts_end; // ts_end is unnecessary
    // TODO: Store measure info here too.
};

struct Timings {
    smem bpms_count;
    BeatInfo *bpms;
};

struct Notes {
    Keymode keymode;
    s32 columns;

    Timings timings;

    // TODO: HoldType original_hold_type;


    // TODO: s32 total_notes_by_type[NOTE_COUNT]
    // Maybe s32 total_notes_by_type_per_column[columns][NOTE_COUNT]
    // TODO: s32 max_vns_per_col[columns];

    // Random Thoughts:
    // For a lot of columns (like 100) this ends up using a lot of unnecessary memory. (But I don't really want to support 88k. Maybe I'll support 24k but not 88k)
    // There are other differences other than how much memory it uses (unnecessary recomputation for method B)
    // Method A: Pack all notes in a single row together (Current) (single note size = k+8, row size = k+8)
    // Method B: Every note has its own timestamp (single note size = 9, row size = 9n)
    // If we have a 4K file and all of the rows are full (only quads) then for method A each row takes (k + 8) bytes.
    // For method B each row takes (9*n) bytes. n is the number of note in that row.
    // When are they equal in size? When (9*n) == (k + 8).
    // x = (k+8)/(9); x is the minimum number of notes for which it method A uses more memory than method B.
    // if x == 2 (k is 10), then if method B has a row with more than two notes, B uses more memory.
    // If the average note count per row in a given file is > x, method B would use more memory for that entire file.
    // For a 7K file (middle difficulty) I guess the average row size is 3 (idk) while (k+8)/9 is 1.66
    // for 19 keys, the ratio is 3.00.
    // The more keys there are in the file the less the average row size per file becomes.
    // You only have 10 fingers. The more keys there are the harder chords become.
    // I guess the average row size per file peaks at around 7-10K. Then it is perfect for this game.
    // I'll continue to use method A.
    s32 pitch; // how many bytes to get from one row to the other
    s32 rows_count;
    NoteTimestamp *timestamps; // @IgnoreSomeBitsOfTheMantissa
    // TODO: Maybe I should store measure info here too.
    NoteType *raw_notes;

    // I don't know what I'm doing with these
    // hold and roll pairs
    // to find one: find_hold_end(notes, hold_column, hold_idx_start)
    b8 *last_hold_is_released; // a bool // count_hold_pairs[column]
#if 0
    // TODO: Wrap these things in a struct so that it gets a bit easier to read.
    s32 *hold_pairs_count; // count_hold_pairs[column]
    s32 **hold_idx_start; // hold_idx_start[column][pair_idx]
    s32 **hold_idx_end;

    // typedef struct {
    //      s32 row_start, row_end;
    //      NoteTime ts_start, ts_end;
    // } HoldInfo;
    // HoldInfo **hold_infos; // [column][pair_idx]
#endif
};


// Just changing from 4 to 8 changed the debug time from 5.96 seconds to 4.98 seconds.
// And release time from to 1.36 to 1.20
#define NOTES_COLUMN_SIZE 8

#define NOTES_COLUMNS_PER_BYTE (8/NOTES_COLUMN_SIZE)
#if NOTES_COLUMN_SIZE == 4
// # define NOTES_GET_COLUMN(row, column) 
//     ((row)[(column)/NOTES_COLUMNS_PER_BYTE] >> 
//      (NOTES_COLUMN_SIZE*(column % NOTES_COLUMNS_PER_BYTE)) & ((1<<(NOTES_COLUMN_SIZE)) - 1))

# define NOTES_GET_COLUMN(row, column) \
    (NoteType)(((row)[(column)/2] >> (4*(column % 2))) & 15)

# define NOTES_GET_PITCH_SIZE(columns) (((columns) / 2) + (columns) % 2)
# define NOTES_SET_COLUMN(row, column, note_value)              \
{                                                               \
    (row)[(column)/2] &= (NoteType)(~(15 << (4*((column)%2))));         \
    (row)[(column)/2] |= (NoteType)((u32)(note_value) << (4*((column)%2))); \
}
#elif NOTES_COLUMN_SIZE == 8
# define NOTES_GET_COLUMN(row, column) (row)[column]
# define NOTES_GET_PITCH_SIZE(columns) (columns)
# define NOTES_SET_COLUMN(row, column, note_value) (row)[(column)] = (note_value)
#else
# error "NOTES_COLUMN_SIZE Unknown size"
#endif

struct Metadata {
    String title, artist;
    // TODO: Maybe<String> title_translit;
    String title_translit, artist_translit;
    // String subtitle, subtitle_translit;
    String creator;
    String diff_name;
    // String diff_description;
    s32 diff_idx; // TODO: Remove this later
    // NoteTime duration; // == timestamps[last_row]-timestamps[0]

    /*
    for .sm files there is:
    per diff:
    u16 chart_type;
    String chart_type_string;
    String description;
    String diff_name;
    s32 diff_number;

    per fileset:
    String title, title_translit;
    String subtitle, subtitle_translit;
    String artist, artist_translit;
    String genre;
    String credit;
    */
};

// typedef struct FilesetGroup {
//     s32 groups_count;
//     struct FilesetGroup *groups;
//     s32 filesets_count;
//     Fileset *filesets;
// } FilesetGroup;


enum ParserType {
    ParserType_INVALID,

    ParserType_SM,
    ParserType_OJN,
    ParserType_OSU,
    ParserType_BMS,

    ParserType_COUNT,
};
static const String parser_type_string_table[] = {
    [ParserType_INVALID] = "INVALID"_s,

    [ParserType_SM] = "SM"_s,
    [ParserType_OJN] = "OJN"_s,
    [ParserType_OSU] = "OSU"_s,
    [ParserType_BMS] = "BMS"_s,

    [ParserType_COUNT] = "COUNT"_s,
};
struct ParserTypeAndFileExtensionString {
    ParserType type;
    String ext;
};
static ParserTypeAndFileExtensionString parser_type_and_file_extension_list[] = {
    { ParserType_SM  , "sm"_s },
    { ParserType_OJN , "ojn"_s },
    { ParserType_OSU , "osu"_s },
    { ParserType_BMS , "bms"_s },
    { ParserType_BMS , "bme"_s },
    { ParserType_BMS , "pms"_s },
};


struct Diff;

// TODO: use arenas for fileset
#define FILESET_USE_ARENAS 0
struct Fileset {
#if FILESET_USE_ARENAS
    MemoryArena arena; // Everything in this fileset and the diffs goes into here.
#endif

    u64 unique_id; // this id is unique
    ParserType parser_type;
    String filepath;
    // filepath's meaning depends on parser_type
    // for bms and osu it is the directory
    // for sm and ojn it is the actual .sm filename


    s32 diffs_count;
    Diff *diffs;

    // u64 hash;
};

struct Diff {
    // TODO: Maybe Diff should have a unique_id too, just for debugging...

    // TODO: Deferred loading of the difficulties. If I want that I might want notes to be a pointer.
    Notes notes;
    // TODO: add these to notes
    // f32 main_bpm;
    // s32 game_mode; // 7K_PLUS_ONE, 4K, etc
    // s32 original_type; // sm or ojn, maybe put it in the fileset

    String filepath;
    Metadata metadata;

    String debug_mp3_filename;
    f64 debug_mp3_offset;

    // TODO: 128 bits hash
    // u64 hash;
};


// TODO: Don't do this.
#define PARSER_GET_FILESET_FROM_FILE_FN(name)                    \
    bool name(MemoryArena *final_arena, MemoryArena *temp_arena, \
              String filename, bool report_errors, Fileset *ret_fileset)
typedef PARSER_GET_FILESET_FROM_FILE_FN(ParserGetFilesetFromFileFn);
extern ParserGetFilesetFromFileFn *parser_get_fileset_from_file_table[ParserType_COUNT];


#endif
