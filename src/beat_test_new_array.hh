#ifndef TEST_NEW_ARRAY_H
#define TEST_NEW_ARRAY_H

// TODO: These names are too big. Rename to something like Arr and ArrDynamic

// TODO: Implement `SegmentedDynamicArray` (similar to zig's `std.SegmentedList`).


// for std::is_same
#include <type_traits>


#define FORV_1(arr, it_idx, it, counter)                     \
    for (smem it_idx = 0, BEAT_JOIN2(_asdf_forv, counter)=1; \
         BEAT_JOIN2(_asdf_forv, counter) && (arr).len>0;     \
         BEAT_JOIN2(_asdf_forv, counter)=0)                  \
         for (auto it =  (arr).data[0]; it_idx < (arr).len; it = (arr).data[++it_idx])

#define Forv(arr, it_idx, it) FORV_1(arr, it_idx, it, __COUNTER__)

#define FORP_1(arr, it_idx, it, counter)                     \
    for (smem it_idx = 0, BEAT_JOIN2(_asdf_forp, counter)=1; \
         BEAT_JOIN2(_asdf_forp, counter) && (arr).len>0;     \
         BEAT_JOIN2(_asdf_forp, counter)=0)                  \
        for (auto it = &(arr).data[0]; it_idx < (arr).len; it++, it_idx++)
#define Forp(arr, it_idx, it) FORP_1(arr, it_idx, it, __COUNTER__)

#define FORDECV_1(arr, it_idx, it, counter)                     \
    for (smem it_idx = (arr).len-1, BEAT_JOIN2(_asdf_fordecv, counter)=1; \
         BEAT_JOIN2(_asdf_fordecv, counter) && (arr).len>0;     \
         BEAT_JOIN2(_asdf_fordecv, counter)=0)                  \
        for (auto it = (arr).data[it_idx]; it_idx >= 0; it = (arr).data[--it_idx])
#define Fordecv(arr, it_idx, it) FORDECV_1(arr, it_idx, it, __COUNTER__)

#define FORDECP_1(arr, it_idx, it, counter)                     \
    for (smem it_idx = (arr).len-1, BEAT_JOIN2(_asdf_fordecp, counter)=1; \
         BEAT_JOIN2(_asdf_fordecp, counter) && (arr).len>0;     \
         BEAT_JOIN2(_asdf_fordecp, counter)=0)                  \
        for (auto it = &(arr).data[it_idx]; it_idx >= 0; it--, it_idx--)
#define Fordecp(arr, it_idx, it) FORDECP_1(arr, it_idx, it, __COUNTER__)

// ArrStatic<Arr<ArrDin<BeatKey>>, Keymode_COUNT> keymode_bindings;

// TODO: Make a new type for NewArrayDynamic except it is fixed size after the first reserve. (maybe just a bool)

#if 0
template <typename T, smem N>
struct NewArrayStatic
{
    static constexpr smem len = N;
    T data[N];

    inline operator Slice<T> () {
        return { .len=len, .data=data };
    }
};
#endif

// Macro to convert from static C array to Slice<T>
#define SLICE_FROM_STATIC(s) Slice<__typeof((s)[0])>{.len=ARRAY_COUNT(s), .data=(s)}
template <typename T>
struct Slice
{
    smem len;
    T *data;

    // This operator won't work on pointers, so I won't define it.
    // T operator [] (smem i);

    inline operator String () {
        STATIC_ASSERT_MSG((std::is_same<T, u8>::value), "Only Slice<u8> can be cast to a String/Buffer.");
        return String{ .len = len, .data = data };
    }

    inline Slice<T> slice(smem lo, smem hi) const {
        ASSERT(ABC(lo, len+1) && ABC(hi, len+1) && lo <= hi);
        return Slice<T>{.len = hi-lo, .data = &data[lo]};
    }
};
inline String::operator Slice<u8> () {
    return {.len = len, .data = data};
}

template <typename T>
struct NewArrayDynamic
{
    smem len;
    T *data;
    smem cap;

    // TODO: bool can_not_realloc; // TODO If this is true and it tries to reallocate it crashes. // TODO: Make init_fixed(n, arena=0) which just reserves n and sets can_not_realloc to true. // I'll do it if I find a good enough use case.
    MemoryArena *arena; // if null, use malloc
    struct {
        s32 temp_count;
        u64 clear_id;
    } _arena_temp_state; // for debug purposes. i could remove them for release

    /// arena can be null
    static NewArrayDynamic<T> init(MemoryArena *arena) {
        NewArrayDynamic<T> result = {};
        result.arena = arena;
        return result;
    }
    /// arena can be null
    static NewArrayDynamic<T> init_capacity(MemoryArena *arena, smem initial_capacity) {
        ASSERT(initial_capacity >= 0);
        NewArrayDynamic<T> result = init(arena);
        result.reserve(initial_capacity);
        return result;
    }

    // Cast to Slice<T>
    inline operator Slice<T> () const;
    inline Slice<T> as_slice() const;
    // Make sure it has space for n more elements.
    void reserve(smem n);
    // Add n uninitialized elements at the end. Returns pointer to the first added element.
    inline T *add_uninit(smem n);
    // Add new element at the end. Returns pointer to the newly added value.
    inline T *push(T val);
    // Add all elements of arr at the end.
    inline T *append(Slice<T> arr);
    // Just like append except that if it doesn't have enough memory, it doesn't do anything and returns 0.
    inline T *append_if_it_fits(Slice<T> arr);
    // Swap element at index `idx` with the last element and decrease len. Return the removed value.
    inline T unordered_remove(smem idx);
    // Remove the last element and return its value. Does ASSERT(len > 0).
    inline T pop_end();
    // free
    inline void free();



    inline operator String () {
        STATIC_ASSERT_MSG((std::is_same<T, u8>::value), "Only NewArrayDynamic<u8> can be cast to a String/Buffer.");
        return String{ .len = len, .data = data };
    }
};
typedef NewArrayDynamic<u8> StringNewArrayDynamic;
typedef StringNewArrayDynamic BufferNewArrayDynamic;


// Cast to Slice<T>
template<typename T>
inline NewArrayDynamic<T>::operator Slice<T> () const {
    return Slice<T>{ .len = len, .data = data };
}
template<typename T>
inline Slice<T> NewArrayDynamic<T>::as_slice() const {
    return Slice<T>{ .len = len, .data = data };
}

// Make sure it has space for n more elements.
template<typename T>
void NewArrayDynamic<T>::reserve(smem n)
{
    if (arena && data) {
        arena_clear_id_check(arena, _arena_temp_state.clear_id);
    }
    if (cap < len+n) {
        smem new_cap = MAXIMUM(MAXIMUM(cap * 2, 8), len+n);
        ASSERT(new_cap >= len+n);
        if (arena) {
            if (!data) {
                _arena_temp_state.temp_count = arena->temp_count;
                _arena_temp_state.clear_id   = arena->clear_id;
            } else {
                arena_temp_check(arena, _arena_temp_state.temp_count);
            }
            data = (T *)arena_realloc(arena, data, new_cap*ssizeof(T), alignof(T));
            ASSERT_SLOW(is_aligned_to((umem)data, alignof(T)));
        } else {
            data = (T *)xrealloc(data, new_cap*ssizeof(T));
            ASSERT_SLOW(is_aligned_to((umem)data, alignof(T)));
        }
        ASSERT(data);
        cap = new_cap;
    }
}

// Add n uninitialized elements at the end. Returns pointer to the first added element.
// It is not explicitly initialized to zero.
template<typename T>
inline T *NewArrayDynamic<T>::add_uninit(smem n)
{
    reserve(n); // already checks clear_id
    T *ptr = &data[len];
    len += n;
    return ptr;
}

// Add new element at the end. Returns pointer to the newly added value.
template<typename T>
inline T *NewArrayDynamic<T>::push(T val)
{
    T *ptr = add_uninit(1); // already checks clear_id
    *ptr = val;
    return ptr;
}

template<typename T>
inline T *NewArrayDynamic<T>::append(Slice<T> b)
{
    T *first_added = add_uninit(b.len); // already checks clear_id
    xmemcpy_struct_count(first_added, b.data, T, b.len);
    return first_added;
}

template<typename T>
inline T *NewArrayDynamic<T>::append_if_it_fits(Slice<T> b)
{
#if BEAT_SLOW
    auto prev_cap = cap;
#endif
    if (cap-len >= b.len) {
        auto *result = append(b);
#if BEAT_SLOW
        ASSERT(prev_cap == cap);
#endif
        return result;
    }
    return 0;
}

template<typename T>
inline T NewArrayDynamic<T>::unordered_remove(smem idx) {
    ASSERT(ABC(idx, len));
    const auto swap_idx = len-1;
    const auto result = data[idx];
    if (idx != swap_idx) {
        data[idx] = data[swap_idx];
    }
    len -= 1;
    return result;
}

template<typename T>
inline T NewArrayDynamic<T>::pop_end() {
    ASSERT(len > 0);
    return data[--len];
}

template<typename T>
inline void NewArrayDynamic<T>::free()
{
    if (arena) {
        if (data) {
            arena_clear_id_check(arena, _arena_temp_state.clear_id);
        }
        arena_free(arena, data);
    } else {
        xfree(data);
    }
    data = 0;
    len = 0;
    cap = 0;
    _arena_temp_state = {};
}




template<smem N_>
struct PackedBoolArray {
    STATIC_ASSERT(N_ > 0);
    static constexpr smem BIT_COUNT = N_;
    typedef u32 T;
    T things_[(BIT_COUNT + 31) / (32)];
    BEAT_FORCE_INLINE bool get(smem idx) {
        u32 arr_idx = (u32)idx / 32;
        u32 bit_off = (u32)idx - 32*arr_idx;
        ASSERT_SLOW(ABC(idx, BIT_COUNT) && ABC(bit_off, 32));
        return (((things_[arr_idx]) >> bit_off) & 1) != 0;
    }
    BEAT_FORCE_INLINE void set(smem idx, bool value) {
        u32 arr_idx = (u32)idx / 32;
        u32 bit_off = (u32)idx - 32*arr_idx;
        ASSERT_SLOW(ABC(idx, BIT_COUNT) && ABC(bit_off, 32));
        u32 thebit = (((u32)1) << bit_off);
        u32 themask = value ? ~(u32)0 : 0;
        things_[arr_idx] &= ~(thebit);
        things_[arr_idx] |=  (thebit & themask);
    }
};


// TODO: BucketArray can be faster. this iterator thing:w
struct BucketLocation
{
    smem bucket, item;
};
inline static bool operator == (BucketLocation a, BucketLocation b) { return   a.bucket == b.bucket && a.item == b.item; }
inline static bool operator != (BucketLocation a, BucketLocation b) { return !(a.bucket == b.bucket && a.item == b.item); }


template <typename T, int n>
struct Bucket
{
    // TODO: occupied actually has one advantage over a regular bucketarray: I can remove an arbitrary element and don't modify any of the pointers. For example:
    // I have an EntityID that is a combination of the BucketLocation and a generational id. To check if the entity exists I can quickly to at_location(bucket_location), check if that exists and if it does, check if the generational id's match.
    // Even if I remove another element from the bucket array, that EntityID will still be valid.
    //
    // If I don't have occupied[N] and I remove one element, the last element in the array will have its pointer changed. Now my EntityID I can't just check the bucket location and generational id. If that doesn't match i need to search for my generational id in the entire bucket array and that's much slower for just finding an entity.
    //
    // However, not having occupied[N] is still useful for other things. It is very good when I don't need to remove elements from the array but a regular NewArrayDynamic would reallocate too often (that could waste a lot of memory in an arena).
    // Maybe I should separate BucketArray into two. One has occupied[N] and the other does not have. I can call the one that does not havea occupied[N] BulkArray<Type, bucket_size> and the one that has, BucketArray<Type, bucket_size>
    // It would have a lot of duplicated code so _maybe_ I could make BucketOrBulkArray__internal<Type, bucket_size, has_occupied> and make typedefs.
    static const smem bucket_size = n;
    STATIC_ASSERT(bucket_size > 0);
    T items[n];
    smem num_occupied;
    // TODO: PackedBoolArray<bucket_size> occupied;
};


#if 0
{
    BucketArray<Thing, 256> things = {};
    // ...
    for (s32 i = 0; i < 123123; i++) {
        things->push(i);
    }
    BucketLocation iter_thing;
    s32 idx_thing = 0;
    for (things->iter_first(&iter_thing);
         things->iter_valid(iter_thing);
         things->iter_next(&iter_thing), idx_thing++) {
        Thing *it = things->at_location(iter_thing);
        if (should_remove(it)){
            free_thing(it);
            things->remove(iter_thing);
            continue;
        }
    }
    // that's bad because it checks twice; i can do this instead
    things->iter_first(&iter_thing);
    if (things->iter_valid(iter_thing)) {
        do {
            // ...
        } while (things->iter_next(&iter_thing));
    }
    // OR I modify iter_valid so that it only checks if iter_thing.valid is true. on iter_first and iter_next I set that value.
}
#endif
template <typename T, int n>
struct BucketArray
{
    static const smem bucket_size = n;
    typedef Bucket<T, n> BucketType;
    NewArrayDynamic<BucketType *> buckets; // shares the arena for the buckets
    smem total_items_occupied;

    // Adds a value to the first free item of the first free bucket.
    inline BucketLocation push(T val) {
        if (buckets.arena && buckets.data) {
            arena_clear_id_check(buckets.arena, buckets._arena_temp_state.clear_id);
        }
        for (smem idx_bu = 0; idx_bu < buckets.len; idx_bu++) {
            BucketType *bu = buckets.data[idx_bu];
            ASSERT(bu);
            ASSERT(bu->num_occupied <= bucket_size && bu->num_occupied >= 0); // TODO Remove later
            if (bu->num_occupied < bucket_size) {
                smem idx_item = bu->num_occupied;
                bu->num_occupied++;
                total_items_occupied++;
                bu->items[idx_item] = val;
                // TODO return pointer too
                return {.bucket = idx_bu, .item = idx_item};
            }
        }
        // add new bucket
        BucketType *new_bucket;
        if (buckets.arena) {
            if (buckets.data) {
                arena_temp_check(buckets.arena, buckets._arena_temp_state.temp_count);
            }
            new_bucket = arena_push_struct(buckets.arena, BucketType, arena_flags_zero());
        } else {
            new_bucket = xmalloc_struct(BucketType, xmalloc_flags_zero());
        }
        buckets.push(new_bucket);
        new_bucket->num_occupied = 0;

        new_bucket->num_occupied++;
        total_items_occupied++;
        new_bucket->items[0] = val;
        return {.bucket = buckets.len-1, .item = 0};
    }

    inline bool in_bounds(BucketLocation loc) const {
        // returns whether the pointer for this location is valid memory.
        // to test if the item exists use is_occupied instead
        if (!ABC(loc.bucket, buckets.len))
            return false;
        if (!ABC(loc.item, bucket_size))
            return false;
        return true;
    }
    inline bool is_occupied(BucketLocation loc) const {
        if (!ABC(loc.bucket, buckets.len))
            return false;
        BucketType *bu = buckets.data[loc.bucket];
        if (!ABC(loc.item, bu->num_occupied))
            return false;
        return true;
    }

    inline T *at_location(BucketLocation loc) const {
        ASSERT_SLOW(is_occupied(loc));
        BucketType *bu = buckets.data[loc.bucket];
        T *item = &bu->items[loc.item];
        if (ABC(loc.item, bucket_size)) {
            return item;
        }
        ASSERT(false && "BucketArray location is not occupied.");
        return NULL;
    }

    inline bool iter_valid(BucketLocation loc) const {
        // i don't like this. iter_next already checks for validity but I do it again here.
        if (!in_bounds(loc))
            return false;
        // iter_valid is only called on the for loop, so if it is in bounds it must be occupied
        ASSERT(is_occupied(loc));
        return true;
    }

    inline void iter_first(BucketLocation *loc) const {
        *loc = {};
        for (smem idx_bu = 0; idx_bu < buckets.len; idx_bu++) {
            BucketType *bu = buckets.data[idx_bu];
            if (bu->num_occupied == 0) continue;
            loc->bucket = idx_bu;
            loc->item = 0;
            return;
        }
        loc->bucket = -1;
    }
    inline void iter_last(BucketLocation *loc) const {
        *loc = {};
        for (smem idx_bu = buckets.len-1; idx_bu >= 0; idx_bu--) {
            BucketType *bu = buckets.data[idx_bu];
            if (bu->num_occupied == 0) continue;
            loc->bucket = idx_bu;
            loc->item = bu->num_occupied-1;
            return;
        }
        loc->bucket = -1;
    }

    inline bool iter_next(BucketLocation *loc) const {
        ASSERT(in_bounds(*loc));
        BucketType *bu = buckets.data[loc->bucket];
        if (loc->item+1 < bu->num_occupied) {
            loc->item++;
            return true;
        } else {
            for (smem idx_bu = loc->bucket+1; idx_bu < buckets.len; idx_bu++) {
                bu = buckets.data[idx_bu];
                if (bu->num_occupied == 0) continue;
                loc->bucket = idx_bu;
                loc->item = 0;
                return true;
            }
        }
        loc->bucket = -1;
        return false;
    }
    inline bool iter_prev(BucketLocation *loc) const {
        ASSERT(in_bounds(*loc));
        BucketType *bu = buckets.data[loc->bucket];
        if (loc->item > 0) {
            loc->item--;
            return true;
        } else {
            for (smem idx_bu = loc->bucket-1; idx_bu >= 0; idx_bu--) {
                bu = buckets.data[idx_bu];
                if (bu->num_occupied == 0) continue;
                loc->bucket = idx_bu;
                loc->item = bu->num_occupied-1;
                return true;
            }
        }
        loc->bucket = -1;
        return false;
    }

    inline void free() {
        if (buckets.arena) {
            if (buckets.data) {
                arena_clear_id_check(buckets.arena, buckets._arena_temp_state.clear_id);
            }
            // for completeness sake, but it probably does nothing useful
            Forv (buckets, _, bu) {
                arena_free(buckets.arena, bu);
            }
        } else {
            Forv (buckets, _, bu) {
                xfree(bu);
            }
        }
        buckets.free();
        total_items_occupied = 0;
    }

    inline void unordered_remove(BucketLocation loc) {
        // substitute loc's item with the last item in the last bucket.
        // this does not free the bucket if the item was the last in that bucket.
        ASSERT(is_occupied(loc));
        BucketType *bu = buckets.data[loc.bucket];
        BucketType *last_bu = buckets.data[buckets.len-1];
        if (loc != BucketLocation{.bucket=buckets.len-1, .item=last_bu->num_occupied-1})
            bu->items[loc.item] = last_bu->items[last_bu->num_occupied-1];
        last_bu->num_occupied--;
        total_items_occupied--;
    }
};


template <typename T>
static void
slice_merge_sort__helper(Slice<T> arr, MemoryArena *arena, smem start, smem count,
                         int (*cmp_fn)(T*, T*, void*), void *user_ptr)
{
    if (count <= 1) {
        // do nothing
    } else if (count == 2) {
        auto *a = &arr.data[start];
        auto *b = &arr.data[start+1];
        if (cmp_fn(a, b, user_ptr) > 0) {
            swap_values(a, b);
        }
    } else {
        // ASSERT(count > 2);
        ASSERT_SLOW(ABC(start, arr.len));
        ASSERT_SLOW(ABC(start+count-1, arr.len));
        ASSERT_SLOW(count > 0);
        auto low_count = count / 2;
        auto high_count = count - low_count;
        auto low_start = start;
        auto low_end = low_start + low_count;
        auto high_start = low_start + low_count;
        auto high_end = high_start + high_count;
        ASSERT_SLOW(low_end == high_start);
        ASSERT_SLOW(high_end == count + start);
        ASSERT_SLOW(high_count + low_count == count);

        auto check_sorted = [arr, cmp_fn, user_ptr](smem check_start, smem check_count) {
            ASSERT_SLOW(ABC(check_start, arr.len) &&
                        ABC(check_start+check_count-1, arr.len) &&
                        check_count > 0);
            for (smem i = check_start; i < check_start+check_count-1; i++) {
                auto *a = &arr.data[i];
                auto *b = &arr.data[i+1];
                auto cmp = cmp_fn(a, b, user_ptr);
                if (cmp > 0) return false;
            }
            return true;
        };

        bool low_sorted = check_sorted(low_start, low_count);
        if (!low_sorted) {
            slice_merge_sort__helper(arr, arena, low_start, low_count, cmp_fn, user_ptr);
            low_sorted = true;
        }

        bool high_sorted = check_sorted(high_start, high_count);
        if (!high_sorted) {
            slice_merge_sort__helper(arr, arena, high_start, high_count, cmp_fn, user_ptr);
            high_sorted = true;
        }


        // join low and high
        // maybe I should put this part into its own function.

        if (low_sorted && high_sorted) {
            auto *a = &arr.data[low_start+low_count-1];
            auto *b = &arr.data[high_start];
            if (cmp_fn(a, b, user_ptr) <= 0) {
                if (count == arr.len) {
                    // printf("Already sorted!\n"); // for debuggin
                }
                return;
            }
        }

        Scoped_Temp_Arena temp_mem(arena);
        auto *low_temp = arena_push_struct_count(temp_mem.temp.arena, T, low_count, arena_flags_nozero());
        xmemcpy_struct_count(low_temp, &arr.data[low_start], T, low_count);
        auto *high_temp = arena_push_struct_count(temp_mem.temp.arena, T, high_count, arena_flags_nozero());
        xmemcpy_struct_count(high_temp, &arr.data[high_start], T, high_count);


        // ASSERT_SLOW(check_sorted(low_start, low_count));
        // ASSERT_SLOW(check_sorted(high_start, high_count));


        smem low_idx = 0;
        smem high_idx = 0;
        smem out_idx = 0;

        {
            auto *low_ptr = low_temp;
            auto *high_ptr = high_temp;
            auto *out_ptr = &arr.data[start];

            while (true) {
                if (low_idx >= low_count) break;
                if (high_idx >= high_count) break;
                // ASSERT_SLOW(ABC(out_idx, count));

                int cmp = cmp_fn(low_ptr, high_ptr, user_ptr);
                if (cmp > 0) {
                    high_idx++;
                    *out_ptr++ = *high_ptr++;
                } else {
                    low_idx++;
                    *out_ptr++ = *low_ptr++;
                }
                // if (out_idx > 0) {
                //     ASSERT(cmp_fn(&arr.data[start + out_idx-1], &arr.data[start + out_idx], user_ptr) <= 0);
                // }
                out_idx++;
            }
            ASSERT_SLOW((low_idx == low_count) != (high_idx == high_count));
        }

        // verify_sorted(out_idx, &arr.data[start]);

        bool did_all_low = false;
        if (high_idx < high_count) {
            auto remaining = high_count - high_idx;
            out_idx     += remaining;
            high_idx    += remaining;
            did_all_low = true;
        } else {
            ASSERT(low_idx < low_count);
            xmemcpy_struct_count(&arr.data[start+out_idx],
                                 &low_temp[low_idx],
                                 T,
                                 (low_count-1)-(low_idx)+1);
            out_idx += (low_count-1)-(low_idx)+1;
            low_idx = low_count;
            ASSERT_SLOW(out_idx == count);
        }

        ASSERT((low_idx == low_count) && (high_idx == high_count) && (out_idx == count));

        // ASSERT_SLOW(check_sorted(start, count));
    }
}

template<typename T>
static void
slice_merge_sort(Slice<T> arr, MemoryArena *arena,
                 int (*cmp_fn)(T*, T*, void*), void *user_ptr)
{
    slice_merge_sort__helper(arr, arena, 0, arr.len, cmp_fn, user_ptr);
}

template<typename T, int bucket_size>
static void
bucket_array_merge_sort__helper(BucketArray<T, bucket_size> *ba, MemoryArena *arena, BucketLocation start, smem count,
                                int (*cmp_fn)(T*, T*))
{
    bool sorted = true;
    if (count == 1) {
        // do nothing
    } else if (count == 2) {
        auto *a = ba->at_location(start);
        auto next = start;
        bool ok = ba->iter_next(&next);
        ASSERT(ok);
        auto *b = ba->at_location(next);
        ASSERT(b);
        if (cmp_fn(a, b) > 0) {
            swap_values(a, b);
            sorted = false;
        }
    } else {
        ASSERT(count > 2);
        // split in two and merge sort each of them.
        smem low_count = count / 2;
        smem high_count = count - low_count;
        auto low_start = start;
        auto high_start = low_start;
        for (smem i = 0; i < low_count; i++) {
            bool ok = ba->iter_next(&high_start);
            ASSERT_SLOW(ok);
        }

        bool low_sorted = true;
        {
            auto iter = low_start;
            auto *a = ba->at_location(iter);
            for (s32 i = 0; i < low_count-1; i++) {
                ba->iter_next(&iter);
                auto *b = ba->at_location(iter);
                if (cmp_fn(a, b) > 0) {
                    low_sorted = false;
                    break;
                }
                a = b;
            }
        }
        if (!low_sorted) {
            bucket_array_merge_sort_helper(ba, arena, low_start, low_count, cmp_fn);
            low_sorted = true;
        }

        bool high_sorted = true;
        {
            auto iter = high_start;
            auto *a = ba->at_location(iter);
            for (s32 i = 0; i < high_count-1; i++) {
                ba->iter_next(&iter);
                auto *b = ba->at_location(iter);
                if (cmp_fn(a, b) > 0) {
                    high_sorted = false;
                    break;
                }
                a = b;
            }
        }
        if (!high_sorted) {
            bucket_array_merge_sort_helper(ba, arena, high_start, high_count, cmp_fn);
            high_sorted = true;
        }

        if (low_sorted && high_sorted) {
            auto low_last = high_start;
            bool ok = ba->iter_prev(&low_last);
            ASSERT_SLOW(ok);
            auto *a = ba->at_location(low_last);
            auto *b = ba->at_location(high_start);
            if (cmp_fn(a, b) <= 0) {
                if (count == ba->total_items_occupied) {
                    printf("Already sorted!\n");
                }
                return;
            }
        }

        auto temp_mem = begin_temp_memory(arena);
        auto *low_temp = arena_push_struct_count(arena, T, low_count, arena_flags_nozero());
        {
            auto iter = low_start;
            for (smem i = 0; i < low_count; i++) {
                auto *f = ba->at_location(iter);
                low_temp[i] = *f;
                ba->iter_next(&iter);
            }
        }
        auto *high_temp = arena_push_struct_count(arena, T, high_count, arena_flags_nozero());
        {
            auto iter = high_start;
            for (smem i = 0; i < high_count; i++) {
                auto *f = ba->at_location(iter);
                high_temp[i] = *f;
                ba->iter_next(&iter);
            }
        }

#if BEAT_SLOW
        auto verify_sorted_ = [ba, cmp_fn](BucketLocation start, smem count) {
            auto iter = start;
            auto *a = ba->at_location(iter);
            for (smem i = 0; i < count-1; i++) {
                ba->iter_next(&iter);
                auto *b = ba->at_location(iter);
                ASSERT(cmp_fn(a, b) <= 0);
                a = b;
            }
        };
# define verify_sorted(count, data) verify_sorted_(count, data)
#else
# define verify_sorted(count, data) do{}while(0)
#endif
        verify_sorted(low_start, low_count);
        verify_sorted(high_start, high_count);


        smem low_idx = 0;
        smem high_idx = 0;
        auto out_loc = start;
        smem num_written = 0;
        while (true) {
            if (low_idx >= low_count) break;
            if (high_idx >= high_count) break;

            auto *a = &low_temp[low_idx];
            auto *b = &high_temp[high_idx];
            if (cmp_fn(a, b) > 0) {
                sorted = false;
                high_idx++;
                auto *out_ptr = ba->at_location(out_loc);
                *out_ptr = *b;
            } else {
                low_idx++;
                auto *out_ptr = ba->at_location(out_loc);
                *out_ptr = *a;
            }
            ba->iter_next(&out_loc);
            num_written++;
        }


        if (high_idx < high_count) {
            auto remaining = high_count - high_idx;
            num_written += remaining;
            high_idx    += remaining;
        }
        while (low_idx < low_count) {
            auto *a = &low_temp[low_idx++];
            auto *out_ptr = ba->at_location(out_loc);
            *out_ptr = *a;
            ba->iter_next(&out_loc);
            num_written++;
        }

        ASSERT(num_written == count);

        verify_sorted(start, count);

        end_temp_memory(&temp_mem);
    }
#undef verify_sorted
}

template<typename T, int bucket_size>
static void
bucket_array_merge_sort(BucketArray<T, bucket_size> *ba, MemoryArena *arena,
                        int (*cmp_fn)(T*, T*))
{
    BucketLocation first;
    ba->iter_first(&first);
    bucket_array_merge_sort__helper(ba, arena, first, ba->total_items_occupied, cmp_fn);
}



template<typename T>
static inline void
ordered_remove(Slice<T> *arr, smem first_index_to_remove, smem count_to_remove)
{
    // first_index_to_remove is left to right
    ASSERT_SLOW(count_to_remove >= 0);
    if (count_to_remove > 0) {
        ASSERT_SLOW(ABC(first_index_to_remove, arr->len));
        ASSERT_SLOW(ABC(first_index_to_remove + count_to_remove - 1, arr->len));
        xmemmove_struct_count(&arr->data[first_index_to_remove],
                              &arr->data[first_index_to_remove + count_to_remove],
                              T,
                              ((arr->len-1) - (first_index_to_remove + count_to_remove) + 1));
        arr->len -= count_to_remove;
    }
}

template<typename T>
static inline void
ordered_remove(NewArrayDynamic<T> *arr, smem first_index_to_remove, smem count_to_remove)
{
    // first_index_to_remove is left to right
    ASSERT_SLOW(count_to_remove >= 0);
    if (count_to_remove > 0) {
        ASSERT_SLOW(ABC(first_index_to_remove, arr->len));
        ASSERT_SLOW(ABC(first_index_to_remove + count_to_remove - 1, arr->len));
        xmemmove_struct_count(&arr->data[first_index_to_remove],
                              &arr->data[first_index_to_remove + count_to_remove],
                              T,
                              ((arr->len-1) - (first_index_to_remove + count_to_remove) + 1));
        arr->len -= count_to_remove;
    }
}

struct OrderedRemoveDecreasingDelayed {
    smem start_removing, count_to_remove;
};

static inline OrderedRemoveDecreasingDelayed
ordered_remove_decreasing_delayed_init()
{
    return { .start_removing = -1, .count_to_remove = 0 };
}

// TODO: Modify `ordered_remove_decreasing_delayed` so that it doesn't need to be called when `cond_to_remove` is false.
template<typename T>
static inline bool
ordered_remove_decreasing_delayed(bool cond_to_remove, smem i, Slice<T> *arr,
                                  OrderedRemoveDecreasingDelayed *ordrem)
{
    // this function is used like this:
#if 0
    {
        // if you know there's a limit to how many items you can remove, you can break at the middle of the loop.
        smem my_limit_to_remove = ???;
        smem my_items_removed = 0;

        auto ordrem = ordered_remove_decreasing_delayed_init();
        for (smem i = arr.len-1; i >= 0; i--) {
            if (want_to_stop) break; // i will not be deleted
            if (my_limit_to_remove == my_items_removed) break; // `i` will not be deleted
            do_something_with_the_item(arr.data[i]);
            bool cond_to_remove_this_item = ???;
            if (cond_to_remove_this_item) my_items_removed++;
            // if it returns true, it changed `arr.len`.
            ordered_remove_decreasing_delayed(cond_to_remove_this_item, i, &arr,
                                              &ordrem);
        }
        ordered_remove_decreasing_delayed_final(&arr, &ordrem);
    }
#endif

#if BEAT_SLOW
    {
        ASSERT(ABC(i, arr->len));
        if (ordrem->start_removing >= 0) {
            ASSERT(ABC(ordrem->start_removing, arr->len));
            ASSERT(ABC(ordrem->start_removing - ordrem->count_to_remove + 1, arr->len));
        }
    }
#endif
    bool did_the_remove = false;
    if (cond_to_remove) {
        if (ordrem->start_removing < 0) {
            ordrem->start_removing = i;
        }
        ordrem->count_to_remove += 1;
    } else {
        if (ordrem->start_removing >= 0) {
            ordered_remove(arr, ordrem->start_removing - ordrem->count_to_remove + 1, ordrem->count_to_remove);
            // no need to modify i since this is a decreasing loop
            ordrem->start_removing = -1;
            ordrem->count_to_remove = 0;
            did_the_remove = true;
        }
    }

    return did_the_remove;
}
template<typename T>
static inline bool
ordered_remove_decreasing_delayed(bool cond_to_remove, smem i, NewArrayDynamic<T> *arr,
                                  OrderedRemoveDecreasingDelayed *ordrem)
{
    Slice<T> other_arr = *arr;
    bool result = ordered_remove_decreasing_delayed(cond_to_remove, i, &other_arr, ordrem);
    if (result) {
        arr->len = other_arr.len;
    } else {
        ASSERT_SLOW(arr->len == other_arr.len);
    }
    return result;
}


template<typename T>
static inline bool
ordered_remove_decreasing_delayed_final(Slice<T> *arr, OrderedRemoveDecreasingDelayed *ordrem)
{
    bool did_the_remove = false;
    if (ordrem->start_removing >= 0) {
        ordered_remove(arr, ordrem->start_removing - ordrem->count_to_remove + 1, ordrem->count_to_remove);
        ordrem->start_removing = -1;
        ordrem->count_to_remove = 0;
        did_the_remove = true;
    }
    return did_the_remove;
}
template<typename T>
static inline bool
ordered_remove_decreasing_delayed_final(NewArrayDynamic<T> *arr, OrderedRemoveDecreasingDelayed *ordrem)
{
    Slice<T> other_arr = *arr;
    bool result = ordered_remove_decreasing_delayed_final(&other_arr, ordrem);
    if (result) {
        arr->len = other_arr.len;
    } else {
        ASSERT_SLOW(arr->len == other_arr.len);
    }
    return result;
}

// return -1 if it didn't find. the index otherwise.
template<typename T>
static inline smem
find_in_slice_by_value(Slice<T> arr, T value_to_search_for)
{
    for (smem i = 0; i < arr.len; i++) {
        if (arr.data[i] == value_to_search_for) {
            return i;
        }
    }
    return -1;
}

template<typename T>
struct SinglyLinkedList {
    struct Node {
        T value;
        MaybePtr<Node> next;
    };
    MaybePtr<Node> head;

    static SinglyLinkedList<T> init() { return { .head = None }; }
    /// Calculate the list length.
    smem len() const {
        smem result = 0;
        auto node = head;
        while (node.is_some()) {
            auto it = node.unwrap_unchecked();
            node = it->next;
            result += 1;
        }
        return result;
    }
    /// Insert `node` at the start of the list.
    void insert_at_head(Node *node) {
        ASSERT(node != nullptr);
        node->next = head;
        head = Some(node);
    }
    /// Remove the head or return none;
    MaybePtr<Node> pop_head_or_none() {
        if (head.is_some()) return Some(remove_node(&head));
        else return None;
    }
    /// Remove the node at `node_ptr` and return the removed node.
    Node *remove_node(MaybePtr<Node> *node_ptr) {
        ASSERT(node_ptr != nullptr && node_ptr->is_some());
#if BEAT_SLOW
        {
            bool found_it = false;
            auto iter = &head;
            while (iter->is_some()) {
                if (iter == node_ptr) {
                    found_it = true;
                    break;
                }
                iter = iter->unwrap_unchecked()->next;
            }
            ASSERT(found_it);
        }
#endif
        auto removed = node_ptr->unwrap();
        *node_ptr = removed->next;
        return removed;
    }
};

template<typename T>
struct FreeAndActiveSinglyLinkedList {
private:
    typedef FreeAndActiveSinglyLinkedList<T> Self;
public:
    typedef typename SinglyLinkedList<T>::Node Node;
    MemoryArena *arena; // can be null
    SinglyLinkedList<T> free_list; // list of uninitialized `T`.
    SinglyLinkedList<T> active_list;
    /// Initialize it. If arena is null, use xmalloc.
    static Self init(MemoryArena *arena) {
        return {
            .arena = arena,
            .free_list = SinglyLinkedList<T>::init(),
            .active_list = SinglyLinkedList<T>::init(),
        };
    }
    /// Initialize it and preallocate `initial_cap` elements. If arena is null, use xmalloc.
    static Self init_capacity(MemoryArena *arena, smem initial_cap) {
        Self result = init(arena);
        result.reserve(initial_cap);
        return result;
    }
    /// Free everything
    void deinit() {
        auto node = free_list.head;
        while (node.is_some()) {
            auto it = node.unwrap_unchecked();
            node = it->next;
            if (arena != nullptr) {
                arena_free(arena, it); // a nop
            } else {
                xfree(it);
            }
        }
        node = active_list.head;
        while (node.is_some()) {
            auto it = node.unwrap_unchecked();
            node = it->next;
            if (arena != nullptr) {
                arena_free(arena, it); // a nop
            } else {
                xfree(it);
            }
        }
        INVALIDATE_STRUCT(this);
    }
    /// Make sure at least `to_reserve` elements are available.
    void reserve(smem to_reserve) {
        const auto free_len = free_list.len();
        if (free_len < to_reserve) {
            const auto count_to_alloc = to_reserve - free_len;
            for (smem i = 0; i < count_to_alloc; i++) {
                free_list.insert_at_head(allocate_node());
            }
        }
        ASSERT_SLOW(free_list.len() >= to_reserve);
    }
    /// Add one uninitialized element to the active list and return a pointer to it.
    T *add_one_uninit_to_active() {
        reserve(1);
        auto newone = free_list.pop_head();
        active_list.insert_at_head(newone);
        return &newone.value;
    }
    /// Add one element to the active list.
    void add_one_to_active(T v) {
        const auto ptr = add_one_uninit_to_active();
        *ptr = v;
    }
    /// Move one element to the free list.
    void move_to_free_list(MaybePtr<Node> *active_node_ptr) {
        free_list.insert_at_head(active_list.remove_node(active_node_ptr));
    }
private:
    Node *allocate_node() {
        if (arena != nullptr) return arena_push_struct(arena, Node);
        else return xmalloc_struct(Node);
    }
};

#if 0
static void
print_ints(Slice<int> arr)
{
    printf("--printing ints by value-- len %d, data %p\n", (int)arr.len, arr.data);
    Forv (arr) { printf("[%d] = %d;\n", (int)it_idx, it); }

    printf("--printing ints by pointer-- len %d, data %p\n", (int)arr.len, arr.data);
    Forp (arr) { printf("*[%d] = %d; at (%p)\n", (int)it_idx, *it, it); }
}

int
main()
{
    Arena arena = {};
    arena.cap = 1024*1024;
    // arena.cap = 32*sizeof(int);
    arena.base = (u8 *)malloc(arena.cap);
    print_arena(arena);

    int myints[] = {10, 20, 30};
    print_ints(NEW_ARRAY_STATIC(myints));
    NewArrayDynamic<int> dyn = {};
    dyn.arena = &arena;
    for (int i = 0; i < 13; i++) {
        int *p = dyn.push(i*i);
        if (i == 2) *p = 123;
    }
    {
        SCOPED_TEMP_ARENA(&arena);
        dyn.push(1); // crashes because of the temp arena
    }

    {
        MemoryArena arena2 = {};
        init_arena(&arena2, ...);
        NewArrayDynamic<int> arr = {};
        arr.arena = &arena2;
        arr.push(1);
        while (true) {
            arena_clear(&arena2);
            arr.push(1); // TODO: This wouldn't crash!
        }
    }


    print_ints(dyn);
    Slice<int> aaa = dyn;
    print_ints(aaa);
    print_arena(arena);
    dyn.free();
    print_arena(arena);

    auto asdf = NEW_ARRAY_STATIC(((int [4]){1, 10, 100, 1000}));
    Forv (asdf) {
        printf("test NEW_ARRAY_STATIC [%d] = %d\n", (int)it_idx, it);
    }
    return 0;
}
#endif
#endif
