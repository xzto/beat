#ifndef BEAT_SONG_ASSET_H
#define BEAT_SONG_ASSET_H

enum {
    SongAssetState_Unloaded,
    SongAssetState_Queued,
    SongAssetState_UnloadedWithError, // might go from unloaded->queued->error (file does not exist, for example)
    SongAssetState_Loaded,
    SongAssetState_Locked,
};
typedef u8 SongAssetState;

enum {
    SongAssetType_Image,
    SongAssetType_Sample,
    // SongAssetType_Notes,
};
typedef u8 SongAssetType;

// How to get the asset
enum {
    SongAssetWhereType_Filesystem, // in the filesystem
    // SongAssetWhereType_O2jam, // inside the o2jam file
    // SongAssetWhereType_SMNotes, // inside the sm file
};
typedef u8 SongAssetWhereType;

typedef struct {
    u32 hash;
    SongAssetWhereType where_type;
    SongAssetType asset_type; // Remove this later when i create a table for each assettype
    union {
        struct {
            // file modified time can probably be a u32 and be relative to the start of the program
            u64 file_modified_time;
            String filepath;
        } filesystem;
        // struct {
        //     s32 diff_idx;
        //     u64 file_modified_time;
        //     String filepath;
        // } sm_notes;
    };
} SongAssetWhere;

typedef struct {
#if 1
    s32 placeholder;
#else
    u8 *pixels;
    s32 width;
    s32 height;
    s32 pitch_in_pixels;
#endif
} LoadedImage;

typedef struct {
    s32 placeholder;
} LoadedSample;

// This is more than 64 bytes
typedef struct SongAsset {
    struct SongAsset *prev, *next;

    // SongAssetType type;
    SongAssetState state;
    SongAssetWhere where;

    // how many songs use this asset. when I add an asset to a song I increase this number.
    // example: 
    // add diff1.osu - add song.mp3, set its ref count to 1
    // add diff2.osu - add song.mp3, set its ref count to 2
    // remove diff2.osu from disk
    // will have to remove diff2 from the wheel
    // remove diff2
    // decrease song.mp3's ref count
    // ref_count > 0, don't delete the assset entirely
    //
    // remove diff1.osu from disk
    // ...
    // decrease song.mp3's ref count
    // ref_count == 0, delete the asset from the asset list
    s32 ref_count;

    // When not loaded, data is null and data_size is unknown (0).
    // When loaded, its whole data will be here. All the pointers inside notedata will be inside this "data".
    smem data_size;
    void *data;
    union {
        LoadedImage *bg_image;
        // Notes *notes;
        LoadedSample *song;
    };
} SongAsset;

// When I enter the song wheel and select the song/pack I load everything
// If I run out of memory I evict some assets (but keep the metadata).

typedef struct RecentAsset {
    struct RecentAsset *prev, *next;
    SongAsset *asset;
} RecentAsset;

#define SONG_ASSET_LIST_TABLE_SIZE 1024
typedef struct {
    // Should I make one table for each asset type?
    SongAsset asset_sentinels[SONG_ASSET_LIST_TABLE_SIZE];

    RecentAsset recent_assets_sentinel;
} SongAssetTable;

typedef struct {
    // for filepath and metadata
    smem stuff_size;
    void *stuff_ptr;

    u32 where_hash;
    String filepath;
    u64 file_modified_time;
    s32 diff_idx;

    Metadata metadata;

    // maybe this goes in the metadata
    f64 preview_sample_start_seconds;
    f64 preview_sample_duration_seconds;

    Notes notes;

    struct {
#if 0
        // Storing only the SongAssetWhere would solve the evict problem,(not a problem) but it would make the program slower.
        SongAssetWhere notes;
        SongAssetWhere bg_image;
        // preview_sample and song might be the same thing. for now i'll just
        // ignore the preview sample and implement only song.
        // SongAssetWhere preview_sample;
        SongAssetWhere song;
        // s32 samples_count;
        // SongAssetWhere *samples;
#else
        // We don't need to store only SongAssetWhere. I thought I needed to
        // because when I had to evict an asset I would remove it entirely from
        // the asset table, but I don't need to. I only evict song_asset->data,
        // not the whole song_asset from the table.

        // SongAsset *notes; // probably doesn't need to be an asset.
        SongAsset *bg_image;
        SongAsset *song;
#endif
    } assets;
} Song;

typedef struct {
    s32 diffs_count;
    Song *diffs;
} SongDiffGroup;

typedef struct SongFilepathList {
    struct SongFilepathList *next;
    String filepath;
    u64 modified_time;
    Song *song;
} SongFilepathList;

typedef struct {
    SongAssetTable asset_table;

    s32 diffgroups_count;
    SongDiffGroup diffgroups[128]; // TODO: not a static array




    SongFilepathList song_filepath_table[4096];

#if 0
    PlatformWorkQueue *asset_load_queue;
#endif
} Wheel;


#endif
