// this is all pseudo code

#include "beat_include.hh"
#include "beat_song_asset.hh"

/*
Song has stuff_size and stuff_ptr
Metadata and the strings go inside stuff_ptr
*/

static u32
get_hash_from_filepath(String filepath)
{
    ASSERT(!"NOT IMPLEMENTED");
    return 0;
}

static b32
song_asset_where_are_equal(SongAssetWhere *a, SongAssetWhere *b)
{
    ASSERT(!"NOT IMPLEMENTED");
    return false;

    b32 result = false;
    if (a->hash != b->hash) {
        result = false;
    } else  if (a->where_type != b->where_type) {
        result = false;
    } else  if (a->asset_type != b->asset_type) {
        result = false;
    } else  if (a->where_type == SongAssetWhereType_Filesystem) {
        if (!strings_are_equal(a->filesystem.filepath, b->filesystem.filepath)) {
            result = false;
        }
    }
    return result;
}

static SongAsset *
find_song_asset(Wheel *wheel, SongAssetWhere where)
{
    SongAsset *result = NULL;
    u32 hash = where.hash;
    s32 entry = hash % SONG_ASSET_LIST_TABLE_SIZE;
    SongAsset *sentinel = &wheel->asset_table.asset_sentinels[entry];
    for (SongAsset *asset = sentinel->next; asset != sentinel; asset = asset->next) {
        if (song_asset_where_are_equal(&asset->where, &where)) {
            result = asset;
            break;
        }
    }
    return result;
}

static SongAsset *
find_asset_from_filesystem(Wheel *wheel, SongAssetType asset_type, String filepath)
{
    SongAsset *result = NULL;
    SongAssetWhere where = {};
    where.hash = get_hash_from_filepath(filepath);
    where.where_type = SongAssetWhereType_Filesystem;
    where.asset_type = asset_type;
    where.filesystem.filepath = filepath;
    result = find_song_asset(wheel, where);
    return result;
}

static u64
platform_get_file_modified_time(String filepath)
{
    ASSERT(!"NOT IMPLEMENTED");
    return false;
}

static b32
new_parser_sm_begin(MemoryArena *temp_arena, ParserSmCtx *ctx, String filepath)
{
    ASSERT(!"NOT IMPLEMENTED");
    return false;
}

static b32
new_parser_sm_get_metadata(ParserSmCtx *ctx, Metadata *metadata)
{
    ASSERT(!"NOT IMPLEMENTED");
    return false;
}

static String
new_parser_sm_get_song_filepath(ParserSmCtx *ctx)
{
    ASSERT(!"NOT IMPLEMENTED");
    return (String){};
}

static void
new_parser_sm_end(ParserSmCtx *ctx)
{
    ASSERT(!"NOT IMPLEMENTED");
    return;
}

static Metadata
copy_metadata_into_arena(MemoryArena *arena, Metadata *metadata)
{
    ASSERT(!"NOT IMPLEMENTED");
    return (Metadata){};
}

static void
new_parser_sm_get_preview_duration(ParserSmCtx *ctx, f64 *ret_preview_sample_duration_seconds,
                                   f64 *ret_preview_sample_start_seconds)
{
    ASSERT(!"NOT IMPLEMENTED");
    return;
}

static String 
new_parser_sm_get_bg_image_filepath(ParserSmCtx *ctx)
{
    ASSERT(!"NOT IMPLEMENTED");
    return (String){};
}

enum {
    NEW_PARSER_SM_LOAD_NOTES_CONTINUE,
    NEW_PARSER_SM_LOAD_NOTES_END,
    NEW_PARSER_SM_LOAD_NOTES_ERROR,
};

static s32
new_parser_sm_load_notes(ParserSmCtx *ctx, Notes *notes, smem *data_size, void **data_ptr)
{
    ASSERT(!"NOT IMPLEMENTED");
    return 0;
}


static LoadedImage *
load_image_from_filepath(String filepath, smem *data_size, void **data)
{
    // returns a LoadedImage pointer inside data
    ASSERT(!"NOT IMPLEMENTED");
    return NULL;
}

static void
push_asset_to_load_queue(Wheel *wheel, SongAsset *asset)
{
    ASSERT(asset->where.where_type == SongAssetWhereType_Filesystem);
    switch (asset->where.asset_type) {
    case SongAssetType_Image: {
#if 0
        WorkLoadAssetImage work = {};
        work.filepath = asset->where.filepath;
        platform_push_work(wheel->asset_load_queue, work, do_work_load_asset_image);
#else
        LoadedImage *image = load_image_from_filepath(asset->where.filesystem.filepath,
                                                      &asset->data_size, &asset->data);
        if (image != NULL) {
            asset->state = SongAssetState_Loaded;
        } else {
            asset->state = SongAssetState_UnloadedWithError;
        }
#endif
    } break;
    default: INVALID_CODE_PATH;
    }
}

static void
load_asset_now(Wheel *wheel, SongAsset *asset)
{
    // TODO: Handle UnloadedWithError
    if (asset->state == SongAssetState_Unloaded) {
        push_asset_to_load_queue(wheel, asset);
    }
}

static void
wheel_push_asset(Wheel *wheel, SongAsset *new_asset)
{
    s32 entry = new_asset->where.hash % SONG_ASSET_LIST_TABLE_SIZE;
    SongAsset *add_to = wheel->asset_table.asset_sentinels[entry].prev;
    new_asset->prev = add_to->prev;
    new_asset->next = add_to->next;
    new_asset->prev->next = new_asset;
    new_asset->next->prev = new_asset;

    RecentAsset *new_recent = xmalloc_struct(RecentAsset, xmalloc_flags_zero());
    new_recent->asset = new_asset;

    RecentAsset *recent_next = wheel->asset_table.recent_assets_sentinel.next;
    new_recent->prev = recent_next->prev;
    new_recent->next = recent_next->next;
    new_recent->prev->next = new_recent;
    new_recent->next->prev = new_recent;
}

static SongAsset *
wheel_push_asset_from_filesystem(Wheel *wheel, SongAssetType asset_type, String filepath, b32 load_now)
{
    SongAssetWhere where = {};
    where.hash = get_hash_from_filepath(filepath);
    where.where_type = SongAssetWhereType_Filesystem;
    where.asset_type = asset_type;
    where.filesystem.filepath = copy_string(filepath);

    SongAsset *new_asset = xmalloc_struct(SongAsset, xmalloc_flags_zero());
    new_asset->state = SongAssetState_Unloaded;
    new_asset->where = where;
    new_asset->ref_count++;

    wheel_push_asset(wheel, new_asset);

    if (load_now) {
        load_asset_now(wheel, new_asset);
    }

    return new_asset;
}

static SongAsset *
add_asset_notedata_sm(Wheel *wheel, String filepath, s32 diff_idx, Notes *notes,
                      smem data_size, void *data_ptr)
{
    ASSERT(!"NOT IMPLEMENTED");
    return 0;
}

static SongAsset *
add_asset_image_from_filesystem(Wheel *wheel, String filepath, b32 load_now)
{
    SongAsset *result = find_asset_from_filesystem(wheel, SongAssetType_Image, filepath);
    if (result != NULL) {
        result->ref_count++;
        if (load_now) {
            load_asset_now(wheel, result);
        }
    } else {
        result = wheel_push_asset_from_filesystem(wheel, SongAssetType_Image, filepath, load_now);
    }
    ASSERT(result != NULL);
    result->ref_count++;
    return result;
}

static SongAsset *
add_asset_sample_from_filesystem(Wheel *wheel, String filepath, b32 load_now)
{
    ASSERT(!"NOT IMPLEMENTED");
    return NULL;
}

static smem
get_metadata_size(Metadata *metadata)
{
    ASSERT(!"NOT IMPLEMENTED");
    return 0;
}

static void
copy_song_base(Song *song_base, Song *new_song, smem stuff_size, smem *stuff_used)
{
    ASSERT(!"NOT IMPLEMENTED");
    return;
    // copy_string song_base.filepath to new_song.filepath inside new_song.stuff_ptr+stuff_used
    if (song_base->metadata.title.data != NULL) {
        // copy it
    }
}

static void
add_songdiffs_to_wheel(Wheel *wheel, s32 diff_count, Song *diffs)
{
    SongDiffGroup diffgroup = {};
    diffgroup.diffs_count = diff_count;
    diffgroup.diffs = xmalloc_struct_count(Song, diff_count);
    xmemcpy_struct_count(diffgroup.diffs, diffs, Song, diff_count);

    ASSERT(wheel->diffgroups_count < ARRAY_COUNT(wheel->diffgroups));
    wheel->diffgroups[wheel->diffgroups_count++] = diffgroup;
}

static void
copy_notes(Notes *temp_notes, Song *song, smem *stuff_used)
{
    ASSERT(!"NOT IMPLEMENTED");
    return;
}

static b32
test_load_song_sm_everything(Wheel *wheel, MemoryArena *temp_arena, String filepath)
{
    ASSERT(!"NOT IMPLEMENTED");
    // This function does not handle the case where the song is already loaded in the wheel.
    Song song_base = {};
    song_base.filepath = filepath;
    song_base.file_modified_time = platform_get_file_modified_time(filepath);
    // SongLoadedState final_state = 0;
    b32 valid;
    ParserSmCtx p;
    // new_parser_sm_begin will create a subarena out of temp_arena for the first part and another subarena for the notes.
    TempMemory memory_flush = begin_temp_memory(temp_arena);
    // TODO: have to verify how much memory is necessary
    MemoryArena parser_temp_arena = sub_arena(temp_arena, KILOBYTES(32));
    valid = new_parser_sm_begin(&parser_temp_arena, &p, filepath);
    if (!valid) {
        // free things, end temp mem
        return false;
    }

    valid = new_parser_sm_get_metadata(&p, &song_base.metadata);
    if (!valid) {
        new_parser_sm_end(&p);
        // free things, end temp mem
        return false;
    }

    new_parser_sm_get_preview_duration(&p, &song_base.preview_sample_duration_seconds,
                                       &song_base.preview_sample_start_seconds);

    String bg_image_filepath = new_parser_sm_get_bg_image_filepath(&p);
    // Not having a bg image is a valid condition
    SongAsset *asset_bg_image = NULL;
    if (bg_image_filepath.len > 0) {
        asset_bg_image = add_asset_image_from_filesystem(wheel, bg_image_filepath, true);
        song_base.assets.bg_image = asset_bg_image;
    }

    // .sm has the same file for AllSamples and PreviewSample
    String song_filepath = new_parser_sm_get_song_filepath(&p);
    SongAsset *asset_song = NULL;
    if (bg_image_filepath.len > 0) {
        asset_song = add_asset_sample_from_filesystem(wheel, bg_image_filepath, true);
        song_base.assets.song = asset_song;
    }

    s32 diff_count = 0;
    // notes come in the parser_temp_arena
    Notes notes;
    smem temp_notes_data_size;
    void *temp_notes_data_ptr;
    Song songs[20] = {};
    s32 err;
    // TODO: Verify the notedata doesn't exist first.
    while ((err = new_parser_sm_load_notes(&p, &notes, &temp_notes_data_size, &temp_notes_data_ptr))
           == NEW_PARSER_SM_LOAD_NOTES_CONTINUE) {
        ASSERT(diff_count < ARRAY_COUNT(songs));
        smem final_stuff_size = get_metadata_size(&song_base.metadata);
        align_forward(&final_stuff_size, 16);
        final_stuff_size += temp_notes_data_size;
        smem stuff_used = 0;
        songs[diff_count].stuff_size = final_stuff_size;
        songs[diff_count].stuff_ptr = xmalloc(final_stuff_size);
        copy_song_base(&song_base, &songs[diff_count], final_stuff_size, &stuff_used);
        songs[diff_count].diff_idx = diff_count;
        copy_notes(&notes, &songs[diff_count], &stuff_used);

        diff_count++;
    }
    if (err != NEW_PARSER_SM_LOAD_NOTES_END) {
        // handle error
    }
    add_songdiffs_to_wheel(wheel, diff_count, songs);
    new_parser_sm_end(&p);
    end_temp_memory(&memory_flush);
    return true;
}
