#include "beat_platform.hh"
#include "beat_platform_linuxsdl.hh"

#include <SDL2/SDL.h>
#include <glad/glad.h>

// For usleep (testing)
#include <unistd.h>




u64 global_frame_count = 1;
b32 global_running = true;
GlobalStats GLOBAL_STATS = {};

#include <time.h>

PLATFORM_GET_CLOCK(PlatformApi::get_clock)
{
#if 1
    return SDL_GetPerformanceCounter();
#else
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
    return (1000000000)*(u64)now.tv_sec + (u64)now.tv_nsec;
#endif
}

PLATFORM_GET_SECS_PER_CLOCK(PlatformApi::get_secs_per_clock)
{
#if 1
    return 1.0/(f64)SDL_GetPerformanceFrequency();
#else
    return 1e-9;
#endif
}


/*
DirectoryIter memory used is increasing like this (written 2021-01-06)
depth (x)   |   mem delta           | mem absolute
0           |   0                   | 0
1           |   a                   | a
2           |   a + b               | 2x + b
2           |   a + b + b           | 3x + 3y
3           |   a + b + b + b       | 4x + 6y
4           |   a + b + b + b + b   | 4x + 10y
x           |   a + x*b             | ??



int (a + x*b) dx
= ax +  b*x^2/2 + C
a is constant increase per depth (iter_root_dir_nullter + DirectoryIter itself, etc)
a=333 ()
b is not constant increase per depth (rel_filename_nullter)
b=30 (guessing an average)
C is the constant memory is uses (practically zero when depth gets big)

After 1000 depth it would use about 15MB of memory (a=333, b=30).
 */
PLATFORM_DIRECTORY_ITER_INIT(PlatformApi::directory_iter_init)
// bool name(DirectoryIter *iter, String dir_not_nullter, MemoryArena *arena, bool is_recursive, DirectoryIter *prev_iter)
{
    zero_struct(iter);
    iter->arena = arena;
    iter->temp_for_end = begin_temp_memory(iter->arena);
    // TODO: @DirectoryIterQuadraticMemory iter_root_dir_nullter is being copied when technically I don't have to copy it. When I recurse into another directory it always _appends_ to the path, it never modifies the existing string. I could use a dynamic array for this (StringNewArrayDynamic, on DirectoryIter I will keep a pointer to it because on realloc the contents will change and when I end the iterator `prev_iter` will have a bad pointer) and when I call this init function, if there was a `prev_iter`, I should assume the directory is a relative path from the previous iter. I append it to the existing string (the StringNewArrayDynamic) on `directory_iter_init` (if there was an error on init I will revert it. Also, I will have to use a separate arena for it since I will use TempMemory inside the iterator and this dynamic string will have a bigger lifetime than the TempMemory, I can't reallocate it when the TempMemory is active - my Arena/NewArrayDynamic code would check for that and crash immediately) and on `linuxsdl_directory_iter__end_just_one`, if there was a `prev_iter`, I remove the last directory from the StringNewArrayDynamic. With this instead of having a quadratic increase in memory usage per depth, it will be linear.


    // TODO: Instead of initializing the thing with `next_dir` (a full path or relative to getcwd()), I could use `openat` on the directory with the previous dir and `fdopendir` with the basename of the new path instead of `opendir` with full path all the time.
    // #if 0
    //     fd_t prev_dir_fd = prev_iter ? prev_iter->dir_fd : AT_CWD;
    //     new_dir_fd = openat(prev_dir_fd, dir_passed_as_argument);
    //     fdopendir(dup(new_dir_fd));
    // #endif

    iter->iter_root_dir_nullter = arena_push_string_nullter(iter->arena, dir_not_nullter);
    ASSERT(string_nullter_is_actually_the_same_len(iter->iter_root_dir_nullter));
    iter->os_specific = arena_push_struct(iter->arena, DirectoryIter_OsSpecific, arena_flags_zero());
    iter->prev_iter = prev_iter;
    if (prev_iter) {
        iter->recursive_depth = prev_iter->recursive_depth + 1;
        // TODO: @DirectoryIterQuadraticMemory If a link infinite loop exists in a directory that has many subdirectories this program might run out of memory before the maximum depth is reached (the amount of memory increases quadratically per depth)
        if (iter->recursive_depth > DirectoryIter::MAXIMUM_RECURSIVE_DEPTH) {
            beat_log("[directory_iter] Failed to linuxsdl_directory_iter_init: dir '%.*s'\n", EXPAND_STR_FOR_PRINTF(dir_not_nullter));
            goto rage_quit;
        }
        iter->unique_dir_id = prev_iter->next_unique_dir_id;
        iter->next_unique_dir_id = iter->unique_dir_id + 1;
        iter->total_count_visited_files = prev_iter->total_count_visited_files;
        iter->total_count_visited_dirs = prev_iter->total_count_visited_dirs;
    } else {
        iter->recursive_depth = 0;
        iter->unique_dir_id = 0;
        iter->next_unique_dir_id = 1;
        iter->total_count_visited_files = 0;
        iter->total_count_visited_dirs = 0;
    }
    iter->is_recursive = is_recursive;
    iter->dirs_to_recurse.arena = iter->arena;

    if (dir_not_nullter.len <= 0)
        goto rage_quit;

    ASSERT(string_is_nullter(iter->iter_root_dir_nullter));
    iter->os_specific->dir = opendir((char *)iter->iter_root_dir_nullter.data);
    if (!iter->os_specific->dir)
        goto rage_quit;

    iter->inited = true;
    // beat_log("[directory_iter] linuxsdl_directory_iter_init: dir '%.*s'\n", EXPAND_STR_FOR_PRINTF(iter->iter_root_dir_nullter));

    return true;
rage_quit:
    end_temp_memory(&iter->temp_for_end);
    zero_struct(iter);
    iter->error = true;
    iter->inited = false;
    beat_log("[directory_iter] Failed to linuxsdl_directory_iter_init: dir '%.*s'\n", EXPAND_STR_FOR_PRINTF(dir_not_nullter));
    return false;
}

static void
linuxsdl_directory_iter__end_just_one(DirectoryIter *iter)
{
    // beat_log("[directory_iter] linuxsdl_directory_iter__end_just_one: ending for '%.*s'\n",
    //                 EXPAND_STR_FOR_PRINTF(iter->iter_root_dir_nullter));
    DirectoryIter *prev_iter = iter->prev_iter;

    ASSERT(iter->inited);
    if (!iter->ended) {
        // do nothing
    }

    if (iter->error) {
        // don't do anything
        beat_log("[directory_iter] linuxsdl_directory_iter_end has error dir '%.*s'\n", EXPAND_STR_FOR_PRINTF(iter->iter_root_dir_nullter));
    }

    if (iter->temp_for_next.arena){
        end_temp_memory(&iter->temp_for_next);
        zero_struct(&iter->item);
    }
    ASSERT(iter->os_specific->dir);
    int res = closedir(iter->os_specific->dir);
    ASSERT(res == 0);
    end_temp_memory(&iter->temp_for_end);
    if (prev_iter) {
        auto next_unique_dir_id        = iter->next_unique_dir_id;
        auto total_count_visited_files = iter->total_count_visited_files;
        auto total_count_visited_dirs  = iter->total_count_visited_dirs;
        *iter = *prev_iter;
        iter->next_unique_dir_id        = next_unique_dir_id;
        iter->total_count_visited_dirs  = total_count_visited_dirs;
        iter->total_count_visited_files = total_count_visited_files;
        if (iter->temp_for_next.arena) {
            end_temp_memory(&iter->temp_for_next);
            zero_struct(&iter->item);
        }
    } else {
        zero_struct(iter);
    }
}

// TODO: This uses too much memory for directories with many subdirectories,
// which is exactly our average case. Try to do something to make it use less
// memory while still recursing at the end.
// One hack would be to iterate through the directory twice. On the first time I return for files and directories.
// On the second time I ignore files and recurse into directories.
// But technically I could recurse into a directory without returning it through DirectoryIter.
PLATFORM_DIRECTORY_ITER_NEXT(PlatformApi::directory_iter_next)
{
    while (true) {
        ASSERT(iter->inited);
        ASSERT(!iter->error);
        iter->started = true;

        // Option 1: dir1, recurse dir1, dir2, recurse dir2, file1, file2
        // Option 2: dir1, dir2, recurse dir1, recurse dir2, file1, file2
        // Option 3: file1, file2, dir1, recurse dir1, dir2, recurse dir2
        // Option 4: file1, file2, dir1, dir2, recurse dir1, recurse dir2
        // Option 5: file1, dir1, file2, dir2, recurse dir1, recurse dir2
        // I think option 5 is the way to go.
        // I did option 5.


        if (iter->started_doing_recursion) {
            if (iter->dir_recurse_idx < iter->dirs_to_recurse.len) {
                String next_dir = iter->dirs_to_recurse.data[iter->dir_recurse_idx++];
                DirectoryIter *copy_iter = arena_push_struct(iter->arena, DirectoryIter, arena_flags_nozero());
                *copy_iter = *iter;
                // beat_log("[directory_iter] linuxsdl_directory_iter_next: init recursive thing for '%.*s'\n", EXPAND_STR_FOR_PRINTF(next_dir));
                bool ok = PlatformApi::directory_iter_init(iter, next_dir, iter->arena, true, copy_iter);
                if (!ok) {
                    // beat_log("[directory_iter] linuxsdl_directory_iter_next: recursive init failed for '%.*s'\n", EXPAND_STR_FOR_PRINTF(next_dir));
                    *iter = *copy_iter;
                    // we'll just ignore it and keep going.
                    // iter->error = true;
                    // goto rage_quit;
                }
                // return linuxsdl_directory_iter_next(iter);
                continue;
            } else {
                iter->ended = true;
                goto rage_quit;
            }
        }

        if (iter->do_recurse_for_last_directory) {
            iter->do_recurse_for_last_directory = false;
            // Instead of recursing now, push this directory into a buffer and recurse at the end. This will use more memory and will be slower, but I will guarantee that everything in one directory appears in order, and then we recurse into the directories.
            ASSERT(iter->temp_for_next.arena);
            String next_dir = iter->item.full_filename_nullter;
            u8 next_dir_stack_buf[next_dir.len+1];
            xmemcpy(next_dir_stack_buf, next_dir.data, next_dir.len+1);
            String next_dir_stack = {.len = next_dir.len, .data = next_dir_stack_buf};
            if (iter->temp_for_next.arena) {
                end_temp_memory(&iter->temp_for_next);
                zero_struct(&iter->item);
            }
            String thenext = arena_push_string_nullter(iter->arena, next_dir_stack);
            iter->dirs_to_recurse.push(thenext);
            // beat_log("[directory_iter] linuxsdl_directory_iter_next: add recursive entry for '%.*s'\n", EXPAND_STR_FOR_PRINTF(thenext));
            // return linuxsdl_directory_iter_next(iter);
            continue;
        }

        if (iter->temp_for_next.arena) {
            end_temp_memory(&iter->temp_for_next);
            zero_struct(&iter->item);
        }
        iter->temp_for_next = begin_temp_memory(iter->arena);

        ASSERT(iter->os_specific->dir);
        errno = 0;
        iter->os_specific->dirent = readdir(iter->os_specific->dir);
        if (errno != 0) {
            iter->error = true;
            goto rage_quit;
        }

        if (!iter->os_specific->dirent) {
            // ended successfully
            // TODO: recurse here
            iter->started_doing_recursion = iter->dirs_to_recurse.len > 0;
            if (iter->started_doing_recursion) {
                iter->dir_recurse_idx = 0;
                // return linuxsdl_directory_iter_next(iter);
                continue;
            } else {
                iter->ended = true;
            }
            goto rage_quit;
        }

        if (!strcmp(iter->os_specific->dirent->d_name, ".") || !strcmp(iter->os_specific->dirent->d_name, "..")) {
            // return linuxsdl_directory_iter_next(iter);
            continue;
        }
        // TODO: check dirent.d_type
        switch (iter->os_specific->dirent->d_type) {
        case DT_DIR: {
            iter->item.file_type = DirectoryIterFileType_Directory;
            iter->do_recurse_for_last_directory = iter->is_recursive;
            iter->total_count_visited_dirs++;
            iter->this_dir_count_visited_dirs++;
        } break;
        case DT_REG: {
            iter->item.file_type = DirectoryIterFileType_File;
            iter->total_count_visited_files++;
            iter->this_dir_count_visited_files++;
        } break;
        case DT_LNK: {
            // TODO: Call readlink.
            beat_log("[directory_iter] Ignoring symbolic link : %s/%s\n",
                     iter->iter_root_dir_nullter.data,
                     iter->os_specific->dirent->d_name);
            continue;
        } break;
        default: {
            // return linuxsdl_directory_iter_next(iter);
            continue;
        } break;
        }
        iter->item.is_on_iter_root = (iter->prev_iter == 0);

        {
            String d_name = cstr_to_string(iter->os_specific->dirent->d_name);
            ASSERT(d_name.len > 0);
            // I'm not sure if this is very compatible, maybe I should just decrease the length.
            ASSERT(d_name.data[d_name.len-1] != '/');
            if (iter->prev_iter) {
                ASSERT(iter->prev_iter->item.file_type == DirectoryIterFileType_Directory);
                iter->item.rel_filename_nullter = join_paths_nullter(iter->arena, iter->prev_iter->item.rel_filename_nullter, d_name);
            } else {
                iter->item.rel_filename_nullter = arena_push_string_nullter(iter->arena, d_name);
            }
        }

        String adir, afilename;
        get_base_dir_and_filename(iter->item.rel_filename_nullter, &adir, &afilename);
        iter->item.rel_dir_nullter       = arena_push_string_nullter(iter->arena, adir);
        iter->item.base_filename_nullter = afilename;
        iter->item.full_filename_nullter = join_paths_nullter(iter->arena, iter->iter_root_dir_nullter, iter->item.base_filename_nullter);
        iter->item.full_dir_nullter      = arena_push_string_nullter(iter->arena, get_base_dir(iter->item.full_filename_nullter));

        ASSERT(string_is_nullter(iter->item.rel_filename_nullter));
        ASSERT(string_is_nullter(iter->item.rel_dir_nullter));
        ASSERT(string_is_nullter(iter->item.base_filename_nullter));
        ASSERT(string_is_nullter(iter->item.full_filename_nullter));
        ASSERT(string_is_nullter(iter->item.full_dir_nullter));

        // beat_log("[directory_iter] linuxsdl_directory_iter_next used %ld high water mark: %ld\n", iter->arena->used, iter->arena->high_water_mark);


        return true;
rage_quit:
        if (iter->temp_for_next.arena) {
            end_temp_memory(&iter->temp_for_next);
            zero_struct(&iter->item);
        }

        if (iter->error) {
            beat_log("[directory_iter] linuxsdl_directory_iter_next: readdir error\n");
        } else {
            ASSERT(iter->ended);
        }

        if (iter->prev_iter) {
            linuxsdl_directory_iter__end_just_one(iter);
            // return linuxsdl_directory_iter_next(iter);
            continue;
        }
        return false;
    }
}

PLATFORM_DIRECTORY_ITER_END(PlatformApi::directory_iter_end)
{
    while (iter->prev_iter) {
        linuxsdl_directory_iter__end_just_one(iter);
    }
    beat_log("[directory_iter] linuxsdl_directory_iter_end: ending root for '%.*s'\n",
             EXPAND_STR_FOR_PRINTF(iter->iter_root_dir_nullter));
    linuxsdl_directory_iter__end_just_one(iter);
}


PLATFORM_TEXT_INPUT_START(PlatformApi::text_input_start)
{
    if (!SDL_IsTextInputActive()) {
        printf("-------START TEXT INPUT!!\n");
        SDL_StartTextInput();
    }
}

PLATFORM_TEXT_INPUT_STOP(PlatformApi::text_input_stop)
{
    if (SDL_IsTextInputActive()) {
        printf("-------STOP TEXT INPUT!!\n");
        SDL_StopTextInput();
    }
}

PLATFORM_TEXT_INPUT_SET_RECT(PlatformApi::text_input_set_rect)
{
    // SDL wants y top-down, but I want it bottom-up.
    SDL_Rect rect = {};
    rect.x = (s32)x;
    rect.y = window_height - (s32)(y + h);
    rect.w = (s32)w;
    rect.h = (s32)(h);
    SDL_SetTextInputRect(&rect);
}

PLATFORM_GET_CLIPBOARD(PlatformApi::get_clipboard)
{
    ASSERT(memory);
    if (memory->clipboard_from_previous_frame.data) {
        // If I grabbed the clipboard twice on the same frame I probably did something wrong in the UI code.
        printf("---- Warning: Got clipboard twice in the same frame!!\n");
        printf("---- Warning: Got clipboard twice in the same frame!!\n");
        printf("---- Warning: Got clipboard twice in the same frame!!\n");
        printf("---- Warning: Got clipboard twice in the same frame!!\n");
        return memory->clipboard_from_previous_frame;
    } else {
        char *clipboard_cstr = SDL_GetClipboardText();

        // TODO: If the clipboard has only one line and it ends with '\n' I should probably remove that character.
        String clipboard = {};
        if (clipboard_cstr) {
            clipboard = cstr_to_string(clipboard_cstr);
            if (clipboard.len > 0 && clipboard.data[clipboard.len-1] == '\n')
                clipboard.len -= 1;
            memory->clipboard_from_previous_frame = clipboard;
        }

        return clipboard;
    }
}

#define WORK_QUEUE_PRINT_STUFF 0
PLATFORM_ADD_WORK(PlatformApi::add_work)
{
    // NOTE: add work from only one thread.

    auto get_to_add = atomic_load(&q->entry_to_add);

    // TODO: don't mod it directly (if I do it there's a bug @WorkQueueABA1)
    auto next_entry_to_add = (get_to_add + 1) % ARRAY_COUNT(q->entries);

    ASSERT(ABC(get_to_add,          ARRAY_COUNT(q->entries)));
    ASSERT(ABC(next_entry_to_add,   ARRAY_COUNT(q->entries)));
    ASSERT(next_entry_to_add != atomic_load(&q->entry_to_do));

    auto *entry = &q->entries[get_to_add];
    ASSERT(!entry->fn); // if it's non zero, it didn't finish.
    ASSERT(!entry->data);
    // zero_struct(entry);
    STATIC_ASSERT(ssizeof(WorkQueueEntry) == 2*ssizeof(void*));
    entry->fn = fn;
    entry->data = data;

#if WORK_QUEUE_PRINT_STUFF
    printf("Adding work... fn %p data %p\n", fn, data);
#endif

    COMPILER_BARRIER();
    atomic_store(&q->entry_to_add, next_entry_to_add, std::memory_order_seq_cst);
    COMPILER_BARRIER();
    SDL_SemPost(q->os_specific->semaphore);
}

static int
linuxsdl_work_handler(void *data)
{
    MemoryArena temp_arena1 = {};
    MemoryArena temp_arena2 = {};
    init_arena(&temp_arena1, MEGABYTES(10), NULL, true);
    init_arena(&temp_arena2, MEGABYTES(10), NULL, true);

    // TODO: How to get the thread name here?
    auto sdl_thread_id = SDL_ThreadID();

    auto q = (WorkQueue *)data;
    while (true) {
        auto get_to_do = atomic_load(&q->entry_to_do);
        auto get_to_add = atomic_load(&q->entry_to_add);
        if (get_to_do != get_to_add) {
            auto next_entry_to_do = (get_to_do + 1) % ARRAY_COUNT(q->entries);
            // TODO: Technically there is a bug here @WorkQueueABA1. Consider the following operations.
            // N = entry count
            // entry_to_do is X and entry_to_add is X+1. It probably wouldn't happen unless core count > entry count
            //  T1: calculate next_entry_to_do
            //  T2: also calculate next_entry_to_do and do the cas succesfully.
            //  T1: cas fails.
            //  No problem, but
            //  T1: calculate next_entry_to_do (it assumed q.to_do != q.to_add)
            //  T1: pause
            //  T2: do the cas
            //  T2: Do it N or so more times.
            //  (now to_do is X and to_add is X -> there is no work to do)
            //  T1: resume
            //  T1: cas succeeds. (wrong) -> it will try to do entry[X] again (T2 already did it)
            //
            // Solution 1: We would need to compare the entry_to_add too. atomic_cas2(theptr, get_to_do, get_to_add, next_entry_to_do, get_to_add);
            // Even if T2 does the thing N or so more times, if to_do and to_add are both the same value after T1 resumes, it will be correct.
            if (!atomic_cas(&q->entry_to_do, get_to_do, next_entry_to_do))
                continue;
            ASSERT(ABC(get_to_do, ARRAY_COUNT(q->entries)));
            {
                Scoped_Temp_Arena temp_mem1(&temp_arena1);
                Scoped_Temp_Arena temp_mem2(&temp_arena1);
                // printf("Calling function!\n");
                auto *entry_ = &q->entries[get_to_do]; // TODO: Technically the queue could wrap around before the entry's fn and data is read.
                auto entry = *(WorkQueueEntry*)entry_;
                zero_size((void*)entry_, ssizeof(WorkQueueEntry));

                ASSERT_SLOW(entry.fn);
                u64 thread_stuff = 0; // TODO pass thread information
                entry.fn(thread_stuff, entry.data, temp_mem1.temp, temp_mem2.temp);
            }
            // COMPILER_BARRIER();
            // q->completed_count += 1;
            continue; // don't wait on the semaphore because there might be more.
        }
        // TODO: Only one thread is waking up...
        SDL_SemWait(q->os_specific->semaphore);
#if WORK_QUEUE_PRINT_STUFF
        printf("---Woke up from \033[0;31m%lx\033[0m---\n", sdl_thread_id>>12); // last 12 bits seem to always be 0x700
#endif
    }
    return 0;
}

static WorkQueue *
linuxsdl_make_work_queue(MemoryArena *arena, s32 count_threads)
{
    printf("Making WorkQueue with %d threads\n", count_threads);
    WorkQueue *q = arena_push_struct(arena, WorkQueue, arena_flags_zero());
    q->os_specific = arena_push_struct(arena, WorkQueue_OsSpecific, arena_flags_zero());
    q->os_specific->semaphore = SDL_CreateSemaphore(0);
    ASSERT(q->os_specific->semaphore);
    for (s32 i = 0; i < count_threads; i++) {
        char buf[128];
        snprintf(buf, ssizeof(buf), "WorkQueue0_%d", i);
        SDL_Thread *th = SDL_CreateThread(linuxsdl_work_handler, buf, q);
        ASSERT(th);
        SDL_DetachThread(th);
    }
    return q;
}

#if 0
static
WORKER_FN(worker_test)
{
    printf("test %s\n", data);
}
#endif

static int
linuxsdl_audio_decoder_thread_fn(void *)
{
    // NOTE: This is temporary code that will be deleted soon
    auto *const audio_state = &audio::global_audio_state;
    ASSERT(audio_state->is_initialized);
    auto perf_freq = SDL_GetPerformanceFrequency();
    u64 perf_counter_start = SDL_GetPerformanceCounter();
    u64 audio_decoder_frame_count = 0;
    while (true) {
        audio_decoder_frame_count += 1;
        s64 total_decoded_samples = 0;
        {
            SCOPED_SHARED_LOCK(&audio_state->decoders.shared_mutex);
            Forp (audio_state->decoders.active_list, _, decoder) {
                total_decoded_samples += audio::maybe_decode_audio(decoder);
            }
        }

        u64 perf_counter_before_sleep = SDL_GetPerformanceCounter();
        u64 perf_counter_end = perf_counter_before_sleep;//SDL_GetPerformanceCounter();
        u64 total_frame_perf_counter = perf_counter_end - perf_counter_start;
        u64 delta_perf_counter_c = perf_counter_end - perf_counter_before_sleep;
        f64 dt = (f64)total_frame_perf_counter*1/(f64)perf_freq;
        f64 dt_without_sleep = dt;

        f64 test_target_dt = 100/1000.0;
        complete_time_interval(total_decoded_samples > 0, test_target_dt,
                               perf_counter_start, perf_counter_before_sleep,
                               &perf_counter_end, &total_frame_perf_counter, &delta_perf_counter_c, &dt);
        perf_counter_start = perf_counter_end;

        if (!true && total_decoded_samples > 0) {
            printf("Audio decoder samples: %ld, %8.2f samples/ms  dt: %.2fms , with sleep %.2fms\n",
                   total_decoded_samples,
                   (f64)total_decoded_samples / (1000*dt_without_sleep),
                   1000*dt_without_sleep, 1000*dt);
        }
    }
}

static void
start_audio_decoder_thread()
{
    // NOTE: This is temporary code that will be deleted soon
    auto *th = SDL_CreateThread(linuxsdl_audio_decoder_thread_fn, "AudioDecoder", 0);
    ASSERT(th);
    SDL_DetachThread(th);
    printf("Started audio decoder thread\n");
}




GAME_UPDATE_AND_RENDER(game_update_and_render);
void game_report_shit(GameMemory *memory, f64 dt_a, f64 dt_b, f64 dt);

GameMemory *GAME_MEMORY = 0;

static void
test_sim_sdl(String arg, b32 arg_bool)
{
#if 0
    {
        struct timespec ts;
        if (clock_gettime(CLOCK_MONOTONIC_RAW, &ts) != 0) {
            printf("No CLOCK_MONOTONIC_RAW. Can't run the game!\n");
            exit(1);
        }
    }
#endif

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        beat_log("SDL_Init failed\n");
        return;
    }

    defer { SDL_Quit(); };

    audio::init();
    start_audio_decoder_thread(); // NOTE: This is temporary code that will be deleted soon
    defer { audio::deinit(); };

    fflush(stdout);
    fflush(stderr);
    printf("-------------- init sdl and audio ok --------------\n");

    MemoryArena platform_arena = {};
    init_arena(&platform_arena, MEGABYTES(10), NULL, true);

#if BEAT_INTERNAL
    {
        // Don't chdir.
        beat_log("Game data path (BEAT_INTERNAL build): '%s'\n", "./");
    }
#else
    {
        char *sdl_base_path = SDL_GetBasePath();
        if (sdl_base_path) {
            int res = chdir(sdl_base_path);
            if (res != 0) {
                printf("chdir into sdl_base_path failed!\n");
                return;
            }
            beat_log("Game data path (Release build): '%s'\n", sdl_base_path);
        } else {
            beat_log("error: SDL_GetBasePath failed!\n");
            beat_log("Game data path (Release build): '%s'\n", "./");
        }
    }
#endif

#if 0
    {
        char *base_path = SDL_GetBasePath(); // The executable's parent directory, ending with a path separator.
        printf("base_path: %s\n", base_path);
        char *pref_path = SDL_GetPrefPath("beatgame_company", "beatgame"); // "/home/user/.local/share/company/program/"
        printf("pref_path: %s\n", pref_path);
        exit(0);
    }
#endif

#if 0
    {
        auto clock_start = linuxsdl_get_clock();

        // sdl mutex is much faster than TicketMutex when there is a lot of contention
#define USE_SDL_MUTEX 1
        smem num_threads = 40;
        smem num_iters = 2000;
        struct Thing {
#if USE_SDL_MUTEX
            Mutex mutex;
#else
            TicketMutex mutex;
#endif
            s64 test_value;
            s64 num_iters;
        };
        auto thread_fn = [](void *data)->int {
            auto *thing = (Thing*)data;
            auto num_iters = thing->num_iters;
            u8 buf1[100*1024];
            u8 buf2[100*1024];
            for (s64 i = 1; i <= num_iters; i++) {
                {
                    SCOPED_LOCK(&thing->mutex);
                    thing->test_value += i;
                }
                // xmemcpy(buf1, buf2, sizeof(buf1));
                // atomic_add(&thing->test_value, i);
            }
            return 0;
        };
        NewArrayDynamic<SDL_Thread*> live_threads = {};
        live_threads.reserve(num_threads);
        Thing thing = {};
#if USE_SDL_MUTEX
        thing.mutex = mutex_create();
#endif
        thing.num_iters = num_iters;
        for (smem i = 0; i < num_threads; i++) {
            auto *th = SDL_CreateThread(thread_fn, "ThreadName", &thing);
            ASSERT(th);
            live_threads.push(th);
        }
        for (smem i = 0; i < live_threads.len; i++) {
            SDL_WaitThread(live_threads.data[i], NULL);
            live_threads.data[i--] = live_threads.data[--live_threads.len];
        }
        s64 correct_result = num_threads * ((num_iters*((num_iters+1))/2));
        auto seconds_elapsed = (f64)(linuxsdl_get_clock() - clock_start) * linuxsdl_get_secs_per_clock();
        printf("%s correct: %ld     calculated: %ld     threads: %ld  iters: %ld  total: %ld\n",
               correct_result == thing.test_value ? "SUCCESS" : "FAILURE",
               correct_result, thing.test_value,
               num_threads, num_iters, num_threads*num_iters);
        printf("Done after %.2fms\n", 1000*seconds_elapsed);
        exit(0);
    }
#endif

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    // SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    // SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    // TODO: Don't use SDL_GL_MULTISAMPLESAMPLES
    if (true) {
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
    } else {
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);
    }

    SDL_Window *window = SDL_CreateWindow("beat",
                                          SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED,
                                          1280,
                                          720,
                                          SDL_WINDOW_OPENGL);
    if (window == NULL) {
        beat_log("SDL_CreateWindow failed\n");
        return;
    }
    defer { SDL_DestroyWindow(window); };


    // TODO: Look at https://wiki.libsdl.org/SDL_GLprofile?highlight=%28%5CbCategoryEnum%5Cb%29%7C%28CategoryVideo%29

    SDL_GLContext glcontext = SDL_GL_CreateContext(window);
    defer { SDL_GL_DeleteContext(glcontext); };


    SDL_GL_MakeCurrent(window, glcontext);
    {
        int i;
#define DOIT(attr) \
        SDL_GL_GetAttribute(attr, &i);\
        printf("%s: %d\n", #attr, i);
        DOIT(SDL_GL_CONTEXT_PROFILE_MASK);
        DOIT(SDL_GL_CONTEXT_MAJOR_VERSION);
        DOIT(SDL_GL_CONTEXT_MINOR_VERSION);
        DOIT(SDL_GL_MULTISAMPLEBUFFERS);
        DOIT(SDL_GL_MULTISAMPLESAMPLES);
#undef DOIT
    }

    if (SDL_GL_LoadLibrary(NULL) != 0) {
        beat_log("SDL_GL_LoadLibrary failed\n");
        return;
    }
    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress)) {
        beat_log("gladLoadGLLoader failed\n");
        return;
    }

    // I fixed my problem with fullscreen limited to 144fps. I changed nvidia settings to BLIP instead of FLIP (Allow Flipping = false).
    // NOTE: if __GL_SYNC_TO_VBLANK or vblank_mode env var (on linux) is set, most programs will read that and try to use that value. If a program doesn't want to use that value it can ignore it (most games let you choose).
    enum FrameLimiterMode {
        FrameLimiterMode_Unlimited,
        FrameLimiterMode_Limited,
        FrameLimiterMode_Vsync,
        FrameLimiterMode_VsyncAdaptive,
    };
    // TODO: Read the second config file!
    FrameLimiterMode frame_limiter_mode = FrameLimiterMode_Vsync;
    if (arg == "-novsync"_s) {
        frame_limiter_mode = FrameLimiterMode_Limited;
    }
    if (arg == "-unlimited-fps"_s) {
        frame_limiter_mode = FrameLimiterMode_Unlimited;
    }
    if (arg == "-limited-fps"_s) {
        frame_limiter_mode = FrameLimiterMode_Limited;
    }
    switch (frame_limiter_mode) {
    case FrameLimiterMode_Unlimited: {
        SDL_GL_SetSwapInterval(0);
        printf("Frame limiter mode: Unlimited\n");
    } break;
    case FrameLimiterMode_Limited: {
        SDL_GL_SetSwapInterval(0);
        printf("Frame limiter mode: Limited\n");
    } break;
    case FrameLimiterMode_Vsync: {
        SDL_GL_SetSwapInterval(1);
        printf("Frame limited mode: Vsync\n");
    } break;
    case FrameLimiterMode_VsyncAdaptive: {
        printf("Frame limited mode: Vsync Adaptive\n");
        auto result = SDL_GL_SetSwapInterval(-1);
        if (result != 0) {
            SDL_GL_SetSwapInterval(1);
            frame_limiter_mode = FrameLimiterMode_Vsync;
            printf("Vsync Adaptive FAILED! Setting Vsync to Yes\n");
        }
    } break;
    }

    GameMemory game_memory = {};

    GAME_MEMORY = &game_memory;
#if BEAT_TIME_MULTIPLIER
    game_memory.debug_time_multiplier = 1.0f;
#endif
    // TODO: Verify that I don't rely on the arenas being initialized to zero anywhere.
    init_arena(&game_memory.permanent,   MEGABYTES(10), NULL, true);
    init_arena(&game_memory.temporary,   MEGABYTES(20), NULL, true);
    init_arena(&game_memory.songs_arena, MEGABYTES(10), NULL, true);
    init_arena(&game_memory.playing,     MEGABYTES(10), NULL, true);


    s32 core_count = SDL_GetCPUCount();
    printf("CPU has %d cores\n", core_count);
    game_memory.low_priority_queue = linuxsdl_make_work_queue(&platform_arena, MAXIMUM(core_count-1, 2));

#if 0
    {
        char *strs[] = {
            "r0",
            "r1",
            "r2",
            "r3",
            "r4",
            "r5",
            "r6",
            "r7",
            "r8",
            "r9",
            "r10",
            "r11",
        };
        for (s32 i = 0; i < ARRAY_COUNT(strs); i++) {
            game_memory.platform_api.add_work(game_memory.low_priority_queue, worker_test, strs[i]);
        }
        // usleep(2*1000*1000);
        // exit(0);
    }
#endif

    // The first frame's dt
    // f32 dt = 0.016666f;
    f64 dt = 0.016666;
    GameUpdateAndRenderFn *game_uar = game_update_and_render;
    GameInput gin = {};
    // First frame
    gin.dt = dt;
    gin.debug_dt_frame = dt;
    gin.debug_dt_a = 0.5*dt;
    gin.debug_dt_b = 0.5*dt;
    gin.debug_dt_c = 0;

    // NOTE: I don't resolve scan codes. I could do that in the future.
    // TODO: SDL has more scancodes (about 286 in total). TODO: Map all those.
    u32 mapped_keyboard_codes[BEAT_KEY__KEYBOARD_END] = {
        [BEAT_KEY_INVALID]            = 0,
        [BEAT_KEY_SPACE]              = SDL_SCANCODE_SPACE,
        [BEAT_KEY_A]                  = SDL_SCANCODE_A,
        [BEAT_KEY_B]                  = SDL_SCANCODE_B,
        [BEAT_KEY_C]                  = SDL_SCANCODE_C,
        [BEAT_KEY_D]                  = SDL_SCANCODE_D,
        [BEAT_KEY_E]                  = SDL_SCANCODE_E,
        [BEAT_KEY_F]                  = SDL_SCANCODE_F,
        [BEAT_KEY_G]                  = SDL_SCANCODE_G,
        [BEAT_KEY_H]                  = SDL_SCANCODE_H,
        [BEAT_KEY_I]                  = SDL_SCANCODE_I,
        [BEAT_KEY_J]                  = SDL_SCANCODE_J,
        [BEAT_KEY_K]                  = SDL_SCANCODE_K,
        [BEAT_KEY_L]                  = SDL_SCANCODE_L,
        [BEAT_KEY_M]                  = SDL_SCANCODE_M,
        [BEAT_KEY_N]                  = SDL_SCANCODE_N,
        [BEAT_KEY_O]                  = SDL_SCANCODE_O,
        [BEAT_KEY_P]                  = SDL_SCANCODE_P,
        [BEAT_KEY_Q]                  = SDL_SCANCODE_Q,
        [BEAT_KEY_R]                  = SDL_SCANCODE_R,
        [BEAT_KEY_S]                  = SDL_SCANCODE_S,
        [BEAT_KEY_T]                  = SDL_SCANCODE_T,
        [BEAT_KEY_U]                  = SDL_SCANCODE_U,
        [BEAT_KEY_V]                  = SDL_SCANCODE_V,
        [BEAT_KEY_W]                  = SDL_SCANCODE_W,
        [BEAT_KEY_X]                  = SDL_SCANCODE_X,
        [BEAT_KEY_Y]                  = SDL_SCANCODE_Y,
        [BEAT_KEY_Z]                  = SDL_SCANCODE_Z,
        [BEAT_KEY_0]                  = SDL_SCANCODE_0,
        [BEAT_KEY_1]                  = SDL_SCANCODE_1,
        [BEAT_KEY_2]                  = SDL_SCANCODE_2,
        [BEAT_KEY_3]                  = SDL_SCANCODE_3,
        [BEAT_KEY_4]                  = SDL_SCANCODE_4,
        [BEAT_KEY_5]                  = SDL_SCANCODE_5,
        [BEAT_KEY_6]                  = SDL_SCANCODE_6,
        [BEAT_KEY_7]                  = SDL_SCANCODE_7,
        [BEAT_KEY_8]                  = SDL_SCANCODE_8,
        [BEAT_KEY_9]                  = SDL_SCANCODE_9,
        [BEAT_KEY_APOSTROPHE]         = SDL_SCANCODE_APOSTROPHE,
        [BEAT_KEY_COMMA]              = SDL_SCANCODE_COMMA,
        [BEAT_KEY_MINUS]              = SDL_SCANCODE_MINUS,
        [BEAT_KEY_DOT]                = SDL_SCANCODE_PERIOD,
        [BEAT_KEY_SLASH]              = SDL_SCANCODE_SLASH,
        [BEAT_KEY_SEMICOLON]          = SDL_SCANCODE_SEMICOLON,
        [BEAT_KEY_EQUALS]             = SDL_SCANCODE_EQUALS,
        [BEAT_KEY_LBRACKET]           = SDL_SCANCODE_LEFTBRACKET,
        [BEAT_KEY_RBRACKET]           = SDL_SCANCODE_RIGHTBRACKET,
        [BEAT_KEY_BACKSPACE]          = SDL_SCANCODE_BACKSPACE,
        [BEAT_KEY_BACKSLASH]          = SDL_SCANCODE_BACKSLASH,
        [BEAT_KEY_NONUSBACKSLASH]     = SDL_SCANCODE_NONUSBACKSLASH,
        [BEAT_KEY_ENTER]              = SDL_SCANCODE_RETURN,
        [BEAT_KEY_ESCAPE]             = SDL_SCANCODE_ESCAPE,
        [BEAT_KEY_GRAVE]              = SDL_SCANCODE_GRAVE,
        [BEAT_KEY_TAB]                = SDL_SCANCODE_TAB,
        [BEAT_KEY_CAPSLOCK]           = SDL_SCANCODE_CAPSLOCK,
        [BEAT_KEY_LSHIFT]             = SDL_SCANCODE_LSHIFT,
        [BEAT_KEY_RSHIFT]             = SDL_SCANCODE_RSHIFT,
        [BEAT_KEY_LCTRL]              = SDL_SCANCODE_LCTRL,
        [BEAT_KEY_RCTRL]              = SDL_SCANCODE_RCTRL,
        [BEAT_KEY_LALT]               = SDL_SCANCODE_LALT,
        [BEAT_KEY_RALT]               = SDL_SCANCODE_RALT,
        [BEAT_KEY_LWIN]               = SDL_SCANCODE_LGUI,
        [BEAT_KEY_RWIN]               = SDL_SCANCODE_RGUI,
        [BEAT_KEY_F1]                 = SDL_SCANCODE_F1,
        [BEAT_KEY_F2]                 = SDL_SCANCODE_F2,
        [BEAT_KEY_F3]                 = SDL_SCANCODE_F3,
        [BEAT_KEY_F4]                 = SDL_SCANCODE_F4,
        [BEAT_KEY_F5]                 = SDL_SCANCODE_F5,
        [BEAT_KEY_F6]                 = SDL_SCANCODE_F6,
        [BEAT_KEY_F7]                 = SDL_SCANCODE_F7,
        [BEAT_KEY_F8]                 = SDL_SCANCODE_F8,
        [BEAT_KEY_F9]                 = SDL_SCANCODE_F9,
        [BEAT_KEY_F10]                = SDL_SCANCODE_F10,
        [BEAT_KEY_F11]                = SDL_SCANCODE_F11,
        [BEAT_KEY_F12]                = SDL_SCANCODE_F12,
        [BEAT_KEY_F13]                = SDL_SCANCODE_F13,
        [BEAT_KEY_F14]                = SDL_SCANCODE_F14,
        [BEAT_KEY_F15]                = SDL_SCANCODE_F15,
        [BEAT_KEY_F16]                = SDL_SCANCODE_F16,
        [BEAT_KEY_F17]                = SDL_SCANCODE_F17,
        [BEAT_KEY_F18]                = SDL_SCANCODE_F18,
        [BEAT_KEY_F19]                = SDL_SCANCODE_F19,
        [BEAT_KEY_F20]                = SDL_SCANCODE_F20,
        [BEAT_KEY_F21]                = SDL_SCANCODE_F21,
        [BEAT_KEY_F22]                = SDL_SCANCODE_F22,
        [BEAT_KEY_F23]                = SDL_SCANCODE_F23,
        [BEAT_KEY_F24]                = SDL_SCANCODE_F24,
        [BEAT_KEY_PRINTSCREEN]        = SDL_SCANCODE_PRINTSCREEN,
        [BEAT_KEY_SCROLLLOCK]         = SDL_SCANCODE_SCROLLLOCK,
        [BEAT_KEY_PAUSE]              = SDL_SCANCODE_PAUSE,
        [BEAT_KEY_INSERT]             = SDL_SCANCODE_INSERT,
        [BEAT_KEY_HOME]               = SDL_SCANCODE_HOME,
        [BEAT_KEY_PAGEUP]             = SDL_SCANCODE_PAGEUP,
        [BEAT_KEY_DELETE]             = SDL_SCANCODE_DELETE,
        [BEAT_KEY_END]                = SDL_SCANCODE_END,
        [BEAT_KEY_PAGEDOWN]           = SDL_SCANCODE_PAGEDOWN,
        [BEAT_KEY_UP]                 = SDL_SCANCODE_UP,
        [BEAT_KEY_DOWN]               = SDL_SCANCODE_DOWN,
        [BEAT_KEY_LEFT]               = SDL_SCANCODE_LEFT,
        [BEAT_KEY_RIGHT]              = SDL_SCANCODE_RIGHT,
        [BEAT_KEY_KP_NUMLOCK]         = SDL_SCANCODE_NUMLOCKCLEAR,
        [BEAT_KEY_KP_SLASH]           = SDL_SCANCODE_KP_DIVIDE,
        [BEAT_KEY_KP_ASTERISK]        = SDL_SCANCODE_KP_MULTIPLY,
        [BEAT_KEY_KP_MINUS]           = SDL_SCANCODE_KP_MINUS,
        [BEAT_KEY_KP_PLUS]            = SDL_SCANCODE_KP_PLUS,
        [BEAT_KEY_KP_DOT]             = SDL_SCANCODE_KP_PERIOD,
        [BEAT_KEY_KP_ENTER]           = SDL_SCANCODE_KP_ENTER,
        [BEAT_KEY_KP_COMMA]           = SDL_SCANCODE_KP_COMMA,
        [BEAT_KEY_KP_0]               = SDL_SCANCODE_KP_0,
        [BEAT_KEY_KP_00]              = SDL_SCANCODE_KP_00,
        [BEAT_KEY_KP_000]             = SDL_SCANCODE_KP_000,
        [BEAT_KEY_KP_1]               = SDL_SCANCODE_KP_1,
        [BEAT_KEY_KP_2]               = SDL_SCANCODE_KP_2,
        [BEAT_KEY_KP_3]               = SDL_SCANCODE_KP_3,
        [BEAT_KEY_KP_4]               = SDL_SCANCODE_KP_4,
        [BEAT_KEY_KP_5]               = SDL_SCANCODE_KP_5,
        [BEAT_KEY_KP_6]               = SDL_SCANCODE_KP_6,
        [BEAT_KEY_KP_7]               = SDL_SCANCODE_KP_7,
        [BEAT_KEY_KP_8]               = SDL_SCANCODE_KP_8,
        [BEAT_KEY_KP_9]               = SDL_SCANCODE_KP_9,
        [BEAT_KEY_THOUSANDSSEPARATOR] = SDL_SCANCODE_THOUSANDSSEPARATOR,
        [BEAT_KEY_INTERNATIONAL1]     = SDL_SCANCODE_INTERNATIONAL1,
        [BEAT_KEY_INTERNATIONAL2]     = SDL_SCANCODE_INTERNATIONAL2,
        [BEAT_KEY_INTERNATIONAL3]     = SDL_SCANCODE_INTERNATIONAL3,
        [BEAT_KEY_INTERNATIONAL4]     = SDL_SCANCODE_INTERNATIONAL4,
        [BEAT_KEY_INTERNATIONAL5]     = SDL_SCANCODE_INTERNATIONAL5,
        [BEAT_KEY_INTERNATIONAL6]     = SDL_SCANCODE_INTERNATIONAL6,
        [BEAT_KEY_INTERNATIONAL7]     = SDL_SCANCODE_INTERNATIONAL7,
        [BEAT_KEY_INTERNATIONAL8]     = SDL_SCANCODE_INTERNATIONAL8,
        [BEAT_KEY_INTERNATIONAL9]     = SDL_SCANCODE_INTERNATIONAL9,
        [BEAT_KEY_ERASEEAZE]          = SDL_SCANCODE_ALTERASE,
    };
    // Maybe it would be better to define these together with the platform macro.
    // The static assert is to make sure I don't forget to add the scan code here in case I add
    STATIC_ASSERT(BEAT_KEY__KEYBOARD_END == 256+1 + 86);
    // xmemcpy(gin.mapped_keyboard_codes, mapped_keyboard_codes, ssizeof(mapped_keyboard_codes));

    u64 perf_freq = SDL_GetPerformanceFrequency();
    u64 perf_counter_start = SDL_GetPerformanceCounter();
    u64 game_start_perf_counter = perf_counter_start;
    f64 os_time_seconds_frame_start = (f64)(perf_counter_start - game_start_perf_counter) *1/ (f64)perf_freq;


    // Set up the keys.
    for (s32 idx_key = 0; idx_key < ARRAY_COUNT(gin.raw_keys); idx_key++) {
        ButtonInput *bi = &gin.raw_keys[idx_key];
        bi->state = ButtonState_NULL;
        bi->os_clock = perf_counter_start;
        bi->os_time_seconds = os_time_seconds_frame_start;
    }
    // Set up the mouse buttons.
    for (s32 idx_key = 0; idx_key < ARRAY_COUNT(gin.mouse.buttons); idx_key++) {
        ButtonInput *bi = &gin.mouse.buttons[idx_key].bi;
        bi->state = ButtonState_NULL;
        bi->os_clock = perf_counter_start;
        bi->os_time_seconds = os_time_seconds_frame_start;
    }

    SDL_StopTextInput();

    gin.text_composition.arena = 0;
    gin.text_composition.reserve(4096);

    {
        // If I don't do this and the first frame takes a long time, the window will be ugly.
        glClearColor(0.0f, 0.9f, 0.9f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        SDL_GL_SwapWindow(window);
    }

    bool window_has_focus = true;

    while (global_running) {
        arena_clear(&game_memory.temporary);
        game_memory.temporary.high_water_mark = game_memory.temporary.used;

        // Input
        {
            xmemcpy(gin.last_raw_keys, gin.raw_keys, ssizeof(gin.raw_keys));
            xmemcpy(&gin.last_mouse, &gin.mouse, ssizeof(gin.mouse));

            // NOTE: Previously, I was only setting the length to zero here. That was the bug. It didn't reset `data` so when I did `gin.text_input.append(text_str)` it thought it already had memory so it didn't call realloc and arena->used didn't get updated.
            // I would have caught this bug with arena_clear_id_check.
            gin.text_input = {};
            gin.text_input.arena = &game_memory.temporary;

#if 0
            {
                // clear fake mouse wheel buttons to generate a RELEASE later
                MouseButtonInput *mbi = 0;
                mbi = &gin.mouse.buttons[BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_WHEEL_DOWN)];
                mbi->bi.state = ButtonState_NULL;
                mbi->bi.os_clock = perf_counter_start;
                mbi->bi.os_time_seconds = os_time_seconds_frame_start;
                mbi = &gin.mouse.buttons[BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_WHEEL_UP)];
                mbi->bi.state = ButtonState_NULL;
                mbi->bi.os_clock = perf_counter_start;
                mbi->bi.os_time_seconds = os_time_seconds_frame_start;

                mbi = &gin.last_mouse.buttons[BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_WHEEL_DOWN)];
                mbi->bi.state = ButtonState_NULL;
                mbi = &gin.last_mouse.buttons[BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_WHEEL_UP)];
                mbi->bi.state = ButtonState_NULL;
            }
#endif

            {
                // free clipboard from the previous frame
                if (game_memory.clipboard_from_previous_frame.data) {
                    SDL_free(game_memory.clipboard_from_previous_frame.data);
                    game_memory.clipboard_from_previous_frame = {};
                }
            }

            // s32 last_event_text_editing_stopped_at = -123;
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                case SDL_QUIT: global_running = false; break;
                case SDL_WINDOWEVENT: {
                    switch (event.window.event) {
                    // TODO: SDL_WINDOWEVENT -> SDL_WINDOWEVENT_CLOSE, _FOCUS_GAINED, _FOCUS_LOST, _MAXIMIZED, _MINIMIZED, _RESTORED, _SIZE_CHANGED
                    // https://wiki.libsdl.org/SDL_WindowEvent
                    // https://wiki.libsdl.org/SDL_WindowEventID
                    case SDL_WINDOWEVENT_CLOSE: {
                        global_running = false;
                    } break;
                    case SDL_WINDOWEVENT_FOCUS_GAINED: {
                        // printf("----- Focus gained! -----\n");
                        window_has_focus = true;
                    } break;
                    case SDL_WINDOWEVENT_FOCUS_LOST: {
                        // printf("----- Focus lost! -----\n");
                        window_has_focus = false;
                    } break;
                    // case SDL_WINDOWEVENT_SIZE_CHANGED: {
                    // } break;
                    default: break;
                    }
                } break;
                case SDL_FINGERUP: {} break; // TODO
                case SDL_FINGERDOWN: {} break; // TODO
                case SDL_FINGERMOTION: {} break; // TODO

                case SDL_KEYUP:
                case SDL_KEYDOWN: {
                    // TODO: Can I receive more than one KEYDOWN/UP per key per frame? If I can my code is a bit broken.
                    if (event.key.keysym.scancode == SDL_SCANCODE_F4
                        && event.type == SDL_KEYDOWN
                        && (event.key.keysym.mod & KMOD_ALT)) {
                        printf("Pressed ALT-F4. Closing.\n");
                        global_running = false;
                    } else {
                        bool found_a_mapping = false;
                        for (s32 i = 0; i < ARRAY_COUNT(mapped_keyboard_codes); i++) {
                            if (event.key.keysym.scancode == (int)mapped_keyboard_codes[i]) {
                                KeyboardMods themods = {};
                                u16 sdlmods = event.key.keysym.mod;
                                if (sdlmods & KMOD_SHIFT)   themods |= KeyboardMods_Shift;
                                if (sdlmods & KMOD_CTRL)    themods |= KeyboardMods_Ctrl;
                                if (sdlmods & KMOD_ALT)     themods |= KeyboardMods_Alt;
                                if (sdlmods & KMOD_GUI)     themods |= KeyboardMods_Win;

                                if (event.type == SDL_KEYDOWN) {
                                    // event.key.timestamp won't help me at all.
                                    // u64 threshold_for_repeat = perf_freq/30;
                                    if (event.key.repeat) {
                                        // if (perf_counter_start-gin.raw_keys[i].os_clock > threshold_for_repeat)
                                        ASSERT(gin.raw_keys[i].state & ButtonState_DOWN);
                                        if (themods == gin.raw_keys[i].mods_for_press_or_release) {
                                            // Only set repeat if we have the same mods.
                                            gin.raw_keys[i].state |= ButtonState_REPEAT;
                                            gin.raw_keys[i].mods_for_press_or_release = themods;
                                        }
                                    } else {
                                        gin.raw_keys[i].state = ButtonState_DOWN;
                                        gin.raw_keys[i].mods_for_press_or_release = themods;
                                        gin.raw_keys[i].os_clock = perf_counter_start;
                                        gin.raw_keys[i].os_time_seconds = os_time_seconds_frame_start;
                                    }
                                } else if (event.type == SDL_KEYUP) {
                                    gin.raw_keys[i].state = ButtonState_NULL;
                                    gin.raw_keys[i].mods_for_press_or_release = themods;
                                    gin.raw_keys[i].os_clock = perf_counter_start;
                                    gin.raw_keys[i].os_time_seconds = os_time_seconds_frame_start;
                                }
                                found_a_mapping = true;
                                break;
                            }
                        }
                        if (!found_a_mapping) {
                            static u32 last_not_found_scancode = 0;
                            if (event.key.keysym.scancode != last_not_found_scancode) {
                                last_not_found_scancode = event.key.keysym.scancode;
                                // TODO: write to a log file
                                printf("XXX not mapped scancode: %u --------\n", event.key.keysym.scancode);
                            }
                        }
                    }
                } break;
                case SDL_MOUSEBUTTONUP:
                case SDL_MOUSEBUTTONDOWN: {
                    s32 mybutton_idx = -1;
                    if (event.button.button == SDL_BUTTON_LEFT) {
                        mybutton_idx = BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_LEFT);
                    } else if (event.button.button == SDL_BUTTON_MIDDLE) {
                        mybutton_idx = BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_MIDDLE);
                    } else if (event.button.button == SDL_BUTTON_RIGHT) {
                        mybutton_idx = BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_RIGHT);
                    } else if (event.button.button == SDL_BUTTON_X1) {
                        mybutton_idx = BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_X1);
                    } else if (event.button.button == SDL_BUTTON_X2) {
                        mybutton_idx = BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_X2);
                    } else {
                        // ASSERT(!"SDL gave me too many buttons!");
                        // Just ignore it.
                        break;
                    }
                    MouseButtonInput *mbi = &gin.mouse.buttons[mybutton_idx];
                    printf("Mouse button clicks: %d\n", event.button.clicks);
                    // SDL Gives clicks like this
                    // down     1
                    // up       1
                    // down     2
                    // up       2
                    // wait
                    // down     1
                    // up       1
                    // I'll do the same thing.
                    // I won't clear the clicks on the next frame because that's useless.
                    // If I want to bind something to a double click it's easy: check for ButtonState_RELEASE and click == 2.
                    // But I can't bind an action for 1 click that won't run if it was 2 clicks.
                    // If I want that for some reason I'll have to do something specific for that case, not something generic in
                    // MouseInput.
                    if (event.type == SDL_MOUSEBUTTONDOWN) {
                        printf("Mouse button down event: %d\n", mybutton_idx);

                        // mods will be set later
                        mbi->bi.state = ButtonState_DOWN;
                        mbi->bi.os_clock = perf_counter_start;
                        mbi->bi.os_time_seconds = os_time_seconds_frame_start;
                        mbi->clicks = event.button.clicks;
                    } else if (event.type == SDL_MOUSEBUTTONUP) {
                        printf("Mouse button up event: %d\n", mybutton_idx);

                        // mods will be set later
                        mbi->bi.state = ButtonState_NULL;
                        mbi->bi.os_clock = perf_counter_start;
                        mbi->bi.os_time_seconds = os_time_seconds_frame_start;
                        mbi->clicks = event.button.clicks;
                    }
                } break;
                case SDL_MOUSEWHEEL: {
                    s32 new_delta = event.wheel.direction == SDL_MOUSEWHEEL_FLIPPED ? -event.wheel.y : event.wheel.y;
                    ASSERT(new_delta != 0);
                    // s32 overflow is ub..
                    gin.mouse.wheel_dont_use_me = (s32)((u32)gin.mouse.wheel_dont_use_me + (u32)new_delta);
#if 0
                    MouseButtonInput *mbi = 0;
                    if (new_delta < 0)  mbi = &gin.mouse.buttons[BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_WHEEL_DOWN)];
                    else                mbi = &gin.mouse.buttons[BEAT_KEY_MOUSE_TO_IDX(BEAT_KEY_MOUSE_WHEEL_UP)];
                    mbi->bi.state = ButtonState_DOWN;
                    mbi->bi.os_clock = perf_counter_start;
                    mbi->bi.os_time_seconds = os_time_seconds_frame_start;
                    mbi->clicks = 1;
#endif
                } break;
                case SDL_TEXTINPUT: {
                    auto text_str = cstr_to_string(event.text.text);
                    printf("Got text input: '%s'\n", event.text.text);
                    gin.text_input.append(text_str);
                } break;
                case SDL_TEXTEDITING: {
                    // IME works now! (on linuxsdl)
                    // TODO: Compile SDL myself
                    // TODO
                    // [x] show commited text (SDL_TEXTINPUT)
                    // [x] show composition (SDL_TEXTEDITING)
                    // [x] show the candidate list at the right spot (SDL_SetTextInputRect)
                    // [x] portuguese dead keys (it magically started working)
                    // [ ] fix flicker [あ] -> press enter -> あ[あ] -> あ  -- あ[あ] shouldn't appear
                    // [ ] highlight the current thing (i don't know how to do it)
                    //  for example (ibus+anthy): () is the highlight, [] is preedit,
                    //  type [こーひーがすきなひと], press space, press right arrow
                    //  it should show [コーヒーが(好きな)人] but I don't highlight anything, 
                    //  I just draw a line under the entire preedit area.

                    // I don't have to ignore SOME bindings.
                    // If I have a composition active and want to press ESCAPE, the game won't receive that keypress.
                    // If I am not in a composition and type 'a', the game will receive both text 'a' and SDL_SCANCODE_A.
                    // If I am not in a composition and press CTRL-a, the game will not receive text 'a' but will receive SDL_SCANCODE_A.
                    // If I press CTRL-= I will receive SDL_SCANCODE_EQUALS and text '=' too. (this is bad, so i'll have to ignore bindings).
                    //
                    // Example: I'm selecting a song with search active and want to change the rate (binding CTRL-=).
                    // The expected result would be to change the rate without typing '=' into the search bar.
                    // What would happen is that it would both change the rate and type '='. It's better if it type '=' but doesn't change the rate.
                    // For every binding in a screen that may enter text I would have to check if text editing was active or not. (SDL_IsTextInputActive() or make platform.text_input_is_active())
                    //  -- if (key_binding_pressed(gin, thekey) && !platform.text_input_is_active())
                    //  that's not very good. what if i want to search for
                    //  something and press F2 with search still active? that
                    //  wouldn't work.
                    //  One thing I can do is make it very explicit when the search bar is on.

                    // Each event can only have something like 32 bytes of text. If the IME is editing more text than that then SDL will give me two or more events.
                    auto edit_str = cstr_to_string(event.edit.text);
                    printf("Got text editing: '%s' start %d count %d len bytes %d\n",
                           event.edit.text, event.edit.start, event.edit.length, (int)edit_str.len);

                    // SDL_TEXTEDITING: UTF-8 text compose events come in blocks of SDL_TEXTEDITINGEVENT_TEXT_SIZE bytes each (32 in my machine). Each TEXTEDITING event has one block. Each block starts at character `event.edit.start` (in unicode characters, not in bytes!) and is `event.edit.length` characters long.
                    // Every time I input some new text I seem to get all the blocks from zero, not just the newest block. TODO: Verify this is not just for my machine with my IME settings.

                    if (event.edit.start == 0) {
                        // NOTE: Each time event.edit.start == 0 we consider the text composition to be reset. If it turns out that we don't always get all the blocks again in every machine, I'd have to change this code.
                        gin.text_composition.len = 0;
                    }
                    // text_composition persists between frames.
                    gin.text_composition.append(edit_str);
                } break;
                default: break;
                }
            }

            // Update mouse
            SDL_GetMouseState(&gin.mouse.pos.x, &gin.mouse.pos.y);
            gin.mouse.delta_pos = gin.mouse.pos - gin.last_mouse.pos;
            // s32 overflow is ub
            gin.mouse.delta_wheel = 1*(s32)((u32)gin.mouse.wheel_dont_use_me - (u32)gin.last_mouse.wheel_dont_use_me);

            if (length_sq_v2s(gin.mouse.delta_pos) > 0) {
                // printf("mouse move: %d %d\n", gin.mouse.delta_pos.x, gin.mouse.delta_pos.y);
            }
            if (gin.mouse.delta_wheel != 0) {
                // printf("mouse wheel: %d\n", gin.mouse.delta_wheel);
            }

            KeyboardMods themods = {};
            SDL_Keymod sdlmods = SDL_GetModState();
            if (sdlmods & KMOD_SHIFT)   themods |= KeyboardMods_Shift;
            if (sdlmods & KMOD_CTRL)    themods |= KeyboardMods_Ctrl;
            if (sdlmods & KMOD_ALT)     themods |= KeyboardMods_Alt;
            if (sdlmods & KMOD_GUI)     themods |= KeyboardMods_Win;

            for (s32 i = 0; i < ARRAY_COUNT(gin.mouse.buttons); i++) {
                // Copy the button input into raw_keys.
                // It needs to be copied before the update, because it will be updated below too.
                // Maybe I should instead make mouse.buttons[i].bi a pointer.
                gin.mouse.buttons[i].bi.mods_for_press_or_release = themods;
                gin.raw_keys[i + 1 + BEAT_KEY__MOUSE_START] = gin.mouse.buttons[i].bi;
                update_button_state(&gin.mouse.buttons[i].bi.state, gin.last_mouse.buttons[i].bi.state);
            }

            // Update raw_keys
            for (s32 i = 0; i < ARRAY_COUNT(gin.raw_keys); i++) {
                update_button_state(&gin.raw_keys[i].state, gin.last_raw_keys[i].state);
#if 0
                auto str_key = beat_key_to_string(i);
                auto str_mods = keyboard_mods_to_string(gin.raw_keys[i].mods_for_press_or_release);
                if (gin.raw_keys[i].state & ButtonState_PRESS) {
                    printf("Key press:   %.*s mods '%.*s'\n",
                           EXPAND_STR_FOR_PRINTF(str_key), EXPAND_STR_FOR_PRINTF(str_mods));
                } else if (gin.raw_keys[i].state & ButtonState_RELEASE) {
                    printf("Key release: %.*s mods '%.*s'\n",
                           EXPAND_STR_FOR_PRINTF(str_key), EXPAND_STR_FOR_PRINTF(str_mods));
                } else if (gin.raw_keys[i].state & ButtonState_REPEAT) {
                    if (1||FRAME_COUNT_ONCE_IN_A_WHILE) {
                        printf("Key repeat:  %.*s mods '%.*s'\n",
                               EXPAND_STR_FOR_PRINTF(str_key), EXPAND_STR_FOR_PRINTF(str_mods));
                    }
                }
#endif
            }
        }
        s32 window_width, window_height;
        SDL_GetWindowSize(window, &window_width, &window_height);

        gin.clock_frame_start = perf_counter_start;
        gin.os_time_seconds_frame_start = os_time_seconds_frame_start;

        // Update and Render
        game_uar(&game_memory, &gin, window_width, window_height);

        u64 perf_counter_before_swap_buffers = SDL_GetPerformanceCounter();
        SDL_GL_SwapWindow(window);
        global_frame_count++;

        u64 delta_perf_counter_a = perf_counter_before_swap_buffers - perf_counter_start;
        u64 perf_counter_before_sleep = SDL_GetPerformanceCounter();
        u64 delta_perf_counter_b = perf_counter_before_sleep - perf_counter_before_swap_buffers;

        u64 perf_counter_end = perf_counter_before_sleep;//SDL_GetPerformanceCounter();
        u64 total_frame_perf_counter = perf_counter_end - perf_counter_start;
        u64 delta_perf_counter_c = perf_counter_end - perf_counter_before_sleep;
        dt = (f64)total_frame_perf_counter*1/(f64)perf_freq;

        // If I limit FPS to 200 or lower, on the main menu dt_b is 0.01ms and dt_c is big. On the wheel it is the opposite.
        // Why is dt_b almost zero?

        // testing limited fps
        bool do_frame_limiter = (frame_limiter_mode == FrameLimiterMode_Limited) || !window_has_focus;
        if (do_frame_limiter) {
            // f64 test_target_dt = 1.0/288;
            f64 target_fps = 500;
            f64 test_target_dt = 1.0/target_fps;
            if (!window_has_focus) {
                test_target_dt = 1.0/12;
            }
            complete_time_interval(window_has_focus,  test_target_dt,
                                   perf_counter_start, perf_counter_before_sleep,
                                   &perf_counter_end, &total_frame_perf_counter, &delta_perf_counter_c, &dt);
        }
        perf_counter_start = perf_counter_end;

#if BEAT_INTERNAL && 0
        if (dt > 2.0) {
            // Using the debugger, so ignore all that time.
            dt = 0.016666;
        }
#endif

        auto dt_frame = dt;
#if BEAT_TIME_MULTIPLIER
        dt *= (f64)game_memory.debug_time_multiplier;
#endif
        // os_time_seconds_frame_start = (f64)(perf_counter_start - game_start_perf_counter) *1/ (f64)perf_freq;
        os_time_seconds_frame_start += dt;
        f64 dt_a = (f64)delta_perf_counter_a/(f64)perf_freq;
        f64 dt_b = (f64)delta_perf_counter_b/(f64)perf_freq;
        f64 dt_c = (f64)delta_perf_counter_c/(f64)perf_freq;
        gin.debug_dt_a = dt_a;
        gin.debug_dt_b = dt_b;
        gin.debug_dt_c = dt_c;
        gin.debug_dt_frame = dt_frame;
        gin.dt = dt;
        game_report_shit(&game_memory, dt_a, dt_b, dt);
    }


    // xfree(game_memory.permanent.base);
    // xfree(game_memory.temporary.base);
}

int main(int argc, char **argv) {
    String arg = {};
    if (argc > 1) {
        arg = cstr_to_string(argv[1]);
    }
    test_sim_sdl(arg, false);
    return 0;
}
