#ifndef BEAT_INCLUDE_H
#define BEAT_INCLUDE_H

// Include everything
#include "beat_platform.hh"
#include "beat_math.hh"
#include "beat_memory.hh"

#include "beat_test_new_array.hh"

#include "beat_new_song.hh"
#include "beat_new_song_gameplay.hh"
#include "beat_parser_sm.hh"
#include "beat_parser_ojn.hh"
#include "beat_parser_osu.hh"
#include "beat_parser_bms.hh"
// #include "beat_very_simple_config_file.hh"
#include "beat_simple_ini.hh"
#include "beat_simple_font.hh"
#include "beat_song_asset.hh"
#include "beat_audio.hh"
#include "beat_render_front.hh"
#include "beat.hh"

#endif
