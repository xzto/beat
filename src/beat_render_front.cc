#include "beat_include.hh"

static const u32 white_tex_side_in_px = 4;
static const u32 white_tex_pos_px = 1;

const struct {
    v2 top_left;
    v2 bot_left;
    v2 bot_right;
    v2 top_right;
} white_tex_coords = {
    .top_left  = ((f32)white_tex_pos_px/(f32)white_tex_side_in_px)*(V2(1,1) + V2(0, 1)),
    .bot_left  = ((f32)white_tex_pos_px/(f32)white_tex_side_in_px)*(V2(1,1) + V2(0, 0)),
    .bot_right = ((f32)white_tex_pos_px/(f32)white_tex_side_in_px)*(V2(1,1) + V2(1, 0)),
    .top_right = ((f32)white_tex_pos_px/(f32)white_tex_side_in_px)*(V2(1,1) + V2(1, 1)),
};

static void
begin_render_entries(GameMemory *memory, Rendering *rendering)
{
    rendering->temp_mem = begin_temp_memory(&memory->temporary);
    auto *entries = &rendering->entries;
    *entries = {};
    // Try to be smart about how much memory we will use.
    smem count_to_reserve = 128;
    if (rendering->last_frames_entry_count > count_to_reserve)
        count_to_reserve = to_next_power_of_two(rendering->last_frames_entry_count);
    // printf("rendering: reserving %ld\n", count_to_reserve);
    entries->arena = rendering->temp_mem.arena;
    entries->reserve(count_to_reserve);
}

static void
end_render_entries(Rendering *rendering)
{
    rendering->last_frames_entry_count = rendering->entries.len;
    end_temp_memory(&rendering->temp_mem);
}

static void
push_render_entry(Rendering *rendering, RenderEntry *e)
{
    auto *entries = &rendering->entries;
    ASSERT(rendering->temp_mem.arena);
    arena_temp_check(rendering->temp_mem.arena, rendering->temp_mem.temp_count); // no need for this check anymore since I implemented it in NewArrayDynamic and BucketArray directly.
    ASSERT(entries->arena);
    entries->push(*e);
}

static inline void
renderer_push_diamond(Rendering *rendering, v4 color, v2 center, f32 half_side_in_pixels)
{
    auto hs = half_side_in_pixels;
    center -= hs*V2(1, 1);
    v2 bot_left  = V2(center.x      , center.y);
    v2 bot_right = V2(center.x+2*hs , center.y);
    v2 top_right = V2(center.x+2*hs , center.y+hs);
    v2 top_left  = V2(center.x      , center.y+hs);
    center += hs*V2(1, 1);

    if (DIAMOND_FOR_NOTES) {
        top_right.y += hs;
        top_left.y += hs;

        bot_left.y  += hs;
        bot_right.x -= hs;
        top_right.y -= hs;
        top_left.x  += hs;
    }

    auto const &wtc = white_tex_coords;
    RenderEntry re = {
        .type = RenderEntryType_SimpleQuad,
        .simple_quad = {
            .pos = { bot_left, bot_right, top_right, top_left },
            .uv = { wtc.bot_left, wtc.bot_right, wtc.top_right, wtc.top_left },
            .color = color,
            .tex = GAME_MEMORY->opengl.white_tex,
        },
    };
    push_render_entry(rendering, &re);
}

static inline void
renderer_push_rectangle(Rendering *rendering, v4 color, v2 min, v2 size)
{
    auto const pos_aabb = AabbPos{ .min = min, .max = min + size };
    auto const &wtc = white_tex_coords;
    RenderEntry re = {
        .type = RenderEntryType_SimpleQuad,
        .simple_quad = {
            .pos = quad_from_aabb(pos_aabb),
            .uv = { wtc.bot_left, wtc.bot_right, wtc.top_right, wtc.top_left },
            .color = color,
            .tex = GAME_MEMORY->opengl.white_tex,
        },
    };
    push_render_entry(rendering, &re);
}

static inline void
renderer_push_point(Rendering *rendering, v4 color, v2 center, f32 side_in_pixels)
{
    auto const min = center - 0.5f*V2(side_in_pixels, side_in_pixels);
    auto const pos_aabb = AabbPos{ .min = min, .max = min + V2(side_in_pixels, side_in_pixels) };
    auto const &wtc = white_tex_coords;
    RenderEntry re = {
        .type = RenderEntryType_SimpleQuad,
        .simple_quad = {
            .pos = quad_from_aabb(pos_aabb),
            .uv = { wtc.bot_left, wtc.bot_right, wtc.top_right, wtc.top_left },
            .color = color,
            .tex = GAME_MEMORY->opengl.white_tex,
        },
    };
    push_render_entry(rendering, &re);
}

static inline void
renderer_push_sdf(Rendering *rendering, v2 min, v2 size, v4 color, u32 tex)
{
    v2 const uv_min = V2(0, 0);
    v2 const uv_max = V2(1, 1);
    auto const uv_aabb = AabbPos{ .min = uv_min, .max = uv_max };

    auto const pos_aabb = AabbPos{ .min = min, .max = min + size };
    auto const &wtc = white_tex_coords;
    RenderEntry re = {
        .type = RenderEntryType_SdfQuad,
        .sdf_quad = {
            .pos = quad_from_aabb(pos_aabb),
            .uv = quad_from_aabb(uv_aabb),
            .color = color,
            .tex = tex,
            .do_drop_shadow = true,
        },
    };
    push_render_entry(rendering, &re);
}

static inline void
renderer_push_sdf_atlas(Rendering *rendering, v2 min, v2 size, v4 color, u32 atlas_tex,
                        v2s total_atlas_size, v2s atlas_element_position, v2s atlas_element_size)
{
    v2 const uv_min = V2(
        (f32)atlas_element_position.x / (f32)total_atlas_size.x,
        (f32)atlas_element_position.y / (f32)total_atlas_size.y
    );
    v2 const uv_max = V2(
        (f32)(atlas_element_position.x + atlas_element_size.x) / (f32)total_atlas_size.x,
        (f32)(atlas_element_position.y + atlas_element_size.y) / (f32)total_atlas_size.y
    );

    auto const uv_aabb = AabbPos{ .min = uv_min, .max = uv_max };
    auto const pos_aabb = AabbPos{ .min = min, .max = min + size };
    auto const &wtc = white_tex_coords;
    RenderEntry re = {
        .type = RenderEntryType_SdfQuad,
        .sdf_quad = {
            .pos = quad_from_aabb(pos_aabb),
            .uv = quad_from_aabb(uv_aabb),
            .color = color,
            .tex = atlas_tex,
            .do_drop_shadow = true,
        },
    };
    push_render_entry(rendering, &re);
}

// draw("Hello")                -> |Hello   |
// draw_right_aligned("Hello")  -> |   Hello|

// TODO: renderer_push_string_aligned_right
// TODO: get_font_string_length (on beat_simple_font.c)

// returns the next position.
static inline f32
renderer_push_string__internal(GameMemory *memory, Rendering *rendering, f32 font_height_px, v2 pos, v4 color, String str,
                               bool draw_it, bool was_from_unknown_glyphs = false)
{
    // TODO: I have to do something better than this.
    auto *font = memory->debug_font0;
    auto *gs = get_game_state(memory);
    bool draw_rect_over_glyph           = gs->debug_show_text_rect & 1; //!true;
    bool draw_rect_over_everything      = gs->debug_show_text_rect & 2; //!true;
    bool draw_rect_on_pos               = gs->debug_show_text_rect & 4; //!true;

    static u64 last_frame_did_print_got_bad_stuff = 0;
    bool did_print_got_bad_stuff = false || (last_frame_did_print_got_bad_stuff == global_frame_count);

    f32 original_height_px = font->pixel_height_stbtt;
    f32 x = pos.x;
    f32 hi = pos.y;
    FontGlyph *last_glyph = NULL;
    for (s32 i = 0; i < str.len;) {
        u32 codepoint = 0;
        s32 codepoint_size = utf8_decode(str, i, &codepoint);
        if (codepoint_size == 0) { // TODO: Maybe draw the hex character?
            i++;
            continue;
        }
        if (codepoint) { // TODO I want to show "\u0000" if there is a null byte.
            FontChunk *chunk;
            FontGlyph *glyph = codepoint_to_glyph(font, codepoint, &chunk);
            x += font_height_px * get_advance(last_glyph, glyph);
            if (glyph) {
#if 0
                if (glyph->bitmap) {
                    v2 size = (font_height_px / original_height_px) * V2((f32)glyph->bitmap_width, (f32)glyph->bitmap_height);
                    v2 bottom_left = V2(x, hi);
                    bottom_left.x +=  font_height_px*glyph->offset.x;
                    bottom_left.y += -font_height_px*glyph->offset.y - size.y;
                    u32 tex = glyph->opengl_tex;
                    ASSERT(tex); // TODO: Not this
                    if (draw_it)
                        renderer_push_sdf(rendering, bottom_left, size, color, tex);

                    if (draw_it && draw_rect_over_glyph) {
                        renderer_push_rectangle(rendering, V4(1,0,0, 0.5f), bottom_left, size);
                    }
                }
#else
                if (glyph->has_bitmap) {
                    v2 size = (font_height_px / original_height_px) * V2((f32)glyph->atlas_element_size.x, (f32)glyph->atlas_element_size.y);
                    v2 bottom_left = V2(x, hi);
                    bottom_left.x +=  font_height_px*glyph->offset.x;
                    bottom_left.y += -font_height_px*glyph->offset.y - size.y;
                    u32 atlas_tex = chunk->opengl_atlas_tex;
                    ASSERT(atlas_tex); // TODO: Not this
                    if (draw_it)
                        renderer_push_sdf_atlas(rendering, bottom_left, size, color, atlas_tex, chunk->atlas_bitmap_size,
                                                glyph->atlas_element_position, glyph->atlas_element_size);

                    if (draw_it && draw_rect_over_glyph) {
                        renderer_push_rectangle(rendering, V4(1,0,0, 0.5f), bottom_left, size);
                    }
                }
#endif
                if (draw_it && draw_rect_on_pos) {
                    renderer_push_rectangle(rendering, V4(0,1,1, 1.0f), V2(x, hi), V2(5,1));
                    renderer_push_rectangle(rendering, V4(0,1,1, 1.0f), V2(x, hi), V2(1,5));
                }
            } else {
                if (was_from_unknown_glyphs) {
                    // Would happen if I didn't have ascii characters in the font.
                    // TODO: Draw a rectangle in a weird color or something.
                    did_print_got_bad_stuff = did_print_got_bad_stuff ||
                        (last_frame_did_print_got_bad_stuff == global_frame_count) ||
                        ((last_frame_did_print_got_bad_stuff == (global_frame_count-1)) && last_frame_did_print_got_bad_stuff != 0);
                    last_frame_did_print_got_bad_stuff = global_frame_count;
                    if (!did_print_got_bad_stuff) {
                        did_print_got_bad_stuff = true;
                        beat_log("=== renderer_push_string__internal: got `was_from_unknown_glyphs` and I don't have the basic glyph!!!\n");
                    }
                } else {
                    char buf[16];
                    String newstr;
                    newstr.data = (u8 *)buf;
                    newstr.len = snprintf((char*)newstr.data, sizeof(buf), "\\u%04x", codepoint);
                    v2 thepos = V2(x, hi);
                    f32 newposx = renderer_push_string__internal(memory, rendering, font_height_px*0.75f, thepos, color, newstr, draw_it, true);
                    if (draw_it)
                        renderer_push_rectangle(rendering, V4(1.0f, 0.0f, 0.0f, 1.0f), thepos, V2(newposx-thepos.x, 1));
                    x = newposx;
                }
            }
            last_glyph = glyph;
        }
        i += codepoint_size;
    }

    f32 newposition = x + font_height_px * get_advance(last_glyph, NULL);
    if (draw_it && draw_rect_over_everything)
        renderer_push_rectangle(rendering, V4(0,1,0, 0.3f),
                                V2(pos.x, pos.y+font_height_px*(font->descent)),
                                V2(newposition-pos.x, font_height_px*(font->ascent-font->descent)));
    return newposition;
}

static inline f32
renderer_push_string(GameMemory *memory, Rendering *rendering, f32 font_height_px, v2 pos, v4 color, String str)
{
    return renderer_push_string__internal(memory, rendering, font_height_px, pos, color, str, true);
}

static inline f32
renderer_push_string_centered(GameMemory *memory, Rendering *rendering, f32 font_height_px, v2 pos, v4 color, String str)
{
    // TODO It would be faster to modify the render entries' position instead of calculating it twice.
    f32 newposx = renderer_push_string__internal(memory, rendering, font_height_px, pos, color, str, false);
    f32 pos_offset = -(newposx-pos.x)/2;
    pos.x += pos_offset;
    return renderer_push_string__internal(memory, rendering, font_height_px, pos, color, str, true);
}


#define renderer_push_strings_v(memory, rendering, font_height_px, min, color, ...) \
    renderer_push_strings_v_(memory, rendering, font_height_px, min, color, COUNT_ARGS_COMMA_ARGS(((String []){__VA_ARGS__})))

static void
renderer_push_strings_v_(GameMemory *memory, Rendering *rendering, f32 font_height_px, v2 pos, v4 color, s32 count, String *strings)
{
    for (s32 i = 0; i < count; i++) {
        pos.x = renderer_push_string(memory, rendering, font_height_px, pos, color, strings[i]);
    }
}

