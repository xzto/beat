#ifndef NOT_SO_SIMPLE_CONFIG_FILE_H
#define NOT_SO_SIMPLE_CONFIG_FILE_H

struct SimpleIni_Item {
    String key;
#if 1
    String value;
#else
    // TODO: Implement arrays.
    // Option A:
    // `songs_dir = /mnt/c/osu/Songs` this would reset the key to only this value
    // `songs_dir += /mnt/c/etterna-files/Songs` this would add another value to the key
    struct SimpleIni_ItemValue {
        String value;
        SimpleIni_ItemValue *next;
    };
    SimpleIni_ItemValue values;
    // I think this is the best way to implement it because I don't have to check anywhere that something is an array. If I don't want that item to be an array I can just use the first value and ignore the rest.

    // Option B:
    // `songs_dir_0 = /mnt/c/osu/Songs`
    // `songs_dir_1 = /mnt/c/etterna-files/Songs`
    // NewArrayDynamic<String> arr_values = {};
    // arr_values.arena = some_arena;
    // simple_ini_get_array(&simple_ini, "songs_dir"_s, temp_arena, &arr_values)
    // I don't like this.
#endif
};

struct SimpleIni_Section {
    String name;
    NewArrayDynamic<SimpleIni_Item> items;
};

struct SimpleIni {
    MemoryArena *arena;
    NewArrayDynamic<SimpleIni_Section> sections;
    EntireFile ef;

    // String filename;
    // int line1;
};


#endif
