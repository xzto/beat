#ifndef BEAT_NEW_SONG_GAMEPLAY_H
#define BEAT_NEW_SONG_GAMEPLAY_H

// TODO: Make a flag VN_WAS_SEEN_THIS_FRAME and VN_HOLD_END_WAS_SEEN_THIS_FRAME and VN_SMROLL_END_xxx
// At the start of the frame we clear these flags.
// At the end of the frame we clear the visible note depending on whether this flag was set or not.

// TODO: How to do SVs: Instead of drawing the notes based on `vn.song_ts - current_song_ts`, do the following:
// Consider a vertical tape with notes with a fixed position on that tape (like a "piano roll"). That's our playfield. It is scaled vertically by one factor: `PlayingVisualConfig::scroll`. SV changes do make it scroll faster/slower however it doesn't scale verticaly in any way (since it already changed the position of the notes). When SV changes, all I'd do is scroll faster or slower. The `Notes` struct would have SV info. When setting up ScenePlaying, also create this Tape structure alongside with `ScenePlaying.other.snappings`.
// When drawing / scrolling, use `pixels_from_bottom = (note_tape_pos - current_tape_position) * base_scroll_speed` instead of `current_song_ts` (If SV = 1.5, current_tape_position would (roughly) increment by `dt*sv*rate` every frame). If SVs are disabled, use the previous method of `song_ts - current_song_ts`.




// Maybe we don't need idx_row here?
struct VisibleNote {
    // @NoteType
    NoteType type; // tap, hold, roll, mine
    u16 flags; // VN_GENERIC_xxx and VN_(TYPE)_xxx

    // no need to store it here if we have an array for each column. maybe it's useful?
    // s32 column;
    s32 idx_row;
    NoteTimestamp song_ts;
    // TODO: `tape_ts` for SVs.

    // Maybe also store a pointer to the beat_info?
    // Might not be necessary
    // BeatInfo *bi;

    // Maybe this is unnecessary
    NoteTimestamp latest_input_song_ts;
    // The number of inputs used. If no input was used, then
    // latest_input_song_ts is invalid
    s32 inputs_used;

    // Make sure there's a struct for every type of note
    // @NoteType
    union {
        struct {
            s32 __placeholder;
        } tap;
        struct {
            s32 __placeholder;
        } mine;
        struct {
            s32 idx_row_end;
            NoteTimestamp song_ts_end;
            s32 times_released;
        } hold;
        struct {
            // latest_input_timestamp was a press
            s32 idx_row_end;
            NoteTimestamp song_ts_end;
            s32 times_tapped;
        } smroll;
    };
};

// Visible Note Generic Flags
// Keep in mind I'm currently using a u16 to store these flags in the vn struct
// If I add too many flags here or at the VN_(TYPE) I'll have to change from u16 to u32
enum {
    VN_FINISHED_INPUT       = 1 << 0,
    // VN_WAS_SEEN_THIS_FRAME  = 1 << 1,

    // When adding more flags here, make sure to modify VN_GENERIC_BIT_NUM if
    // needed
#define VN_GENERIC_BIT_NUM 4
#define VN_SPECIFIC_BIT(x) (VN_GENERIC_BIT_NUM + x)
    VN_GENERIC_MASK         = (1 << VN_SPECIFIC_BIT(0)) - 1,
};

// @NoteType
// Tap flags
enum {
    // IS_DEAD means we didn't hit the tap and can't do it anymore
    // When it's dead or hit, VN_FINISHED_INPUT should be set.
    VN_TAP_IS_DEAD          = 1 << VN_SPECIFIC_BIT(0),
    VN_TAP_HIT              = 1 << VN_SPECIFIC_BIT(1),
    VN_TAP_DO_NOT_DRAW      = 1 << VN_SPECIFIC_BIT(2),
};

// Mine flags
enum {
    // IS_DEAD means we didn't hit the mine and can't do it anymore
    // When it's dead or exploded, VN_FINISHED_INPUT should be set.
    VN_MINE_IS_DEAD         = 1 << VN_SPECIFIC_BIT(0),
    VN_MINE_EXPLODED        = 1 << VN_SPECIFIC_BIT(1),
};

// Hold flags
enum {
    // IS_DEAD means we either dindn't hit the roll or we broke it and can't
    // regrab it
    // To test if not dead, use !(flags & HOLD_IS_DEAD)
    // When it's dead, VN_FINISHED_INPUT should be set.
    VN_HOLD_IS_DEAD         = 1 << VN_SPECIFIC_BIT(0), // Can't regrab

    VN_HOLD_IS_RELEASED     = 1 << VN_SPECIFIC_BIT(1),
    VN_HOLD_IS_PRESSED      = 1 << VN_SPECIFIC_BIT(2),

    // For rendering
    VN_HOLD_VISIBLE_END     = 1 << VN_SPECIFIC_BIT(3), // The end is visible
};

// Roll flags
enum {
    // IS_DEAD means we either dindn't hit the roll or we broke it and can't
    // regrab it
    // To test if not dead, use (flags & ~ROLL_IS_DEAD)
    // When it's dead, VN_FINISHED_INPUT should be set.
    VN_SMROLL_IS_DEAD         = 1 << VN_SPECIFIC_BIT(0), // Can't regrab.

    // For rendering
    VN_SMROLL_VISIBLE_END     = 1 << VN_SPECIFIC_BIT(1), // The end is visible
};

// These shouldn't be constant. They might need to change dependind on the judge used
// For example, an extreme judge that has a miss window of two seconds wouldn't work with these
#define TIME_TO_LOOK_BEHIND 1.0
#define TIME_TO_LOOK_AHEAD 1.0

struct TimedInput {
    // f64 os_time; // not being used right now
    NoteTimestamp song_ts; // in song time (doesn't change with rate)
    // s32 column; // no need to store it here if we have an array for each column
    bool pressed;
    s32 times_used; // probably always either 0 or 1. I have to check.
};

// @NoteType
enum InputActionStuffType : u32 {
    INPUT_ACTION_NOTHING = 0,

    // NOTE_TAP
    INPUT_ACTION_TAP_DIE,  // When you don't press the tap
    INPUT_ACTION_TAP_TAPS, // When you press for a tap

    // NOTE_MINE
    INPUT_ACTION_MINE_DIE, // When you don't press the mine
    INPUT_ACTION_MINE_EXPLODE, // When you hit the mine

    // NOTE_HOLD
    // TODO: Duplicate these for LNs
    INPUT_ACTION_HOLD_DIE,              // When you don't press the hold (ias.first_grab)
    INPUT_ACTION_HOLD_GRAB,             // When you first press down the key (ias.first_grab)
    INPUT_ACTION_HOLD_GRAB_OTHER,       // When you first press down the key if it broke before (ias.other_grab)
    INPUT_ACTION_HOLD_GRAB_CONTINUE,    // When you are holding down the key
    INPUT_ACTION_HOLD_BREAK_CAN_REGRAB, // If you release it in the middle (can regrab it)
    INPUT_ACTION_HOLD_BREAK_END_WASREL,     // When the hold ends before you pressed it again (you get a BREAK_CAN_REGRAB before this)
    INPUT_ACTION_HOLD_BREAK_END_WASPRESS,   // When you don't release at the end.
    INPUT_ACTION_HOLD_BREAK_CONTINUE,   // When you keep the key up after releasing
    INPUT_ACTION_HOLD_FINISH,           // When you release properly at the end

    // Configurable. SM allows it, osu does not.
    // What do I do with these?
    // INPUT_ACTION_HOLD_BREAK_CAN_REGRAB, // When you release it briefly
    // INPUT_ACTION_HOLD_REGRAB, // When you press down the key after releasing
    // INPUT_ACTION_HOLD_REGRAB_CONTINUE,

    // NOTE_SMROLL
    INPUT_ACTION_SMROLL_DIE,            // Might be configurable
    INPUT_ACTION_SMROLL_TAP_FIRST,      // When you first tap the roll
    INPUT_ACTION_SMROLL_TAP_CONTINUE,   // When you keep tapping the roll
    INPUT_ACTION_SMROLL_BREAK,          // When you break the roll by not tapping
    INPUT_ACTION_SMROLL_FINISH,         // When you finish the roll
};


// @NoteType
// One union for each possible INPUT_ACTION_XXX
struct InputActionStuff {
    // real_ts_diff is in real time, not in song time (changes with rate)
    // Maybe I should store the actual song timestamp from the input too?
    // NoteTimestamp input_song_ts_; // TODO: Use this later
    InputActionStuffType type;
    union {
        struct {
            f64 offset_seconds;
        } tap;
        struct {
            s32 __placeholder;
            // maybe put things like evade or hit here
        } mine;
        struct {
            struct {
                f64 offset_seconds;
            } first_grab;
            struct {
                // TODO: What should I put here?
                f64 offset_seconds_from_start; // time since hold start.
                f64 offset_seconds_from_release; // time since you released the break.
            } other_grab;
            struct {
                f64 offset_seconds;
            } finish;
            struct {
                f64 offset_seconds;
                bool too_late_for_end;
                bool was_released_before_it_broke;
            } release_break_;

            struct {
            } break_can_regrab;
            struct {
            } break_end_was_released;
            struct {
            } break_end_was_pressed;
        } hold;
        struct {
            // should have more stuff here.
            f64 offset_seconds;
        } smroll;
    };
};




#endif
