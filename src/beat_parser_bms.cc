
// "*---------------------- HEADER FIELD"
// "#KEY val"
// "*---------------------- MAIN DATA FIELD"
// "#xxxyy:zzzzzzzzz"
// xxx = measure (base ???)
// yy = channel (base ???)
//
//
//
// 1x or 2x, 1 is player 1, 2 is player 2
//
// 5k+1 (can be double)
// K1 K2 K3 K4 K5 SCRATCH
// 11 12 13 14 15 16
//
// for 7k+1 (can be double)
// K1 K2 K3 K4 K5 K6 K7 SCRATCH
// 11 12 13 14 15 18 19 16
//
// pms (9k) ("pms.pms") (no double)
// K1 K2 K3 K4 K5 K6 K7 K8 K9
// 11 12 13 14 15 16 17 18 19
//
// pms (bme type) ("pms.bme") (can be double)
// K1 K2 K3 K4 K5 K6 K7 K8 K9
// 11 12 13 14 15 18 19 16 17
//
// how to recognize if it is 9k or 7k+1 or 5k+1 or 14k+2, etc when it's .bme or .bms?
// see what channels it used and guess
// if only 11 to 16, it is 5k+1
// if only 11 to 16 and 18-19, it is 7k+1
// if ext is .pms, treat it as "pms.pms"
// if only 11 to 19 and 17, it is 9k pms, if extension is .bme, treat it as "pms.bme", if ext is .pms treat it as "pms.pms"
// if it used both 1x and 2x it is double
// if it only used either 1x or 2x, it is single

static bool
parser_bms_get_line(ParserBms_Ctx *ctx, String *ret_str)
{
    while (true) {
        if (ctx->buf_ptr >= ctx->buf_end)
            return false;

        ctx->line1++;
        u8 *ptr = ctx->buf_ptr;
        u8 *prev_ptr = ctx->buf_ptr;
        while (ptr < ctx->buf_end) {
            if (*ptr == '\n') {
                break;
            }
            ptr++;
        }
        s32 len = (s32)(ptr-prev_ptr);
        if (ptr < ctx->buf_end) {
            ASSERT(*ptr == '\n');
            if ((ptr-1)>=prev_ptr && (*(ptr-1) == '\r')) len--;
            ptr++;
        }
        ctx->buf_ptr = ptr;

        String str = {.len = len, .data = prev_ptr};
        trim_whitespace(&str);
        if (str.len <= 0)
            continue;
        *ret_str = str;
        return true;
    }
    return false;
}

static bool
parser_bms_parse_int_base16_len2(String str, s32 *ret_num)
{
    if (str.len < 2)
        return false;

    s32 total = 0;
    s32 mult = 1;
    for (s32 i = 1; i >= 0; i--) {
        u8 ch = str.data[i];
        s32 num;
        if (BETWEEN(ch, '0', '9')) num = ch - '0';
        else if (BETWEEN(ch|32, 'a', 'f')) num = 10 + (ch|32) - 'a';
        else return false;
        total += num * mult;
        mult *= 16;
    }
    ASSERT(BETWEEN(total, 0, 0xff));
    *ret_num = total;
    return true;
}

// str start with the channel
static bool
parser_bms_parse_int_base36_len2(String str, s32 *ret_num)
{
    if (str.len < 2)
        return false;

    s32 total = 0;
    s32 mult = 1;
    for (s32 i = 1; i >= 0; i--) {
        u8 ch = str.data[i];
        s32 num;
        if (BETWEEN(ch, '0', '9')) num = ch - '0';
        else if (BETWEEN(ch|32, 'a', 'z')) num = 10 + (ch|32) - 'a';
        else return false;
        // it won't overflow with just 2 digits and base 36
        total += num * mult;
        mult *= 36;
    }
    ASSERT(BETWEEN(total, 0, 36*36-1));
    *ret_num = total;
    return true;
}



static bool
parser_bms_parse_and_maybe_set_s32(String rhs, u8 base, ParserBms_Ctx *ctx, Maybe<s32> *maybe, s64 min, s64 max, String name)
{
    s64 i;
    bool valid = parse_s64(rhs, &i, base);
    if (!valid) {
        parser_bms_report_error_v(ctx, "failed to parse s64 for '%.*s'\n", EXPAND_STR_FOR_PRINTF(name));
        return false;
    }
    valid = BETWEEN(i, min, max);
    if (!valid) {
        parser_bms_report_error_v(ctx, "s64 is out of bounds for '%.*s' (min %lld max %lld)\n",
                                  EXPAND_STR_FOR_PRINTF(name), (long long)min, (long long)max);
        return false;
    }
    *maybe = Some((s32)i);
    return true;
}

static bool
parser_bms_parse_and_maybe_set_f32(String rhs, ParserBms_Ctx *ctx, Maybe<f32> *maybe, f64 min, f64 max, String name)
{
    f64 f;
    bool valid = parse_f64(rhs, &f);
    if (!valid) {
        parser_bms_report_error_v(ctx, "failed to parse f64 for '%.*s'\n", EXPAND_STR_FOR_PRINTF(name));
        return false;
    }
    valid = BETWEEN(f, min, max);
    if (!valid) {
        parser_bms_report_error_v(ctx, "f64 is out of bounds for '%.*s' (min %f max %f)\n",
                                  EXPAND_STR_FOR_PRINTF(name), min, max);
        return false;
    }
    *maybe = Some((f32)f);
    return true;
}

static Maybe<String>
parser_bms_sjis_to_utf8(MemoryArena *arena, String str_sjis)
{
#if 0
    NOT_IMPLEMENTED;
    return None;
#else
    // TODO: Properly convert it.
    return Some(arena_push_string(arena, str_sjis));
#endif
}

static int
parser_bms_cmp_channel_data(const void *a_, const void *b_)
{
    auto *a = (ParserBms_ChannelData *)a_;
    auto *b = (ParserBms_ChannelData *)b_;
    int result = 0;
    if (result == 0) result = (s32)a->measure - b->measure;
    if (result == 0) result = (s32)a->channel - b->channel;
    return result;
}

static void
parser_bms_sort_channels_data(Slice<ParserBms_ChannelData> arr)
{
    qsort(arr.data, (size_t)arr.len, sizeof(arr.data[0]), parser_bms_cmp_channel_data);
}

static ParserBms_ChannelData *
parser_bms_find_duplicate(Slice<ParserBms_ChannelData> arr, s32 measure, s32 channel, smem start_at = 0)
{
    // arr should already be sorted
    ASSERT(ABC(start_at, arr.len));
    for (smem i = start_at+1; i < arr.len; i++) {
        auto *it = &arr.data[i];
        if (it->measure < measure) continue;
        if (it->measure == measure) {
            if (it->channel < channel) continue;
            if (it->channel == channel) {
                return it;
            }
            if (it->channel > channel) break;
        }
        if (it->measure > measure) break;
    }
    for (smem i = start_at-1; i >= 0; i--) {
        auto *it = &arr.data[i];
        if (it->measure > measure) continue;
        if (it->measure == measure) {
            if (it->channel > channel) continue;
            if (it->channel == channel) {
                return it;
            }
            if (it->channel < channel) break;
        }
        if (it->measure < measure) break;
    }
    return NULL;
}

static bool
parser_bms_make_bpms_temp(ParserBms_Ctx *ctx, NewArrayDynamic<ParserOjn_Bpm> *bpms_temp,
                          ParserBms_ChannelData *from3, ParserBms_ChannelData *from8,
                          ParserOjn_Bpm *last_actual_thebpm, s32 measure, f64 measure_start_seconds)
{
    ParserOjn_Bpm something_for_the_thing = {
        .bps = last_actual_thebpm->bps,
        .measure = measure,
        .measure_position = 0,
        .start_seconds = measure_start_seconds,
    };
    ParserOjn_Bpm *last_thebpm = NULL;

    auto cmp_bpms_temp = [](ParserOjn_Bpm *a, ParserOjn_Bpm *b, void *_) {
        return sign_of(((f64)a->measure + a->measure_position) - ((f64)b->measure + b->measure_position));
    };

    auto sort_bpms_temp = [&]() {
        slice_merge_sort(bpms_temp->as_slice(), ctx->temp_arena, *cmp_bpms_temp, 0);
    };

#if 0
    if (0 && from3 && from8) {
        // TODO: Handle this!
        static int thecounter = 0;
        thecounter++;
        parser_bms_report_error_v(ctx, "bpms from3 and from8 : %d\n", thecounter);
        goto rage_quit;
    }
#else
    // I don't know if this works. Haven't tested it. It didn't crash so maybe it works.
#endif

    {
        bool got_bpm_on_measure_start = false;
        // make measure_fracs and insert, then sort by measure_frac, removing duplicates, and then make seconds_from_start
        ParserBms_ChannelData *datafieldsss[] = { from3, from8 };
        for (s32 idx_datafieldsss = 0; idx_datafieldsss < ARRAY_COUNT(datafieldsss); idx_datafieldsss++) {
            auto *datafield = datafieldsss[idx_datafieldsss];
            if (!datafield) continue;
            for (s32 idx_item = 0; idx_item < datafield->items.len; idx_item++) {
                auto bpm_u16 = datafield->items.data[idx_item];
                if (idx_item == 0 && bpm_u16) {
                    got_bpm_on_measure_start = true;
                }
                if (bpm_u16 == 0){
                    continue;
                }
                f32 bps;
                if (datafield->channel == 3) {
                    bps = (f32)bpm_u16 / 60.0f;
                } else {
                    ASSERT(datafield->channel == 8);
                    if (!ctx->exbpms.ok[bpm_u16]) {
                        parser_bms_report_error_v(ctx, "EXBPM doesnt exist: (base 10)%d\n", bpm_u16);
                        goto rage_quit;
                    }
                    bps = ctx->exbpms.bpms[bpm_u16] / 60.0f;
                }
                f64 measure_position = (f64)idx_item / (f64)datafield->items.len;
                f64 measure_frac = (f64)datafield->measure + measure_position;

                ParserOjn_Bpm thebpm = {};
                thebpm.bps = bps;
                thebpm.measure = datafield->measure;
                thebpm.measure_position = measure_position;
                if (measure_frac == 0) {
                    *last_actual_thebpm = {
                        .bps = bps,
                    };
                    last_thebpm = last_actual_thebpm;
                } else {
                    bpms_temp->push(thebpm);
                }
            }
        }
        if (!last_thebpm && !got_bpm_on_measure_start) {
            last_thebpm = &something_for_the_thing;
        }
        // remove duplicates (small margin) and sort bpms_temp by measure_frac
        sort_bpms_temp();
        {
            // TODO: leave it sorted
            f64 last_measure_frac = -123;
            for (smem i = 0; i < bpms_temp->len; i++) {
                auto *bpm = &bpms_temp->data[i];
                f64 measure_frac = (f64)bpm->measure + bpm->measure_position;
                if (f64_equal_margin(measure_frac, last_measure_frac, 0.0001)) {
                    parser_bms_report_error_s(ctx, "found conflicting bpm channel 03 and 08. aborting\n");
                    goto rage_quit;
                    // bpms_temp->data[i--] = bpms_temp->data[--bpms_temp->len];
                    // continue;
                }
                last_measure_frac = measure_frac;
            }
        }
        sort_bpms_temp(); // TODO: Don't sort again!!
    }
    {
        f32 meter = 1;
        for (s32 idx_item = 0; idx_item < bpms_temp->len; idx_item++) {
            auto *bpm_temp = &bpms_temp->data[idx_item];
            f64 measure_frac = (f64)bpm_temp->measure + bpm_temp->measure_position;
            f32 bps = bpm_temp->bps;
            f64 seconds_from_measure_start = 0;
            if (last_thebpm) {
                f64 last_thebpm_measure_frac = (f64)last_thebpm->measure + last_thebpm->measure_position;
                f64 delta_measure_frac_from_bpm_start = measure_frac - last_thebpm_measure_frac;
                ASSERT(delta_measure_frac_from_bpm_start > 0);
                ASSERT(last_thebpm->measure == bpm_temp->measure);
                // TODO: meter
                f64 seconds_from_last_bpm_start = 4*(f64)meter*delta_measure_frac_from_bpm_start / (f64)last_thebpm->bps;
                f64 seconds_from_start = seconds_from_last_bpm_start + last_thebpm->start_seconds;
                seconds_from_measure_start = seconds_from_start - measure_start_seconds;
            }


            ParserOjn_Bpm *thebpm = &bpms_temp->data[idx_item];
            thebpm->start_seconds = measure_start_seconds + seconds_from_measure_start; // @IgnoreSomeBitsOfTheMantissa
            if (measure_frac == 0) {
                ASSERT(false);
                // last_thebpm = last_actual_thebpm;
                // *last_thebpm = thebpm;
            } else {
                last_thebpm = &bpms_temp->data[idx_item];
            }
        }
    }
    return true;
rage_quit:
    return false;
}

static bool
parser_bms_diff_from_file(MemoryArena *final_arena, MemoryArena *temp_arena,
                          String filename_not_nullter, bool report_errors, Diff *ret_diff)
{
    final_arena = 0; // not using it.
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    // Assert there's at least a megabyte free.
    ASSERT(temp_arena->size - temp_arena->used > MEGABYTES(1));

    String filename = arena_push_string_nullter(temp_arena, filename_not_nullter);

    ParserBms_Ctx *ctx = 0;

    bool valid;
    EntireFile ef;
    // One big file could be 120KiB.
    valid = read_entire_file_nullter_into_arena(filename, temp_arena, &ef);
    if (!valid) {
        goto rage_quit;
    }

    // the ctx is quite big becauase of the arrays of wav_files and bmp_files
    ctx = arena_push_struct(temp_arena, ParserBms_Ctx, arena_flags_zero());
    ctx->report_errors = report_errors && true;
    ctx->filename = filename;
    ctx->buf = ef.buf;
    ctx->buf_ptr = ctx->buf.data;
    ctx->buf_end = ctx->buf.data + ctx->buf.len;
    ctx->line1 = 0; // on first call to get_line it will set it to 1
    // ctx->final_arena = final_arena;
    ctx->temp_arena = temp_arena;
    ctx->is_buf_utf8 = false;

    ctx->channels_data.arena = ctx->temp_arena;
    ctx->channels_data.reserve(1024); // 1024 elems*sizeof(ParserBms_NoteChannelData (is 32)) == 32KiB

    // test for utf8 encoding magic
    {
        if (ctx->buf.len >= 3) {
            u8 utf8_order_mark[] = { 0xef, 0xbb, 0xbf };
            s32 i;
            for (i = 0; i < 3; i++) {
                if ((u8)ctx->buf.data[i] != utf8_order_mark[i])
                    break;
            }
            if (i == 3) {
                ctx->buf_ptr += 3;
                ctx->is_buf_utf8 = true;
            }
        }
    }

    // Parse the file.
    {
        while (true) {
            String str;
            // @BmsQuirk I don't know if I should trim whitespaces here or not... But I probably can.
            valid = parser_bms_get_line(ctx, &str);
            if (!valid) break;
            if (str.data[0] != '#') continue;
            advance(&str, 1);

            // printf("%.*s\n", EXPAND_STR_FOR_PRINTF(str));

            bool got_channel_data = false;
            if (str.len >= 6 && str.data[5] == ':') { // @BmsQuirk TODO: channel could be 1 byte ??? need to verify
                do {
                    bool the_data = true;
                    // measure - xxx - base 10
                    for (s32 i = 0; i < 3; i++) {
                        u8 ch = str.data[i];
                        if (!(BETWEEN(ch, '0', '9'))) {
                            the_data = false;
                            break;
                        }
                    }
                    if (!the_data) break;
                    for (s32 i = 0; i < 2; i++) {
                        u8 ch = str.data[3+i];
                        if (!(BETWEEN(ch, '0', '9') || BETWEEN(ch|32, 'a', 'z'))) {
                            the_data = false;
                            break;
                        }
                    }
                    if (!the_data) break;
                    got_channel_data = true;
                } while (false);
            }

            if (!got_channel_data) {
                // printf("HEADER %.*s\n", EXPAND_STR_FOR_PRINTF(str));
                String lhs, rhs;
                // @BmsQuirk space or tab, have to verify if there are files like that
                bool found_space = split_by_char_and_trim_whitespace(str, ' ', &lhs, &rhs);
                // printf("%d, '%.*s', %.*s\n", (s32)found_space, EXPAND_STR_FOR_PRINTF(lhs), EXPAND_STR_FOR_PRINTF(rhs));

                // @BmsQuirk fixed: uppercase/lowercase
                // convert to upper case
                for (s32 i = 0; i < lhs.len; i++) {
                    if (BETWEEN(lhs.data[i], 'a', 'z'))
                        lhs.data[i] &= ~32;
                }
                if (lhs == "RANDOM"_s
                    || lhs == "SWITCH"_s
                    || lhs == "IF"_s) {
                    static int thecounter = 0;
                    thecounter++;
                    parser_bms_report_error_v(ctx, "i don't support random and if: %d\n", thecounter);
                    goto rage_quit;
                }

                if (lhs == "TITLE"_s) {
                    if (rhs.len > 0) {
                        if (ctx->is_buf_utf8) ctx->title_utf8 = Some(rhs);
                        else ctx->title_utf8 = parser_bms_sjis_to_utf8(ctx->temp_arena, rhs);
                    }
                } else if (lhs == "ARTIST"_s) {
                    if (rhs.len > 0) {
                        if (ctx->is_buf_utf8) ctx->artist_utf8 = Some(rhs);
                        else ctx->artist_utf8 = parser_bms_sjis_to_utf8(ctx->temp_arena, rhs);
                    }
                } else if (lhs == "GENRE"_s) {
                    if (rhs.len > 0) {
                        if (ctx->is_buf_utf8) ctx->genre_utf8 = Some(rhs);
                        else ctx->genre_utf8 = parser_bms_sjis_to_utf8(ctx->temp_arena, rhs);
                    }
                } else if (lhs == "COMMENT"_s) {
                    if (rhs.len > 0) {
                        if (ctx->is_buf_utf8) ctx->comment_utf8 = Some(rhs);
                        else ctx->comment_utf8 = parser_bms_sjis_to_utf8(ctx->temp_arena, rhs);
                    }
                } else if (lhs == "PLAYER"_s) {
                    if (rhs.len > 0) {
                        valid = parser_bms_parse_and_maybe_set_s32(rhs, 10, ctx, &ctx->player, 1, 4, lhs);
                        if (!valid) goto rage_quit;
                    }
                } else if (lhs == "PLAYLEVEL"_s) {
                    // TODO: this isn't only a number. it could be a string
                    if (rhs.len > 0) {
#if 0
                        valid = parser_bms_parse_and_maybe_set_s32(rhs, 10, ctx, &ctx->playlevel, 1, 0x7fffffff, lhs);
                        if (!valid) goto rage_quit;
#else
                        if (ctx->is_buf_utf8) ctx->playlevel_str_utf8 = Some(rhs);
                        else ctx->playlevel_str_utf8 = parser_bms_sjis_to_utf8(ctx->temp_arena, rhs);
#endif
                    }
                } else if (lhs == "RANK"_s) {
                    if (rhs.len > 0) {
                        valid = parser_bms_parse_and_maybe_set_s32(rhs, 10, ctx, &ctx->rank, 0, 4, lhs);
                        if (!valid) goto rage_quit;
                    }
                } else if (lhs == "TOTAL"_s) {
                    if (rhs.len > 0) {
                        valid = parser_bms_parse_and_maybe_set_s32(rhs, 10, ctx, &ctx->total, 0, 0x7fff, lhs);
                        if (!valid) goto rage_quit;
                    }
#if 0
                } else if (lhs == "LNOBJ"_s) {
                    // TODO: LNOBJ
                    if (rhs.len > 0) {
                        // TODO: Parse base36 2 chars
                    }
#endif
                } else if (lhs == "DIFFICULTY"_s ||
                           lhs == "DIFFICULT"_s) {
                    if (rhs.len > 0) {
                        valid = parser_bms_parse_and_maybe_set_s32(rhs, 10, ctx, &ctx->difficulty, 0, 0x7fff, lhs);
                        if (!valid) goto rage_quit;
                    }
                } else if (string_starts_with(lhs, "BPM"_s)) {
                    if (lhs.len == 3) {
                        if (rhs.len > 0) {
                            valid = parser_bms_parse_and_maybe_set_f32(rhs, ctx, &ctx->header_bpm, 0.0001, 8.0e6, lhs);
                            if (!valid) goto rage_quit;
                        }
                    } else if (lhs.len == 5) {
                        if (rhs.len > 0) {
                            s32 bpm_id;
                            valid = parser_bms_parse_int_base36_len2(string_offset(lhs, 3), &bpm_id);
                            if (!valid) {
                                parser_bms_report_error_v(ctx, "failed to parse #BPMxx's xx: '%.*s'\n",
                                                          EXPAND_STR_FOR_PRINTF(lhs));
                                goto rage_quit;
                            }
                            f64 bpm;
                            valid = parse_f64(rhs, &bpm);
                            valid = valid && BETWEEN(bpm, 0.0001, 8.0e6);
                            if (!valid) {
                                parser_bms_report_error_s(ctx, "exbpm float error\n");
                                goto rage_quit;
                            }
                            ctx->exbpms.ok[bpm_id] = true;
                            ctx->exbpms.bpms[bpm_id] = (f32)bpm;
                        }
                    }
                } else if (string_starts_with(lhs, "EXBPM"_s)) {
                    if (rhs.len > 0 && lhs.len == 7) {
                        s32 bpm_id;
                        valid = parser_bms_parse_int_base36_len2(string_offset(lhs, 5), &bpm_id);
                        if (!valid) {
                            parser_bms_report_error_v(ctx, "failed to parse #EXBPMxx's xx: '%.*s'\n",
                                                      EXPAND_STR_FOR_PRINTF(lhs));
                            goto rage_quit;
                        }
                        f64 bpm;
                        valid = parse_f64(rhs, &bpm);
                        valid = valid && BETWEEN(bpm, 0.0001, 8.0e6);
                        if (!valid) {
                            parser_bms_report_error_s(ctx, "exbpm float is error\n");
                            goto rage_quit;
                        }
                        ctx->exbpms.ok[bpm_id] = true;
                        ctx->exbpms.bpms[bpm_id] = (f32)bpm;
                    }
                }
            } else {
                // printf("DATA %.*s\n", EXPAND_STR_FOR_PRINTF(str));
                ParserBms_ChannelData thing = {};
                thing.items.arena = ctx->temp_arena;
                s32 thebase = 36;
                s32 themeasure = (str.data[0]-'0')*100 + (str.data[1]-'0')*10 + (str.data[2]-'0');
                s32 chan;
                // Actually I think chan is only base 10...
                valid = parser_bms_parse_int_base36_len2(string_offset(str, 3), &chan);
                if (!valid) {
                    parser_bms_report_error_s(ctx, "failed to parse channel as base36 two digit integer\n");
                    goto rage_quit;
                }
                thing.measure = (s16)themeasure;
                thing.channel = (s16)chan;
                // printf("got data field: measure %d\tchannel \"%.*s\"\t(chnum %d)\n", thing.measure, 2, str.data+3, thing.channel);

                String data_str = str;
                advance(&data_str, 6);
                trim_whitespace(&data_str);
                if (data_str.len <= 0) {
                    parser_bms_report_error_s(ctx, "data thing is empty\n");
                    goto rage_quit;
                }
                if (chan == 9) {
                    parser_bms_report_error_s(ctx, "channel 9 (stop) is not supported!\n");
                    goto rage_quit;
                } else if (chan == 2) {
                    // https://en.wikipedia.org/wiki/Metre_(music)
                    // parser_bms_report_error_s(ctx, "channel 02 ignored for now (metre)\n");
                    f64 f64_val;
                    valid = parse_f64(data_str, &f64_val);
                    if (!valid) {
                        parser_bms_report_error_s(ctx, "failed to parse float value (time signature)\n");
                        goto rage_quit;
                    }
                    thing.f32_val = (f32)f64_val;
                    ctx->channels_data.push(thing);
                } else if (chan == 3) {
                    // TODO: goto rage_quit if more than one 03 or 08 exists in one measure.
                    if (data_str.len & 1) {
                        parser_bms_report_error_s(ctx, "data thing does not have an even number of characters\n");
                        goto rage_quit;
                    }
                    for (s32 i = 0; i < data_str.len; i += 2) {
                        s32 num;
                        valid = parser_bms_parse_int_base16_len2(string_offset(data_str, i), &num);
                        if (!valid) {
                            parser_bms_report_error_s(ctx, "bpm is not base 16\n");
                            goto rage_quit;
                        }
                        ASSERT(BETWEEN(num, 0, 0xff));
                        thing.items.push((u16)num);
                    }
                    ASSERT(thing.items.len == data_str.len/2);

                    ctx->channels_data.push(thing);
                } else if (chan == 8) {
                    // TODO: goto rage_quit if more than one 03 or 08 exists in one measure.
                    if (data_str.len & 1) {
                        parser_bms_report_error_s(ctx, "data thing does not have an even number of characters\n");
                        goto rage_quit;
                    }
                    for (s32 i = 0; i < data_str.len; i += 2) {
                        s32 num;
                        valid = parser_bms_parse_int_base36_len2(string_offset(data_str, i), &num);
                        if (!valid) {
                            parser_bms_report_error_s(ctx, "exbpm is not base 36\n");
                            goto rage_quit;
                        }
                        thing.items.push((u16)num);
                    }
                    ASSERT(thing.items.len == data_str.len/2);

                    ctx->channels_data.push(thing);
                } else {
                    // channels: 01 = bgm
                    // channels: 02 = metre - length of #xxx (1 is 4/4 meter) - integer or decimal fraction
                    //
                    // #xxx02:1 -> 4/4 meter
                    // #xxx02:2 -> 8/4 meter
                    // #xxx02:0.75 -> 3/4 meter
                    //
                    // #xxx02:y acts only on xxx
                    // if not specified in another measure, it is 1
                    //
                    //
                    //
                    // channels: 03 = bpm change 01-FF -> 1->255 bpm (??)
                    // #xxx03:yy[yyyyyyyyyy]
                    // yy is a base 16 number.
                    // i don't know exactly what 00 means.
                    // #xxx03:beff
                    // 190 bpm at start of xxx
                    // 255 bpm at half of xxx
                    //
                    // channels: 08 = bpm change (#BPMxx n, #EXBPMxx n)
                    // similar to channel 03, except that instead of giving bpm from 1-255, it is like a WAVxx
                    // at the header: #BPMxx y or #EXBPMxx y
                    // where y is a regular number
                    // example:
                    // #BPMII 300
                    // #BPMzz 155.5
                    // #00108:IIzzIIzz
                    //
                    // channels 03 and 08 could collide, so I need to check for that
                    //
                    // question: does the bpm stay active for the rest of the song or only for that particular measure?
                    // YES. it stays active until the next bpm
                    // what about #xxx03:00ff
                    // what does 00 mean?
                    //
                    // channels: 04 = bga-base (ignored, visuals only)
                    // channels: 05 = extended object (ignored, purely visual)
                    // channels: 06 = bga poor (ignored, purely visual)
                    // channels: 07 = bga
                    // channels: 09 = stop (#STOPxx n)
                    // channels: 0A-0E = bga
                    // channels: 11-1Z: 1P regular note
                    // channels: 21-2Z: 2P regular note
                    // channels: 31-3Z: 1P invisible note (only purpose is to change the bgm)
                    // channels: 41-4Z: 2P invisible note (only purpose is to change the bgm)
                    // channels: 51-5Z: 1P LN object
                    // channels: 61-6Z: 2P LN object
                    // channels: 97 = bgm volume
                    // channels: 98 = key volume
                    // channels: D1-D9 = 1P mine
                    // channels: E1-E9 = 2P mine
                    //
                    // LNs:
                    // type 1 (strict BML):
                    // header #LNOBJ xx where xx is the WAVxx to be used as the LN end.
                    // if you have
                    // #xxx11-19:00AB0001
                    // it means start ln at AB and end at 01
                    //
                    // type 2 (loose BML):
                    // #xxx51-59:zzzzzz
                    // on header #LNTYPE 1 or 2
                    // LNTYPE 1 is RDM notation
                    // LNTYPE 2 is MGQ notation
                    // RDM notation:
                    // LN start point if the index which is not 00 is found.
                    // LN end point if the index which is not 00 is found next.
                    // so basically
                    // #xxx51-59:00AA00BB
                    // start ln with AA and end with BB
                    //
                    // #LNTYPE 1
                    // #00151:00220000
                    // #06451:000000000033
                    // ln starts at measure 001 and ends at measure 064
                    //
                    //
                    // MGQ notation:
                    // ln starts when index which is not 00 is found
                    // ln is active when indexes are not 00
                    // ln ends when the next 00 is found
                    // #xxx51-59:00aaaaaaaabb00
                    //             ^         ^
                    //             start     end
                    // i don't think I need to implement MGQ notation
                    //
                    //
                    // apparently a lot of implementations don't support mgq anymore.
                    //
                    // LNTYPE is 1 by default
                    //
                    // if LNOBJ is present, LNOBJ at channel 11-19 and channel 51-59 can not appear on the same file.
                    //
                    // you can have multiple #LNOBJ. if you ahve
                    // #LNOBJ AA
                    // #LNOBJ BB
                    // it means you can end ln with either AA or BB. it will change the sound.
                    //



                    if (BETWEEN(chan, 36+1, 36+9)) {
                        if (data_str.len & 1) {
                            parser_bms_report_error_s(ctx, "data thing does not have an even number of characters\n");
                            goto rage_quit;
                        }
                        for (s32 i = 0; i < data_str.len; i += 2) {
                            s32 num;
                            // Actually, some channels require values base16, and others, base36....
                            // @BmsQuirk TODO: Some channels expect a floating point number instead of base36. (channel "02", for example)
                            valid = parser_bms_parse_int_base36_len2(string_offset(data_str, i), &num);
                            if (!valid) {
                                parser_bms_report_error_s(ctx, "data thing is not base 36\n");
                                goto rage_quit;
                            }
                            ASSERT(BETWEEN(num, 0, 0x7fff));
                            thing.items.push((u16)num);
                        }
                        ASSERT(thing.items.len == data_str.len/2);

                        // TODO: Handle repeated channels. Some channels can be
                        // repeated (I don't know which). Some can not (should replace the old one with the
                        // latest)
                        ctx->channels_data.push(thing);
                    } else {
                        // ignore channel
                        // parser_bms_report_error_v(ctx, "ignored channel %d\n", (s32)chan);
                    }
                }
            }
        }
    }

    // Transform the ctx into a fileset
    {
        // TODO: In the future I'll want to all the .bms inside a directory into one fileset.
        // For now it is one bms diff per fileset.
        // Fileset new_fileset = {};
        // new_fileset.parser_type = ParserType_BMS;
        // new_fileset.filepath = copy_string(filename);
        // new_fileset.diffs_count = 1;
        // new_fileset.diffs = xmalloc_struct_count(Diff, new_fileset.diffs_count, xmalloc_flags_zero());
        // Diff *diff = new_fileset.diffs;
        // // diff->parent_fileset = ret_fileset;
        // diff->filepath = copy_string(new_fileset.filepath);
        Diff thediff_ = {};
        Diff *diff = &thediff_;
        diff->filepath = copy_string(filename);
        diff->metadata.title = copy_string(ctx->title_utf8.unwrap_or("(No Title)"_s));
        diff->metadata.artist = copy_string(ctx->artist_utf8.unwrap_or("(No Artist)"_s));

        diff->metadata.diff_idx = 0;

        if (!ctx->header_bpm.is_some()) {
            parser_bms_report_error_s(ctx, "No header bpm\n");
            goto rage_quit;
        }
        NewArrayDynamic<NoteTimestamp> ts_buf = {};
        ts_buf.arena = ctx->temp_arena;
        ts_buf.reserve(4096/SSIZEOF_VAL(ts_buf.data));
        NewArrayDynamic<NoteType> raw_notes_buf = {};
        raw_notes_buf.arena = ctx->temp_arena;
        raw_notes_buf.reserve(4*4096);

        f32 header_bpm = ctx->header_bpm.unwrap();
        f32 header_bps = header_bpm / 60.0f;
        Notes notes_temp = {};
        s32 max_cols = 24;
        // We'll start with 5k+1 and as we parse the file we'll change it to 7k+1 or 9k if necessary
        notes_temp.keymode = Keymode_5kp1;
        notes_temp.columns = 6;
        notes_temp.pitch = NOTES_GET_PITCH_SIZE(max_cols);
        notes_temp.timestamps = ts_buf.data;
        notes_temp.raw_notes = raw_notes_buf.data;
        bool seen_cols[2][9] = {};
        s32 last_note_idx = 0;
        bool got_keys_for_7kp1 = false;
        bool got_keys_for_pms = false;

        const Keymode columns_to_keymode_table[123] = {
            [6] = Keymode_5kp1,
            [8] = Keymode_7kp1,
            [9] = Keymode_9k,
        };

        s32 last_seen_measure = -1;
        s32 last_seen_channel = -1;

        f32 meter = 1;
        // TODO: f32 meters[1000];

        f64 measure_start_seconds = 0; // TODO
        s32 last_bidx_used = 0;

        NewArrayDynamic<ParserOjn_Bpm> thebpms = {};
        thebpms.arena = ctx->temp_arena;
        ParserOjn_Bpm initial_bpm = {};
        initial_bpm.bps = header_bpm / 60.0f;
        auto *measure_start_bpm = thebpms.push(initial_bpm);

        struct {
            bool doit;
            s32 measure;
        } want_to_ignore_bpm_chan[2] = {};

        NewArrayDynamic<ParserOjn_Bpm> bpms_temp = {};
        bpms_temp.arena = ctx->temp_arena;



        // TODO: Sort datafields first.
        parser_bms_sort_channels_data(ctx->channels_data);
        // Make the notes from the datafields
        for (smem idx_data = 0;
             idx_data < ctx->channels_data.len;
             idx_data++) {
            auto *datafield = &ctx->channels_data.data[idx_data];

            if (last_seen_measure != datafield->measure) {
                last_seen_measure = datafield->measure;
                if (last_seen_channel > datafield->measure) {
                    parser_bms_report_error_s(ctx, "measures aren't sorted\n");
                    goto rage_quit;
                }
                last_seen_channel = -1;
                meter = 1;
            }

            {
                f64 measure_frac = (f64)datafield->measure;
                s32 measure_start_bidx = parser_ojn_find_beat_idx_by_measure_frac(thebpms, measure_frac, last_bidx_used,
                                                                                  NULL, true);
                last_bidx_used = measure_start_bidx;
                measure_start_bpm = &thebpms.data[measure_start_bidx];

                f64 bpm_measure_frac = (f64)measure_start_bpm->measure + measure_start_bpm->measure_position;
                f64 delta_measure_frac_from_bpm_start = measure_frac - bpm_measure_frac;
                ASSERT(measure_start_bpm->bps != 0.0f);
                // TODO: meter
                f64 seconds_from_bpm_start = 4*delta_measure_frac_from_bpm_start / (f64)measure_start_bpm->bps;
                f64 seconds_from_song_start = measure_start_bpm->start_seconds + seconds_from_bpm_start;
                measure_start_seconds = seconds_from_song_start;
            }

            // TODO: bpm and measure
            if (datafield->channel == 2) {
                meter = datafield->f32_val;
                // TODO: do this
#if 0
                parser_bms_report_error_s(ctx, "meter is not supported yet\n");
#endif
                goto rage_quit;
            } else if (datafield->channel == 3 || datafield->channel == 8) {
                // TODO: don't goto rage_quit if more than one 03 or 08 exists in one measure. try to merge them.

                // TODO: Properly do this

                bool is_chan8 = datafield->channel == 8;
                s32 other_chan = is_chan8 ? 3 : 8;
                // want_to_ignore_bpm_chan should be outside of the loop
                if (parser_bms_find_duplicate(ctx->channels_data, datafield->measure,
                                              datafield->channel, idx_data)) {
                    parser_bms_report_error_s(ctx, "found duplicate bpm channel!\n");
                    goto rage_quit;
                }
                if (want_to_ignore_bpm_chan[!is_chan8].doit
                    && want_to_ignore_bpm_chan[!is_chan8].measure == datafield->measure) {
                    want_to_ignore_bpm_chan[!is_chan8] = {};
                    continue;
                }
                auto *from_other = parser_bms_find_duplicate(ctx->channels_data, datafield->measure,
                                                             other_chan, idx_data);
                ParserBms_ChannelData *from3 = 0;
                ParserBms_ChannelData *from8 = 0;
                if (!is_chan8) {
                    from3 = datafield;
                    from8 = from_other;
                } else {
                    from3 = from_other;
                    from8 = datafield;
                }

                bpms_temp.len = 0;
                ASSERT(thebpms.len > 0);
                // parser_bms_make_bpms_temp will compute each bpm's start_seconds
                valid = parser_bms_make_bpms_temp(ctx, &bpms_temp, from3, from8, &thebpms.data[thebpms.len-1],
                                                  datafield->measure, measure_start_seconds);
                if (!valid) {
                    parser_bms_report_error_s(ctx, "make_bpms_temp\n");
                    goto rage_quit;
                }
                if (from_other) {
                    want_to_ignore_bpm_chan[is_chan8].doit = true;
                    want_to_ignore_bpm_chan[is_chan8].measure = datafield->measure;
                }

                for (s32 i = 0; i < bpms_temp.len; i++) {
                    thebpms.push(bpms_temp.data[i]);
                }
            } else {
                bool is_normal_note = false;
                s32 playerside_for_normal_note = 0;
                s32 key_idx = -1;
                // TODO: Handle double. Right now I treat it as if it were the same thing, which is wrong.
                // TODO: Handle pms type 11-15,22-25, recognize extension.
                if (BETWEEN(datafield->channel, 36+1, 36+9)) {
                    is_normal_note = true;
                    playerside_for_normal_note = 1;
                    key_idx = datafield->channel - (36+1);
                    // TODO: In the end i'll have to swap the columns.
                    if (key_idx == 8-1 || key_idx == 9-1) {
                        key_idx -= 1;
                        got_keys_for_7kp1 = true;
                    } else if (key_idx == 7-1) {
                        key_idx += 2;
                        got_keys_for_pms = true;
                    }
                } else if (BETWEEN(datafield->channel, 2*36+1, 2*36+9)) {
                    // I'll ignore p2 for now. TODO: Don't ignore it.
                    is_normal_note = false;
                    // is_normal_note = true;
                    // playerside_for_normal_note = 2;
                    // key_idx = datafield->channel - 21;
                }

                // TODO: LNs and mines
                if (is_normal_note) {
                    if (parser_bms_find_duplicate(ctx->channels_data, datafield->measure, datafield->channel, idx_data)) {
                        parser_bms_report_error_s(ctx, "Found duplicate channel\n");
                        goto rage_quit;
                    }
                    ASSERT(key_idx >= 0);
                    // TODO: reimplement this seen_cols thing. this sucks. p2 doesn't work.
                    // TODO: Translate the column right now instead of doing it later.
                    if (!seen_cols[playerside_for_normal_note][key_idx]) {
                        seen_cols[playerside_for_normal_note][key_idx] = true;
                        if (key_idx+1 > notes_temp.columns) {
                            notes_temp.columns = key_idx+1;
                            // TODO: some pms files have both p1 and p2.
                            ASSERT(notes_temp.columns <= max_cols);
                        }
                    }
                    ASSERT(key_idx >= 0 && key_idx < notes_temp.columns);
                    // TODO: Handle bpm properly like in parser_ojn

                    f64 last_seconds_from_song_start = -F64_MAX;
                    s32 this_last_bidx = -123;
                    s32 this_last_note_idx = -123;

                    u16 *items = datafield->items.data;
                    smem items_len = datafield->items.len;
                    for (smem idx_item = 0;
                         idx_item < items_len;
                         idx_item++) {
                        u16 item = items[idx_item];
                        if (!item) continue;
                        // there was a note. we don't care what WAV it was.
                        s32 measure = datafield->measure;
                        f64 measure_position = (f64)idx_item/(f64)items_len;
                        f64 measure_frac = (f64)measure + measure_position;

                        ParserOjn_Bpm this_thebpm;
                        s32 this_bidx = parser_ojn_find_beat_idx_by_measure_frac(thebpms, measure_frac, last_bidx_used,
                                                                                 &this_thebpm, true);
                        last_bidx_used = this_bidx;
                        ASSERT(this_bidx >= this_last_bidx);
                        this_last_bidx = this_bidx;

                        f64 bpm_start_seconds = this_thebpm.start_seconds;
                        f64 bpm_measure_frac = (f64)this_thebpm.measure + this_thebpm.measure_position;
                        f64 delta_measure_frac_from_bpm_start = measure_frac - bpm_measure_frac;
                        ASSERT(this_thebpm.bps > 0);
                        // TODO: meter[measure]
                        // TODO: integrate meter from bpm.measure to datafield->measure;
                        f64 seconds_from_bpm_start = /*(f64)meter * */delta_measure_frac_from_bpm_start * 4 / (f64)this_thebpm.bps;
                        f64 seconds_from_song_start = bpm_start_seconds + seconds_from_bpm_start;
                        ASSERT(seconds_from_song_start > last_seconds_from_song_start);
                        last_seconds_from_song_start = seconds_from_song_start;

                        NoteType new_note_type = NOTE_TAP;

                        s32 new_note_idx = parser_utils_add_note_to_diff(&notes_temp, &ts_buf, &raw_notes_buf,
                                                                         new_note_type, key_idx,
                                                                         seconds_from_song_start, last_note_idx);
                        ASSERT(new_note_idx > this_last_note_idx);
                        last_note_idx = new_note_idx;
                        this_last_note_idx = new_note_idx;
                    }
                }
            }
        }

        // Copy the temp notes into the new thing.
        s32 rows_count = notes_temp.rows_count;
        s32 pitch = NOTES_GET_PITCH_SIZE(notes_temp.columns);
        ASSERT(pitch <= notes_temp.pitch);

        ASSERT(notes_temp.columns < ARRAY_COUNT(columns_to_keymode_table));
        notes_temp.keymode = columns_to_keymode_table[notes_temp.columns];
        {
            // TODO: Remove this when I fix the things
            if (!keymode_is_valid(notes_temp.keymode)) {
                static int thecounter = 0;
                thecounter++;
                parser_bms_report_error_v(ctx, "got invalid keymode!!! fix this!!! columns %d (counter %d)\n",
                                          notes_temp.columns, thecounter);
                goto rage_quit;
            }
        }
        ASSERT(keymode_is_valid(notes_temp.keymode));

        diff->notes.keymode = notes_temp.keymode;

        {
            const String diffs_to_str[5] = {
                "BEGINNER"_s,
                "NORMAL"_s,
                "HYPER"_s,
                "ANOTHER"_s,
                "INSANE"_s,
            };
            char buf[512];
            String theplaylevel = ctx->playlevel_str_utf8.unwrap_or(""_s);
            s32 therank = ctx->rank.unwrap_or(9999);
            s32 thedifficulty = ctx->difficulty.unwrap_or(9999);
            String tmp;
            if (ABC(thedifficulty-1, ARRAY_COUNT(diffs_to_str))) {
                auto bmsdiffstr = diffs_to_str[thedifficulty-1];
                tmp.len = snprintf(buf, sizeof(buf), "Some BMS Diff: rank %d, %.*s playlevel: '%.*s' diff %d",
                                   therank,
                                   EXPAND_STR_FOR_PRINTF(bmsdiffstr),
                                   EXPAND_STR_FOR_PRINTF(theplaylevel), thedifficulty);
            } else {
                tmp.len = snprintf(buf, sizeof(buf), "Some BMS Diff: rank %d, playlevel: '%.*s' diff %d",
                                   therank,
                                   EXPAND_STR_FOR_PRINTF(theplaylevel), thedifficulty);
            }
            tmp.data = (u8 *)buf;
            diff->metadata.diff_name = copy_string(tmp);
        }
        diff->notes.columns = notes_temp.columns;
        diff->notes.pitch = pitch;
        diff->notes.rows_count = rows_count;
        diff->notes.timestamps = xmalloc_struct_count(NoteTimestamp, rows_count);
        xmemcpy_struct_count(diff->notes.timestamps, notes_temp.timestamps, NoteTimestamp, rows_count);

        diff->notes.raw_notes = xmalloc_struct_count(NoteType, rows_count*pitch);
        // Since the pitch is different now we need to copy each row individually
        for (s32 idx_row = 0; idx_row < rows_count; idx_row++) {
            // TODO: Translate the column at the start, not here. This should be a straight copy.
            NoteType *dst = diff->notes.raw_notes + idx_row*pitch;
            NoteType *src = notes_temp.raw_notes + idx_row*notes_temp.pitch;
            if (got_keys_for_pms) {
                // TODO: Swap columns, check file extension for .pms (need to verify what the correct thing is.)
                // type PMS (i guess this is extension .pms)
                // K0 K1 K2 K3 K4 K5 K6 K7 K8
                // 11 12 13 14 15 22 23 24 25
                // type PMS (bme-type) (i guess this is extension .bme or .bms (we know it's pms with channel 17))
                // K0 K1 K2 K3 K4 K5 K6 K7 K8     K0 K1 K2 K3 K4 K5 K6 K7 K8
                // 11 12 13 14 15 18 19 16 17     21 22 23 24 25 28 29 26 27
                // type PMS (5 button)
                // K0 K1 K2 K3 K4 K5 K6 K7 K8
                // -- -- 13 14 15 22 23 -- --
            } else if (got_keys_for_7kp1) {
                // K0 K1 K2 K3 K4 K5 K6 SCRATCH
                //  0  1  2  3  4  6  7  5
                // swap columns:
                // col 5 to 7
                // col 6,7 to 5,6
                NoteType *row = src;
                NoteType k5 = NOTES_GET_COLUMN(row, 6);
                NoteType k6 = NOTES_GET_COLUMN(row, 7);
                NoteType ks = NOTES_GET_COLUMN(row, 5);
                NOTES_SET_COLUMN(row, 5, k5);
                NOTES_SET_COLUMN(row, 6, k6);
                NOTES_SET_COLUMN(row, 7, ks);
            }
            xmemcpy_struct_count(dst, src, NoteType, pitch);
        }

        diff->notes.last_hold_is_released = xmalloc_struct_count(b8, diff->notes.columns, xmalloc_flags_zero());


        diff->notes.timings.bpms_count = (s32)thebpms.len;
        diff->notes.timings.bpms = xmalloc_struct_count(BeatInfo, diff->notes.timings.bpms_count, xmalloc_flags_zero());
        for (s32 idx_bpm = 0; idx_bpm < thebpms.len; idx_bpm++) {
            ParserOjn_Bpm *thebpm = &thebpms.data[idx_bpm];
            BeatInfo *new_bi = &diff->notes.timings.bpms[idx_bpm];
            new_bi->bps = thebpm->bps;
            new_bi->ts_start = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(thebpm->start_seconds)};
            if (idx_bpm + 1 < thebpms.len) {
                // @IgnoreSomeBitsOfTheMantissa
                new_bi->ts_end = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL((thebpm+1)->start_seconds)};
            } else {
                new_bi->ts_end = NOTE_MAX_TIMESTAMP;
            }
            ASSERT(new_bi->ts_start.value >= 0);
            ASSERT(new_bi->ts_end > new_bi->ts_start);
        }

        *ret_diff = *diff;
        // *ret_fileset = new_fileset;
    }

    // if (ctx->genre_utf8.ok) printf("GENRE is '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->genre_utf8.val));
    // if (ctx->title_utf8.ok) printf("TITLE is '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->title_utf8.val));
    // if (ctx->artist_utf8.ok) printf("ARTIST is '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->artist_utf8.val));
    // if (ctx->comment_utf8.ok) printf("COMMENT is '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->comment_utf8.val));

    end_temp_memory(&temp_mem);
    return true;
rage_quit:
    end_temp_memory(&temp_mem);
    return false;
}

static bool
parser_bms_fileset_from_file(MemoryArena *final_arena, MemoryArena *temp_arena,
                             String filename_not_nullter, bool report_errors, Fileset *ret_fileset)
{
    final_arena = 0; // not using it.
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    // Assert there's at least a megabyte free.
    ASSERT(temp_arena->size - temp_arena->used > MEGABYTES(1));

    String filename = arena_push_string_nullter(temp_arena, filename_not_nullter);

    bool valid;
    {
        // TODO: In the future I'll want to all the .bms inside a directory into one fileset.
        // For now it is one bms diff per fileset.
        Fileset new_fileset = {};
        new_fileset.parser_type = ParserType_BMS;
        new_fileset.filepath = copy_string(filename);
        new_fileset.diffs_count = 1;
        new_fileset.diffs = xmalloc_struct_count(Diff, new_fileset.diffs_count, xmalloc_flags_zero());
        Diff *diff = new_fileset.diffs;
        // diff->parent_fileset = ret_fileset;
        valid = parser_bms_diff_from_file(final_arena, temp_arena, filename, report_errors, diff);
        if (!valid) {
            goto rage_quit;
        }
        *ret_fileset = new_fileset;
    }

    end_temp_memory(&temp_mem);
    return true;
rage_quit:
    end_temp_memory(&temp_mem);
    return false;
}

// TODO: parser_bms_fileset_from_directory() give a directory and it will
// iteratate on that directory (non recursively) to get .bms files
