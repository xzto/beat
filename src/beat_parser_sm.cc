// Incomplete

#include "beat_include.hh"
#include "beat_parser_sm.hh"

// TODO: After using arenas here, remove all the free calls


// TODO: I might need to ignore files that have stops (or properly parse them).


static void
parser_sm_free_bpms(NewArrayDynamic<ParserSmBpm> *bpms)
{
    ASSERT(bpms != NULL);
    bpms->free();
}

// frees the things inside the diff, not the diff object itself
static void
parser_sm_free_diff(ParserSmDiff *diff)
{
#if !TEST_PASM_USE_ARENAS
    ASSERT(diff != NULL);
    // Assert that notes is inside the pool
    ASSERT((u8 *)diff->notes.notes_by_measure == diff->notes_raw_data);
    xfree(diff->notes_raw_data);

    // if (diff->chart_type_string.data != NULL) free_string(&diff->chart_type_string);
    // This string is not allocated on the heap. It's from
    // parser_sm_chart_types_string_table which is constant.
    if (diff->chart_type_string.data != NULL) {
        diff->chart_type_string.data = NULL;
        diff->chart_type_string.len = 0;
    }
    if (diff->description.data != NULL) free_string(&diff->description);
    if (diff->diff_name.data != NULL) free_string(&diff->diff_name);

    zero_struct(diff);
#else
    // TODO: Remove this 
    ASSERT(false);
#endif
}

static void
parser_sm_free_file(ParserSmFile *file)
{
#if !TEST_PASM_USE_ARENAS
    ASSERT(file != NULL);

    if (file->title.data != NULL) free_string(&file->title);
    if (file->subtitle.data != NULL) free_string(&file->subtitle);
    if (file->artist.data != NULL) free_string(&file->artist);
    if (file->title_translit.data != NULL) free_string(&file->title_translit);
    if (file->subtitle_translit.data != NULL) free_string(&file->subtitle_translit);
    if (file->artist_translit.data != NULL) free_string(&file->artist_translit);
    if (file->genre.data != NULL) free_string(&file->genre);
    if (file->credit.data != NULL) free_string(&file->credit);
    if (file->banner.data != NULL) free_string(&file->banner);
    if (file->background.data != NULL) free_string(&file->background);
    if (file->cdtitle.data != NULL) free_string(&file->cdtitle);
    if (file->music.data != NULL) free_string(&file->music);

    if (file->bpms.data != NULL) parser_sm_free_bpms(&file->bpms);

    for (s32 i = 0; i < file->diffs_count; i++) {
        parser_sm_free_diff(&file->diffs[i]);
    }

    zero_struct(file);
#else
    ASSERT(false);
#endif
}


// Parser begins here
#define parser_sm_is_whitespace(ch) ((ch) == ' ' || (ch) == '\t')

#define parser_sm_in_bounds_at_ptr(ctx, ptr, i) ((ptr) + (i) < (ctx)->buf_end)
#define parser_sm_peek_char_at_ptr(ptr, i, ch) ((ptr)[(i)] == (ch))
#define parser_sm_expect_char_at_ptr(ptr, ch) (parser_sm_peek_char_at_ptr(ptr, 0, ch) ? ((ptr)++, true) : false)

#define parser_sm_in_bounds(ctx, i)     (parser_sm_in_bounds_at_ptr(ctx, (ctx)->buf_ptr, i))
#define parser_sm_peek_char(ctx, i, ch) (parser_sm_peek_char_at_ptr((ctx)->buf_ptr, i, ch))
#define parser_sm_expect_char(ctx, ch)  (parser_sm_expect_char_at_ptr((ctx)->buf_ptr, ch))


static void
parser_sm_skip_space(ParserSmCtx *ctx)
{
    while (parser_sm_in_bounds(ctx, 0) && parser_sm_is_whitespace(*ctx->buf_ptr)) {
        ctx->buf_ptr++;
    }
}


// Returns 0 if not a newline and the size of the newline in bytes if it is.
static s32
parser_sm_is_newline(ParserSmCtx *ctx, s32 offset)
{
    s32 size = 0;
    if (parser_sm_in_bounds(ctx, 1)) {
        if (parser_sm_peek_char(ctx, offset + size, '\r'))
            size++;
        if (parser_sm_peek_char(ctx, offset + size, '\n'))
            size++;
    } else if (parser_sm_in_bounds(ctx, 0)) {
        if (parser_sm_peek_char(ctx, offset + 0, '\r'))
            size++;
        else if (parser_sm_peek_char(ctx, offset + 0, '\n'))
            size++;
    }
    return size;
}

static bool
parser_sm_skip_comment(ParserSmCtx *ctx)
{
    bool got_comment = false;
    if (parser_sm_in_bounds(ctx, 1)) {
        if (parser_sm_peek_char(ctx, 0, '/') && parser_sm_peek_char(ctx, 1, '/'))  {
            got_comment = true;
            ctx->buf_ptr += 2;
            while (parser_sm_in_bounds(ctx, 0) && (parser_sm_is_newline(ctx, 0) == 0)) {
                ctx->buf_ptr++;
            }
        }
    }
    return got_comment;
}

static bool
parser_sm_expect_newline_or_eof(ParserSmCtx *ctx)
{
    bool valid = true;
    valid = !parser_sm_in_bounds(ctx, 0);
    if (valid) return true;
    s32 newline_size = parser_sm_is_newline(ctx, 0);
    valid = newline_size != 0;
    if (!valid) return false;
    ctx->buf_ptr += newline_size;
    ctx->line1++;
    return true;
}

static void
parser_sm_skip_spaces_comments_and_newlines(ParserSmCtx *ctx)
{
    bool valid;
    parser_sm_skip_space(ctx);
    while (parser_sm_skip_comment(ctx) || parser_sm_is_newline(ctx, 0)) {
        valid = parser_sm_expect_newline_or_eof(ctx);
        ASSERT_SLOW(valid);
        parser_sm_skip_space(ctx);
    }
}


// Don't use it for newlines
// NULL if didn't find it, the pointer to it otherwise
static u8 *
parser_sm_find_char(ParserSmCtx *ctx, char ch, bool skip_backslashed, s32 *ret_newlines)
{
    ASSERT_SLOW(ch != '\r');
    ASSERT_SLOW(ch != '\n');
    bool valid = true;
    u8 *ptr = ctx->buf_ptr;
    s32 count_newlines = 0;

    if (*ptr != ch) {
        while (parser_sm_in_bounds_at_ptr(ctx, ptr, 0)
               && (!parser_sm_peek_char_at_ptr(ptr, 0, ch)
                   || (skip_backslashed && parser_sm_peek_char_at_ptr(ptr, -1, '\\')))) {
            s32 nl_size = 0;
            if (*ptr == '\r') {
                nl_size++;
                if (parser_sm_in_bounds_at_ptr(ctx, ptr, 1) && parser_sm_peek_char_at_ptr(ptr, 1, '\n')) {
                    nl_size++;
                }
            } else if (*ptr == '\n') {
                nl_size++;
            }
            if (nl_size > 0) {
                ptr += nl_size;
                count_newlines++;
            } else {
                ptr++;
            }
        }
    }

    if (ret_newlines != NULL)
        *ret_newlines = count_newlines;

    valid = parser_sm_in_bounds_at_ptr(ctx, ptr, 0);
    if (!valid) return NULL;
    return ptr;
}

static bool
parser_sm_find_string_ending_with_char(ParserSmCtx *ctx, char ch, bool can_get_newlines,
                                       bool skip_backslashed, String *ret_string)
{
    ASSERT_SLOW(ret_string != NULL);
    zero_struct(ret_string);

    bool valid = true;
    s32 newlines;
    u8 *the_ptr = parser_sm_find_char(ctx, ch, skip_backslashed, &newlines);
    valid = the_ptr != NULL;
    if (!valid) {
        parser_sm_report_error_v(ctx, "expected '%c' somewhere!!!\n", ch);
        return false;
    }
    if (newlines > 0) {
        ctx->line1 += newlines;
    }
    // can_get_newlines isn't that important. i could remove it
    valid = can_get_newlines || newlines == 0;
    if (!valid) {
        parser_sm_report_error_v(ctx, "Got newline before '%c' but shouldn't have!\n", ch);
        return false;
    }
    ret_string->len = the_ptr - ctx->buf_ptr;
    ret_string->data = ctx->buf_ptr;
    ctx->buf_ptr = the_ptr + 1;
    return true;
}

static bool
parser_sm_parse_s64(ParserSmCtx *ctx, s64 *ret_s64, s32 base)
{
    ASSERT_SLOW(ret_s64 != NULL);
    bool valid = true;
    char *end;
    // @CStringFutureProblem
    s64 i = strtol((char *)ctx->buf_ptr, &end, base);
    valid = end != (char *)ctx->buf_ptr;
    if (!valid) {
        parser_sm_report_error_s(ctx, "failed reading integer\n");
        return false;
    }
    valid = end <= (char *)ctx->buf_end;
    if (!valid) {
        parser_sm_report_error_s(ctx, "overran buffer reading integer\n");
        return false;
    }
    // smem diff_ptrs = end - (char *)ctx->buf_ptr;
    ctx->buf_ptr = (u8 *)end;
    *ret_s64 = i;
    return true;
}

static bool
parser_sm_parse_f64(ParserSmCtx *ctx, f64 *ret_f64)
{
    ASSERT_SLOW(ret_f64 != NULL);
    bool valid = true;
    char *end;
    // @CStringFutureProblem
    f64 f = strtod((char *)ctx->buf_ptr, &end);
    valid = end != (char *)ctx->buf_ptr;
    if (!valid) {
        parser_sm_report_error_s(ctx, "failed reading float\n");
        return false;
    }
    valid = end <= (char *)ctx->buf_end;
    if (!valid) {
        parser_sm_report_error_s(ctx, "overran buffer reading float\n");
        return false;
    }
    // smem diff_ptrs = end - (char *)ctx->buf_ptr;
    ctx->buf_ptr = (u8 *)end;
    *ret_f64 = f;
    return true;
}

// returns a PARSER_SM_ERR_xxx
static s32
parser_sm_parse_f64_pair(ParserSmCtx *ctx, s32 count_pairs, f64 *ret_f1, f64 *ret_f2)
{
    ASSERT_SLOW(ctx->buf.data != NULL);
    ASSERT_SLOW(ret_f1 != NULL);
    ASSERT_SLOW(ret_f2 != NULL);
    *ret_f1 = 0.0;
    *ret_f2 = 0.0;
    // skip all whitespace and comment (SAWC)
    // maybe get a comma
    // SAWC
    // parse a float
    // SAWC
    // equal sign
    // SAWC
    // parse a float
    // done
    // In the future when i fully rewrite the parser, pass a ParseState instead of just the buf and buf_idx
    // The ParseState would include this value_buf, value_buf_idx, and a file_buf and file_buf_idx
    bool valid = true;
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_in_bounds(ctx, 0);
    if (!valid)
        return PARSER_SM_ERR_DONE; // We hit the end of the string.
    // If this is not the first pair, expect a comma
    if (count_pairs > 0) {
        valid = parser_sm_expect_char(ctx, ',');
        if (!valid) {
            // Parse error: no comma
            parser_sm_report_error_s(ctx, "Failed to parse bpms: expected ','\n");
            return PARSER_SM_ERR_FAILED;
        }
        parser_sm_skip_spaces_comments_and_newlines(ctx);
    }
    // Parse a pair in the form "number_first=number_second"
    // Parse the first one
    f64 first;
    valid = parser_sm_parse_f64(ctx, &first);
    if (!valid) {
        // Parse error: failed to parse float
        parser_sm_report_error_s(ctx, "Failed to parse first float of bpms\n");
        return PARSER_SM_ERR_FAILED;
    }
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    // Expect an equal sign
    valid = parser_sm_expect_char(ctx, '=');
    if (!valid) {
        // Parse error: no equal sign
        parser_sm_report_error_s(ctx, "Failed to parse bpms: expected '=' after first float\n");
        return PARSER_SM_ERR_FAILED;
    }
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    // Parse the second one
    f64 second;
    valid = parser_sm_parse_f64(ctx, &second);
    if (!valid) {
        // Parse error: failed to parse float
        parser_sm_report_error_s(ctx, "Failed to parse second float of bpms\n");
        return PARSER_SM_ERR_FAILED;
    }
    // Parsed "float=float" successfully
    *ret_f1 = first;
    *ret_f2 = second;
    return PARSER_SM_ERR_CONTINUE;
}

static bool
parser_sm_parse_display_bpm(ParserSmCtx *ctx, ParserSmDisplayBpm *ret_display_bpm)
{
    ASSERT_SLOW(ret_display_bpm != NULL);
    zero_struct(ret_display_bpm);

    bool valid = true;
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_in_bounds(ctx, 0);
    if (!valid) return false;
    // number END, or number:number END, or * END
    if (parser_sm_peek_char(ctx, 0, '*')) {
        ret_display_bpm->type = PARSER_SM_DISPLAY_BPM_RANDOMLY_CHANGES;
        return true;
    }
    f64 f0;
    valid = parser_sm_parse_f64(ctx, &f0);
    if (!valid) return false;
    if (parser_sm_in_bounds(ctx, 0) && parser_sm_expect_char(ctx, ':')) {
        f64 f1;
        valid = parser_sm_parse_f64(ctx, &f1);
        if (!valid) return false;
        ret_display_bpm->type = PARSER_SM_DISPLAY_BPM_RANGE;
        ret_display_bpm->bpms[0] = (f32)f0;
        ret_display_bpm->bpms[1] = (f32)f1;
        return true;
    }

    ret_display_bpm->type = PARSER_SM_DISPLAY_BPM_STATIC;
    ret_display_bpm->bpms[0] = (f32)f0;
    return true;
}

static bool
parser_sm_parse_bpms(ParserSmCtx *ctx, NewArrayDynamic<ParserSmBpm> *ret_bpms)
{
    ASSERT_SLOW(ret_bpms != NULL);
    zero_struct(ret_bpms);
    ParserSmBpm *last_one = NULL;
    // TempMemory temp_mem = begin_temp_memory(ctx->temp_arena); // not using it for anything
    TempMemory temp_for_final_if_error = begin_temp_memory(ctx->final_arena);
    NewArrayDynamic<ParserSmBpm> bpms = {};
    bpms.arena = ctx->final_arena;

    bool valid = true;
    bpms.reserve(1024);

    s32 count_pairs = 0;
    s32 err;
    s32 buf_idx = 0;
    f64 this_beat, this_bpm;
    for (count_pairs = 0;
         (err = parser_sm_parse_f64_pair(ctx, count_pairs, &this_beat, &this_bpm))
         == PARSER_SM_ERR_CONTINUE;
         count_pairs++) {
        if (this_beat < 0.0) {
            parser_sm_report_error_s(ctx, "negative beat\n");
            goto rage_quit;
        }
        // ASSERT(this_bpm >= 0.0);
        if (this_bpm <= 0.0) {
            parser_sm_report_error_s(ctx, "negative or zero bpm. discarding song\n");
            goto rage_quit;
        }
        // We wont check if the bpm is too high here. The bpm might not even be
        // used in a note. We'll do it later when we transform the notes into
        // just timestamps

        ParserSmBpm one = {};
        one.start_beat = this_beat;
        // one.bpm = (f32)this_bpm;
        one.bps = (f32)(this_bpm / 60.0);
        // one.spb = (f32)(60.0 / this_bpm);
        if (count_pairs > 0) {
            ASSERT_SLOW(last_one != NULL);
            last_one->active_beats = (one.start_beat - last_one->start_beat);
            last_one->active_seconds = last_one->active_beats / (f64)last_one->bps;

            f64 seconds_to_add = ((one.start_beat - last_one->start_beat)
                                  / (f64)last_one->bps);
            one.start_seconds = last_one->start_seconds + seconds_to_add;

            valid = last_one->start_beat < one.start_beat;
            if (!valid) {
                parser_sm_report_error_s(ctx, "BPMS aren't sorted. Discarding song.\n");
                goto rage_quit;
            }

            // If the above one succeeded but this one failed we have a bug
            valid = last_one->start_seconds < one.start_seconds;
            if (!valid) {
                parser_sm_report_error_s(ctx, "BPM Seconds aren't sorted. We Have A Bug.\n");
                ASSERT(false);
                goto rage_quit;
            }
            // Make sure the beat is active for long enough.
            // valid = (one.start_seconds - last_one->start_seconds) > NOTE_PRECISION_SECONDS;
            // if (!valid) {
            //     parser_sm_report_error_s(ctx, "Beat is active isn't active long enough.\n");
            //     goto rage_quit;
            // }
        } else {
            one.start_seconds = 0.0;
        }

        last_one = bpms.push(one);
    }
    valid = err == PARSER_SM_ERR_DONE;
    if (!valid) {
        parser_sm_report_error_s(ctx, "failed to parse bpms\n");
        goto rage_quit;
    }
    valid = count_pairs > 0;
    if (!valid)  {
        parser_sm_report_error_s(ctx, "didn't get any bpms\n");
        goto rage_quit;
    }

    last_one->active_beats = F64_MAX;
    last_one->active_seconds = F64_MAX;

    *ret_bpms = bpms;

    end_temp_memory_dont_free(&temp_for_final_if_error);
    // end_temp_memory(&temp_mem); // not using it for anything

    return true;
rage_quit:
    // bpms.free(); /
    end_temp_memory(&temp_for_final_if_error);
    // end_temp_memory(&temp_mem); // not using it for anything
    return false;
}

static bool
parser_sm_parse_notes(ParserSmCtx *ctx, ParserSmFile *smfile)
{
    // TEST_TIMER_X_BEGIN(1);
    ASSERT_SLOW(smfile != NULL);

    // It wants in this order (We're gonna ignore the 5 spaces here);
    //      Chart Type  COLON
    //      Description/Author COLON
    //      Difficulty Name (Beginner, Easy, Medium, Hard, Challenge, Edit) COLON
    //      Difficulty number COLON
    //      Groove radar - comma-separated list of floats COLON - will ignore this
    // and the notes
    // the notes are like this
    // 0000
    // 0000
    // 0011
    // 0000
    // 0200
    // 1000
    // 0310
    // 0001
    // ,
    // repeat
    // Each number is a note in the columns. by chart type we know how many
    // columns there are.
    // In this case there are eight divisions in the measure.
    // The measure is a group of notes separated by a comma.
    // Each measure contains 4 beats exactly, so in this case every two rows
    // represent one beat and every row represents 1/2 a beat.
    // The measures always must contain a multiple of four rows of notes. If
    // that fails we failed to parse this diff.

    bool valid = true;

    // In this function I allocated the following things, in order:
    // - The strings inside the diff. (there are three of them):
    //   They don't get freed at the end.
    //
    // - final_notes_raw_data
    //   Does not get freed at the end.
    //
    // That's it.
    // When I transform it into a NewSong the latter two probably do get freed
    // and reallocated get into a proper space.

    TempMemory temp_mem = begin_temp_memory(ctx->temp_arena);
    TempMemory temp_for_final_if_error = begin_temp_memory(ctx->final_arena);

    void *final_notes_raw_data = NULL;
    ParserSmDiff final_diff = {};

    // stupid compiler!!!!!
    s32 notes_pitch;

    // this will be enough
    s32 temp_divisions_max_size;
    s32 temp_divisions_count;
    s16 *temp_divisions;

    s32 temp_notes_buffer_max_size;
    s32 temp_notes_buffer_count;
    u8 *temp_notes_buffer;

    s32 total_number_of_rows;
    s32 number_of_blank_rows;

    s32 columns;
    s32 columns_to_read;

    String grooveradar;
    String diffnumber;
    s32 the_actual_diffnumber;

    String desc;

    String chart_type;
    s32 chart_type_number;

    String diffname;

    valid = smfile->diffs_count < ARRAY_COUNT(smfile->diffs);
    if(!valid) {
        parser_sm_report_error_s(ctx, "Too many diffs!\n");
        goto rage_quit;
    }

    // Get the chart type
    // TODO: Currently, the final string includes the backslashes if there was a backslashed colon!!
    chart_type = {};
    chart_type_number = PARSER_SM_CHART_TYPE_UNKNOWN;
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_find_string_ending_with_char(ctx, ':', false, true, &chart_type);
    if (!valid) {
        parser_sm_report_error_s(ctx, "Couldn't get chart type\n");
        goto rage_quit;
    }
    valid = chart_type.len > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "chart type is empty\n");
        goto rage_quit;
    }
    parser_sm_skip_space(ctx);
    parser_sm_skip_comment(ctx);
    parser_sm_expect_newline_or_eof(ctx);


    // Get the description
    desc = {};
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_find_string_ending_with_char(ctx, ':', false, true, &desc);
    if (!valid) {
        parser_sm_report_error_s(ctx, "Couldn't get chart description\n");
        goto rage_quit;
    }
    // valid = desc.len > 0;
    // if (!valid) {
    //     parser_sm_report_error_s(ctx, "chart description is empty\n");
    //     goto rage_quit;
    // }
    parser_sm_skip_space(ctx);
    parser_sm_skip_comment(ctx);
    parser_sm_expect_newline_or_eof(ctx);


    // Get the difficulty name
    diffname = {};
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_find_string_ending_with_char(ctx, ':', false, true, &diffname);
    if (!valid) {
        parser_sm_report_error_s(ctx, "Couldn't get chart diffname\n");
        goto rage_quit;
    }
    valid = diffname.len > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "chart diffname is empty\n");
        goto rage_quit;
    }
    parser_sm_skip_space(ctx);
    parser_sm_skip_comment(ctx);
    parser_sm_expect_newline_or_eof(ctx);

    // Get the difficulty number
    diffnumber = {};
    the_actual_diffnumber = 0;
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_find_string_ending_with_char(ctx, ':', false, true, &diffnumber);
    if (!valid) {
        parser_sm_report_error_s(ctx, "Couldn't get chart diffnumber\n");
        goto rage_quit;
    }
    valid = diffnumber.len > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "chart diffnumber is empty\n");
        goto rage_quit;
    }
    // Parse the actual diff number
    {
        // Line number is wrong here. I don;t are.
        ParserSmCtx new_ctx = *ctx;
        new_ctx.buf = diffnumber;
        new_ctx.buf_ptr = new_ctx.buf.data;
        new_ctx.buf_end = new_ctx.buf.data + new_ctx.buf.len;
        s64 thenumber;
        valid = parser_sm_parse_s64(&new_ctx, &thenumber, 10);
        if (!valid) {
            parser_sm_report_error_s(&new_ctx, "chart diffnumber is not a number\n");
            goto rage_quit;
        }
        the_actual_diffnumber = (s32)thenumber;
    }
    parser_sm_skip_space(ctx);
    parser_sm_skip_comment(ctx);
    parser_sm_expect_newline_or_eof(ctx);

    // Get the groove radar - we ignore it
    grooveradar = {};
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_find_string_ending_with_char(ctx, ':', false, true, &grooveradar);
    if (!valid) {
        parser_sm_report_error_s(ctx, "Couldn't get chart grooveradar\n");
        goto rage_quit;
    }
    // valid = grooveradar.len > 0;
    // if (!valid) {
    //     parser_sm_report_error_s(ctx, "chart grooveradar is empty\n");
    //     goto rage_quit;
    // }
    parser_sm_skip_space(ctx);
    parser_sm_skip_comment(ctx);
    parser_sm_expect_newline_or_eof(ctx);

    // Make sure chart type is valid.
    for (s32 i = 0; i < ARRAY_COUNT(parser_sm_chart_types_string_table); i++) {
        if (chart_type == parser_sm_chart_types_string_table[i]) {
            // We found our chart type
            chart_type_number = i;
            // This way we don't need to malloc the new string.
            chart_type = parser_sm_chart_types_string_table[i];
            break;
        }
    }

    valid = chart_type_number != PARSER_SM_CHART_TYPE_UNKNOWN;
    if (!valid) {
        parser_sm_report_error_v(ctx, "Unknown chart type: '%.*s'\n",
                                 (int)chart_type.len, chart_type.data);
        goto rage_quit;
    }

    columns = parser_sm_chart_types_columns_table[chart_type_number];
    ASSERT(columns > 0);
    // This actually may fail. The file might say it is dance-double but it might have 7 columns. Or it might say it is kb7-single but it has 8 column... So I'm gonna read this first line to determine if it is right... I'm so angry at this shit.

    // Maybe I don't want to fix this shit. Maybe I should ignore the files..
    // Fix this shit
    columns_to_read = columns;
    {
        parser_sm_skip_spaces_comments_and_newlines(ctx);
        u8 *shitstart = ctx->buf_ptr;
        u8 *shitpointer = shitstart;
        u8 *maxshit = ctx->buf_end;
        // see how many valid notes are here
        while (shitpointer < maxshit) {
            // if (parser_sm_char_to_note_table[*shitpointer] == PARSER_SM_NOTE_UNKNOWN)
            // if (!PARSER_SM_NOTE_IS_VALID(parser_sm_char_to_note_table[*shitpointer]))
            if (!PARSER_SM_NOTE_IS_VALID(PARSER_SM_CHAR_TO_NOTE(*shitpointer)))
                break;
            shitpointer++;
        }
        s32 newshit = (s32)(shitpointer - shitstart);
        ASSERT(newshit > 0);
        if (newshit != columns) {
            columns_to_read = newshit;
        }
    }

    valid = columns <= PARSER_SM_MAX_COLUMNS;
    if (!valid) {
        parser_sm_report_error_v(ctx, "Too many columns! '%d'\n", columns)
        goto rage_quit;
    }
    valid = columns_to_read >= columns;
    if (!valid) {
        parser_sm_report_error_v(ctx, "Columns to read is less than columns! ctr: '%d', c: '%d'\n", columns_to_read, columns)
        goto rage_quit;
    }

    // declarations were here... stupid c++
    notes_pitch = NOTES_GET_PITCH_SIZE(columns);

    // this will be enough
    temp_divisions_max_size = ((s32)ctx->buf.len / columns_to_read);
    temp_divisions_count = 0;
    temp_divisions = arena_push_struct_count(ctx->temp_arena, s16, temp_divisions_max_size, arena_flags_zero());

    temp_notes_buffer_max_size = ((s32)ctx->buf.len / columns_to_read)*notes_pitch;
    temp_notes_buffer_count = 0;
    temp_notes_buffer = (u8 *)arena_push_size(ctx->temp_arena, temp_notes_buffer_max_size, arena_flags_zero());

    total_number_of_rows = 0;
    number_of_blank_rows = 0;

    // Get the notes now!

    parser_sm_skip_spaces_comments_and_newlines(ctx);
    s32 measure_count;
    for (measure_count = 0; parser_sm_in_bounds(ctx, 0); measure_count++) {
        // parse this measure
        // expect comma
        s32 division_count = 0;
        for (division_count = 0; parser_sm_in_bounds(ctx, 0); division_count++) {
            valid = division_count < PARSER_SM_MAX_DIVISIONS;
            if (!valid) {
                parser_sm_report_error_s(ctx, "Too many divisoins\n");
                goto rage_quit;
            }
            if (parser_sm_peek_char(ctx, 0, ',')) {
                // Measure ended lets stop getting the divisions.
                break;
            }
            // Get (columns) bytes and parse them.
            // Make sure we can read the columns
            valid = ctx->buf_ptr + columns_to_read < ctx->buf_end;
            if (!valid) {
                goto rage_quit;
            }
            ASSERT_SLOW(temp_notes_buffer_count + notes_pitch <= temp_notes_buffer_max_size);
            u8 *this_row = temp_notes_buffer + temp_notes_buffer_count;
            temp_notes_buffer_count += notes_pitch;
            u8 *ptr = ctx->buf_ptr;
            s32 blanks_count = 0;
            s32 idx_column;
            for (idx_column = 0; idx_column < columns; idx_column++, ptr++) {
                // u8 this_note = parser_sm_char_to_note_table[*ptr];
                u8 this_note = PARSER_SM_CHAR_TO_NOTE(*ptr);
                valid = PARSER_SM_NOTE_IS_VALID(this_note);
                if (!valid) {
                    parser_sm_report_error_v(ctx, "Unknown note: '%c'!\n", *ptr);
                    goto rage_quit;
                }
                if (this_note == PARSER_SM_NOTE_BLANK)
                    blanks_count++;
                PARSER_SM_NOTES_SET_COLUMN(this_row, idx_column, this_note);
                ASSERT_SLOW(this_note == PARSER_SM_NOTES_GET_COLUMN(this_row, idx_column));
            }
            // Skip one column for 7k. Weird that that is a thing.
            while (idx_column < columns_to_read) {
                idx_column++;
                ptr++;
            }
            if (blanks_count == columns) {
                number_of_blank_rows++;
            }
#if NOTES_COLUMN_SIZE == 4
            // Not really necessary, but...
            if (columns % 2 == 1) {
                PARSER_SM_NOTES_SET_COLUMN(this_row, columns, 0);
            }
#endif
            ctx->buf_ptr += columns_to_read;
            ASSERT_SLOW(ctx->buf_ptr == ptr);
            parser_sm_skip_spaces_comments_and_newlines(ctx);
        }
        valid = parser_sm_expect_char(ctx, ',');
        if (!valid) {
            valid = !parser_sm_in_bounds(ctx, 0);
            if (!valid) {
                parser_sm_report_error_s(ctx, "Didn't get comma but shomething else!!\n");
                goto rage_quit;
            }
        }
        if (division_count == 0) {
            parser_sm_report_error_s(ctx, "Zero divisions on the chart!!!\n");
            goto rage_quit;
        } else if (division_count % 4 != 0) {
            parser_sm_report_error_v(ctx, "Divisions not a multiple of 4!! It is %d\n",
                                     division_count);
            goto rage_quit;
        }
        // Put the values we just parsed into the temporary buffers
        temp_divisions[temp_divisions_count++] = (s16)division_count;
        total_number_of_rows += division_count;
        // temp_notes_buffer already added

        parser_sm_skip_spaces_comments_and_newlines(ctx);
    }
    valid = !parser_sm_in_bounds(ctx, 0);
    if (!valid) {
        parser_sm_report_error_s(ctx, "i dont know what od do wanymore\n");
        goto rage_quit;
    }
    valid = measure_count > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "NOTES has zero measures!\n");
        goto rage_quit;
    }

    valid = number_of_blank_rows < total_number_of_rows;
    if (!valid) {
        parser_sm_report_error_s(ctx, "NOTES has only blank rows!!\n");
        goto rage_quit;
    }

    ASSERT(temp_divisions_count == measure_count);
    // We parsed the notes correctly!!

    // Copy into the diff
    // ParserSmDiff final_diff = {}; // declared at the beginning
    final_diff.chart_type = (u16)chart_type_number;
    // final_diff.chart_type_string = copy_string(chart_type);
    final_diff.chart_type_string = chart_type; // It's a constant string.
    final_diff.description = arena_push_string(ctx->final_arena, desc);
    final_diff.diff_name = arena_push_string(ctx->final_arena, diffname);
    final_diff.diff_number = the_actual_diffnumber;


    final_diff.notes.columns = columns;
    final_diff.notes.pitch = notes_pitch;
    final_diff.notes.measures_count = measure_count;
    final_diff.notes.blank_rows = number_of_blank_rows;
    final_diff.notes.total_rows_count = total_number_of_rows;

    // Make the pointers from the raw_data
    {
        smem final_ptrs_size = measure_count * ssizeof(void *);
        smem final_divs_size = measure_count * SSIZEOF_VAL(final_diff.notes.divisions);
        smem final_notes_buffer_size = temp_notes_buffer_count * SSIZEOF_VAL(temp_notes_buffer);
        smem final_total_size = final_ptrs_size + final_divs_size + final_notes_buffer_size;
        final_diff.notes_raw_data_size = final_total_size;
        final_notes_raw_data = arena_push_size(ctx->final_arena, final_total_size);
        final_diff.notes_raw_data = final_notes_raw_data;

        // declaration was here before
        u8 *raw_data_ptr = (u8 *)final_diff.notes_raw_data;
        // // raw_data_ptr is aligned to malloc alignment now
        // raw_data_ptr is aligned to default arena alignment now TODO: arena custom alignment

        final_diff.notes.notes_by_measure = (u8 **)raw_data_ptr;
        raw_data_ptr += final_ptrs_size;
        // raw_data_ptr is now aligned to ssizeof(void *)

        final_diff.notes.divisions = (s16 *)raw_data_ptr;
        raw_data_ptr += final_divs_size;
        // raw_data_ptr is now aligned to SSIZEOF_VAL(notes.divisions) 2 bytes

        final_diff.notes.notes = raw_data_ptr;
        raw_data_ptr += final_notes_buffer_size;
        // raw_data_ptr is now aligned to 1 byte

        ASSERT_SLOW(raw_data_ptr == (u8 *)final_diff.notes_raw_data + final_diff.notes_raw_data_size);
    }

    // Copy the divisions
    // Just to make sure I didn't type the type wrong
    ASSERT_SLOW(SSIZEOF_VAL(final_diff.notes.divisions) == SSIZEOF_VAL(temp_divisions));
    ASSERT_SLOW(final_diff.notes.measures_count == temp_divisions_count);
    xmemcpy(final_diff.notes.divisions, temp_divisions,
            final_diff.notes.measures_count * SSIZEOF_VAL(final_diff.notes.divisions));

    // Copy the actual notes
    ASSERT_SLOW(total_number_of_rows * final_diff.notes.pitch == temp_notes_buffer_count);
    xmemcpy(final_diff.notes.notes, temp_notes_buffer,
            total_number_of_rows * final_diff.notes.pitch);
    // Now assign the values to the pointers
    {
        u8 *row_ptr = final_diff.notes.notes;
        for (s32 idx_measure = 0;
             idx_measure < final_diff.notes.measures_count;
             idx_measure++) {
            final_diff.notes.notes_by_measure[idx_measure] = (u8 *)row_ptr;
            row_ptr += final_diff.notes.pitch * final_diff.notes.divisions[idx_measure];
        }
    }

    smfile->diffs[smfile->diffs_count++] = final_diff;

    // Free temporary stuff.

    PARSER_SM_INFO("parser_sm_parse_notes temp size: %d\n", (s32)(temp_mem.arena->used - temp_mem.used));
    end_temp_memory_dont_free(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);

    // TEST_TIMER_X_END(1);
    return true;
rage_quit:
    // Free stuff
#if !TEST_PASM_USE_ARENAS
    if (final_notes_raw_data != NULL) {
        xfree(final_notes_raw_data);
        final_notes_raw_data = NULL;
        final_diff.notes_raw_data = NULL;
    }
    if (final_diff.chart_type_string.data != NULL)
        free_string(&final_diff.chart_type_string);
    if (final_diff.description.data != NULL)
        free_string(&final_diff.description);
    if (final_diff.diff_name.data != NULL)
        free_string(&final_diff.diff_name);
#else
    end_temp_memory(&temp_for_final_if_error);
#endif

    end_temp_memory(&temp_mem);
    return false;
}

static s32
parser_sm_key(ParserSmCtx *ctx)
{
    auto const str_trim_whitespace = [](String str){
        smem i = 0;
        while (i < str.len && parser_sm_is_whitespace(str.data[i]))
            i += 1;
        smem j = str.len;
        if (i < str.len) {
            while (j > i+1 && parser_sm_is_whitespace(str.data[j-1]))
                j -= 1;
        }
        return (String)(((Slice<u8>)str).slice(i, j));
    };
    bool valid = true;
    parser_sm_skip_spaces_comments_and_newlines(ctx);
    valid = parser_sm_in_bounds(ctx, 0);
    if (!valid)
        return PARSER_SM_ERR_DONE;

    valid = parser_sm_expect_char(ctx, '#');
    if (!valid) {
        parser_sm_report_error_s(ctx, "expected '#'\n");
        return PARSER_SM_ERR_FAILED;
    }

    String key = {};
    valid = parser_sm_find_string_ending_with_char(ctx, ':', false, true, &key);
    if (!valid) {
        return PARSER_SM_ERR_FAILED;
    }
    key = str_trim_whitespace(key);
    valid = key.len > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "key is empty\n");
        return PARSER_SM_ERR_FAILED;
    }

    String val = {};
    valid = parser_sm_find_string_ending_with_char(ctx, ';', true, true, &val);
    if (!valid) {
        parser_sm_report_error_s(ctx, "couldn't parse the value\n");
        return PARSER_SM_ERR_FAILED;
    }
    val = str_trim_whitespace(val);
    // parser_sm_skip_comment(ctx);
    // valid = parser_sm_expect_newline_or_eof(ctx);
    // if (!valid) {
    //     parser_sm_report_error_s(ctx, "expected newline or semicolon!\n");
    //     return PARSER_SM_ERR_FAILED;
    // }
    ctx->curr_key = key;
    ctx->curr_val = val;
    return PARSER_SM_ERR_CONTINUE;
}

static bool
parser_sm_do_sanity_check(ParserSmCtx *ctx, ParserSmFile *file)
{
    ASSERT_SLOW(file != NULL);

    bool valid = true;

    // First make sure it has the basic strings.
    // I have some files that don't have title. Maybe I should generate a title for it..
    if (0) {
        valid = file->title.is_some();
        valid = valid && string_exists_and_is_not_empty(file->title.unwrap());
        if (!valid) {
            parser_sm_report_error_s(ctx, "File doesn't have title.\n");
            return false;
        }
    }

    // I have some files that don't have artist
    if (0) {
        valid = file->artist.is_some();
        valid = valid && string_exists_and_is_not_empty(file->artist.unwrap());
        if (!valid) {
            parser_sm_report_error_s(ctx, "File doesn't have artist.\n");
            return false;
        }
    }

    // Next, verify it has bpms
    valid = file->bpms.is_some();
    valid = valid && file->bpms.unwrap().data != NULL;
    valid = valid && file->bpms.unwrap().len > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "File doesn't have bpms.\n");
        // pack Initial J file Hot Hot Racing Car was hitting this
        // Reason is, the key previous to BPMS (GENRE) didn't have a semicolon,
        // so it thought #BPMS:...; was part of it and didn't parse the key
        // BPMS at all. I don't want to fix this shit. I'd rather ignore that file.
        return false;
    }

    // Verify the bpms are sorted: Already did that on parse_bpms

    // Next, verify it has at least one difficulty
    valid = file->diffs_count > 0;
    if (!valid) {
        parser_sm_report_error_s(ctx, "File doesn't have difficulties.\n");
        return false;
    }

    // There still might be some incompatibilities between this and Fileset but
    // we'll deal with those when we convert.

    // Verify the difficulties are all ok: Already did that on parse_notes
    return true;
}

static bool
parser_sm_parse_file_from_buffer(MemoryArena *final_arena, MemoryArena *temp_arena,
                                 String filename, String buf, bool report_errors,
                                 ParserSmFile *result)
{
    ASSERT_SLOW(temp_arena->base);
    // ASSERT(ctx != NULL);
    ASSERT_SLOW(string_exists_and_is_not_empty(filename));
    ASSERT_SLOW(buf.data != NULL);
    ASSERT_SLOW(buf.len > 0);
    ASSERT_SLOW(result != NULL);
    ParserSmCtx context = {};
    ParserSmCtx *ctx = &context; // too lazy to rename it
    zero_struct(result);
    ctx->report_errors = report_errors;
    ctx->filename = filename;
    ctx->buf = buf;
    ctx->buf_ptr = buf.data;
    ctx->buf_end = buf.data + buf.len;
    ctx->line1 = 1;
    ctx->final_arena = final_arena;
    ctx->temp_arena = temp_arena;
    TempMemory temp_mem = begin_temp_memory(ctx->temp_arena);
    TempMemory temp_for_final_if_error = begin_temp_memory(ctx->final_arena);

    result->filename = filename;

    // test for utf8 encoding magic
    {
        if (ctx->buf.len >= 3) {
            u8 utf8_order_mark[] = { 0xef, 0xbb, 0xbf };
            s32 i;
            for (i = 0; i < 3; i++) {
                if ((u8)ctx->buf.data[i] != utf8_order_mark[i])
                    break;
            }
            if (i == 3) {
                ctx->buf_ptr += 3;
            }
        }
    }

    bool valid = true;
    s32 err;
    for (s32 count_keys = 0;
         (err = parser_sm_key(ctx)) == PARSER_SM_ERR_CONTINUE;
         count_keys++) {
        ParserSmCtx val_ctx = *ctx;
        val_ctx.buf = ctx->curr_val;
        val_ctx.buf_ptr = val_ctx.buf.data;
        val_ctx.buf_end = val_ctx.buf.data + val_ctx.buf.len;
        bool sskv_found = false;
        for (s32 i = 0; i < ARRAY_COUNT(parser_sm_simple_strings); i++) {
            const ParserSmSimpleStringKeyValPair *sskv = &parser_sm_simple_strings[i];
            if (ctx->curr_key == sskv->key) {
                Maybe<String> *sskv_val = (Maybe<String> *)((u8 *)result + sskv->struct_offset);
                if (sskv_val->is_some()) {
                    // Already have that, error.
                    // i think this reports one line after the culprit.
                    parser_sm_report_error_v(ctx, "already has key '%.*s'\n", EXPAND_STR_FOR_PRINTF(sskv->key));
                    goto rage_quit;
                }
                // TODO: Currently, the final string includes the backslashes if there was a backslashed semicolon!!
                // TODO: Trim it
                *sskv_val = Some(arena_push_string(final_arena, ctx->curr_val));
                sskv_found = true;
                break;
            }
        }
        if (sskv_found) continue;
        if (ctx->curr_key == "OFFSET"_s) {
            if (result->offset.is_some()) {
                // Already have that, error.
                parser_sm_report_error_v(ctx, "already has key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            f64 v;
            valid = parser_sm_parse_f64(&val_ctx, &v);
            if (!valid) {
                parser_sm_report_error_v(&val_ctx, "failed parsing float of key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            result->offset = Some(v);
        } else if (ctx->curr_key == "SAMPLESTART"_s) {
            if (result->sample_start.is_some()) {
                // Already have that, error.
                parser_sm_report_error_v(ctx, "already has key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            f64 v;
            valid = parser_sm_parse_f64(&val_ctx, &v);
            if (!valid) {
                parser_sm_report_error_v(&val_ctx, "failed parsing float of key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            result->sample_start = Some(v);
        } else if (ctx->curr_key == "SAMPLELENGTH"_s) {
            if (result->sample_length.is_some()) {
                // Already have that, error.
                parser_sm_report_error_v(ctx, "already has key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            f64 v;
            valid = parser_sm_parse_f64(&val_ctx, &v);
            if (!valid) {
                parser_sm_report_error_v(&val_ctx, "failed parsing float of key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            result->sample_length = Some(v);
        } else if (ctx->curr_key == "DISPLAYBPM"_s) {
            if (result->display_bpm.is_some()) {
                // Already have that, error.
                parser_sm_report_error_v(ctx, "already has key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            ParserSmDisplayBpm v;
            valid = parser_sm_parse_display_bpm(&val_ctx, &v);
            if (!valid) {
                parser_sm_report_error_s(&val_ctx, "failed parsing displaybpm\n");
                goto rage_quit;
            }
            result->display_bpm = Some(v);
        } else if (ctx->curr_key == "BPMS"_s) {
            if (result->bpms.is_some()) {
                // Already have that, error.
                parser_sm_report_error_v(ctx, "already has key '%.*s'\n", EXPAND_STR_FOR_PRINTF(ctx->curr_key));
                goto rage_quit;
            }
            NewArrayDynamic<ParserSmBpm> v;
            valid = parser_sm_parse_bpms(&val_ctx, &v);
            if (!valid) {
                parser_sm_report_error_s(&val_ctx, "failed parsing bpms\n");
                goto rage_quit;
            }
            result->bpms = Some(v);
        } else if (ctx->curr_key == "NOTES"_s) {
            valid = parser_sm_parse_notes(&val_ctx, result);
            if (!valid) {
                parser_sm_report_error_s(&val_ctx, "failed parsing notes\n");
                goto rage_quit;
            }
        }
    }
    valid = err == PARSER_SM_ERR_DONE;
    if (!valid) {
        parser_sm_report_error_s(ctx, "shit key\n");
        goto rage_quit;
    }
    // Do some checking like if the number of difficulty == 0, failed
    // Verify bpms are sorted
    valid = parser_sm_do_sanity_check(ctx, result);
    if (!valid) {
        parser_sm_report_error_s(ctx, "File failed sanity check.\n");
        goto rage_quit;
    }
    end_temp_memory_dont_free(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    return true;
rage_quit:
    // free everything here
#if !TEST_PASM_USE_ARENAS
    parser_sm_free_file(result);
#else
    end_temp_memory(&temp_for_final_if_error);
#endif
    end_temp_memory(&temp_mem);
    return false;
}

// Functions to write the file back

// When you call this function, make sure buf has enough space.
static void
parser_sm_write_buf_print_row(s32 columns, u8 *row, u8 *buf)
{
    s32 i;
    for (i = 0; i < columns; i++) {
        u8 this_note = PARSER_SM_NOTES_GET_COLUMN(row, i);
        ASSERT(this_note != 0);
        buf[i] = parser_sm_note_to_char_table[this_note];
        ASSERT(buf[i] != 0);
    }
    buf[i] = 0;
}

static void
parser_sm_write_string(FILE *f, ParserSmFile *file, char *key, String val)
{
    fprintf(f, "#%s:%.*s;\n", key, (int)val.len, val.data);
}

#if 0
static void
parser_sm_write_f64_pair_count(FILE *f, ParserSmFile *file, char *key, int count, F64pair *fp)
{
    if (file->has_flags & has) {
        int i;
        fprintf(f, "#%s:", key);
        if (fp == NULL || count == 0) {
            fprintf(f, ";\n");
        } else {
            fprintf(f, "%.3f=%.3f\n", fp[0].e[0], fp[0].e[1]);
            for (i = 1; i < count; i++) {
                fprintf(f, ",%.3f=%.3f\n", fp[i].e[0], fp[i].e[1]);
            }
            fprintf(f, ";\n");
        }
    }
}
#endif

static void
parser_sm_write_bpms(FILE *f, ParserSmFile *file, char *key, smem count, ParserSmBpm *beats)
{
    int i;
    fprintf(f, "#%s:", key);
    if (beats == NULL || count == 0) {
        fprintf(f, ";\n");
    } else {
        fprintf(f, "%.3f=%.3f\n", beats[0].start_beat, 60.0*(f64)beats[0].bps);
        for (i = 1; i < count; i++) {
            fprintf(f, ",%.3f=%.3f\n", beats[i].start_beat, 60.0*(f64)beats[i].bps);
        }
        fprintf(f, ";\n");
    }
}

static void
parser_sm_write_f64(FILE *cfile, ParserSmFile *file, char *key, f64 f)
{
    fprintf(cfile, "#%s:%.3f;\n", key, f);
}

static void
parser_sm_write_display_bpm(FILE *f, ParserSmFile *file, char *key, ParserSmDisplayBpm display_bpm)
{
    fprintf(f, "#%s:", key);
    switch (display_bpm.type) {
    case PARSER_SM_DISPLAY_BPM_RANDOMLY_CHANGES: fprintf(f, "*;\n"); break;
    case PARSER_SM_DISPLAY_BPM_RANGE: fprintf(f, "%.3f:%.3f;\n", (f64)display_bpm.bpms[0], (f64)display_bpm.bpms[1]); break;
    case PARSER_SM_DISPLAY_BPM_STATIC: fprintf(f, "%.3f;\n", (f64)display_bpm.bpms[0]); break;
    default: ASSERT(false); break;
    }
}

static void
parser_sm_write_notes(FILE *f, ParserSmFile *file, ParserSmDiff *diff)
{
    (void)file;
    ASSERT(diff->chart_type == 1);
    fprintf(f, "//--------------- %.*s - %.*s ----------------\n",
            (int)diff->chart_type_string.len, diff->chart_type_string.data,
            (int)diff->description.len, diff->description.data);
    fprintf(f, "#NOTES:\n");
    fprintf(f, "     %.*s:\n", (int)diff->chart_type_string.len, diff->chart_type_string.data);
    fprintf(f, "     %.*s:\n", (int)diff->description.len, diff->description.data);
    fprintf(f, "     %.*s:\n", (int)diff->diff_name.len, diff->diff_name.data);
    fprintf(f, "     %d:\n", diff->diff_number);
    fprintf(f, "     0,0,0,0,0:\n");
    int i;
    int j;
    for (i = 0; i < diff->notes.measures_count; i++) {
        u8 *measure_row = diff->notes.notes_by_measure[i];
        for (j = 0; j < diff->notes.divisions[i]; j++) {
            u8 *this_row =  measure_row + j*diff->notes.pitch;
            ASSERT(diff->notes.columns < 100);
            u8 buf[diff->notes.columns+1];
            parser_sm_write_buf_print_row(diff->notes.columns, this_row, buf);
            fprintf(f, "%s\n", (char*)buf);
        }
        if (i != diff->notes.measures_count - 1)
            fprintf(f, ",\n");
        else
            fprintf(f, ";\n");
    }
}

static void
parser_sm_write_file(FILE *f, ParserSmFile *file)
{
    if (file->title.is_some())             parser_sm_write_string(f, file, "TITLE",            file->title.unwrap());
    if (file->subtitle.is_some())          parser_sm_write_string(f, file, "SUBTITLE",         file->subtitle.unwrap());
    if (file->artist.is_some())            parser_sm_write_string(f, file, "ARTIST",           file->artist.unwrap());
    if (file->title_translit.is_some())    parser_sm_write_string(f, file, "TITLETRANSLIT",    file->title_translit.unwrap());
    if (file->subtitle_translit.is_some()) parser_sm_write_string(f, file, "SUBTITLETRANSLIT", file->subtitle_translit.unwrap());
    if (file->artist_translit.is_some())   parser_sm_write_string(f, file, "ARTISTTRANSLIT",   file->artist_translit.unwrap());
    if (file->genre.is_some())             parser_sm_write_string(f, file, "GENRE",            file->genre.unwrap());
    if (file->credit.is_some())            parser_sm_write_string(f, file, "CREDIT",           file->credit.unwrap());
    if (file->music.is_some())             parser_sm_write_string(f, file, "MUSIC",            file->music.unwrap());
    if (file->banner.is_some())            parser_sm_write_string(f, file, "BANNER",           file->banner.unwrap());
    if (file->background.is_some())        parser_sm_write_string(f, file, "BACKGROUND",       file->background.unwrap());
    if (file->cdtitle.is_some())           parser_sm_write_string(f, file, "CDTITLE",          file->cdtitle.unwrap());

    if (file->offset.is_some())            parser_sm_write_f64(f, file, "OFFSET",              (f64)file->offset.unwrap());
    if (file->sample_start.is_some())      parser_sm_write_f64(f, file, "SAMPLESTART",         (f64)file->sample_start.unwrap());
    if (file->sample_length.is_some())     parser_sm_write_f64(f, file, "SAMPLELENGTH",        (f64)file->sample_length.unwrap());

    if (file->display_bpm.is_some()) parser_sm_write_display_bpm(f, file, "DISPLAYBPM", file->display_bpm.unwrap());

    if (file->bpms.is_some()) parser_sm_write_bpms(f, file, "BPMS", file->bpms.unwrap().len, file->bpms.unwrap().data);

    for (int i = 0; i < file->diffs_count; i++) {
        parser_sm_write_notes(f, file, &file->diffs[i]);
    }
}

// Functions to transform into a NewSong

static s32
parser_sm_find_beat_idx_by_beat(ParserSmFile *smfile, f64 beat, s32 beat_idx_start,
                                ParserSmBpm *maybe_ret_beat_info,
                                bool can_go_reverse)
{
    ASSERT(ABC(beat_idx_start, smfile->bpms.unwrap().len));
    s32 result = 0;
    bool found = false;
    bool go_reverse = false;
    ASSERT(smfile->bpms.is_some());
    const auto bpms = smfile->bpms.unwrap().as_slice();

    // At the end of the function, if (found) we'll write to the result
    // beat_info this beat_info here
    ParserSmBpm *bi = NULL;
    s32 i = 0;
    f64 thef64margin = 0.0001; // TODO: find a good value
    if (beat < 0.0) {
        found = true;
        result = 0;
        bi = &bpms.data[0];
    } else {
        for (i = beat_idx_start;
             i < bpms.len;
             i++) {
            bi = &bpms.data[i];

            if (f64_less_than_or_eq_margin(bi->start_beat, beat, thef64margin)) {
                f64 beat_end_beats = bi->start_beat + bi->active_beats;
                if (f64_greater_than_and_not_eq_margin(beat_end_beats, beat, thef64margin)) {
                    // Beat started before 'beat', is active
                    // What we want is this one.
                    found = true;
                    result = i;
                    break;
                } else {
                    // Beat started before 'beat', is not active.
                    // What we want is after this one.
                    continue;
                }
            } else {
                // Beat started after 'beat', is not active.
                // What we want is before this one.

                // If this hits, there either was an error on the if() or
                // beat_idx_start is too big.
                // If beat_idx_start is too big, let's make another loop
                // decrement i until we find it.
                // If this assert fails, the comparison in the if() msut be the problem
                ASSERT(i == beat_idx_start);
                go_reverse = true;
                break;
            }
        }
        if (go_reverse) {
            for (i--; i >= 0; i--) {
                bi = &bpms.data[i];
                if (f64_less_than_or_eq_margin(bi->start_beat, beat, thef64margin)) {
                    f64 beat_end_beats = bi->start_beat + bi->active_beats;
                    if (f64_greater_than_and_not_eq_margin(beat_end_beats, beat, thef64margin)) {
                        // Beat started before 'beat', is active
                        // What we want is this one.
                        found = true;
                        result = i;
                        break;
                    } else {
                        // Beat started before 'beat', is not active.
                        // What we want is after this one.
                        ASSERT(false);
                    }
                    ASSERT(found);
                } else {
                    // Beat started after 'beat', is not active.
                    // What we want is before this one.
                    continue;
                }
            }
        }
    }
    // if we ever hit this, the code that called this functions has en error
    ASSERT(can_go_reverse || !go_reverse);
    // if found == false, we hit the end, just confirming it
    ASSERT(found);
    ASSERT(bi != NULL);

    if (maybe_ret_beat_info != NULL) {
        *maybe_ret_beat_info = *bi;
    }

    return result;
}

static smem
parser_sm_get_final_fileset_memory_size(ParserSmFile *sm)
{
    smem result = 0;
    result += sm->filename.len + 1;

    align_forward(&result, 8);
    result += ssizeof(Diff) * sm->diffs_count;

    for (s32 idx_diff = 0; idx_diff < sm->diffs_count; idx_diff++) {
        align_forward(&result, 8);
        result += sm->filename.len + 1;

        auto *smdiff = &sm->diffs[idx_diff];
        result += sm->title.unwrap_or("(No Title)"_s).len + 1;
        result += sm->artist.unwrap_or("(No Artist)"_s).len + 1;
        result += sm->title_translit.unwrap_or({}).len + 1;
        result += sm->artist_translit.unwrap_or({}).len + 1;
        result += sm->credit.unwrap_or("(No Creator)"_s).len + 1;
        result += smdiff->diff_name.len + 1;
        {
            u8 buf[512];
            String tmp = {};
            tmp.data = buf;
            tmp.len = snprintf((char *)buf, ssizeof(buf), "%.*s sm %d",
                               EXPAND_STR_FOR_PRINTF(smdiff->diff_name),
                               smdiff->diff_number);
            result += tmp.len + 1;
        }

        align_forward(&result, 16);
        result += ssizeof(BeatInfo)*sm->bpms.unwrap().len;

        s32 rows_count = smdiff->notes.total_rows_count - smdiff->notes.blank_rows;
        align_forward(&result, 16);
        result += ssizeof(NoteTimestamp) * rows_count;

        s32 notes_pitch = NOTES_GET_PITCH_SIZE(smdiff->notes.columns);
        align_forward(&result, 16);
        result += ssizeof(NoteType) * rows_count * notes_pitch;

        align_forward(&result, 16);
        result += ssizeof(b8) * smdiff->notes.columns;
    }

    return result;
}

// @Malloc
static bool
parser_sm_to_fileset(MemoryArena *final_arena, MemoryArena *temp_arena,
                     ParserSmFile *sm, Fileset *ret_fileset)
{
    ASSERT(sm->bpms.is_some() && sm->bpms.unwrap().len > 0);
    ASSERT(sm->diffs_count > 0);

    TempMemory temp_mem = begin_temp_memory(temp_arena);

    Fileset fileset = {};
#if FILESET_USE_ARENAS
    smem final_fileset_memory_size = parser_sm_get_final_fileset_memory_size(sm);
    // printf("final_fileset_memory_size: %ld\n", final_fileset_memory_size);
    void *final_fileset_memory = xmalloc(final_fileset_memory_size);
    fileset.arena = {};
    fileset.arena.base = (u8*)final_fileset_memory;
    fileset.arena.size = final_fileset_memory_size;
#endif
    // fileset.hash = sm->entire_file_hash;
    fileset.parser_type = ParserType_SM;
#if !FILESET_USE_ARENAS
    fileset.filepath = copy_string(sm->filename); //@FilesetMalloc
#else
    fileset.filepath = arena_push_string_nullter(&fileset.arena, sm->filename); //@FilesetMalloc
#endif

    // Start by copying metadata
    // @Malloc
    Metadata temp_metadata = {};
    temp_metadata.title = sm->title.unwrap_or("(No Title)"_s);
    temp_metadata.artist = sm->artist.unwrap_or("(No Artist)"_s);
    temp_metadata.title_translit = sm->title_translit.unwrap_or({});
    temp_metadata.artist_translit = sm->artist_translit.unwrap_or({});

    // Copy the bpms. Needs transformation from start_seconds to ts_start
    Timings temp_new_timings = {};
    const auto sm_bpms = sm->bpms.unwrap().as_slice();
    temp_new_timings.bpms_count = (s32)sm_bpms.len;
    temp_new_timings.bpms = arena_push_struct_count(temp_arena, BeatInfo, temp_new_timings.bpms_count);
    for (s32 i = 0; i < sm_bpms.len; i++) {
        ParserSmBpm *sm_bpm = &sm_bpms.data[i];
        BeatInfo *new_bpm = &temp_new_timings.bpms[i];
        // @IgnoreSomeBitsOfTheMantissa
        new_bpm->bps = sm_bpm->bps;
        // @IgnoreSomeBitsOfTheMantissa
        new_bpm->ts_start = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(sm_bpm->start_seconds)};
        if (i + 1 < sm_bpms.len) {
            // @IgnoreSomeBitsOfTheMantissa
            new_bpm->ts_end = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(sm_bpm->start_seconds
                                                           + sm_bpm->active_seconds)};
        } else {
            new_bpm->ts_end = NOTE_MAX_TIMESTAMP;
        }
        ASSERT(new_bpm->ts_start.value >= 0);
        ASSERT(new_bpm->ts_end > new_bpm->ts_start);
    }
    fileset.diffs_count = sm->diffs_count;
#if !FILESET_USE_ARENAS
    fileset.diffs = xmalloc_struct_count(Diff, fileset.diffs_count, xmalloc_flags_zero()); // @FilesetMalloc
#else
    fileset.diffs = arena_push_struct_count(&fileset.arena, Diff, fileset.diffs_count, arena_flags_zero()); // @FilesetMalloc
#endif

#if 0
    // temp_hold_idx_start[column].data[hold_idx]
    s32 temp_holds_max_columns = PARSER_SM_MAX_COLUMNS;
    Array(s32) *temp_hold_idx_start = xmalloc(temp_holds_max_columns * SSIZEOF_VAL(temp_hold_idx_start));
    zero_struct_count(temp_hold_idx_start, temp_holds_max_columns);
    Array(s32) *temp_hold_idx_end = xmalloc(temp_holds_max_columns * SSIZEOF_VAL(temp_hold_idx_end));
    zero_struct_count(temp_hold_idx_end, temp_holds_max_columns);
#endif


    for (s32 idx_diff = 0; idx_diff < sm->diffs_count; idx_diff++) {
        // For each diff, remove the blank columns, calculate the timestamps of the notes, find hold pairs
        ParserSmDiff *smdiff = &sm->diffs[idx_diff];
        Diff *newdiff = &fileset.diffs[idx_diff];
        // newdiff->parent_fileset = ret_fileset;
        if (sm->music.is_some()) {
            if (get_file_extension(sm->music.unwrap()) == "mp3"_s) {
                MemoryArena debug_arena_for_mp3name = {}; // @Leak
                init_arena(&debug_arena_for_mp3name, 4096, 0, false);
                newdiff->debug_mp3_filename = join_paths_nullter(&debug_arena_for_mp3name, get_base_dir(sm->filename), sm->music.unwrap()); // @Leak @FilesetMalloc
                newdiff->debug_mp3_offset = sm->offset.unwrap_or(0);
            }
        }
#if !FILESET_USE_ARENAS
        newdiff->filepath = copy_string(fileset.filepath); // @FilesetMalloc
        copy_metadata_malloc(&temp_metadata, &newdiff->metadata); // @FilesetMalloc
        newdiff->metadata.creator = copy_string(sm->credit.unwrap_or("(No Creator)"_s)); // @FilesetMalloc
        newdiff->metadata.diff_name = copy_string(smdiff->diff_name); // @FilesetMalloc
        {
            u8 buf[512];
            String tmp = {};
            tmp.data = buf;
            tmp.len = snprintf((char *)buf, ssizeof(buf), "%.*s sm %d",
                               EXPAND_STR_FOR_PRINTF(smdiff->diff_name),
                               smdiff->diff_number);
            newdiff->metadata.diff_name = copy_string(tmp); // @FilesetMalloc
        }
#else
        newdiff->filepath = arena_push_string_nullter(&fileset.arena, fileset.filepath);
        copy_metadata_malloc(&temp_metadata, &newdiff->metadata);
        newdiff->metadata.creator = arena_push_string_nullter(&fileset.arena, sm->credit.or_default("(No Creator)"_s));
        newdiff->metadata.diff_name = arena_push_string_nullter(&fileset.arena, smdiff->diff_name);
        {
            u8 buf[512];
            String tmp = {};
            tmp.data = buf;
            tmp.len = snprintf((char *)buf, ssizeof(buf), "%.*s sm %d",
                               EXPAND_STR_FOR_PRINTF(smdiff->diff_name),
                               smdiff->diff_number);
            newdiff->metadata.diff_name = arena_push_string_nullter(&fileset.arena, tmp);
        }
#endif
        newdiff->metadata.diff_idx = idx_diff;

        zero_struct(&newdiff->notes);
#if !FILESET_USE_ARENAS
        newdiff->notes.timings = copy_timings(&temp_new_timings); // @FilesetMalloc
#else
        newdiff->notes.timings = copy_timings_into_arena(&fileset.arena, &temp_new_timings);
#endif
        newdiff->notes.keymode = parser_sm_chart_types_keymode_table[smdiff->chart_type];
        ASSERT(keymode_is_valid(newdiff->notes.keymode));
        newdiff->notes.columns = smdiff->notes.columns;
        ASSERT(newdiff->notes.columns == keymode_to_columns_table[newdiff->notes.keymode]);
        newdiff->notes.pitch = NOTES_GET_PITCH_SIZE(newdiff->notes.columns);
        newdiff->notes.rows_count = smdiff->notes.total_rows_count - smdiff->notes.blank_rows;
        ASSERT(newdiff->notes.rows_count > 0);

#if 0
        // Clear the temp buffers
        for (s32 i = 0; i < newdiff->notes.columns; i++) {
            temp_hold_idx_start[i].len = 0;
            temp_hold_idx_end[i].len = 0;
        }
#endif
#if !FILESET_USE_ARENAS
        newdiff->notes.timestamps =
            xmalloc_struct_count(NoteTimestamp, newdiff->notes.rows_count, xmalloc_flags_zero()); // @FilesetMalloc
        s32 total_raw_notes_size = ssizeof(NoteType) * newdiff->notes.rows_count * newdiff->notes.pitch;
        newdiff->notes.raw_notes = (NoteType *)xmalloc(total_raw_notes_size, xmalloc_flags_zero()); // @FilesetMalloc

        newdiff->notes.last_hold_is_released = xmalloc_struct_count(b8, newdiff->notes.columns, xmalloc_flags_zero()); // @FilesetMalloc
#else
        newdiff->notes.timestamps =
            arena_push_struct_count(&fileset.arena, NoteTimestamp, newdiff->notes.rows_count, arena_flags_zero()); // @FilesetMalloc
        s32 total_raw_notes_size = ssizeof(NoteType) * newdiff->notes.rows_count * newdiff->notes.pitch;
        STATIC_ASSERT(ssizeof(NoteType) == 1);
        newdiff->notes.raw_notes = (NoteType *)arena_push_size(&fileset.arena, total_raw_notes_size, arena_flags_zero()); // @FilesetMalloc

        newdiff->notes.last_hold_is_released = arena_push_struct_count(&fileset.arena, b8, newdiff->notes.columns, arena_flags_zero()); // @FilesetMalloc
#endif
#if 0
        newdiff->notes.hold_pairs_count = xmalloc_struct_count(s32, newdiff->notes.columns, xmalloc_flags_zero());
        newdiff->notes.hold_idx_start   = xmalloc_struct_count(s32, newdiff->notes.columns, xmalloc_flags_zero());
        newdiff->notes.hold_idx_end     = xmalloc_struct_count(s32, newdiff->notes.columns, xmalloc_flags_zero());
        // I still have to make the actual hold pairs.
#endif

        NoteType *raw_notes_ptr = newdiff->notes.raw_notes;
        s32 out_row_idx = 0;

        NoteType last_newnote[PARSER_SM_MAX_COLUMNS] = {};
        b8 inside_a_hold[PARSER_SM_MAX_COLUMNS] = {}; // a bool
        b8 inside_a_roll[PARSER_SM_MAX_COLUMNS] = {}; // a bool

        // @Leak everywhere I goto rage_quit in this function

        f64 last_beat_in_the_song = sm_bpms.data[sm_bpms.len-1].start_beat;
        f64 last_time_from_song_start = -F64_MAX;
        s32 bidx = 0;
        for (s32 idx_measure = 0;
             idx_measure < smdiff->notes.measures_count;
             idx_measure++) {
            s32 num_divs = smdiff->notes.divisions[idx_measure];
            s32 num_divs_divby4 = num_divs / 4;
            ASSERT_SLOW(num_divs % 4 == 0);
            ASSERT_SLOW(num_divs != 0);
            u8 *row_at_measure = smdiff->notes.notes_by_measure[idx_measure];
            bool recalc_beat_info = false;
            s32 measure_start_beat = 4 * idx_measure;
            ParserSmBpm bi;
            s32 thelastbidxmeasure = bidx;
            bidx = parser_sm_find_beat_idx_by_beat(sm, (f64)measure_start_beat, thelastbidxmeasure, &bi, true);
            ASSERT_SLOW(bidx >= thelastbidxmeasure);
            ASSERT_SLOW(bidx >= 0);
            ASSERT_SLOW(bi.start_beat >= 0.0);
            f64 beats_from_beat_info_start_to_measure_start =
                (f64)measure_start_beat - bi.start_beat;
            ASSERT_SLOW(beats_from_beat_info_start_to_measure_start >= 0.0);
            f64 time_from_song_start_to_measure_start =
                bi.start_seconds +
                ((beats_from_beat_info_start_to_measure_start)/(f64)bi.bps);


            f64 beat_snap_ratio_to_add = 1.0 / (f64)num_divs_divby4;
            // This gets incremented before it gets used.
            f64 beats_from_measure_start = -beat_snap_ratio_to_add;
            // Each measure has 4 beats
            // They are sure to be initialized. on the first time of the loop
            s32 next_bidx;
            s32 next_bidx_by_measure;
            for (s32 integer_beats_from_measure_start = 0;
                 integer_beats_from_measure_start < 4;
                 integer_beats_from_measure_start++) {
                s32 last_bidx = bidx;
                s32 integer_beat = measure_start_beat + integer_beats_from_measure_start;
                bidx = parser_sm_find_beat_idx_by_beat(sm, (f64)integer_beat,
                                                       last_bidx, &bi, false);
                // Redundant check because the function was called with the
                // same arguments when this is true.
                if (integer_beats_from_measure_start == 0)
                    ASSERT_SLOW(bidx == last_bidx);
                ASSERT_SLOW(bidx >= 0);
                // Make sure we didn't miss any bpm.
                // Actually it is possible that this might fail.
                // Suppose we have a measure with 4 divisions.
                // We'll only search for a next_bidx on an integer beat.
                // But if there's a new bpm at 21.5 for example,
                // when we test for recalc, beat is 20.0, we don't do it 
                // next time it's 21, we don't do it.
                // next time (here) it's 22 but we didn't do the recalc before!
                // ASSERT(bidx == last_bidx || bidx == last_bidx + 1);

                // s32 next_bidx_by_measure;
                // This is the bidx we would have gotten on the next
                // integer_beats_from_measure_start loop. (maybe on the next idx_measure)
                if ((f64)(integer_beat + 1) < last_beat_in_the_song) {
                    // If this is not the last.
                    next_bidx_by_measure =
                        parser_sm_find_beat_idx_by_beat(sm, (f64)(integer_beat + 1),
                                                        bidx, NULL, false);
                } else {
                    next_bidx_by_measure =
                        parser_sm_find_beat_idx_by_beat(sm, last_beat_in_the_song,
                                                        bidx, NULL, false);
                    // If this is the last just pretend that's the next.
                    // next_bidx_by_measure = bidx;
                }

                // next_bi only gets used if recalc_beat_info is true
                ParserSmBpm next_bi;

                // Let's do it this way: We'll recalculate if the beat that
                // starts at the next measure is not the same as this one.
                next_bidx = bidx;
                recalc_beat_info = true;
                if (next_bidx_by_measure > bidx) {
                    // We would have skipped a beat. This never hits if this is
                    // the last beat. We don't go over array bounds.
                    recalc_beat_info = true;
                    next_bidx = bidx + 1;
                }
                next_bi = sm_bpms.data[next_bidx];

                // This gets incremented before it gets used.
                f64 beat_snap_ratio = -beat_snap_ratio_to_add;
                f64 thef64margin = 0.0001;
                // Basically, idx_div loops from 0 to num_divs-1.
                for (s32 idx_div = integer_beats_from_measure_start*num_divs_divby4;
                     idx_div < (integer_beats_from_measure_start+1)*num_divs_divby4;
                     idx_div++) {
                    ASSERT_SLOW(idx_div < num_divs);
                    beat_snap_ratio += beat_snap_ratio_to_add;
                    beats_from_measure_start += beat_snap_ratio_to_add;
                    f64 beats_from_song_start = (beats_from_measure_start
                                                 + (f64)measure_start_beat);
                    ASSERT_SLOW(f64_greater_than_or_eq_margin(beats_from_song_start, bi.start_beat, thef64margin));

                    // Check if we are supposed to be using next_bi now.
                    // A loop because there could be two beats 
                    while (recalc_beat_info &&
                           bidx < next_bidx &&
                           bi.start_beat != next_bi.start_beat &&
                           f64_less_than_or_eq_margin(next_bi.start_beat, beats_from_song_start, thef64margin)) {
                        // Found it
                        ASSERT_SLOW(bidx != next_bidx);
                        bidx = next_bidx;
                        bi = next_bi;

                        // Now we have to check if there will be another beat.
                        // We won't go out of bounds here.
                        if (next_bidx_by_measure > bidx) {
                            recalc_beat_info = true;
                            next_bidx++;
                            next_bi = sm_bpms.data[next_bidx];
                        } else {
                            recalc_beat_info = false;
                        }
                    }

                    ASSERT_SLOW(bidx <= next_bidx);

                    u8 *this_row = row_at_measure + idx_div*smdiff->notes.pitch;
                    // blanks_from_col0 is the number of blanks starting from
                    // column 0, not necessarily the total number of blanks in
                    // this row. "0010" Would give blanks_from_col0 == 2, even though it has 3 blanks.
                    s32 blanks_from_col0;
                    for (blanks_from_col0 = 0;
                         blanks_from_col0 < smdiff->notes.columns;
                         blanks_from_col0++) {
                        u8 thenote = PARSER_SM_NOTES_GET_COLUMN(this_row, blanks_from_col0);
                        if (thenote != PARSER_SM_NOTE_BLANK)
                            break;
                    }

                    // If this row is full os blanks, skip it.
                    if (blanks_from_col0 == smdiff->notes.columns)
                        continue;


                    f64 beats_since_beat_info_start =
                        (f64)beats_from_measure_start + ((f64)measure_start_beat - bi.start_beat);
                    ASSERT_SLOW(beats_since_beat_info_start < bi.active_beats);
                    f64 time_from_song_start =
                        ((bi.start_seconds + ((f64)beats_since_beat_info_start / (f64)bi.bps)));
                    f64 time_diff = time_from_song_start - last_time_from_song_start;
                    // If this assert fails, there's a bug in our code.
                    ASSERT_SLOW(time_diff > 0.0);
                    last_time_from_song_start = time_from_song_start;

                    // @IgnoreSomeBitsOfTheMantissa
                    NoteTimestamp this_timestamp = {NOTE_TIMESTAMP_FROM_SECONDS__INTERNAL(time_from_song_start)};
                    // Make sure the time is not too big like 2 billion tenths of a millisecond.
                    if (this_timestamp > NOTE_MAX_TIMESTAMP) {
                        beat_log("[parser_sm] This file is way too long!\n");
                        goto rage_quit;
                    }

                    for (s32 idx_column = blanks_from_col0;
                         idx_column < smdiff->notes.columns;
                         idx_column++) {
                        NoteType last_newnote_val = last_newnote[idx_column];
                        u8 thenote = PARSER_SM_NOTES_GET_COLUMN(this_row, idx_column);
                        if (thenote == PARSER_SM_NOTE_BLANK)
                            blanks_from_col0++;
                        // Check if the hold is valid, something like that.
                        NoteType newnote = PARSER_SM_NOTE_TO_NEW_NOTE(thenote);
                        if (thenote == PARSER_SM_NOTE_HOLD_END && last_newnote_val == NOTE_SMROLL) {
                            newnote = NOTE_SMROLL_END;
                        }
                        NOTES_SET_COLUMN(raw_notes_ptr, idx_column, newnote);
                        // Do the hold pair thing
                        // If having a hold start, hold start, hold end is a
                        // valid thing, maybe I should rethink about this thing.
                        // If it's not valid I should be checking this on parse_notes, not here.
                        // Of 2500 files I didn't find a single one with a hold like that, so I'll just say it's invalid.
                        if (thenote == PARSER_SM_NOTE_HOLD) {
                            if (inside_a_hold[idx_column] || inside_a_roll[idx_column]) {
                                beat_log("[parser_sm] Hold starts in the middle of another one.\n");
                                goto rage_quit;
                            }
                            inside_a_hold[idx_column] = true;
                        } else if (thenote == PARSER_SM_NOTE_ROLL) {
                            if (inside_a_hold[idx_column] || inside_a_roll[idx_column]) {
                                beat_log("[parser_sm] Hold starts in the middle of another one.\n");
                                goto rage_quit;
                            }
                            inside_a_roll[idx_column] = true;
                        } else if (thenote == PARSER_SM_NOTE_HOLD_END) {
                            if (!((inside_a_hold[idx_column] && (last_newnote_val == NOTE_HOLD)) ||
                                  (inside_a_roll[idx_column] && (last_newnote_val == NOTE_SMROLL)))) {
                                beat_log("[parser_sm] There's a note inside the hold.\n");
                                ASSERT(false);
                                goto rage_quit;
                            }
                            inside_a_hold[idx_column] = false;
                            inside_a_roll[idx_column] = false;
                        }
                        if (thenote != PARSER_SM_NOTE_BLANK)
                            last_newnote[idx_column] = newnote;
                    }

                    // @IgnoreSomeBitsOfTheMantissa
                    newdiff->notes.timestamps[out_row_idx] = this_timestamp;
                    raw_notes_ptr += newdiff->notes.pitch;
                    out_row_idx++;
                }
            }
            // It's possible to fail this. With only 4 divisions. Same reson for the (bidx == lastbidx + 1) check before the loop
            // ASSERT(bidx == next_bidx || next_bidx == next_bidx_by_measure);
        }
        ASSERT(raw_notes_ptr == newdiff->notes.raw_notes + total_raw_notes_size);
        ASSERT(out_row_idx == newdiff->notes.rows_count);
        // Now verify the holds are right and then malloc on the original, copy them
        for (s32 idx_column = 0;
             idx_column < newdiff->notes.columns;
             idx_column++) {
#if 0
            Array(s32) *ptr_temp_start = &temp_hold_idx_start[idx_column];
            Array(s32) *ptr_temp_end   = &temp_hold_idx_end[idx_column];
            s32 pairs_count = newdiff->notes.hold_pairs_count[idx_column];
            s32 **ptr_start = &newdiff->notes.hold_idx_start[idx_column];
            s32 **ptr_end   = &newdiff->notes.hold_idx_end[idx_column];
            // This should always be right, kinda unnecessary
            ASSERT(pairs_count == ptr_temp_start->len);
            // If this fails, there's more hold starts than grabs excluding the
            // last one which is acceptable
            s32 diff_len_grab_release = (s32)ptr_temp_start->len - (s32)ptr_temp_end->len;
            ASSERT(diff_len_grab_release == 1 || diff_len_grab_release == 0);
            ASSERT(diff_len_grab_release == (inside_a_hold[idx_column]));


            // We're okay here so let's copy
            s32 size_to_copy = pairs_count * SSIZEOF_VAL(*ptr_start);
            *ptr_start = xmalloc(size_to_copy);
            xmemcpy(*ptr_start, ptr_temp_start->data, size_to_copy);
            // we do want the same size
            *ptr_end = xmalloc(size_to_copy - diff_len_grab_release);
            xmemcpy(*ptr_end, ptr_temp_end->data, size_to_copy - diff_len_grab_release);
#endif
            newdiff->notes.last_hold_is_released[idx_column] = inside_a_hold[idx_column];
        }
        // Now we're gonna make the hash 
        // String string_for_hash;
        // string_for_hash.len = ssizeof(NoteType) * newdiff->notes.rows_count * newdiff->notes.pitch;
        // string_for_hash.data = (u8 *)newdiff->notes.raw_notes;
        // u64 hash = make_hash_from_simple_string(string_for_hash);
        // newdiff->hash = hash;
        // printf("diff|%016lx|%s|%d\n", hash, sm->filename, idx_diff);
    }

#if FILESET_USE_ARENAS
    // printf("filset.arena.size-used: %ld\n", fileset.arena.size - fileset.arena.used);
#endif
    *ret_fileset = fileset;

    end_temp_memory(&temp_mem);

#if 0
    // Quick test. Will output all the notes in the format TIMESTAMP,NOTES
    // It works!
    for (s32 idx_diff = 0;
         idx_diff < ret_fileset->diffs_count;
         idx_diff++) {
        Diff *diff = &ret_fileset->diffs[idx_diff];
        Notes *notes = diff->notes;
        for (s32 idx_row = 0; idx_row < notes->rows_count; idx_row++) {
            u8 *row = notes->raw_notes + idx_row*notes->pitch;
            NoteTime ts = notes->timestamps[idx_row];
            printf("%05d,", ts);
            for (s32 idx_col = 0; idx_col < notes->columns; idx_col++) {
                u8 this_note = NOTES_GET_COLUMN(row, idx_col);
                u8 sm_note = this_note | PARSER_SM_NOTE_BLANK;
                ASSERT(PARSER_SM_NOTE_IS_VALID(sm_note));
                // quick hack to get the character
                printf("%c", parser_sm_note_to_char_table[sm_note]);
            }
            printf("\n");
        }
    }
#endif
    return true;
rage_quit:
    // Later make sure I free everything I need to.
    // Free the temp stuff
    end_temp_memory(&temp_mem);

#if !FILESET_USE_ARENAS
    free_fileset(&fileset);
#else
    xfree(fileset.arena.base);
#endif
    return false;
}

static bool
parser_sm_fileset_from_buffer(MemoryArena *final_arena, MemoryArena *temp_arena,
                              String filename, String buf, bool report_errors,
                              Fileset *ret_fileset)
{
    ASSERT_SLOW(temp_arena->base);
    // TODO: use Scoped_Temp_Arena
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    ParserSmFile sm_file;
    bool valid = true;

    // TODO: make sm_final_arena out of temp_arena and a fileset_final_arena
    // TODO: Check how much memory the thing uses compared to the file size and come up with a good constant for this.
    smem estimated_max_size = MAXIMUM(4*buf.len, KILOBYTES(256));
    MemoryArena sm_final_arena = sub_arena(temp_arena, estimated_max_size);
    ASSERT_SLOW(temp_arena->size - temp_arena->used > estimated_max_size);
    MemoryArena *sm_temp_arena = temp_arena;

    valid = parser_sm_parse_file_from_buffer(&sm_final_arena, sm_temp_arena, filename, buf, report_errors, &sm_file);
    if (!valid) {
        end_temp_memory(&temp_mem);
        return false;
    }

    valid = parser_sm_to_fileset(final_arena, temp_arena, &sm_file, ret_fileset);
#if !TEST_PASM_USE_ARENAS
    parser_sm_free_file(&sm_file);
#endif
    if (!valid) {
        end_temp_memory(&temp_mem);
        return false;
    }

    end_temp_memory(&temp_mem);
    return true;
}

static bool
parser_sm_fileset_from_file(MemoryArena *final_arena, MemoryArena *temp_arena,
                            String filename, bool report_errors, Fileset *ret_fileset)
{
    ASSERT_SLOW(temp_arena->base);
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    bool valid;
    EntireFile entire_file;
    valid = read_entire_file_nullter_into_arena(filename, temp_arena, &entire_file);
    if (!valid) {
        end_temp_memory(&temp_mem);
        return false;
    }

    valid = parser_sm_fileset_from_buffer(final_arena, temp_arena, filename, entire_file.buf, report_errors, ret_fileset);
    // free_entire_file(&entire_file); // commented because arenas
    if (!valid) {
        end_temp_memory(&temp_mem);
        return false;
    }

    end_temp_memory(&temp_mem);
    return true;
}
