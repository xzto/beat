#include "beat/generated/math_vectors.hh"

static f32
lerp(f32 a, f32 b, f32 t)
{
    f32 result = a + t*(b-a);
    return result;
}
static f64
lerp(f64 a, f64 b, f64 t)
{
    f64 result = a + t*(b-a);
    return result;
}

template<typename T>
static T
lerp_template_f32(T a, T b, f32 t)
{
    T result = a + t*(b-a);
    return result;
}

static f32
inverse_lerp(f32 a, f32 b, f32 x)
{
    // lerp(a, b, inverse_lerp(a, b, x)) == x;
    auto b_minus_a = b-a;
    f32 t = 0;
    if (b_minus_a != 0) {
        t = (x - a) / b_minus_a;
    }
    return t;
}
static f64
inverse_lerp(f64 a, f64 b, f64 x)
{
    // lerp(a, b, inverse_lerp(a, b, x)) == x;
    auto b_minus_a = b-a;
    f64 t = 0;
    if (b_minus_a != 0) {
        t = (x - a) / b_minus_a;
    }
    return t;
}

template<typename T>
static T
clamp(T x, T min, T max)
{
#if BEAT_SLOW
    ASSERT(min <= max);
#endif
    T result = x < min ? min : (x > max ? max : x);
    return result;
}

static f64
clamp01(f64 x)
{
    f64 result = x < 0 ? 0 : (x > 1 ? 1 : x);
    return result;
}
static f32
clamp01(f32 x)
{
    f32 result = x < 0 ? 0 : (x > 1 ? 1 : x);
    return result;
}


template<typename T>
static T
modmod(T x, T m)
{
    ASSERT(m != 0);
    T result = ((x % m) + m) % m;
    return result;
}

template<typename T>
static s32
sign_of(T x)
{
    return (x > 0) - (x < 0);
    // return x < 0 ? -1 : (x == 0 ? 0 : 1);
}

// TODO: Generate these.
static v2
minimum_v2(v2 a, v2 b) {
    return V2(
        MINIMUM(a.x, b.x),
        MINIMUM(a.y, b.y)
    );
}
static v2
maximum_v2(v2 a, v2 b) {
    return V2(
        MAXIMUM(a.x, b.x),
        MAXIMUM(a.y, b.y)
    );
}

static AabbPos aabb_canonicalize(AabbPos a) {
    return AabbPos{
        .min = minimum_v2(a.min, a.max),
        .max = maximum_v2(a.min, a.max),
    };
}

static QuadPos quad_from_aabb(AabbPos a) {
    return QuadPos{
        .pos = {
            V2(a.min.x, a.min.y),
            V2(a.max.x, a.min.y),
            V2(a.max.x, a.max.y),
            V2(a.min.x, a.max.y),
        },
    };
}

static bool aabb_intersects_aabb(AabbPos a, AabbPos b) {
    auto anew = aabb_canonicalize(a);
    auto bnew = aabb_canonicalize(b);
    v2 inter_min = maximum_v2(anew.min, bnew.min);
    v2 inter_max = minimum_v2(anew.max, bnew.max);
    v2 inter_size = inter_max - inter_min;
    return inter_size.x > 0 && inter_size.y > 0;
}

static Maybe<AabbPos> aabb_try_from_quad(QuadPos quad) {
    // TODO: Currently, when I call this function, the quad will probably be an aabb in the form of { bot_left, bot_right, top_right, top_left } however that might not always be the case.
    auto const dy01 = quad.pos[0].y - quad.pos[1].y;
    auto const dy23 = quad.pos[2].y - quad.pos[3].y;
    auto const dx03 = quad.pos[0].x - quad.pos[3].x;
    auto const dx12 = quad.pos[1].x - quad.pos[2].x;
    if (dy01 == 0 && dy23 == 0 && dx03 == 0 && dx12 == 0) {
        return Some(
            aabb_canonicalize(AabbPos{
                .min = quad.pos[0],
                .max = quad.pos[2],
            })
        );
    } else {
        return None;
    }
}

static bool quad_intersects_aabb(QuadPos quad, AabbPos aabb) {
    auto quad_as_aabb = aabb_try_from_quad(quad);
    if (quad_as_aabb.is_some()) {
        return aabb_intersects_aabb(quad_as_aabb.unwrap_unchecked(), aabb);
    } else {
        // TODO implement this.
        return true;
    }
}
