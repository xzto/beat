#ifndef BEAT_ATOMIC_H
#define BEAT_ATOMIC_H


template<typename T>
struct Atomic {
    // Can't do Atomic<T> atomic_var = some_T;
    // Can Atomic<T> atomic_var = {};
    // Can atomic_var = {};
    // If I delete the copy assignment operator, I can't do this:
    // struct Thing {
    //      Atomic<u32> some_atomic;
    // }
    // Thing *t = ...;
    // t = {};
    // So I'll keep it.

    constexpr Atomic() = default;
    constexpr Atomic(T v) : value(v) {}
    Atomic<T> &operator=(const T&) = delete;
    inline T load_nonatomic() const { return value; }
    inline void store_nonatomic(T v) { value = v; }
private:
    volatile T value;
};

#if BEAT_COMPILER_CLANG || BEAT_COMPILER_GCC

template<typename T>
static inline bool atomic_cas(Atomic<T> *value, T expected, T new_value) {
    return __sync_bool_compare_and_swap((T*)value, expected, new_value);
}

// returns old value
template<typename T>
static inline T atomic_increment(Atomic<T> *value) {
    return __sync_fetch_and_add((T*)value, 1);
}
template<typename T>
static inline T atomic_decrement(Atomic<T> *value) {
    return __sync_fetch_and_sub((T*)value, 1);
}
template<typename T>
static inline T atomic_add(Atomic<T> *value, T to_add) {
    return __sync_fetch_and_add((T*)value, to_add);
}
template<typename T>
static inline T atomic_sub(Atomic<T> *value, T to_sub) {
    return __sync_fetch_and_sub((T*)value, to_sub);
}

#else
# error "not implemented"
#endif

#include <atomic>


template<typename T>
static inline void atomic_store(Atomic<T> *value, T new_value, std::memory_order memory_order = std::memory_order_seq_cst) {
    STATIC_ASSERT(( sizeof(Atomic<T>) ==  sizeof(T)) && ( sizeof(T) ==  sizeof(std::atomic<T>)));
    STATIC_ASSERT((alignof(Atomic<T>) == alignof(T)) && (alignof(T) == alignof(std::atomic<T>)));
    std::atomic_store_explicit((std::atomic<T>*)value, new_value, memory_order);
}
template<typename T>
static inline T atomic_load(Atomic<T> *value, std::memory_order memory_order = std::memory_order_seq_cst) {
    STATIC_ASSERT(( sizeof(Atomic<T>) ==  sizeof(T)) && ( sizeof(T) ==  sizeof(std::atomic<T>)));
    STATIC_ASSERT((alignof(Atomic<T>) == alignof(T)) && (alignof(T) == alignof(std::atomic<T>)));
    return std::atomic_load_explicit((std::atomic<T>*)value, memory_order);
}

#endif
