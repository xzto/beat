#ifndef BEAT_PARSER_SM_H
#define BEAT_PARSER_SM_H

// TODO: Remove this macro later
#define TEST_PASM_USE_ARENAS 1


#define parser_sm_report_error_v(c, format, ...) do {        \
    if ((c)->report_errors) {                                \
        beat_log("[parser_sm] %.*s:%d: error: " format,      \
                 (int)(c)->filename.len, (c)->filename.data, \
                 (c)->line1,                                 \
                 __VA_ARGS__);                               \
    }                                                        \
} while (0);

#define parser_sm_report_error_s(c, format) do {             \
    if ((c)->report_errors) {                                \
        beat_log("[parser_sm] %.*s:%d: error: " format,      \
                 (int)(c)->filename.len, (c)->filename.data, \
                 (c)->line1);                                \
    }                                                        \
} while (0);

#define PARSER_SM_DO_REPORT_INFO 0


#if PARSER_SM_DO_REPORT_INFO
# define PARSER_SM_INFO(...)  do { \
    beat_printf(__VA_ARGS__);      \
} while (0)
#else
# define PARSER_SM_INFO(...) do{}while(0);
#endif

#if NOTES_COLUMN_SIZE == 4
# define PARSER_SM_NOTES_GET_COLUMN(row, column) \
    (u8)(((row)[(column)/2] >> (4*(column % 2))) & 15)

# define PARSER_SM_NOTES_GET_PITCH_SIZE(columns) (((columns) / 2) + (columns) % 2)
# define PARSER_SM_NOTES_SET_COLUMN(row, column, note_value)              \
{                                                               \
    (row)[(column)/2] &= (u8)(~(15 << (4*((column)%2))));         \
    (row)[(column)/2] |= (u8)((u32)(note_value) << (4*((column)%2))); \
}
#elif NOTES_COLUMN_SIZE == 8
# define PARSER_SM_NOTES_GET_COLUMN(row, column) (row)[column]
# define PARSER_SM_NOTES_GET_PITCH_SIZE(columns) (columns)
# define PARSER_SM_NOTES_SET_COLUMN(row, column, note_value) (row)[(column)] = (note_value)
#else
# error "NOTES_COLUMN_SIZE Unknown size"
#endif


// 2048 elements of s16, that's 4096 bytes.
// That's probably overkill for 99.9% of files.
// #define PARSER_SM_TEMP_DIVISIONS_INIT_SIZE 2048

// Some files will go over this. Not many.
// #define PARSER_SM_TEMP_NOTES_INIT_SIZE 16384

// 192nd beat for 4 beats, thats 768
#define PARSER_SM_MAX_DIVISIONS 768

#define PARSER_SM_MAX_COLUMNS 100

// Used for things like while(parse_f64_pairs() == PARSER_SM_ERR_CONTINUE) { ... }
enum {
    PARSER_SM_ERR_CONTINUE,
    PARSER_SM_ERR_DONE,
    PARSER_SM_ERR_FAILED,
};

enum {
    PARSER_SM_DISPLAY_BPM_STATIC,
    PARSER_SM_DISPLAY_BPM_RANGE,
    PARSER_SM_DISPLAY_BPM_RANDOMLY_CHANGES,
};

// typedef struct {
//     String key;
//     String value;
// } ParserSmKeyValuePair;

typedef struct {
    // Allocator allocator;
    // We'll malloc for now

    String buf;
    u8 *buf_ptr;
    u8 *buf_end;

    String filename;
    s32 line1;

    // ParserSmKeyValuePair curr_key_val;
    String curr_key;
    String curr_val;

    bool report_errors;

    MemoryArena *final_arena;
    MemoryArena *temp_arena;
} ParserSmCtx;

typedef struct {
    String key;
    smem struct_offset;
} ParserSmSimpleStringKeyValPair;

typedef struct {
    // Bpm can't be zero or negative
    f32 bps;
    f64 active_beats, active_seconds;
    f64 start_beat, start_seconds;
} ParserSmBpm;

typedef struct {
    u8 type;
    f32 bpms[2];
} ParserSmDisplayBpm;

enum {
    PARSER_SM_CHART_TYPE_UNKNOWN,
    PARSER_SM_CHART_TYPE_DANCE_SINGLE,
    PARSER_SM_CHART_TYPE_DANCE_SOLO,
    PARSER_SM_CHART_TYPE_DANCE_DOUBLE,
    PARSER_SM_CHART_TYPE_DANCE_COUPLE,
    PARSER_SM_CHART_TYPE_KB7_SINGLE,
    PARSER_SM_CHART_TYPE_PUMP_SINGLE,
    PARSER_SM_CHART_TYPE_LIGHTS_CABINET,

    PARSER_SM_CHART_TYPE_COUNT,
};

// Storing the blank notes.
typedef struct {
    s32 columns;
    s32 pitch;
    s32 measures_count;
    s32 blank_rows;
    s32 total_rows_count;

    // notes_by_measure[measure_idx] will give a pointer to the notes starting at that measure idx.
    // It isn't strictly necessary but it's useful.
    // With this we can get a pointer directly to the notes starting at a
    // certain measure. Without it, we would need to go from the start of the
    // divisions array and keep adding that many bytes to notes until we reach
    // the measure we want.
    u8 **notes_by_measure;

    // Maybe this could be an u8 instead since the max divisoins is 192 on the
    // stepmania github wiki
    // This is the second thing in notes_raw_data
    s16 *divisions; // array size measures_count

    // size is pitch * total_rows_count
    // access it like this: u8 *row = notes + row_idx*pitch
    //
    u8 *notes;
} ParserSmNotes;

typedef struct {
    smem notes_raw_data_size;
    void *notes_raw_data;
    ParserSmNotes notes;

    u16 chart_type;
    String chart_type_string;
    String description;
    String diff_name;
    s32 diff_number;
} ParserSmDiff;

typedef struct {
    // Just the basics are implemented

    // temp
    String filename;

    Maybe<String> title;
    Maybe<String> subtitle;
    Maybe<String> artist;
    Maybe<String> title_translit;
    Maybe<String> subtitle_translit;
    Maybe<String> artist_translit;
    Maybe<String> genre;
    Maybe<String> credit;

    Maybe<String> banner;
    Maybe<String> background;
    Maybe<String> cdtitle;

    Maybe<String> music;

    Maybe<NewArrayDynamic<ParserSmBpm>> bpms;
    // ChartStops stops;

    // I don't have any files with over 5 difficulties
    s32 diffs_count;
    ParserSmDiff diffs[16];

    // @f64maybe
    Maybe<f64> offset;
    Maybe<f64> sample_start;
    Maybe<f64> sample_length;
    // Maybe<String> selectable;
    Maybe<ParserSmDisplayBpm> display_bpm;
} ParserSmFile;

static const ParserSmSimpleStringKeyValPair parser_sm_simple_strings[] = {
    { "TITLE"_s            , STRUCT_OFFSET(ParserSmFile , title) }             ,
    { "SUBTITLE"_s         , STRUCT_OFFSET(ParserSmFile , subtitle) }          ,
    { "ARTIST"_s           , STRUCT_OFFSET(ParserSmFile , artist) }            ,

    { "TITLETRANSLIT"_s    , STRUCT_OFFSET(ParserSmFile , title_translit) }    ,
    { "SUBTITLETRANSLIT"_s , STRUCT_OFFSET(ParserSmFile , subtitle_translit) } ,
    { "ARTISTTRANSLIT"_s   , STRUCT_OFFSET(ParserSmFile , artist_translit) }   ,

    { "GENRE"_s            , STRUCT_OFFSET(ParserSmFile , genre) }             ,
    { "CREDIT"_s           , STRUCT_OFFSET(ParserSmFile , credit) }            ,
    { "BANNER"_s           , STRUCT_OFFSET(ParserSmFile , banner) }            ,
    { "BACKGROUND"_s       , STRUCT_OFFSET(ParserSmFile , background) }        ,
    { "CDTITLE"_s          , STRUCT_OFFSET(ParserSmFile , cdtitle) }           ,
    { "MUSIC"_s            , STRUCT_OFFSET(ParserSmFile , music) }             ,
    // { "SELECTABLE"_s       , STRUCT_OFFSET(ParserSmFile , selectable) }        ,
};


static const String parser_sm_chart_types_string_table[PARSER_SM_CHART_TYPE_COUNT] = {
    // [PARSER_SM_CHART_TYPE_UNKNOWN]        = "idk"_s,
    [PARSER_SM_CHART_TYPE_DANCE_SINGLE]   = "dance-single"_s,
    [PARSER_SM_CHART_TYPE_DANCE_SOLO]     = "dance-solo"_s,
    [PARSER_SM_CHART_TYPE_DANCE_DOUBLE]   = "dance-double"_s,
    [PARSER_SM_CHART_TYPE_DANCE_COUPLE]   = "dance-couple"_s,
    [PARSER_SM_CHART_TYPE_KB7_SINGLE]     = "kb7-single"_s,
    [PARSER_SM_CHART_TYPE_PUMP_SINGLE]    = "pump-single"_s,
    [PARSER_SM_CHART_TYPE_LIGHTS_CABINET] = "lights-cabinet"_s,
};

// TODO Maybe I should make a table for keymode only..
static const s32 parser_sm_chart_types_columns_table[PARSER_SM_CHART_TYPE_COUNT] = {
    [PARSER_SM_CHART_TYPE_UNKNOWN]        = 0,
    [PARSER_SM_CHART_TYPE_DANCE_SINGLE]   = 4,
    [PARSER_SM_CHART_TYPE_DANCE_SOLO]     = 6,
    [PARSER_SM_CHART_TYPE_DANCE_DOUBLE]   = 8,
    [PARSER_SM_CHART_TYPE_DANCE_COUPLE]   = 8,
    [PARSER_SM_CHART_TYPE_KB7_SINGLE]     = 7,
    [PARSER_SM_CHART_TYPE_PUMP_SINGLE]    = 5,
    [PARSER_SM_CHART_TYPE_LIGHTS_CABINET] = 5,
};

static const Keymode parser_sm_chart_types_keymode_table[PARSER_SM_CHART_TYPE_COUNT] = {
    [PARSER_SM_CHART_TYPE_UNKNOWN]        = Keymode_Invalid,
    [PARSER_SM_CHART_TYPE_DANCE_SINGLE]   = Keymode_4k,
    [PARSER_SM_CHART_TYPE_DANCE_SOLO]     = Keymode_6k,
    [PARSER_SM_CHART_TYPE_DANCE_DOUBLE]   = Keymode_8k,
    [PARSER_SM_CHART_TYPE_DANCE_COUPLE]   = Keymode_8k,
    [PARSER_SM_CHART_TYPE_KB7_SINGLE]     = Keymode_7k,
    [PARSER_SM_CHART_TYPE_PUMP_SINGLE]    = Keymode_5k,
    [PARSER_SM_CHART_TYPE_LIGHTS_CABINET] = Keymode_5k,
};


enum {
    PARSER_SM_NOTE_UNKNOWN = 255, // (u8)-1

    PARSER_SM_NOTE_BLANK = 0,

    PARSER_SM_NOTE_TAP,
    PARSER_SM_NOTE_MINE,
    PARSER_SM_NOTE_HOLD,
    PARSER_SM_NOTE_ROLL,

    // PARSER_SM_NOTE_FAKE,
    PARSER_SM_NOTE_HOLD_END,
    // PASRSER_SM_NOTE_ROLL_END,


    // If PARSER_SM_NOTE_BLANK is zero, COUNT is the actual number of valid
    // notes. If it's is not zero, COUNT is the first invalid note. 
    PARSER_SM_NOTE_COUNT,
};

#define PARSER_SM_NOTE_IS_VALID(n) \
    (((n) >= PARSER_SM_NOTE_BLANK) && ((n) < PARSER_SM_NOTE_COUNT))

#define PARSER_SM_NOTE_TO_NEW_NOTE(smnote) parser_sm_note_to_new_note_table[smnote]

static constexpr NoteType parser_sm_note_to_new_note_table[PARSER_SM_NOTE_COUNT] = {
    [PARSER_SM_NOTE_BLANK]    = NOTE_BLANK,
    [PARSER_SM_NOTE_TAP]      = NOTE_TAP,
    [PARSER_SM_NOTE_HOLD]     = NOTE_HOLD,
    [PARSER_SM_NOTE_ROLL]     = NOTE_SMROLL,
    [PARSER_SM_NOTE_HOLD_END] = NOTE_HOLD_END,
    [PARSER_SM_NOTE_MINE]     = NOTE_MINE,
};


// Adding one here because all the undefined characters will default to zero.
// When I later subtract 1 again with the macro PARSER_SM_CHAR_TO_NOTE(ch),
// they will go to 255 (PARSER_SM_NOTE_UNKNOWN).
// Don't use this table directly; use the macro instead.
static constexpr u8 parser_sm_char_to_note_plus_one_table[256] = {
    ['0'] = 1 + PARSER_SM_NOTE_BLANK,
    ['1'] = 1 + PARSER_SM_NOTE_TAP,
    ['2'] = 1 + PARSER_SM_NOTE_HOLD,
    ['3'] = 1 + PARSER_SM_NOTE_HOLD_END,
    ['4'] = 1 + PARSER_SM_NOTE_ROLL,
    ['M'] = 1 + PARSER_SM_NOTE_MINE,

    // For now ignoring fakes
    ['F'] = 1 + PARSER_SM_NOTE_BLANK,
};
#define PARSER_SM_CHAR_TO_NOTE(ch) (u8)(parser_sm_char_to_note_plus_one_table[(u8)(ch)] - 1)
STATIC_ASSERT(PARSER_SM_NOTE_UNKNOWN == 255);
STATIC_ASSERT(PARSER_SM_CHAR_TO_NOTE(' ') == PARSER_SM_NOTE_UNKNOWN);

static const u8 parser_sm_note_to_char_table[PARSER_SM_NOTE_COUNT] = {
    [PARSER_SM_NOTE_BLANK]    = '0',
    [PARSER_SM_NOTE_TAP]      = '1',
    [PARSER_SM_NOTE_HOLD]     = '2',
    [PARSER_SM_NOTE_ROLL]     = '4',
    [PARSER_SM_NOTE_HOLD_END] = '3',
    [PARSER_SM_NOTE_MINE]     = 'M',
};


#endif
