#include "beat_include.hh"

static bool
very_simple_config_get_line(u8 **buf_ptr, u8 *buf_end, String *ret_str)
{
    while (true) {
        if (*buf_ptr >= buf_end)
            return false;

        // ctx->line1++;
        u8 *ptr = *buf_ptr;
        u8 *prev_ptr = *buf_ptr;
        while (ptr < buf_end) {
            if (*ptr == '\n') {
                break;
            }
            ptr++;
        }
        s32 len = (s32)(ptr-prev_ptr);
        if (ptr < buf_end) {
            ASSERT(*ptr == '\n');
            if ((ptr-1)>=prev_ptr && (*(ptr-1) == '\r')) len--;
            ptr++;
        }
        *buf_ptr = ptr;

        String str = {.len = len, .data = prev_ptr};
        trim_whitespace(&str);
        if (str.len <= 0)
            continue;
        *ret_str = str;
        return true;
    }
    return false;
}

static void
very_simple_config_push(NewArrayDynamic<VerySimpleConfig> *configs, String key, String val)
{
    bool found = false;
    for (smem i = 0; i < configs->len; i++) {
        if (configs->data[i].key == key) {
            found = true;
            configs->data[i].value = arena_push_string(configs->arena, val);
            break;
        }
    }
    if (!found) {
        VerySimpleConfig new_config = {};
        new_config.key = arena_push_string(configs->arena, key);
        new_config.value = arena_push_string(configs->arena, val);
        configs->push(new_config);
    }
}

static String
very_simple_config_get_or_default(NewArrayDynamic<VerySimpleConfig> *configs, String key, String default_value)
{
    String result = default_value;
    for (smem i = 0; i < configs->len; i++) {
        if (configs->data[i].key == key) {
            result = configs->data[i].value;
            break;
        }
    }
    return result;
}

static bool
very_simple_config_get(NewArrayDynamic<VerySimpleConfig> *configs, String key, String *ret_val)
{
    for (smem i = 0; i < configs->len; i++) {
        if (configs->data[i].key == key) {
            *ret_val = configs->data[i].value;
            return true;
        }
    }
    return false;
}

static bool
very_simple_config_file(MemoryArena *final_arena, MemoryArena *temp_arena,
                        String filename_not_nullter, NewArrayDynamic<VerySimpleConfig> *ret_configs)
{
    // each line is "key value"
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    TempMemory temp_for_final_if_error = begin_temp_memory(final_arena);

    NewArrayDynamic<VerySimpleConfig> configs = {};
    String filename = arena_push_string_nullter(temp_arena, filename_not_nullter);
    EntireFile ef;
    u8 *buf_ptr, *buf_end;
    if (!read_entire_file_into_arena(filename, temp_arena, &ef)) {
        beat_printf_err("Failed to read config file: '%.*s'\n", EXPAND_STR_FOR_PRINTF(filename));
        goto rage_quit;
    }
    buf_ptr = ef.buf.data;
    buf_end = ef.buf.data + ef.buf.len;

    configs.arena = final_arena;
    configs.reserve(64);

    while (true) {
        String line;
        if (!very_simple_config_get_line(&buf_ptr, buf_end, &line)) break;
        if (line.data[0] == '#') continue;
        String lhs, rhs;
        split_by_char_and_trim_whitespace(line, ' ', &lhs, &rhs);
        very_simple_config_push(&configs, lhs, rhs);
    }
    end_temp_memory_dont_free(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    *ret_configs = configs;
    return true;
rage_quit:
    end_temp_memory(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    return false;
}
