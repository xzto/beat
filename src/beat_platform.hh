#ifndef BEAT_PLATFORM_H
#define BEAT_PLATFORM_H

// How to get the compiler macros for gcc: "gcc -dM -E - </dev/null"
// Recognize compiler
#define BEAT_COMPILER_GCC 0
#define BEAT_COMPILER_CLANG 0
#define BEAT_COMPILER_MSVC 0
#define BEAT_COMPILER_TCC 0

#if defined(__clang__)
# undef BEAT_COMPILER_CLANG
# define BEAT_COMPILER_CLANG 1
#elif defined(__GNUC__) || defined(__GNUG__)
# undef BEAT_COMPILER_GCC
# define BEAT_COMPILER_GCC 1
#elif defined(_MSC_VER)
# undef BEAT_COMPILER_MSVC
# define BEAT_COMPILER_MSVC 1
#elif defined(__TINYC__)
# undef BEAT_COMPILER_TCC
# define BEAT_COMPILER_TCC 1
#else
# error "Compiler not supported"
#endif


// Recognize operating system
#define BEAT_OS_WIN32 0
#define BEAT_OS_LINUX 0

#if defined(_WIN32)
# undef BEAT_OS_WIN32
# define BEAT_OS_WIN32 1
#elif defined(__linux__)
# undef BEAT_OS_LINUX
# define BEAT_OS_LINUX 1
#else
# error "OS not supported"
#endif


// Recognize architecture
#define BEAT_ARCH_X64 0

#if defined(__x86_64__) || defined(__amd64__) || defined(_M_X64) || defined(_M_AMD64)
# undef BEAT_ARCH_X64
# define BEAT_ARCH_X64 1
#else
# error "Architecture not supported"
#endif

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
# define BEAT_IS_LITTLE_ENDIAN 1
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
# define BEAT_IS_LITTLE_ENDIAN 0
#else
#error "Failed to recognize endianness"
#endif


#if !BEAT_IS_LITTLE_ENDIAN
// TODO: When I rewrite this game, make sure that I don't do anything specific to little endian.
# error "Must be little endian"
#endif


#ifndef BEAT_SLOW
# define BEAT_SLOW 1
#endif

#ifndef BEAT_INTERNAL
# define BEAT_INTERNAL 1
#endif



// Types
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <float.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef s8 b8;
typedef s16 b16;
typedef s32 b32;

typedef float f32;
typedef double f64;

typedef ssize_t smem;
typedef size_t umem;

#define F32_MAX FLT_MAX
#define F64_MAX DBL_MAX

#define S8_MAX ((s8)0x7f)
#define S8_MIN ((s8)0x80)
#define U8_MAX ((u8)0xff)
#define S16_MAX ((s16)0x7fff)
#define S16_MIN ((s16)0x8000)
#define U16_MAX ((u16)0xffff)
#define S32_MAX ((s32)0x7fffffff)
#define S32_MIN ((s32)0x80000000)
#define U32_MAX ((u32)0xffffffff)
#define S64_MAX ((s64)0x7fffffffffffffff)
#define S64_MIN ((s64)0x8000000000000000)
#define U64_MAX ((u64)0xffffffffffffffff)

#define STRINGIFY(x) #x
#define BEAT_JOIN2_(a, b) a##b
#define BEAT_JOIN2(a, b) BEAT_JOIN2_(a, b)

#define KILOBYTES(x) (1024   *          (x))
#define MEGABYTES(x) (1024   * KILOBYTES(x))
#define GIGABYTES(x) (1024LL * MEGABYTES(x))

#define ssizeof(x) ((smem)sizeof(x))

#if BEAT_COMPILER_CLANG || BEAT_COMPILER_GCC
# define ATTRIBUTE_FORMAT_PRINTF(fmt_index, va_index) __attribute__ ((format (printf, fmt_index, va_index)))
#else
# error "Not implemented in this compiler"
#endif

#define beat_log(...) fprintf(stdout, __VA_ARGS__)


#define ASSERT_ALWAYS(x) do {                        \
    if (!(x)) {                                      \
        beat_assert_handler(__FILE__, __LINE__, #x); \
    }                                                \
} while (0)

#define ASSERT_FMT_ALWAYS(x, fmt, ...) do {                              \
    if (!(x)) {                                                          \
        beat_assert_handler_v(__FILE__, __LINE__, #x, fmt, __VA_ARGS__); \
    }                                                                    \
} while (0)

#define ASSERT_MSG_ALWAYS(x, msg) ASSERT_FMT_ALWAYS(x, "%s", msg)

#if BEAT_INTERNAL || 1
# define ASSERT(x) ASSERT_ALWAYS(x)
# define ASSERT_FMT(x, fmt, ...) ASSERT_FMT_ALWAYS(x, fmt, __VA_ARGS__)
# define ASSERT_MSG(x, msg) ASSERT_MSG_ALWAYS(x, msg)
#else
# define ASSERT(x) do{}while(0)
# define ASSERT_FMT(x, fmt, ...) do{}while(0)
# define ASSERT_MSG(x, msg) do{}while(0)
#endif

#if BEAT_SLOW
# define ASSERT_SLOW(x) ASSERT(x)
#else
# define ASSERT_SLOW(x) do{}while(0)
#endif

#if 0
# define STATIC_ASSERT(x) static char BEAT_JOIN2(_temp_static_assert_, __COUNTER__)[(x) ? 1 : -1]
# define STATIC_ASSERT_MSG(x, msg) STATIC_ASSERT(x)
#else
# define STATIC_ASSERT(x) static_assert(x, "")
# define STATIC_ASSERT_MSG(x, msg) static_assert(x, msg)
#endif

STATIC_ASSERT(ssizeof(umem) == ssizeof(void *));
STATIC_ASSERT(ssizeof(smem) == ssizeof(void *));

// Make sure the types are actually what they say they are (not necessary)
STATIC_ASSERT(ssizeof(u8) == 1);
STATIC_ASSERT(ssizeof(s8) == 1);
STATIC_ASSERT(ssizeof(u16) == 2);
STATIC_ASSERT(ssizeof(s16) == 2);
STATIC_ASSERT(ssizeof(u32) == 4);
STATIC_ASSERT(ssizeof(s32) == 4);
STATIC_ASSERT(ssizeof(u64) == 8);
STATIC_ASSERT(ssizeof(s64) == 8);
STATIC_ASSERT(ssizeof(f32) == 4);
STATIC_ASSERT(ssizeof(f64) == 8);

#define INVALID_CODE_PATH do { ASSERT_ALWAYS(!"Invalid code path"); } while (0)
#define NOT_IMPLEMENTED do { ASSERT_ALWAYS(!"Not implemented"); } while (0)
#if BEAT_INTERNAL
# define DEBUG_BREAK do { asm volatile ("int3"); int volatile BEAT_JOIN2(_debug_break_, __COUNTER__) = 123; } while (0)
#else
# define DEBUG_BREAK do { } while (0)
// # define DEBUG_BREAK do { STATIC_ASSERT(!"DEBUG_BREAK can not be used in a release build!"); } while (0)
#endif

#define 😡 ASSERT_ALWAYS(!"😡");

[[noreturn]]
static void
beat_assert_handler(char *file, int line, char *cond_str)
{
    // TODO: popup a message box, ignore assertion
    beat_log("Assert fail at '%s:%d': '%s'\n", file, line, cond_str);
    fflush(stdout); fflush(stderr);
#if BEAT_INTERNAL
    DEBUG_BREAK;
#endif
    abort();
}

[[noreturn]]
static void
beat_assert_handler_v(char *file, int line, char *cond_str, char *fmt, ...)
{
    // TODO: popup a message box, ignore assertion
    va_list ap;

    char str[2048];
    va_start(ap, fmt);
    vsnprintf(str, ssizeof(str), fmt, ap);
    va_end(ap);

    beat_log("Assert fail at '%s:%d': '%s', %s\n", file, line, cond_str, str);

    fflush(stdout); fflush(stderr);
#if BEAT_INTERNAL
    DEBUG_BREAK;
#endif
    abort();
}

#define FALLTHROUGH [[fallthrough]]

#include "beat_intrinsics.hh"
#include "beat_atomic.hh"


// Only tested for clang (and it works)
// Doesn't work with gcc
#if BEAT_COMPILER_GCC || BEAT_COMPILER_CLANG
# define BEAT_FORCE_INLINE __attribute__((always_inline))
#elif BEAT_COMPILER_MSVC
# define BEAT_FORCE_INLINE __forceinline
#elif BEAT_COMPILER_TCC
// No inline in tcc
# define BEAT_FORCE_INLINE inline
#else
# error "Compiler not supported"
#endif

#if 0
# define STRUCT_OFFSET(s, e) ((smem)&((s *)0)->e)
#else
# include <stddef.h>
# define STRUCT_OFFSET(s, e) offsetof(s, e)
#endif

#define zero_size(ptr, size) xmemset((ptr), 0, (smem)(size))
#define zero_struct(s) zero_size((s), ssizeof(*(s)))
#define zero_struct_count(s, n) zero_size((s), (n)*ssizeof(*(s)))
#define zero_static_carray(a) zero_struct_count((a), ARRAY_COUNT(a))

// Used to invalidate memory.
#define INVALIDATE_STRUCT(s) INVALIDATE_SIZE((s), ssizeof(*(s)))
#if BEAT_SLOW
// All pointers set to 0xaaaa... will be invalid (they're not even canonical on x86_64).
# define INVALIDATE_SIZE(ptr, size) do { xmemset((ptr), 0xaa, (size)); } while (0)
#else
# define INVALIDATE_SIZE(ptr, size) do { (void)(ptr); (void)(size); } while (0)
#endif


#define SSIZEOF_VAL(s) ssizeof(*(s))

#define MINIMUM(a, b) ((a) < (b) ? (a) : (b))
#define MAXIMUM(a, b) ((a) > (b) ? (a) : (b))
#define BETWEEN(x, a, b) (((x) >= (a)) && ((x) <= (b)))
#define ABC(idx, count) (((idx) >= (0)) && ((idx) < (count)))

#define EXPAND_STR_FOR_PRINTF(s) (int)(s).len, (s).data
#define string_literal(s) ((String){ssizeof(s)-1, (u8 *)(s)})

#define ARRAY_COUNT(a) (int)(ssizeof(a)/ssizeof((a)[0]))

// #define SWAP(a, b) do {  \
//     auto _tmp_for_swap = *(a); \
//     *(a) = *(b);               \
//     *(b) = _tmp_for_swap;      \
// } while (0);

template <typename T>
static inline void swap_values(T *a, T *b) {
    auto tmp = *a;
    *a = *b;
    *b = tmp;
}


#if BEAT_COMPILER_CLANG || BEAT_COMPILER_GCC
# define COMPILER_BARRIER() __asm__ __volatile__ ("" ::: "memory")
#else
# error "not implemented"
#endif



// Used to make a padding if the pointer type is 4 bytes
typedef u32 MaybePadIfPointer32[ssizeof(void *) == 8 ? 0 : 1];
STATIC_ASSERT(ssizeof(MaybePadIfPointer32) == 0);

#define BEAT_POINTER32 0
#if BEAT_POINTER32
# define PAD_IF_POINTER32(name) \
    MaybePadIfPointer32 BEAT_JOIN2(_padding_if_pointer32_, name)
#else
# define PAD_IF_POINTER32(name)
#endif

#include "beat_memory.hh"

template <typename T> struct Slice;

struct String {
    smem len;
    u8 *data;

    inline operator Slice<u8> (); // impl on beat_test_new_array.hh
};
typedef String Buffer;
inline static constexpr String operator ""_s (const char *cstr, size_t len) { return {.len=(smem)len, .data=(u8*)cstr}; }
static bool strings_are_equal(String a, String b);
inline static bool operator == (String a, String b) { return strings_are_equal(a, b); }
inline static bool operator != (String a, String b) { return !strings_are_equal(a, b); }


// struct StringNullter {
//     smem len;
//     u8 *data;

//     inline operator String () { return { .len = len, .data = data }; }

//     inline void assert() { ASSERT(data[len] == 0); }
// };

typedef struct {
    String filename;
    String buf;

    // arena is null when it wasn't created with an arena. this is just to make
    // sure that I don't free it if it came from an arena.
    MemoryArena *arena;
} EntireFile;

struct MaybeNoneType_ {};
static constexpr MaybeNoneType_ None = {};
template<typename T> struct MaybeSomeType_ { T value; };
template<typename T> MaybeSomeType_<T> Some(T v) { return {v}; }
// MaybeSomeType_<void> Some() { return {}; }

template<typename T>
struct Maybe {
private:
    T value;
    bool ok;
public:
    Maybe() = default; // none
    Maybe(MaybeNoneType_) : ok(false) {}
    Maybe(MaybeSomeType_<T> s) : value(s.value), ok(true) {}
    BEAT_FORCE_INLINE bool is_some() { return ok; }
    BEAT_FORCE_INLINE bool is_none() { return !is_some(); }
    BEAT_FORCE_INLINE T unwrap() { ASSERT(is_some()); return unwrap_unchecked(); }
    BEAT_FORCE_INLINE T unwrap_unchecked() { return value; }
    BEAT_FORCE_INLINE T unwrap_or(T other) { return is_some() ? unwrap_unchecked() : other; }
};


template<typename T>
struct MaybePtr {
private:
    T *ptr;
public:
    MaybePtr() = default; // none
    MaybePtr(MaybeNoneType_) : ptr(nullptr) {}
    MaybePtr(MaybeSomeType_<T*> s) : ptr(s.value) { ASSERT(s.value != nullptr); }
    BEAT_FORCE_INLINE bool is_some() { return ptr != nullptr; }
    BEAT_FORCE_INLINE bool is_none() { return !is_some(); }
    BEAT_FORCE_INLINE T *unwrap() { ASSERT(is_some()); return unwrap_unchecked(); }
    BEAT_FORCE_INLINE T *unwrap_unchecked() { return ptr; }
    BEAT_FORCE_INLINE T *unwrap_or(T* other) { return is_some() ? unwrap_unchecked() : other; }
};
STATIC_ASSERT(sizeof(MaybePtr<u32>) == sizeof(void*));

// https://mg.guelker.eu/articles/2020/02/02/defer-in-cpp/
template<typename T>
struct DeferStruct {
    ~DeferStruct() {
        fn();
    }

    const T& fn;
};

struct DeferHelper { int a; };
template<typename T>
DeferStruct<T> operator+=(const DeferHelper&, const T& other)
{
    return DeferStruct<T>{ .fn = other };
}

// TODO: Sometimes when I compile with clang -O1, it crashes on some defers.
#define DEFER_JOIN_A(a, b) a ## b
#define DEFER_JOIN_B(a, b) DEFER_JOIN_A(a, b)
#define DEFER_COUNTER(counter) DeferHelper DEFER_JOIN_B(WVZQ_DeferHelper_, counter); \
    auto DEFER_JOIN_B(WVZQ_defer_, counter) = DEFER_JOIN_B(WVZQ_DeferHelper_, counter) += [&]()->void
#define defer DEFER_COUNTER(__COUNTER__)
// defer { printf("Hello, world!"); };


// for std::underlying_type, std::is_same
#include <type_traits>

// compiler_stupidity++;
#define STUPID_ENUM_OPERATOR_UNARY(e, op) \
    BEAT_FORCE_INLINE e operator op (e self) { return (e)op(std::underlying_type<e>::type)self; }
#define STUPID_ENUM_OPERATOR_BIN(e, op)                                                                                          \
    BEAT_FORCE_INLINE e operator op (e self, e other) { return (e)((std::underlying_type<e>::type)self op (std::underlying_type<e>::type)other); } \
    BEAT_FORCE_INLINE e operator op##= (e &self, e other) { return self=(e)((std::underlying_type<e>::type)self op (std::underlying_type<e>::type)other); }

#define STUPID_ENUM_FLAGS_OPERATORS(e) \
    STUPID_ENUM_OPERATOR_UNARY(e, ~);  \
    STUPID_ENUM_OPERATOR_BIN(e, &);    \
    STUPID_ENUM_OPERATOR_BIN(e, |);    \


// When defining an enum flags, do `static constexpr Name Name_value0 = { .x = 1<<0 };` for every flag definition.
#define DEFINE_ENUM_FLAGS(Name, IntegerType)                                            \
    struct Name {                                                                       \
        IntegerType x;                                                                  \
        inline operator bool () const { return x != 0; }                                \
        inline constexpr Name operator ~ () const { return { ~x }; }                    \
        inline constexpr Name operator | (Name other) const { return { x | other.x }; } \
        inline constexpr Name operator & (Name other) const { return { x & other.x }; } \
        inline Name & operator |= (Name other) { x |= other.x; return *this; }          \
        inline Name & operator &= (Name other) { x &= other.x; return *this; }          \
        inline constexpr bool operator == (Name other) const { return x == other.x; }   \
        inline constexpr bool operator != (Name other) const { return x != other.x; }   \
    }


#define do_binary_search(arr_ptr, arr_len, the_ret_idx, the_ret_thing, member, to_search) do { \
    smem _tmp_unused_ret_idx = NULL;                                                           \
    auto _tmp_unused_ret_thing = (arr_ptr);                                                    \
    auto _tmp_ret_thing = (the_ret_thing) ? (the_ret_thing) : &_tmp_unused_ret_thing;          \
    auto _tmp_ret_idx = (the_ret_idx) ? (the_ret_idx) : &_tmp_unused_ret_idx;                  \
    if ((arr_len) <= 0) {                                                                      \
        *(_tmp_ret_thing) = NULL;                                                              \
        *(_tmp_ret_idx) = 0;                                                                   \
    } else {                                                                                   \
        smem _tmp_start = 0;                                                                   \
        smem _tmp_end = (arr_len)-1;                                                           \
        *(_tmp_ret_thing) = NULL;                                                              \
        *(_tmp_ret_idx) = (_tmp_start + _tmp_end) / 2;                                         \
        for (;;) {                                                                             \
            *(_tmp_ret_idx) = ((_tmp_end) + (_tmp_start)) / 2;                                 \
            auto *_tmp_thing = &(arr_ptr)[*(_tmp_ret_idx)];                                    \
            auto _tmp_value = _tmp_thing->member;                                              \
            if (_tmp_value == (to_search)) {                                                   \
                *(_tmp_ret_thing) = _tmp_thing;                                                \
                break;                                                                         \
            } else if ((_tmp_start) == (_tmp_end)) {                                           \
                *(_tmp_ret_thing) = NULL;                                                      \
                if (_tmp_value < (to_search)) {                                                \
                    *(_tmp_ret_idx) += 1;                                                      \
                }                                                                              \
                break;                                                                         \
            } else if (_tmp_value < (to_search)) {                                             \
                (_tmp_start) = *(_tmp_ret_idx)+1;                                              \
            } else { /* if (_tmp_value > (to_search))*/                                        \
                (_tmp_end) = *(_tmp_ret_idx)-1;                                                \
            }                                                                                  \
        }                                                                                      \
    }                                                                                          \
} while (0)


#define DO_ALL_BEAT_KEY_THINGS              \
    DO_BEAT_KEY_THING_INVALID()             \
    DO_BEAT_KEY_THING__START(ASCII)         \
    DO_BEAT_KEY_THING_CH(SPACE, ' ') /*32*/ \
    DO_BEAT_KEY_THING_AZ(A) /*65*/          \
    DO_BEAT_KEY_THING_AZ(B)                 \
    DO_BEAT_KEY_THING_AZ(C)                 \
    DO_BEAT_KEY_THING_AZ(D)                 \
    DO_BEAT_KEY_THING_AZ(E)                 \
    DO_BEAT_KEY_THING_AZ(F)                 \
    DO_BEAT_KEY_THING_AZ(G)                 \
    DO_BEAT_KEY_THING_AZ(H)                 \
    DO_BEAT_KEY_THING_AZ(I)                 \
    DO_BEAT_KEY_THING_AZ(J)                 \
    DO_BEAT_KEY_THING_AZ(K)                 \
    DO_BEAT_KEY_THING_AZ(L)                 \
    DO_BEAT_KEY_THING_AZ(M)                 \
    DO_BEAT_KEY_THING_AZ(N)                 \
    DO_BEAT_KEY_THING_AZ(O)                 \
    DO_BEAT_KEY_THING_AZ(P)                 \
    DO_BEAT_KEY_THING_AZ(Q)                 \
    DO_BEAT_KEY_THING_AZ(R)                 \
    DO_BEAT_KEY_THING_AZ(S)                 \
    DO_BEAT_KEY_THING_AZ(T)                 \
    DO_BEAT_KEY_THING_AZ(U)                 \
    DO_BEAT_KEY_THING_AZ(V)                 \
    DO_BEAT_KEY_THING_AZ(W)                 \
    DO_BEAT_KEY_THING_AZ(X)                 \
    DO_BEAT_KEY_THING_AZ(Y)                 \
    DO_BEAT_KEY_THING_AZ(Z) /*90*/          \
    DO_BEAT_KEY_THING_09(0) /*48*/          \
    DO_BEAT_KEY_THING_09(1)                 \
    DO_BEAT_KEY_THING_09(2)                 \
    DO_BEAT_KEY_THING_09(3)                 \
    DO_BEAT_KEY_THING_09(4)                 \
    DO_BEAT_KEY_THING_09(5)                 \
    DO_BEAT_KEY_THING_09(6)                 \
    DO_BEAT_KEY_THING_09(7)                 \
    DO_BEAT_KEY_THING_09(8)                 \
    DO_BEAT_KEY_THING_09(9) /*57*/          \
    DO_BEAT_KEY_THING_CH(APOSTROPHE,'\'')   /*39*/ \
    DO_BEAT_KEY_THING_CH(COMMA,     ',')    /*44*/ \
    DO_BEAT_KEY_THING_CH(MINUS,     '-')    /*45*/ \
    DO_BEAT_KEY_THING_CH(DOT,       '.')    /*46*/ \
    DO_BEAT_KEY_THING_CH(SLASH,     '/')    /*47*/ \
    DO_BEAT_KEY_THING_CH(SEMICOLON, ';')    /*59*/ \
    DO_BEAT_KEY_THING_CH(EQUALS,    '=')    /*61*/ \
    DO_BEAT_KEY_THING_CH(LBRACKET,  '[')    /*91*/ \
    DO_BEAT_KEY_THING_CH(RBRACKET,  ']')    /*93*/ \
    DO_BEAT_KEY_THING__END2(ASCII, 256)     \
    DO_BEAT_KEY_THING(BACKSPACE)            \
    DO_BEAT_KEY_THING(BACKSLASH)            \
    DO_BEAT_KEY_THING(NONUSBACKSLASH)       \
    DO_BEAT_KEY_THING(ENTER)                \
    DO_BEAT_KEY_THING(ESCAPE)               \
    DO_BEAT_KEY_THING(GRAVE)                \
    DO_BEAT_KEY_THING(TAB)                  \
    DO_BEAT_KEY_THING(CAPSLOCK)             \
    DO_BEAT_KEY_THING__START(MODS)          \
    DO_BEAT_KEY_THING(LSHIFT)               \
    DO_BEAT_KEY_THING(RSHIFT)               \
    DO_BEAT_KEY_THING(LCTRL)                \
    DO_BEAT_KEY_THING(RCTRL)                \
    DO_BEAT_KEY_THING(LALT)                 \
    DO_BEAT_KEY_THING(RALT)                 \
    DO_BEAT_KEY_THING(LWIN)                 \
    DO_BEAT_KEY_THING(RWIN)                 \
    DO_BEAT_KEY_THING__END(MODS)            \
    DO_BEAT_KEY_THING(F1)                   \
    DO_BEAT_KEY_THING(F2)                   \
    DO_BEAT_KEY_THING(F3)                   \
    DO_BEAT_KEY_THING(F4)                   \
    DO_BEAT_KEY_THING(F5)                   \
    DO_BEAT_KEY_THING(F6)                   \
    DO_BEAT_KEY_THING(F7)                   \
    DO_BEAT_KEY_THING(F8)                   \
    DO_BEAT_KEY_THING(F9)                   \
    DO_BEAT_KEY_THING(F10)                  \
    DO_BEAT_KEY_THING(F11)                  \
    DO_BEAT_KEY_THING(F12)                  \
    DO_BEAT_KEY_THING(F13)                  \
    DO_BEAT_KEY_THING(F14)                  \
    DO_BEAT_KEY_THING(F15)                  \
    DO_BEAT_KEY_THING(F16)                  \
    DO_BEAT_KEY_THING(F17)                  \
    DO_BEAT_KEY_THING(F18)                  \
    DO_BEAT_KEY_THING(F19)                  \
    DO_BEAT_KEY_THING(F20)                  \
    DO_BEAT_KEY_THING(F21)                  \
    DO_BEAT_KEY_THING(F22)                  \
    DO_BEAT_KEY_THING(F23)                  \
    DO_BEAT_KEY_THING(F24)                  \
    DO_BEAT_KEY_THING(PRINTSCREEN)          \
    DO_BEAT_KEY_THING(SCROLLLOCK)           \
    DO_BEAT_KEY_THING(PAUSE)                \
    DO_BEAT_KEY_THING(INSERT)               \
    DO_BEAT_KEY_THING(HOME)                 \
    DO_BEAT_KEY_THING(PAGEUP)               \
    DO_BEAT_KEY_THING(DELETE)               \
    DO_BEAT_KEY_THING(END)                  \
    DO_BEAT_KEY_THING(PAGEDOWN)             \
    DO_BEAT_KEY_THING(UP)                   \
    DO_BEAT_KEY_THING(DOWN)                 \
    DO_BEAT_KEY_THING(LEFT)                 \
    DO_BEAT_KEY_THING(RIGHT)                \
    DO_BEAT_KEY_THING(KP_NUMLOCK)           \
    DO_BEAT_KEY_THING(KP_SLASH)             \
    DO_BEAT_KEY_THING(KP_ASTERISK)          \
    DO_BEAT_KEY_THING(KP_MINUS)             \
    DO_BEAT_KEY_THING(KP_PLUS)              \
    DO_BEAT_KEY_THING(KP_DOT)               \
    DO_BEAT_KEY_THING(KP_ENTER)             \
    DO_BEAT_KEY_THING(KP_COMMA)             \
    DO_BEAT_KEY_THING(KP_0)                 \
    DO_BEAT_KEY_THING(KP_00)                \
    DO_BEAT_KEY_THING(KP_000)               \
    DO_BEAT_KEY_THING(KP_1)                 \
    DO_BEAT_KEY_THING(KP_2)                 \
    DO_BEAT_KEY_THING(KP_3)                 \
    DO_BEAT_KEY_THING(KP_4)                 \
    DO_BEAT_KEY_THING(KP_5)                 \
    DO_BEAT_KEY_THING(KP_6)                 \
    DO_BEAT_KEY_THING(KP_7)                 \
    DO_BEAT_KEY_THING(KP_8)                 \
    DO_BEAT_KEY_THING(KP_9)                 \
    DO_BEAT_KEY_THING(THOUSANDSSEPARATOR)   \
    DO_BEAT_KEY_THING(INTERNATIONAL1)       \
    DO_BEAT_KEY_THING(INTERNATIONAL2)       \
    DO_BEAT_KEY_THING(INTERNATIONAL3)       \
    DO_BEAT_KEY_THING(INTERNATIONAL4)       \
    DO_BEAT_KEY_THING(INTERNATIONAL5)       \
    DO_BEAT_KEY_THING(INTERNATIONAL6)       \
    DO_BEAT_KEY_THING(INTERNATIONAL7)       \
    DO_BEAT_KEY_THING(INTERNATIONAL8)       \
    DO_BEAT_KEY_THING(INTERNATIONAL9)       \
    DO_BEAT_KEY_THING(ERASEEAZE)            \
    DO_BEAT_KEY_THING__END(KEYBOARD)        \
    DO_BEAT_KEY_THING__START(MOUSE)         \
    DO_BEAT_KEY_THING(MOUSE_LEFT)           \
    DO_BEAT_KEY_THING(MOUSE_MIDDLE)         \
    DO_BEAT_KEY_THING(MOUSE_RIGHT)          \
    DO_BEAT_KEY_THING(MOUSE_X1)             \
    DO_BEAT_KEY_THING(MOUSE_X2)             \
    DO_BEAT_KEY_THING__END(MOUSE)           \
    DO_BEAT_KEY_THING__END(SIMPLE)

// DO_BEAT_KEY_THING(MOUSE_WHEEL_DOWN)
// DO_BEAT_KEY_THING(MOUSE_WHEEL_UP)

// TODO Rename BeatKey to something like RawButton
typedef u32 BeatKey;
enum {
#define DO_BEAT_KEY_THING_INVALID()     BEAT_KEY_INVALID = 0,
#define DO_BEAT_KEY_THING(name)         BEAT_KEY_##name,
#define DO_BEAT_KEY_THING_CH(name, ch)  BEAT_KEY_##name = ch,
#define DO_BEAT_KEY_THING_09(num)       BEAT_KEY_##num = (#num)[0],
#define DO_BEAT_KEY_THING_AZ(ch)        BEAT_KEY_##ch = (#ch)[0],
#define DO_BEAT_KEY_THING__START(name)  BEAT_KEY__##name##_START,
#define DO_BEAT_KEY_THING__END(name)    BEAT_KEY__##name##_END,
#define DO_BEAT_KEY_THING__END2(name, num)  BEAT_KEY__##name##_END = num,
    DO_ALL_BEAT_KEY_THINGS
#undef DO_BEAT_KEY_THING_INVALID
#undef DO_BEAT_KEY_THING
#undef DO_BEAT_KEY_THING_CH
#undef DO_BEAT_KEY_THING_09
#undef DO_BEAT_KEY_THING_AZ
#undef DO_BEAT_KEY_THING__START
#undef DO_BEAT_KEY_THING__END
#undef DO_BEAT_KEY_THING__END2
};

#define BEAT_KEY_MOUSE_TO_IDX(beat_key) ((beat_key) - BEAT_KEY__MOUSE_START - 1)

static const String beat_key_to_string_table[BEAT_KEY__SIMPLE_END] = {
#define DO_BEAT_KEY_THING_INVALID()     [BEAT_KEY_INVALID] = "INVALID"_s,
#define DO_BEAT_KEY_THING(name)         [BEAT_KEY_##name] = string_literal(#name),
#define DO_BEAT_KEY_THING_CH(name, ch)  [BEAT_KEY_##name] = string_literal(#name),
#define DO_BEAT_KEY_THING_09(num)       [BEAT_KEY_##num] = string_literal(#num),
#define DO_BEAT_KEY_THING_AZ(ch)        [BEAT_KEY_##ch] = string_literal(#ch),
#define DO_BEAT_KEY_THING__START(name)
#define DO_BEAT_KEY_THING__END(name)
#define DO_BEAT_KEY_THING__END2(name, num)
    DO_ALL_BEAT_KEY_THINGS
#undef DO_BEAT_KEY_THING_INVALID
#undef DO_BEAT_KEY_THING
#undef DO_BEAT_KEY_THING_CH
#undef DO_BEAT_KEY_THING_09
#undef DO_BEAT_KEY_THING_AZ
#undef DO_BEAT_KEY_THING__START
#undef DO_BEAT_KEY_THING__END
#undef DO_BEAT_KEY_THING__END2
};

DEFINE_ENUM_FLAGS(ButtonState, u32);
static constexpr ButtonState ButtonState_NULL    = { .x = 0 };
static constexpr ButtonState ButtonState_PRESS   = { .x = 1<<0 }; // key was pressed this frame
static constexpr ButtonState ButtonState_RELEASE = { .x = 1<<1 }; // key was released this frame
static constexpr ButtonState ButtonState_DOWN    = { .x = 1<<2 }; // key is down
static constexpr ButtonState ButtonState_REPEAT  = { .x = 1<<3 }; // key repeat (not set on PRESS)

// called on the platform code
static void
update_button_state(ButtonState *curr, ButtonState last)
{
    // Update ButtonState_PRESS, RELEASE, REPEAT
    if (last & ButtonState_REPEAT) {
        *curr &= ~ButtonState_REPEAT;
    }
    if (*curr & ButtonState_DOWN) {
        if (!(last & ButtonState_DOWN)) {
            *curr &= ~ButtonState_RELEASE;
            *curr |= ButtonState_PRESS;
        } else {
            *curr &= ~ButtonState_PRESS;
            *curr &= ~ButtonState_RELEASE;
        }
    } else {
        if (last & ButtonState_DOWN) {
            *curr |= ButtonState_RELEASE;
            *curr &= ~ButtonState_PRESS;
        } else {
            *curr &= ~ButtonState_PRESS;
            *curr &= ~ButtonState_RELEASE;
        }
    }
    if ((*curr & ButtonState_PRESS) || !(*curr & ButtonState_DOWN))
        *curr &= ~ButtonState_REPEAT;
}


#include "beat_math.hh"

DEFINE_ENUM_FLAGS(KeyboardMods, u32);
static constexpr KeyboardMods KeyboardMods_None     = { .x = 0 };
static constexpr KeyboardMods KeyboardMods_Shift    = { .x = 1<<1 };
static constexpr KeyboardMods KeyboardMods_Ctrl     = { .x = 1<<2 };
static constexpr KeyboardMods KeyboardMods_Alt      = { .x = 1<<3 };
static constexpr KeyboardMods KeyboardMods_Win      = { .x = 1<<4 };
static constexpr KeyboardMods KeyboardMods_Required = KeyboardMods_Shift | KeyboardMods_Ctrl | KeyboardMods_Alt | KeyboardMods_Win;
static constexpr KeyboardMods KeyboardMods_Any      = { .x = 1<<6 };


struct KeyBinding {
    BeatKey key;
    KeyboardMods mods;
};


struct ButtonInput {
    ButtonState state;
    // TODO maybe make mods_press and mods_release
    KeyboardMods mods_for_press_or_release; // mods serve only for PRESS and RELEASE
    // f32 float_val; // TODO:
    u64 os_clock;
    f64 os_time_seconds;
};

struct MouseButtonInput {
    ButtonInput bi;
    s32 clicks; // 1 for single click, 2 for double click, etc
};

struct MouseInput {
    v2s pos, delta_pos;
    // i probably only need delta_wheel but i'll keep the "absolute" too.
    s32 wheel_dont_use_me, delta_wheel;
    MouseButtonInput buttons[BEAT_KEY__MOUSE_END - BEAT_KEY__MOUSE_START - 1];
};

STATIC_ASSERT(ARRAY_COUNT(MouseInput::buttons) == (BEAT_KEY__MOUSE_END - BEAT_KEY__MOUSE_START - 1));

extern b32 global_running;
extern u64 global_frame_count;
#define FRAME_COUNT_ONCE_IN_A_WHILE ((global_frame_count % 144) == 0)

// TODO: Don't include these files here
#include "beat_utils.cc"
#include "beat_memory.cc"
#include "file.cc"

#include "beat_test_new_array.hh"


// TODO: Get input from controllers and assign a BeatKey to that controller's input.
// Maybe until BEAT_KEY__SIMPLE_END it is just that array, and if it is bigger than that, I use a hash table and something like this:
#if 0
{
    // a BeatKey must represent either a keyboard key or a controller button.
    // If I disconnect and connect the controller or restart the game, the BeatKey for that controller's button should be the same.
    // If I do the "next_unique_beat_key" method, that won't work.
    // So the only way to do that is to include some information about the controller in the BeatKey.
    // Maybe it doesn't need 32 bytes but something like that.
    // It would be cool if the controller were to have the same controller_id between linux and windows.
    #define BEAT_CONTROLLER_ID_BYTES 32
    struct BeatKey {
         u32 key;
         u8 controller_id[BEAT_CONTROLLER_ID_BYTES];
    };
    // to represent it in text for string_to_beat_key: "!controller_id=encoded_in_hex,button=3"
    // string starts with "!" so I know that I need to parse it differently.
    // if button is 3, key would be 100003 or something like that, so I can check quickly if the beatkey is from a keyboard or not
    struct ControllerThing {
        u8 controller_id[BEAT_CONTROLLER_ID_BYTES];
        bool will_be_deleted_on_next_frame;
        // BeatKey beat_key_start;
        s32 bis_count;
        ButtonInput *bis; // [bis_count], malloced probably
        // also analog_count, analog_values, but that wouldn't have much to do
        // with BeatKey (unless we make a digital button out of it - probably not);
    };
    struct ControllersInput {
        // if a controller gets disconnected it is probably good to set all of
        // the buttons UP in one frame and delete the controller on the next
        // one.
        BeatKey next_unique_beat_key; // beat_key_start for the next ControllerThing we add.
        NewArrayDynamic<ControllerThing> controllers;
    };
    {
        // on GameInput
        MouseInput mouse, last_mouse;
        ButtonInput raw_keys[RAW_KEYS_COUNT], last_raw_keys[RAW_KEYS_COUNT];
        // from BeatKey to ButtonInput: if key < RAW_KEYS_COUNT, raw_keys[key]. if
        // not we search on controllers.controllers.data[i].beat_key_start and
        // bis_count for the key we want. if we don't find it that key becomes invalid.
        ControllersInput controllers, last_controllers;
    }
    {
        BeatKey key = string_to_beat_key("!controller_id=asdfasdf...,button=3"_s);
        ButtonInput bi = simple_key_get(gin, key);
    }
    // modify this function
    static inline ButtonInput simple_key_get(GameInput *gin, BeatKey key)
    {
        if (key.key_id == 0) return {};

        ASSERT(key.key_id >= 0);
        if (key.key_id < ARRAY_COUNT(gin->raw_keys)) {
            return gin->raw_keys[key.key_id];
        } else if (key.key_id >= BEAT_CONTROLLER_KEY_START) {
            u32 key_idx_in_controller = key.key_id - BEAT_CONTROLLER_KEY_START;
            for (controller in gin->controllers.controllers) {
                if (controller->controller_id == key.controller_id) {
                    if (key_idx_in_controller < controller->bis_count) {
                        return controller->bis[key_idx_in_controller];
                    } else {
                        // that button doesn't exist in that controller, probably show error
                        return {};
                    }
                }
            }
            return {};
        } else {
            // Invalid code, probably
            // maybe show error
            return {};
        }
    }
}

#endif

#define RAW_KEYS_COUNT BEAT_KEY__SIMPLE_END
STATIC_ASSERT(RAW_KEYS_COUNT >= BEAT_KEY__KEYBOARD_END);
struct GameInput {
    // these can be f32. I'll change it later.
    f64 debug_dt_a; // time before swap buffers
    f64 debug_dt_b; // time after swap buffers, before sleep
    f64 debug_dt_c; // time slept
    f64 debug_dt_frame; // total frame time

    f64 dt; // time used for simulation. == debug_dt_frame * memory->time_multiplier

    // KeyboardMods keyboard_mods; // TODO

    MouseInput mouse;
    MouseInput last_mouse;

    // TODO: Rename raw_keys to basic_keys
    ButtonInput raw_keys[RAW_KEYS_COUNT];
    ButtonInput last_raw_keys[RAW_KEYS_COUNT];

    StringNewArrayDynamic text_input; // UTF-8 // on temporary
    StringNewArrayDynamic text_composition; // UTF-8 // on the heap

    f64 os_time_seconds_frame_start;
    u64 clock_frame_start;
};

#include "beat_work_queue.hh"

struct GameMemory;
struct DirectoryIter;

#define PLATFORM_GET_CLOCK(name) u64 name(void)
// typedef PLATFORM_GET_CLOCK(PlatformGetClockFn);

#define PLATFORM_GET_SECS_PER_CLOCK(name) f64 name(void)
// typedef PLATFORM_GET_SECS_PER_CLOCK(PlatformGetSecsPerClockFn);

// init the directory iterator
// a shame that I can't make default arguments for function pointers =(
#define PLATFORM_DIRECTORY_ITER_INIT(name) bool name(DirectoryIter *iter, String dir_not_nullter, MemoryArena *arena, bool is_recursive, DirectoryIter *prev_iter)
// typedef PLATFORM_DIRECTORY_ITER_INIT(PlatformDirectoryIterInitFn);

// get the next directory iterator
#define PLATFORM_DIRECTORY_ITER_NEXT(name) bool name(DirectoryIter *iter)
// typedef PLATFORM_DIRECTORY_ITER_NEXT(PlatformDirectoryIterNextFn);

// stop the directory iterator (need to call it even after next failed)
#define PLATFORM_DIRECTORY_ITER_END(name) void name(DirectoryIter *iter)
// typedef PLATFORM_DIRECTORY_ITER_END(PlatformDirectoryIterEndFn);

#define PLATFORM_TEXT_INPUT_START(name) void name(void)
// typedef PLATFORM_TEXT_INPUT_START(PlatformTextInputStartFn);
#define PLATFORM_TEXT_INPUT_STOP(name) void name(void)
// typedef PLATFORM_TEXT_INPUT_STOP(PlatformTextInputStopFn);
// text_input_set_rect: y is bottom-up; the rect represents the current text from gin.text_composition.
#define PLATFORM_TEXT_INPUT_SET_RECT(name) void name(f32 x, f32 y, f32 w, f32 h, s32 window_height)
// typedef PLATFORM_TEXT_INPUT_SET_RECT(PlatformTextInputSetRectFn);

// get clipboard with `String clip = platform.get_clipboard()`. it will
// automatically be freed (SDL_free) on the start of the next frame.
#define PLATFORM_GET_CLIPBOARD(name) String name(GameMemory *memory)
// typedef PLATFORM_GET_CLIPBOARD(PlatformGetClipboardFn);


struct WorkQueue;

#define PLATFORM_ADD_WORK(name) void name(WorkQueue *q, WorkerFn *fn, void *data)
// typedef PLATFORM_ADD_WORK(PlatformAddWorkFn);


struct PlatformApi {
#if 0
    union {
        struct {
            PlatformGetClockFn        *get_clock;
            PlatformGetSecsPerClockFn *get_secs_per_clock;;

            PlatformDirectoryIterInitFn *directory_iter_init;
            PlatformDirectoryIterNextFn *directory_iter_next;
            PlatformDirectoryIterEndFn  *directory_iter_end;

            PlatformTextInputStartFn    *text_input_start;
            PlatformTextInputStopFn     *text_input_stop;
            PlatformTextInputSetRectFn  *text_input_set_rect;

            PlatformGetClipboardFn  *get_clipboard;

            PlatformAddWorkFn *add_work;

            void *_terminator;
        };
        void *as_array[10];
    };
#else
    static PLATFORM_GET_CLOCK(get_clock);
    static PLATFORM_GET_SECS_PER_CLOCK(get_secs_per_clock);
    static PLATFORM_DIRECTORY_ITER_INIT(directory_iter_init);
    static PLATFORM_DIRECTORY_ITER_NEXT(directory_iter_next);
    static PLATFORM_DIRECTORY_ITER_END(directory_iter_end);
    static PLATFORM_TEXT_INPUT_START(text_input_start);
    static PLATFORM_TEXT_INPUT_STOP(text_input_stop);
    static PLATFORM_TEXT_INPUT_SET_RECT(text_input_set_rect);
    static PLATFORM_GET_CLIPBOARD(get_clipboard);
    static PLATFORM_ADD_WORK(add_work);
#endif
};

static PlatformApi platform;

#if 0
// verify as_array has the right number of elements
STATIC_ASSERT(ARRAY_COUNT(PlatformApi::as_array) == (STRUCT_OFFSET(PlatformApi, _terminator)/ssizeof(void *)));

static inline void
platform_api_check(PlatformApi *platform)
{
    // Make sure we have everything.
    for (s32 i = 0; i < ARRAY_COUNT(platform->as_array); i++) {
        ASSERT(platform->as_array[i]);
    }
}
#endif

// Audio won't work properly with this, so I'll disable it.
// Technically I could work around it by modifying the song speed and something else.
#define BEAT_TIME_MULTIPLIER 0

struct Font;
struct GameMemory {
    bool is_initialized;

    // PlatformApi platform_api;
    bool get_clipboard_on_next_frame;

#if BEAT_TIME_MULTIPLIER
    f32 debug_time_multiplier;
#endif

    // TODO: permanent and temporary should be dynamic
    MemoryArena permanent; // permanent stuff
    MemoryArena temporary; // for basically everything
    MemoryArena songs_arena; // TODO remove this

    // the things that only exist during the playing state
    MemoryArena playing; // TODO: This can be dynamic and allocated/freed on demand.

    WorkQueue *low_priority_queue; // for stuff that does not have a hard time constraint
    BucketArray<WorkMemory, 16> work_memory_bucketarray; // on permanent // TODO: BulkArray

    struct {
        Font *debug_font0;
        // u32 *debug_font0_textures;
    };

    String clipboard_from_previous_frame; // I trim the final newline if there is one.

    struct {
        u32 white_tex;
        // One big vbo for all the shaders.
        // NOTE: Many VAOs can use the same VBO but each VAO can use only one VBO.
        u32 big_vbo;
        smem big_vbo_size_in_bytes;

        struct {
            u32 program;
            u32 vao;

            // s32 uniform_color_loc;
            s32 projection_loc;
        } simple;

        struct {
            u32 program;
            u32 vao;

            // s32 uniform_color_loc;
            s32 projection_loc;
            s32 do_drop_shadow_loc;
        } sdf;
    } opengl;
};
extern GameMemory *GAME_MEMORY;

void render(GameMemory *gs);

#define GAME_UPDATE_AND_RENDER(name) void name(GameMemory *memory, GameInput *gin,  \
                                               s32 window_width, s32 window_height)
typedef GAME_UPDATE_AND_RENDER(GameUpdateAndRenderFn);


enum DirectoryIterFileType {
    DirectoryIterFileType_Directory,
    DirectoryIterFileType_File,
};

struct DirectoryIter_OsSpecific;
struct DirectoryIter {
    static const s32 MAXIMUM_RECURSIVE_DEPTH = 100;
    bool error; // set to true when an error is found
    bool inited; // set to true on init()
    bool started; // set to true on the first call to next
    bool ended; // set to true when next reaches the end successfully

    bool is_recursive;

    bool started_doing_recursion;
    s32 dir_recurse_idx;

    MemoryArena *arena;
    TempMemory temp_for_end; // gets freed after end()
    String iter_root_dir_nullter;
    NewArrayDynamic<String> dirs_to_recurse; // inside temp_for_end // TODO: Put this in OsSpecific
    DirectoryIter_OsSpecific *os_specific; // inside temp_for_end
    TempMemory temp_for_next; // gets_freed_on next() or end()

    // i think s32 is good enough for this..
    s32 total_count_visited_files;
    s32 total_count_visited_dirs;
    s32 this_dir_count_visited_files;
    s32 this_dir_count_visited_dirs;

    bool do_recurse_for_last_directory;
    s32 recursive_depth; // the count of `prev_iter`s.
    DirectoryIter *prev_iter;

    // maybe rename to unique_parent_id
    // s32 is probably good enough..
    s64 next_unique_dir_id;
    s64 unique_dir_id;

    // for temp_each
    struct {
        bool is_on_iter_root;

        DirectoryIterFileType file_type;

        String full_filename_nullter; // joined with iter_root_dir_nullter
        String full_dir_nullter; // joined with iter_root_dir_nullter

        String rel_filename_nullter; // relative to iter_root_dir_nullter
        String rel_dir_nullter; // relative to iter_root_dir_nullter

        String base_filename_nullter;
    } item;
};

struct GlobalStats {
    smem cmd_buffer_total_num_vertices_simple = 0;
    smem cmd_buffer_total_num_vertices_sdf = 0;
    smem cmd_buffer_len = 0;
};

extern GlobalStats GLOBAL_STATS;

#endif
