#ifndef BEAT_MEMORY_H
#define BEAT_MEMORY_H

struct MemoryArenaFlags {
    bool zero;
    smem alignment;
};

static inline MemoryArenaFlags
arena_flags_default() {
    MemoryArenaFlags result = {};
    result.zero = true;
    result.alignment = -1; // use align_of_for_structs
    return result;
}
static inline MemoryArenaFlags
arena_flags_zero() {
    MemoryArenaFlags result = arena_flags_default();
    result.zero = true;
    return result;
}
static inline MemoryArenaFlags
arena_flags_nozero() {
    MemoryArenaFlags result = arena_flags_default();
    result.zero = false;
    return result;
}
static inline MemoryArenaFlags
arena_flags_zero_align(smem alignment) {
    MemoryArenaFlags result = arena_flags_default();
    result.zero = true;
    result.alignment = alignment;
    return result;
}
static inline MemoryArenaFlags
arena_flags_nozero_align(smem alignment) {
    MemoryArenaFlags result = arena_flags_default();
    result.zero = false;
    result.alignment = alignment;
    return result;
}

struct MemoryArena {
    u8 *base;
    smem size;
    smem used;

    smem high_water_mark;
    s32 num_pushs;
    s32 temp_count;
    void *last_pushed_ptr; // for arena_realloc

    u64 clear_id; // each time clear is called, this is incremented // used to prevent some bugs with newarraydynamic

    // For dynamic arenas
    // s32 num_blocks;
};

struct TempMemory {
    MemoryArena *arena;
    smem used;
    s32 num_pushs;
    s32 temp_count;
};


static void init_arena(MemoryArena *arena, smem size, void *base, bool zero);
static inline void arena_temp_check(MemoryArena *arena, s32 check = 0);
static inline void arena_clear_id_check(MemoryArena *arena, u64 check);
static void arena_clear(MemoryArena *arena);
static void *arena_push_size_(MemoryArena *arena, smem size, smem align_of_for_structs, MemoryArenaFlags flags=arena_flags_default());
static void *arena_realloc(MemoryArena *arena, void *prev_ptr, smem size_in_bytes, smem alignment=-1);
static TempMemory begin_temp_memory(MemoryArena *arena);
static void end_temp_memory(TempMemory *temp);

#define arena_push_size(arena, size, ...) arena_push_size_(arena, size, -1, ##__VA_ARGS__)
#define arena_push_struct_count(arena, T, num, ...) (T *)arena_push_size_(arena, ssizeof(T)*num, alignof(T), ##__VA_ARGS__)
#define arena_push_struct(arena, T, ...) arena_push_struct_count(arena, T, 1, ##__VA_ARGS__)


// usage: SCOPED_TEMP_ARENA(arena); or Scoped_Temp_Arena temp(arena);
struct Scoped_Temp_Arena {
    Scoped_Temp_Arena(MemoryArena *arena) {
        this->temp = begin_temp_memory(arena);
    }
    ~Scoped_Temp_Arena() {
        end_temp_memory(&temp);
    }
    TempMemory temp;
};

#define SCOPED_TEMP_ARENA_JOIN_A(a, b) a ## b
#define SCOPED_TEMP_ARENA_JOIN_B(a, b) SCOPED_TEMP_ARENA_JOIN_A(a, b)
#define SCOPED_TEMP_ARENA_COUNTER(arena, counter) \
    Scoped_Temp_Arena SCOPED_TEMP_ARENA_JOIN_B(_scoped_temp_arena_, counter)(arena);
#define SCOPED_TEMP_ARENA(arena) SCOPED_TEMP_ARENA_COUNTER(arena, __COUNTER__)

#endif
