
static WorkMemory *
begin_work_memory(GameMemory *game_memory)
{
    smem work_memory_size = KILOBYTES(36); // 32k for windows unc paths + 4k extra
#if 0
    for (smem i = 0; i < ARRAY_COUNT(game_memory->work_memory); i++) {
        auto *work_mem = &game_memory->work_memory[i];
        if (!work_mem->being_used) {
            if (!work_mem->is_initialized) {
                work_mem->is_initialized = true;
                init_arena(&work_mem->arena, work_memory_size, NULL, true);
            }
            work_mem->being_used = true;
            work_mem->temp_for_work = begin_temp_memory(&work_mem->arena);
            return work_mem;
        }
    }
    return 0;
#else
    // the reason for deinitialize_on_end is theoretically i could add a bunch
    // of jobs before the other jobs finish and then I'd have a lot of unused
    // WorkMemory's after they all finish.
    auto do_the_thing = [](WorkMemory *work_mem, smem work_memory_size, bool deinitialize_on_end) {
        if (!work_mem->is_initialized) {
            printf("[begin_work_memory] is_initialized was false; initializing with size %ld\n", work_memory_size);
            work_mem->is_initialized = true;
            init_arena(&work_mem->arena, work_memory_size, NULL, true);
        }
        work_mem->being_used = true;
        work_mem->temp_for_work = begin_temp_memory(&work_mem->arena);
        work_mem->deinitialize_on_end = deinitialize_on_end;
        if (work_mem->deinitialize_on_end) {
            printf("[begin_work_memory] deinitialize_on_end was true\n");
        }
    };
    BucketLocation iter;
    auto *work_memory_ba = &game_memory->work_memory_bucketarray;
    for (work_memory_ba->iter_first(&iter);
         work_memory_ba->iter_valid(iter);
         work_memory_ba->iter_next(&iter)) {
        auto *work_mem = work_memory_ba->at_location(iter);
        if (work_mem->being_used)
            continue;
        do_the_thing(work_mem, work_memory_size, iter.bucket > 0);
        return work_mem;
    }
    // ba was full / completely empty.
    {
        if (work_memory_ba->buckets.len <= 0) {
            printf("[begin_work_memory] ba was empty. adding first bucket.\n");
        } else if ((work_memory_ba->total_items_occupied % work_memory_ba->bucket_size) == 0) {
            printf("[begin_work_memory] ba was full of buckets. adding one more bucket.\n");
        } else {
            printf("[begin_work_memory] ba was full of items. adding one more item.\n");
        }
        auto loc = work_memory_ba->push({});
        auto *work_mem = work_memory_ba->at_location(loc);
        do_the_thing(work_mem, work_memory_size, loc.bucket > 0);
        return work_mem;
    }
#endif
}

static void
end_work_memory(WorkMemory *work_mem)
{
    ASSERT(work_mem->is_initialized);
    ASSERT(work_mem->being_used);
    work_mem->being_used = false;
    end_temp_memory(&work_mem->temp_for_work);

    if (work_mem->deinitialize_on_end) {
        printf("[end_work_memory] deinitialize_on_end was true. freeing stuff.\n");
        xfree(work_mem->arena.base);
        *work_mem = {};
    }
}

template <typename T>
static T *
begin_work_async_data_from_arr(NewArrayDynamic<T*> *wasd_arr_specific, void *begin_asd_args = 0)
{
    STATIC_ASSERT((std::is_same<decltype(T::base), WorkAsyncData>::value));
    // STATIC_ASSERT(T::TYPECHECK_WorkAsyncData == 1);
    // STATIC_ASSERT(ABC(T::ENUM_VALUE, WorkAsyncDataType_COUNT));
    T *specific = 0;
    Forv (*wasd_arr_specific, _, it) {
        if (it->base.being_used)
            continue;

        specific = it;
        break;
    }
    if (!specific) {
        bool can_fail = false; // TODO Make this an argument or dependant on the type or whatever.
        if (can_fail) {
            return 0;
        }
        if (wasd_arr_specific->arena) {
            specific = arena_push_struct(wasd_arr_specific->arena, T, arena_flags_zero());
        } else {
            specific = xmalloc_struct(T, xmalloc_flags_zero());
        }
        STATIC_ASSERT(STRUCT_OFFSET(T, base) == 0);
        wasd_arr_specific->push(specific);
    }

    ASSERT(specific);
    *specific = {};
    specific->base.being_used = true;
    specific->base.completed = false;

    // If I find a use case for is_initialized I'll add it again.
    // if (!specific->base.is_initialized) {
    //     init_work_async_data(specific);
    //     specific->base.is_initialized = true;
    // }
    begin_work_async_data(specific, begin_asd_args);
    return specific;
}


static void
end_work_async_data__base(WorkAsyncData *asd)
{
    asd->being_used = false;
    asd->completed = false;
    asd->seconds_elapsed = 0;
}

template <typename T>
static smem
count_work_async_data_being_used(NewArrayDynamic<T*> *wasd_arr_specific)
{
    smem result = 0;
    Forv (*wasd_arr_specific, _, it) {
        if (it->base.being_used)  result += 1;
    }
    return result;
}
