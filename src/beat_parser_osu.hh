#ifndef BEAT_PARSER_OSU_H
#define BEAT_PARSER_OSU_H

#define parser_osu_report_error_v(c, format, ...) do {      \
    if ((c)->report_errors) {                               \
        beat_log("[parser_osu] %.*s: error: " format,       \
                (int)(c)->filename.len, (c)->filename.data, \
                __VA_ARGS__);                               \
    }                                                       \
} while (0);

#define parser_osu_report_error_s(c, format) do {            \
    if ((c)->report_errors) {                                \
        beat_log("[parser_osu] %.*s: error: " format,        \
                (int)(c)->filename.len, (c)->filename.data); \
    }                                                        \
} while (0);


#endif
