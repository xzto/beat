#include "beat_platform.hh"
#include "beat_include.hh"

#include "beat_math.cc"
#include "beat_new_song.cc"
#include "beat_parser_utils.cc"
#include "beat_parser_sm.cc"
#include "beat_parser_ojn.cc"
#include "beat_parser_osu.cc"
#include "beat_parser_bms.cc"

// #include "beat_very_simple_config_file.cc"
#include "beat_simple_ini.cc"
#include "beat_simple_font.cc"
#include "beat_song_asset.cc"

#include "beat_threads.cc"
#include "beat_work_queue.cc"

// for usleep (debug)
#include <unistd.h>

#include "beat_audio.cc"



#define RASTERIZER_FONT_SIZE 40
#define DEBUG_OVERLAY_FONT_SIZE 20
#define SCENE_FIRST_FONT_SIZE 24
#define SCENE_WHEEL_FONT_SIZE 24
#define SCENE_PLAYING_FONT_SIZE 60
#define SCORE_FONT_SIZE 24
#define FADING_TEXT_FONT_SIZE 40

static GameMemory *DEBUG_GLOBAL_MEMORY;

ParserGetFilesetFromFileFn *parser_get_fileset_from_file_table[ParserType_COUNT] = {
    [ParserType_SM] = parser_sm_fileset_from_file,
    [ParserType_OJN] = parser_ojn_fileset_from_file,
    [ParserType_OSU] = parser_osu_fileset_from_file,
    [ParserType_BMS] = parser_bms_fileset_from_file,
};

static inline GameState *
get_game_state(GameMemory *memory)
{
    GameState *gs = (GameState *)memory->permanent.base;
    return gs;
}


#include "beat_bindings.cc"
#include "beat_render_front.cc"




static inline bool
is_snapped_to(f32 a, f32 b)
{
    // I should calculate a good number for this based on our precision and a high bpm and snap
    f32 margin = b * 0.002f;
    ASSERT(!f32_equal_margin(b, 0.0f, 0.0001f));
    f32 fmf;
    // fmf = fmodf(a, b);
    fmf = fmodf(a + margin, b);
    return f32_equal_margin(fmf, margin, margin);// || f32_equal_margin(fmf, b, margin);
}

static inline v4
u32_to_color_v4(u32 u) {
    return V4(((f32)(u8)(((u32)u)>>16))/255.0f,
              ((f32)(u8)(((u32)u)>> 8))/255.0f,
              ((f32)(u8)(((u32)u)>> 0))/255.0f,
              ((f32)(u8)(((u32)u)>>24))/255.0f);
}

static inline v4
get_snapping_type_color(PlayingVisualConfig *vc, SnappingType snap)
{
    ASSERT(snap >= 0 && snap < SnappingType_Count);
#if 0
    if (FRAME_COUNT_ONCE_IN_A_WHILE && snap == SnappingType_Unknown)
        printf("Unknown snap ratio\n");
#endif
    return vc->colors_by_snapping_type[snap];
}

static inline f32
get_window_scale_factor(PlayingVisualConfig *vc, f32 window_height)
{
    f32 result = window_height / 720.0f;
    return result;
}

static f32
calc_pixels_from_bottom(f32 notepos_to_add, f32 notepos_to_multiply, NoteTimestampDiff delta_ts)
{
    // rate_float is already included in notepos_to_multiply
    return notepos_to_add + notepos_to_multiply*(f32)NOTE_SECONDS_FROM_TIMESTAMP__INTERNAL(delta_ts.value);
}

// If you hit too early, decrease it. (In seconds)
f64 debug__global_visual_offset = 0/1000.0;
static void
get_timestamp_to_visual_distance_multipler(PlayingVisualConfig *vc, f32 window_height, f64 rate,
                                           f32 *to_multiply, f32 *to_add)
{
    ASSERT(to_multiply != NULL);
    ASSERT(to_add != NULL);
    ASSERT(rate > 0.1 && rate < 10.0);
    f32 window_scale = get_window_scale_factor(vc, window_height);
    f32 note_width = vc->note_width * window_scale;
    f32 note_spacing = vc->note_spacing * window_scale;
    f32 note_base_height_px = vc->note_base_height * window_scale;

    switch (vc->scroll.type) {
    case SCROLL_TYPE_PIXELS_PER_SECOND: {
        ASSERT(vc->scroll.pixels_per_second > 0);
        if (vc->scroll.scale_with_bpm) {
            ASSERT(!"NOT IMPLEMENTED");
            // f32 scale = 1.0f;
            // ASSERT(vc->scroll.bpm_to_normalize_to > 0.001);
            // if (bpm > 0.00001f) {
            //     scale *= bpm / (f32)vc->scroll.bpm_to_normalize_to;
            // }
            // result = scale * (f32)NOTE_SECONDS_FROM_TIMESTAMP(ts) * (f32)vc->scroll.pixels_per_second;
        } else {
            *to_multiply = window_scale * (f32)vc->scroll.pixels_per_second / (f32)rate;
        }
    } break;
    case SCROLL_TYPE_TIME_TO_APPEAR: {
        ASSERT(vc->scroll.time_to_appear > 1);
        *to_multiply = ((window_height - note_base_height_px) * 1000.0f / (f32)(vc->scroll.time_to_appear)) / (f32)rate;
    } break;
    }
    *to_add = note_base_height_px;
    *to_add += (f32)(-debug__global_visual_offset) * (*to_multiply) * (f32)rate;
}

// NOTE: add_fading_text can be called anywhere, even after rendering took place.
static void
add_fading_text(GameState *gs, String str)
{
    gs->testing.fading_texts.push(FadingText::init(str));
}

static void
sim_and_render_fading_texts(GameMemory *memory, GameState *gs, f64 dt)
{
    f32 font_size_px = FADING_TEXT_FONT_SIZE;
    auto *font = memory->debug_font0;
    f32 font_hi = font_size_px * (font->ascent - font->descent + font->line_gap);
    v2 text_pos = 0.5f*V2((f32)gs->rendering.window_width, (f32)gs->rendering.window_height);

    auto *fts = &gs->testing.fading_texts;
    smem count_drawn = 0;
    for (smem i = fts->len-1; i >= 0; i--) {
        v4 color = V4(1,1,1,1);
        auto *it = &fts->data[i];
        it->seconds_elapsed += dt;
        f64 s1 = 2;
        f64 s2 = 3;
        // TODO:
        const auto draw_text_and_box = [&](String text) {
            // TODO: New rendering api
            auto new_posx = renderer_push_string__internal(memory, &gs->rendering, font_size_px,
                                                           text_pos, color, text, false);
            const auto text_size_x = new_posx - text_pos.x;
            const v2 left_pos = text_pos-V2(text_size_x/2, 0);
            const v2 box_min = left_pos + V2(0, font_size_px*font->descent);
            const v2 box_size = V2(text_size_x, font_size_px);
            const v4 box_color = V4(0, 0, 0, 0.5f * color.a);
            renderer_push_rectangle(&gs->rendering, box_color, box_min, box_size);
            renderer_push_string__internal(memory, &gs->rendering, font_size_px,
                                           left_pos, color, it->text, true);
        };
        if (it->seconds_elapsed < s1) {
            color.a *= 1;
            draw_text_and_box(it->text);
            count_drawn += 1;
            text_pos.y -= font_hi;
        } else if (it->seconds_elapsed < s2) {
            f32 alpha = clamp01((f32)inverse_lerp(s2, s1, it->seconds_elapsed));
            color.a *= alpha;
            draw_text_and_box(it->text);
            count_drawn += 1;
            text_pos.y -= font_hi;
        } else {
            it->deinit();
            xmemmove_struct_count(&fts->data[i], &fts->data[i+1], FadingText, (fts->len-1)-(i+1)+1);
            fts->len -= 1;
        }
    }
}

// export
void
game_report_shit(GameMemory *memory, f64 dt_a, f64 dt_b, f64 dt)
{
}

// does not modify the thing on error
static bool
parse_scroll_speed_method(String str, ScrollConfig *ret_scroll_config)
{
    String lhs, rhs;
    if (!split_by_char_and_trim_whitespace(str, ' ', &lhs, &rhs)) {
        return false;
    }
    if (lhs == "ms"_s) {
        s32 number;
        if (!parse_s32(rhs, &number, 10, SCROLL_LIMIT_MIN, SCROLL_LIMIT_MAX)) {
            return false;
        }
        ret_scroll_config->type = SCROLL_TYPE_TIME_TO_APPEAR;
        ret_scroll_config->time_to_appear= number;
        return true;
    } else if (lhs == "pxps"_s) {
        s32 number;
        if (!parse_s32(rhs, &number, 10, SCROLL_LIMIT_MIN, SCROLL_LIMIT_MAX)) {
            return false;
        }
        ret_scroll_config->type = SCROLL_TYPE_PIXELS_PER_SECOND;
        ret_scroll_config->pixels_per_second = number;
        return true;
    }

    return false;
}

static void init_wheel();

static void
init_game_state(GameMemory *memory, GameState *gs)
{
    zero_struct(gs);

    gs->debug_overlay_active = true;

    {
        // TODO: Split the config file into two later for setting window size, FPS, etc
        TestArgs *test_args = &gs->test_args;
        MemoryArena *arena = &memory->permanent;
        auto *configs = &gs->simple_ini_config;
        if (!simple_ini_file(arena, &memory->temporary,
                             "beat_config.ini"_s,
                             configs)) {
            printf("Failed to load simple ini config file (beat_config.ini at CWD)! quitting!\n");
            exit(1);
        }
        // simple_ini_print(configs);

        auto *root_section = &configs->sections.data[0];

        String mode_str = simple_ini_get_or_default_with_section_ptr(configs, root_section, "mode"_s, ""_s);
        if (mode_str == "wheel_directory_iter"_s) {
            test_args->mode = TestArgGameMode::WheelDirectoryIter;
        } else if (mode_str == "wheel_from_file"_s) {
            test_args->mode = TestArgGameMode::WheelFromFile;
        } else if (mode_str == "wheel_single_file"_s) {
            test_args->mode = TestArgGameMode::WheelSingleFile;
        } else {
            printf("simple_ini file has no 'mode'\n");
            exit(1);
        }
        test_args->wheel_directory_iter = simple_ini_get_or_default_with_section_ptr(configs, root_section, "wheel_directory_iter"_s, ""_s);
        test_args->wheel_from_file = simple_ini_get_or_default_with_section_ptr(configs, root_section, "wheel_from_file"_s, ""_s);
        test_args->wheel_single_file = simple_ini_get_or_default_with_section_ptr(configs, root_section, "wheel_single_file"_s, ""_s);
    }

    // CONFIG1
    {
        for (Keymode keymode = (Keymode)(Keymode_Invalid+1);
             keymode < Keymode_COUNT;
             keymode = (Keymode)(keymode+1)) {
            s32 columns = keymode_to_columns_table[keymode];
            gs->bindings_for_keymodes[keymode] =
                arena_push_struct_count(&memory->permanent, NewArrayDynamic<BeatKey>, columns, arena_flags_zero());
            for (s32 idx_col = 0; idx_col < columns; idx_col++) {
                gs->bindings_for_keymodes[keymode][idx_col].arena = &memory->permanent;
            }
        }
        // mapped keys for keymodes
        // TODO: Default bindings.
        for (Keymode keymode = (Keymode)(Keymode_Invalid+1);
             keymode < Keymode_COUNT;
             keymode = (Keymode)(keymode+1)) {
            s32 columns = keymode_to_columns_table[keymode];
            String keymode_section_name = keymode_to_ini_string_table[keymode];
            String keymode_str = keymode_to_simple_string_table[keymode];

            auto *keymode_section = simple_ini_get_section(&gs->simple_ini_config, keymode_section_name);

            for (s32 idx_col = 0; idx_col < columns; idx_col++) {
                u8 buf[128];
                String buf_str = {};
                // TODO: Assert that it fits in the buffer
                xsnprintf_string(buf, ssizeof(buf), false, &buf_str, "binding_key%d", idx_col+1);
                String val_str;

                NewArrayDynamic<BeatKey> *bindings_for_col = &gs->bindings_for_keymodes[keymode][idx_col];
                ASSERT_SLOW(bindings_for_col->arena);
                if (simple_ini_get_with_section_ptr(&gs->simple_ini_config, keymode_section, buf_str, &val_str)) {
                    // TODO: Ignore MOUSE_WHEEL_UP/DOWN
                    s32 max_bindings = 1;
                    if (keymode == Keymode_5kp1 || keymode == Keymode_7kp1) {
                        if (idx_col == columns-1) {
                            max_bindings = 2;
                        }
                    }
                    string_to_multiple_beat_keys(bindings_for_col, val_str, max_bindings);
                }

                if (bindings_for_col->len <= 0) {
                    printf("Warning: No key binding for keymode %.*s and column %d!\n",
                           EXPAND_STR_FOR_PRINTF(keymode_str), idx_col+1);
                }
            }
        }

#define DOIT(name, def) do {                                                        \
    u8 buf[128];                                                                    \
    String buf_str = {};                                                            \
    xsnprintf_string(buf, ssizeof(buf), false, &buf_str,                            \
                     "binding_%.*s", EXPAND_STR_FOR_PRINTF(string_literal(#name))); \
    String the_str;                                                                 \
    auto *binding = &gs->bindings.name;                                             \
    binding->arena = &memory->permanent;                                            \
    if (simple_ini_get(&gs->simple_ini_config, "_root"_s, buf_str, &the_str)) {     \
        string_to_multiple_key_bindings(binding, the_str);                          \
    } else {                                                                        \
        auto d = def;                                                               \
        if (d.key) binding->push(d);                                                \
    }                                                                               \
    num_mapped++;                                                                   \
} while (0);

        // other mapped keys
        s32 num_mapped = 0;
        DOIT(playing_restart,           make_keybinding(BEAT_KEY_GRAVE));
        DOIT(scroll_faster,             make_keybinding(BEAT_KEY_F4));
        DOIT(scroll_slower,             make_keybinding(BEAT_KEY_F3));
        DOIT(debug_virtual_time_faster, make_keybinding(BEAT_KEY_F10,   KeyboardMods_Ctrl));
        DOIT(debug_virtual_time_slower, make_keybinding(BEAT_KEY_F9,    KeyboardMods_Ctrl));

        DOIT(wheel_rate_faster,         make_keybinding(BEAT_KEY_EQUALS));
        DOIT(wheel_rate_slower,         make_keybinding(BEAT_KEY_MINUS));
        DOIT(wheel_left,                make_keybinding(BEAT_KEY_LEFT));
        DOIT(wheel_right,               make_keybinding(BEAT_KEY_RIGHT));
        DOIT(wheel_up,                  make_keybinding(BEAT_KEY_UP));
        DOIT(wheel_down,                make_keybinding(BEAT_KEY_DOWN));
        DOIT(wheel_enter,               make_keybinding(BEAT_KEY_ENTER));
        DOIT(wheel_back,                make_keybinding(BEAT_KEY_ESCAPE));
        DOIT(wheel_remove_and_reload,   make_keybinding(BEAT_KEY_F5));
        DOIT(wheel_sort,                make_keybinding(BEAT_KEY_F6));
        DOIT(toggle_debug_overlay,      make_keybinding(BEAT_KEY_F1));
        DOIT(toggle_show_keypress,      make_keybinding(BEAT_KEY_F1,        KeyboardMods_Ctrl));
        DOIT(toggle_debug_show_text_rect,   make_keybinding(BEAT_KEY_F1,    KeyboardMods_Shift));
        DOIT(playing_seek_negative,     make_keybinding(BEAT_KEY_F7));
        DOIT(playing_seek_positive,     make_keybinding(BEAT_KEY_F8));
        DOIT(playing_pause,             make_keybinding(BEAT_KEY_F12));
        DOIT(wheel_global_audio_offset_plus,  make_keybinding(BEAT_KEY_EQUALS,    KeyboardMods_Ctrl));
        DOIT(wheel_global_audio_offset_minus, make_keybinding(BEAT_KEY_MINUS,     KeyboardMods_Ctrl));
        DOIT(wheel_global_visual_offset_plus,  make_keybinding(BEAT_KEY_EQUALS,   KeyboardMods_Alt));
        DOIT(wheel_global_visual_offset_minus, make_keybinding(BEAT_KEY_MINUS,    KeyboardMods_Alt));
        DOIT(wheel_toggle_search,       make_keybinding(BEAT_KEY_F,         KeyboardMods_Ctrl));
        // Make sure I mapped everything
        ASSERT_MSG(num_mapped == ARRAY_COUNT(gs->bindings.as_array), "Didn't map every key in GameState::bindings");
#undef DOIT

        // test for overlaps
        // some overlaps don't hurt like wheel_sort and keymode_X_keyY, but I report them anyway
        for (s32 i = 0; i < ARRAY_COUNT(gs->bindings.as_array); i++) {
            bool no_bindings = true;
            auto *bs = &gs->bindings.as_array[i];
            for (s32 idx_b = 0; idx_b < bs->len; idx_b++) {
                if (bs->data[idx_b].key) {
                    no_bindings = false;
                    break;
                }
            }
            if (no_bindings) {
                printf("Warning: No key binding for bindings.as_array[%d]\n", i);
            }
        }

        for (s32 i = 0; i < ARRAY_COUNT(gs->bindings.as_array); i++) {
            auto *bs_i = &gs->bindings.as_array[i];
            for (s32 idx_bs_i = 0; idx_bs_i < bs_i->len; idx_bs_i++) {
                KeyBinding key_i = bs_i->data[idx_bs_i];
                if (!key_i.key) continue;
                for (s32 j = i+1; j < ARRAY_COUNT(gs->bindings.as_array); j++) {
                    auto *bs_j = &gs->bindings.as_array[j];
                    for (s32 idx_bs_j = 0; idx_bs_j < bs_j->len; idx_bs_j++) {
                        KeyBinding key_j = bs_j->data[idx_bs_j];
                        if (key_i.key == key_j.key && keyboard_mods_match(key_i.mods, key_j.mods)) {
                            // TODO: Show string instead of index to as_array
                            printf("Warning: bindings.as_array keys overlap! idx %d and %d\n", i, j);
                        }
                    }
                }

                for (Keymode keymode = (Keymode)(Keymode_Invalid+1);
                     keymode < Keymode_COUNT;
                     keymode = (Keymode)(keymode+1)) {
                    s32 columns = keymode_to_columns_table[keymode];
                    String keymode_str = keymode_to_simple_string_table[keymode];

                    for (s32 idx_col = 0; idx_col < columns; idx_col++) {
                        auto *bindings_for_key = &gs->bindings_for_keymodes[keymode][idx_col];
                        for (s32 idx_arr_inner = 0; idx_arr_inner < bindings_for_key->len; idx_arr_inner++) {
                            BeatKey key_inner = bindings_for_key->data[idx_arr_inner];
                            if (key_inner == key_i.key) {
                                // TODO: Show string instead of index to as_array
                                printf("Warning: bindings.as_array idx %d overlaps with keymode %.*s column %d\n",
                                       i, EXPAND_STR_FOR_PRINTF(keymode_str), idx_col+1);
                            }
                        }
                    }
                }
            }
        }

        for (Keymode keymode = (Keymode)(Keymode_Invalid+1);
             keymode < Keymode_COUNT;
             keymode = (Keymode)(keymode+1)) {
            s32 columns = keymode_to_columns_table[keymode];
            String keymode_str = keymode_to_simple_string_table[keymode];
            for (s32 idx_col_outer = 0; idx_col_outer < columns; idx_col_outer++) {
                auto *bindings_for_key_outer = &gs->bindings_for_keymodes[keymode][idx_col_outer];
                for (s32 idx_arr_outer = 0; idx_arr_outer < bindings_for_key_outer->len; idx_arr_outer++) {
                    BeatKey key_outer = bindings_for_key_outer->data[idx_arr_outer];
                    if (!key_outer) continue;
                    for (s32 idx_col_inner = idx_col_outer+1; idx_col_inner < columns; idx_col_inner++) {
                        auto *bindings_for_key_inner = &gs->bindings_for_keymodes[keymode][idx_col_inner];
                        for (s32 idx_arr_inner = 0; idx_arr_inner < bindings_for_key_inner->len; idx_arr_inner++) {
                            BeatKey key_inner = bindings_for_key_inner->data[idx_arr_inner];
                            if (key_inner == key_outer) {
                                printf("Warning: bindings_for_keymode keymode %.*s column %d overlaps with column %d\n",
                                       EXPAND_STR_FOR_PRINTF(keymode_str), idx_col_outer+1, idx_col_inner+1);
                            }
                        }
                    }
                }
            }
        }
    }

    auto *root_section = &gs->simple_ini_config.sections.data[0];

    {
        // get visual and audio offset (in seconds)
        // if you hit too late, increase it; if you hit too early, decrease it.
        String global_visual_offset_str = simple_ini_get_or_default_with_section_ptr(
            &gs->simple_ini_config, root_section,
            "global_visual_offset"_s, "0"_s);
        f64 f = 0;
        if (!parse_f64(global_visual_offset_str, &f)) f = 0;
        debug__global_visual_offset = f;
        String global_audio_offset_str = simple_ini_get_or_default_with_section_ptr(
            &gs->simple_ini_config, root_section,
            "global_audio_offset"_s, "0"_s);
        f = 0;
        if (!parse_f64(global_audio_offset_str, &f)) f = 0;
        audio::debug__sound_global_offset = f;
    }

    String default_scroll_method_str = simple_ini_get_or_default_with_section_ptr(&gs->simple_ini_config, root_section,
                                                                                  "scroll_speed_method"_s, "ms 600"_s);

    ScrollConfig default_scroll_config = {};
    default_scroll_config.type = SCROLL_TYPE_TIME_TO_APPEAR;
    default_scroll_config.time_to_appear = 600;
    if (!parse_scroll_speed_method(default_scroll_method_str, &default_scroll_config)) {
        printf("Warning: scroll_speed_method is wrong! '%.*s'\n", EXPAND_STR_FOR_PRINTF(default_scroll_method_str));
    }

    for (Keymode keymode = (Keymode)(Keymode_Invalid+1);
         keymode < Keymode_COUNT;
         keymode = (Keymode)(keymode+1)) {
        String keymode_section_name = keymode_to_ini_string_table[keymode];
        auto *keymode_section = simple_ini_get_section(&gs->simple_ini_config, keymode_section_name);

        ScrollConfig scroll_config = default_scroll_config;
        {
            String scroll_speed_method_str;
            if (simple_ini_get_with_section_ptr(&gs->simple_ini_config, keymode_section,
                                                "scroll_speed_method"_s, &scroll_speed_method_str)) {
                if (!parse_scroll_speed_method(scroll_speed_method_str, &scroll_config)) {
                    printf("Warning: scroll_speed_method is wrong! '%.*s'\n", EXPAND_STR_FOR_PRINTF(scroll_speed_method_str));
                }
            }
        }

        PlayingVisualConfig *vc = &gs->playing_visual_configs_per_keymode[keymode];
        vc->scroll.scale_with_bpm = false;
        vc->scroll = scroll_config;

        vc->note_base_height = 140;
        vc->note_width = 80.0f;
        vc->note_spacing = 4.0f;

        vc->receiver_color         = V4(1.0f, 1.0f, 1.0f, 0.4f);
        vc->receiver_pressed_color = V4(1.0f, 1.0f, 1.0f, 1.0f);
        vc->background_color       = V4(0.0f, 0.0f, 0.0f, 0.9f);
        vc->playfield_color        = V4(0.0f, 0.0f, 0.0f, 1.0f);

        if (keymode == Keymode_7k) {
            vc->note_width *= 0.5f;
            vc->note_spacing = 0;
            // vc->note_base_height = 80;
            vc->note_coloring_by_column = true;
            v4 colors[7] = {
                u32_to_color_v4(0xffffffff),
                u32_to_color_v4(0xff00ffff),
                u32_to_color_v4(0xffffffff),

                u32_to_color_v4(0xffffff00),

                u32_to_color_v4(0xffffffff),
                u32_to_color_v4(0xff00ffff),
                u32_to_color_v4(0xffffffff),
            };
            vc->colors_by_column_count = ARRAY_COUNT(colors);
            vc->colors_by_column = arena_push_struct_count(&memory->permanent, v4, vc->colors_by_column_count, arena_flags_nozero());
            xmemcpy(vc->colors_by_column, colors, ssizeof(colors));
        } else if (keymode == Keymode_7kp1) {
            vc->note_width *= 0.5f;
            vc->note_spacing = 0;
            // vc->note_base_height = 80;
            vc->note_coloring_by_column = true;
            v4 colors[8] = {
                u32_to_color_v4(0xffffffff),
                u32_to_color_v4(0xff00ffff),
                u32_to_color_v4(0xffffffff),

                u32_to_color_v4(0xffffff00),

                u32_to_color_v4(0xffffffff),
                u32_to_color_v4(0xff00ffff),
                u32_to_color_v4(0xffffffff),

                u32_to_color_v4(0xffff4400),
            };
            vc->colors_by_column_count = ARRAY_COUNT(colors);
            vc->colors_by_column = arena_push_struct_count(&memory->permanent, v4, vc->colors_by_column_count, arena_flags_nozero());
            xmemcpy(vc->colors_by_column, colors, ssizeof(colors));
        } else if (keymode == Keymode_9k) {
            vc->note_width *= 0.5f;
            vc->note_spacing = 0;
            vc->note_coloring_by_column = true;
            v4 colors[9] = {
                u32_to_color_v4(0xff00ff00),

                u32_to_color_v4(0xffffffff),
                u32_to_color_v4(0xff00ffff),
                u32_to_color_v4(0xffffffff),

                u32_to_color_v4(0xffffff00),

                u32_to_color_v4(0xffffffff),
                u32_to_color_v4(0xff00ffff),
                u32_to_color_v4(0xffffffff),

                u32_to_color_v4(0xff00ff00),
            };
            vc->colors_by_column_count = ARRAY_COUNT(colors);
            vc->colors_by_column = arena_push_struct_count(&memory->permanent, v4, vc->colors_by_column_count, arena_flags_nozero());
            xmemcpy(vc->colors_by_column, colors, ssizeof(colors));
        } else {
            vc->note_coloring_by_column = false;
            v4 colors[SnappingType_Count] = {
                [SnappingType_Unknown] = u32_to_color_v4(0xffa6a6a6),
                [SnappingType_1_1] = u32_to_color_v4(0xfffd0000),
                [SnappingType_1_2] = u32_to_color_v4(0xff003ffd),
                [SnappingType_1_4] = u32_to_color_v4(0xff00fd00),
                [SnappingType_1_8] = u32_to_color_v4(0xfffdfd00),
                [SnappingType_1_3] = u32_to_color_v4(0xfffd00bd),
                [SnappingType_1_6] = u32_to_color_v4(0xfffda200),
                [SnappingType_1_12] = u32_to_color_v4(0xff00fdfd),
                [SnappingType_1_24] = u32_to_color_v4(0xffa6a6a6),
            };
            xmemcpy(vc->colors_by_snapping_type, colors, ssizeof(colors));
        }
    }

    gs->testing.fading_texts.arena = &memory->permanent;

    gs->acc_system.type = AccSystemType_Mytestwife3;
    gs->life_system.type = LifeSystemType_Testdonothing;

    {
        gs->work_async_data_arr_wheel_add_directory.arena = &memory->permanent;
        gs->work_async_data_arr_wheel_add_directory.reserve(2);

        gs->work_async_data_arr_load_font_chunk.arena = &memory->permanent;
        gs->work_async_data_arr_load_font_chunk.reserve(8);
    }

    init_wheel();
}

// on render.c
void renderer_load_gl_crap(GameMemory *memory);

static void
debug_game_load_font0(GameMemory *memory)
{
    auto start_clock = platform.get_clock();
    bool valid = true;
    // String filename = "thefont.ttf"_s;
    String filename = simple_ini_get_or_default(&get_game_state(memory)->simple_ini_config, "_root"_s, "thefont"_s, "thefont.ttf"_s);


    EntireFile ef = {};
    valid = read_entire_file_nullter(filename, &ef);
    if (!valid) {
        ASSERT_FMT(false, "failed to open font file '%.*s'", EXPAND_STR_FOR_PRINTF(filename));
    }

    f32 font_size_px = RASTERIZER_FONT_SIZE;
    memory->debug_font0 = arena_push_struct(&memory->permanent, Font);
    valid = new_make_simple_font_from_ttf_buffer(ef, font_size_px, memory->debug_font0, &memory->temporary);
    if (!valid) {
        ASSERT(!"make_simple_font_from_ttf_buffer failed");
    }
    auto end_clock = platform.get_clock();
    f64 seconds_elapsed = (f64)(end_clock - start_clock) * platform.get_secs_per_clock();
    {
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(
            buf, ssizeof(buf), false, &bufstr,
            "initial font loading took %.2fms for %d codepoints (%.2fms/codepoint)",
            1000 * seconds_elapsed,
            memory->debug_font0->total_glyph_count,
            1000*seconds_elapsed/(f64)(memory->debug_font0->total_glyph_count));
        printf("%s\n", buf);
        auto *gs = get_game_state(memory);
        add_fading_text(gs, bufstr);
    }

    auto const queue_load_codepoint_range = [&](u32 lo, u32 hi){
        for (u32 codepoint = lo; codepoint < hi; codepoint += FONT_CHUNK_SIZE)
            queue_font_chunk_for_codepoint(memory->debug_font0, codepoint);
    };
    queue_load_codepoint_range(0x100, 0x24f);
    queue_load_codepoint_range(0xff00, 0xff5e);
    queue_load_codepoint_range(0x2e80, 0x3100);
    // queue_load_codepoint_range(0x3400, 0x4db5);
    // queue_load_codepoint_range(0x4e00, 0x9fcb);
    queue_load_codepoint_range(0xf9aa, 0xfa6a);

    // Opengl textures
    renderer_load_gl_crap(memory);
}

static void
init_game_memory(GameMemory *memory)
{
#if 0
    platform = memory->platform_api;
    platform_api_check(&platform);
#endif

    DEBUG_GLOBAL_MEMORY = memory;

    memory->work_memory_bucketarray.buckets.arena = &memory->permanent;

    GameState *gs = get_game_state(memory);
    init_game_state(memory, gs);
    debug_game_load_font0(memory);
}


#define LAST_REPORTED_AUDIO_SOUND_TIME_FOR_SET_PLAYING_TIME_TS (-123123.51283129381239184)

// set_playing_time_ts does NOT take rate into account.
static void
set_playing_time_ts(GameState *gs, NoteTimestamp new_song_ts, /* u64 os_clock_frame_start, */ f64 os_time_seconds_frame_start)
{
    ASSERT(gs->scene == SCENE_PLAYING);
    ASSERT(gs->playing.other.state >= PlayingState_Ready);
    ASSERT(new_song_ts < NOTE_MAX_TIMESTAMP);
    ASSERT(new_song_ts > seconds_to_song_ts(-100, (f64)gs->playing.params.rate_float));
    auto old_song_ts = gs->playing.other.newthings.song_ts;
    gs->playing.other.newthings.song_ts = new_song_ts;
    if (gs->playing.other.state >= PlayingState_Started) {
        gs->playing.other.acc_index = 0;
        gs->playing.other.acc_total = 0;

        if (new_song_ts < old_song_ts) {
            for (s32 idx_col = 0; idx_col < gs->playing.other.columns; idx_col++) {
                auto *per_col = &gs->playing.other.per_column[idx_col];
                // Clear inputs.
                per_col->tis.len = 0;
                // Clear visible notes.
                // This will break stuff because I just ASSERT(false) if a hold end is not found.
                // I should instead add the hold start for that hold end.
                // Might break input too (maybe not).
                per_col->vns.len = 0;

                per_col->debug_test_accs.len = 0;
            }
        }
    }

    // TODO: Set reference to right now instead of os_time_seconds_frame_start
    gs->playing.other.reference.os_time_seconds = os_time_seconds_frame_start;
    // gs->playing.other.reference.os_clock = os_clock_frame_start;
    gs->playing.other.reference.song_ts = new_song_ts;
    gs->playing.other.reference.last_reported_audio_sound_time = LAST_REPORTED_AUDIO_SOUND_TIME_FOR_SET_PLAYING_TIME_TS;

    gs->playing.other.score = 0.0;
    gs->playing.other.max_score_right_now = 0.0;

    gs->playing.other.reference.last_seek_frame_count = global_frame_count;

    Diff *diff = gs->playing.params.diff;
    gs->playing.other.newthings.curr_row_idx =
        find_row_idx_by_similar_timestamp(&diff->notes, new_song_ts, 0, false);
    gs->playing.other.newthings.curr_bpm_idx =
        find_beat_idx_by_timestamp(&diff->notes.timings, new_song_ts, 0, NULL, false);
    if (gs->playing.other.state >= PlayingState_Started) {
        gs->playing.other.state = PlayingState_Started;
    } else {
        gs->playing.other.state = PlayingState_Ready;
    }

    if (gs->playing.other.debug_the_audio_decoder_id.is_valid()) {
        audio::start_playing_sound(gs->playing.other.debug_the_audio_decoder_id, (f32)gs->playing.params.rate_float,
                                   note_timestamp_diff_to_seconds({new_song_ts.value}, gs->playing.params.rate_float));
    }
}

// set_playing_time_seconds does take rate into account.
static void
set_playing_time_seconds(GameState *gs, f64 song_time_seconds, /* u64 os_clock_frame_start, */ f64 os_time_seconds_frame_start)
{
    ASSERT(gs->scene == SCENE_PLAYING);
    ASSERT(gs->playing.other.state >= PlayingState_Ready);
    NoteTimestamp new_song_ts = seconds_to_song_ts(song_time_seconds, (f64)gs->playing.params.rate_float);
    set_playing_time_ts(gs, new_song_ts, /* os_clock_frame_start, */ os_time_seconds_frame_start);
}

// TODO: Use the arenas
static bool
game_open_file_recognize_ext(MemoryArena *final_arena, MemoryArena *temp_arena, bool report_errors,
                             String filename, Fileset *ret_fileset) {
    ASSERT(final_arena != NULL);
    ASSERT(temp_arena != NULL);
    ASSERT(string_exists_and_is_not_empty(filename));
    bool valid;
    s32 parser = get_parser_from_filename(filename);
    ParserGetFilesetFromFileFn *fn = parser_get_fileset_from_file_table[parser];
    valid = fn != NULL;
    if (!valid) {
        beat_log("game_open_file_recognize_ext: Ignoring file '%.*s'. Unknown extension.\n", EXPAND_STR_FOR_PRINTF(filename));
        return false;
    }

    // This time includes the filesystem read
    u64 clock_parse_before = platform.get_clock();
    valid = fn(final_arena, temp_arena, filename, report_errors, ret_fileset);
    u64 clock_parse_after = platform.get_clock();

#if 0
    String parser_ext = get_file_extension(filename);
    f64 seconds_to_parse = (f64)(clock_parse_after - clock_parse_before)*platform.get_secs_per_clock();
    beat_log("Time to parse (%s) '%.*s': %.2fms\n",
             (int)valid ? "ok" : "error",
             (int)parser_ext.len, parser_ext.data,
             1000.0 * seconds_to_parse);
#endif

    if (!valid) {
        // beat_log(" [*FAIL*] %.*s\n", filename.len, filename.data);
        return false;
    }

    return true;
}

#include "beat_new_song_gameplay.cc"

static void
enter_scene(GameMemory *memory, GameState *gs, SceneEnum scene)
{
    platform.text_input_stop();
    gs->last_scene = gs->scene;
    gs->scene = scene;
    switch (scene) {
    case SCENE_FIRST: {
        // ASSERT(false);
    } break;
    case SCENE_WHEEL: {
    } break;
    case SCENE_PLAYING: {
    } break;
    }
}

static void
draw_debug_overlay(GameMemory *memory, GameState *gs, GameInput *gin)
{
    Rendering *rendering = &gs->rendering;
    smem rendering_total_entries_count_before_debug_overlay = rendering->entries.len;

    f32 font_size_px = DEBUG_OVERLAY_FONT_SIZE;

    v4 text_color = u32_to_color_v4(0xffffffff);

    auto *font = memory->debug_font0;
    f32 font_hi = font_size_px*(font->ascent - font->descent + font->line_gap);
    v2 pad = V2(5.0f, 5.0f);
    s32 probably_max_num_chars = 50;
    probably_max_num_chars += 5;
    FontGlyph *glyph_a = codepoint_to_glyph(font, 'A');
    if (!glyph_a) return;
    ASSERT(glyph_a);
    // We'll estimate the string length in pixels with the 'A' glyph's advance.
    f32 a_width = font_size_px*glyph_a->advance;
    s32 num_lines = 3;
    if (gs->scene == SCENE_PLAYING) {
        num_lines += 2;
        probably_max_num_chars += 12;
    }
#if BEAT_TIME_MULTIPLIER
    num_lines += 1;
#endif
    v2 rect_size = V2((f32)probably_max_num_chars*a_width + 2.0f*pad.x,
                      num_lines*font_hi + 2.0f*pad.y);
    v2 rect_max = V2((f32)rendering->window_width - 10.0f,
                     10.0f + rect_size.y);
    v2 rect_min = rect_max - rect_size;
    v2 text_at = rect_min + V2(pad.x, pad.y - font_size_px*font->descent);
    renderer_push_rectangle(rendering, V4(0.4f, 0.4f, 0.4f, 0.6f), rect_min, rect_size);

    // Drawing the text from bottom to top

    // Taking the average frame time because the current frame time changes too much every
    // frame to be able to read it.
#define SZ 32
    static f64 last_dt_frame[SZ] = {};
    static f64 last_dt_a[SZ] = {};
    static f64 last_dt_b[SZ] = {};
    static f64 last_dt_c[SZ] = {};
    static s32 thing_frame_idx = 0;
    last_dt_frame[thing_frame_idx] = gin->debug_dt_frame;
    last_dt_a[thing_frame_idx]     = gin->debug_dt_a;
    last_dt_b[thing_frame_idx]     = gin->debug_dt_b;
    last_dt_c[thing_frame_idx]     = gin->debug_dt_c;
    thing_frame_idx = (thing_frame_idx + 1) % SZ;
    f64 the_dt_frame = 0;
    f64 the_dt_a = 0;
    f64 the_dt_b = 0;
    f64 the_dt_c = 0;
    for (s32 i = 0; i < SZ; i++) {
        the_dt_frame += last_dt_frame[i] / (f64)SZ;
        the_dt_a     += last_dt_a[i]     / (f64)SZ;
        the_dt_b     += last_dt_b[i]     / (f64)SZ;
        the_dt_c     += last_dt_c[i]     / (f64)SZ;
    }
#undef SZ

    // Show ms/frame and fps
    {
        u8 buf[128];
        String temp_str = {};
        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "dt_a: %5.2fms  " // 15 chars for dt=10ms
                         "dt_b: %5.2fms  " // 15 chars
                         "dt_c: %5.2fms  " // 15 chars
                         "dt_f: %5.2fms  " // 13 chars
                         "%4.1ffps", // 8 chars
                         1000.0*the_dt_a,
                         1000.0*the_dt_b,
                         1000.0*the_dt_c,
                         1000.0*the_dt_frame,
                         1.0/the_dt_frame);
                         // (s32)round(1.0/the_dt_frame));
        renderer_push_string(memory, rendering, DEBUG_OVERLAY_FONT_SIZE, text_at, text_color, temp_str);
    }
    text_at.y += font_hi;

    // Show rendering stats (it doesn't count everything on the debug overlay)
    // Also show time multiplier here.

    {
        u8 buf[128];
        String temp_str = {};
        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "entries: %ld  " // 11+6 max (probably)
                         "cmds: %ld  "
                         "verts simple: %ld  "
                         "verts sdf: %ld  "
                         ,
                         rendering_total_entries_count_before_debug_overlay,
                         GLOBAL_STATS.cmd_buffer_len,
                         GLOBAL_STATS.cmd_buffer_total_num_vertices_simple,
                         GLOBAL_STATS.cmd_buffer_total_num_vertices_sdf
        );
        renderer_push_string(memory, rendering, DEBUG_OVERLAY_FONT_SIZE, text_at, text_color, temp_str);
        text_at.y += font_hi;
    }

#if BEAT_TIME_MULTIPLIER
    {
        u8 buf[128];
        String temp_str = {};
        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "time mult: %.2f  ", // 13+4 max (probably)
                         (f64)memory->debug_time_multiplier
        );
        renderer_push_string(memory, rendering, DEBUG_OVERLAY_FONT_SIZE, text_at, text_color, temp_str);
        text_at.y += font_hi;
    }
#endif

    {
        u8 buf[128];
        String temp_str = {};
        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "temp: %3ldKB  " // 10+4
                         "perm: %3ldKB  " // 10+4
                         "songs: %3ldKB  " // 11+4
                         "playing: %3ldKB  " // 13+4
                         ,
                         (memory->temporary.high_water_mark   + 1023) / 1024,
                         (memory->permanent.high_water_mark   + 1023) / 1024,
                         (memory->songs_arena.high_water_mark + 1023) / 1024,
                         (memory->playing.high_water_mark     + 1023) / 1024);
        renderer_push_string(memory, rendering, DEBUG_OVERLAY_FONT_SIZE, text_at, text_color, temp_str);
        text_at.y += font_hi;
    }
    // Show playing stats
    if (gs->scene == SCENE_PLAYING && gs->playing.other.state == PlayingState_Started) {
        Diff *diff = gs->playing.params.diff;
        s32 total_vns = 0;
        for (s32 idx_col = 0; idx_col < gs->playing.other.columns; idx_col++) {
            total_vns += gs->playing.other.per_column[idx_col].vns.len;
        }
        f64 rate_float = gs->playing.params.rate_float;
        f64 playing_raw_seconds = note_timestamp_diff_to_seconds((NoteTimestampDiff)gs->playing.other.newthings.song_ts, rate_float);
        s32 playing_time_min, playing_time_sec;
        auto playing_time = get_minutes_and_seconds(playing_raw_seconds);

        // Adding 2.1 seconds because I only say it ended after 2 seconds passed after the last note.
        // NoteTime dur_ts = NOTE_TIMESTAMP_FROM_SECONDS((f64)gs->playing.params.rate_float*2.0) + diff->notes.timestamps[diff->notes.rows_count-1];
        NoteTimestamp dur_ts = diff->notes.timestamps[diff->notes.rows_count-1];
        f64 duration_raw_seconds = note_timestamp_diff_to_seconds((NoteTimestampDiff)dur_ts, rate_float);
        s32 song_duration_min, song_duration_sec;
        auto song_duration = get_minutes_and_seconds(duration_raw_seconds);
        ASSERT(!song_duration.is_negative);

        f64 remaining_raw_secs = duration_raw_seconds - playing_raw_seconds;
        s32 remaining_time_min, remaining_time_sec;
        auto remaining_time = get_minutes_and_seconds(remaining_raw_secs);

        {
            u8 buf[128];
            String temp_str = {};
            xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                             "ptime: %s%d:%02d  "
                             "rem time: %s%d:%02d  "
                             "dur: %d:%02d  "
                             // "rate: %d.%02d  "
                             "rate: %.2f  "
                             "vns: %d  ",
                             playing_time.is_negative ? "-" : "",
                             playing_time.minutes, playing_time.seconds,
                             remaining_time.is_negative ? "-" : "",
                             remaining_time.minutes, remaining_time.seconds,
                             song_duration.minutes, song_duration.seconds,
                             (f64)gs->playing.params.rate_float,
                             total_vns);
            renderer_push_string(memory, rendering, DEBUG_OVERLAY_FONT_SIZE, text_at, text_color, temp_str);
            text_at.y += font_hi;
        }
        {
            auto scroll = gs->playing.other.visual_config->scroll;
            s32 scroll_number = 0;
            char *scroll_type_cstr;
            if (scroll.type == SCROLL_TYPE_TIME_TO_APPEAR) {
                scroll_number = scroll.time_to_appear;
                scroll_type_cstr = "TTA";
            } else if (scroll.type == SCROLL_TYPE_PIXELS_PER_SECOND) {
                scroll_number = scroll.pixels_per_second;
                scroll_type_cstr = "PXPS";
            } else {
                scroll_type_cstr = "idk";
            }
            u8 buf[128];
            String temp_str = {};
            xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                             // "notes: %d  "
                             "rows: %d  "
                             "scroll: %s %d ",
                             // note_count,
                             diff->notes.rows_count,
                             scroll_type_cstr,
                             scroll_number);
            renderer_push_string(memory, rendering, DEBUG_OVERLAY_FONT_SIZE, text_at, text_color, temp_str);
            text_at.y += font_hi;
        }
    }


    // renderer_push_diamond(rendering, V4(1,0,0,1), V2(600,600), 100);
    // renderer_push_diamond(rendering, V4(1,0,0,1), V2(5,5), 100);
}

static void
scene_first_logic(GameMemory *memory, GameState *gs, GameInput *gin)
{
    SceneFirst *first = &gs->scene_first;
#if 0
    enter_scene(memory, gs, SCENE_WHEEL);
    return;
#endif

    // Draw the Background
    {
        v4 color = u32_to_color_v4(0xffaaaa00);
        v2 min = V2(0, 0);
        v2 size = V2((f32)gs->rendering.window_width, (f32)gs->rendering.window_height);
        renderer_push_rectangle(&gs->rendering, color, min, size);
    }

    String options[] = {
        "Wheel"_s,
        "Quit"_s,
    };
    s32 options_count = ARRAY_COUNT(options);
    ASSERT(first->selected_option < options_count);

    if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_down)) {
        first->selected_option = (first->selected_option + 1) % options_count;
    } else if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_up)) {
        first->selected_option = (first->selected_option - 1 + options_count) % options_count;
    } else if (key_binding_pressed(gin, gs->bindings.wheel_enter)) {
        switch (first->selected_option) {
        case 0: enter_scene(memory, gs, SCENE_WHEEL); break;
        case 1: global_running = false; break;
        }
    }

    Font *font = memory->debug_font0;
    f32 font_size_px = SCENE_FIRST_FONT_SIZE;
    f32 font_hi = font_size_px*(font->ascent - font->descent + font->line_gap);
    f32 border = 4.0f;

    v4 text_color = u32_to_color_v4(0xffffffff);
    // TODO: get_font_string_length(font, str, font_height_px);
    v4 color_button_normal = u32_to_color_v4(0xff1d1d1d);
    v4 color_button_selected = u32_to_color_v4(0xff4d4d1d);
    v4 color_separator_normal = u32_to_color_v4(0xffaaaaaa);
    v4 color_separator_selected = u32_to_color_v4(0xffffffff);
    v2 rect_size = V2(200.0f, font_hi + border);
    v2 rect_border_size = V2(rect_size.x, 8.0f);
    v2 rect_total = V2(rect_size.x, rect_size.y + rect_border_size.y);
    v2 half_window_size = V2(gs->rendering.window_width*0.5f, gs->rendering.window_height*0.5f);
    v2 pos = half_window_size - hadamard_mult(rect_total, V2(0.5f, 0.5f*(f32)options_count));

    f32 text_pad_x = 4;
    for (s32 idx_opt = 0; idx_opt < options_count; idx_opt++) {
        v4 col, col_separator;
        if (idx_opt == first->selected_option) {
            col = color_button_selected;
            col_separator = color_separator_selected;
        } else {
            col = color_button_normal;
            col_separator = color_separator_normal;
        }
        v2 min = pos - V2(0, idx_opt*rect_total.y);
        v2 rect_min = V2(min.x, min.y + font_size_px*font->descent - border);
        renderer_push_rectangle(&gs->rendering, col, rect_min, rect_size);
        renderer_push_rectangle(&gs->rendering, col_separator, rect_min - V2(0, rect_border_size.y), rect_border_size);

        renderer_push_string(memory, &gs->rendering, SCENE_FIRST_FONT_SIZE, min+V2(text_pad_x, 0), text_color, options[idx_opt]);
    }
}

static int
cmp_string(String *a, String *b)
{
    ASSERT(a);
    ASSERT(b);
    return string_compare(*a, *b);
}

static int
cmp_string_case_insensitive(String *a, String *b)
{
    ASSERT(a);
    ASSERT(b);
    return string_compare_case_insensitive(*a, *b);
}

static Fileset *
get_fileset_from_unique_id(Slice<Fileset> filesets, u64 unique_id)
{
    // filesets must be sorted by unique_id
    Fileset *result = 0;
    do_binary_search(filesets.data, filesets.len, NULL, &result, unique_id, unique_id);
#if BEAT_SLOW
    if (!result) {
        for (smem i = 0; i < filesets.len; i++) {
            auto *fileset = &filesets.data[i];
            ASSERT(fileset->unique_id != unique_id);
        }
    }
#endif
    return result;
}

static int
cmp_diffloc(WheelDiffLocation *aloc, WheelDiffLocation *bloc, void *filesets_)
{
    // TODO: Is it possible to sort by artist and title, and then group by unique_id with only one sort?
    // I don't think that is possible. At least not with merge sort.
    // Example: a b y c , x a y
    // After merge: a b x a y y c
    auto *filesets = (NewArrayDynamic<Fileset>*) filesets_;
    auto *fa = get_fileset_from_unique_id(*filesets, aloc->fileset_unique_id);
    auto *fb = get_fileset_from_unique_id(*filesets, bloc->fileset_unique_id);
    ASSERT(fa);
    ASSERT(fb);
    ASSERT(ABC(aloc->diff_idx, fa->diffs_count));
    ASSERT(ABC(bloc->diff_idx, fb->diffs_count));
    auto *a = &fa->diffs[aloc->diff_idx];
    auto *b = &fb->diffs[bloc->diff_idx];
    ASSERT(fa->diffs_count > 0 && fb->diffs_count > 0);
#define CMP_STRING_FIELD(field) if (result == 0) result = cmp_string_case_insensitive(&a->metadata.field, &b->metadata.field)
    int result = 0;
#if 1
    // if (result == 0) result = sign_of((smem)a->notes.timings.bpms_count - (smem)b->notes.timings.bpms_count);
    if (result == 0) result = (int)a->notes.keymode - (int)b->notes.keymode;
    CMP_STRING_FIELD(artist);
    CMP_STRING_FIELD(title);
    // CMP_STRING_FIELD(diff_name);
#else
    if (result == 0) result = sign_of((s64)fa->unique_id - (s64)fb->unique_id);
    if (result == 0) result = sign_of(aloc->diff_idx - bloc->diff_idx);
#endif
    return result;
#undef CMP_STRING_FIELD
}


static Fileset *
wheel_append_fileset(SceneWheel *wheel, Fileset *thenewfileset,
                     NewArrayDynamic<WheelDiffLocation> *my_diffs_arr = 0)
{

    NewArrayDynamic<WheelDiffLocation> *sorted_diffs = &wheel->thenew.sorted_diffs;
    NewArrayDynamic<WheelDiffLocation> *wheel_entries = &wheel->thenew.wheel_entries;
    ASSERT(thenewfileset->diffs_count > 0);
    thenewfileset->unique_id = wheel->next_unique_id_for_fileset++;
    ASSERT(thenewfileset->unique_id > 0);
    // push the fileset
    auto *fileset = wheel->thenew.loaded_filesets.push(*thenewfileset);
    // push the diffs
    bool dududududu = (my_diffs_arr != 0);
    for (s32 idx_diff = 0; idx_diff < fileset->diffs_count; idx_diff++) {
        auto *diff = &fileset->diffs[idx_diff];
        ASSERT_SLOW(diff->notes.columns > 0);
        ASSERT_SLOW(keymode_is_valid(diff->notes.keymode));
        ASSERT_SLOW(keymode_to_columns_table[diff->notes.keymode] == diff->notes.columns);
        WheelDiffLocation diff_loc = {};
        diff_loc.fileset_unique_id = fileset->unique_id;
        diff_loc.diff_idx = idx_diff;
        if (dududududu) {
            my_diffs_arr->push(diff_loc);
        } else {
            sorted_diffs->push(diff_loc);
            wheel_entries->push(diff_loc);
        }
    }
    // dont refilter yet
    return fileset;
}

static void
wheel_append_filesets(SceneWheel *wheel, MemoryArena *temp_arena, Slice<Fileset> new_filesets)
{
    Scoped_Temp_Arena temp(temp_arena);
    NewArrayDynamic<WheelDiffLocation> new_diffs = {};
    new_diffs.arena = temp.temp.arena;
    for (smem i = 0; i < new_filesets.len; i++) {
        wheel_append_fileset(wheel, &new_filesets.data[i], &new_diffs);
    }

    // Maybe if I my wheel is already sorted and I append some diffs to the end
    // and they are already sorted, I could call merge_sort__join directly.

    slice_merge_sort(new_diffs.as_slice(), temp.temp.arena,
                     cmp_diffloc, &wheel->thenew.loaded_filesets);
    wheel->thenew.sorted_diffs.append(new_diffs);
    wheel->thenew.wheel_entries.append(new_diffs);
}


static bool
debug_wheel_open_some_files(MemoryArena *final_arena, MemoryArena *temp_arena, String filename, s32 debug_max_to_open,
                            SceneWheel *wheel)
    // Fileset *filesets, s32 *opened_count)
{
    ASSERT(string_exists_and_is_not_empty(filename));
    FILE *f;
    char filename_cstr[1024];
    ASSERT(filename.len < ssizeof(filename_cstr));
    char delim = '\n';
    xmemcpy(filename_cstr, filename.data, filename.len);
    filename_cstr[filename.len] = 0;
    f = fopen(filename_cstr, "rb");
    if (!f) {
        fprintf(stderr, "Error opening file '%.*s'!\n", (int)filename.len, filename.data);
        return false;
    }
    char buf[2048];
    int len = 0;
    memset(buf, 0, ssizeof(buf));
    s32 file_success_count = 0;
    s32 file_fail_count = 0;
    // Maybe instead of fgetc use fread and use a ring buffer?
    u64 start_clock = platform.get_clock();
    bool quit_on_first_fail = false;
    for (;;) {
        int c = fgetc(f);
        if (c == -1) {
            break;
        } else if (c == (int)delim) {
            if (len) {
                buf[len] = 0; // just to be sure
                String str;
                str.data = (u8 *)buf;
                str.len = len;

                Fileset next_fileset;
                if (game_open_file_recognize_ext(final_arena, temp_arena, true, str,
                                                 &next_fileset)) {
                    file_success_count++;
                    wheel_append_fileset(wheel, &next_fileset);
                } else {
                    file_fail_count++;
                    if (quit_on_first_fail)
                        break;
                }
                if (file_success_count == debug_max_to_open) {
                    break;
                }
                len = 0;
                memset(buf, 0, ssizeof(buf));
            }
        } else {
            buf[len] = (char)c;
            len++;
            if ((size_t)len >= ssizeof(buf)) {
                ASSERT(!"BUFFER TOO SMALL");
                break;
            }
        }
    }
    u64 end_clock = platform.get_clock();
    f64 secs_per_clock = platform.get_secs_per_clock();
    fclose(f);

    printf("File success count: %d     fail: %d\n", file_success_count, file_fail_count);
    printf("Time it took: %.2fms (avg %.2fms/file including fs read)\n",
           1000.0*(f64)(end_clock - start_clock)*secs_per_clock,
           1000.0*(f64)(end_clock - start_clock)*secs_per_clock/(f64)file_success_count);
    return true;
}


enum FilterTestType {
    FilterTestType_none,
    FilterTestType_simple_str,
    FilterTestType_parser_type,
    FilterTestType_keymode,
    FilterTestType_artist,
    FilterTestType_title,
    FilterTestType_filepath,
};

struct FilterTest {
    FilterTestType type;
    u8 *data;
    union {
        String simple_str;
        ParserType parser_type;
        Keymode keymode;
        String artist;
        String title;
        String filepath;
    };

    BoyerMooreTableType table[256];
};

/// Return true when it matches.
static bool
filter_wheel_test(SceneWheel *wheel, WheelDiffLocation diff_loc, FilterTest *filter)
{
    // TODO:
    // 1. Since i'll process filter_string I can probably pass something else instead of just the string.
    // 2. "light it up" would search for words "light", "it" and "up" at the same time.
    // 3. "hime hime" would search for the word "hime" twice. "hime asdf hime" would match but "hime" would not.
    //    "him hime" would not match "hime" or "hime him", but it would match "hime hime".
    // 4. "artist=camellia title=light it up"
    // 5. "nanahira" matches "ななひら" - this isn't necessary but would be cool

    auto *fileset = get_fileset_from_unique_id(wheel->thenew.loaded_filesets, diff_loc.fileset_unique_id);
    ASSERT(fileset);
    ASSERT(ABC(diff_loc.diff_idx, fileset->diffs_count));
    auto *diff = &fileset->diffs[diff_loc.diff_idx];

    bool result = true;

    switch (filter->type) {
    case FilterTestType_none: {
        return true;
    } break;
    case FilterTestType_simple_str: {
        if (filter->simple_str.len <= 0)
            return true;

        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.title_translit, filter->simple_str, filter->table);
        if (result) return true;
        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.title, filter->simple_str, filter->table);
        if (result) return true;

        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.artist_translit, filter->simple_str, filter->table);
        if (result) return true;
        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.artist, filter->simple_str, filter->table);
        if (result) return true;

        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->filepath, filter->simple_str, filter->table);
        if (result) return true;
    } break;
    case FilterTestType_parser_type: {
        result = (fileset->parser_type == filter->parser_type);
        if (result) return true;
    } break;
    case FilterTestType_keymode: {
        result = (diff->notes.keymode == filter->keymode);
        if (result) return true;
    } break;
    case FilterTestType_artist: {
        if (filter->artist.len <= 0)
            return true;

        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.artist_translit, filter->artist, filter->table);
        if (result) return true;
        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.artist, filter->artist, filter->table);
        if (result) return true;
    } break;
    case FilterTestType_title: {
        if (filter->title.len <= 0)
            return true;

        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.title_translit, filter->title, filter->table);
        if (result) return true;
        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->metadata.title, filter->title, filter->table);
        if (result) return true;
    } break;
    case FilterTestType_filepath: {
        if (filter->filepath.len <= 0)
            return true;

        result = string_has_substring_case_insensitive_boyer_moore_with_table(diff->filepath, filter->filepath, filter->table);
        if (result) return true;
    } break;
    }

    return false;
}

static FilterTest
get_filter_test_from_string(String str)
{
    // NOTE: On clang with -std=c++11 result is NOT copied on return.
    // FilterTest filter = get_filter_test_from_string(str);
    // &filter == &result inside get_filter_test_from_string;

    // TODO: parse str "artist=xxxx", "parser_type=xxxx"
    FilterTest result = {}; // TODO Don't set the table to zero
    if (str.len <= 0) {
        return FilterTest{.type = FilterTestType_none};
    }
    String str_parser_eq = "parser="_s;
    String str_keymode_eq = "keymode="_s;
    String str_artist_eq = "artist="_s;
    String str_title_eq = "title="_s;
    String str_filepath_eq = "filepath="_s;
    if (string_starts_with_case_insensitive(str, str_parser_eq)) {
        String rest = string_offset(str, str_parser_eq.len);
        ParserType parser_type = ParserType_INVALID;
        for (s32 i = 1; i < ParserType_COUNT; i++) {
            String parser_str = parser_type_string_table[i];
            if (strings_are_equal_case_insensitive(parser_str, rest)
                || string_starts_with_case_insensitive(parser_str, rest)) {
                parser_type = (ParserType)i;
                break;
            }
        }
        result = { .type = FilterTestType_parser_type, .parser_type = parser_type };
    } else if (string_starts_with_case_insensitive(str, str_keymode_eq)) {
        String rest = string_offset(str, str_keymode_eq.len);
        Keymode keymode = Keymode_Invalid;
        if (rest.len > 0) {
            for (s32 i = 1; i < Keymode_COUNT; i++) {
                String keymode_str = keymode_to_fancy_string_table[i];
                if (strings_are_equal_case_insensitive(keymode_str, rest)
                    || string_starts_with_case_insensitive(keymode_str, rest)) {
                    printf("keymode %d\n", (int)keymode);
                    keymode = (Keymode)i;
                    break;
                }
            }
        }
        result = { .type = FilterTestType_keymode, .keymode = keymode };
    } else if (string_starts_with_case_insensitive(str, str_artist_eq)) {
        String rest = string_offset(str, str_artist_eq.len);
        result = { .type = FilterTestType_artist, .artist = rest };
        generate_table_for_case_insensitive_boyer_moore_string_search(rest, result.table);
    } else if (string_starts_with_case_insensitive(str, str_title_eq)) {
        String rest = string_offset(str, str_title_eq.len);
        result = { .type = FilterTestType_title, .title = rest };
        generate_table_for_case_insensitive_boyer_moore_string_search(rest, result.table);
    } else if (string_starts_with_case_insensitive(str, str_filepath_eq)) {
        String rest = string_offset(str, str_filepath_eq.len);
        result = { .type = FilterTestType_filepath, .filepath = rest };
        generate_table_for_case_insensitive_boyer_moore_string_search(rest, result.table);
    } else {
        result = { .type = FilterTestType_simple_str, .simple_str = str };
        generate_table_for_case_insensitive_boyer_moore_string_search(str, result.table);
    }
    return result;
}

static bool
is_filter_test_a_subset_of(FilterTest filter, FilterTest test_subset)
{
    if (filter.type != test_subset.type)
        return false;

    switch (filter.type) {
    case FilterTestType_none: {
        return filter.type == test_subset.type;
    } break;
    case FilterTestType_simple_str: {
        return string_has_substr(filter.simple_str, test_subset.simple_str);
    } break;
    case FilterTestType_parser_type: {
        return filter.parser_type == test_subset.parser_type;
    } break;
    case FilterTestType_keymode: {
        return filter.keymode == test_subset.keymode;
    } break;
    case FilterTestType_artist: {
        return string_has_substr(filter.artist, test_subset.artist);
    } break;
    case FilterTestType_title: {
        return string_has_substr(filter.title, test_subset.title);
    } break;
    case FilterTestType_filepath: {
        return string_has_substr(filter.filepath, test_subset.filepath);
    } break;
    }
    INVALID_CODE_PATH;
    return false;
}


static void
filter_wheel(SceneWheel *wheel, bool always_from_scratch = false)
{
    // TODO: Allow to filter only a part of the wheel too (for wheel_append_filesets, if there is no change in the filter, i only need to filter the new entries)
    // always_from_scratch must be true only if you resorted.
    // if you do wheel_append_fileset you don't need always_from_scratch to be true.
    auto clock_start = platform.get_clock();
    defer {
        auto seconds_elapsed = (f64)(platform.get_clock() - clock_start) * platform.get_secs_per_clock();
        u8 buf[128];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "filter_wheel took %.2fms", 1000*seconds_elapsed);
        printf("%s\n", buf);
    };

    WheelDiffLocation prev_diff_loc = {};
    bool found_prev_sel = false;
    smem prev_sel_entry = wheel->thenew.selected_wheel_entry;
    if (ABC(prev_sel_entry, wheel->thenew.wheel_entries.len)) {
        found_prev_sel = true;
        prev_diff_loc = wheel->thenew.wheel_entries.data[prev_sel_entry];
    } else {
        found_prev_sel = false;
        // We don't have anything selected because it is empty.
        ASSERT(wheel->thenew.wheel_entries.len <= 0);
        ASSERT(prev_sel_entry == 0);
    }

    bool did_it_from_scratch = false;
    // filter
    // DONE What i want: there are diffs 0, 1, 2, 3, 4. we have 2 selected. after filtering only 0, 1, 4 are available. we should now select 1.
    {
        // NOTE: On clang with -std=c++11 filter is NOT copied on return.
        FilterTest filter = get_filter_test_from_string(wheel->search_str);
        FilterTest prev_filter = get_filter_test_from_string(wheel->prev_search_str);

        if (filter.type == FilterTestType_none) {
            // No filtering. Find the selected entry and then add all the diffs to `wheel->thenew.wheel_entries`.
            did_it_from_scratch = true;
            auto diffs = wheel->thenew.sorted_diffs;
            smem prev_sel_in_sorted_diffs;
            if (found_prev_sel) {
                prev_sel_in_sorted_diffs = find_in_slice_by_value(diffs.as_slice(), prev_diff_loc);
                ASSERT(prev_sel_in_sorted_diffs >= 0);
            } else {
                prev_sel_in_sorted_diffs = -1;
            }
            smem new_sel_entry = prev_sel_in_sorted_diffs; // need to clamp later
            wheel->thenew.wheel_entries.len = 0;
            wheel->thenew.wheel_entries.append(diffs);

            // find previously selected thing
            new_sel_entry = clamp(new_sel_entry, (smem)0, MAXIMUM(0, wheel->thenew.wheel_entries.len-1));
            wheel->thenew.selected_wheel_entry = new_sel_entry;
        } else if (!always_from_scratch && is_filter_test_a_subset_of(filter, prev_filter)) {
            // This is a subset of the previous filter. `wheel_entries`
            // contains all the new entries and some invalid ones. Loop on
            // `wheel_entries` and remove those that don't pass
            // `filter_wheel_test`.
            // refilter it.
            // no need to worry if wheel_entries.len <= 0
            did_it_from_scratch = false;
            smem new_sel_entry = found_prev_sel ? prev_sel_entry : -1; // need to clamp later!
            auto ordrem = ordered_remove_decreasing_delayed_init();
            for (smem i = wheel->thenew.wheel_entries.len-1; i >= 0; i--) {
                auto diff_loc = wheel->thenew.wheel_entries.data[i];
                bool cond_to_remove = !filter_wheel_test(wheel, diff_loc, &filter);
                // <= means go up if the current gets deleted
                // <  means go down if the current gets deleted
                if (i <= new_sel_entry && cond_to_remove) {
                    new_sel_entry -= 1; // need to clamp new_sel_entry later!
                }
                ordered_remove_decreasing_delayed(cond_to_remove, i, &wheel->thenew.wheel_entries, &ordrem);
            }
            ordered_remove_decreasing_delayed_final(&wheel->thenew.wheel_entries, &ordrem);

            // find previously selected thing
            new_sel_entry = clamp(new_sel_entry, (smem)0, MAXIMUM(0, wheel->thenew.wheel_entries.len-1));
            wheel->thenew.selected_wheel_entry = new_sel_entry;
        } else {
            // Filter it from scratch.
            did_it_from_scratch = true;
            auto diffs = wheel->thenew.sorted_diffs;
            smem prev_sel_in_sorted_diffs;
            if (found_prev_sel) {
                prev_sel_in_sorted_diffs = find_in_slice_by_value(diffs.as_slice(), prev_diff_loc);
                ASSERT(prev_sel_in_sorted_diffs >= 0);
            } else {
                prev_sel_in_sorted_diffs = -1;
            }
            smem new_sel_entry = prev_sel_in_sorted_diffs; // need to clamp later
            wheel->thenew.wheel_entries.len = 0;
            for (smem i = 0; i < diffs.len; i++) {
                auto diff_loc = diffs.data[i];
                if (filter_wheel_test(wheel, diff_loc, &filter)) {
                    wheel->thenew.wheel_entries.push(diff_loc);
                } else {
                    // <= means go up if the current gets deleted
                    // <  means go down if the current gets deleted
                    if (i <= prev_sel_in_sorted_diffs) {
                        new_sel_entry -= 1; // need to clamp new_sel_entry later!
                    }
                }
            }

            // find previously selected thing
            new_sel_entry = clamp(new_sel_entry, (smem)0, MAXIMUM(0, wheel->thenew.wheel_entries.len-1));
            wheel->thenew.selected_wheel_entry = new_sel_entry;
        }

        if (found_prev_sel) {
            if (filter_wheel_test(wheel, prev_diff_loc, &filter)) {
                ASSERT(wheel->thenew.wheel_entries.len > 0);
                ASSERT(wheel->thenew.wheel_entries.data[wheel->thenew.selected_wheel_entry] == prev_diff_loc);
            }
        }
    }

    if (wheel->thenew.wheel_entries.len <= 0) {
        ASSERT(wheel->thenew.selected_wheel_entry == 0);
    }

    if (always_from_scratch) ASSERT(did_it_from_scratch);
}

static void
sort_wheel(GameMemory *memory, SceneWheel *wheel)
{
    beat_log("-----SORTING WHEEL-----\n");
    u64 clock_start = platform.get_clock();
    u64 filter_wheel_clock_start = 0;
    defer {
        u64 clock_end = platform.get_clock();
        f64 secs_per_clock = platform.get_secs_per_clock();
        f64 delta_secs_total = (f64)(clock_end-clock_start)*secs_per_clock;
        f64 delta_secs_for_filter = (f64)(clock_end-filter_wheel_clock_start)*secs_per_clock;
        f64 delta_secs_for_sort = delta_secs_total - delta_secs_for_filter;

        u8 buf[128];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "sort_wheel %.2fms, filter_wheel took %.2fms, totalling %.2fms",
                         1000*delta_secs_for_sort,
                         1000*delta_secs_for_filter,
                         1000*delta_secs_total);
        add_fading_text(get_game_state(memory), bufstr);
        beat_log("%.*s\n", EXPAND_STR_FOR_PRINTF(bufstr));
    };

    auto *filesets = &wheel->thenew.loaded_filesets;
    slice_merge_sort(wheel->thenew.sorted_diffs.as_slice(), &memory->temporary,
                     cmp_diffloc, filesets);

#if BEAT_SLOW
    {
        auto diffs = wheel->thenew.sorted_diffs;
        for (smem i = 0; i < diffs.len-1; i++) {
            auto *a = &diffs.data[i];
            auto *b = &diffs.data[i+1];
            ASSERT(cmp_diffloc(a, b, filesets) <= 0);
        }
    }
#endif

    // filter
    filter_wheel_clock_start = platform.get_clock();
    filter_wheel(wheel, true);
}

static int
cmp_fileset_id(Fileset *a, Fileset *b, void *_)
{
    return sign_of((s64)a->unique_id - (s64)b->unique_id);
}

#if 0
// I won't need to use this since every time I add a fileset it goes to the end of the array.
static void
sort_filesets_by_id(Slice<Fileset> filesets, MemoryArena *temp_arena)
{
    slice_merge_sort_helper(filesets, temp_arena, 0, filesets.len, cmp_fileset_id, 0);
}
#endif

static void
remove_and_free_fileset(SceneWheel *wheel, Fileset *fileset)
{
    bool found_prev_sel;
    smem prev_sel_entry;
    if (wheel->thenew.wheel_entries.len > 0) {
        found_prev_sel = true;
        prev_sel_entry = wheel->thenew.selected_wheel_entry;
        ASSERT(ABC(wheel->thenew.selected_wheel_entry, wheel->thenew.wheel_entries.len));
    } else {
        found_prev_sel = false;
        prev_sel_entry = -1;
    }

    smem count_diffs_removed = 0;
    smem count_diffs = fileset->diffs_count;
    {
        // Remove from wheel->thenew.sorted_diffs
        auto ordrem = ordered_remove_decreasing_delayed_init();
        for (smem i = wheel->thenew.sorted_diffs.len-1; i >= 0; i--) {
            if (count_diffs_removed == count_diffs) break;
            auto diff_loc = wheel->thenew.sorted_diffs.data[i];
            bool cond_to_remove = (diff_loc.fileset_unique_id == fileset->unique_id);
            if (cond_to_remove) {
                count_diffs_removed++;
            }
            ordered_remove_decreasing_delayed(cond_to_remove, i, &wheel->thenew.sorted_diffs, &ordrem);
        }
        ordered_remove_decreasing_delayed_final(&wheel->thenew.sorted_diffs, &ordrem);

        ASSERT(count_diffs == count_diffs_removed);
        count_diffs_removed = 0;
    }
    smem count_wheel_entries_removed_before_selected = 0;
    {
        // Remove from wheel->thenew.wheel_entries
        // TODO: check if the diffs are filtered before searching for them.
        auto ordrem = ordered_remove_decreasing_delayed_init();
        for (smem i = wheel->thenew.wheel_entries.len-1; i >= 0; i--) {
            if (count_diffs_removed == count_diffs) break;
            auto diff_loc = wheel->thenew.wheel_entries.data[i];
            bool cond_to_remove = (diff_loc.fileset_unique_id == fileset->unique_id);
            if (cond_to_remove) {
                count_diffs_removed++;
                if (i <= prev_sel_entry) // go up if current was deleted
                    count_wheel_entries_removed_before_selected++;
            }
            ordered_remove_decreasing_delayed(cond_to_remove, i, &wheel->thenew.wheel_entries, &ordrem);
        }
        ordered_remove_decreasing_delayed_final(&wheel->thenew.wheel_entries, &ordrem);

        ASSERT(count_diffs == count_diffs_removed);
        count_diffs_removed = 0;
    }

    smem new_sel_entry;
    if (found_prev_sel) {
        new_sel_entry = clamp(prev_sel_entry - count_wheel_entries_removed_before_selected,
                              (smem)0, MAXIMUM(0, wheel->thenew.wheel_entries.len));
    } else {
        new_sel_entry = 0;
    }
    wheel->thenew.selected_wheel_entry = new_sel_entry;

    free_fileset(fileset);
    smem fileset_idx = (smem)(fileset - wheel->thenew.loaded_filesets.data);
    ASSERT(ABC(fileset_idx, wheel->thenew.loaded_filesets.len));
    ordered_remove(&wheel->thenew.loaded_filesets, fileset_idx, 1);
}


static
WORKER_FN(worker_load_font_chunk)
{
    auto *work = (Work_load_font_chunk*)data;
    defer { end_work_memory(work->work_mem); };
    u64 clock_start = platform.get_clock();

    printf("worker_load_font_chunk: %ld chunks\n", work->queued_chunks.len);
    Forv (work->queued_chunks, _, it) {
        printf("  u+%04x to u+%04x id %lu\n", it.first_codepoint, it.first_codepoint + it.count_codepoint - 1, it.id);
    }

    Forv (work->queued_chunks, _, it) {
        // TODO: Cancel if `!global_running`.
        FontChunk this_chunk;
        make_font_chunk(&this_chunk, work->font_make_info, it.first_codepoint, it.count_codepoint, temp_mem.arena);
        {
            SCOPED_LOCK(&work->async_data->mutex);
            work->async_data->new_chunks.push({.chunk=this_chunk, .queued_id=it.id});
        }
    }

    work->async_data->base.seconds_elapsed = (f64)(platform.get_clock() - clock_start) * platform.get_secs_per_clock();
    COMPILER_BARRIER();
    work->async_data->base.completed = true;
}

struct WorkAsyncDataBeginArgs_load_font_chunk {
    smem count_queued_chunks;
};

static inline void
begin_work_async_data(WorkAsyncData_load_font_chunk *async_data, void *data)
{
    auto count_queued_chunks = ((WorkAsyncDataBeginArgs_load_font_chunk*)data)->count_queued_chunks;
    ASSERT(count_queued_chunks > 0);

    // the mutex is already initialized to zero

    async_data->new_chunks.arena = 0; // TODO: @Malloc Not malloc
    async_data->new_chunks.reserve(count_queued_chunks);
}

static void
end_work_async_data(WorkAsyncData_load_font_chunk *asd)
{
    end_work_async_data__base(&asd->base);
    asd->new_chunks.free();
}


static void
add_work_load_font_chunk(GameMemory *memory, FontMakeinfo *font_make_info, Slice<FontChunkQueued*> queued_chunks)
{
    // This function is only called from the main loop.

    ASSERT(queued_chunks.len > 0);

    auto *gs = get_game_state(memory);
    auto *work_mem = begin_work_memory(memory);
    ASSERT(work_mem);
    auto *work = arena_push_struct(&work_mem->arena, Work_load_font_chunk, arena_flags_zero());
    work->work_mem = work_mem;
    WorkAsyncDataBeginArgs_load_font_chunk begin_asd_args = {};
    begin_asd_args.count_queued_chunks = queued_chunks.len;
    work->async_data = begin_work_async_data_from_arr(&gs->work_async_data_arr_load_font_chunk, &begin_asd_args);
    ASSERT(work->async_data);
    work->queued_chunks.arena = &work_mem->arena;
    work->queued_chunks.reserve(queued_chunks.len);
    work->font_make_info = font_make_info;

    Forv (queued_chunks, _, it) {
        work->queued_chunks.push(*it);
        it->job_started = true;
    }

    platform.add_work(memory->low_priority_queue, worker_load_font_chunk, work);
    // gs->work_async_data_active_load_font_chunk.push(work->async_data);
}



static
WORKER_FN(worker_wheel_add_directory)
{
    auto *work = (Work_wheel_add_directory*)data;
    defer { end_work_memory(work->work_mem); };
    u64 clock_start = platform.get_clock();

    printf("worker_wheel_add_directory: '%.*s'\n", EXPAND_STR_FOR_PRINTF(work->dir));

    smem num_filesets_to_give = 100; // I don't care if it's off by a few when I send the filesets.

    // TODO: Clean this up a bit. I just copy and pasted from the old wheel_add_directory.

    bool report_errors = false;
    {
        // TODO: I don't like the way I'm using memory->temporary and temp2_arena. If I do this in other places I could probably make a bug - though temp_count would probably catch it.
        s32 file_success_count = 0;
        s32 file_fail_count = 0;
        u64 start_clock = platform.get_clock();
        defer {
            u64 end_clock = platform.get_clock();
            f64 secs_per_clock = platform.get_secs_per_clock();

            beat_log("File success count: %d    fail: %d\n", file_success_count, file_fail_count);
            beat_log("Time it took: %.2fms (avg %.2fms/file including fs read)\n",
                     1000.0*(f64)(end_clock - start_clock)*secs_per_clock,
                     1000.0*(f64)(end_clock - start_clock)*secs_per_clock/(f64)file_success_count);
        };


        MemoryArena *temp_arena1 = temp_mem.arena; //memory->temporary;
        MemoryArena *temp_arena_for_songs_unused = temp_arena1;


        SCOPED_TEMP_ARENA(temp_arena1);

        MemoryArena temp2_arena = sub_arena(temp_arena1, MEGABYTES(1));

        NewArrayDynamic<Fileset> new_filesets = {};
        new_filesets.arena = &temp2_arena;

        DirectoryIter dir_iter;
        if (!platform.directory_iter_init(&dir_iter, work->dir, temp_arena1, true, 0)) {
            ASSERT(!"Failed to start directory iter!");
        }

        struct DoingDirFileset {
            // used for .bms and .osu - the fileset is the directory
            // .sm and .ojn aren't like this, all the diffs are in one file.
            ParserType parser_type;
            bool doing_it;
            s64 dir_id;
            TempMemory temp_mem;
            StringNewArrayDynamic dir; // on temp_mem.arena
            NewArrayDynamic<Diff> diffs; // on temp_mem.arena
            MemoryArena *songs_arena;
            MemoryArena *big_temp_arena;

            // NOTE: temp_arena_ MUST be different from dir_iter.arena because dir_iter would overwrite its data!.
            void init(ParserType type, MemoryArena *songs_arena_, MemoryArena *temp_arena_, MemoryArena *big_temp_arena_) {
                *this = {};
                ASSERT(type != ParserType_INVALID);
                parser_type = type;
                dir_id = -1;
                temp_mem = begin_temp_memory(temp_arena_);
                dir.arena = temp_mem.arena;
                diffs.arena = temp_mem.arena;
                songs_arena = songs_arena_;
                big_temp_arena = big_temp_arena_;
            }
            void make_fileset_and_add_to_arr(NewArrayDynamic<Fileset> *arr, s32 *file_success_count) {
                ASSERT(doing_it);

                if (diffs.len > 0) {
                    Fileset new_fileset = {};
                    new_fileset.parser_type = parser_type;
                    new_fileset.filepath = copy_string(dir);
                    new_fileset.diffs_count = (s32)diffs.len;
                    new_fileset.diffs = xmalloc_struct_count(Diff, new_fileset.diffs_count, xmalloc_flags_zero());
                    xmemcpy_struct_count(new_fileset.diffs, diffs.data, Diff, new_fileset.diffs_count);

                    arr->push(new_fileset);
                    *file_success_count += 1;
                }
                doing_it = false;
                diffs.len = 0;
                dir.len = 0;
                dir_id = -1;
            }
            void after_dir_iter_end(NewArrayDynamic<Fileset> *arr, s32 *file_success_count, s32 debug_max_to_open) {
                if (doing_it) {
                    if (*file_success_count < debug_max_to_open) {
                        make_fileset_and_add_to_arr(arr, file_success_count);
                        printf("Making fileset from after_dir_iter_end\n");
                    } else {
                        // @Leak Diff is leaked
                    }
                }
                end_temp_memory(&temp_mem);
                *this = {};
            }
            void each_dir_item(DirectoryIter *dir_iter, NewArrayDynamic<Fileset> *arr,
                               s32 *file_success_count, s32 debug_max_to_open) {
                ASSERT_SLOW(dir_iter->arena != temp_mem.arena);
                if (doing_it && dir_id != dir_iter->unique_dir_id) {
                    if (*file_success_count < debug_max_to_open) {
                        make_fileset_and_add_to_arr(arr, file_success_count);
                    } else {
                        // @Leak Diff is leaked
                    }
                }
            }
            void make_diff_from_file(DirectoryIter *dir_iter, bool report_errors, s32 *file_fail_count) {
                bool do_print_got_ok = false;
                bool do_print_got_fail = false;
                // ASSERT(parser == parser_type);
                if (!doing_it) {
                    doing_it = true;
                    ASSERT(dir.len == 0);
                    dir.append(dir_iter->item.full_dir_nullter);
                } else {
                    ASSERT(dir_id == dir_iter->unique_dir_id);
                }
                Diff diff;
                auto *diff_from_file_fn = parser_bms_diff_from_file;
                diff_from_file_fn = 0;
                switch (parser_type) { // TODO: not this
                case ParserType_BMS: diff_from_file_fn = parser_bms_diff_from_file; break;
                default: INVALID_CODE_PATH;
                }
                ASSERT(diff_from_file_fn);
                if (diff_from_file_fn(songs_arena, big_temp_arena, dir_iter->item.full_filename_nullter,
                                      report_errors, &diff)) {
                    diffs.push(diff);
                    if (do_print_got_ok) {
                        printf("Got %.*s ok    '%.*s'\n",
                               EXPAND_STR_FOR_PRINTF(parser_type_string_table[parser_type]),
                               EXPAND_STR_FOR_PRINTF(dir_iter->item.full_filename_nullter));
                    }
                } else {
                    *file_fail_count += 1;
                    if (do_print_got_fail) {
                        printf("Got %.*s fail! '%.*s'\n",
                               EXPAND_STR_FOR_PRINTF(parser_type_string_table[parser_type]),
                               EXPAND_STR_FOR_PRINTF(dir_iter->item.full_filename_nullter));
                    }
                }
            }
        };
        DoingDirFileset doing_arr[1] = {};
        doing_arr[0].init(ParserType_BMS, temp_arena_for_songs_unused, &temp2_arena, temp_arena1);
        while (platform.directory_iter_next(&dir_iter)) {
            // TODO: Cancel if `!global_running`.
            if (dir_iter.item.file_type != DirectoryIterFileType_File) continue;
            ParserType parser = get_parser_from_filename(dir_iter.item.full_filename_nullter);
            if (parser == ParserType_INVALID) continue;

            // @Leak DoingDirFileset Diffs but that's not important, debug_max_to_open is just a debug value.
            if (file_success_count >= work->debug_max_to_open)
                break;

            if (new_filesets.len >= num_filesets_to_give) {
                // Contention would already be VERY rare here.
                if (ticket_mutex_try_lock(&work->async_data->mutex)) {
                    work->async_data->new_filesets.append(new_filesets);
                    ticket_mutex_unlock(&work->async_data->mutex);
                    new_filesets.len = 0;
                }
            }

            bool got_it = false;
            for (smem idx_doing = 0; idx_doing < ARRAY_COUNT(doing_arr); idx_doing++) {
                auto *doing = &doing_arr[idx_doing];
                doing->each_dir_item(&dir_iter, &new_filesets, &file_success_count, work->debug_max_to_open);
                if (!got_it && parser == doing->parser_type) {
                    got_it = true;
                    doing_arr->make_diff_from_file(&dir_iter, report_errors, &file_fail_count);
                }
            }
            if (!got_it) {
                // do the default
                Fileset thenewfileset = {};
                bool valid = game_open_file_recognize_ext(temp_arena_for_songs_unused, temp_arena1,
                                                          report_errors,
                                                          dir_iter.item.full_filename_nullter,
                                                          &thenewfileset);
                if (valid) {
                    new_filesets.push(thenewfileset);
                    file_success_count++;
                } else {
                    file_fail_count += 1;
                }
            }
        }
        platform.directory_iter_end(&dir_iter);

        for (smem idx_doing = ARRAY_COUNT(doing_arr)-1; idx_doing >= 0; idx_doing--) {
            auto *doing = &doing_arr[idx_doing];
            doing->after_dir_iter_end(&new_filesets, &file_success_count, work->debug_max_to_open);
        }

        if (new_filesets.len > 0) {
            {
                SCOPED_LOCK(&work->async_data->mutex);
                work->async_data->new_filesets.append(new_filesets);
            }
            new_filesets.len = 0;
        }
    }

    work->async_data->base.seconds_elapsed = (f64)(platform.get_clock() - clock_start) * platform.get_secs_per_clock();
    COMPILER_BARRIER();
    work->async_data->base.completed = true;
}


static inline void
begin_work_async_data(WorkAsyncData_wheel_add_directory *async_data, void *_data)
{
    // TODO: Not malloc!
    // the mutex is already initialized to zero
    async_data->new_filesets.arena = 0; // TODO @Malloc
    async_data->new_filesets.reserve(256);
}

static void
end_work_async_data(WorkAsyncData_wheel_add_directory *asd)
{
    end_work_async_data__base(&asd->base);
    asd->new_filesets.free();
}


static void
add_work_wheel_add_directory(GameMemory *memory, SceneWheel *wheel, String dir, s32 debug_max_to_open)
{
    // This function is only called from the main loop.
    // TODO: Multiple threads loading the thing. (call this function once and a lot of threads fire up to load the songs)

    ASSERT(wheel->is_initialized);

    auto *gs = get_game_state(memory);
    auto *work_mem = begin_work_memory(memory);
    ASSERT(work_mem);
    auto *work = arena_push_struct(&work_mem->arena, Work_wheel_add_directory, arena_flags_zero());
    work->work_mem = work_mem;
    work->async_data = begin_work_async_data_from_arr(&gs->work_async_data_arr_wheel_add_directory);
    ASSERT(work->async_data);
    work->dir = arena_push_string_nullter(&work_mem->arena, dir);
    work->debug_max_to_open = debug_max_to_open;
    platform.add_work(memory->low_priority_queue, worker_wheel_add_directory, work);
}

static void load_gl_crap_for_font_chunk(FontChunk *chunk);

static void
start_loading_fonts_asynchronously(GameMemory *memory) {
    // Start loading fonts asynchronously.
    Scoped_Temp_Arena temp(&memory->temporary);
    NewArrayDynamic<FontChunkQueued*> chunks = {};
    chunks.arena = temp.temp.arena;
    auto *font = memory->debug_font0;
    Forp (font->chunks_queued, _, it) {
        if (it->job_started)
            continue;
        chunks.push(it);
    }
    for (smem i = 0; i < chunks.len; i++)
        add_work_load_font_chunk(memory, &font->make_info, chunks.as_slice().slice(i, i+1));
}

static void acquire_work_result_load_font_chunk(GameMemory *memory, GameState *gs)
{
    // load_font_chunk
    Forv (gs->work_async_data_arr_load_font_chunk, _, async_data) {
        if (async_data->base.being_used) {
            bool completed = async_data->base.completed;
            COMPILER_BARRIER();
            auto *font = memory->debug_font0;
            if (ticket_mutex_try_lock(&async_data->mutex)) {
                defer { ticket_mutex_unlock(&async_data->mutex); };
                if (async_data->new_chunks.len > 0) {
                    auto count_new_chunks = async_data->new_chunks.len;
                    s32 new_glyphs_count = 0;
                    Forv (async_data->new_chunks, idx_new_chunk, asd_new_chunk_and_queued_id) {
                        auto *new_chunk = font->chunks.push(asd_new_chunk_and_queued_id.chunk);
                        new_glyphs_count += new_chunk->glyphs_count - 1;

                        // I could copy everything to a local array first and release the lock before loading it but it doesn't matter.
                        bool found = false;
                        auto queued_id = async_data->new_chunks.data[idx_new_chunk].queued_id;
                        Fordecp (font->chunks_queued, idx_queued, queued) {
                            if (queued->id == queued_id) {
                                ASSERT(queued->first_codepoint == new_chunk->first_codepoint);
                                ASSERT(queued->count_codepoint == new_chunk->count_codepoint);
                                *queued = font->chunks_queued.data[--font->chunks_queued.len];
                                found = true;
                                break;
                            }
                        }
                        ASSERT(found);

                        load_gl_crap_for_font_chunk(new_chunk);
                    }
                    async_data->new_chunks.len = 0;

                    async_data->count_new_chunks += count_new_chunks;
                    async_data->new_glyphs_count += new_glyphs_count;
                    font->total_glyph_count += new_glyphs_count;

                    printf("Adding %ld font chunks with %d glyphs\n", count_new_chunks, new_glyphs_count);
                }
            } else {
                ASSERT(!completed);
                continue;
            }
            if (completed) {
                auto seconds_elapsed = async_data->base.seconds_elapsed;
                auto count_new_chunks = async_data->count_new_chunks;
                auto new_glyphs_count = async_data->new_glyphs_count;
                u8 buf[512];
                String bufstr = {};
                xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                 "load font chunk complete %d chunks %.2fms for %d codepoints (%.2fms/codepoint) - total %d",
                                 count_new_chunks,
                                 1000 * seconds_elapsed,
                                 new_glyphs_count,
                                 1000 * seconds_elapsed/(f64)(new_glyphs_count),
                                 font->total_glyph_count);
                if (gs->scene != SCENE_PLAYING) {
                    add_fading_text(gs, bufstr);
                }
                printf("%s\n", buf);
                end_work_async_data(async_data);
            }
        }
    }
}

static void
init_wheel()
{
    auto memory = GAME_MEMORY;
    auto gs = get_game_state(memory);

    // TODO: Make a macro that automatically does this.
    auto temp_count_at_start = memory->temporary.temp_count;
    defer { arena_temp_check(&memory->temporary, temp_count_at_start); };

    add_fading_text(gs, "Initializing wheel (async)..."_s);
    beat_log("Initializing wheel (async)...\n");

    zero_struct(&gs->wheel);
    gs->wheel.is_initialized = true;
    gs->wheel.rate_id = RATE_ID_ONE;
    gs->wheel.arena = &memory->songs_arena;


#if 0
    {
        smem count_async_data_for_wheel_add_dir = 4;
        gs->wheel.work_async_data_wheel_add_directory_arr.arena = 0; // it can't be on permanent.
        gs->wheel.work_async_data_wheel_add_directory_arr.reserve(count_async_data_for_wheel_add_dir);
        for (smem i = 0; i < count_async_data_for_wheel_add_dir; i++) {
            gs->wheel.work_async_data_wheel_add_directory_arr.push({});
        }
    }
#endif

    String debug_max_to_open_str = simple_ini_get_or_default(&gs->simple_ini_config, "_root"_s,
                                                             "debug_wheel_max_to_open"_s, "unlimited"_s);
    s32 debug_max_to_open = 40000; // I can remove this for release
    if (debug_max_to_open_str == "unlimited"_s) {
        debug_max_to_open = S32_MAX;
    } else {
        parse_s32(debug_max_to_open_str, &debug_max_to_open, 10, 0);
    }

    gs->wheel.search_str.arena = gs->wheel.arena;
    gs->wheel.search_str.reserve(1024*4); // that will be enough
    gs->wheel.prev_search_str.arena = gs->wheel.search_str.arena;
    gs->wheel.prev_search_str.reserve(gs->wheel.search_str.cap);

    // TODO: If it goes past 10000 diffs, we will waste a lot of memory. Maybe I should use a very big BucketArray/BulkArray instead.
    gs->wheel.thenew.loaded_filesets.arena = gs->wheel.arena;
    gs->wheel.thenew.loaded_filesets.reserve(10000);
    gs->wheel.thenew.sorted_diffs.arena = gs->wheel.arena;
    gs->wheel.thenew.sorted_diffs.reserve(10000);
    gs->wheel.thenew.wheel_entries.arena = gs->wheel.arena;
    gs->wheel.thenew.wheel_entries.reserve(10000);
    gs->wheel.next_unique_id_for_fileset = 1;

    switch (gs->test_args.mode) {
        case TestArgGameMode::WheelFromFile: {
            bool valid = debug_wheel_open_some_files(&memory->songs_arena, &memory->temporary,
                                                     gs->test_args.wheel_from_file,
                                                     debug_max_to_open, &gs->wheel);
            ASSERT_MSG(valid, "Parse error");
            filter_wheel(&gs->wheel);
        } break;
        case TestArgGameMode::WheelSingleFile: {
            Fileset thenewfileset = {};
            bool valid = game_open_file_recognize_ext(&memory->songs_arena, &memory->temporary,
                                                 true,
                                                 gs->test_args.wheel_single_file,
                                                 &thenewfileset);
            ASSERT_MSG(valid, "Parse error");
            wheel_append_fileset(&gs->wheel, &thenewfileset);
            filter_wheel(&gs->wheel);
        } break;
        case TestArgGameMode::WheelDirectoryIter: {
            // wheel_add_directory(gs->test_args.wheel_directory_iter, debug_max_to_open);
            add_work_wheel_add_directory(memory, &gs->wheel, gs->test_args.wheel_directory_iter, debug_max_to_open);
            // Two at the same time works now!
            // Even if you add the same directory it works. It will get duplicate filesets, but it doesn't crash or anything.
            // add_work_wheel_add_directory(memory, &gs->wheel, "/mnt/c/Games/o2china English/Music"_s, debug_max_to_open);
        } break;
    }
}

static void
scene_wheel_logic(GameMemory *memory, GameState *gs, GameInput *gin)
{
    ASSERT(gs->wheel.is_initialized);
    ASSERT(rate_id_is_valid(gs->wheel.rate_id));


    gs->wheel.prev_search_str.len = 0;
    gs->wheel.prev_search_str.append(gs->wheel.search_str);

    if (gs->wheel.thenew.wheel_entries.len <= 0) {
        ASSERT(gs->wheel.thenew.selected_wheel_entry == 0);
    }

    auto *loaded_filesets = &gs->wheel.thenew.loaded_filesets;

    // async tasks
    {
        // wheel_add_directory
        Forv (gs->work_async_data_arr_wheel_add_directory, _, async_data) {
            if (async_data->base.being_used) {
                bool completed = async_data->base.completed;
                COMPILER_BARRIER();
                if (( true || FRAME_COUNT_ONCE_IN_A_WHILE) || completed) {
                    Scoped_Temp_Arena temp_mem(&memory->temporary);
                    NewArrayDynamic<Fileset> new_filesets = {};
                    new_filesets.arena = temp_mem.temp.arena;
                    if (ticket_mutex_try_lock(&async_data->mutex)) {
                        if (async_data->new_filesets.len > 0) {
                            new_filesets.append(async_data->new_filesets);
                            async_data->new_filesets.len = 0;
                        }
                        ticket_mutex_unlock(&async_data->mutex);
                    } else {
                        ASSERT(!completed);
                        // We'll do this again on the next frame.
                        continue;
                    }
                    if (new_filesets.len > 0) {
                        wheel_append_filesets(&gs->wheel, &memory->temporary, new_filesets);
                        filter_wheel(&gs->wheel);
                        {
                            u8 buf[512];
                            String bufstr = {};
                            xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                             "adding %ld filesets", new_filesets.len);
                            // add_fading_text(gs, bufstr);
                            // printf("%s\n", buf);
                        }
                    }
                }

                if (completed) {
                    ASSERT(async_data->new_filesets.len == 0);
                    auto seconds_elapsed = async_data->base.seconds_elapsed;
                    u8 buf[512];
                    String bufstr = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                     "wheel complete (%.2fms)", 1000*seconds_elapsed);
                    add_fading_text(gs, bufstr);
                    end_work_async_data(async_data);
                }
            }
        }
    }

    if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_global_audio_offset_plus)) {
        audio::debug__sound_global_offset += 5/1000.0;
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "debug__sound_global_offset = %.0fms", 1000*audio::debug__sound_global_offset);
        printf("%s\n", buf);
        add_fading_text(gs, bufstr);
    }
    if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_global_audio_offset_minus)) {
        audio::debug__sound_global_offset -= 5/1000.0;
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "debug__sound_global_offset = %.0fms", 1000*audio::debug__sound_global_offset);
        printf("%s\n", buf);
        add_fading_text(gs, bufstr);
    }

    if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_global_visual_offset_plus)) {
        debug__global_visual_offset += 5/1000.0;
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "debug__global_visual_offset = %.0fms", 1000*debug__global_visual_offset);
        printf("%s\n", buf);
        add_fading_text(gs, bufstr);
    }
    if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_global_visual_offset_minus)) {
        debug__global_visual_offset -= 5/1000.0;
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "debug__global_visual_offset = %.0fms", 1000*debug__global_visual_offset);
        printf("%s\n", buf);
        add_fading_text(gs, bufstr);
    }

    if (!gs->wheel.is_search_active && key_binding_pressed(gin, gs->bindings.wheel_back)) {
        // TODO: Don't just return. Screen will flash.
        gs->wheel.thenew.selected_wheel_entry = 0;
        gs->wheel.rate_id = RATE_ID_ONE;
        enter_scene(memory, gs, SCENE_FIRST);
        return;
    }

    // TODO: Remove fileset and sort wheel.
    if (!false && key_binding_pressed(gin, gs->bindings.wheel_remove_and_reload)) {
        if (gs->wheel.thenew.wheel_entries.len > 0) {
            // remove the selected fileset from the wheel.
            Scoped_Temp_Arena temp_mem(&memory->temporary);
            auto *sel_entry = &gs->wheel.thenew.selected_wheel_entry;
            ASSERT(ABC(*sel_entry, gs->wheel.thenew.wheel_entries.len));
            auto sel_diff_loc = gs->wheel.thenew.wheel_entries.data[*sel_entry];
            auto *fileset = get_fileset_from_unique_id(*loaded_filesets, sel_diff_loc.fileset_unique_id);
            ASSERT(fileset);
            String filepath_copy = arena_push_string_nullter(temp_mem.temp.arena, fileset->filepath);
            remove_and_free_fileset(&gs->wheel, fileset);
            fileset = 0;

            // TODO: either sort again or move the sorted_diffs to where they were?
            ASSERT(gs->wheel.thenew.sorted_diffs.len > 0);

            // now reload it
            Fileset thenewfileset = {};
            if (game_open_file_recognize_ext(&memory->songs_arena, temp_mem.temp.arena, true, filepath_copy, &thenewfileset)) {
                wheel_append_fileset(&gs->wheel, &thenewfileset);
                filter_wheel(&gs->wheel);
                add_fading_text(gs, "wheel_remove_and_reload ok"_s);
            } else {
                add_fading_text(gs, "wheel_remove_and_reload fail"_s);
            }
        }
    }

    if (key_binding_pressed(gin, gs->bindings.wheel_sort)) {
        sort_wheel(memory, &gs->wheel);
    }

    if (key_binding_pressed(gin, gs->bindings.wheel_toggle_search)) {
        gs->wheel.is_search_active = !gs->wheel.is_search_active;
    }
    if (gs->wheel.is_search_active && key_binding_pressed(gin, make_keybinding(BEAT_KEY_ESCAPE, KeyboardMods_None))) {
        gs->wheel.is_search_active = false;
        gs->wheel.search_str.len = 0;
        filter_wheel(&gs->wheel);
    }

    if (gs->wheel.is_search_active) {
        platform.text_input_start();
        auto *search_str = &gs->wheel.search_str;
        if (gin->text_input.len > 0) {
            if (search_str->append_if_it_fits(gin->text_input)) {
                filter_wheel(&gs->wheel);
            } else {
                printf("search_str would have hit the maximum length (%ld). not appending from gin->text_input '%.*s'\n",
                       search_str->cap, EXPAND_STR_FOR_PRINTF(gin->text_input));
            }
        }
        KeyBinding backspace_keys_for_delete_backwards[] = {
            make_keybinding(BEAT_KEY_BACKSPACE, KeyboardMods_None),
            make_keybinding(BEAT_KEY_BACKSPACE, KeyboardMods_Shift),
        };

        KeyBinding backspace_keys_for_delete_word_backwards[] = {
            make_keybinding(BEAT_KEY_BACKSPACE, KeyboardMods_Any|KeyboardMods_Ctrl),
            make_keybinding(BEAT_KEY_BACKSPACE, KeyboardMods_Any|KeyboardMods_Alt),
            make_keybinding(BEAT_KEY_W, KeyboardMods_Ctrl),
        };
        if (key_binding_pressed_or_repeat(gin, SLICE_FROM_STATIC(backspace_keys_for_delete_backwards))) {
            if (search_str->len >= 1) {
                s32 bytes_to_remove = utf8_decode_backwards(*search_str, 0, NULL);
                if (bytes_to_remove <= 0) bytes_to_remove = 1;
                search_str->len -= bytes_to_remove;
                ASSERT(search_str->len >= 0);
                filter_wheel(&gs->wheel);
            }
        }
        if (key_binding_pressed_or_repeat(gin, SLICE_FROM_STATIC(backspace_keys_for_delete_word_backwards))) {
            if (search_str->len >= 1) {
                bool stop_on_whitespace = false;
                while (true) {
                    u32 codepoint;
                    s32 bytes_to_remove = utf8_decode_backwards(*search_str, 0, &codepoint);
                    if (bytes_to_remove > 0) {
                        if (codepoint != ' ') stop_on_whitespace = true;
                        else if (stop_on_whitespace) break;
                        search_str->len -= bytes_to_remove;
                    } else if (search_str->len > 0) {
                        search_str->len -= 1;
                        stop_on_whitespace = true;
                    } else {
                        break;
                    }
                }
                filter_wheel(&gs->wheel);
            }
        }
        if (key_binding_pressed_or_repeat(gin, make_keybinding(BEAT_KEY_V, KeyboardMods_Ctrl))) {
            String clipboard = platform.get_clipboard(memory);
            // printf("clip: '%.*s'\n", EXPAND_STR_FOR_PRINTF(clipboard));
            if (clipboard.len > 0) {
                if (search_str->append_if_it_fits(clipboard)) {
                    filter_wheel(&gs->wheel);
                } else {
                    printf("search_str would have hit the maximum length (%ld). not appending from clipboard '%.*s'\n",
                           search_str->cap, EXPAND_STR_FOR_PRINTF(clipboard));
                }
            }
        }
    } else {
        // gs->wheel.search_str.len = 0;
        platform.text_input_stop();
    }


    if (key_binding_pressed(gin, gs->bindings.wheel_enter)) {
        if (gs->wheel.thenew.wheel_entries.len > 0) {
            gs->wheel.is_search_active = false;

            zero_struct(&gs->playing.params);
            auto *sel_entry = &gs->wheel.thenew.selected_wheel_entry;
            ASSERT(ABC(*sel_entry, gs->wheel.thenew.wheel_entries.len));
            auto sel_diff_loc = gs->wheel.thenew.wheel_entries.data[*sel_entry];
            auto *fileset = get_fileset_from_unique_id(*loaded_filesets, sel_diff_loc.fileset_unique_id);

            s32 diff_idx = sel_diff_loc.diff_idx;
            ASSERT(ABC(diff_idx, fileset->diffs_count));
            Diff *diff = &fileset->diffs[diff_idx];
            gs->playing.params.diff = diff;
            gs->playing.params.rate_id = gs->wheel.rate_id;
            gs->playing.params.rate_float = rate_id_to_float(gs->playing.params.rate_id);

            beat_log("Diff: %d/%d columns: %d rate_id: %d rate_float: %.2f\n",
                     diff_idx+1, fileset->diffs_count,
                     diff->notes.columns,
                     gs->playing.params.rate_id.id,
                     (f64)gs->playing.params.rate_float);

            enter_scene(memory, gs, SCENE_PLAYING);
            return;
        }
    }

    {
        s32 times = 0;
        if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_rate_faster) && !gs->wheel.is_search_active)
            times += RATE_ID_CONSTANT/10;
        if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_rate_slower) && !gs->wheel.is_search_active)
            times -= RATE_ID_CONSTANT/10;
        if (times != 0)
            gs->wheel.rate_id.id = (u16)clamp((s32)gs->wheel.rate_id.id + times, RATE_ID_MIN, RATE_ID_MAX);
    }

    static f32 accumulator_for_transition = 0;
    static f32 accumulator_for_transition_target = 0;
    if (gs->wheel.thenew.wheel_entries.len > 0) {
        s32 times = 0;
        if (gin->mouse.delta_wheel != 0) times -= gin->mouse.delta_wheel;
        if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_left)) times -= 1;
        if (key_binding_pressed_or_repeat(gin, gs->bindings.wheel_right)) times += 1;
        if (times != 0) {
            gs->wheel.thenew.selected_wheel_entry = modmod(gs->wheel.thenew.selected_wheel_entry + times,
                                                           gs->wheel.thenew.wheel_entries.len);

            if (accumulator_for_transition == accumulator_for_transition_target) {
                accumulator_for_transition = 0;
                accumulator_for_transition_target = (f32)times;
            } else {
                accumulator_for_transition_target += (f32)times;
            }
        }
    } else {
        gs->wheel.thenew.selected_wheel_entry = 0;
    }

    if (gs->wheel.thenew.wheel_entries.len <= 0) {
        ASSERT(gs->wheel.thenew.selected_wheel_entry == 0);
    }

#if BEAT_SLOW
    {
        for (smem i = 0; i < gs->wheel.thenew.loaded_filesets.len; i++) {
            auto *f = &gs->wheel.thenew.loaded_filesets.data[i];
            ASSERT(f->unique_id > 0);
        }
        for (smem i = 0; i < gs->wheel.thenew.sorted_diffs.len; i++) {
            auto diffloc = gs->wheel.thenew.sorted_diffs.data[i];
            auto *fileset = get_fileset_from_unique_id(*loaded_filesets, diffloc.fileset_unique_id);
            ASSERT(fileset);
            ASSERT(ABC(diffloc.diff_idx, fileset->diffs_count));
            // auto *diff = &fileset->diffs[diffloc.diff_idx];
        }
        for (smem i = 0; i < gs->wheel.thenew.wheel_entries.len; i++) {
            auto diffloc = gs->wheel.thenew.sorted_diffs.data[i];
            auto *fileset = get_fileset_from_unique_id(*loaded_filesets, diffloc.fileset_unique_id);
            ASSERT(fileset);
            ASSERT(ABC(diffloc.diff_idx, fileset->diffs_count));
        }
    }
#endif

    Font *font = memory->debug_font0;
    f32 font_size_px = SCENE_WHEEL_FONT_SIZE;
    f32 font_hi = font_size_px*((f32)font->ascent - (f32)font->descent + (f32)font->line_gap);

    s32 num_lines = 5;
    f32 border = 4.0f;

    v4 color_button_normal              = u32_to_color_v4(0xff1d1d1d);
    v4 color_button_selected_diff       = u32_to_color_v4(0xff4d4d1d);
    v4 color_button_selected_fileset    = u32_to_color_v4(0xff303030);
    v4 color_separator_normal           = u32_to_color_v4(0xffaaaaaa);
    v4 color_separator_selected_diff    = u32_to_color_v4(0xffeeeeee);
    v4 color_separator_selected_fileset = u32_to_color_v4(0xffffffdd);

    v2 rect_size = V2(800.0f, num_lines*font_hi + border);
    v2 rect_border_size = V2(rect_size.x, 8.0f);
    v2 pos = V2(0, gs->rendering.window_height*0.5f);
    v4 text_color = u32_to_color_v4(0xffffffff);

    f32 transition_offset = 0;
    do_smooth_scroll(&accumulator_for_transition, &accumulator_for_transition_target, &transition_offset, 8, (f32)gin->dt);
    transition_offset *= rect_size.y;

    if (gs->wheel.thenew.wheel_entries.len <= 0) {
        renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, pos, text_color,
                             "Empty wheel"_s);
    } else {
        auto sel_entry = gs->wheel.thenew.selected_wheel_entry;
        ASSERT(ABC(sel_entry, gs->wheel.thenew.wheel_entries.len));
        auto sel_diff_loc = gs->wheel.thenew.wheel_entries.data[sel_entry];

        smem const margin_entry_count = 10 + (smem)((f32)gs->rendering.window_height / (f32)rect_size.y / 2);
        auto wheel_entrie_start = MAXIMUM(0, sel_entry - margin_entry_count);
        auto wheel_entrie_end = MINIMUM(gs->wheel.thenew.wheel_entries.len, sel_entry + margin_entry_count);
        for (auto idx_wheel_entrie = wheel_entrie_start; idx_wheel_entrie < wheel_entrie_end; idx_wheel_entrie++) {
            auto diff_loc = gs->wheel.thenew.wheel_entries.data[idx_wheel_entrie];

            v2 themin = pos;
            themin.y -= (f32)(idx_wheel_entrie - sel_entry)*(rect_size.y + rect_border_size.y);

            {
                v2 min = themin;
                min.y -= transition_offset;

                auto *fileset = get_fileset_from_unique_id(*loaded_filesets, diff_loc.fileset_unique_id);
                Diff *diff = &fileset->diffs[diff_loc.diff_idx];
                Metadata *metadata = &diff->metadata;

                v2 rect_min = V2(min.x, min.y - (num_lines-1)*font_hi + font_size_px*font->descent - border);

                v4 col_button, col_separator;
                if (diff_loc.fileset_unique_id == sel_diff_loc.fileset_unique_id) {
                    if (idx_wheel_entrie == sel_entry) {
                        col_button = color_button_selected_diff;
                        col_separator = color_separator_selected_diff;
                    } else {
                        col_button = color_button_selected_fileset;
                        col_separator = color_separator_selected_fileset;
                    }
                } else {
                    col_button = color_button_normal;
                    col_separator = color_separator_normal;
                }

                {
                    f32 text_pad_x = 4;
                    renderer_push_rectangle(&gs->rendering, col_button, rect_min, rect_size);
                    renderer_push_rectangle(&gs->rendering, col_separator, rect_min - V2(0, rect_border_size.y), rect_border_size);

                    renderer_push_strings_v(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0), text_color,
                                            "Filepath: "_s, diff->filepath);
                    min.y -= font_hi;

                    bool show_translit_thing = true;
                    if (show_translit_thing && metadata->title_translit.len > 0) {
                        renderer_push_strings_v(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0),
                                                text_color,
                                                "Artist: "_s, metadata->artist, " -> Translit: "_s, metadata->artist_translit);
                    } else {
                        renderer_push_strings_v(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0),
                                                text_color,
                                                "Artist: "_s, metadata->artist);
                    }
                    min.y -= font_hi;

                    if (show_translit_thing && metadata->title_translit.len > 0) {
                        renderer_push_strings_v(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0),
                                                text_color,
                                                "Title: "_s, metadata->title, " -> Translit: "_s, metadata->title_translit);
                    } else {
                        renderer_push_strings_v(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0),
                                                text_color,
                                                "Title: "_s, metadata->title);
                    }
                    min.y -= font_hi;

                    u8 buf[128];
                    String temp_str = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                                     "Diff name (%d/%d): %.*s", diff_loc.diff_idx+1, fileset->diffs_count,
                                     EXPAND_STR_FOR_PRINTF(metadata->diff_name));
                    renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0), text_color, temp_str);
                    min.y -= font_hi;

                    xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                                     "Keys: %d  Keymode: %.*s Bpms: %d", diff->notes.columns,
                                     EXPAND_STR_FOR_PRINTF(keymode_to_fancy_string_table[diff->notes.keymode]),
                                     (s32)diff->notes.timings.bpms_count);
                    renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min+V2(text_pad_x,0), text_color, temp_str);
                    min.y -= font_hi;
                }
            }
        }
    }

    v4 col_button = color_button_normal;
    v4 col_separator = color_separator_normal;
    {
        smem count_async_loading_songs = count_work_async_data_being_used(&gs->work_async_data_arr_wheel_add_directory);
        smem count_async_loading_fonts = count_work_async_data_being_used(&gs->work_async_data_arr_load_font_chunk);

        num_lines = 4;
        if (count_async_loading_songs > 0) num_lines++;
        if (count_async_loading_fonts > 0) num_lines++;
        v2 rect_max = V2((f32)gs->rendering.window_width - 10, (f32)gs->rendering.window_height - 10);
        rect_size = V2(450, font_hi*(num_lines) - font_size_px*font->descent + 2*border);
        v2 rect_min = rect_max - rect_size;
        v2 min = V2(rect_min.x + border, rect_max.y - font_hi - border - font_size_px*font->descent);
        renderer_push_rectangle(&gs->rendering, col_button, rect_min, rect_size);
        renderer_push_rectangle(&gs->rendering, col_separator,
                                V2(rect_min.x, rect_min.y - rect_border_size.y),
                                V2(rect_size.x, rect_border_size.y));

        u8 buf[128];
        String temp_str = {};

        {
#define PUT_QUOTE_AROUND_SEARCH 0
            xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                             "search %s (%ld): %s%.*s", // there was a quote before %.*s
                             gs->wheel.is_search_active ? "(active)" : "(inactive)",
                             gs->wheel.search_str.len,
                             PUT_QUOTE_AROUND_SEARCH ? "'" : "",
                             EXPAND_STR_FOR_PRINTF(gs->wheel.search_str));
            f32 search_new_minx = renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min, text_color, temp_str);

            if (gin->text_composition.len > 0) {
                xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                                 "%.*s",
                                 EXPAND_STR_FOR_PRINTF(gin->text_composition));
                f32 old_x = search_new_minx;
                v2 editing_min = V2(search_new_minx, min.y);
                search_new_minx = renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, editing_min,
                                                       text_color, temp_str);
                v2 editing_line_size = {};
                editing_line_size.x = search_new_minx - old_x;
                editing_line_size.y = 1;
                renderer_push_rectangle(&gs->rendering, text_color, editing_min+V2(0, font_size_px*font->descent), editing_line_size);

                platform.text_input_set_rect(editing_min.x,
                                             editing_min.y + font_size_px*font->descent,
                                             editing_line_size.x,
                                             font_hi,
                                             gs->rendering.window_height);
            }


            v2 cursor_min = V2(search_new_minx, min.y+font_size_px*font->descent);
#if PUT_QUOTE_AROUND_SEARCH
            temp_str.len = xsnprintf((char *)buf, ssizeof(buf), "'");
            renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, V2(search_new_minx, min.y), text_color, temp_str);
#endif
#undef PUT_QUOTE_AROUND_SEARCH
            min.y -= font_hi;

            if (gs->wheel.is_search_active) {
                v2 cursor_size = V2(2, font_hi);
                v4 cursor_color = text_color;
                static f32 t_for_cursor_alpha = 0;
                f32 period_seconds = 0.8f;
                t_for_cursor_alpha += (f32)gin->dt * 2*3.1415926535f / period_seconds;
                t_for_cursor_alpha = fmodf(t_for_cursor_alpha, 2*3.1415926535f); // TODO: PI constant
                cursor_color.a = sinf(t_for_cursor_alpha)/2 + 0.5f;
                // cursor_color.a = t_for_cursor_alpha > 3.1415926535f ? 1 : 0;
                renderer_push_rectangle(&gs->rendering, cursor_color, cursor_min, cursor_size);
                // search_new_minx += cursor_size.x + 2;
            }
        }

        smem a = gs->wheel.thenew.selected_wheel_entry;
        smem b = gs->wheel.thenew.wheel_entries.len;
        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "Songs: (diff %d/%d) filesets %d", (s32)a+1, (s32)b,
                         (s32)gs->wheel.thenew.loaded_filesets.len);
        renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min, text_color, temp_str);
        min.y -= font_hi;

        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "Rate: %.2fx", (f64)rate_id_to_float(gs->wheel.rate_id));
        renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min, text_color, temp_str);
        min.y -= font_hi;

        xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                         "scroll x: %.9f tgt: %.2f",
                         (f64)accumulator_for_transition,
                         (f64)accumulator_for_transition_target);
        renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min, text_color, temp_str);
        min.y -= font_hi;

        if (count_async_loading_songs > 0) {
            xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                             "async loading songs: %ld",
                             count_async_loading_songs);
            renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min, text_color, temp_str);
            min.y -= font_hi;
        }

        if (count_async_loading_fonts > 0) {
            xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                             "async loading fonts: %ld",
                             count_async_loading_fonts);
            renderer_push_string(memory, &gs->rendering, SCENE_WHEEL_FONT_SIZE, min, text_color, temp_str);
            min.y -= font_hi;
        }
    }
}

static SnappingType *
make_snappings_for_diff(MemoryArena *arena, Diff *diff)
{
    u64 clock_start = platform.get_clock();
    f32 test_snap_ratios[SnappingType_Count] = {
        [SnappingType_Unknown] = 0.0f, // should never hit this in the is_snapped_to_f64(a, test)
        [SnappingType_1_1] = 1.0f/1.0f,
        [SnappingType_1_2] = 1.0f/2.0f,
        [SnappingType_1_4] = 1.0f/4.0f,
        [SnappingType_1_8] = 1.0f/8.0f,

        [SnappingType_1_3] = 1.0f/3.0f,
        [SnappingType_1_6] = 1.0f/6.0f,
        [SnappingType_1_12] = 1.0f/12.0f,
        [SnappingType_1_24] = 1.0f/24.0f,
    };
    SnappingType *snappings = arena_push_struct_count(arena, SnappingType, diff->notes.rows_count, arena_flags_nozero());
    s32 prev_bi = 0;
    for (s32 idx_row = 0;
         idx_row < diff->notes.rows_count;
         idx_row++) {
        NoteTimestamp note_ts = diff->notes.timestamps[idx_row];
        s32 idx_bi = find_beat_idx_by_timestamp(&diff->notes.timings, note_ts, prev_bi, NULL, true);
        prev_bi = idx_bi;
        BeatInfo *bi = &diff->notes.timings.bpms[idx_bi];
        f64 beat = note_timestamp_diff_to_seconds(note_ts - bi->ts_start, 1)*(f64)bi->bps;
        ASSERT(beat >= 0.0);
        f32 beat_snap_ratio = (f32)(beat - (f64)(s32)beat);
        SnappingType this_snap_type = SnappingType_Unknown;
        for (SnappingType idx_test = SnappingType_Unknown+1; idx_test <ARRAY_COUNT(test_snap_ratios); idx_test++) {
            if (is_snapped_to(beat_snap_ratio, test_snap_ratios[idx_test])) {
                this_snap_type = idx_test;
                break;
            }
        }
        if (this_snap_type == SnappingType_Unknown) {
            printf("Unknown snapping ratio: %.6f\n", (f64)beat_snap_ratio);
        }
        snappings[idx_row] = this_snap_type;
    }
    u64 clock_diff = platform.get_clock() - clock_start;
    f64 seconds_elapsed = (f64)clock_diff * platform.get_secs_per_clock();
    printf("make_snappings_for_diff took %.2fms\n", 1000.0*seconds_elapsed);
    return snappings;
}

static bool
visual_config_is_valid(PlayingVisualConfig *vc)
{
    bool valid;
    if (vc->note_coloring_by_column) {
        valid = vc->colors_by_column_count > 0;
        if (!valid) return false;
    }
    // TODO: More checks maybe
    return true;
}

static void
init_scene_playing(GameMemory *memory, GameState *gs)
{
    ASSERT(gs->wheel.is_initialized);
    ScenePlaying *playing = &gs->playing;
    // maybe free some things here before zeroing the struct.
    ASSERT(!playing->is_initialized);
    playing->is_initialized = true;
    playing->arena = &memory->playing;
    // printf("Debug: playing->arena->used == %d\n", (int)playing->arena->used);
    // arena_clear(playing->arena);

    zero_struct(&playing->other);

    ASSERT(playing->params.diff != NULL);

    Diff *diff = playing->params.diff;
    playing->other.keymode = diff->notes.keymode;
    playing->other.columns = diff->notes.columns;
    s32 columns = playing->other.columns;
    ASSERT(columns == keymode_to_columns_table[playing->other.keymode]);

    playing->other.visual_config = &gs->playing_visual_configs_per_keymode[playing->other.keymode];
    ASSERT(visual_config_is_valid(playing->other.visual_config));

    playing->other.per_column = arena_push_struct_count(playing->arena, ScenePlayingPerColumnThing, columns, arena_flags_zero());
    for (s32 idx_col = 0; idx_col < columns; idx_col++) {
        auto *per_col = &playing->other.per_column[idx_col];
        NewArrayDynamic<VisibleNote> *vns = &per_col->vns;
        vns->arena = playing->arena;
        vns->reserve(128);

        NewArrayDynamic<TimedInput> *tis = &per_col->tis;
        tis->arena = playing->arena;
        tis->reserve(8);

        auto *debug_test_accs = &per_col->debug_test_accs;
        debug_test_accs->arena = playing->arena;
        debug_test_accs->reserve(2048);
    }

    playing->other.snappings = make_snappings_for_diff(playing->arena, diff);

    if (diff->debug_mp3_filename.len > 0) {
        ASSERT(string_is_nullter(diff->debug_mp3_filename));
        playing->other.debug_the_audio_decoder_id = audio::make_audio_decoder(diff->debug_mp3_filename, diff->debug_mp3_offset);

        {
            u8 buf[2048];
            String bufstr = {};
            xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                             "make_audio_decoder %s '%.*s'",
                             playing->other.debug_the_audio_decoder_id.is_valid() ? "success" : "failure",
                             EXPAND_STR_FOR_PRINTF(diff->debug_mp3_filename));
            add_fading_text(gs, bufstr);
        }
    } else {
        // NOT_IMPLEMENTED;
        // Do nothing.
        {
            u8 buf[512];
            String bufstr = {};
            xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "make_audio_decoder failure no filename mp3");
            add_fading_text(gs, bufstr);
        }
    }

    playing->other.state = PlayingState_Ready;
}

static void
leave_scene_playing(GameMemory *memory, GameState *gs)
{
    ScenePlaying *pl = &gs->playing;
    ASSERT(pl->is_initialized);

    pl->is_initialized = false;
    arena_temp_check(pl->arena);
    printf("leaving playing: arena high water mark: %ld\n", pl->arena->high_water_mark);

    if (gs->playing.other.debug_the_audio_decoder_id.is_valid()) {
        audio::remove_audio_decoder(pl->other.debug_the_audio_decoder_id);
    }

    arena_clear(pl->arena);
    pl->arena->high_water_mark = 0;
    zero_struct(pl);
    enter_scene(memory, gs, SCENE_WHEEL);
}

static v4
get_note_color(ScenePlaying *pl, s32 idx_row, s32 idx_col)
{
    v4 color;
    if (!pl->other.visual_config->note_coloring_by_column) {
        ASSERT(ABC(idx_row, pl->params.diff->notes.rows_count));
        SnappingType snapping_type = pl->other.snappings[idx_row];
        color = get_snapping_type_color(pl->other.visual_config, snapping_type);
    } else {
        s32 col_idx = clamp(idx_col, 0, pl->other.visual_config->colors_by_column_count-1);
        color = pl->other.visual_config->colors_by_column[col_idx];
    }
    return color;
}

static v4
get_color_for_abs_offset(f32 abs_offset_seconds)
{
    v4 color;
    if (abs_offset_seconds < (f32)JUDGE_MARV_MAX) {        // marv, white
        color = V4(1.0f, 1.0f, 1.0f, 1);
    } else if (abs_offset_seconds < (f32)JUDGE_PERF_MAX) { // perf, yellow
        color = V4(1.0f, 1.0f, 0.0f, 1);
    } else if (abs_offset_seconds < (f32)JUDGE_GREAT_MAX) { // great, green
        color = V4(0.0f, 1.0f, 0.0f, 1);
    } else if (abs_offset_seconds < (f32)JUDGE_GOOD_MAX) { // good, blue
        color = V4(0.0f, 0.5f, 1.0f, 1);
    } else if (abs_offset_seconds < (f32)JUDGE_BAD_MAX) { // bad, red
        color = V4(0.8f, 0.0f, 0.5f, 1);
    } else {
        color = V4(1.0f, 0.0f, 0.0f, 1);
    }
    return color;
}

static void
scene_playing_logic(GameMemory *memory, GameState *gs, GameInput *gin)
{
    if (!gs->playing.is_initialized) {
        init_scene_playing(memory, gs);
    }

    // always set to false at the start
    // if we happen to set started=true this frame, we'll set this to true too
    if (key_binding_pressed(gin, gs->bindings.wheel_back)) {
        leave_scene_playing(memory, gs);
        // TODO: Don't just return. Screen will flash.
        return;
    }

    v4 text_color = u32_to_color_v4(0xffffffff);

    if (key_binding_pressed_or_repeat(gin, gs->bindings.playing_seek_negative)) {
        if (gs->playing.other.state >= PlayingState_Started) {
            set_playing_time_ts(gs, gs->playing.other.newthings.song_ts + (NoteTimestampDiff)seconds_to_song_ts(-2, gs->playing.params.rate_float),
                                /* gin->clock_frame_start, */ gin->os_time_seconds_frame_start);
        }
    }
    if (key_binding_pressed_or_repeat(gin, gs->bindings.playing_seek_positive)) {
        if (gs->playing.other.state >= PlayingState_Started) {
            set_playing_time_ts(gs, gs->playing.other.newthings.song_ts + (NoteTimestampDiff)seconds_to_song_ts(+2, gs->playing.params.rate_float),
                                /* gin->clock_frame_start, */ gin->os_time_seconds_frame_start);
        }
    }

    bool unpaused_on_this_frame = false;
    if (gs->playing.other.state == PlayingState_Started && key_binding_pressed(gin, gs->bindings.playing_pause)) {
        gs->playing.other.paused = !gs->playing.other.paused;
        if (gs->playing.other.paused) {
            printf("---PAUSED---\n");
        } else {
            printf("--UNPAUSED--\n");
            unpaused_on_this_frame = true;
        }
        gs->playing.other.reference.os_time_seconds = gin->os_time_seconds_frame_start;
        // gs->playing.other.reference.os_clock        = gin->clock_frame_start;
        gs->playing.other.reference.song_ts         = gs->playing.other.newthings.song_ts;

        audio::sound_set_pause(gs->playing.other.debug_the_audio_decoder_id, gs->playing.other.paused);
    }

    if (key_binding_pressed(gin, gs->bindings.playing_restart)) {
        printf("--RESTART---\n");
        // start 2 seconds before the first beat. after audio is implemented, this
        // will change to the start of the song relative to the first beat
        // free gs->playing.other.acc
        // For now the way it works is if you're ready and didn't start, you automatically start.
        if (gs->playing.other.state >= PlayingState_Started) {
            gs->playing.other.state = PlayingState_Ready;
        }
    }

    if (key_binding_pressed_or_repeat(gin, gs->bindings.scroll_slower)) {
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "%s", "--SCROLL SLOWER: ");
        switch (gs->playing.other.visual_config->scroll.type) {
        case SCROLL_TYPE_PIXELS_PER_SECOND: {
            if (gs->playing.other.visual_config->scroll.pixels_per_second > SCROLL_LIMIT_MIN) {
                gs->playing.other.visual_config->scroll.pixels_per_second =
                    (s32)((f32)gs->playing.other.visual_config->scroll.pixels_per_second /
                          SCROLL_CHANGE_MULTIPLIER);
            }
            xsnprintf_string(buf, ssizeof(buf), true, &bufstr, "PXPS: %d---", gs->playing.other.visual_config->scroll.pixels_per_second);
        } break;
        case SCROLL_TYPE_TIME_TO_APPEAR:  {
            if (gs->playing.other.visual_config->scroll.time_to_appear < SCROLL_LIMIT_MAX) {
                gs->playing.other.visual_config->scroll.time_to_appear =
                    (s32)((f32)gs->playing.other.visual_config->scroll.time_to_appear *
                          SCROLL_CHANGE_MULTIPLIER);
            }
            xsnprintf_string(buf, ssizeof(buf), true, &bufstr, "TTA: %d---", gs->playing.other.visual_config->scroll.time_to_appear);
        } break;
        }
        printf("%.*s\n", EXPAND_STR_FOR_PRINTF(bufstr));
        add_fading_text(gs, bufstr);
    }
    if (key_binding_pressed_or_repeat(gin, gs->bindings.scroll_faster)) {
        u8 buf[512];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "%s", "--SCROLL FASTER: ");
        switch (gs->playing.other.visual_config->scroll.type) {
        case SCROLL_TYPE_PIXELS_PER_SECOND: {
            if (gs->playing.other.visual_config->scroll.pixels_per_second < SCROLL_LIMIT_MAX) {
                gs->playing.other.visual_config->scroll.pixels_per_second =
                    (s32)((f32)gs->playing.other.visual_config->scroll.pixels_per_second *
                          SCROLL_CHANGE_MULTIPLIER);
            }
            xsnprintf_string(buf, ssizeof(buf), true, &bufstr, "PXPS: %d---", gs->playing.other.visual_config->scroll.pixels_per_second);
        } break;
        case SCROLL_TYPE_TIME_TO_APPEAR: {
            if (gs->playing.other.visual_config->scroll.time_to_appear > SCROLL_LIMIT_MIN) {
                gs->playing.other.visual_config->scroll.time_to_appear =
                    (s32)((f32)gs->playing.other.visual_config->scroll.time_to_appear /
                          SCROLL_CHANGE_MULTIPLIER);
            }
            xsnprintf_string(buf, ssizeof(buf), true, &bufstr, "TTA: %d---", gs->playing.other.visual_config->scroll.time_to_appear);
        } break;
        }
        printf("%.*s\n", EXPAND_STR_FOR_PRINTF(bufstr));
        add_fading_text(gs, bufstr);
    }

    static u64 debug__frames_after_ready = 0;
    if (gs->playing.other.state == PlayingState_Ready) {
        gs->playing.other.state = PlayingState_Started;
        set_playing_time_seconds(gs, -2, /* gin->clock_frame_start, */ gin->os_time_seconds_frame_start);
        // set_playing_time_seconds(gs, +3, /* gin->clock_frame_start, */ gin->os_time_seconds_frame_start);
        debug__frames_after_ready = 0;
    }



    if (gs->playing.other.state >= PlayingState_Started) {
        Diff *diff = gs->playing.params.diff;
        ASSERT(diff != NULL);
        ASSERT(diff->notes.timings.bpms_count > 0);

        bool last_reported_was_temp_for_seek =
            (gs->playing.other.reference.last_reported_audio_sound_time == LAST_REPORTED_AUDIO_SOUND_TIME_FOR_SET_PLAYING_TIME_TS);
        bool we_actually_seeked_on_this_frame = global_frame_count == gs->playing.other.reference.last_seek_frame_count;
        // TODO: I have an idea to fix the bug that happens when I press a key
        // in the same frame I seeked or paused: Ignore keypresses that happen
        // close to when I pause / seek. If a key was already press, send a
        // virtual "key up" for that key.
        // increase time if not paused
        if (!gs->playing.other.paused && gs->playing.other.state < PlayingState_Ended) {
            auto prev_song_ts = gs->playing.other.newthings.song_ts;
            auto new_audio_pos = gs->playing.other.reference.last_reported_audio_sound_time;
            {
                SCOPED_SHARED_LOCK(&audio::global_audio_state.decoders.shared_mutex);
                auto *decoder = audio::get_decoder_by_id__already_have_the_lock(gs->playing.other.debug_the_audio_decoder_id);
                if (decoder) {
                    SCOPED_LOCK(&decoder->locked.mutex);
                    if (!decoder->locked.data.mixer.is_finished) {
                        // this could return the same value as the last reported
                        new_audio_pos = decoder->locked.data.mixer.curr_sound_time;
                    }
                }
            }
            if (new_audio_pos == gs->playing.other.reference.last_reported_audio_sound_time) {
                // printf("-@-- Same reported audio position. Incrementing song time with dt.\n");
                // this handles when the audio ended nicely.
                // NOTE Found the bug!!! (I think).
                // I seek and set reference.os_time_seconds to a value bigger than os_time_seconds_frame_start.
                // Now I calculate newthings.song_ts and it is smaller than prev_song_ts!
                // To fix it I'd only need to not do anything here if I seeked on this frame.
                // NOTE Actually, no that's not it....
                if (!we_actually_seeked_on_this_frame) {
                    gs->playing.other.newthings.song_ts = seconds_to_song_ts(
                        gin->os_time_seconds_frame_start, gs->playing.params.rate_float,
                        gs->playing.other.reference.os_time_seconds, gs->playing.other.reference.song_ts);
                    // This happens very frequently if portaudio's frames per buffer is 512.
                    // if portaudio's frames per buffer is paFramesPerBufferUnspecified, then this almost never happens (it is at least very infrequent)
#if 0
                    printf("--- new_audio_pos == last_reported_audio_soung_time\n");
#endif
                } else {
                    // No need to update song_ts. We already did when we seeked.
                    // actually, this probably never will happen beacuse new_audio_pos will be decoder->curr_sound_time;
                    // NOTE: This can happen if I seek at the end of the song!
                    printf("------- Actually seeked on this frame and new_audio_pos == last_reported_audio_sound_time\n");
                }
            } else {
                ASSERT(new_audio_pos != LAST_REPORTED_AUDIO_SOUND_TIME_FOR_SET_PLAYING_TIME_TS);
                // NOTE: If we_actually_seeked_on_this_frame I am sure that new_audio_pos is valid according to the new playing position after the seek.
                auto expected_audio_pos = gs->playing.other.reference.last_reported_audio_sound_time + gin->debug_dt_frame*gs->playing.params.rate_float; // TODO don't use dt_frame. eh whatever won't make any difference since audio won't work with the time multiplier.
                f64 some_big_value = 500/1000.0;
                if (fabs(expected_audio_pos - new_audio_pos) > gs->playing.params.rate_float*some_big_value
                    || last_reported_was_temp_for_seek) {
                    // we seeked or an underrun occured
                    if (last_reported_was_temp_for_seek) {
#if 1
                        printf("-+-+ We got seek temp position. -> timestamp %.2f. We seeked that's ok.\n",
                               new_audio_pos/gs->playing.params.rate_float);
#endif

                        // Assert that we got a valid new audio position.
                    } else {
                        // Underrun
                        if (!unpaused_on_this_frame) {
                            printf("-$$😡 New reported audio position. Not expected. %.2f diff real seconds -> timestamp %.2f. We did NOT seek. An underrun occurred (probably)\n",
                                   (expected_audio_pos - new_audio_pos)/gs->playing.params.rate_float,
                                   new_audio_pos/gs->playing.params.rate_float);
                            DEBUG_BREAK; // I don't want to miss this if it happens. // This happens if I pause.
                        } else {
                            // turns out this doesn't happen when I unpause.
                            printf("-- Unpaused on this frame\n");
                        }
                    }
                    // TODO: `new_audio_pos` represents the position of the song at time X (not exactly, but close to the time that I loaded `new_audio_pos` from the audio struct). The frame started at time S. We are off sync by at least (X-S) seconds. The higher the frame rate the less off sync I will be. Actually, I don't need the the entire frame to be fast, only the time it takes from the start of the frame to here must be very fast.

                    gs->playing.other.newthings.song_ts = audio::sound_time_to_note_timestamp(
                        new_audio_pos,
                        diff->debug_mp3_offset,
                        gs->playing.params.rate_float);
                    gs->playing.other.reference.song_ts = gs->playing.other.newthings.song_ts;
                    gs->playing.other.reference.os_time_seconds = gin->os_time_seconds_frame_start;
                } else {
#if 0
                    printf("---- New reported audio position. Close to expected. %.2f real seconds\n", (expected_audio_pos - new_audio_pos)/gs->playing.params.rate_float);
#endif

                    auto new_song_pos_with_dt = seconds_to_song_ts(
                        gin->os_time_seconds_frame_start, gs->playing.params.rate_float,
                        gs->playing.other.reference.os_time_seconds, gs->playing.other.reference.song_ts);

                    auto new_song_pos_with_audio_pos = audio::sound_time_to_note_timestamp(
                        new_audio_pos,
                        diff->debug_mp3_offset,
                        gs->playing.params.rate_float);

                    gs->playing.other.newthings.song_ts = {(new_song_pos_with_dt.value + new_song_pos_with_audio_pos.value) / 2};

                    // Update references. On the next frame we will use them.
                    gs->playing.other.reference.song_ts = gs->playing.other.newthings.song_ts;
                    gs->playing.other.reference.os_time_seconds = gin->os_time_seconds_frame_start;
                }

                gs->playing.other.reference.last_reported_audio_sound_time = new_audio_pos;
            }
            // TODO: This assertion is hitting. Fix it.
            // ASSERT(gs->playing.other.newthings.song_ts.value > prev_song_ts.value
            //        || (we_seeked_on_this_frame && gs->playing.other.newthings.song_ts.value == prev_song_ts.value));
            // ASSERT(gs->playing.other.newthings.song_ts.value > prev_song_ts.value || we_actually_seeked_on_this_frame);
            // This is off by about 1ms when I seek (including song startup).
            // If I had input at this frame, shit would break.

            gs->playing.other.newthings.curr_row_idx =
                find_row_idx_by_similar_timestamp(&diff->notes, gs->playing.other.newthings.song_ts, gs->playing.other.newthings.curr_row_idx, true);

            gs->playing.other.newthings.curr_bpm_idx =
                find_beat_idx_by_timestamp(&diff->notes.timings, gs->playing.other.newthings.song_ts,
                                           // gs->playing.other.newthings.curr_bpm_idx, NULL, we_seeked_on_this_frame);
                                           gs->playing.other.newthings.curr_bpm_idx, NULL, true); // TODO: can_go_reverse should be we_seeked_on_this_frame. assertion prev_song_ts > this song ts made this not work
        }


        ASSERT(note_timestamp_diff_to_seconds((NoteTimestampDiff)gs->playing.other.newthings.song_ts, gs->playing.params.rate_float) > -100);

        if (gs->playing.other.newthings.curr_row_idx == diff->notes.rows_count - 1
            && (gs->playing.other.newthings.song_ts
                > diff->notes.timestamps[diff->notes.rows_count-1] + (NoteTimestampDiff)seconds_to_song_ts(+2, gs->playing.params.rate_float))) {
            // In the future that 2.0 seconds should be replaced with the miss
            // window plus 1 or 2 seconds (so that a big miss window works on the last note.)
            // reached end of song (2 seconds after the last note)
            if (gs->playing.other.state < PlayingState_Ended) {
                printf("---END---!\n");
            }
            gs->playing.other.state = PlayingState_Ended;
        }

        f32 window_scale = get_window_scale_factor(gs->playing.other.visual_config, (f32)gs->rendering.window_height);
        f32 half_window_width = 0.5f * (f32)gs->rendering.window_width;
        f32 half_note_spacing = 0.5f * gs->playing.other.visual_config->note_spacing * window_scale;
        f32 half_note_width = 0.5f * gs->playing.other.visual_config->note_width * window_scale;
        f32 mine_half_note_width = 0.125f * half_note_width;
        f32 note_width_plus_spacing = 2.0f*half_note_width + 2.0f*half_note_spacing;;
        f32 left_of_playfield =
            half_window_width + half_note_spacing -
            ((0.5f*(f32)(diff->notes.columns)) * note_width_plus_spacing);
        f32 playfield_width = (f32)(diff->notes.columns)*note_width_plus_spacing - 2.0f*half_note_spacing;
        f32 receiver_height_centered = gs->playing.other.visual_config->note_base_height*window_scale + half_note_width;


        // Draw the Background
        {
            v4 color = gs->playing.other.visual_config->background_color;
            v2 min = V2(0, 0);
            v2 size = V2((f32)gs->rendering.window_width, (f32)gs->rendering.window_height);
            renderer_push_rectangle(&gs->rendering, color, min, size);
        }

        // TODO: Put the code that draws the thing inside a function so that I
        // can use it for both preview and gameplay (pass the playfield as an
        // argument).

        if (gs->playing.other.state < PlayingState_Ended) {
            BeatInfo this_frame_bi;
            gs->playing.other.newthings.curr_bpm_idx =
                find_beat_idx_by_timestamp(&diff->notes.timings, gs->playing.other.newthings.song_ts,
                                           gs->playing.other.newthings.curr_bpm_idx, &this_frame_bi, false);

            ASSERT(this_frame_bi.ts_start.value >= 0);

            ASSERT(gs->playing.other.newthings.curr_row_idx >= 0);
            newsong_game_loop(gs, gin);

            // Draw the playfield
            {
                v4 color = gs->playing.other.visual_config->playfield_color;
                v2 min = V2(left_of_playfield - 2.0f*half_note_spacing, 0);
                v2 size = V2(playfield_width + 4.0f*half_note_spacing, (f32)gs->rendering.window_height);
                renderer_push_rectangle(&gs->rendering, color, min, size);
            }

            {
                static f32 alpha_for_col[128] = {};
                for (int idx_col = 0; idx_col < diff->notes.columns; idx_col++) {
                    v4 color = u32_to_color_v4(0xff111111);
                    if (column_key_down(gs, gin, diff->notes.keymode, idx_col)) {
                        alpha_for_col[idx_col] = 1.0f; // 1.2f
                    } else {
                        f32 time_to_live = 0.2f;
                        alpha_for_col[idx_col] = MAXIMUM(0, alpha_for_col[idx_col] - (f32)gin->dt*1/time_to_live);
                    }
                    color.a = alpha_for_col[idx_col];
                    if (color.a > 0) {
                        v2 min_left = V2(left_of_playfield + idx_col*note_width_plus_spacing,
                                         receiver_height_centered);
                        f32 to_remove = (1-color.a)*half_note_width;
                        to_remove = 0;
                        v2 size = V2(2*half_note_width, gs->rendering.window_height - receiver_height_centered);
#if !DIAMOND_FOR_NOTES
                        min_left.y -= half_note_width;
                        size.y += half_note_width;
#endif
                        min_left.x += to_remove;
                        size.x -= 2*to_remove;
                        color.a = MINIMUM(1, color.a + 0.1f);
                        renderer_push_rectangle(&gs->rendering, color, min_left, size);
                    }
                }
            }

            // Make the render entries
            // Notes
            {
                // I could render these at the same time that I update them.
                // For now I'll keep it here.

                {
                    f32 notepos_to_add, notepos_to_multiply;
                    get_timestamp_to_visual_distance_multipler(gs->playing.other.visual_config,
                                                               (f32)gs->rendering.window_height,
                                                               gs->playing.params.rate_float,
                                                               &notepos_to_multiply, &notepos_to_add);

                    v2 note_pos_centered_base = V2(left_of_playfield+half_note_width, half_note_width);
                    for (s32 idx_col = 0; idx_col < diff->notes.columns; idx_col++) {
                        NewArrayDynamic<VisibleNote> *vns = &gs->playing.other.per_column[idx_col].vns;
                        for (s32 idx_vn = 0; idx_vn < vns->len; idx_vn++) {
                            VisibleNote *vn = &vns->data[idx_vn];
                            ASSERT(vn->type != NOTE_BLANK);
                            u8 onenote = vn->type;
                            // We're making an unnecessary recalculation here.
                            NoteTimestampDiff delta_ts = vn->song_ts - gs->playing.other.newthings.song_ts;
                            f32 pixels_from_bottom = calc_pixels_from_bottom(notepos_to_add, notepos_to_multiply, delta_ts);
                            if (is_note_offscreen(gs, pixels_from_bottom)) {
                                break;
                            }
                            // TODO: Verify that everything's right after I introduced debug__global_visual_offset_seconds.
                            v2 note_pos_centered = note_pos_centered_base;
                            note_pos_centered.x += (f32)idx_col * note_width_plus_spacing;
                            note_pos_centered.y += pixels_from_bottom;
                            if (onenote == NOTE_TAP) {
                                v4 color = get_note_color(&gs->playing, vn->idx_row, idx_col);
                                // bool draw = note_pos_centered.y >= receiver_height_centered;
                                // TODO: if (previewing_chart && preview_autoplay) draw = (note is above receivers);
                                bool draw = !(vn->flags & VN_TAP_DO_NOT_DRAW);
                                if (draw)
                                    renderer_push_diamond(&gs->rendering, color, note_pos_centered, half_note_width);
                            } else if (onenote == NOTE_HOLD) {
                                v4 color = get_note_color(&gs->playing, vn->idx_row, idx_col);
                                // v4 color = u32_to_color_v4(0xffffffff);
                                v4 middle_color; // = u32_to_color_v4(0xff999999);
                                if (vn->flags & VN_HOLD_IS_PRESSED) {
                                    middle_color = u32_to_color_v4(0xff999999);
                                } else {
                                    middle_color = u32_to_color_v4(0xff666666);
                                }
                                v4 note_end_color = middle_color;
                                v2 note_end_pos_centered;
                                note_end_pos_centered.x = note_pos_centered.x;
                                NoteTimestampDiff end_delta_ts = vn->hold.song_ts_end - gs->playing.other.newthings.song_ts;
                                f32 note_end_pixels_from_bottom = (f32)gs->rendering.window_height + 10;
                                if (vn->flags & VN_HOLD_VISIBLE_END) {
                                    note_end_pixels_from_bottom = calc_pixels_from_bottom(notepos_to_add, notepos_to_multiply, end_delta_ts);
                                }
                                note_end_pos_centered.y = note_pos_centered_base.y+note_end_pixels_from_bottom;
                                // rectangle between them
                                v2 middle_min = note_pos_centered;
                                middle_min.x -= half_note_width;

                                v2 middle_size = V2(2.0f * half_note_width, note_end_pixels_from_bottom - pixels_from_bottom);
#if !DIAMOND_FOR_NOTES
                                middle_min.y -= half_note_width;
#endif
                                bool draw_begin = true, draw_middle = true, draw_end = true;
                                // don't draw the thing below the receiver line
                                if (vn->inputs_used > 0) {
                                    draw_begin = false;
                                    f32 delta_y = note_pos_centered.y - receiver_height_centered;
                                    note_pos_centered.y = receiver_height_centered;
                                    middle_size.y += delta_y;
                                    middle_min.y -= delta_y;
                                }
                                // don't draw the thing at all if the end is below the receiver line
                                if (note_end_pos_centered.y >= receiver_height_centered) {
                                    if (draw_middle)
                                        renderer_push_rectangle(&gs->rendering, middle_color, middle_min, middle_size);
                                    if (draw_begin)
                                        renderer_push_diamond(&gs->rendering, color, note_pos_centered, half_note_width);
                                    if (DRAW_BAR_LN_END) {
                                        if (draw_end)
                                            renderer_push_diamond(&gs->rendering, note_end_color, note_end_pos_centered, half_note_width);
                                    }
                                }
                            } else if (onenote == NOTE_MINE) {
                                v4 color = u32_to_color_v4(0xffdd0000);
                                bool draw = note_pos_centered.y >= receiver_height_centered;
                                if (draw)
                                    renderer_push_diamond(&gs->rendering, color, note_pos_centered, mine_half_note_width);
                            } else {
                                continue;
                                // INVALID_CODE_PATH;
                            }
                        }
                    }
                }

                // Receivers
                {
#if 0
                    // good for arrows
                    for (int idx_col = 0; idx_col < diff->notes.columns; idx_col++) {
                        v4 color;
                        if (column_key_down(gs, gin, diff->notes.keymode, idx_col)) {
                            color = gs->playing.other.visual_config->receiver_color;
                        } else {
                            color = gs->playing.other.visual_config->receiver_pressed_color;
                        }
                        v2 note_pos_centered = V2(left_of_playfield+half_note_width + idx_col*note_width_plus_spacing,
                                                  receiver_height_centered);
                        renderer_push_diamond(&gs->rendering, color, note_pos_centered, half_note_width);
                    }
#else
                    // good for bars
                    v2 recv_red_line_size = V2(diff->notes.columns*note_width_plus_spacing+2*half_note_spacing, 5*window_scale);
                    v2 recv_red_line_min = V2(left_of_playfield-2*half_note_spacing, receiver_height_centered - half_note_width - recv_red_line_size.y);
                    renderer_push_rectangle(&gs->rendering, V4(1,0,0,1), recv_red_line_min, recv_red_line_size);

                    for (int idx_col = 0; idx_col < diff->notes.columns; idx_col++) {
                        v4 color;
                        if (column_key_down(gs, gin, diff->notes.keymode, idx_col)) {
                            color = gs->playing.other.visual_config->receiver_pressed_color;
                        } else {
                            color = 0.5f*gs->playing.other.visual_config->receiver_color;
                        }
                        v2 size = 0.5f*half_note_width*V2(1, 1);
                        v2 pos = V2(left_of_playfield+half_note_width + idx_col*note_width_plus_spacing - 0.5f*size.x,
                                    receiver_height_centered - half_note_width - 2*recv_red_line_size.y - 2*size.y);
                        renderer_push_rectangle(&gs->rendering, color, pos, size);
                    }
#endif
                }

                // Draw progress bar
                {
                    v4 color1 = V4(.3f, .3f, .3f, 1);
                    v4 color2 = V4(.5f, .5f, .5f, 1);
                    v2 min = V2(100, 400);
                    v2 size = V2(100, 20);
                    NoteTimestamp duration_ts = diff->notes.timestamps[diff->notes.rows_count-1];
                    ASSERT(duration_ts.value > 0);
                    f32 progress = clamp01((f32)(gs->playing.other.newthings.song_ts.value / duration_ts.value));
                    renderer_push_rectangle(&gs->rendering, color1, min, size);
                    renderer_push_rectangle(&gs->rendering, color2, min, V2(size.x*progress, size.y));
                }

                // Draw TestAcc
                {
                    v2 bar_size = V2(4.0f, 20.0f);
                    v2 base_bar_pos = V2(0.5f*(f32)gs->rendering.window_width - bar_size.x,
                                         0.65f*(f32)gs->rendering.window_height);
                    renderer_push_rectangle(&gs->rendering, V4(0.6f, 0.0f, 0.6f, 1.0f), base_bar_pos, bar_size);
                    // We don't want to draw 16 of them if only 1 exists.
                    s32 max_idx_acc = MINIMUM(ARRAY_COUNT(gs->playing.other.acc), gs->playing.other.acc_total);
                    for (s32 idx_acc = 0 ; idx_acc < max_idx_acc; idx_acc++) {
                        // Draw from the newest to oldest.
                        s32 acc_index = (ARRAY_COUNT(gs->playing.other.acc) + gs->playing.other.acc_index - idx_acc - 1) % ARRAY_COUNT(gs->playing.other.acc);
                        TestAcc acc = gs->playing.other.acc[acc_index];
                        f32 offset_px_per_sec = 1500.0f;
                        f32 offset_seconds = (f32)acc.offset_seconds;
                        f32 abs_offset_seconds = offset_seconds < 0 ? -offset_seconds : offset_seconds;
                        // f32 offset_x = offset_px_per_sec * (abs_offset_seconds + abs_offset_seconds*abs_offset_seconds/2.0f + abs_offset_seconds*abs_offset_seconds*abs_offset_seconds/6.0f) * 2.0f;
                        // f32 offset_x = offset_px_per_sec * (abs_offset_seconds + abs_offset_seconds*abs_offset_seconds/2.0f) * 4.0f;
                        f32 offset_x = window_scale * offset_px_per_sec * abs_offset_seconds;
                        if (offset_seconds < 0.0f) {
                            offset_x = -offset_x;
                        }
                        NoteTimestamp acc_ts = acc.playing_ts;
                        f32 alpha_decay_per_sec = 1.0f/0.5f;
                        f32 alpha = 1.0f - alpha_decay_per_sec*(f32)note_timestamp_diff_to_seconds(gs->playing.other.newthings.song_ts - acc_ts, gs->playing.params.rate_float);
                        if (alpha > 0.0f) {
                            v4 color;
                            if (acc.miss) {
                                color = V4(1.0f, 0.0f, 0.0f, alpha);
                            } else {
                                // TODO: Lerp the color
                                // stepmania J5
                                color = get_color_for_abs_offset(abs_offset_seconds);
                                color.a = alpha;
                            }
                            v2 min = base_bar_pos;
                            min.x += offset_x;
                            renderer_push_rectangle(&gs->rendering, color, min, bar_size);
                        }
                    }
                }


                if (gs->playing.other.paused) {
                    renderer_push_string_centered(memory, &gs->rendering, SCENE_PLAYING_FONT_SIZE,
                                                  0.5f*V2((f32)gs->rendering.window_width, (f32)gs->rendering.window_height),
                                                  text_color, "PAUSED"_s);
                }

            }
        }

        // Draw TestAcc
        {
#define TEST_DO_DRAW_TEST_ACC_GRAPH 1
            // Draw test acc graph
            if (TEST_DO_DRAW_TEST_ACC_GRAPH) {
                // Draw test acc graph
                // This is _very_ slow. On a 5 minute file 60fps at the end of the song.
                // This can probably be much faster if I render to a framebuffer and copy that framebuffer to the screen. (only update that framebuffer when there are changes).
                ASSERT(diff->notes.rows_count > 0);
                NoteTimestamp duration_ts = diff->notes.timestamps[diff->notes.rows_count-1];
                auto get_y_pos_for_offset = [](f32 graph_min_y, f32 graph_max_y, f64 offset) {
                    return lerp(graph_min_y, graph_max_y, (f32)inverse_lerp(-JUDGE_MISS_MIN, +JUDGE_MISS_MIN, offset));
                };
                v2 graph_min = V2(50, 50);
                v2 graph_size = V2(450, 250);
                v2 graph_max = graph_min + graph_size;
                f32 graph_center_y = graph_min.y + graph_size.y/2;
                renderer_push_rectangle(&gs->rendering, V4(0.2f, 0.2f, 0.2f, 1.0f), graph_min, graph_size);
                f32 middle_size = 1;
                f32 linea = 0.4f;
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea), V2(graph_min.x, graph_center_y-middle_size/2), V2(graph_size.x, middle_size));

                f32 perf_pos = get_y_pos_for_offset(graph_min.y, graph_max.y, JUDGE_MARV_MAX) - graph_center_y;
                f32 great_pos = get_y_pos_for_offset(graph_min.y, graph_max.y, JUDGE_PERF_MAX) - graph_center_y;
                f32 good_pos = get_y_pos_for_offset(graph_min.y, graph_max.y, JUDGE_GREAT_MAX) - graph_center_y;
                f32 bad_pos = get_y_pos_for_offset(graph_min.y, graph_max.y, JUDGE_GOOD_MAX) - graph_center_y;
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/2), V2(graph_min.x, (graph_center_y+perf_pos)    -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/2), V2(graph_min.x, (graph_center_y-perf_pos)    -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/3), V2(graph_min.x, (graph_center_y+great_pos)   -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/3), V2(graph_min.x, (graph_center_y-great_pos)   -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/4), V2(graph_min.x, (graph_center_y+good_pos)    -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/4), V2(graph_min.x, (graph_center_y-good_pos)    -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/5), V2(graph_min.x, (graph_center_y+bad_pos)     -middle_size/2), V2(graph_size.x, middle_size));
                renderer_push_rectangle(&gs->rendering, V4(1,1,1,linea/5), V2(graph_min.x, (graph_center_y-bad_pos)     -middle_size/2), V2(graph_size.x, middle_size));

                for (smem idx_col = 0; idx_col < gs->playing.other.columns; idx_col++) {
                    auto *debug_test_accs = &gs->playing.other.per_column[idx_col].debug_test_accs;
                    Forv (*debug_test_accs, idx_acc, acc){
                        // printf("Drawing acc %ld %ld\n", idx_col, idx_acc);
                        v2 point_pos = {};
                        point_pos.x = floorf(lerp(graph_min.x, graph_max.x,
                                                  (f32)inverse_lerp(0.0, duration_ts.value, acc.playing_ts.value)));
                        point_pos.y = floorf(get_y_pos_for_offset(graph_min.y, graph_max.y, acc.offset_seconds));
                        renderer_push_point(&gs->rendering, get_color_for_abs_offset((f32)fabs(acc.offset_seconds)), point_pos, 1);
                    }
                }
            }

            // Draw score (the accuracy)
            {
                Font *font = memory->debug_font0;
                f32 font_size_px = SCORE_FONT_SIZE;
                f32 font_hi = font_size_px*((f32)font->ascent - (f32)font->descent + (f32)font->line_gap);
                // v2 rect_min = ;
                // v2 rect_size = ;
                v2 text_at = V2(200, 600);
                // renderer_push_rectangle(&gs->rendering, V4(0.5f, 0.5f, 0.5f, 0.75f), rect_min, rect_size);
                f64 myacc = 0.0;
                if (gs->playing.other.max_score_right_now != 0.0) {
                    myacc = gs->playing.other.score / gs->playing.other.max_score_right_now;
                }

                {
                    u8 buf[32];
                    String temp_str = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                                     "%6.2f%%", // 7 chars
                                     100.0*myacc);
                    renderer_push_string(memory, &gs->rendering, SCORE_FONT_SIZE, text_at, text_color,
                                         temp_str);
                    text_at.y -= font_hi;
                }
                {
                    u8 buf[32];
                    String temp_str = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                                     "score: %.0f", // 7 chars
                                     gs->playing.other.score);
                    renderer_push_string(memory, &gs->rendering, SCORE_FONT_SIZE, text_at, text_color,
                                         temp_str);
                    text_at.y -= font_hi;
                }
                {
                    u8 buf[32];
                    String temp_str = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &temp_str,
                                     "max score: %.0f", // 7 chars
                                     gs->playing.other.max_score_right_now);
                    renderer_push_string(memory, &gs->rendering, SCORE_FONT_SIZE, text_at, text_color,
                                         temp_str);
                    text_at.y -= font_hi;
                }
            }
        }

        if (gs->playing.other.state == PlayingState_Ended) {
            renderer_push_string_centered(memory, &gs->rendering, SCENE_PLAYING_FONT_SIZE,
                                          0.5f*V2((f32)gs->rendering.window_width, (f32)gs->rendering.window_height),
                                          text_color, "ENDED"_s);
        }

    }
    if (false) {
        String my_str = "The quick brown fox jumps over the lazy dog,.!@?-_+=~!||'\""_s;
        v2 fox_pos = V2(10, 10 - SCENE_PLAYING_FONT_SIZE*memory->debug_font0->descent);
        f32 adv = renderer_push_string(memory, &gs->rendering, SCENE_PLAYING_FONT_SIZE, fox_pos,
                                       V4(1, 0.8f, 0.8f, 0.8f),
                                       my_str);
        renderer_push_rectangle(&gs->rendering, V4(1.0f, 0.0f, 0.0f, 1.0f),
                                fox_pos, V2(adv, 2));
    }
}

bool
debug_open_single_file(MemoryArena *final_arena, MemoryArena *temp_arena, String filename)
{
    bool valid;

    ASSERT(string_exists_and_is_not_empty(filename));
    s32 parser = get_parser_from_filename(filename);
    ParserGetFilesetFromFileFn *fn = parser_get_fileset_from_file_table[parser];
    valid = fn != NULL;
    if (!valid) {
        beat_log("Ignoring file '%.*s'. Unknown extension.\n", EXPAND_STR_FOR_PRINTF(filename));
        return false;
    }

    TempMemory temp_mem = begin_temp_memory(temp_arena);

    Fileset newfileset;
    valid = fn(final_arena, temp_arena, filename, true, &newfileset);
    if (!valid) {
        // printf(" [*FAIL*] %.*s\n", filename.len, filename.data);
        end_temp_memory(&temp_mem);
        return false;
    }

    free_fileset(&newfileset);
    end_temp_memory(&temp_mem);
    return true;
}

void
debug_open_list_of_files(MemoryArena *final_arena, MemoryArena *temp_arena, String filename, char delim)
{
    ASSERT(string_exists_and_is_not_empty(filename));
    FILE *f;
    char filename_cstr[1024];
    ASSERT(filename.len < ssizeof(filename_cstr));
    xmemcpy(filename_cstr, filename.data, filename.len);
    filename_cstr[filename.len] = 0;
    f = fopen(filename_cstr, "rb");
    if (!f) {
        fprintf(stderr, "Error opening file '%.*s'!\n", (int)filename.len, filename.data);
    }
    // ...
    char buf[2048];
    int len = 0;
    memset(buf, 0, ssizeof(buf));
    u64 file_success_count = 0;
    u64 file_fail_count = 0;
    // Maybe instead of fgetc use fread and use a ring buffer?
    u64 start_clock = platform.get_clock();
    bool quit_on_first_fail = false;
    for (;;) {
        int c = fgetc(f);
        if (c == -1) {
            break;
        } else if (c == (int)delim) {
            if (len) {
                buf[len] = 0; // just to be sure
                String str;
                str.data = (u8 *)buf;
                str.len = len;

                if (debug_open_single_file(final_arena, temp_arena, str))
                    file_success_count++;
                else {
                    file_fail_count++;
                    if (quit_on_first_fail)
                        break;
                }
                len = 0;
                memset(buf, 0, ssizeof(buf));
            }
        } else {
            buf[len] = (char)c;
            len++;
            if ((size_t)len >= ssizeof(buf)) {
                ASSERT(!"BUFFER TOO SMALL");
                break;
            }
        }
    }
    u64 end_clock = platform.get_clock();
    fclose(f);
    printf("File success count: %ld    fail: %ld   elapsed: %.2fms\n",
           file_success_count, file_fail_count,
           1000.0*(f64)(end_clock - start_clock)*platform.get_secs_per_clock());
}

static bool
debug_test_ojn(MemoryArena *final_arena, MemoryArena *temp_arena, String filename)
{
    // @Malloc
    ASSERT(string_exists_and_is_not_empty(filename));
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    TempMemory temp_for_final_if_error = begin_temp_memory(final_arena);
    bool valid;
    EntireFile entire_file;
    valid = read_entire_file_into_arena(filename, final_arena, &entire_file);
    if (!valid) {
        end_temp_memory(&temp_for_final_if_error);
        end_temp_memory(&temp_mem);
        return false;
    }

    ParserOjnFile ojn_file;
    valid = parser_ojn_parse_file_from_buffer(final_arena, temp_arena, filename, entire_file.buf, true, &ojn_file);
    if (!valid) {
        printf(" [*FAIL*] %.*s\n", (int)filename.len, filename.data);
        free_entire_file(&entire_file);
        end_temp_memory(&temp_for_final_if_error);
        end_temp_memory(&temp_mem);
        return false;
    }
    Fileset newfileset;
    valid = parser_ojn_to_fileset(final_arena, temp_arena, &ojn_file, &newfileset);
    parser_ojn_free_file(&ojn_file);
    free_entire_file(&entire_file);
    if (!valid) {
        end_temp_memory(&temp_for_final_if_error);
        end_temp_memory(&temp_mem);
        return false;
    }

    for (s32 i = 0; i < newfileset.diffs_count; i++) {
        Diff *diff = &newfileset.diffs[i];
        printf("-\n");
        print_diff(diff);
    }
    free_fileset(&newfileset);

    end_temp_memory(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    return true;
}

static void
debug_directory_iter_test(void)
{
    u64 start_clock = platform.get_clock();
    MemoryArena arena_for_iter;
    init_arena(&arena_for_iter, MEGABYTES(10), NULL, false);
    // String dir_name = "packs\0/standalone//// "_s;
    // String dir_name = "packs"_s;
    String dir_name = "/mnt/c/Games/etterna-files/Songs"_s;
    // String dir_name = "/mnt/c/Games/o2china English/Music"_s;
    // String dir_name = "/mnt/c/LR2Songs/"_s;
    // String dir_name = "/usr/share"_s;

    s32 prev_temp_count = arena_for_iter.temp_count;
    printf("debug_directory_iter_test high water mark (before init): %ld/%ld\n", arena_for_iter.high_water_mark, arena_for_iter.size);
    DirectoryIter dir_iter;
    if (!platform.directory_iter_init(&dir_iter, dir_name, &arena_for_iter, true, 0)) {
        printf("Got error on platform.directory_iter_init for dir '%.*s'\n", EXPAND_STR_FOR_PRINTF(dir_name));
        arena_temp_check(&arena_for_iter, prev_temp_count);
        return;
    }
    printf("debug_directory_iter_test high water mark (after init): %ld\n", arena_for_iter.high_water_mark);
    while (platform.directory_iter_next(&dir_iter)) {
        if (dir_iter.item.file_type == DirectoryIterFileType_File) {
            // printf("FILE: dir_id %4ld total d %4d f %4d this_dir d %4d f %4d %.*s\n", dir_iter.unique_dir_id,
            //        dir_iter.total_count_visited_dirs, dir_iter.total_count_visited_files,
            //        dir_iter.this_dir_count_visited_dirs, dir_iter.this_dir_count_visited_files,
            //        EXPAND_STR_FOR_PRINTF(dir_iter.item.full_filename_nullter));
        } else if (dir_iter.item.file_type == DirectoryIterFileType_Directory) {
            printf("DIR:  dir_id %4ld total d %4d f %4d this_dir d %4d f %4d %.*s\n", dir_iter.unique_dir_id,
                   dir_iter.total_count_visited_dirs, dir_iter.total_count_visited_files,
                   dir_iter.this_dir_count_visited_dirs, dir_iter.this_dir_count_visited_files,
                   EXPAND_STR_FOR_PRINTF(dir_iter.item.full_filename_nullter));
        } else {
            INVALID_CODE_PATH;
        }
        if (dir_iter.item.file_type != DirectoryIterFileType_File) continue;

        ParserType parser_type = get_parser_from_filename(dir_iter.item.base_filename_nullter);
        if (parser_type == ParserType_INVALID) continue;
        // printf("parser %d full_filename '%.*s'\n", parser_type, EXPAND_STR_FOR_PRINTF(dir_iter.item.full_filename_nullter));
    }
    printf("platform.directory_iter_next ended for dir '%.*s'\n", EXPAND_STR_FOR_PRINTF(dir_name));
    platform.directory_iter_end(&dir_iter);
    printf("debug_directory_iter_test high water mark (after end): %ld/%ld\n", arena_for_iter.high_water_mark, arena_for_iter.size);
    arena_temp_check(&arena_for_iter, prev_temp_count);

    u64 end_clock = platform.get_clock();
    f64 secs_per_clock = platform.get_secs_per_clock();
    f64 seconds_elapsed = (f64)(end_clock - start_clock) * secs_per_clock;
    printf("debug_directory_iter_test took %.2fms\n", 1000*seconds_elapsed);
}

static void
get_base_dir_tests()
{
    ASSERT(get_base_dir("/"_s)          == "/"_s);
    ASSERT(get_base_dir("////"_s)       == "////"_s);
    ASSERT(get_base_dir("//aa"_s)       == "//"_s);
    ASSERT(get_base_dir("//aa///b"_s)   == "//aa///"_s);
    ASSERT(get_base_dir("//aa///b//"_s) == "//aa///"_s);
    ASSERT(get_base_dir("//aa//b///"_s) == "//aa//"_s);
    ASSERT(get_base_dir("aa/b/"_s)      == "aa/"_s);
    ASSERT(get_base_dir("aa/b"_s)       == "aa/"_s);
    ASSERT(get_base_dir("aa//"_s)       == ""_s);
    ASSERT(get_base_dir("aa/"_s)        == ""_s);
    ASSERT(get_base_dir("aa"_s)         == ""_s);
    ASSERT(get_base_dir(""_s)           == ""_s);
    ASSERT(get_base_dir(""_s)           == ""_s);

    ASSERT(get_base_dir("//data///"_s)  == "//"_s);
    ASSERT(get_base_dir("//data"_s)     == "//"_s);
    ASSERT(get_base_dir("//"_s)         == "//"_s);
    ASSERT(get_base_dir("data///"_s)    == ""_s);
    ASSERT(get_base_dir("data//"_s)     == ""_s);
    ASSERT(get_base_dir("data/"_s)      == ""_s);
    ASSERT(get_base_dir("data"_s)       == ""_s);
    ASSERT(get_base_dir("gamedir/data///"_s)    == "gamedir/"_s);
    ASSERT(get_base_dir("gamedir//data//"_s)    == "gamedir//"_s);
    ASSERT(get_base_dir("gamedir/data/"_s)      == "gamedir/"_s);
    ASSERT(get_base_dir("gamedir/data"_s)       == "gamedir/"_s);
#if BEAT_OS_WIN32
    ASSERT(get_base_dir("C:\\gamedir\\data\\\\"_s)  == "C:\\gamedir\\"_s);
    ASSERT(get_base_dir("C:\\gamedir\\data\\/"_s)   == "C:\\gamedir\\"_s);
    ASSERT(get_base_dir("C:\\gamedir/data\\/"_s)    == "C:\\gamedir/"_s);
    ASSERT(get_base_dir("C:/gamedir\\data\\/\\"_s)  == "C:/gamedir\\"_s);
    ASSERT(get_base_dir("C:/gamedir\\data"_s)       == "C:/gamedir\\"_s);
    ASSERT(get_base_dir("C:/datadir"_s)             == "C:/"_s);
    ASSERT(get_base_dir("C:\\"_s)                   == "C:\\"_s);
    ASSERT(get_base_dir("C:"_s)                     == "C:"_s);
#endif

    printf("get_base_dir_tests success\n");
    exit(0);
}

// export
GAME_UPDATE_AND_RENDER(game_update_and_render)
{
    ASSERT(memory->permanent.base != NULL);
    ASSERT(memory->temporary.base != NULL);

    GameState *gs = get_game_state(memory);
    if (!memory->is_initialized) {
        memory->is_initialized = true;
        ASSERT(memory->permanent.used == 0);
        GameState *old_gs = gs;
        gs = arena_push_struct(&memory->permanent, GameState, arena_flags_zero());
        ASSERT(gs == old_gs);
        init_game_memory(memory);
    }

#if 0
    // get_base_dir_tests();
    debug_directory_iter_test();
    exit(0);
#endif

    // TODO: what should I do on the first frame? should I set the previous size to zero? We'll see when I actually use this thing.
    gs->rendering.changed_resolution_this_frame = false;
    if (gs->rendering.window_width != window_width) {
        gs->rendering.changed_resolution_this_frame = true;
        gs->rendering.old_window_width = gs->rendering.window_width;
        gs->rendering.window_width = window_width;
    }
    if (gs->rendering.window_height != window_height) {
        gs->rendering.changed_resolution_this_frame = true;
        gs->rendering.old_window_height = gs->rendering.window_height;
        gs->rendering.window_height = window_height;
    }
    ASSERT(gs->rendering.window_width != 0);
    ASSERT(gs->rendering.window_height != 0);
    if (gs->rendering.changed_resolution_this_frame
        && gs->rendering.old_window_width != 0) {
        u8 buf[128];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                         "changed resolution: %4dx%-4d (was %4dx%-4d)",
                         gs->rendering.window_width,
                         gs->rendering.window_height,
                         gs->rendering.old_window_width,
                         gs->rendering.old_window_height);
        add_fading_text(gs, bufstr);
        beat_log("%s\n", buf);
    }


    arena_temp_check(&memory->permanent);
    arena_temp_check(&memory->temporary);
    arena_temp_check(&memory->songs_arena);
    // arena_temp_check(&memory->playing);

    // NOTE: Rendering state needs to persist between frames!
    begin_render_entries(memory, &gs->rendering);
    defer { end_render_entries(&gs->rendering); };

#if 0
    {
        // It is safer to use TempMemory instead of just MemoryArena for some things. because I can check if temp_count is the same.
        // Now it doesn't make any difference since I implemented that temp_check directly in NewArrayDynamic and BucketArray.
        auto temp = begin_temp_memory(&memory->temporary);
        RenderEntry e = {};
        push_render_entry(&gs->rendering, &e); // it checks if gs->rendering.temp.temp_count is == gs->rendering.temp.arena.temp_count and crashes
        end_temp_memory(&temp);
    }
#endif

    if (key_binding_pressed(gin, gs->bindings.toggle_debug_overlay)) {
        gs->debug_overlay_active = !gs->debug_overlay_active;
    }
    if (key_binding_pressed(gin, gs->bindings.toggle_show_keypress)) {
        gs->debug_show_keypress = (gs->debug_show_keypress+1)%3;
        // printf("gs->debug_show_keypress = %s\n", gs->debug_show_keypress ? "true" : "false");
        char *strsss[] = {"nothing", "BeatKey only", "KeyBinding only"};
        u8 buf[128];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                         "gs->debug_show_keypress = %s",
                         strsss[gs->debug_show_keypress]);
        add_fading_text(gs, bufstr);
        beat_log("%s\n", buf);
    }
    if (key_binding_pressed(gin, gs->bindings.toggle_debug_show_text_rect)) {
        gs->debug_show_text_rect = (gs->debug_show_text_rect + 1) & 7;
        char *strsss[] = {"nothing", "BeatKey only", "KeyBinding only"};
        u8 buf[128];
        String bufstr = {};
        xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                         "debug_show_text_rect = %s%s%s",
                         (gs->debug_show_text_rect & 1) ? "over glyph, " : "",
                         (gs->debug_show_text_rect & 2) ? "over everything, " : "",
                         (gs->debug_show_text_rect & 4) ? "on pos" : "");
        add_fading_text(gs, bufstr);
        beat_log("%s\n", buf);
    }

#if BEAT_TIME_MULTIPLIER
    {
        auto last_time_multiplier = memory->debug_time_multiplier;
        if (key_binding_pressed_or_repeat(gin, gs->bindings.debug_virtual_time_faster)
            && memory->debug_time_multiplier < 100) {
            memory->debug_time_multiplier *= 1.125f;
        }
        if (key_binding_pressed_or_repeat(gin, gs->bindings.debug_virtual_time_slower)
            && memory->debug_time_multiplier > 0.01f) {
            memory->debug_time_multiplier /= 1.125f;
        }
        if (memory->debug_time_multiplier != 1 && fabsf(memory->debug_time_multiplier - 1) <= 0.015625f) {
            // printf("asdf\n");
            memory->debug_time_multiplier = 1.0f;
        }
        if (memory->debug_time_multiplier != last_time_multiplier) {
            u8 buf[256];
            String bufstr = {};
            xsnprintf_string(buf, ssizeof(buf), false, &bufstr, "Time Multiplier: %.2f", (f64)memory->debug_time_multiplier);
            add_fading_text(gs, bufstr);
            beat_log("%.*s\n", EXPAND_STR_FOR_PRINTF(bufstr));
        }
    }
#endif

#if 1
    // TODO: Copy this code when I decide to make a screen for setting keybindings.
    if (gs->debug_show_keypress & 1) {
        // Recognize a raw key.
        BeatKey key = check_pressed_beat_key(gin);
        if (key) {
            if (key == BEAT_KEY_ESCAPE) {
                {
                    u8 buf[128];
                    String bufstr = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                     "check_pressed_beat_key ESCAPE, ignoring");
                    add_fading_text(gs, bufstr);
                    beat_log("%s\n", buf);
                }
            } else {
                auto str = beat_key_to_string(key);
                {
                    u8 buf[128];
                    String bufstr = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                     "check_pressed_beat_key valid: '%.*s'",
                                     EXPAND_STR_FOR_PRINTF(str));
                    add_fading_text(gs, bufstr);
                    beat_log("%s\n", buf);
                }
            }
        }
    }
    if (gs->debug_show_keypress & 2) {
        // Recognize a key with mods.
        KeyBinding keybinding = check_pressed_key_binding_with_mods(gin);
        if (keybinding.key) {
            if (keybinding.key == BEAT_KEY_ESCAPE) {
                {
                    u8 buf[128];
                    String bufstr = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                     "check_pressed_key_binding_with_mods ESCAPE, ignoring");
                    add_fading_text(gs, bufstr);
                    beat_log("%s\n", buf);
                }
            } else {
                auto str_mods = keyboard_mods_to_string(keybinding.mods);
                auto str_key = beat_key_to_string(keybinding.key);
                {
                    u8 buf[128];
                    String bufstr = {};
                    xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                     "check_pressed_key_binding_with_mods: '%.*s%.*s'",
                                     EXPAND_STR_FOR_PRINTF(str_mods),
                                     EXPAND_STR_FOR_PRINTF(str_key));
                    add_fading_text(gs, bufstr);
                    beat_log("%s\n", buf);
                }
            }
        }
    }
#endif

    // Maybe make a scene_logic_fn_table and call that
    switch (gs->scene) {
    case SCENE_FIRST: scene_first_logic(memory, gs, gin); break;
    case SCENE_WHEEL: scene_wheel_logic(memory, gs, gin); break;
    case SCENE_PLAYING: scene_playing_logic(memory, gs, gin); break;
    }

    sim_and_render_fading_texts(memory, gs, gin->dt);
    if (gs->debug_overlay_active) {
        draw_debug_overlay(memory, gs, gin);
    }

    start_loading_fonts_asynchronously(memory);
    acquire_work_result_load_font_chunk(memory, gs);

    render(memory);

    static smem last_temp_hwm = 0;
    if (0 && ((0&&FRAME_COUNT_ONCE_IN_A_WHILE) || last_temp_hwm < memory->temporary.high_water_mark)) {
        printf("frame end: arenas high water mark: temp %ld perm %ld songs %ld playing %ld\n",
               memory->temporary.high_water_mark,
               memory->permanent.high_water_mark,
               memory->songs_arena.high_water_mark,
               memory->playing.high_water_mark
              );
    }
    last_temp_hwm = memory->temporary.high_water_mark;
}



// idk where to put this
static void
complete_time_interval(bool sleep_zero, f64 target_interval,
                       u64 perf_counter_start, u64 perf_counter_before_sleep,
                       u64 *perf_counter_end, u64 *total_frame_perf_counter, u64 *delta_perf_counter_sleep,
                       f64 *dt)
{
    auto secs_per_clock = platform.get_secs_per_clock();
    f64 remaining_seconds = target_interval - *dt;
    while (remaining_seconds > 0) {
        if (!sleep_zero) {
            // low cpu usage if in background
            usleep((u32)(1000*1000*remaining_seconds));
        } else {
            // This seems to be about 0.1ms off. This is good enough of a frame limiter.
            usleep(0);
        }

        *perf_counter_end = platform.get_clock();
        *total_frame_perf_counter = *perf_counter_end - perf_counter_start;
        *delta_perf_counter_sleep = *perf_counter_end - perf_counter_before_sleep;
        *dt = (f64)(*total_frame_perf_counter)*secs_per_clock;

        remaining_seconds = target_interval - *dt;
    }
}
