#include "beat_include.hh"

// #include <glad/glad.h>
// vim autocomplete doesn't know about -Iexternal
#include "../external/glad/include/glad/glad.h"

STATIC_ASSERT(sizeof(GLuint) == sizeof(u32));
// STATIC_ASSERT(SIMPLE_FONT_BYTES_PER_PIXEL == 4);
// STATIC_ASSERT(SIMPLE_FONT_BITMAP_ROW_ALIGNMENT == 1);

#include "beat_opengl.cc"

struct RenderCommand {
    enum class Kind {
        Simple,
        Sdf,
    };
    Kind kind;
    union Specific {
        struct Simple {
            struct Vertex {
                v2 pos;
                v2 tex_coords;
                v4 color;
            };
            u32 tex;
            NewArrayDynamic<Vertex> vertexes;
        } simple;
        struct Sdf {
            struct Vertex {
                v2 pos;
                v2 tex_coords;
                v4 color;
            };
            bool do_drop_shadow;
            u32 tex;
            NewArrayDynamic<Vertex> vertexes;
        } sdf;
    } specific;
    smem get_vbo_occupied_size_in_bytes() const {
        switch (kind) {
        case Kind::Simple: return ssizeof(Specific::Simple::Vertex)*specific.simple.vertexes.len; break;
        case Kind::Sdf: return ssizeof(Specific::Sdf::Vertex)*specific.sdf.vertexes.len; break;
        }
    }
};


static void
load_gl_crap_for_font_chunk(FontChunk *chunk)
{
    // I don't know what I'm doing.
    ASSERT(!chunk->did_load_gl_crap);
    if (chunk->atlas_bitmap) {
        glGenTextures(1, &chunk->opengl_atlas_tex);
        glBindTexture(GL_TEXTURE_2D, chunk->opengl_atlas_tex);

        if (SIMPLE_FONT_BITMAP_ROW_ALIGNMENT == 1) {
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        } else {
            // default GL_UNPACK_ALIGNMENT is 4
            ASSERT(SIMPLE_FONT_BITMAP_ROW_ALIGNMENT == 4);
        }
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        ASSERT_SLOW(chunk->atlas_bitmap_pitch % SIMPLE_FONT_BITMAP_ROW_ALIGNMENT == 0);
        // ASSERT(g->width == g->pitch);
        // ASSERT(SIMPLE_FONT_BYTES_PER_PIXEL == 4);
        // ASSERT(SIMPLE_FONT_BITMAP_ROW_ALIGNMENT == 1);
        if (SIMPLE_FONT_BYTES_PER_PIXEL == 1) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R8,
                         chunk->atlas_bitmap_size.x, chunk->atlas_bitmap_size.y,
                         0, GL_RED, GL_UNSIGNED_BYTE, chunk->atlas_bitmap);
        } else {
            ASSERT(false);
            // glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
            //              chunk->atlas_bitmap_size.x, chunk->atlas_bitmap_size.y,
            //              0, GL_RGBA, GL_UNSIGNED_BYTE, chunk->atlas_bitmap);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    chunk->did_load_gl_crap = true;
}

static void
opengl_message_callback(GLenum source,
                        GLenum type,
                        GLuint id,
                        GLenum severity,
                        GLsizei length,
                        const GLchar* message,
                        const void* userParam)
{
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type, severity, message);
}

// export
void
renderer_load_gl_crap(GameMemory *memory)
{
    {
        // During init, enable debug output
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(opengl_message_callback, 0);
    }
    {
        // Generate a white texture
        glGenTextures(1, &memory->opengl.white_tex);
        glBindTexture(GL_TEXTURE_2D, memory->opengl.white_tex);
        defer { glBindTexture(GL_TEXTURE_2D, 0); };

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        u32 buf[white_tex_side_in_px*white_tex_side_in_px] = {0};
        buf[white_tex_pos_px*white_tex_side_in_px + white_tex_pos_px] = 0xffffffff;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, white_tex_side_in_px, white_tex_side_in_px, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);
    }

    // I don't know what I'm doing.
    Font *font = memory->debug_font0;
    for (smem idx_chunk = 0; idx_chunk < font->chunks.len; idx_chunk++) {
        auto *chunk = &font->chunks.data[idx_chunk];
        load_gl_crap_for_font_chunk(chunk);
    }

    // Make a big vbo.
    {
        memory->opengl.big_vbo_size_in_bytes = KILOBYTES(256);
        glGenBuffers(1, &memory->opengl.big_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, memory->opengl.big_vbo);
        defer { glBindBuffer(GL_ARRAY_BUFFER, 0); };
        glBufferData(GL_ARRAY_BUFFER, memory->opengl.big_vbo_size_in_bytes, NULL, GL_DYNAMIC_DRAW);
    }
    {
        u32 simple_program = 0;
        bool ok = create_shader_program(
            R"END(
#version 330
layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 in_tex_coords;
layout (location = 2) in vec4 color;
smooth out vec4 vert_color;
smooth out vec2 tex_coords;
uniform mat4 projection;

void main()
{
    gl_Position = projection * vec4(pos, 0, 1);
    vert_color = color;
    tex_coords = in_tex_coords;
}
)END"_s,

            R"END(
#version 330
smooth in vec4 vert_color;
in vec2 tex_coords;
out vec4 out_color;
uniform sampler2D tex;

void main()
{
    vec4 sample_center = texture(tex, tex_coords);
    out_color = vert_color * sample_center;
}
)END"_s,
        &simple_program);
        if (!ok) {
            ASSERT(!"shader fail");
        }
        memory->opengl.simple.program = simple_program;
        u32 vao;
        glGenVertexArrays(1, &vao);

        glBindBuffer(GL_ARRAY_BUFFER, memory->opengl.big_vbo);
        defer { glBindBuffer(GL_ARRAY_BUFFER, 0); };

        glBindVertexArray(vao);
        defer { glBindVertexArray(0); };

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(RenderCommand::Specific::Simple::Vertex), 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(RenderCommand::Specific::Simple::Vertex), (void*)(2*sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(RenderCommand::Specific::Simple::Vertex), (void*)(4*sizeof(float)));

        memory->opengl.simple.vao = vao;
        // memory->opengl.simple.vbo = vbo;
        memory->opengl.simple.projection_loc    = glGetUniformLocation(simple_program, "projection");
    }
    {
        u32 sdf_program = 0;
        bool ok = create_shader_program(
        R"END(
#version 330
layout (location = 0) in vec4 vertex; // vec2 pos, vec2 tex_coord
layout (location = 1) in vec4 color;
smooth out vec4 vert_color;
out vec2 tex_coords;
uniform mat4 projection;

void main()
{
    gl_Position = projection * vec4(vertex.xy, 0, 1);
    tex_coords = vertex.zw;
    vert_color = color;
}
)END"_s,

        R"END(
#version 330
out vec4 out_color;
in vec2 tex_coords;
smooth in vec4 vert_color;
uniform sampler2D tex;
uniform bool do_drop_shadow;

void main()
{
    // float col = texture(tex, tex_coords).r;
    // vec4 sampled = vec4(col, col, col, col);
    float dist = texture(tex, tex_coords).r;
    float pad = 5/255.0;
    float onedge = 180.0/255.0;
    float pixel_dist_scale = onedge / pad;

    vec4 shadow_color = vec4(0,0,0,1);
    vec4 fill_color = vec4(1,1,1,1);

    float edge_width = length(vec2(dFdx(dist), dFdy(dist)));
    // multiplier was 0.7
    float edge_width_outer = 1.10  * edge_width; // outside: big: blurry, small: sharp
    float edge_width_inner = 0.625 * edge_width; // inside:  big: blurry, small: sharp

    vec4 res = vec4(0,0,0,0);

    float fill_t = smoothstep(onedge - edge_width_outer,
                              onedge + edge_width_inner,
                              dist);

    if (do_drop_shadow) {
        ivec2 shadow_texel_dist = 2*ivec2(-1, 1); // the atlas has to have at least this much padding.
        float shadow_width = 2*edge_width;
        float shadow_width_outer = 1.00 * shadow_width; // outside: big: blurry, small: sharp
        float shadow_width_inner = 0.95 * shadow_width; // inside:  big: blurry, small: sharp
        float dist2 = textureOffset(tex, tex_coords, shadow_texel_dist).r;

        float shadow_t = smoothstep(onedge - shadow_width_outer,
                                    onedge + shadow_width_inner,
                                    dist2);
        res = mix(res, shadow_color, shadow_t);
        res = mix(res, fill_color, fill_t);
    } else {
        res = vec4(1,1,1, fill_t);
    }

    res *= vert_color;
    out_color = res;
}
)END"_s,
    &sdf_program);
        if (!ok) {
            ASSERT(!"shader fail");
        }
        memory->opengl.sdf.program = sdf_program;
        u32 vao;
        glGenVertexArrays(1, &vao);

        glBindBuffer(GL_ARRAY_BUFFER, memory->opengl.big_vbo);
        defer { glBindBuffer(GL_ARRAY_BUFFER, 0); };

        glBindVertexArray(vao);
        defer { glBindVertexArray(0); };

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(RenderCommand::Specific::Sdf::Vertex), 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(RenderCommand::Specific::Sdf::Vertex), (void*)(4*sizeof(float)));

        memory->opengl.sdf.vao = vao;
        // memory->opengl.sdf.vbo = vbo;
        // memory->opengl.sdf.uniform_color_loc = glGetUniformLocation(sdf_program, "uniform_color");
        memory->opengl.sdf.projection_loc     = glGetUniformLocation(sdf_program, "projection");
        memory->opengl.sdf.do_drop_shadow_loc = glGetUniformLocation(sdf_program, "do_drop_shadow");
    }
}


// export
void
render(GameMemory *memory)
{
    auto temp_mem = begin_temp_memory(&memory->temporary);
    defer { end_temp_memory(&temp_mem); };

    // I don't know what I'm doing.
    ASSERT(memory->is_initialized);
    GameState *gs = (GameState *)memory->permanent.base;

    glViewport(0, 0, gs->rendering.window_width, gs->rendering.window_height);
    // glMatrixMode(GL_PROJECTION);
    // glLoadIdentity();
    // glOrtho(0, gs->rendering.window_width, 0, gs->rendering.window_height, -1, 1);
    /* glOrtho makes this matrix (column major)
     * {2/width, 0,          0,  0
     *  0,       2/height,   0,  0
     *  0,       0,          1,  0
     *  -1,      -1,         0,  1}
     *  maps from {[0,width], [0,height]} to {[-1, 1], [-1, 1]}
     *  no z
     */

    v2 const window_size = V2((f32)gs->rendering.window_width, (f32)gs->rendering.window_height);
    AabbPos const window_aabb = {.min = V2(0,0), .max = V2(0,0)+window_size};

    f32 projection[] = {
        2.0f/(f32)gs->rendering.window_width,   0,                                      0, 0,
        0,                                      2.0f/(f32)gs->rendering.window_height,  0, 0,
        0,                                      0,                                      1, 0,
        -1,                                     -1,                                     0, 1,
    };

    // glMatrixMode(GL_TEXTURE);
    // glLoadIdentity();




    glClearColor(0.9f, 0.0f, 0.9f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    {
        {
            glUseProgram(memory->opengl.simple.program);
            glUniformMatrix4fv(memory->opengl.simple.projection_loc, 1, GL_FALSE, projection);
            glUseProgram(0);
        }
        {
            glUseProgram(memory->opengl.sdf.program);
            glUniformMatrix4fv(memory->opengl.sdf.projection_loc, 1, GL_FALSE, projection);
            glUseProgram(0);
        }
    }

    glEnable(GL_TEXTURE_2D);

    // TODO: Dynamic arenas.
    static MemoryArena other_temp_arena = {};
    if (other_temp_arena.base == NULL) {
        // Initialize this arena here. In the future when I implement dynamic arenas this would be `memory->temporary`.
        init_arena(&other_temp_arena, MEGABYTES(10), NULL, false);
        // other_temp_arena will never be freed; that's ok.
    }
    auto other_temp = begin_temp_memory(&other_temp_arena);
    defer { end_temp_memory(&other_temp); };

    static smem max_other_temp_used = 0;
    defer {
        if (other_temp_arena.used > max_other_temp_used) {
            max_other_temp_used = other_temp_arena.used;
            if (!true && max_other_temp_used > MEGABYTES(1)) {
                u8 buf[128];
                String bufstr = {};
                xsnprintf_string(buf, ssizeof(buf), false, &bufstr,
                                 "New maximum other_temp_arena.used: %ld",
                                 max_other_temp_used);
                add_fading_text(gs, bufstr);
                printf("%.*s\n", (int)bufstr.len, buf);
            }
        }
    };

    // TODO: Create a `SegmentedDynamicArray` (a list of arrays where
    // `SegmentedDynamicArray.get_nth_array(i).capacity` increases exponentially with `i`) (similar
    // to zig's `std.SegmentedList`). The benefit over a regular `NewArrayDynamic` is that it never
    // reallocates existing memory therefore it is good to use with a `MemoryArena`.
    auto cmd_buffer = NewArrayDynamic<RenderCommand>::init_capacity(&memory->temporary, 4096);

    GLOBAL_STATS.cmd_buffer_total_num_vertices_simple = 0;
    GLOBAL_STATS.cmd_buffer_total_num_vertices_sdf = 0;
    auto const do_common_push_command_simple = [](
        MemoryArena *arena,
        NewArrayDynamic<RenderCommand> *cmd_buffer,
        RenderCommand::Specific::Simple::Vertex *verts_ptr,
        smem verts_len,
        u32 tex) -> void
    {
        // There is a fixed limit on how much stuff I can draw. TODO: When I implement dynamic arenas I can remove this limit.
        // With `arena->size/2` I won't ever crash because of OOM.
        if (arena->used > arena->size/2) return;
        using Vertex = RenderCommand::Specific::Simple::Vertex;
        auto const this_kind = RenderCommand::Kind::Simple;
        auto const big_vbo_size_in_bytes = GAME_MEMORY->opengl.big_vbo_size_in_bytes;
        auto const this_will_occupy_bytes_in_big_vbo = ssizeof(RenderCommand::Specific::Simple)*verts_len;
        RenderCommand *const last_cmd = cmd_buffer->len > 0 ? &cmd_buffer->data[cmd_buffer->len-1] : NULL;
        RenderCommand *final_cmd = NULL;
        if (last_cmd &&
            last_cmd->kind == this_kind &&
            last_cmd->specific.simple.tex == tex &&
            last_cmd->get_vbo_occupied_size_in_bytes() + this_will_occupy_bytes_in_big_vbo <= big_vbo_size_in_bytes)
        {
            // Append to the current command.
            final_cmd = last_cmd;
        } else {
            // Make a new commmand
            final_cmd = cmd_buffer->push(RenderCommand{
                .kind = this_kind,
                .specific = {
                    .simple = {
                        .tex = tex,
                        .vertexes = NewArrayDynamic<Vertex>::init_capacity(arena, verts_len),
                    },
                },
            });
        }
        ASSERT(final_cmd && final_cmd->kind == this_kind && final_cmd->specific.simple.tex == tex);
        final_cmd->specific.simple.vertexes.append({.len = verts_len, .data = verts_ptr});
        GLOBAL_STATS.cmd_buffer_total_num_vertices_simple += verts_len;
        ASSERT(final_cmd->get_vbo_occupied_size_in_bytes() <= big_vbo_size_in_bytes);
    };

    auto const do_common_push_command_sdf = [](
        MemoryArena *arena,
        NewArrayDynamic<RenderCommand> *cmd_buffer,
        RenderCommand::Specific::Sdf::Vertex *verts_ptr,
        smem verts_len,
        u32 tex,
        bool do_drop_shadow) -> void
    {
        // There is a fixed limit on how much stuff I can draw. TODO: When I implement dynamic arenas I can remove this limit.
        // With `arena->size/2` I won't ever crash because of OOM.
        if (arena->used > arena->size/2) return;
        using Vertex = RenderCommand::Specific::Sdf::Vertex;
        auto const this_kind = RenderCommand::Kind::Sdf;
        auto const big_vbo_size_in_bytes = GAME_MEMORY->opengl.big_vbo_size_in_bytes;
        auto const this_will_occupy_bytes_in_big_vbo = ssizeof(RenderCommand::Specific::Sdf)*verts_len;
        RenderCommand *const last_cmd = cmd_buffer->len > 0 ? &cmd_buffer->data[cmd_buffer->len-1] : NULL;
        RenderCommand *final_cmd = NULL;
        if (last_cmd &&
            last_cmd->kind == this_kind &&
            last_cmd->specific.sdf.tex == tex &&
            last_cmd->specific.sdf.do_drop_shadow == do_drop_shadow &&
            last_cmd->get_vbo_occupied_size_in_bytes() + this_will_occupy_bytes_in_big_vbo <= big_vbo_size_in_bytes)
        {
            // Append to the current command.
            final_cmd = last_cmd;
        } else {
            // Make a new commmand
            final_cmd = cmd_buffer->push(RenderCommand{
                .kind = this_kind,
                .specific = {
                    .sdf = {
                        .do_drop_shadow = do_drop_shadow,
                        .tex = tex,
                        .vertexes = NewArrayDynamic<Vertex>::init_capacity(arena, verts_len),
                    },
                },
            });
        }
        ASSERT(final_cmd && final_cmd->kind == this_kind && final_cmd->specific.sdf.tex == tex);
        final_cmd->specific.sdf.vertexes.append({.len = verts_len, .data = verts_ptr});
        ASSERT(final_cmd->get_vbo_occupied_size_in_bytes() <= big_vbo_size_in_bytes);
        GLOBAL_STATS.cmd_buffer_total_num_vertices_sdf += verts_len;
    };


    auto const *const entries = &gs->rendering.entries;
    for (smem idx_entry = 0;
         idx_entry < entries->len;
         idx_entry++) {
        RenderEntry *re = &entries->data[idx_entry];
        switch (re->type) {
        case RenderEntryType_COUNT: INVALID_CODE_PATH; break;
        case RenderEntryType_Ignore: {
            // do nothing
        } break;
        case RenderEntryType_SimpleQuad: {
            auto const &quad = re->simple_quad;
            if (!quad_intersects_aabb(quad.pos, window_aabb))
                continue;
            using Vertex = RenderCommand::Specific::Simple::Vertex;
            Vertex verts[6] = {
                {quad.pos.pos[0],  quad.uv.pos[0],  quad.color},
                {quad.pos.pos[1],  quad.uv.pos[1],  quad.color},
                {quad.pos.pos[2],  quad.uv.pos[2],  quad.color},

                {quad.pos.pos[2],  quad.uv.pos[2],  quad.color},
                {quad.pos.pos[3],  quad.uv.pos[3],  quad.color},
                {quad.pos.pos[0],  quad.uv.pos[0],  quad.color},
            };

            // Push to the command buffer
            do_common_push_command_simple(&other_temp_arena, &cmd_buffer, verts, ARRAY_COUNT(verts), quad.tex);
        } break;

        case RenderEntryType_SdfQuad: {
            auto const &quad = re->sdf_quad;
            if (!quad_intersects_aabb(quad.pos, window_aabb))
                continue;
            using Vertex = RenderCommand::Specific::Sdf::Vertex;
            Vertex verts[6] = {
                {quad.pos.pos[0],  quad.uv.pos[0],  quad.color},
                {quad.pos.pos[1],  quad.uv.pos[1],  quad.color},
                {quad.pos.pos[2],  quad.uv.pos[2],  quad.color},

                {quad.pos.pos[2],  quad.uv.pos[2],  quad.color},
                {quad.pos.pos[3],  quad.uv.pos[3],  quad.color},
                {quad.pos.pos[0],  quad.uv.pos[0],  quad.color},
            };

            // Push to the command buffer
            do_common_push_command_sdf(&other_temp_arena, &cmd_buffer, verts, ARRAY_COUNT(verts), quad.tex, quad.do_drop_shadow);
        } break;
        }
    }

    // Draw the commands.
    {
        auto const big_vbo = memory->opengl.big_vbo;
        auto const big_vbo_size_in_bytes = memory->opengl.big_vbo_size_in_bytes;

        Forp (cmd_buffer, _, cmd) {
            switch (cmd->kind) {
            case RenderCommand::Kind::Simple: {
                auto const *const simple = &cmd->specific.simple;
                ASSERT(simple->vertexes.len > 0);
                glUseProgram(GAME_MEMORY->opengl.simple.program);
                defer { glUseProgram(0); };

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, simple->tex);
                defer { glBindTexture(GL_TEXTURE_2D, 0); };

                glBindVertexArray(GAME_MEMORY->opengl.simple.vao);
                defer { glBindVertexArray(0); };

                auto const this_cmd_size_in_bytes = simple->vertexes.len*ssizeof(RenderCommand::Specific::Simple::Vertex);
                ASSERT(big_vbo_size_in_bytes >= this_cmd_size_in_bytes);

                glBindBuffer(GL_ARRAY_BUFFER, big_vbo);
                defer { glBindBuffer(GL_ARRAY_BUFFER, 0); };
                glBufferSubData(GL_ARRAY_BUFFER, 0, this_cmd_size_in_bytes, simple->vertexes.data);
                glDrawArrays(GL_TRIANGLES, 0, (int)simple->vertexes.len);

                // u32 hash = fnv1a((u8 const*)simple->vertexes.data, simple->vertexes.len*SSIZEOF_VAL(simple->vertexes.data));
                //
                // printf("verts 2 hash %08x, tex %u, vao: %u\n", hash, simple->tex, GAME_MEMORY->opengl.simple->vao);
            } break;
            case RenderCommand::Kind::Sdf: {
                auto const *const sdf = &cmd->specific.sdf;
                ASSERT(sdf->vertexes.len > 0);
                glUseProgram(GAME_MEMORY->opengl.sdf.program);
                defer { glUseProgram(0); };

                glUniform1i(memory->opengl.sdf.do_drop_shadow_loc, sdf->do_drop_shadow ? 1 : 0);

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, sdf->tex);
                defer { glBindTexture(GL_TEXTURE_2D, 0); };

                glBindVertexArray(GAME_MEMORY->opengl.sdf.vao);
                defer { glBindVertexArray(0); };

                auto const this_cmd_size_in_bytes = sdf->vertexes.len*ssizeof(RenderCommand::Specific::Sdf::Vertex);
                ASSERT(big_vbo_size_in_bytes >= this_cmd_size_in_bytes);

                glBindBuffer(GL_ARRAY_BUFFER, big_vbo);
                defer { glBindBuffer(GL_ARRAY_BUFFER, 0); };
                glBufferSubData(GL_ARRAY_BUFFER, 0, this_cmd_size_in_bytes, sdf->vertexes.data);
                glDrawArrays(GL_TRIANGLES, 0, (int)sdf->vertexes.len);
                // DEBUG_BREAK;
            } break;
            }
        }
        GLOBAL_STATS.cmd_buffer_len = cmd_buffer.len;
    }


    auto gl_error = glGetError();
    if (gl_error != GL_NO_ERROR) {
        printf("GOT GL ERROR: %d\n", gl_error);
        DEBUG_BREAK;
    }

}
