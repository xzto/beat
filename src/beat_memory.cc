static void
init_arena(MemoryArena *arena, smem size, void *base, bool zero)
{
    ASSERT(arena != NULL);
    ASSERT(size > 0);
    if (base == NULL) {
        // TODO: Get rid of this when I implement dynamic arenas. This is just a hack.
        base = xmalloc(size);
    }
    zero_struct(arena);
    arena->base = (u8 *)base;
    arena->size = size;
    if (zero) {
        zero_size(base, size);
    }
}

static inline void
arena_temp_check(MemoryArena *arena, s32 check)
{
    ASSERT(arena->temp_count == check);
}

static inline void
arena_clear_id_check(MemoryArena *arena, u64 check)
{
    ASSERT(arena->clear_id == check);
}

static void
arena_clear(MemoryArena *arena)
{
    ASSERT(arena != NULL);
    ASSERT(arena->base != NULL);
    arena_temp_check(arena);
    arena->used = 0;
    // leave high_water_mark as it is. Should I set high water mark to zero?
    arena->num_pushs = 0;
    // temp_count is already zero
    arena->last_pushed_ptr = 0;
    arena->clear_id++;

    // auto high_water_mark = arena->high_water_mark;
    // auto last_clear_id = arena->clear_id;
    // init_arena(arena, arena->size, arena->base, false);
    // arena->high_water_mark = high_water_mark;
    // arena->clear_id = last_clear_id+1;
}

static inline smem
get_arena_alignment_offset(MemoryArena *arena, smem alignment)
{
    smem result = 0;
    smem new_ptr = (smem)arena->base + arena->used;
    smem not_aligned = new_ptr & (alignment - 1);
    if (not_aligned)
        result = alignment - not_aligned;

    return result;
}


// TODO: Also an argument so that arena_push_struct(type) can pass alignof(type).
static void *
arena_push_size_(MemoryArena *arena, smem size, smem align_of_for_structs, MemoryArenaFlags flags)
{
    // TODO: Maybe I need to accept a size of zero
    ASSERT_SLOW(size >= 0);
    ASSERT_SLOW(arena != NULL);
    ASSERT_SLOW(arena->base != NULL);
    ASSERT(arena->used >= 0);
    ASSERT(arena->size >= arena->used);
    void *result = NULL;

    smem alignment = flags.alignment > 0 ? flags.alignment : (align_of_for_structs > 0 ? align_of_for_structs : 8);
    ASSERT(alignment > 0);
    smem alignment_offset = get_arena_alignment_offset(arena, alignment);
    smem alloc_size = size + alignment_offset;

    bool do_zero = flags.zero;

    smem next_used = arena->used + alloc_size;
    if (next_used <= arena->size) {
        result = arena->base + arena->used + alignment_offset;
        if (do_zero)
            zero_size(result, size);
        arena->used = next_used;
        arena->num_pushs++;
    } else {
        ASSERT(!"Arena is out of memory!");
    }
    arena->last_pushed_ptr = result;
    ASSERT(result != NULL);
    if (arena->used > arena->high_water_mark) arena->high_water_mark = arena->used;
    // beat_printf_err("Arena push size %d, used %d max %d\n", (int)size, (int)arena->used, (int)arena->size);

    ASSERT_SLOW(is_aligned_to((umem)result, (umem)alignment));
    return result;
}

// This functions tries to act like realloc. It does not push it again if
// prev_ptr was the last thing pushed into the arena.
// TODO: ArenaFlags.
// TODO: Make a macro to realloc an array of a type.
static void *
arena_realloc(MemoryArena *arena, void *prev_ptr, smem size_in_bytes, smem alignment)
{
    // This doesn't check for the size of the thing pushed.
    ASSERT(size_in_bytes >= 0);
    ASSERT(arena);
    void *result = 0;
    if (!prev_ptr) {
        result = arena_push_size_(arena, size_in_bytes, alignment);
        ASSERT(result);
    } else {
        ASSERT(arena->base);
        // Assert the pointer is inside the arena. Won't work for dynamic arenas.
        ASSERT(prev_ptr >= arena->base && prev_ptr < arena->base + arena->size);

        if (prev_ptr == arena->last_pushed_ptr) {
            smem last_used = arena->used;
            smem new_used = ((u8 *)prev_ptr - arena->base) + size_in_bytes;
            ASSERT(new_used <= arena->size);
            // clear to zero
            smem diff_used = new_used - last_used;
            if (diff_used > 0) {
                zero_size((u8 *)prev_ptr + diff_used, diff_used);
            }
            arena->used = new_used;
            if (arena->used > arena->high_water_mark) arena->high_water_mark = arena->used;
            result = prev_ptr;
        } else {
            if (size_in_bytes > 0) {
                result = arena_push_size_(arena, size_in_bytes, alignment); // TODO ArenaFlags
                smem max_possible_last_size = arena->used - ((u8*)prev_ptr - arena->base);
                // we can ASSERT(max_possible_last_size >= actual_last_size)
                // although this is not correct, it's not wrong either.
                // it will not overflow the arena buffer.
                smem size_to_copy = MINIMUM(max_possible_last_size, size_in_bytes);
                ASSERT((u8*)prev_ptr+size_to_copy <= (u8*)arena->base + arena->used);
                xmemcpy(result, prev_ptr, size_to_copy);
            } else {
                return 0;
            }
        }
    }
    // print_arena(*arena);
    ASSERT(result);
    return result;
}

static void
arena_free(MemoryArena *arena, void *ptr)
{
    if (arena->last_pushed_ptr == ptr) {
        arena_realloc(arena, ptr, 0);
    }
}

// TODO: Don't init to zero the whole subarena.
static MemoryArena
sub_arena(MemoryArena *primary, smem size)
{
    ASSERT(primary->size >= primary->used);
    smem remaining_size = primary->size - primary->used;
    ASSERT(size < remaining_size);

    MemoryArena result = {};

    // actually, it is zeroing it
    void *base = arena_push_size(primary, size, arena_flags_nozero());
    ASSERT(base != NULL);
    result.base = (u8 *)base;
    result.size = size;
    return result;
}


static TempMemory
begin_temp_memory(MemoryArena *arena)
{
    TempMemory result = {};
    result.arena = arena;
    result.used = arena->used;
    result.num_pushs = arena->num_pushs;
    arena->temp_count++;
    result.temp_count = arena->temp_count;
    return result;
}

static void
end_temp_memory(TempMemory *temp_)
{
    TempMemory const temp = *temp_;
    zero_struct(temp_);

    ASSERT(temp.arena != NULL);

    ASSERT(temp.arena->temp_count > 0);
    ASSERT(temp.arena->temp_count == temp.temp_count);

    temp.arena->used = temp.used;
    temp.arena->num_pushs = temp.num_pushs;
    temp.arena->temp_count--;
}

// TODO: Rename TempMemory to ArenaState and begin and end to push_state and pop_state, pop_state_as_if_nothing_ever_happend
static void
end_temp_memory_dont_free(TempMemory *temp_)
{
    TempMemory const temp = *temp_;
    zero_struct(temp_);

    ASSERT(temp.arena != NULL);

    ASSERT(temp.arena->temp_count > 0);
    ASSERT(temp.arena->temp_count == temp.temp_count);

    temp.arena->temp_count--;
}


// If string is empty, push one byte (set to zero).
// result is not null terminated
// maybe I do always want a null terminated "String"? if i don't want text
// maybe i should typedef String Buffer;??
static String
arena_push_string(MemoryArena *arena, String str)
{
    ASSERT(str.data != NULL);
    String result = {};
    if (str.len > 0 && str.data != NULL) {
        result.data = (u8 *)arena_push_size(arena, str.len, arena_flags_nozero());
        result.len = str.len;
        xmemcpy(result.data, str.data, str.len);
    } else {
        result.data = (u8 *)arena_push_size(arena, 1, arena_flags_nozero());
        result.len = 0;
        result.data[0] = 0;
    }
    return result;
}

// same as arena_push_size except that we push one extra byte for the zero
static String
arena_push_string_nullter(MemoryArena *arena, String str)
{
    ASSERT(str.data != NULL);
    String result = {};
    if (str.len > 0 && str.data != NULL) {
        result.data = (u8 *)arena_push_size(arena, str.len + 1, arena_flags_nozero());
        result.len = str.len;
        xmemcpy(result.data, str.data, str.len);
        result.data[result.len] = 0;
    } else {
        result.data = (u8 *)arena_push_size(arena, 1, arena_flags_nozero());
        result.len = 0;
        result.data[0] = 0;
    }
    return result;
}


#define COUNT_ARGS_COMMA_ARGS(args) ARRAY_COUNT(args), (args)
#define arena_push_string_concat(arena, ...) \
    arena_push_string_concat_(arena, COUNT_ARGS_COMMA_ARGS(((String []){__VA_ARGS__})))

static String
arena_push_string_concat_(MemoryArena *arena, s32 count, String *strings)
{
    String result;
    smem total_len = 0;
    for (s32 i = 0; i < count; i++) {
        total_len += strings[i].len;
    }
    result.data = (u8 *)arena_push_size(arena, total_len + 1, arena_flags_nozero());
    result.len = total_len;
    u8 *buf = result.data;
    for (s32 i = 0; i < count; i++) {
        xmemcpy(buf, strings[i].data, strings[i].len);
        buf += strings[i].len;
    }
    *buf = 0;
    ASSERT(buf == result.data + result.len);
    return result;
}

