#include "beat_include.hh"

static bool
create_shader_program(String vert_text, String frag_text, u32 *ret_program)
{
    *ret_program = 0;
    auto create_shader = [] (String text, u32 type, u32 *ret_shader) -> bool {
        ASSERT(type == GL_VERTEX_SHADER || type == GL_FRAGMENT_SHADER);
        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, (char**)&text.data, (GLint*)&text.len);
        glCompileShader(shader);
        GLint status;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
        if (!status) {
            char buffer[512];
            glGetShaderInfoLog(shader, sizeof(buffer), NULL, buffer);
            beat_log("Shader failed to compile: %s\n", buffer);
            ASSERT(false);
            return false;
        }
        *ret_shader = shader;
        return true;
    };

    u32 vert_shader = 0, frag_shader = 0;
    if (!create_shader(vert_text, GL_VERTEX_SHADER, &vert_shader)) {
        beat_log("Vertex shader failed to compile.\n");
        ASSERT(false);
        return false;
    }
    if (!create_shader(frag_text, GL_FRAGMENT_SHADER, &frag_shader)) {
        beat_log("Fragment shader failed to compile.\n");
        ASSERT(false);
        return false;
    }

    u32 program = glCreateProgram();
    glAttachShader(program, vert_shader);
    glAttachShader(program, frag_shader);
    glLinkProgram(program);

    int status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if(!status) {
        char buffer[512];
        glGetProgramInfoLog(program, sizeof(buffer), NULL, buffer);
        beat_log("Shader failed to link: %s\n", buffer);
        ASSERT(false);
        return false;
    }

    *ret_program = program;
    return true;
}
