#ifndef BEAT_PARSER_OJN_H
#define BEAT_PARSER_OJN_H

#define parser_ojn_report_error_v(c, format, ...) do {      \
    if ((c)->report_errors) {                               \
        beat_log("[parser_ojn] %.*s: error: " format,       \
                (int)(c)->filename.len, (c)->filename.data, \
                __VA_ARGS__);                               \
    }                                                       \
} while (0);

#define parser_ojn_report_error_s(c, format) do {            \
    if ((c)->report_errors) {                                \
        beat_log("[parser_ojn] %.*s: error: " format,        \
                (int)(c)->filename.len, (c)->filename.data); \
    }                                                        \
} while (0);

// Don't change it!
#define PARSER_OJN_NUM_DIFFS 3

struct ParserOjnFile;
typedef struct {
    String filename;
    String buf;
    u8 *buf_ptr;
    u8 *buf_end;
    bool report_errors;
} ParserOjnCtx;

// https://fileformats.fandom.com/wiki/O2Jam_note_files
// https://open2jam.wordpress.com/the-ojn-documentation/
// Saved open2jam.wordpress.com files in docs/

// "ojn\0"
#define PARSER_OJN_MAGIC_OJN 0x006e6a6f

// ASSERT(no padding in this struct);
// ASSERT(is little_endian)
typedef struct {
    s32 song_id;
    u32 magic_ojn; // 4f, 4a, 4e, 00
    union {
        f32 encoder_version; // 9a, 99, 39, 40
        u32 encoder_version_as_u32;
    };
    s32 genre;
    f32 bpm;

    s16 diff_level[PARSER_OJN_NUM_DIFFS];
    s16 _padding_123;

    s32 event_count[PARSER_OJN_NUM_DIFFS]; // easy, normal and hard
    s32 note_count[PARSER_OJN_NUM_DIFFS]; // easy, normal and hard
    s32 measure_count[PARSER_OJN_NUM_DIFFS]; // easy, normal and hard
    s32 block_count[PARSER_OJN_NUM_DIFFS]; // easy, normal and hard

    u16 old_encode_version; // unused // always 1d, 00
    u16 old_songid; // unused
    char old_genre[20]; // unused
    s32 bmp_size;
    s32 old_file_version;

    // these strings are null terminated
    char title[64];
    char artist[32];
    char notecharter[32];
    char name_of_the_ojm_file[32];

    s32 cover_size;

    s32 duration_seconds[PARSER_OJN_NUM_DIFFS]; // easy, normal and hard

    s32 note_section_offset[PARSER_OJN_NUM_DIFFS]; // easy, normal and hard
    s32 image_section_offset;

    u8 _terminal_offset[0];
} ParserOjnFileHeader;
// Make sure there's no unwanted padding
// STATIC_ASSERT(STRUCT_OFFSET(ParserOjnFileHeader, _terminal_offset) == 300);
STATIC_ASSERT(sizeof(ParserOjnFileHeader) == 300);

// this is shared with parser_bms because it's exactly the same thing and i didn't wan to copy and paste it.
struct ParserOjn_Bpm {
    // Bpm can't be zero or negative
    f32 bps;
    // f64 active_beats, active_seconds;
    s32 measure;
    f64 measure_position; // from 0 to 1
    f64 start_seconds;
};

// Maybe put more things later
struct ParserOjnFile {
    String filename;
    // buf.data and header are the same pointer
    String buf;
    ParserOjnFileHeader *header;

    ParserOjnCtx ctx;

    ///////
    NewArrayDynamic<ParserOjn_Bpm> bpms[PARSER_OJN_NUM_DIFFS];
};

// Each integer corresponds to a genre:
// 0 - Ballad
// 1 - Rock
// 2 - Dance
// 3 - Techno
// 4 - Hip-hop
// 5 - Soul/R&B
// 6 - Jazz
// 7 - Funk
// 8 - Classical
// 9 - Traditional
// 10 - Etc.

typedef struct {
    s32 measure;
    s16 channel;
    s16 event_count;
} ParserOjnBlockHeader;

enum {
    PARSER_OJN_NOTE_EVENT_MASK_VOLUME = 0xf,
    PARSER_OJN_NOTE_EVENT_MASK_PAN = 0xf0,
};

// If a block has 16 events on channel NOTE_3, each of them will represent a
// note at 1/16th of the measure

enum {
    PARSER_OJN_NOTE_TYPE_NORMAL   = 0,
    PARSER_OJN_NOTE_TYPE_LN_START = 2,
    PARSER_OJN_NOTE_TYPE_LN_END   = 3,
    PARSER_OJN_NOTE_TYPE_OGG_SAMPLE = 4, // ??
};

typedef struct {
    union {
        struct {
            // reference to the sampleon the ojm file
            s16 value; // when 0, it's ignored
            u8 volume_and_pan; // PARSER_OJN_NOTE_EVENT_MASK_PAN and volume
            // see PARSER_OJN_NOTE_TYPE_xxx
            u8 note_type;
        } note;
        struct {
            f32 bpm;
        } bpm_change;
        struct {
            f32 value;
        } time_signature;
    };
} ParserOjnEvent;


// Channel assignment
enum {
    PARSER_OJN_CHANNEL_TIME_SIGNATURE = 0,
    PARSER_OJN_CHANNEL_BPM_CHANGE = 1,

    PARSER_OJN_CHANNEL_NOTE_1 = 2,
    PARSER_OJN_CHANNEL_NOTE_2,
    PARSER_OJN_CHANNEL_NOTE_3,
    PARSER_OJN_CHANNEL_NOTE_4,
    PARSER_OJN_CHANNEL_NOTE_5,
    PARSER_OJN_CHANNEL_NOTE_6,
    PARSER_OJN_CHANNEL_NOTE_7 = 8,

    PARSER_OJN_CHANNEL_AUTO_PLAY_SAMPLES_START = 9,
    PARSER_OJN_CHANNEL_AUTO_PLAY_SAMPLES_END = 22,
};

#endif
