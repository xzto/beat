// TODO: Don't include sdl directly here!

// For now I'll if0 this because I don't need it for the game code.
#if 0
static void
semaphore_init(Semaphore *sem, u32 initial_value)
{
    *sem = {};
    auto *sdl_sem = SDL_CreateSemaphore(initial_value);
    ASSERT(sdl_sem);
    sem->sdl_sem = sdl_sem;
}

static void
semaphore_destroy(Semaphore *sem)
{
    ASSERT(sem->sdl_sem);
    SDL_DestroySemaphore(sem->sdl_sem);
    *sem = {};
}

static void
semaphore_post1(Semaphore *sem)
{
    int ret = SDL_SemPost(sem->sdl_sem);
    ASSERT(ret == 0);
}

static void
semaphore_wait(Semaphore *sem)
{
    int ret = SDL_SemWait(sem->sdl_sem);
}

// TODO: semaphore_try_wait
#endif


static inline void
ticket_mutex_lock(TicketMutex *m)
{
    auto my_ticket = atomic_increment(&m->ticket);
    while (my_ticket != atomic_load(&m->served)) {
        cpu_relax();
    }
}

// returns whether we got the lock or not
static inline bool
ticket_mutex_try_lock(TicketMutex *m)
{
    auto my_ticket = atomic_load(&m->ticket);
    if (my_ticket == atomic_load(&m->served)) {
        return atomic_cas(&m->ticket, my_ticket, my_ticket + 1);
    }
    return false;
}

static inline void
ticket_mutex_unlock(TicketMutex *m)
{
    atomic_increment(&m->served);
}



#if 0
static inline void
shared_mutex_init(SharedMutex *m)
{
    m->combined.store_nonatomic(m->COMBINED_VALUE_FOR_UNIQUE_FREE); // just a zero
}

static inline void
shared_mutex_unique_lock(SharedMutex *m)
{
    // if (shared == 0 && unique == 0)
    //     unique = 1;
    // else
    //     continue;
    // break;
    while (true) {
        if (atomic_cas(&m->combined, m->COMBINED_VALUE_FOR_UNIQUE_FREE, m->COMBINED_VALUE_FOR_UNIQUE_HELD))
            break;
        cpu_relax();
    }
}

static inline void
shared_mutex_unique_unlock(SharedMutex *m)
{
    // unique = 0;
#if BEAT_SLOW
    ASSERT_SLOW(atomic_load(&m->combined) == m->COMBINED_VALUE_FOR_UNIQUE_HELD);
#endif
    atomic_store(&m->combined, (u32)0);
}

static inline void
shared_mutex_shared_lock(SharedMutex *m)
{
    while (true) {
        auto old_combined = atomic_load(&m->combined);
        STATIC_ASSERT(ssizeof(m->shared) == 2 && STRUCT_OFFSET(SharedMutex, shared) == 0);
        u16 old_unique = (u16)((old_combined>>16) & U16_MAX);
        if (old_unique == 0) {
            u16 old_shared = old_combined & U16_MAX;
            u16 new_shared = old_shared + 1;
            ASSERT_SLOW(new_shared > old_shared); // This doesn't actually need to be a failure. I could just try again... ANyway, 65000 simultaneous locks will never happen for me.
            u32 new_combined = (old_combined & ~(u32)U16_MAX) | new_shared;
            if (atomic_cas(&m->combined, old_combined, new_combined))
                break;
        }
        cpu_relax();
    }
    // if (unique == 0)
    //     shared += 1;
#if BEAT_SLOW
    ASSERT_SLOW(atomic_load(&m->unique) == 0);
#endif
}

static inline void
shared_mutex_shared_unlock(SharedMutex *m)
{
#if BEAT_SLOW
    STATIC_ASSERT(ssizeof(m->shared) == 2 && STRUCT_OFFSET(SharedMutex, shared) == 0);
    auto old_combined = atomic_load(&m->combined);
    // u16 old_shared = (u16)(old_combined       & 0xffff);
    // u16 old_unique = (u16)((old_combined>>16) & 0xffff);
    // ASSERT_SLOW(old_shared > 0 && old_unique == 0);
    ASSERT_SLOW(old_combined < 0x10000/*no unique*/ && old_combined > 0 /*yes shared*/);
#endif
    // shared -= 1;
    atomic_decrement(&m->shared);
}
#else
inline SharedTicketMutex SharedTicketMutex::init()
{
    return {};
}

static inline void shared_ticket_mutex_unique_lock(SharedTicketMutex *m)
{
    ticket_mutex_lock(&m->underlying_mutex);
    while (atomic_load(&m->shared_count) != 0) {
        cpu_relax();
    }
}
static inline void shared_ticket_mutex_unique_unlock(SharedTicketMutex *m)
{
    ticket_mutex_unlock(&m->underlying_mutex);
}
static inline void shared_ticket_mutex_shared_lock(SharedTicketMutex *m)
{
    ticket_mutex_lock(&m->underlying_mutex);
    atomic_increment(&m->shared_count);
    ticket_mutex_unlock(&m->underlying_mutex);
}
static inline void shared_ticket_mutex_shared_unlock(SharedTicketMutex *m)
{
    atomic_decrement(&m->shared_count);
}
#endif
