#include "beat_include.hh"

// TODO: make simple_ini_get and other functions accept a SimpleIni that failed
// to initialize (it would act as if that key/section didn't exist)
// TODO: I don't need to goto rage_quit on some cases here.
// TODO: Don't arena_push_string_nullter for every key and value. Point to the original file.

#define simple_ini__report_error(c, filename, line1, format, ...) do {   \
    if (1) {                                                             \
        beat_log("[simple_ini] %.*s:%d: error: " format,                 \
                 EXPAND_STR_FOR_PRINTF(filename), line1, ##__VA_ARGS__); \
    }                                                                    \
} while (0);

static bool
simple_ini__get_line(u8 **buf_ptr, u8 *buf_end, s32 *line1, String *ret_str)
{
    while (true) {
        if (*buf_ptr >= buf_end)
            return false;

        if (line1) *line1 += 1;
        u8 *ptr = *buf_ptr;
        u8 *prev_ptr = *buf_ptr;
        while (ptr < buf_end) {
            if (*ptr == '\n') {
                break;
            }
            ptr++;
        }
        s32 len = (s32)(ptr-prev_ptr);
        if (ptr < buf_end) {
            ASSERT(*ptr == '\n');
            if ((ptr-1)>=prev_ptr && (*(ptr-1) == '\r')) len--;
            ptr++;
        }
        *buf_ptr = ptr;

        String str = {.len = len, .data = prev_ptr};
        trim_whitespace(&str);
        if (str.len <= 0)
            continue;
        *ret_str = str;
        return true;
    }
    return false;
}

static void
simple_ini__copy_value(MemoryArena *arena, SimpleIni_Item *dest, SimpleIni_Item item)
{
    dest->value = item.value;
}

static void
simple_ini__push(SimpleIni *cfg, SimpleIni_Section *section, SimpleIni_Item item)
{
    bool found = false;
    for (smem i = 0; i < section->items.len; i++) {
        if (section->items.data[i].key == item.key) {
            found = true;
            simple_ini__copy_value(cfg->arena, &section->items.data[i], item);
        }
    }
    if (!found) {
        SimpleIni_Item new_item = {};
        new_item.key = item.key;
        simple_ini__copy_value(cfg->arena, &new_item, item);
        section->items.push(new_item);
    }
}

static SimpleIni_Section *
simple_ini_get_section(SimpleIni *cfg, String section_name)
{
    for (smem i = 0; i < cfg->sections.len; i++) {
        if (cfg->sections.data[i].name == section_name)
            return &cfg->sections.data[i];
    }
    return NULL;
}

static bool
simple_ini_get_with_section_ptr(SimpleIni *cfg, SimpleIni_Section *section, String key, String *ret_val)
{
    if (section) {
        for (smem i = 0; i < section->items.len; i++) {
            if (section->items.data[i].key == key) {
                *ret_val = section->items.data[i].value;
                return true;
            }
        }
    }
    return false;
}

static bool
simple_ini_get(SimpleIni *cfg, String section_name, String key, String *ret_val)
{
    return simple_ini_get_with_section_ptr(cfg, simple_ini_get_section(cfg, section_name), key, ret_val);
}

static String
simple_ini_get_or_default(SimpleIni *cfg, String section_name, String key, String default_value)
{
    String v;
    if (simple_ini_get(cfg, section_name, key, &v))
        return v;
    return default_value;
}

static String
simple_ini_get_or_default_with_section_ptr(SimpleIni *cfg, SimpleIni_Section *section, String key, String default_value)
{
    String v;
    if (simple_ini_get_with_section_ptr(cfg, section, key, &v))
        return v;
    return default_value;
}


static bool
simple_ini__name_is_valid(String str)
{
    if (str.len <= 0)
        return false;
    for (smem i = 0; i < str.len; i++) {
        switch (str.data[i]) {
        case '.':
        case ' ':
        case '@':
        case '$':
        // case '+':
        case '=':
        case '[':
        case ']': return false; break;
        default: break;
        }
    }
    if (str == "self"_s)
        return false;
    return true;
}

static bool
simple_ini_file(MemoryArena *final_arena, MemoryArena *temp_arena,
                String filename_not_nullter, SimpleIni *ret_cfg)
{
    TempMemory temp_mem = begin_temp_memory(temp_arena);
    TempMemory temp_for_final_if_error = begin_temp_memory(final_arena);

    SimpleIni cfg = {};
    cfg.arena = final_arena;
    String filename = arena_push_string_nullter(final_arena, filename_not_nullter);
    s32 line1 = 0;
    EntireFile ef;
    u8 *buf_ptr, *buf_end;
    if (!read_entire_file_into_arena(filename, final_arena, &ef)) {
        beat_log("[simple_ini] Failed to read config file: '%.*s'\n", EXPAND_STR_FOR_PRINTF(filename));
        goto rage_quit;
    }
    buf_ptr = ef.buf.data;
    buf_end = ef.buf.data + ef.buf.len;

    cfg.sections.arena = cfg.arena;
    cfg.sections.reserve(64);
    cfg.ef = ef;


    {
        auto *curr_section = cfg.sections.push({});
        curr_section->items.arena = cfg.arena;
        curr_section->name = "_root"_s;

        while (true) {
            String line;
            if (!simple_ini__get_line(&buf_ptr, buf_end, &line1, &line)) break;
            if (line.data[0] == '#') continue;
            if (line.data[0] == '[') {
                advance(&line, 1);
                // new section
                String section_name;
                if (!split_by_char_and_trim_whitespace(line, ']', &section_name, NULL)) {
                    simple_ini__report_error(&cfg, filename, line1, "section didn't end with ']'\n");
                    goto rage_quit;
                }
                if (!simple_ini__name_is_valid(section_name)) {
                    simple_ini__report_error(&cfg, filename, line1, "section name is invalid: `%.*s`\n", EXPAND_STR_FOR_PRINTF(section_name));
                    goto rage_quit;
                }
                auto *test_section = simple_ini_get_section(&cfg, section_name);
                if (test_section) {
                    curr_section = test_section;
                } else {
                    curr_section = cfg.sections.push({});
                    curr_section->items.arena = cfg.arena;
                    curr_section->name = section_name;
                }
            } else if (line.data[0] == '@') {
                advance(&line, 1);
                trim_whitespace(&line); // yes, i need to trim here
                String lhs, rhs;
                if (!split_by_char_and_trim_whitespace(line, ' ', &lhs, &rhs)) {
                    simple_ini__report_error(&cfg, filename, line1, "expected some whitespace\n");
                    goto rage_quit;
                }
                if (lhs == "inherit"_s) {
                    String section_name = rhs;
                    auto *test_section = simple_ini_get_section(&cfg, section_name);
                    if (!test_section) {
                        simple_ini__report_error(&cfg, filename, line1, "unknown section name '%.*s'\n",
                                                 EXPAND_STR_FOR_PRINTF(section_name));
                        goto rage_quit;
                    }
                    for (smem i = 0; i < test_section->items.len; i++) {
                        simple_ini__push(&cfg, curr_section, test_section->items.data[i]);
                    }
                } else {
                    simple_ini__report_error(&cfg, filename, line1, "idk what this means (expected inherit)\n");
                    goto rage_quit;
                }
            } else {
                String var_name;
                String after_equal;
                if (!split_by_char_and_trim_whitespace(line, '=', &var_name, &after_equal)) {
                    printf("'%.*s'\n", EXPAND_STR_FOR_PRINTF(line));
                    simple_ini__report_error(&cfg, filename, line1, "expected '='\n");
                    goto rage_quit;
                }
                if (!simple_ini__name_is_valid(var_name)) {
                    simple_ini__report_error(&cfg, filename, line1, "var name is invalid: `%.*s`\n", EXPAND_STR_FOR_PRINTF(var_name));
                    goto rage_quit;
                }

                if (after_equal.len > 0 && after_equal.data[0] == '$') {
                    // copy the value from "$section.key"
                    // section name "self" is reserved to represent the current section
                    advance(&after_equal, 1);
                    String inherit_section, inherit_var_name;
                    if (!split_by_char_and_trim_whitespace(after_equal, '.', &inherit_section, &inherit_var_name)) {
                        simple_ini__report_error(&cfg, filename, line1, "expected '.' after '%.*s'\n",
                                                 EXPAND_STR_FOR_PRINTF(after_equal));
                        goto rage_quit;
                    }
                    SimpleIni_Section *the_section = 0;
                    if (inherit_section == "self"_s) {
                        the_section = curr_section;
                        inherit_section = curr_section->name;
                    } else {
                        the_section = simple_ini_get_section(&cfg, inherit_section);
                    }
                    if (!the_section) {
                        simple_ini__report_error(&cfg, filename, line1, "didin't find section '%.*s'\n",
                                                 EXPAND_STR_FOR_PRINTF(inherit_section));
                        goto rage_quit;
                    }
                    String inherited_value;
                    if (!simple_ini_get_with_section_ptr(&cfg, the_section, inherit_var_name, &inherited_value)) {
                        simple_ini__report_error(&cfg, filename, line1, "didn't find var '%.*s' from section '%.*s'\n",
                                                 EXPAND_STR_FOR_PRINTF(inherit_var_name),
                                                 EXPAND_STR_FOR_PRINTF(inherit_section));
                        goto rage_quit;
                    }
                    SimpleIni_Item new_item = {};
                    new_item.key = var_name;
                    new_item.value = inherited_value;
                    simple_ini__push(&cfg, curr_section, new_item);
                } else  {
                    SimpleIni_Item new_item = {};
                    new_item.key = var_name;
                    new_item.value = after_equal;
                    if (new_item.value.len > 0 && new_item.value.data[0] == '"') {
                        advance(&new_item.value, 1);
                        if (!split_by_char_from_right(new_item.value, '"', &new_item.value, NULL)) {
                            simple_ini__report_error(&cfg, filename, line1, "Unmatched '\"'\n");
                            goto rage_quit;
                        }
                    }
                    simple_ini__push(&cfg, curr_section, new_item);
                }
            }
        }
    }
    end_temp_memory_dont_free(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    *ret_cfg = cfg;
    return true;
rage_quit:
    end_temp_memory(&temp_for_final_if_error);
    end_temp_memory(&temp_mem);
    return false;
}


static void
simple_ini_print(SimpleIni *configs)
{
    // NOTE: This only prints the current state of the config. It does not
    // print original comments or blank lines or inherit other things.
    // Do not use this to overwrite the previous file.
    printf("----- PRINTING CONFIG START -----\n");
    for (s32 idx_section = 0; idx_section < configs->sections.len; idx_section++) {
        auto *section = &configs->sections.data[idx_section];
        printf("[%.*s]\n", EXPAND_STR_FOR_PRINTF(section->name));
        for (s32 idx_item = 0; idx_item < section->items.len; idx_item++) {
            auto *item = &section->items.data[idx_item];
            // ASSERT(item->value.type == SimpleIni_ItemType::String);
            printf("'%.*s' = '%.*s'\n", EXPAND_STR_FOR_PRINTF(item->key), EXPAND_STR_FOR_PRINTF(item->value));
        }
    }
    printf("----- PRINTING CONFIG END -----\n");
}
