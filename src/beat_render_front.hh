#ifndef BEAT_RENDER_FRONT_H
#define BEAT_RENDER_FRONT_H

// TODO: New rendering api
#if 0
{
    // NOTE: I just wrote down whatever came to mind so that I don't forget about this stuff later when I actually implement the new rendering api. I don't think everything in this comment is a good idea.
    // Make a render group that contains a list of things to render.
    // If mask is true and the render group is smaller than the screen, it does
    // not render anything outside of the bounding box of the render group. If
    // masked is false, it can render stuff outside of the groups' bounding
    // box.
    // auto group = RenderGroup::begin(parent_group, parent_relative_pos, size, ..., masked=true or false);
    // Make a new group using the entire screen space.
    // I can do this in two ways:
    // A:
    {
        auto group = RenderGroup::begin(gs.rendering.main_group, None, None, ..., masked = true);
        // Append an entry to the group
        auto text_bounding_box = group.append_text("Some text"_s, color, ...);
        // The returned bounding box of the text is inside the group's bounding box (pos, size on RenderGroup::begin())
        // Insert an entry at the start of the group.
        group.insert_rectangle_at(0, text_bounding_box, color, ...);
        // Append this group to the main render group.
        // 
        gs.rendering.main_group.append_group(group);
        // On render.c:
        // Loop on gs.rendering.main.group.elements
        // Render each element.
        // If the element is a group and `group.masked` is true, don't render stuff outside of the bounding box of the group.
        // If a group is masked and it has a subgroup that is not masked, that subgroup respects it's parent mask.
    }

    // B:
    {
        // A `RenderGroup` is a framebuffer with a list of things to draw on it.
        // A `RenderGroupTemp` is temporary memory that will later be appended
        // to a `RenderGroup` (I can shuffle entries around before append to the
        // main group).
        // RenderGroupTemp is a temporary group that has the same bounding box
        // as the parent `RenderGroup` (gs.rendering.main_group in this case).
        // When I call append_to_parent_and_end, it will append its contents to
        // the parent and end_temp_memory().
        // It will ASSERT(temp_arena != gs.rendering.main_group.arena).
        // RenderGroupTemp will have a member called 'offset'. When
        // append_to_parent_and_end is called, all members will have their
        // position changed by 'offset'.
        {
            auto temp_group = RenderGroupTemp::begin(&temp_arena, &gs.rendering.main_group);
            defer { temp_group.append_to_parent_and_end(); };
            // defer { group.end(); };
            // Append an entry to the group
            auto text_bounding_box = temp_group.group.append_text("Some text"_s, color, ...);

            // The returned bounding box of the text is inside the group's bounding box (pos, size on RenderGroup::begin())
            // Insert an entry at the start of the group.
            temp_group.group.insert_rectangle_at(0, text_bounding_box, color, ...);
            // Append this group to the main render group.
            // gs.rendering.main_group.append_temp_group(group);
        }
        // Append text centered would be made like this (no need to iterate over the glyphs twice).
        BoundingBox append_text_centered(String text) {
            auto temp_group = RenderGroupTemp::begin(&temp_arena, &gs.rendering.main_group);
            defer { temp_group.append_to_parent_and_end(); };
            auto text_bounding_box = temp_group.group.append_text(text, color, ...);
            temp_group.offset = -text_bounding_box.width / 2;
        }
        // Render text and show
        BoundingBox append_text_and_show_each_glyphs_bounding_box(String text) {
            auto temp_group_for_box = RenderGroupTemp::begin(&temp_arena1, &gs.rendering.main_group);
            auto temp_group_for_text = RenderGroupTemp::begin(&temp_arena2, &gs.rendering.main_group);
            _ = temp_group.group.append_text(text, color, ...);
            for (entry in temp_group.group.entries) {
                ASSERT(glyph.type == RenderEntryType_TextGlyph);
                auto const *glyph = &entry.specific.glyph;
                temp_group_for_box.append_rectangle(glyph->bounding_box.pos, glyph->bounding_box.size, box_color);
            }
            // Render boxes and then the text
            temp_group_for_box.append_to_parent_and_end();
            temp_group_for_text.append_to_parent_and_end();
        }
    }


}
#endif

enum RenderEntryType {
    RenderEntryType_Ignore,
    RenderEntryType_SimpleQuad,
    RenderEntryType_SdfQuad,

    // RenderEntryType_Text,

    RenderEntryType_COUNT,
};

struct RenderEntry {
    RenderEntryType type;
    union {
        struct {
            QuadPos pos;
            QuadPos uv;
            v4 color;
            u32 tex;
        } simple_quad;
        struct {
            QuadPos pos;
            QuadPos uv;
            v4 color;
            u32 tex;
            bool do_drop_shadow;
        } sdf_quad;
    };
};

struct Rendering {
    // NOTE: Rendering state needs to persist between frames!
    s32 window_width;
    s32 window_height;
    bool changed_resolution_this_frame; // zero on first frame
    s32 old_window_width; // zero on the first frame
    s32 old_window_height;

    TempMemory temp_mem;
    // DEBUG build:
    // NewArrayDynamic: 0.85ms
    // BucketArray with occupied: 1.80ms
    // BucketArray without occupied: 1.02ms
    // RELEASE build:
    // NewArrayDynamic: 0.51ms
    // BucketArray with occupied: 0.70ms
    // BucketArray without occupied: 0.54ms
    smem last_frames_entry_count;
    NewArrayDynamic<RenderEntry> entries; // on temp_mem.arena
};


#endif
