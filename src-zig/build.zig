const std = @import("std");
const builtin = @import("builtin");
const Builder = std.build.Builder;

pub fn myStandardReleaseOptions(self: *Builder, default: std.builtin.Mode) std.builtin.Mode {
    if (self.release_mode) |mode| return mode;

    const debug = self.option(bool, "debug", "Optimizations off and safety on") orelse false;
    const release_safe = self.option(bool, "release-safe", "Optimizations on and safety on") orelse false;
    const release_fast = self.option(bool, "release-fast", "Optimizations on and safety off") orelse false;
    const release_small = self.option(bool, "release-small", "Size optimizations on and safety off") orelse false;

    const mode: std.builtin.Mode = if (release_safe and !release_fast and !release_small and !debug)
        .ReleaseSafe
    else if (release_fast and !release_safe and !release_small and !debug)
        .ReleaseFast
    else if (release_small and !release_fast and !release_safe and !debug)
        .ReleaseSmall
    else if (!release_small and !release_fast and !release_safe and debug)
        .Debug
    else if (!release_fast and !release_safe and !release_small and !debug)
        default
    else {
        // std.debug.print("Multiple release modes (of -Drelease-safe, -Drelease-fast and -Drelease-small)", .{});
        @panic("Multiple release modes (of -Drelease-safe, -Drelease-fast and -Drelease-small)");
        // break :x default;
    };
    self.is_release = mode != .Debug;
    self.release_mode = mode;
    return mode;
}

const Internal = enum {
    internal,
    ship,
};

pub fn build(b: *Builder) void {
    const internal: Internal = if (b.option(bool, "internal", "produce internal build") orelse true) .internal else .ship;
    var target: std.zig.CrossTarget = b.standardTargetOptions(.{});
    target.cpu_arch = target.cpu_arch orelse builtin.cpu.arch;
    target.cpu_model =  switch (target.cpu_arch.?) {
        .x86_64 => .{ .explicit = &std.Target.x86.cpu.ivybridge },
        else => @panic("Unsupported architecture"),
    };

    // target.os_tag = target.os_tag orelse std.builtin.os.tag; // TODO: Can not linkSystemLibrary


    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    var mode = myStandardReleaseOptions(b, if (internal == .ship) .ReleaseFast else .Debug);

    const exe = b.addExecutable("linuxsdl_beat_zig", "src/main.zig");
    exe.addCSourceFile("./external/stb_truetype/stb_truetype_impl.c", &[_][]const u8{"-std=c99"});
    exe.addCSourceFile("./external/minimp3/minimp3_impl.c", &[_][]const u8{"-std=c99"});
    exe.addCSourceFile("./external/glad/src/glad.c", &[_][]const u8{"-std=c99"});
    exe.addIncludeDir("./external");
    exe.addIncludeDir("./external/glad/include");
    exe.linkSystemLibrary("c");
    exe.linkSystemLibrary("sdl2");
    exe.linkSystemLibrary("portaudio");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.strip = internal == .ship;
    exe.single_threaded = false;
    exe.disable_sanitize_c = true;

    exe.install();
    // TODO If exe succeeds to compile, also make symbolic links in the build_target directory. Maybe this is bad. If I build a Debug build, cd into the latest directory (linked to Debug) and then recompile to ReleaseSafe, the shell will think it is in latest (which is now ReleaseSafe) but it will actually be in Debug and this might be misleading.
    // build_target/
    //  x86_64-linux-gnu/
    //   Debug/
    //    ...
    //   ReleaseSafe/
    //   latest -> Debug/
    //  latest -> x86_64-linux-gnu/

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
