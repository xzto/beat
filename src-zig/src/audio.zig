const std = @import("std");
const c = @import("c.zig");
const print = std.debug.print;
const assert = std.debug.assert;
const NewArena = @import("my_arena.zig").NewArena;
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const Wrap = @import("wrap.zig").Wrap;
const global_running = @import("root").global_running;
const PRINT_SHIT_EVERY_SECOND = @import("root").PRINT_SHIT_EVERY_SECOND;
const TicketMutex = @import("ticket_mutex.zig").TicketMutex;
const SharedTicketMutex = @import("ticket_mutex.zig").SharedTicketMutex;

const AudioDecoderId = struct {
    gen: u64,
    pub fn invalid() AudioDecoderId {
        return .{ .gen = 0 };
    }
    pub fn is_valid(self: AudioDecoderId) bool {
        return self.gen != invalid().gen;
    }
    pub fn eql(self: AudioDecoderId, other: AudioDecoderId) bool {
        return std.meta.eql(self, other);
    }
};

pub const AudioDecoder = struct {
    id: AudioDecoderId,
    specific: Specific,

    sound_offset_seconds: f64,
    channels: u32,
    samples_per_second: u32,
    total_sample_count: u64,

    locked: struct {
        mutex: TicketMutex,
        data: LockedData,
    },

    sample_buffer_sample_count: u32,
    allocator: Allocator,
    main_buffer: SampleBuffer,
    aux_buffer: SampleBuffer,
    pub const MAX_CHANNELS = 2;
    const SampleBuffer = struct {
        const ALIGN = 16;
        // data: *[sample_buffer_sample_count*num_channels]align(ALIGN) i16,
        data: []align(ALIGN) i16,
    };
    pub const Specific = union(enum) {
        mp3: *c.mp3dec_ex_t,
    };
    const LockedData = struct {
        debug__caled_start_playing_at_least_once: bool,
        is_paused: bool,
        // is_looping: bool, // TODO
        mixer: MixerData,
        decoder: DecoderData,
        const MixerData = struct {
            total_read_sample_idx: u64,
            buffer_read_idx: u32,
            buffer_read_delta_idx: f32,
            curr_sound_time: f64, // does not take `sound_offset_seconds` into account
            speed_to_play: f32,
            is_finished: bool,
            pub fn eql(self: MixerData, other: MixerData) bool {
                std.meta.eql(self, other);
            }
        };
        const DecoderData = struct {
            total_write_sample_idx: u64,
            buffer_write_idx: u32,
            did_first_write: bool,
            pub fn eql(self: DecoderData, other: DecoderData) bool {
                std.meta.eql(self, other);
            }
        };
    };
};

pub const Volume = @Vector(2, f32);

pub var global_audio_state: AudioState = undefined;
pub const AudioState = struct {
    // permanent data. This arena is not thread safe; be careful. For now only decoders.active_list uses it but later something else might use it.
    arena: NewArena,
    decoders: struct {
        pub fn get_next_gen_id(self: *@This()) u64 {
            const new_val = @atomicRmw(u64, &self.next_gen_id.unwrap, .Add, 1, .Relaxed);
            assert(new_val > 0);
            return new_val;
        }
        next_gen_id: Wrap(u64) = Wrap(u64).init(1),
        // everything below this needs a lock
        shared_mutex: SharedTicketMutex,
        active_list: ArrayList(AudioDecoder),
        // decoders: ActiveAndFreeList(AudioDecoder), // TODO
        // decoders: std.SegmentedList(AudioDecoder), // TODO
    },

    master_volume: Volume,
    output_buffer_samples_per_second: u32,

    pa_stream: ?*c.PaStream,
    audio_decoder_thread: std.Thread,
};

var is_initialized = false;
pub fn init(gpa: Allocator) !void {
    assert(!is_initialized);
    is_initialized = true;

    errdefer global_running.set(false);

    var pa_err = c.Pa_Initialize();
    if (pa_err != c.paNoError) {
        print("[audio.init] error Pa_Initialize: {s}\n", .{c.Pa_GetErrorText(pa_err)});
        return error.PaError;
    }
    errdefer _ = c.Pa_Terminate();
    global_audio_state.arena = NewArena.init_size(gpa, 1 * 1024 * 1024) catch unreachable;
    errdefer global_audio_state.arena.deinit();
    global_audio_state.decoders = .{
        .shared_mutex = SharedTicketMutex{},
        .active_list = try ArrayList(AudioDecoder).initCapacity(global_audio_state.arena.allocator(), 64),
    };
    errdefer global_audio_state.decoders.active_list.deinit();
    global_audio_state.master_volume = .{ 0.3, 0.3 };
    global_audio_state.output_buffer_samples_per_second = 44100;
    var output_parameters = std.mem.zeroes(c.PaStreamParameters);
    output_parameters.device = c.Pa_GetDefaultOutputDevice();

    // #define paNoDevice ((PaDeviceIndex)-1)
    const paNoDevice = @as(c.PaDeviceIndex, -1);
    if (output_parameters.device == paNoDevice) {
        print("[audio.init] no default output device\n", .{});
        return error.PaError;
    }
    output_parameters.channelCount = 2;
    output_parameters.sampleFormat = c.paFloat32;
    output_parameters.suggestedLatency = @ptrCast(?*const c.PaDeviceInfo, c.Pa_GetDeviceInfo(output_parameters.device)).?.defaultLowOutputLatency;
    output_parameters.hostApiSpecificStreamInfo = null;

    pa_err = c.Pa_OpenStream(
        &global_audio_state.pa_stream,
        0,
        &output_parameters,
        @intToFloat(f64, global_audio_state.output_buffer_samples_per_second),
        // c.paFramesPerBufferUnspecified,
        512,
        c.paClipOff,
        my_pa_callback,
        null,
    );
    if (pa_err != c.paNoError) {
        print("[audio.init] error Pa_OpenStream: {s}\n", .{c.Pa_GetErrorText(pa_err)});
        return error.PaError;
    }
    errdefer _ = c.Pa_CloseStream(global_audio_state.pa_stream);

    pa_err = c.Pa_StartStream(global_audio_state.pa_stream);
    if (pa_err != c.paNoError) {
        print("[audio.init] error Pa_StartStream: {s}\n", .{c.Pa_GetErrorText(pa_err)});
        return error.PaError;
    }
    errdefer c.Pa_StopStream(global_audio_state.pa_stream);

    global_audio_state.audio_decoder_thread = std.Thread.spawn(
        .{ .stack_size = 32 * 1024 },
        audio_decoder_thread_proc,
        .{},
    ) catch @panic("failed to spawn audio decoder thread");

    return;
}

pub fn deinit() void {
    assert(is_initialized);
    global_running.set(false); // just to make sure

    _ = c.Pa_StopStream(global_audio_state.pa_stream);
    _ = c.Pa_CloseStream(global_audio_state.pa_stream);
    _ = c.Pa_Terminate();
    global_audio_state.arena.deinit();
    // No need to free anything
    global_audio_state = undefined;
    print("audio.deinit\n", .{});
}

fn do_the_mixer(
    output_buffer: [][2]f32,
    output_buffer_samples_per_second: u32,
    decoder: *AudioDecoder,
    volume: Volume,
) void {
    _ = output_buffer;
    _ = output_buffer_samples_per_second;
    _ = decoder;
    _ = volume;
}

fn my_pa_callback(
    input_buffer: ?*const anyopaque,
    output_buffer_: ?*anyopaque,
    frames_per_buffer: c_ulong,
    time_info: [*c]const c.PaStreamCallbackTimeInfo,
    status_flags: c.PaStreamCallbackFlags,
    user_data: ?*anyopaque,
) callconv(.C) c_int {
    _ = input_buffer;
    _ = time_info;
    _ = status_flags;
    _ = user_data;
    const output_buffer: [][2]f32 =
        @ptrCast([*][2]f32, @alignCast(@alignOf(f32), output_buffer_.?))[0..frames_per_buffer];
    assert(std.mem.sliceAsBytes(output_buffer).len == frames_per_buffer * 2 * @sizeOf(f32));
    std.mem.set(u8, std.mem.sliceAsBytes(output_buffer), 0);
    const output_buffer_samples_per_second = global_audio_state.output_buffer_samples_per_second;

    const audio_frame_count: *volatile u64 = &(struct {
        var x: u64 = 1;
    }).x;
    defer audio_frame_count.* += 1;

    const master_volume = global_audio_state.master_volume;
    {
        var held = global_audio_state.decoders.shared_mutex.sharedLock();
        defer held.unlock();

        for (global_audio_state.decoders.active_list.items) |*decoder| {
            do_the_mixer(
                output_buffer,
                output_buffer_samples_per_second,
                decoder,
                master_volume,
            );
        }
    }

    // about once a second
    if (audio_frame_count.* % (44100 / 512) == 0) {
        if (PRINT_SHIT_EVERY_SECOND) {
            print("Hello from my_pa_callback: {}\n", .{audio_frame_count.*});
        }
    }

    if (true) {
        var val: f32 = 0;
        for (output_buffer) |*sample| {
            sample.* = master_volume * @Vector(2, f32){ val, val };
            val += 0.001;
            if (val > 1) val = -1;
        }
    }

    return c.paContinue;
}

fn audio_decoder_thread_proc() void {
    while (global_running.get()) {
        std.time.sleep(1 * std.time.ns_per_s);
        if (PRINT_SHIT_EVERY_SECOND) {
            print("Hello from audio_decoder_thread\n", .{});
        }
    }
}
