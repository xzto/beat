const std = @import("std");

const assert = std.debug.assert;
const Wrap = @import("./wrap.zig").Wrap;

// TODO: Get rid of this garbage.

pub const TicketMutex = struct {
    const Self = @This();

    const Ticket = Wrap(u64);
    ticket: Ticket = Ticket.init(0),
    served: Ticket = Ticket.init(0),

    // const debugPrint = std.debug.warn;
    inline fn debugPrint(comptime _: []const u8, _: anytype) void {}

    pub const Held = struct {
        m: *TicketMutex,

        pub fn unlock(held: *Held) void {
            _ = @atomicRmw(Ticket.T, &held.m.served.unwrap, .Add, 1, .SeqCst);
            held.* = undefined;
            debugPrint("unlock\n", .{});
        }
    };

    pub fn lock(self: *Self) Held {
        const my_ticket = @atomicRmw(Ticket.T, &self.ticket.unwrap, .Add, 1, .SeqCst);
        while (my_ticket != @atomicLoad(Ticket.T, &self.served.unwrap, .SeqCst)) {
            std.atomic.spinLoopHint();
        }
        debugPrint("lock\n", .{});
        return Held{ .m = self };
    }

    pub fn tryLock(self: *Self) ?Held {
        debugPrint("entered tryLock\n", .{});
        const my_ticket = @atomicLoad(Ticket.T, &self.ticket.unwrap, .SeqCst);
        if (my_ticket == @atomicLoad(Ticket.T, &self.served.unwrap, .SeqCst)) {
            if (@cmpxchgStrong(Ticket.T, &self.ticket.unwrap, my_ticket, my_ticket +% 1, .SeqCst, .SeqCst) == null) {
                debugPrint("tryLock ok\n", .{});
                return Held{ .m = self };
            }
        }
        debugPrint("tryLock fail\n", .{});
        return null;
    }
};

pub const SharedTicketMutex = struct {
    const Self = @This();

    underlying_mutex: TicketMutex = TicketMutex{},
    shared_count: Wrap(u64) = Wrap(u64).init(0),

    // const debugPrint = std.debug.warn;
    inline fn debugPrint(comptime _: []const u8, _: anytype) void {}

    pub const HeldUnique = struct {
        underlying_held: TicketMutex.Held,
        pub fn unlock(held: *@This()) void {
            held.underlying_held.unlock();
            debugPrint("unique unlock\n", .{});
        }
    };
    pub const HeldShared = struct {
        sh_mutex: *SharedTicketMutex,
        pub fn unlock(held: *@This()) void {
            _ = @atomicRmw(u64, &held.sh_mutex.shared_count.unwrap, .Sub, 1, .SeqCst);
            debugPrint("shared unlock\n", .{});
        }
    };

    pub fn uniqueLock(self: *Self) HeldUnique {
        var under_held = self.underlying_mutex.lock();
        while (true) {
            const shared_count = @atomicLoad(TicketMutex.Ticket.T, &self.shared_count.unwrap, .SeqCst);
            if (shared_count == 0) {
                break;
            }
            std.Thread.spinLoopHint();
        }
        debugPrint("uniqueLock\n", .{});
        return HeldUnique{ .underlying_held = under_held };
    }
    pub fn uniqueTryLock(self: *Self) ?HeldUnique {
        if (self.underlying_mutex.tryLock()) |*underlying_held| {
            const shared_count = @atomicLoad(TicketMutex.Ticket.T, &self.shared_count.unwrap, .SeqCst);
            if (shared_count == 0) {
                debugPrint("uniqueTryLock ok\n", .{});
                return HeldUnique{ .underlying_held = underlying_held.* };
            }
            underlying_held.unlock();
        }
        debugPrint("uniqueTryLock fail\n", .{});
        return null;
    }

    pub fn sharedLock(self: *Self) HeldShared {
        debugPrint("sharedLock1\n", .{});
        var under_held = self.underlying_mutex.lock();
        _ = @atomicRmw(u64, &self.shared_count.unwrap, .Add, 1, .SeqCst);
        under_held.unlock();
        debugPrint("sharedLock2\n", .{});
        return HeldShared{ .sh_mutex = self };
    }
    pub fn sharedTryLock(self: *Self) ?HeldShared {
        if (self.underlying_mutex.tryLock()) |*underlying_held| {
            _ = @atomicRmw(u64, &self.shared_count.unwrap, .Add, 1, .SeqCst);
            underlying_held.unlock();
            debugPrint("sharedTryLock ok\n", .{});
            return HeldShared{ .sh_mutex = self };
        }
        debugPrint("sharedTryLock fail\n", .{});
        return null;
    }
};
