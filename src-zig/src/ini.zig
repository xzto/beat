// File syntax:
// key = value
// [section_name]
// key = value
// @inherit section_name
// key2 = $section_name.key2
// key3 = $self.key2
// Values are strings.

// TODO: Review this file

const std = @import("std");
const mem = std.mem;
const fs = std.fs;
const ArrayList = std.ArrayList;
const debugPrint = std.debug.warn;
const assert = std.debug.assert;

const misc = @import("./misc.zig");

pub const Ini = struct {
    const Self = @This();
    pub const Section = struct {
        pub const Item = struct {
            name: []const u8, // on file_contents
            value: []const u8, // on file_contents
        };

        name: []const u8, // on file_contents or "_root"
        items: ArrayList(Item), // on allocator

        pub fn findItemByName(section: *const Section, name: []const u8) ?[]const u8 {
            return if (findItemByNameInternal(section, name)) |it| it.value else null;
        }
    };

    filename: []const u8, // not allocated - the thing passed to `fromFilename`
    allocator: *mem.Allocator,
    file_contents: []const u8, // on allocator
    sections: ArrayList(Section), // on allocator

    const Error = error{ OutOfMemory, ReadFileError, Syntax };
    pub fn initFromFilename(filename: []const u8, allocator: *mem.Allocator) !Self {
        const bytes = fs.cwd().readFileAlloc(allocator, filename, 1024 * 1024 * 64) catch return error.ReadFileError;
        errdefer allocator.free(bytes);
        var remaining = bytes[0..];
        var sections = try ArrayList(Section).initCapacity(allocator, 1);
        errdefer {
            for (sections.items) |se| {
                se.items.deinit();
            }
            sections.deinit();
        }
        sections.appendAssumeCapacity(.{
            .name = "_root",
            .items = try ArrayList(Section.Item).initCapacity(allocator, 8),
        });
        var curr_section = &sections.items[0];
        while (misc.getLineTrimmed(&remaining)) |line| {
            assert(line.len > 0);
            if (line[0] == '[') {
                // new section
                if (line.len < 2 or line[line.len - 1] != ']') {
                    return error.Syntax;
                }
                const section_name = misc.trimWhitespace(line[1 .. line.len - 1]);
                if (!nameIsValid(section_name)) {
                    return error.Syntax;
                }

                if (findSectionByNameInternal(sections.items, section_name)) |section| {
                    curr_section = section;
                } else {
                    curr_section = try sections.addOne();
                    curr_section.* = .{
                        .name = section_name,
                        .items = try ArrayList(Section.Item).initCapacity(allocator, 8),
                    };
                }
            } else {
                // new item in curr_section
                if (line.len >= 1 and line[0] == '@') {
                    const chop_result = misc.chopByDelimFromLeft(line, ' ', true);
                    if (mem.eql(u8, chop_result.lhs, "@inherit")) {
                        const inherit_name = chop_result.rhs orelse return error.Syntax;
                        if (!nameIsValid(inherit_name)) {
                            return error.Syntax;
                        }
                        const section = findSectionByNameInternal(sections.items, inherit_name) orelse return error.Syntax;
                        for (section.items.items) |item| {
                            try addItemToSection(curr_section, item.name, item.value);
                        }
                    } else {
                        return error.Syntax;
                    }
                } else {
                    const chop_result = misc.chopByDelimFromLeft(line, '=', true);
                    const item_name = chop_result.lhs;
                    if (!nameIsValid(item_name)) {
                        return error.Syntax;
                    }
                    const equal_rhs = chop_result.rhs orelse return error.Syntax;
                    var item_value: []const u8 = undefined;
                    if (equal_rhs.len >= 1 and equal_rhs[0] == '$') {
                        const dot_chop_result = misc.chopByDelimFromLeft(equal_rhs[1..], '.', true);
                        var dollar_section: *Section = undefined;
                        if (mem.eql(u8, dot_chop_result.lhs, "self")) {
                            dollar_section = curr_section;
                        } else {
                            const dollar_section_name = dot_chop_result.lhs;
                            if (!nameIsValid(dollar_section_name)) {
                                return error.Syntax;
                            }
                            dollar_section = findSectionByNameInternal(sections.items, dollar_section_name) orelse return error.Syntax;
                        }
                        const dollar_item_name = dot_chop_result.rhs orelse return error.Syntax;
                        if (!nameIsValid(dollar_item_name)) {
                            return error.Syntax;
                        }
                        const dollar_item = findItemByNameInternal(dollar_section, dollar_item_name) orelse return error.Syntax;
                        item_value = dollar_item.value;
                    } else {
                        item_value = try parseValue(equal_rhs);
                    }

                    try addItemToSection(curr_section, item_name, item_value);
                }
            }
        }
        assert(remaining.len == 0);
        return Self{
            .filename = filename,
            .allocator = allocator,
            .file_contents = bytes,
            .sections = sections,
        };
    }

    pub fn deinit(self: *Self) void {
        for (self.sections.items) |section| {
            section.items.deinit();
        }
        self.sections.deinit();
        self.allocator.free(self.file_contents);
        self.* = undefined;
    }

    pub fn debugPrintEntireThing(self: Self) void {
        debugPrint("Ini test: filename `{}`\n", .{self.filename});
        for (self.sections.items) |section, section_idx| {
            debugPrint("[`{}`] ({})\n", .{ section.name, section_idx });
            for (section.items.items) |item, item_idx| {
                debugPrint("({}) - `{}` = `{}`\n", .{ item_idx, item.name, item.value });
            }
            debugPrint("\n", .{});
        }
    }

    pub fn getRoot(self: Self) *Section {
        assert(self.sections.items.len > 0);
        return &self.sections.items[0];
    }

    pub fn findSectionByName(self: Self, name: []const u8) ?*Section {
        return findSectionByNameInternal(self.sections.items, name);
    }

    pub fn findItemByName(self: Self, section: ?*const Section, name: []const u8) ?[]const u8 {
        if (section) |s|
            if (findItemByNameInternal(s, name)) |it|
                return it.value;
        return null;
    }
};

fn findSectionByNameInternal(sections: []Ini.Section, name: []const u8) ?*Ini.Section {
    for (sections) |*s| {
        if (mem.eql(u8, s.name, name)) {
            return s;
        }
    }
    return null;
}

fn findItemByNameInternal(section: *const Ini.Section, name: []const u8) ?*Ini.Section.Item {
    for (section.items.items) |*item| {
        if (mem.eql(u8, item.name, name)) {
            return item;
        }
    }
    return null;
}

fn nameIsValid(name: []const u8) bool {
    if (name.len <= 0) return false;
    for (name) |ch| {
        switch (ch) {
            ' ', '[', ']', '=', '@', '$', '+', '"' => return false,
            else => {},
        }
    }
    if (mem.eql(u8, name, "self")) return false;
    return true;
}

fn addItemToSection(section: *Ini.Section, item_name: []const u8, item_value: []const u8) !void {
    if (findItemByNameInternal(section, item_name)) |old_item| {
        old_item.value = item_value;
    } else {
        try section.items.append(.{
            .name = item_name,
            .value = item_value,
        });
    }
}

/// If `old_value` starts and ends with '"', remove those characters, else return `old_value`.
fn parseValue(value: []const u8) ![]const u8 {
    const quote = '"';
    if (value.len > 0 and value[0] == quote) {
        if (value.len < 2 or value[value.len - 1] != quote) {
            return error.Syntax;
        }
        return value[1 .. value.len - 1];
    }
    return value;
}
