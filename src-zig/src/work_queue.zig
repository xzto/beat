const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const NewArena = @import("my_arena.zig").NewArena;
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const Wrap = @import("wrap.zig").Wrap;
const global_running = @import("root").global_running;
const ResetEvent = std.Thread.ResetEvent;
const TicketMutex = @import("ticket_mutex.zig").TicketMutex;
const Atomic = std.atomic.Atomic;

const AtomicBool = Atomic(bool);
const AtomicBool_ = struct {
    x: bool,
    pub fn init(x: bool) AtomicBool {
        return .{ .x = x };
    }
    pub fn get(self: *AtomicBool) bool {
        return @atomicLoad(bool, &self.x, .SeqCst);
    }
    pub fn set(self: *AtomicBool, new_value: bool) void {
        return @atomicStore(bool, &self.x, new_value, .SeqCst);
    }
};

// TODO: `fn init(result: *Self, ...) void` to `fn init(...) Self`: https://github.com/ziglang/zig/issues/2765: result location: ability to refer to the return result location before the `return` statement #2765

const Work_TestNew = struct {
    const Self = @This();
    base: WorkQueue.WorkBase,
    input: struct {
        msg: []const u8, // on base.arena
    },
    output: struct {
        msg: []const u8, // on base.arena
        done: AtomicBool,
    },
    pub fn init(allocator: Allocator, number: usize) *Self {
        // I could also pass base as an argument.
        const result = blk: {
            var base = WorkQueue.WorkBase.init(allocator);
            const result = base.arena.allocator().create(Self) catch unreachable;
            result.base = base;
            break :blk result;
        };
        result.input = .{
            .msg = std.fmt.allocPrint(
                result.base.arena.allocator(),
                "test work {}",
                .{number},
            ) catch unreachable,
        };
        result.output = undefined;
        result.output.done = AtomicBool.init(false);
        return result;
    }
    pub fn deinit(self: *Self) void {
        assert(self.output.done.load(.SeqCst));
        // self.base.arena.allocator.free(self.input.msg);
        // self.base.arena.allocator.free(self.output.msg);

        var base = self.base;
        base.arena.allocator().destroy(self);
        // self is invalid
        base.deinit();
        // I could also do: base.arena.clear() and base.arena.destroy_unused_blocks(1) and return base (instead of base.deinit).
        // return base;
    }
    pub fn entry(self: *Self) WorkQueue.Entry {
        return WorkQueue.Entry{ .func = func, .data = self };
    }
    fn func(ctx: WorkQueue.WorkContext, data: *anyopaque) void {
        const self: *Self = @ptrCast(*Self, @alignCast(@alignOf(Self), data));
        defer self.output.done.store(true, .SeqCst);
        std.time.sleep(std.time.ns_per_ms * 50);
        self.output.msg = std.fmt.allocPrint(
            self.base.arena.allocator(),
            "Test work ctx `{}` inputmsg `{s}`\n",
            .{ ctx, self.input.msg },
        ) catch unreachable;
    }
};

fn test_some_work_new(wqueue: *WorkQueue) void {
    // const gpa = std.heap.page_allocator;
    var gpa_instance = std.heap.GeneralPurposeAllocator(.{}){};
    const gpa = gpa_instance.allocator();
    defer {
        const leaks = gpa_instance.deinit();
        if (leaks) {
            print("test_some_work_new: There were leaks\n", .{});
        }
    }
    const NUM_TO_DO = 8;
    var alive_works = ArrayList(*Work_TestNew).initCapacity(gpa, NUM_TO_DO) catch unreachable;
    defer alive_works.deinit();
    {
        var i: usize = 0;
        while (i < NUM_TO_DO) : (i += 1) {
            const work = Work_TestNew.init(gpa, i);
            alive_works.appendAssumeCapacity(work);
            wqueue.add_work(work.entry());
        }
    }
    defer assert(alive_works.items.len == 0);

    while (alive_works.items.len > 0) {
        var i = alive_works.items.len;
        while (i > 0) {
            i -= 1;
            const w = alive_works.items[i];
            if (w.output.done.load(.SeqCst)) {
                // print("[WorkQueueTestNew] Work is done: `{s}`\n", .{w.output.msg});
                w.deinit();
                _ = alive_works.swapRemove(i);
            }
        }
    }
    print("[WorkQueueTestNew] Finished everything\n", .{});
}

const Work_TestNoOutput = struct {
    const Self = @This();
    base: WorkQueue.WorkBase,
    input: struct {
        msg: []const u8,
    },
    output: struct {
        done: AtomicBool,
    },
    pub fn init(result: *Self, allocator: Allocator, number: usize) void {
        result.* = undefined;
        result.base = WorkQueue.WorkBase.init(allocator);
        result.input = .{ .msg = std.fmt.allocPrint(result.base.arena.allocator(), "test work {}", .{number}) catch unreachable };
        result.output = undefined;
        result.output.done = AtomicBool.init(false);
    }
    pub fn deinit(self: *Self) void {
        assert(self.output.done.load(.SeqCst));
        // self.base.arena.allocator.free(self.input.msg);
        self.base.deinit();
        self.* = undefined;
    }
    pub fn entry(self: *Self) WorkQueue.Entry {
        return WorkQueue.Entry{ .func = func, .data = self };
    }
    fn func(ctx: WorkQueue.WorkContext, data: *anyopaque) void {
        _ = ctx;
        const self: *Self = @ptrCast(*Self, @alignCast(@alignOf(Self), data));
        defer self.output.done.store(true, .SeqCst);
        std.time.sleep(std.time.ns_per_ms * 10);
        // print("Test work `{}` `{}`\n", .{ ctx, self.input.msg });
    }
};

fn test_some_work(wqueue: *WorkQueue) void {
    // const gpa = std.heap.page_allocator;
    const gpa = wqueue.gpa;
    var work_pool = ArrayList(Work_TestNoOutput).initCapacity(gpa, 50) catch unreachable;
    defer work_pool.deinit();
    work_pool.resize(20) catch unreachable;
    var alive_works = ArrayList(*Work_TestNoOutput).initCapacity(gpa, work_pool.capacity) catch unreachable;
    defer alive_works.deinit();
    for (work_pool.items) |_, iiii| {
        const test_work = work_pool.addOneAssumeCapacity();
        test_work.init(gpa, iiii);
        alive_works.appendAssumeCapacity(test_work);
        wqueue.add_work(test_work.entry());
    }
    // result.finish_all_work();
    while (alive_works.items.len > 0) {
        var i = alive_works.items.len;
        while (i > 0) {
            i -= 1;
            const w = alive_works.items[i];
            if (w.output.done.load(.SeqCst)) {
                // print("[WorkQueueTest] Work is done: {}\n", .{i});
                w.deinit();
                _ = alive_works.swapRemove(i);
            }
        }
    }
    print("[WorkQueueTest] Finished everything\n", .{});
}

const Work_TestYesOutput = struct {
    const Self = @This();
    base: WorkQueue.WorkBase,
    input: struct {
        number: i32,
    },
    output: struct {
        mutex: TicketMutex,
        results: ArrayList(i32),
        done: bool,
    },
    // TODO: https://github.com/ziglang/zig/issues/2765
    pub fn init(result: *Self, allocator: Allocator, number: i32) void {
        result.* = undefined;
        result.base = WorkQueue.WorkBase.init(allocator);
        result.input = .{ .number = number };
        // var arena_copy = result.base.arena; // this will crash
        result.output = .{
            .mutex = TicketMutex{},
            .results = ArrayList(i32).init(result.base.arena.allocator()),
            // .results = ArrayList(i32).init(&arena_copy.allocator), // this will crash
            .done = false,
        };
    }
    pub fn deinit(self: *Self) void {
        assert(self.output.done);
        assert(self.output.results.items.len == 0);
        // self.output.results.deinit(); // on self.base.arena
        self.base.deinit();
        self.* = undefined;
    }
    pub fn entry(self: *Self) WorkQueue.Entry {
        return WorkQueue.Entry{ .func = func, .data = self };
    }
    fn func(ctx: WorkQueue.WorkContext, data: *anyopaque) void {
        const self: *Self = @ptrCast(*Self, @alignCast(@alignOf(Self), data));
        var power: i32 = 1;
        var i: i32 = 1;
        while (i <= 3) : (i += 1) {
            if (ctx.shall_quit.load(.SeqCst)) break;
            var calculated: i32 = undefined;
            {
                calculated = i * power;
                std.time.sleep(std.time.ns_per_ms * 10);
            }
            {
                // could also have a local array, append to the local array first and try_lock()
                var held = self.output.mutex.lock();
                defer held.unlock();
                self.output.results.append(calculated) catch unreachable;
            }
            power *= 10;
        }
        {
            var held = self.output.mutex.lock();
            defer held.unlock();
            self.output.done = true;
        }
    }
    /// Returns true if it's done.
    pub fn acquire_some_output(self: *Self, out_array: *ArrayList(i32)) bool {
        // The common pattern for acquire_some_output is:
        // - Every frame {
        //      var local_temporary_array: ArrayList(Output_for_xyz);
        //      - Loop on some ArrayList(*Work_xyz) - a list of active work of type Work_xyz {
        //          work.output.lock() {
        //              - Check if there is new output from the job, if there is, move to a local temporary array,
        //              - If the work was done, the caller will call work.deinit() and remove from the active list of work.
        //          }
        //      }
        //      Move result from the local temporary array to the final place. (The reason for a local temporary array is that moving to the final place might not be just an appendSlice())
        // }
        var held = self.output.mutex.lock();
        defer held.unlock();
        const was_done = self.output.done;
        out_array.appendSlice(self.output.results.items) catch unreachable;
        self.output.results.items.len = 0;
        return was_done;
    }
};

fn test_some_work_with_output(wqueue: *WorkQueue) void {
    // const gpa = std.heap.page_allocator;
    const gpa = wqueue.gpa;
    var work_pool = ArrayList(Work_TestYesOutput).initCapacity(gpa, 200) catch unreachable;
    defer work_pool.deinit();
    var alive_works = ArrayList(*Work_TestYesOutput).initCapacity(gpa, work_pool.capacity) catch unreachable;
    defer alive_works.deinit();
    work_pool.resize(4) catch unreachable;
    for (work_pool.items) |*w, iiii| {
        w.init(gpa, @intCast(i32, iiii * 10));
        alive_works.appendAssumeCapacity(w);
        wqueue.add_work(w.entry());
    }

    // var xxarena1 = NewArena.init_size(gpa, 1_000_000) catch unreachable;
    // defer xxarena1.deinit();
    // var temp_arena = NewArena.init(&xxarena1.allocator);
    var temp_arena = NewArena.init(gpa);
    defer temp_arena.deinit();
    while (true) {
        if (alive_works.items.len == 0) break;
        {
            // run this once a frame
            var temp = temp_arena.save();
            defer temp.restore(&temp_arena);
            {
                var x = temp_arena.allocator().alloc(u32, 1) catch unreachable;
                x = temp_arena.allocator().realloc(x, 3) catch unreachable;
                x = temp_arena.allocator().realloc(x, 1) catch unreachable;
                x = temp_arena.allocator().realloc(x, 10) catch unreachable;
                x = temp_arena.allocator().realloc(x, 0) catch unreachable;
                x = temp_arena.allocator().realloc(x, 2) catch unreachable;
                x = temp_arena.allocator().realloc(x, 20) catch unreachable;
                x = temp_arena.allocator().realloc(x, 50) catch unreachable;
                x = temp_arena.allocator().realloc(x, 100) catch unreachable;
                x = temp_arena.allocator().realloc(x, 200) catch unreachable;
                x = temp_arena.allocator().realloc(x, 5000) catch unreachable;
                temp_arena.allocator().free(x);
            }
            var temp_results_arr = ArrayList(i32).init(temp_arena.allocator());
            defer temp_results_arr.deinit();
            var i = alive_works.items.len;
            while (i > 0) {
                i -= 1;
                const w = alive_works.items[i];
                if (w.acquire_some_output(&temp_results_arr)) {
                    print("[WorkQueueTestWithOutput] Work is done: {}\n", .{w.input.number});
                    w.deinit();
                    _ = alive_works.swapRemove(i);
                }
            }
            for (temp_results_arr.items) |n| {
                _ = n;
                // print("n: {}\n", .{n});
            }
            temp_results_arr.items.len = 0;
        }
    }
    print("[WorkQueueTestWithOutput] Finished everything\n", .{});
}

pub const WorkQueue = struct {
    /// A thread safe allocator that all threads can use.
    gpa: Allocator,
    /// An arena for `free_entries`
    entries_arena: NewArena,
    /// `Entry`s free to use.
    free_entries: EntryQueue,
    /// `Entry`s to be done.
    entry_queue: EntryQueue,
    /// The worker threads.
    threads: ArrayList(std.Thread),

    /// Incremented on `add_work()`
    total_added: Atomic(u64) = Atomic(u64).init(0),
    /// Incremented before the entry's function is called.
    total_started: Atomic(u64) = Atomic(u64).init(0),
    /// Incremented after the entry's function completes.
    total_completed: Atomic(u64) = Atomic(u64).init(0),

    /// A semaphore-like thing.
    reset_event: ResetEvent,

    /// Used to kill all threads.
    shall_quit: struct {
        x: AtomicBool align(cache_line_size_estimate),
    },
    const cache_line_size_estimate = 128;
    comptime {
        assert(@sizeOf(@TypeOf(@as(WorkQueue, undefined).shall_quit)) % cache_line_size_estimate == 0);
        assert(@alignOf(@TypeOf(@as(WorkQueue, undefined).shall_quit)) == cache_line_size_estimate);
    }

    const EntryQueue = std.atomic.Queue(Entry);

    // WorkBase is something most stuff will need (it's not 100% necessary to call add_work).
    pub const WorkBase = struct {
        // This is for stuff that will be freed when the work ends.
        allocator: Allocator,
        arena: NewArena,
        pub fn init(allocator: Allocator) WorkBase {
            // TODO: Might be worth to make a `free_arenas: std.atomic.Queue(NewArena)` if I make a high priority queue (something that runs every frame).
            // TODO: Recreating arenas every time a work starts isn't that bad if the work queue has low priority (background loading, etc) and `add_work` is not called every frame.
            // If I need to `add_work` every frame that is not true.
            // I could just not use WorkBase in that case and do something specific.
            return WorkBase{
                .allocator = allocator,
                .arena = NewArena.init(allocator),
            };
        }
        pub fn deinit(base: *WorkBase) void {
            base.arena.deinit();
            base.* = undefined;
        }
    };
    pub const Entry = struct {
        func: fn (work_ctx: WorkContext, data: *anyopaque) void,
        data: *anyopaque,
    };
    pub fn init(result: *WorkQueue, gpa: Allocator, num_threads: u32) !void {
        result.* = WorkQueue{
            .gpa = gpa,
            .entries_arena = undefined, // later
            .free_entries = EntryQueue.init(),
            .entry_queue = EntryQueue.init(),
            .shall_quit = .{ .x = AtomicBool.init(false) },
            .threads = undefined, // later
            .reset_event = undefined, // later
        };
        try result.reset_event.init();
        result.entries_arena = try NewArena.init_size(result.gpa, 4096);
        errdefer result.entries_arena.deinit();
        result.threads = try ArrayList(std.Thread).initCapacity(result.gpa, num_threads);
        errdefer result.threads.deinit();

        errdefer comptime unreachable;

        // NOTE: When `worker` gets called, `result.threads` is not yet completely populated.
        {
            var i: usize = 0;
            while (i < num_threads) : (i += 1) {
                result.threads.appendAssumeCapacity(std.Thread.spawn(
                    .{},
                    worker,
                    .{ result, i },
                ) catch @panic("std.Thread.spawn failed"));
            }
        }

        test_some_work(result);
        test_some_work_with_output(result);
        test_some_work_new(result);
    }

    /// Deinitialize the WorkQueue. If you want the complete all the work, call `finish_all_work`
    /// before calling `deinit`. The WorkQueue will not do anything to the entries that were
    /// canceled in the middle or that had yet to start. If they allocated resources and those need
    /// to be cleaned up, you need to do it after the work queue stops (after deinit is called). The
    /// entries have a pointer to this variable and they can check if they need to stop doing work
    /// if they take a long time. `WorkQueue.deinit` will only return after all entries in
    /// `entry_queue` that have started stop executing. For example: if when deinit is called there
    /// are 7 entries in `entry_queue` and 4 are active (inside worker.func), those 3 inactive ones
    /// will never execute and the WorkQueue will wait for the 4 active entries to exit. If you have
    /// an entry that takes a long time to complete, it should probably check if `shall_quit` is
    /// true and then cancel its work.
    pub fn deinit(work_queue: *WorkQueue) void {
        work_queue.shall_quit.x.store(true, .SeqCst);
        work_queue.reset_event.set();
        while (work_queue.threads.popOrNull()) |th| {
            th.join();
        }
        work_queue.threads.deinit();
        work_queue.reset_event.deinit();

        work_queue.entries_arena.deinit();
        print("WorkQueue.deinit\n", .{});
        work_queue.* = undefined;
    }

    pub fn finish_all_work(work_queue: *WorkQueue) void {
        // TODO: Instead of looping, send an event from the work threads.
        while (work_queue.total_added.load(.SeqCst) != work_queue.total_completed.load(.SeqCst)) {
            assert(!work_queue.shall_quit.x.load(.SeqCst));
            // std.SpinLock.yield();
        }
    }

    /// Return the number of entries that were added to the work queue and have yet to complete.
    pub fn get_num_entries_to_complete(work_queue: *WorkQueue) u64 {
        return work_queue.total_added.load(.SeqCst) -% work_queue.total_completed.load(.SeqCst);
    }

    /// Return the number of entries that were added to the work queue and have yet to start.
    pub fn get_num_entries_to_start(work_queue: *WorkQueue) u64 {
        return work_queue.total_added.load(.SeqCst) -% work_queue.total_started.load(.SeqCst);
    }

    pub fn add_work(work_queue: *WorkQueue, entry: Entry) void {
        assert(!work_queue.shall_quit.x.load(.SeqCst));
        const node = work_queue.free_entries.get() orelse blk: {
            // `NewArena.allocator.create` is not thread safe so I need a lock (in case of multiple producer).
            work_queue.free_entries.mutex.lock();
            defer work_queue.free_entries.mutex.unlock();
            break :blk work_queue.entries_arena.allocator().create(EntryQueue.Node) catch unreachable;
        };
        node.* = .{ .data = entry };
        _ = work_queue.total_added.fetchAdd(1, .SeqCst);
        work_queue.entry_queue.put(node);
        work_queue.reset_event.set();
    }

    const WorkContext = struct {
        worker_idx: usize,
        shall_quit: *AtomicBool,
    };
    const WorkerContext = struct { work_queue: *WorkQueue, worker_idx: usize };
    fn worker(work_queue: *WorkQueue, worker_idx: usize) void {
        // TODO: If I add only one entry, all threads will wake up when I only needed one worker to do so.
        while (!work_queue.shall_quit.x.load(.SeqCst)) {
            var entry: ?Entry = null;
            if (work_queue.entry_queue.get()) |node| {
                entry = node.data;
                work_queue.free_entries.put(node);
            }
            if (entry) |e| {
                // print("[WorkQueue] #{} Got entry\n", .{worker_idx});
                // Situation: There are 2 workers; 10 entries are added to the queue just before worker1 calls wait(); worker2 calls reset and goes to the start of the loop => worker1 will not handle any of the entries.
                // To fix that, I set reset_event again here, and worker1 would start doing them again.
                work_queue.reset_event.set();

                _ = work_queue.total_started.fetchAdd(1, .SeqCst);
                e.func(WorkContext{ .worker_idx = worker_idx, .shall_quit = &work_queue.shall_quit.x }, e.data);
                _ = work_queue.total_completed.fetchAdd(1, .SeqCst);
                continue;
            }

            // Situation: there are two worker threads; this is worker1; got no entry; someone called add_work() 4 times in the main thread; worker2 just called reset_event.reset(); => worker1 will wait() and will not handle the new work; worker2 will handle all of the 4 jobs. To fix that, I call reset_event.set() when I get an entry.

            // print("[WorkQueue] #{} --- reset_event.wait() called\n", .{ctx.worker_idx});
            work_queue.reset_event.wait();
            // print("[WorkQueue] #{} --- reset_event.wait() completed\n", .{ctx.worker_idx});
            {
                const num_to_start = work_queue.get_num_entries_to_start();
                // print("[WorkQueue] #{} --- num entries to start after reset_event.wait(): {}\n",
                //       .{ctx.worker_idx, num_to_start});
                // Do not `reset_event.reset()` if there is work to be done.
                if (num_to_start > 0) continue;
                // There is no work to be done so reset it.
            }
            // A new entry might have been added but we still reset it.
            work_queue.reset_event.reset();
        }
        // shall_quit is true: we might have called `reset_event.reset()` so set it again.
        work_queue.reset_event.set();
    }
};
