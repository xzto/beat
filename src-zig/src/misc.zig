const std = @import("std");
pub const debugPrint = std.debug.warn;
pub const assert = std.debug.assert;
const mem = std.mem;

// TODO: Rewrite this file

pub fn notImplemented() void {
    @setCold(true);
    @panic("Not implemented");
}

pub fn chopByDelimFromLeft(str: []const u8, delim: u8, trim_whitespace: bool) struct {
    lhs: []const u8,
    rhs: ?[]const u8,
} {
    for (str) |ch, i| {
        if (ch == delim) {
            if (trim_whitespace) {
                return .{
                    .lhs = trimWhitespace(str[0..i]),
                    .rhs = trimWhitespace(str[i + 1 .. str.len]),
                };
            } else {
                return .{
                    .lhs = str[0..i],
                    .rhs = str[i + 1 .. str.len],
                };
            }
        }
    }
    return .{ .lhs = str[0..], .rhs = null };
}

pub fn trimWhitespace(str: []const u8) []const u8 {
    return mem.trim(u8, str, " \t");
    // var start_nonblank = @as(usize, 0);
    // while (start_nonblank < str.len and std.ascii.isBlank(str[start_nonblank])) : (start_nonblank += 1) {}
    // if (start_nonblank == str.len)
    //     return str[0..0];
    // var last_nonblank = str.len - 1;
    // while (last_nonblank >= start_nonblank and std.ascii.isBlank(str[last_nonblank])) : (last_nonblank -= 1) {}
    // return str[start_nonblank .. last_nonblank + 1];
}

pub fn getLineTrimmed(remaining: *[]const u8) ?[]const u8 {
    while (true) {
        if (remaining.len <= 0) return null;
        const line_len = mem.indexOfScalar(u8, remaining.*, '\n') orelse remaining.len;
        var line = mem.trim(u8, remaining.*[0..line_len], " \t\r\n");
        const size_to_skip = if (line_len < remaining.len) line_len + 1 else line_len;
        assert(size_to_skip <= remaining.len);
        remaining.* = remaining.*[size_to_skip..remaining.len];
        if (line.len <= 0) continue; // skip blank line
        if (line[0] == '#' or line[0] == ';') continue; // comment
        return line;
    }
    return null;
}

pub fn getChoppedByDelim(remaining: *[]const u8, delim: u8, trim_whitespace: bool) ?[]const u8 {
    if (remaining.len <= 0) return null;
    const chop = chopByDelimFromLeft(remaining.*, delim, trim_whitespace);
    remaining.* = if (chop.rhs) |r| r else remaining.*[remaining.len..remaining.len];
    return if (chop.lhs.len <= 0) null else chop.lhs;
}

// The different from using std.fmt.parseInt directly is that the buffer might be `?` null.
// I also take a minimum and maximum value.
// useful for:
// var int: i32 = maybeStringToInt(i32, ini_section.maybeGetItem("key"), min, max) catch default_value;
// instead of
// var int: i32 = (if (ini_section.maybeGetItem) |item| maybeStringtoInt(i32, item, min, max) catch default_value else default_value)
// var int: i32 = (if (ini_section.maybeGetItem) |item| maybeStringtoInt(i32, item, min, max) else error.Asdf) catch default_value;
pub fn maybeStringToInt(comptime T: type, buf_maybe: ?[]const u8, radix: u8, min: T, max: T) !T {
    if (buf_maybe) |buf| {
        var x = try std.fmt.parseInt(T, buf, radix); // catch return null;
        return if (x >= min and x <= max) x else error.OutOfRange;
    } else return error.NoInput;
}

pub fn maybeStringToFloat(comptime T: type, buf_maybe: ?[]const u8, min: T, max: T) !T {
    if (buf_maybe) |buf| {
        var x = try std.fmt.parseFloat(T, buf); // catch return null;
        return if (x >= min and x <= max) x else error.OutOfRange;
    } else return error.NoInput;
}

pub fn arrayListAppendSliceNoRealloc(comptime T: type, array_list: anytype, slice: []const T) !void {
    if (array_list.items.len + slice.len <= array_list.capacity) {
        array_list.appendSliceAssumeCapacity(slice);
    }
    return error.WouldRealloc;
}
