const std = @import("std");
const c = @import("c.zig");
const game = @import("game.zig");

pub fn get_clipboard(memory: *game.GameMemory) []const u8 {
    if (memory.clipboard_from_previous_frame != null) {
        std.debug.print("get_clipboard twice\n", .{});
    }

    if (memory.clipboard_from_previous_frame == null) {
        var ptr = @ptrCast(?[*:0]const u8, c.SDL_GetClipboardText()) orelse
            @ptrCast([*:0]const u8, std.heap.c_allocator.alloc(u8, 0) catch @panic("oom"));
        memory.clipboard_from_previous_frame =
            std.mem.spanZ(@ptrCast([*:0]const u8, ptr));
    }
    return memory.clipboard_from_previous_frame.?;
}
pub fn get_core_count() u32 {
    return @intCast(u32, c.SDL_GetCPUCount());
}
