const std = @import("std");
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;

pub fn ActiveAndFreeList(comptime T: type) type {
    return struct {
        const Self = @This();
        // I use a doubly linked list because list.remove() is faster.
        const Node = std.TailQueue(T).Node;
        allocator: *Allocator,
        // A list of undefined T's.
        free_list: std.TailQueue(T),
        // A list of active T's.
        active_list: std.TailQueue(T),

        pub fn init(allocator: *Allocator) Self {
            return Self{
                .allocator = allocator,
                .active_list = .{},
                .free_list = .{},
            };
        }
        pub fn deinit(self: *Self) void {
            assert(self.active_list.first == null);
            while (self.free_list.popFirst()) |node| {
                self.allocator.destroy(node);
            }
            self.* = undefined;
        }
        pub fn remove(self: *Self, node: *Node) void {
            if (std.debug.runtime_safety) {
                var it: ?*const Node = node;
                while (it) |n| : (it = n.next) {
                    if (n == node) break;
                } else @panic("move_from_active_to_free: node_ptr is not in active_list");
            }
            self.active_list.remove(node);
            self.free_list.prepend(node);
        }
        pub fn add_one(self: *Self) !*Node {
            const node = self.free_list.popFirst() orelse try allocator.create(Node);
            self.active_list.prepend(node);
            return node;
        }
    };
}
