const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const ArrayList = std.ArrayList;
const ArrayListUnmanaged = std.ArrayListUnmanaged;
const Allocator = std.mem.Allocator;
const platform = @import("platform.zig");
const NewArena = @import("my_arena.zig").NewArena;
const WorkQueue = @import("work_queue.zig").WorkQueue;

fn CurrAndLast(comptime T: type) type {
    return struct {
        const Self = @This();
        curr: T,
        last: T,
        pub fn curr_to_last(self: *Self) void {
            self.last = self.curr;
        }
        pub fn update(self: *Self) void {
            self.curr.update(self.last);
        }
    };
}

pub const ButtonState = packed struct {
    down: bool = false,
    press: bool = false,
    release: bool = false,
    repeat: bool = false,

    pub fn is_press_or_repeat(self: ButtonState) bool {
        return self.press or self.repeat;
    }
    pub fn update(self: *ButtonState, last: ButtonState) void {
        // update self.state
        if (last.repeat) {
            self.repeat = false;
        }
        if (self.down) {
            if (!last.down) {
                self.release = false;
                self.press = true;
            } else {
                self.release = false;
                self.press = false;
            }
        } else {
            if (last.down) {
                self.release = true;
                self.press = false;
            } else {
                self.release = false;
                self.press = false;
            }
        }
        if (self.press or !self.down) {
            self.repeat = false;
        }
    }
};

pub const KeyboardMods = packed struct {
    shift: bool = false,
    ctrl: bool = false,
    alt: bool = false,
    win: bool = false,
    // TODO: any will be used for mappings
    // any   : bool,
    pub fn matches(self: KeyboardMods, other: KeyboardMods) bool {
        // TODO: any
        return std.meta.eql(self, other);
    }
};

// Any digital button.
pub const ButtonInput = struct {
    state: ButtonState,
    /// The keyboard mods at the time the event was received. If I press CTRL-A and then release CTRL but keep A pressed, A's ButtonInput's kbd_mods will still have CTRL. Maybe I should make a boolean to indicate that I changed keyboard mods before the button was released.
    kbd_mods_for_press_or_release: KeyboardMods,
    /// This is used for mouse input (for double click, `clicks == 2`). For
    /// keyboard and controllers (TODO) it will always be `1`.
    clicks: u32,
    /// The timestamp of the event.
    os_time_seconds: f64,
};

pub const MouseInput = struct {
    wheel_dont_use_me: i32,
    delta_wheel: i32, // TODO positive is up/down?
    // Position inside the window
    pos: @Vector(2, i32),
    delta_pos: @Vector(2, i32),

    pub fn update(self: *MouseInput, last: MouseInput) void {
        self.delta_wheel = self.wheel_dont_use_me -% last.wheel_dont_use_me;
        self.delta_pos = self.pos - last.pos;
    }
};

pub const SimpleKeysInput = struct {
    // TODO: Non-exhaustive BeatKeySimple.
    bis: [@enumToInt(BeatKeySimple._end_simple)]ButtonInput,
    pub fn update(self: *SimpleKeysInput, last: SimpleKeysInput) void {
        for (self.bis) |*b, idx| {
            b.state.update(last.bis[idx].state);
            if (b.state.press) {
                std.debug.print("press {}\n", .{@intToEnum(BeatKeySimple, @intCast(std.meta.Tag(BeatKeySimple), idx))});
            }
        }
    }
};

/// qwerty:  ASDF
/// colemak: ARST
/// - I don't actually know the internals of the operating system to know that this comment is actually accurate, but this comments assumes it is true.
/// If your keyboard (hardware) is qwerty but you use the colemak layout (software), when you press the key "R", the keyboard actually sends the code for "S" and the OS interprets it as an "R". SDL sends to your window both the "scan code" "S" and the "key sym" "R".
/// scancode - the code that represents the physical button on the keyboard - "S".
/// keysym - the symbol you get when you press that key - "R"
///
/// There's also IME which is a different thing.
/// The IME program chooses a basic keyboard layout and sends it to X11.
///
/// If IME is not active in the current window, X11 uses the basic keyboard
/// layout from the IME program to map scan codes to keysyms.
/// If IME is active in the current window, every time a key is pressed X11 sends it to the IME program. The IME program decides what to do with it and it might also send back to X11 many things: "keysym", "text composition" and "text input".
/// - keysym - same definition as before
/// - text composition - text being edited - example: "にほn"
/// - text input - finished text input - example: "日本"
///
/// The following is what happens on my machine.
/// If IME is active and you are in hankaku mode and type "nihon<SPACE><ENTER>" SDL will send you
/// keysym + scancode "n"
/// text input "n"
/// keysym + scancode "i"
/// text input "i"
/// keysym + scancode "h"
/// text input "h"
/// keysym + scancode "o"
/// text input "o"
/// keysym + scancode "n"
/// text input "n"
/// keysym + scancode "space"
/// text input "space"
/// keysym + scancode "enter"
/// If IME is active and you are in zenkaku mode and type "nihon<SPACE><ENTER>" SDL will send to you
/// text composition "n"
/// text composition "に"
/// text composition "にh"
/// text composition "にほ"
/// text composition "にほn"
/// text composition "日本"
/// text composition ""
/// text input "日本"
/// If IME is NOT active and you are in zenkaku mode and type "nihon<SPACE><ENTER>" SDL will send to you
/// keysym + scancode "n"
/// keysym + scancode "i"
/// keysym + scancode "h"
/// keysym + scancode "o"
/// keysym + scancode "n"
/// keysym + scancode "space"
/// keysym + scancode "enter"
///
///
/// I named these us_* because I need a simple name for them.
/// I might not want to do a cross platform enum like this and instead map OS scancodes or keysyms directly.
pub const BeatKeySimple = enum(u10) {
    invalid = 0,
    _start_keyboard,

    _start_mods,
    lshift,
    rshift,
    lctrl,
    rctrl,
    lalt,
    ralt,
    lwin,
    rwin,
    _end_mods,

    _start_ascii,
    us_space,
    us_a,
    us_b,
    us_c,
    us_d,
    us_e,
    us_f,
    us_g,
    us_h,
    us_i,
    us_j,
    us_k,
    us_l,
    us_m,
    us_n,
    us_o,
    us_p,
    us_q,
    us_r,
    us_s,
    us_t,
    us_u,
    us_v,
    us_w,
    us_x,
    us_y,
    us_z,
    us_0,
    us_1,
    us_2,
    us_3,
    us_4,
    us_5,
    us_6,
    us_7,
    us_8,
    us_9,
    us_apostrophe,
    us_comma,
    us_minus,
    us_dot,
    us_slash,
    us_semicolon,
    us_equals,
    us_lbracket,
    us_rbracket,
    _end_ascii_,

    us_backspace,
    us_backslash,
    us_nonusbackslash,
    us_enter,
    us_escape,
    us_grave,
    us_tab,
    us_capslock,

    us_f1,
    us_f2,
    us_f3,
    us_f4,
    us_f5,
    us_f6,
    us_f7,
    us_f8,
    us_f9,
    us_f10,
    us_f11,
    us_f12,
    us_f13,
    us_f14,
    us_f15,
    us_f16,
    us_f17,
    us_f18,
    us_f19,
    us_f20,
    us_f21,
    us_f22,
    us_f23,
    us_f24,
    us_printscreen,
    us_scrolllock,
    us_pause,
    us_insert,
    us_home,
    us_pageup,
    us_delete,
    us_end,
    us_pagedown,
    us_up,
    us_down,
    us_left,
    us_right,
    us_kp_numlock,
    us_kp_slash,
    us_kp_asterisk,
    us_kp_minus,
    us_kp_plus,
    us_kp_dot,
    us_kp_enter,
    us_kp_comma,
    us_kp_0,
    us_kp_00,
    us_kp_000,
    us_kp_1,
    us_kp_2,
    us_kp_3,
    us_kp_4,
    us_kp_5,
    us_kp_6,
    us_kp_7,
    us_kp_8,
    us_kp_9,
    us_thousandsseparator,
    us_international1,
    us_international2,
    us_international3,
    us_international4,
    us_international5,
    us_international6,
    us_international7,
    us_international8,
    us_international9,
    us_eraseeaze,
    // TODO: More SDL scancodes.
    // TODO: I might want to use pure Xlib/xcb because SDL doesn't give me every keysym from X.
    _end_keyboard,

    // TODO: After gamepad support is done and I make `const BeatKeyId = union(enum) { BeatKeySimpleId, MouseKeyId, GamepadKeyId }`, I won't need to duplicate the mouse state here.
    _start_mouse,
    mouse_left,
    mouse_middle,
    mouse_right,
    mouse_x1,
    mouse_x2,
    _end_mouse,

    _end_simple,
    // _, // TODO: Non-exhaustive BeatKeySimple. Instead of using `_end_simple` for `SimpleKeysInput.bis.len`, use `std.math.maxInt(std.meta.Tag(BeatKeySimple))`.
};

// This can probably go to platform.zig
pub const GameInput = struct {
    const Self = @This();

    pub fn init(permanent_allocator: Allocator) Self {
        const initial_dt = 1.0 / 60.0;
        // TODO: zig issue 2765
        var result = Self{
            .dt = initial_dt,
            .dt_frame = initial_dt,
            .dt_before_swap_window = initial_dt / 2.0,
            .dt_before_sleep = initial_dt / 2.0,
            .os_time_seconds_frame_start = initial_dt,
            .simple_keys = std.mem.zeroInit(CurrAndLast(SimpleKeysInput), .{}),
            .mouse = std.mem.zeroInit(CurrAndLast(MouseInput), .{}),
            .new_text_input = ArrayListUnmanaged(u8).initCapacity(permanent_allocator, text_input_buf_size) catch @panic("early oom"),
            .curr_text_composition = ArrayListUnmanaged(u8).initCapacity(permanent_allocator, text_input_buf_size) catch @panic("early oom"),
        };
        return result;
    }
    pub const text_input_buf_size: usize = 16384;

    // for simulation
    dt: f64,
    // for debug stuff
    dt_before_swap_window: f64,
    dt_before_sleep: f64,
    dt_frame: f64,

    os_time_seconds_frame_start: f64,

    simple_keys: CurrAndLast(SimpleKeysInput),
    mouse: CurrAndLast(MouseInput),

    // These can not allocate
    new_text_input: ArrayListUnmanaged(u8),
    curr_text_composition: ArrayListUnmanaged(u8),
};

pub const GameState = struct {
    is_initialized: bool,
    arena: NewArena,
};

// This can probably go to platform.zig
// NOTE: Only the main thread can have pointers that point to memory that will be invalid when GameMemory.deinit() is called. If I need to pass stuff around threads I'll make a copy.
pub const GameMemory = struct {
    const Self: type = @This();

    gpa: Allocator,
    permanent_arena: NewArena,
    gs: GameState,
    clipboard_from_previous_frame: ?[]const u8 = null, // std.heap.c_allocator
    low_priority_work_queue: WorkQueue,

    pub fn init(gpa: Allocator) *Self {
        // TODO: Move this SDL code out of here.
        const core_count: u32 = platform.get_core_count();
        print("[GameMemory.init] CPU has {} cores\n", .{core_count});

        // TODO: zig issue 2765
        const memory = gpa.create(Self) catch unreachable;
        memory.* = Self{
            .gpa = gpa,
            .permanent_arena = NewArena.init_size(gpa, 1 * 1024 * 1024) catch unreachable,
            .gs = undefined,
            .low_priority_work_queue = undefined,
        };
        memory.gs.is_initialized = false;
        memory.low_priority_work_queue.init(gpa, if (core_count >= 2) core_count - 1 else 1) catch @panic("Work queue failed to start");
        return memory;
    }
    pub fn deinit(self: *Self) void {
        // Only deinitialize what is strictly necessary. The OS will free most of the stuff.
        // low_priority_work_queue: The threads will quit when main() exits.
        if (true) {
            // This will wait for all jobs to finish.
            self.low_priority_work_queue.deinit();

            if (self.gs.is_initialized) {
                self.gs.arena.deinit();
            }
            self.permanent_arena.deinit();
            if (self.clipboard_from_previous_frame) |clip| {
                std.heap.c_allocator.free(clip);
            }
        }
        print("GameMemory.deinit\n", .{});
    }
};
