const std = @import("std");
const c = @import("c.zig");
const print = std.debug.print;
const assert = std.debug.assert;
const game = @import("game.zig");
const audio = @import("audio.zig");
const platform = @import("platform.zig");
const misc = @import("misc.zig");
const WorkQueue = @import("work_queue.zig").WorkQueue;
const NewArena = @import("my_arena.zig").NewArena;

pub const global_running = struct {
    var x: bool = true;
    pub fn get() bool {
        return @ptrCast(*volatile bool, &x).*;
    }
    pub fn set(new_value: bool) void {
        if (!new_value and get()) {
            print("Global running set to false! Quitting!\n", .{});
        }
        @ptrCast(*volatile bool, &x).* = new_value;
    }
};

pub const PRINT_SHIT_EVERY_SECOND = false;
pub var global_frame_count: u64 = 1;
pub fn global_frame_count_once_in_a_while() bool {
    if (!PRINT_SHIT_EVERY_SECOND) return false;
    return global_frame_count % 144 == 0;
}

pub fn timer_complete_interval(timer: *std.time.Timer, interval_goal_ns: u64, is_important: bool) u64 {
    var now = timer.read();
    while (now < interval_goal_ns) {
        if (false and is_important) {
            // std.time.sleep(0);
            // asm volatile ("pause\n" ** 16);
        } else {
            std.time.sleep(interval_goal_ns - now);
        }
        now = timer.read();
    }
    return timer.lap();
}

pub const frame_limiter = struct {
    var is_vsync_adaptive_available_: ?bool = null;
    var current: Mode = undefined;
    pub const Mode = union(enum) {
        unlimited,
        limited: f64,
        vsync,
        vsync_adaptive,
    };

    pub fn init() void {
        _ = is_vsync_adaptive_available(); // initialize the variable
        set_vsync_adaptive_or_fallback(.vsync);
    }
    pub fn set(x: Mode) void {
        current = x;
        switch (x) {
            .unlimited => _ = c.SDL_GL_SetSwapInterval(0),
            .limited => _ = c.SDL_GL_SetSwapInterval(0),
            .vsync => _ = c.SDL_GL_SetSwapInterval(1),
            .vsync_adaptive => set_vsync_adaptive_or_fallback(.vsync),
        }
    }
    pub fn get() Mode {
        return current;
    }
    pub fn is_vsync_adaptive_available() bool {
        if (is_vsync_adaptive_available_) |x| {
            return x;
        }
        const prev = current;
        defer set(prev);
        is_vsync_adaptive_available_ = (c.SDL_GL_SetSwapInterval(-1) == 0);
        return is_vsync_adaptive_available_.?;
    }
    pub fn set_vsync_adaptive_or_fallback(fallback: Mode) void {
        if (is_vsync_adaptive_available()) {
            const a = c.SDL_GL_SetSwapInterval(-1);
            assert(a == 0);
            current = .vsync_adaptive;
        } else {
            set(fallback);
        }
    }
};

fn keyboard_mods_from_sdl_mods(sdlmods: u32) game.KeyboardMods {
    return game.KeyboardMods{
        .shift = ((sdlmods & @as(u32, c.KMOD_SHIFT)) != 0),
        .ctrl = ((sdlmods & @as(u32, c.KMOD_CTRL)) != 0),
        .alt = ((sdlmods & @as(u32, c.KMOD_ALT)) != 0),
        .win = ((sdlmods & @as(u32, c.KMOD_GUI)) != 0),
    };
}

pub fn main() void {
    defer print("main() quit\n", .{});
    var the_gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = the_gpa.deinit();
    const gpa = the_gpa.allocator();

    if (c.SDL_Init(c.SDL_INIT_VIDEO) != 0) {
        print("SDL_Init failed\n", .{});
        return;
    }
    defer c.SDL_Quit();

    var memory = game.GameMemory.init(gpa);
    defer gpa.destroy(memory);
    defer memory.deinit();

    audio.init(gpa) catch {
        print("audio.init() failed\n", .{});
        return;
    };
    defer audio.deinit();

    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_PROFILE_MASK, c.SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_MINOR_VERSION, 3);

    // TODO: Don't use SDL_GL_MULTISAMPLESAMPLES
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_MULTISAMPLEBUFFERS, 1);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_MULTISAMPLESAMPLES, 16);

    var window_size = @Vector(2, i32){ 1280, 720 };
    const window = c.SDL_CreateWindow(
        "My Game Window",
        c.SDL_WINDOWPOS_UNDEFINED,
        c.SDL_WINDOWPOS_UNDEFINED,
        window_size[0],
        window_size[1],
        c.SDL_WINDOW_OPENGL,
    ) orelse {
        print("SDL_CreateWindow failed: {s}\n", .{@ptrCast([*:0]const u8, c.SDL_GetError())});
        return;
    };
    defer c.SDL_DestroyWindow(window);

    var glcontext = c.SDL_GL_CreateContext(window);
    defer c.SDL_GL_DeleteContext(glcontext);

    _ = c.SDL_GL_MakeCurrent(window, glcontext);
    if (c.SDL_GL_LoadLibrary(null) != 0) {
        print("SDL_GL_LoadLibrary failed\n", .{});
        return;
    }
    if (c.gladLoadGLLoader(@ptrCast(c.GLADloadproc, c.SDL_GL_GetProcAddress)) == 0) {
        print("gladLoadGLLoader failed\n", .{});
        return;
    }
    c.SDL_StopTextInput();
    print("-------------- init sdl ok --------------\n", .{});
    // If I don't do this and the first frame takes a long time, the window will be ugly.
    c.glClearColor(0.0, 0.9, 0.9, 1.0);
    c.glClear(c.GL_COLOR_BUFFER_BIT);
    c.SDL_GL_SwapWindow(window);

    frame_limiter.init();
    print("Frame limiter mode: {}\n", .{frame_limiter.get()});

    var window_has_focus = true;
    var timer_end = std.time.Timer.start() catch @panic("early panic: std.time.Timer.start");
    var dt: f64 = 1 / 60.0;

    var gin = game.GameInput.init(memory.permanent_arena.allocator());

    const mapped_keyboard_scancodes = comptime x: {
        @setEvalBranchQuota(2000);
        var arr: [c.SDL_NUM_SCANCODES]game.BeatKeySimple = undefined;
        for (arr) |*arr_val, i| arr_val.* = switch (i) {
            // else => unreachable, // TODO: More scan codes.
            else => .invalid,
            // else => x: { assert(i != 0); break :x @intToEnum(game.BeatKeySimple, std.math.cast(std.meta.Tag(game.BeatKeySimple), @enumToInt(game.BeatKeySimple._end_simple) + i) catch @enumToInt(game.BeatKeySimple.invalid)); }, // TODO: Non-exhaustive BeatKeySimple.

            c.SDL_SCANCODE_UNKNOWN => .invalid,

            c.SDL_SCANCODE_LSHIFT => .lshift,
            c.SDL_SCANCODE_RSHIFT => .rshift,
            c.SDL_SCANCODE_LCTRL => .lctrl,
            c.SDL_SCANCODE_RCTRL => .rctrl,
            c.SDL_SCANCODE_LALT => .lalt,
            c.SDL_SCANCODE_RALT => .ralt,
            c.SDL_SCANCODE_LGUI => .lwin,
            c.SDL_SCANCODE_RGUI => .rwin,

            c.SDL_SCANCODE_SPACE => .us_space,
            c.SDL_SCANCODE_A => .us_a,
            c.SDL_SCANCODE_B => .us_b,
            c.SDL_SCANCODE_C => .us_c,
            c.SDL_SCANCODE_D => .us_d,
            c.SDL_SCANCODE_E => .us_e,
            c.SDL_SCANCODE_F => .us_f,
            c.SDL_SCANCODE_G => .us_g,
            c.SDL_SCANCODE_H => .us_h,
            c.SDL_SCANCODE_I => .us_i,
            c.SDL_SCANCODE_J => .us_j,
            c.SDL_SCANCODE_K => .us_k,
            c.SDL_SCANCODE_L => .us_l,
            c.SDL_SCANCODE_M => .us_m,
            c.SDL_SCANCODE_N => .us_n,
            c.SDL_SCANCODE_O => .us_o,
            c.SDL_SCANCODE_P => .us_p,
            c.SDL_SCANCODE_Q => .us_q,
            c.SDL_SCANCODE_R => .us_r,
            c.SDL_SCANCODE_S => .us_s,
            c.SDL_SCANCODE_T => .us_t,
            c.SDL_SCANCODE_U => .us_u,
            c.SDL_SCANCODE_V => .us_v,
            c.SDL_SCANCODE_W => .us_w,
            c.SDL_SCANCODE_X => .us_x,
            c.SDL_SCANCODE_Y => .us_y,
            c.SDL_SCANCODE_Z => .us_z,
            c.SDL_SCANCODE_0 => .us_0,
            c.SDL_SCANCODE_1 => .us_1,
            c.SDL_SCANCODE_2 => .us_2,
            c.SDL_SCANCODE_3 => .us_3,
            c.SDL_SCANCODE_4 => .us_4,
            c.SDL_SCANCODE_5 => .us_5,
            c.SDL_SCANCODE_6 => .us_6,
            c.SDL_SCANCODE_7 => .us_7,
            c.SDL_SCANCODE_8 => .us_8,
            c.SDL_SCANCODE_9 => .us_9,
            c.SDL_SCANCODE_APOSTROPHE => .us_apostrophe,
            c.SDL_SCANCODE_COMMA => .us_comma,
            c.SDL_SCANCODE_MINUS => .us_minus,
            c.SDL_SCANCODE_PERIOD => .us_dot,
            c.SDL_SCANCODE_SLASH => .us_slash,
            c.SDL_SCANCODE_SEMICOLON => .us_semicolon,
            c.SDL_SCANCODE_EQUALS => .us_equals,
            c.SDL_SCANCODE_LEFTBRACKET => .us_lbracket,
            c.SDL_SCANCODE_RIGHTBRACKET => .us_rbracket,

            c.SDL_SCANCODE_BACKSPACE => .us_backspace,
            c.SDL_SCANCODE_BACKSLASH => .us_backslash,
            c.SDL_SCANCODE_NONUSBACKSLASH => .us_nonusbackslash,
            c.SDL_SCANCODE_RETURN => .us_enter,
            c.SDL_SCANCODE_ESCAPE => .us_escape,
            c.SDL_SCANCODE_GRAVE => .us_grave,
            c.SDL_SCANCODE_TAB => .us_tab,
            c.SDL_SCANCODE_CAPSLOCK => .us_capslock,

            c.SDL_SCANCODE_F1 => .us_f1,
            c.SDL_SCANCODE_F2 => .us_f2,
            c.SDL_SCANCODE_F3 => .us_f3,
            c.SDL_SCANCODE_F4 => .us_f4,
            c.SDL_SCANCODE_F5 => .us_f5,
            c.SDL_SCANCODE_F6 => .us_f6,
            c.SDL_SCANCODE_F7 => .us_f7,
            c.SDL_SCANCODE_F8 => .us_f8,
            c.SDL_SCANCODE_F9 => .us_f9,
            c.SDL_SCANCODE_F10 => .us_f10,
            c.SDL_SCANCODE_F11 => .us_f11,
            c.SDL_SCANCODE_F12 => .us_f12,
            c.SDL_SCANCODE_F13 => .us_f13,
            c.SDL_SCANCODE_F14 => .us_f14,
            c.SDL_SCANCODE_F15 => .us_f15,
            c.SDL_SCANCODE_F16 => .us_f16,
            c.SDL_SCANCODE_F17 => .us_f17,
            c.SDL_SCANCODE_F18 => .us_f18,
            c.SDL_SCANCODE_F19 => .us_f19,
            c.SDL_SCANCODE_F20 => .us_f20,
            c.SDL_SCANCODE_F21 => .us_f21,
            c.SDL_SCANCODE_F22 => .us_f22,
            c.SDL_SCANCODE_F23 => .us_f23,
            c.SDL_SCANCODE_F24 => .us_f24,
            c.SDL_SCANCODE_PRINTSCREEN => .us_printscreen,
            c.SDL_SCANCODE_SCROLLLOCK => .us_scrolllock,
            c.SDL_SCANCODE_PAUSE => .us_pause,
            c.SDL_SCANCODE_INSERT => .us_insert,
            c.SDL_SCANCODE_HOME => .us_home,
            c.SDL_SCANCODE_PAGEUP => .us_pageup,
            c.SDL_SCANCODE_DELETE => .us_delete,
            c.SDL_SCANCODE_END => .us_end,
            c.SDL_SCANCODE_PAGEDOWN => .us_pagedown,
            c.SDL_SCANCODE_UP => .us_up,
            c.SDL_SCANCODE_DOWN => .us_down,
            c.SDL_SCANCODE_LEFT => .us_left,
            c.SDL_SCANCODE_RIGHT => .us_right,
            c.SDL_SCANCODE_NUMLOCKCLEAR => .us_kp_numlock,
            c.SDL_SCANCODE_KP_DIVIDE => .us_kp_slash,
            c.SDL_SCANCODE_KP_MULTIPLY => .us_kp_asterisk,
            c.SDL_SCANCODE_KP_MINUS => .us_kp_minus,
            c.SDL_SCANCODE_KP_PLUS => .us_kp_plus,
            c.SDL_SCANCODE_KP_PERIOD => .us_kp_dot,
            c.SDL_SCANCODE_KP_ENTER => .us_kp_enter,
            c.SDL_SCANCODE_KP_COMMA => .us_kp_comma,
            c.SDL_SCANCODE_KP_0 => .us_kp_0,
            c.SDL_SCANCODE_KP_00 => .us_kp_00,
            c.SDL_SCANCODE_KP_000 => .us_kp_000,
            c.SDL_SCANCODE_KP_1 => .us_kp_1,
            c.SDL_SCANCODE_KP_2 => .us_kp_2,
            c.SDL_SCANCODE_KP_3 => .us_kp_3,
            c.SDL_SCANCODE_KP_4 => .us_kp_4,
            c.SDL_SCANCODE_KP_5 => .us_kp_5,
            c.SDL_SCANCODE_KP_6 => .us_kp_6,
            c.SDL_SCANCODE_KP_7 => .us_kp_7,
            c.SDL_SCANCODE_KP_8 => .us_kp_8,
            c.SDL_SCANCODE_KP_9 => .us_kp_9,
            c.SDL_SCANCODE_THOUSANDSSEPARATOR => .us_thousandsseparator,
            c.SDL_SCANCODE_INTERNATIONAL1 => .us_international1,
            c.SDL_SCANCODE_INTERNATIONAL2 => .us_international2,
            c.SDL_SCANCODE_INTERNATIONAL3 => .us_international3,
            c.SDL_SCANCODE_INTERNATIONAL4 => .us_international4,
            c.SDL_SCANCODE_INTERNATIONAL5 => .us_international5,
            c.SDL_SCANCODE_INTERNATIONAL6 => .us_international6,
            c.SDL_SCANCODE_INTERNATIONAL7 => .us_international7,
            c.SDL_SCANCODE_INTERNATIONAL8 => .us_international8,
            c.SDL_SCANCODE_INTERNATIONAL9 => .us_international9,
            c.SDL_SCANCODE_ALTERASE => .us_eraseeaze,
        };
        break :x arr;
    };

    while (global_running.get()) : (global_frame_count += 1) {
        {
            gin.mouse.curr_to_last();
            gin.simple_keys.curr_to_last();
            gin.new_text_input.shrinkRetainingCapacity(0);
        }

        {
            if (memory.clipboard_from_previous_frame) |clip| {
                // std.heap.c_allocator.free(clip);
                c.SDL_free(@intToPtr(*anyopaque, @ptrToInt(clip.ptr)));
                memory.clipboard_from_previous_frame = null;
            }
        }

        {
            var event: c.SDL_Event = undefined;
            events: while (c.SDL_PollEvent(&event) != 0) {
                const event_type: c.SDL_EventType = @bitCast(c.SDL_EventType, event.type);
                switch (event_type) {
                    c.SDL_QUIT => global_running.set(false),
                    c.SDL_WINDOWEVENT => {
                        switch (event.window.event) {
                            c.SDL_WINDOWEVENT_CLOSE => global_running.set(false),
                            c.SDL_WINDOWEVENT_FOCUS_GAINED => window_has_focus = true,
                            c.SDL_WINDOWEVENT_FOCUS_LOST => window_has_focus = false,
                            else => {},
                        }
                    },
                    c.SDL_MOUSEMOTION => {
                        gin.mouse.curr.pos = .{ event.motion.x, event.motion.y };
                    },
                    c.SDL_MOUSEBUTTONUP, c.SDL_MOUSEBUTTONDOWN => {
                        const down = event_type == c.SDL_MOUSEBUTTONDOWN;
                        const clicks = event.button.clicks;
                        const mybutton: game.BeatKeySimple = switch (event.button.button) {
                            c.SDL_BUTTON_LEFT => .mouse_left,
                            c.SDL_BUTTON_MIDDLE => .mouse_middle,
                            c.SDL_BUTTON_RIGHT => .mouse_right,
                            c.SDL_BUTTON_X1 => .mouse_x1,
                            c.SDL_BUTTON_X2 => .mouse_x2,
                            else => continue :events,
                        };
                        const down_up_str = if (down) "down" else "up";
                        print("Mouse '{}' {s} times {}\n", .{ mybutton, down_up_str, clicks });

                        const bi = &gin.simple_keys.curr.bis[@enumToInt(mybutton)];

                        const mymods = keyboard_mods_from_sdl_mods(c.SDL_GetModState());
                        bi.* = .{
                            .state = .{ .down = down },
                            .os_time_seconds = gin.os_time_seconds_frame_start,
                            .kbd_mods_for_press_or_release = mymods,
                            .clicks = event.button.clicks,
                        };
                    },
                    c.SDL_KEYUP, c.SDL_KEYDOWN => {
                        const mymods = keyboard_mods_from_sdl_mods(event.key.keysym.mod);
                        const scancode = event.key.keysym.scancode;
                        // Hardcode Alt-F4
                        if (event.key.keysym.scancode == c.SDL_SCANCODE_F4 and event_type == c.SDL_KEYDOWN and mymods.alt) {
                            global_running.set(false);
                        }
                        const beatkeysimple = mapped_keyboard_scancodes[@intCast(usize, scancode)];
                        if (beatkeysimple != .invalid) {
                            const bi = &gin.simple_keys.curr.bis[@enumToInt(beatkeysimple)];
                            if (event_type == c.SDL_KEYDOWN) {
                                if (event.key.repeat != 0) {
                                    if (mymods.matches(bi.kbd_mods_for_press_or_release)) {
                                        bi.state.repeat = true;
                                        print("repeat {}\n", .{beatkeysimple});
                                    }
                                } else {
                                    bi.* = .{
                                        .state = .{ .down = true },
                                        .kbd_mods_for_press_or_release = mymods,
                                        .os_time_seconds = gin.os_time_seconds_frame_start,
                                        .clicks = 1,
                                    };
                                }
                            } else {
                                assert(event_type == c.SDL_KEYUP);
                                bi.* = .{
                                    .state = .{ .down = false },
                                    .kbd_mods_for_press_or_release = mymods,
                                    .os_time_seconds = gin.os_time_seconds_frame_start,
                                    .clicks = 1,
                                };
                            }
                        } else {
                            const H = struct {
                                var last_not_found_scancode: c.SDL_Scancode = c.SDL_SCANCODE_UNKNOWN;
                            };
                            if (scancode != H.last_not_found_scancode) {
                                H.last_not_found_scancode = scancode;
                                // TODO: write to a log file
                                print("XXX not mapped scancode: {} --------\n", .{scancode});
                            }
                        }
                    },
                    c.SDL_MOUSEWHEEL => {
                        const flip = if (event.wheel.direction == c.SDL_MOUSEWHEEL_FLIPPED) @as(i32, -1) else 1;
                        const new_delta: i32 = event.wheel.y * flip;
                        gin.mouse.curr.wheel_dont_use_me +%= new_delta;
                    },
                    c.SDL_TEXTINPUT => {
                        const ev_text = &event.text;
                        const text_str: []const u8 = ev_text.text[0 .. std.mem.indexOfScalar(u8, &ev_text.text, 0) orelse ev_text.text.len];
                        assert(text_str.len <= ev_text.text.len);
                        print("Got text input: `{s}`\n", .{text_str});
                        misc.arrayListAppendSliceNoRealloc(u8, &gin.new_text_input, text_str) catch {
                            print("Text input is too large to fit inside gin.new_text_input\n", .{});
                        };
                    },
                    c.SDL_TEXTEDITING => {
                        const ev_edit = &event.edit;
                        const edit_str: []const u8 = ev_edit.text[0 .. std.mem.indexOfScalar(u8, &ev_edit.text, 0) orelse ev_edit.text.len];
                        assert(edit_str.len <= ev_edit.text.len);
                        print("Got text editing: `{s}` start {} count {} len bytes {}\n", .{ edit_str, ev_edit.start, ev_edit.length, edit_str.len });
                        if (ev_edit.start == 0) {
                            gin.curr_text_composition.shrinkRetainingCapacity(0);
                        }
                        misc.arrayListAppendSliceNoRealloc(u8, &gin.curr_text_composition, edit_str) catch {
                            print("Text editing is too large to fit inside gin.curr_text_composition\n", .{});
                        };
                        print("gin.curr_text_composition: {s}\n", .{gin.curr_text_composition.items});
                    },
                    // TODO .SDL_KEYMAPCHANGED
                    else => {
                        print("Unknown sdl event: {}\n", .{@as(u32, event.type)});
                    },
                }
            }
            c.SDL_GetWindowSize(window, &window_size[0], &window_size[1]);
        }

        {
            gin.mouse.update();
            gin.simple_keys.update();
        }

        if (false and global_frame_count_once_in_a_while()) {
            const clipboard = platform.get_clipboard(&memory);
            print("Clipboard: `{}`\n", .{clipboard});
        }
        if (true) {
            if (gin.simple_keys.curr.bis[@enumToInt(game.BeatKeySimple.mouse_left)].state.press) {
                c.SDL_StartTextInput();
            }
            if (gin.simple_keys.curr.bis[@enumToInt(game.BeatKeySimple.mouse_right)].state.press) {
                c.SDL_StopTextInput();
            }
        }

        if (!true and gin.simple_keys.curr.bis[@enumToInt(game.BeatKeySimple.us_p)].state.press) {
            const H = struct {
                var num: u32 = 0;
            };
            const work = gpa.create(WorkQueue.Work_TestNoOutput_) catch unreachable; // leak
            work.init(memory.low_priority_work_queue.gpa, H.num);
            H.num += 1;
            memory.low_priority_work_queue.add_work(work.entry());
        }

        {
            // game update and render
        }

        const dt_ns_before_swap_window = timer_end.read();
        c.SDL_GL_SwapWindow(window);
        const dt_ns_before_sleep = timer_end.read();
        const do_frame_limiter = (frame_limiter.get() == .limited) or !window_has_focus;
        const dt_ns = blk: {
            if (do_frame_limiter) {
                const target_dt_seconds: f64 = if (!window_has_focus) 1.0 / 12.0 else frame_limiter.get().limited;
                break :blk timer_complete_interval(
                    &timer_end,
                    @floatToInt(u64, target_dt_seconds * std.time.ns_per_s),
                    window_has_focus,
                );
            } else {
                break :blk timer_end.lap();
            }
        };
        dt = @intToFloat(f64, dt_ns) / std.time.ns_per_s;
        assert(dt_ns > 0 and dt > 0);
        gin.dt = dt;
        gin.dt_before_swap_window = @intToFloat(f64, dt_ns_before_swap_window) / std.time.ns_per_s;
        gin.dt_before_sleep = @intToFloat(f64, dt_ns_before_sleep) / std.time.ns_per_s;
        gin.dt_frame = dt;
        gin.os_time_seconds_frame_start += gin.dt_frame; // TODO Not this
        if (global_frame_count_once_in_a_while()) {
            const round1000 = struct {
                fn h(x: f64) f64 {
                    const n = 1000;
                    return @round(n * x) / n;
                }
            }.h;
            print("Frame #{}: dt {d}ms | fps {d} | " ++
                "dt before swap window {d}ms | dt before sleep {d}ms | sleeped {d}ms\n", .{
                global_frame_count,
                round1000(1000 * dt),
                @floatToInt(u64, @round(1 / dt)),
                round1000(1000 * gin.dt_before_swap_window),
                round1000(1000 * gin.dt_before_sleep),
                round1000(1000 * (gin.dt_frame - gin.dt_before_sleep)),
            });
        }
    }

    if (false) {
        std.time.sleep(std.time.ns_per_ms * 500);
        print("Temporary std.process.exit\n", .{});
        std.process.exit(0);
    }
}
