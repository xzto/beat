pub usingnamespace @cImport({
    @cInclude("glad/glad.h");
    @cInclude("SDL2/SDL.h");
    @cInclude("minimp3/minimp3_ex.h");
    @cInclude("stb_truetype/stb_truetype.h");
    @cInclude("portaudio.h");
});

// pub const SDL_WINDOWPOS_UNDEFINED = @bitCast(c_int, c.SDL_WINDOWPOS_UNDEFINED_MASK);
