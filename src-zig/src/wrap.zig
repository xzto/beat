/// Wraps a T so that I can't use that variable directly. Useful when I want to
/// enforce every load and store to be atomic. (std.AtomicInt doesn't have
/// cmpxchg)
pub fn Wrap(comptime T_: type) type {
    return struct {
        const Self = @This();
        pub const T = T_;
        unwrap: T,
        pub fn init(val: T) Self {
            return Self{ .unwrap = val };
        }
    };
}
