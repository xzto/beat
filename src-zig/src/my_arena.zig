const std = @import("std");
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;

/// A liner allocator. It creates new blocks automatically. The block size increases exponentially.
/// This is very similar to `std.heap.ArenaAllocator` except that it has `Save` and `init_size()`
pub const NewArena = struct {
    /// Each block
    pub const BlockData = struct {
        // TODO: `size` and `idx` should skip `@sizeOf(BlockNode)` and talk about the memory _after_ the `BlockNode`. I don't want to deal with this right now.
        size: usize,
        // TODO: Now that I think about it, I don't need `idx` to be inside `BlockData`. It can be inside `State`.
        idx: usize,
        /// If `is_static`, the block is _not_ managed by `child_allocator`.
        is_static: bool,
    };
    pub const BlockList = std.SinglyLinkedList(BlockData);
    pub const BlockNode = BlockList.Node;

    const has_extra_safety_checks = std.debug.runtime_safety;
    /// Extra state to perform safety checks.
    const ExtraSafetyState = if (!has_extra_safety_checks) struct {} else struct {
        /// Each time `NewArena.save` is called, this value increases. Each time `forget` or `restore` is called it decreases.
        saved_count: u32 = 0,
        /// `saved_block_start` and `saved_block_idx` are used to catch a reallocation with memory that was created before the current save.
        /// When `saved_count > 0`, this is the start of the block (the block ptr) when `NewArena.save` was called.
        saved_block_start: usize = 0,
        /// When `saved_count > 0`, this is the idx of the block when the save happened.
        saved_block_idx: usize = 0,
    };

    pub const State = struct {
        blocks: BlockList = .{},
        extra_safety: ExtraSafetyState = .{},
        pub fn promote(state: State, child_allocator: Allocator) NewArena {
            return .{ .child_allocator = child_allocator, .state = state };
        }
    };

    child_allocator: Allocator,
    state: State,

    pub fn allocator(arena: *NewArena) Allocator {
        return Allocator.init(arena, alloc, resize, free);
    }

    /// Initialize a `NewArena`.
    pub fn init(child_allocator: Allocator) NewArena {
        return (State{}).promote(child_allocator);
    }

    /// Initialize a `NewArena` with an `initial_size`.
    pub fn init_size(child_allocator: Allocator, initial_size: usize) !NewArena {
        var arena = init(child_allocator);
        if (initial_size > 0) {
            arena.state.blocks.first = try create_block(child_allocator, initial_size);
        }
        return arena;
    }

    /// Initialize a `NewArena` with a static buffer.
    pub fn init_static_buffer(child_allocator: Allocator, buf: []u8) NewArena {
        var arena = init(child_allocator);
        const buf_start = @ptrToInt(buf.ptr);
        const buf_end = buf_start + buf.len;
        const aligned_buf_start = std.mem.alignForward(buf_start, @alignOf(BlockNode));
        const aligned_buf_len = buf_end - aligned_buf_start;
        assert(aligned_buf_len > @sizeOf(BlockNode));
        const aligned_buf = @alignCast(@alignOf(BlockNode), buf[aligned_buf_start - buf_start ..][0..aligned_buf_len]);
        arena.state.blocks.first = init_block_from_buf(aligned_buf, .{ .is_static = true });
        return arena;
    }

    const BlockFlags = packed struct { is_static: bool };
    fn init_block_from_buf(buf: []align(@alignOf(BlockNode)) u8, flags: BlockFlags) *BlockNode {
        const node = @ptrCast(*BlockNode, buf);
        node.* = .{ .next = null, .data = .{ .idx = @sizeOf(BlockNode), .size = buf.len, .is_static = flags.is_static } };
        return node;
    }

    /// Deinitialize a `NewArena`.
    pub fn deinit(arena: *NewArena) void {
        if (has_extra_safety_checks) {
            assert(arena.state.extra_safety.saved_count == 0);
        }
        while (arena.state.blocks.popFirst()) |block| {
            destroy_block(arena.child_allocator, block);
        }
        arena.* = undefined;
    }

    /// Create a new block with a minimum size of `size`. Destroy it with `destroy_block`.
    fn create_block(child_allocator: Allocator, size: usize) !*BlockNode {
        // NOTE: The child allocator might return more than I requested.
        const requested_size = std.math.max(size, @sizeOf(BlockNode) + 64);
        const mem = try child_allocator.allocAdvanced(u8, @alignOf(BlockNode), requested_size, .at_least);
        assert(mem.len > @sizeOf(BlockNode));
        const block = init_block_from_buf(mem, .{ .is_static = false });
        return block;
    }

    /// Destroy a block previously created with `create_block`.
    fn destroy_block(child_allocator: Allocator, block: *BlockNode) void {
        if (!block.data.is_static)
            child_allocator.free(get_block_owned_memory(block));
    }

    /// Try to resize the block instead of creating a new block. Return true on success.
    fn block_request_remaining_size(child_allocator: Allocator, block: *BlockNode, requested_remaining_size: usize) bool {
        // errdefer std.debug.print("--- NewArena resize block failure!\n", .{});
        const remaining_size = block.data.size - block.data.idx;
        if (remaining_size >= requested_remaining_size)
            return true;
        if (block.data.is_static)
            return false;

        const new_block_requested_size = std.math.mul(usize, 2, std.math.add(usize, requested_remaining_size, block.data.size) catch return false) catch return false;
        // There is no Allocator.resizeAdvanced so I have to call resizeFn directly.
        const new_block_actual_size = child_allocator.rawResize(
            get_block_owned_memory(block),
            @alignOf(BlockNode),
            new_block_requested_size,
            1, // len_align=1 === .at_least
            @returnAddress(),
        ) orelse return false;
        // std.debug.print("--- NewArena resize block success\n", .{});
        assert(new_block_actual_size >= new_block_requested_size);
        block.data.size = new_block_actual_size;
        return true;
    }

    /// Get the owned memory from a `BlockNode`.
    fn get_block_owned_memory(block: *BlockNode) []align(@alignOf(BlockNode)) u8 {
        return @ptrCast([*]u8, block)[0..block.data.size];
    }

    /// Save a state of a `NewArena`. Restore it with `Save.restore` or forget it with `Save.forget`.
    pub fn save(arena: *NewArena) Save {
        const extra_safety: ExtraSafetyState = arena.state.extra_safety;
        if (has_extra_safety_checks) {
            arena.state.extra_safety.saved_count += 1;
            arena.state.extra_safety.saved_block_start = if (arena.state.blocks.first) |b| @ptrToInt(b) else 0;
            arena.state.extra_safety.saved_block_idx = if (arena.state.blocks.first) |b| b.data.idx else 0;
        }
        return .{
            .first_block = arena.state.blocks.first,
            .idx = if (arena.state.blocks.first) |block| block.data.idx else 0,
            .extra_safety = extra_safety,
        };
    }

    /// A saved state.
    pub const Save = struct {
        /// The head of the block list when the save began.
        first_block: ?*BlockNode,
        /// The `arena.state.blocks.first.?.idx` when the save began.
        idx: usize,
        extra_safety: ExtraSafetyState,

        /// Restore to the state when `save()` was called. I keep the current block to avoid reallocating blocks in a loop.
        pub fn restore(saved_: *Save, arena: *NewArena) void {
            // Undefine the memory from `saved_` to prevent bugs if I end up restoring twice. Copy to the stack because `saved_` might be inside a block that we will free.
            const saved = saved_.*;
            saved_.* = undefined;
            saved.end_extra_safety(arena);
            const first_block = arena.state.blocks.first;
            while (true) {
                if (arena.state.blocks.first) |block| {
                    if (block == saved.first_block) {
                        assert(@ptrToInt(saved.first_block.?) + saved.idx <= @ptrToInt(block) + block.data.idx);
                        block.data.idx = saved.idx;
                        break;
                    }
                    arena.state.blocks.first = block.next;
                    if (block != first_block) {
                        destroy_block(arena.child_allocator, block);
                    } else {
                        first_block.?.next = null;
                    }
                } else {
                    // If this assert fails, it probably means that I gave the wrong arena as an argument.
                    assert(saved.first_block == null);
                    break;
                }
            }
            if (first_block != arena.state.blocks.first) {
                const first = first_block.?;
                first.data.idx = @sizeOf(BlockNode);
                arena.state.blocks.prepend(first);
            }
        }

        /// Forget a temporary saved state. It will be as if you didin't call `save()` in the first place. This is useful if you need to restore an arena to a state on error and on success you don't do it. If safety checks are disabled, this is a no-op.
        pub fn forget(saved: *Save, arena: *NewArena) void {
            saved.end_extra_safety(arena);
            saved.* = undefined;
        }

        fn end_extra_safety(saved: Save, arena: *NewArena) void {
            if (has_extra_safety_checks) {
                assert(saved.extra_safety.saved_count == arena.state.extra_safety.saved_count - 1);
                arena.state.extra_safety.saved_count -= 1;
                arena.state.extra_safety.saved_block_start = saved.extra_safety.saved_block_start;
                arena.state.extra_safety.saved_block_idx = saved.extra_safety.saved_block_idx;
            }
        }
    };

    fn alloc(arena: *NewArena, len: usize, ptr_align: u29, len_align: u29, ret_addr: usize) Allocator.Error![]u8 {
        _ = len_align;
        _ = ret_addr;
        const extra_size_for_ptr_align = ptr_align - 1;
        var remaining_size = if (arena.state.blocks.first) |block| blk: {
            assert(block.data.size >= block.data.idx);
            break :blk block.data.size - block.data.idx;
        } else 0;
        const min_buf_needed = std.math.add(usize, extra_size_for_ptr_align, len) catch return error.OutOfMemory;
        if (remaining_size < min_buf_needed) make_block: {
            // Try to resize the block before creating a new one.
            if (arena.state.blocks.first) |block| {
                if (block_request_remaining_size(arena.child_allocator, block, min_buf_needed)) {
                    break :make_block;
                }
            }

            const first_block_size = if (arena.state.blocks.first) |first| first.data.size else 0;
            var requested_size: usize = std.math.add(usize, @sizeOf(BlockNode), min_buf_needed) catch return error.OutOfMemory;
            requested_size = std.math.add(usize, requested_size, first_block_size) catch return error.OutOfMemory;
            requested_size = std.math.add(usize, requested_size, requested_size / 2) catch return error.OutOfMemory;
            arena.state.blocks.prepend(try create_block(arena.child_allocator, requested_size));
        }
        errdefer comptime unreachable;
        const block = arena.state.blocks.first.?;
        remaining_size = block.data.size - block.data.idx;
        assert(remaining_size >= min_buf_needed);
        var result: usize = std.mem.alignForward(@ptrToInt(block) + block.data.idx, ptr_align);
        block.data.idx = result + len - @ptrToInt(block);
        assert(block.data.idx <= block.data.size);
        return @intToPtr([*]u8, result)[0..len];
    }

    fn resize(arena: *NewArena, buf: []u8, buf_align: u29, new_len: usize, len_align: u29, ret_addr: usize) ?usize {
        _ = buf_align;
        _ = ret_addr;
        const old_start = @ptrToInt(@ptrCast([*]u8, buf));
        const old_end = old_start + buf.len;
        const actual_new_len = blk: {
            if (new_len == 0) break :blk 0;
            if (new_len <= buf.len) break :blk std.mem.alignAllocLen(buf.len, new_len, len_align);
            break :blk new_len;
        };
        if (has_extra_safety_checks) {
            // Check that the old memory was not created before the current save state.
            var old_mem_is_valid: bool = false;
            var it = arena.state.blocks.first;
            while (it) |b| : (it = b.next) {
                const b_start = @ptrToInt(b);
                if (b_start == arena.state.extra_safety.saved_block_start)
                    break;
                const b_idx = b_start + b.data.idx;
                if (old_start >= b_start and old_end <= b_idx) {
                    old_mem_is_valid = true;
                    break;
                }
            }
            if (!old_mem_is_valid) {
                if (it) |b| {
                    assert(arena.state.extra_safety.saved_count > 0);
                    const b_start = @ptrToInt(b);
                    assert(b_start == arena.state.extra_safety.saved_block_start);
                    const b_good_start = b_start + arena.state.extra_safety.saved_block_idx;
                    const b_good_end = b_start + b.data.idx;
                    assert(b.data.idx >= arena.state.extra_safety.saved_block_idx);
                    if (old_start >= b_good_start and old_end <= b_good_end) {
                        old_mem_is_valid = true;
                    }
                }
            }
            if (!old_mem_is_valid) {
                @panic("you reallocated memory that was created before the current save or the old memory was not from this arena");
            }
        }
        if (arena.state.blocks.first) |block| {
            const new_end = std.math.add(usize, old_start, actual_new_len) catch return null;
            const block_start = @ptrToInt(block);
            const block_at_idx = block_start + block.data.idx;
            const block_at_size = block_start + block.data.size;
            if (old_end == block_at_idx) {
                if (new_end <= block_at_size) {
                    block.data.idx = new_end - block_start;
                    return actual_new_len;
                } else {
                    assert(new_end > old_end);
                    if (block_request_remaining_size(arena.child_allocator, block, new_end - old_end)) {
                        assert(new_end <= block_start + block.data.size);
                        block.data.idx = new_end - block_start;
                        return actual_new_len;
                    }
                }
            }
        }
        if (actual_new_len <= buf.len) {
            return actual_new_len;
        }
        // Can't resize in place.
        return null;
    }

    fn free(arena: *NewArena, buf: []u8, buf_align: u29, ret_addr: usize) void {
        _ = arena.resize(buf, buf_align, 0, 0, ret_addr) orelse unreachable;
    }
};
