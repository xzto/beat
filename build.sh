#!/bin/sh

set -e

BASE_DIR="$(realpath "$(dirname "$0")")"
cd "$BASE_DIR"

LAST_DIR_FILE=".last_build_dir"
LAST_OPTIONS_FILE=".last_build_options"

CC="${CC:-clang}"
HOSTCC="${HOSTCC:-$CC}"
VERSION="0.0.1"
BUILD_OS=linux
BUILD_ARCH=x86_64
# BUILD_DIR will be concatenated with "-release" or "-debug" later
BUILD_DIR="build/$BUILD_OS-$BUILD_ARCH"
DEBUG_CFLAGS="-g3 -O0 -DBEAT_INTERNAL=1 -DBEAT_SLOW=1 -ffast-math"
RELEASE_CFLAGS="-O2 -DBEAT_INTERNAL=0 -DBEAT_SLOW=0 -DNDEBUG -ffast-math -flto"
GAMEOUT="beat_linuxsdl"

CFLAGS="$CFLAGS -DVERSION=\"$VERSION\""
CFLAGS="$CFLAGS -std=c++11"
CFLAGS="$CFLAGS -Wall -Wextra"
# CFLAGS="$CFLAGS -pg"
CFLAGS="$CFLAGS -Wimplicit-fallthrough"
CFLAGS="$CFLAGS -Wuninitialized"
CFLAGS="$CFLAGS -Winit-self -Wshadow -Wundef"
CFLAGS="$CFLAGS -Wfloat-conversion -Wdouble-promotion"
# CFLAGS="$CFLAGS -Wshorten-64-to-32"
CFLAGS="$CFLAGS -Wconversion"
CFLAGS="$CFLAGS -Wsign-conversion"
# [ "$CC" = "clang" ] && 
    CFLAGS="$CFLAGS -Wno-string-conversion"
# CFLAGS="$CFLAGS -Wpadded"
CFLAGS="$CFLAGS -Wno-unused-parameter -Wno-unused-function -Wno-unused-variable"
CFLAGS="$CFLAGS -Wtype-limits"
[ "$CC" = "clang" ] && CFLAGS="$CFLAGS -Wno-writable-strings"
CFLAGS="$CFLAGS -Wno-write-strings"
CFLAGS="$CFLAGS -Werror=implicit-function-declaration -Werror=return-type"
CFLAGS="$CFLAGS -Wno-c99-designator"
CFLAGS="$CFLAGS -fno-exceptions"
CFLAGS="$CFLAGS -Wnarrowing"
CFLAGS="$CFLAGS -Wmissing-field-initializers"
CFLAGS="$CFLAGS -Werror"

# The generated include path for the C compiler. This is relative to the build directory.
CFLAGS="$CFLAGS -Igenerated_include_path"
# The directory for the generated files. This is realtive to the build directory. This is so I can
# specify "beat/generated" in the #include directive.
generated_dir="generated_include_path/beat/generated"

# sanitize="-fsanitize=undefined,integer,implicit-conversion,address"
# CFLAGS="$CFLAGS $sanitize"
# LDFLAGS="$LDFLAGS $sanitize -lstdc++ -lubsan" # for ubsan

LDFLAGS="$LDFLAGS -lm -pthread"

LDFLAGS="$LDFLAGS -fno-exceptions"

CFLAGS="$CFLAGS -I$BASE_DIR/external"
CFLAGS="$CFLAGS -I$BASE_DIR/external/glad/include"
# CFLAGS="$CFLAGS -I$BASE_DIR/external/glad/src"
LDFLAGS="$LDFLAGS -ldl" # For glad

CFLAGS="$CFLAGS $(pkg-config --cflags sdl2)"
LDFLAGS="$LDFLAGS $(pkg-config --libs sdl2)"

PORTAUDIO_CFLAGS="$(pkg-config --cflags --libs portaudio-2.0)"

CFLAGS="$CFLAGS -Wno-error"

make_realsrc() {
    REALSRC=""
    # No quotes on $@
    for f in $@; do
        REALSRC="$REALSRC $BASE_DIR/$f"
    done
}

print_and_run() {
    echo "$@"
    "$@"
}

options() {
    echo "beat build options:"
    echo "CC        = $CC"
    echo "HOSTCC    = $HOSTCC"
    echo "CFLAGS    = $CFLAGS"
    echo "LDFLAGS   = $LDFLAGS"
    echo "BUILD_DIR = $BUILD_DIR"
}


# TODO: mytime nested
mytime() {
    start_time=$(date "+%s.%N")
    "$@"
    code=$?
    end_time=$(date "+%s.%N")
    echo "took $(echo "$start_time" "$end_time" | awk '{printf "%.2f", $2 - $1}')s"
    return $code
}

dep_time_check() {
    # $1: target filename
    # $2, $3, ...: dependency filenames (at least one)
    # return 0 (success) if the dependency is newer than the target
    test "$#" -gt 0
    x="$1"
    test -f "$x" || return 0
    shift
    while [ "$#" -gt 0 ]; do
        test "$(stat -c %Y "$x" ||:)" -lt "$(stat -c %Y "$1" ||:)" && return 0 || shift
    done
    return 1
}

run_if_not_exist_or_build_everything() {
    # $1: filename to test
    # $2, ...: command to run
    test "$#" -gt 1
    x="$1"
    shift
    if [ ! -f "$x" -o "$build_everything" = 1 ]; then
        echo "$x..."
        "$@"
    fi
    unset x
}

build() {
    build_everything=0

    cwd="$PWD"
    [ -d "$BUILD_DIR" ] || mkdir -p "$BUILD_DIR"

    print_and_run cd "$BUILD_DIR"

    # generated/math_vectors.hh
    # TODO: Don't put the generated files inside 'src'.
    if false || dep_time_check "$generated_dir/math_vectors.hh" "$BASE_DIR/src/codegen_math_vectors.cc" || [ "$build_everything" = 1 ]; then
        make_realsrc "src/codegen_math_vectors.cc"
        OUT="codegen_math_vectors"
        echo "$OUT..."
        mytime $HOSTCC $CFLAGS $REALSRC -o "$OUT" $LDFLAGS
        codegen_path="$(realpath "$PWD/$OUT")"
        mkdir -p "$generated_dir"
        "$codegen_path" "$generated_dir/math_vectors.hh"
        echo
    fi

    # Main program
    if true || [ "$build_everything" = 1 ]; then
        lib_objs=""
        lib_objs="$lib_objs stb_truetype.o"
        run_if_not_exist_or_build_everything "stb_truetype.o" \
            mytime $CC -x c++ -DSTB_TRUETYPE_IMPLEMENTATION -c "$BASE_DIR/external/stb/stb_truetype.h" -o "stb_truetype.o"
        lib_objs="$lib_objs minimp3.o"
        run_if_not_exist_or_build_everything "minimp3.o" \
            mytime $CC -x c++ -DMINIMP3_IMPLEMENTATION -c "$BASE_DIR/external/minimp3/minimp3_ex.h" -o "minimp3.o"
        lib_objs="$lib_objs glad.o"
        run_if_not_exist_or_build_everything "glad.o" \
            mytime $CC -I"$BASE_DIR/external/glad/include/" -c "$BASE_DIR/external/glad/src/glad.c" -o "glad.o"
        make_realsrc "src/unity_build.cc"
        OUT="$GAMEOUT"
        echo "$OUT..."
        mytime $CC $CFLAGS -MMD -c $REALSRC -o game.o
        echo "linking..."
        mytime $CC $CFLAGS $PORTAUDIO_CFLAGS game.o $lib_objs -o "$OUT" $LDFLAGS
        unset lib_objs
        # echo "objdump..."
        # objdump -M intel --headers --source-comment -S -d "$OUT" > "$OUT".lss
        echo "$GAMEOUT done"
        echo
    fi

    # wave_file test
    if false || [ "$build_everything" = 1 ]; then
        make_realsrc "src/experiments/wave_file.cc"
        OUT="wave_file_test"
        echo "$OUT..."
        # echo $CC $REALSRC -o "$OUT"
        mytime $CC $CFLAGS $REALSRC -o "$OUT" $LDFLAGS
        echo
    fi

    cd "$cwd"
}

# printit() {
#     echo $CC $CFLAGS $REALSRC -o "$OUT" $LDFLAGS
# }

debug() {
    CFLAGS="$CFLAGS $DEBUG_CFLAGS"
    LDFLAGS="$LDFLAGS $DEBUG_LDFLAGS"
    BUILD_DIR="${BUILD_DIR}-internal"
    # options >&2
    build
    options > "$LAST_OPTIONS_FILE"
    echo "$BUILD_DIR" > "$LAST_DIR_FILE"
}

release() {
    CFLAGS="$CFLAGS $RELEASE_CFLAGS"
    LDFLAGS="$LDFLAGS $RELEASE_LDFLAGS"
    BUILD_DIR="${BUILD_DIR}-release"
    # options >&2
    build
    options > "$LAST_OPTIONS_FILE"
    echo "$BUILD_DIR" > "$LAST_DIR_FILE"
}

distsrc() {
    pkgdir="beat-distsrc-${VERSION}-$(git describe --always)"
    git archive HEAD -o "$pkgdir.tar.gz" --prefix="$pkgdir/"
    unset pkgdir
}

distbin() {
    release
    pkgdir="beat-distbin-${VERSION}-$(git describe --always)"
    mkdir -p -- "$pkgdir"
    cp -- "$BUILD_DIR/$GAMEOUT" "${pkgdir}/"
    cp -r -- "other_data/" "${pkgdir}/"
    unset pkgdir
}

clean() {
    print_and_run rm -rf -- "$BUILD_DIR"
    print_and_run rm -rf -- "build/"
    print_and_run rm -rf -- "beat-distsrc-"*
    print_and_run rm -rf -- "beat-distbin-"*
    print_and_run rm -f -- "$LAST_DIR_FILE" "$LAST_OPTIONS_FILE"
    print_and_run rm -rf -- "src/generated/"
}

runlast() {
    if [ -f "$LAST_DIR_FILE" ]; then
        last_build_dir="$(cat "$LAST_DIR_FILE")"
        test -d "$last_build_dir"
        cd test_data
        print_and_run "../$last_build_dir/$GAMEOUT" "$@"
        exit "$?"
    else
        echo "Build the thing first!"
    fi
}

runlastgdb() {
    if [ -f "$LAST_DIR_FILE" ]; then
        last_build_dir="$(cat "$LAST_DIR_FILE")"
        test -d "$last_build_dir"
        cd test_data
        print_and_run gdb --quiet --args "../$last_build_dir/$GAMEOUT" "$@"
        exit "$?"
    else
        echo "Build the thing first!"
    fi
}

case "$1" in
    debug)      debug ;;
    release)    release ;;
    options)    options ;;
    distsrc)    distsrc ;;
    distbin)    distbin ;;
    clean)      clean ;;
    # print)      printit ;;
    run)        shift; runlast "$@" ;;
    gdb)        shift; runlastgdb "$@" ;;
    *)          debug ;;
esac
